
Boston University P/N G2-LOGIC Rev B

6 layers
0.062 final thickness
surface finish ENIG
soldermask both sides (green)
silkscreen both sides (white)

Top and bottom edges of board must be routed for card guides if panelization is used (no tabs or vscore).
Tabs or vscore are ok on right and left sides if panelized.


all copper 1/2 oz initial

G2-LOGIC-revB-B_Mask.pho
G2-LOGIC-revB-B_Paste.pho
G2-LOGIC-revB-B_SilkS.pho
G2-LOGIC-revB.drl
G2-LOGIC-revB-drl_map.pho
G2-LOGIC-revB-Fab_dwg.pho
G2-LOGIC-revB-F_Mask.pho
G2-LOGIC-revB-F_Paste.pho
G2-LOGIC-revB-F_SilkS.pho
G2-LOGIC-revB-Layer1.pho
G2-LOGIC-revB-Layer2.pho
G2-LOGIC-revB-Layer3.pho
G2-LOGIC-revB-Layer4.pho
G2-LOGIC-revB-Layer5.pho
G2-LOGIC-revB-Layer6.pho
G2-LOGIC-revB-NPTH.drl
G2-LOGIC-revB-NPTH-drl_map.pho
