PCBNEW-LibModule-V1  Sun 23 Nov 2014 09:50:12 PM EST
# encoding utf-8
Units mm
$INDEX
ZL40212
$EndINDEX
$MODULE ZL40212
Po 0 0 0 15 54729CDE 00000000 ~~
Li ZL40212
Sc 0
AR 
Op 0 0 0
T0 0.0762 5.715 1 1 0 0.15 N V 21 N "ZL40212"
T1 0 3.5052 1 1 0 0.15 N V 21 N "VAL**"
DS -1.5 1 -1 1.5 0.15 21
DS 1.5 1.5 1.5 1 0.15 21
DS 1 1.5 1.5 1.5 0.15 21
DS -1.5 -1.5 -1.5 -1 0.15 21
DS -1 -1.5 -1.5 -1.5 0.15 21
DS 1 -1.5 1.5 -1.5 0.15 21
DS 1.5 -1.5 1.5 -1 0.15 21
$PAD
Sh "2" R 0.25 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.25 1.5
$EndPAD
$PAD
Sh "1" R 0.25 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.75 1.5
$EndPAD
$PAD
Sh "3" R 0.25 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.25 1.5
$EndPAD
$PAD
Sh "4" R 0.25 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.75 1.5
$EndPAD
$PAD
Sh "13" R 0.6 0.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.5 -0.75
$EndPAD
$PAD
Sh "14" R 0.6 0.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.5 -0.25
$EndPAD
$PAD
Sh "15" R 0.6 0.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.5 0.25
$EndPAD
$PAD
Sh "16" R 0.6 0.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.5 0.75
$EndPAD
$PAD
Sh "5" R 0.6 0.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.5 0.75
$EndPAD
$PAD
Sh "6" R 0.6 0.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.5 0.25
$EndPAD
$PAD
Sh "7" R 0.6 0.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.5 -0.25
$EndPAD
$PAD
Sh "8" R 0.6 0.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.5 -0.75
$EndPAD
$PAD
Sh "9" R 0.25 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.75 -1.5
$EndPAD
$PAD
Sh "10" R 0.25 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.25 -1.5
$EndPAD
$PAD
Sh "12" R 0.25 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.75 -1.5
$EndPAD
$PAD
Sh "11" R 0.25 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.25 -1.5
$EndPAD
$EndMODULE ZL40212
$EndLIBRARY
