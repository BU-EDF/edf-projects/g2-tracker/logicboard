PCBNEW-LibModule-V1  Thu 20 Nov 2014 04:49:47 PM EST
# encoding utf-8
Units mm
$INDEX
TSSOP20
$EndINDEX
$MODULE TSSOP20
Po 0 0 0 15 546E61C8 00000000 ~~
Li TSSOP20
Sc 0
AR 
Op 0 0 0
T0 0 0 1.524 1.524 0 0.3048 N V 21 N "TSSOP20"
T1 0 0 1.524 1.524 0 0.3048 N V 21 N "VAL**"
DS -3.24866 -2.19964 3.24866 -2.19964 0.19812 21
DS 3.24866 -2.19964 3.24866 2.19964 0.19812 21
DS -3.24866 2.19964 3.24866 2.19964 0.19812 21
DS -3.24866 -2.19964 -3.24866 2.19964 0.19812 21
DS -3.24866 1.59766 -2.64922 2.19964 0.19812 21
$PAD
Sh "1" R 0.39878 1.4 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.92354 2.84988
$EndPAD
$PAD
Sh "2" R 0.39878 1.4 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.2733 2.84988
$EndPAD
$PAD
Sh "3" R 0.39878 1.4 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.62306 2.84988
$EndPAD
$PAD
Sh "4" R 0.39878 1.4 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.97282 2.84988
$EndPAD
$PAD
Sh "5" R 0.39878 1.4 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.32258 2.84988
$EndPAD
$PAD
Sh "6" R 0.39878 1.4 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.32258 2.84988
$EndPAD
$PAD
Sh "7" R 0.39878 1.4 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.97282 2.84988
$EndPAD
$PAD
Sh "8" R 0.39878 1.4 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.62306 2.84988
$EndPAD
$PAD
Sh "9" R 0.39878 1.4 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.2733 2.84988
$EndPAD
$PAD
Sh "10" R 0.39878 1.4 0 0 1800
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.92354 2.84988
$EndPAD
$PAD
Sh "11" R 0.39878 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.92354 -2.84988
$EndPAD
$PAD
Sh "12" R 0.39878 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.2733 -2.84988
$EndPAD
$PAD
Sh "13" R 0.39878 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.62306 -2.84988
$EndPAD
$PAD
Sh "14" R 0.39878 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.97282 -2.84988
$EndPAD
$PAD
Sh "15" R 0.39878 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.32258 -2.84988
$EndPAD
$PAD
Sh "16" R 0.39878 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.32258 -2.84988
$EndPAD
$PAD
Sh "17" R 0.39878 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.97282 -2.84988
$EndPAD
$PAD
Sh "18" R 0.39878 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.62306 -2.84988
$EndPAD
$PAD
Sh "19" R 0.39878 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.2733 -2.84988
$EndPAD
$PAD
Sh "20" R 0.39878 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.92354 -2.84988
$EndPAD
$EndMODULE TSSOP20
$EndLIBRARY
