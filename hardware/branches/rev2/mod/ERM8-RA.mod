PCBNEW-LibModule-V1  Thu 08 Jan 2015 03:34:02 PM EST
# encoding utf-8
Units mm
$INDEX
ERM8-RA
$EndINDEX
$MODULE ERM8-RA
Po 0 0 0 15 54AEE9A4 00000000 ~~
Li ERM8-RA
Sc 0
AR 
Op 0 0 0
T0 0 -6.1 1 1 0 0.15 N V 21 N "ERM8-RA"
T1 0 4.1 1 1 0 0.15 N V 21 N "VAL**"
DC 0 0 0.11 -0.01 0.05 21
DC 0 0 0.19 -0.14 0.05 21
T2 21 -0.75 1 1 0 0.15 N V 21 N "99"
T2 21.3 0.9 0.9 0.9 0 0.15 N V 21 N "100"
T2 -20.85 0.95 1 1 0 0.15 N V 21 N "2"
T2 -20.85 -0.85 1 1 0 0.15 N V 21 N "1"
DS 20.76 1.63 -20.75 1.63 0.15 21
DS 20.76 7.61 20.76 1.63 0.15 21
DS -20.75 7.61 -20.75 1.63 0.15 21
DS 26.51 7.61 20.76 7.61 0.15 21
DS -26.5 7.61 -20.75 7.61 0.15 21
DS -26.5 -3.74 -26.5 7.61 0.15 21
DS 26.51 -3.74 -26.5 -3.74 0.15 21
DS 26.51 -3.74 26.51 7.61 0.15 21
$PAD
Sh "" C 1.91 1.91 0 0 0
Dr 1.91 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po -23.62 4.26
$EndPAD
$PAD
Sh "" C 1.91 1.91 0 0 0
Dr 1.91 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 23.63 4.26
$EndPAD
$PAD
Sh "" C 1.45 1.45 0 0 0
Dr 1.45 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po -23.62 0.26
$EndPAD
$PAD
Sh "27" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -9.2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "28" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -9.2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "4" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -18.8 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "2" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -19.6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "6" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -18 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "8" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -17.2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "16" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -14 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "14" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -14.8 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "10" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -16.4 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "12" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -15.6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "30" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -8.4 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "26" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -10 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "32" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -7.6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "34" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.8 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "24" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -10.8 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "22" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -11.6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "18" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -13.2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "20" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -12.4 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "54" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "52" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.4 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "56" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "58" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.8 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "66" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "64" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "60" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "62" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.4 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "46" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "44" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.8 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "48" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "50" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.4 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "42" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "40" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.4 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "36" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "38" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "100" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 19.6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "86" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 14 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "84" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 13.2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "88" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 14.8 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "90" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 15.6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "98" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 18.8 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "96" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 18 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "92" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 16.4 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "94" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 17.2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "78" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 10.8 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "76" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 10 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "80" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 11.6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "82" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 12.4 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "74" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 9.2 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "72" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 8.4 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "68" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.8 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "70" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7.6 0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "69" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7.6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "67" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.8 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "71" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 8.4 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "73" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 9.2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "81" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 12.4 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "79" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 11.6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "75" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 10 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "77" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 10.8 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "93" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 17.2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "91" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 16.4 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "95" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 18 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "97" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 18.8 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "89" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 15.6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "87" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 14.8 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "83" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 13.2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "85" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 14 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "99" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 19.6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "37" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "35" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "39" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.4 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "41" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "49" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.4 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "47" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "43" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.8 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "45" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "61" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.4 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "59" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "63" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "65" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "57" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.8 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "55" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "51" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.4 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "53" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "19" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -12.4 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "17" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -13.2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "21" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -11.6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "23" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -10.8 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "33" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.8 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "31" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -7.6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "25" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -10 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "29" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -8.4 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "11" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -15.6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "9" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -16.4 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "13" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -14.8 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "15" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -14 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "7" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -17.2 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "5" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -18 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "1" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -19.6 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "3" R 0.5 1.25 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -18.8 -0.9
.LocalClearance 0.07
$EndPAD
$PAD
Sh "" C 1.45 1.45 0 0 0
Dr 1.45 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 23.63 0.26
$EndPAD
$EndMODULE ERM8-RA
$EndLIBRARY
