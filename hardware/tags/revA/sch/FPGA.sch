EESchema Schematic File Version 2
LIBS:sn65hvd3083edgsr
LIBS:conn
LIBS:power
LIBS:pspice
LIBS:zl40216
LIBS:sfp
LIBS:FIN1108
LIBS:sn65lvds389
LIBS:max5825baup
LIBS:tps72301dbvr
LIBS:ina195a
LIBS:tps78630kttt
LIBS:tps73501drbt
LIBS:lt3015eq
LIBS:lt1086cm-3
LIBS:tps72012drvt
LIBS:ina202a
LIBS:transistors
LIBS:2sk3019
LIBS:sn74hc05d
LIBS:ad7998
LIBS:ao3401a
LIBS:opa703
LIBS:6slx45fgg484pkg
LIBS:ERM8-RA
LIBS:cdclvd1204
LIBS:sy89835u
LIBS:zl40212
LIBS:opa4703ea
LIBS:tps73201qdbvrq1
LIBS:m25p32
LIBS:bav99
LIBS:si590
LIBS:iso7242c
LIBS:lmk00304
LIBS:pins_14x2
LIBS:ds18b20u
LIBS:2x14_conn
LIBS:led
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 16 23
Title ""
Date "17 mar 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1850 4150 2500 4150
Wire Bus Line
	6200 1500 6550 1500
Wire Bus Line
	6200 1300 6550 1300
Wire Bus Line
	6200 2450 6550 2450
Wire Wire Line
	2500 1800 2150 1800
Wire Wire Line
	1950 3250 2500 3250
Wire Wire Line
	1950 3350 2500 3350
Wire Wire Line
	1950 3450 2500 3450
Wire Wire Line
	1950 3550 2500 3550
Wire Wire Line
	2500 3650 1950 3650
Wire Wire Line
	1950 2400 2500 2400
Wire Bus Line
	1950 2100 2500 2100
Wire Bus Line
	1950 2300 2500 2300
Wire Bus Line
	1950 2500 2500 2500
Wire Wire Line
	1850 3950 2500 3950
Wire Wire Line
	1850 4050 2500 4050
Wire Wire Line
	1850 4250 2500 4250
Wire Wire Line
	2500 1900 2150 1900
Wire Bus Line
	6200 2350 6550 2350
Wire Bus Line
	6200 1400 6550 1400
Wire Bus Line
	6200 1600 6550 1600
Text HLabel 6550 1600 2    60   Input ~ 0
TDO[1..0]
Text HLabel 6550 1500 2    60   Output ~ 0
TDI[1..0]
Text HLabel 6550 1400 2    60   Output ~ 0
TMS[1..0]
Text HLabel 6550 1300 2    60   Output ~ 0
TCK[1..0]
Text HLabel 6550 2450 2    60   Output ~ 0
TEMP_PWR[1..0]
Text HLabel 6550 2350 2    60   BiDi ~ 0
TEMP[1..0]
Text HLabel 2150 1900 0    60   BiDi ~ 0
SDA
Text HLabel 2150 1800 0    60   Output ~ 0
SCL
Text HLabel 6550 3950 2    60   Output ~ 0
TDC_TP_EN[3..0]
Text HLabel 6550 3850 2    60   Output ~ 0
TDC_TP[7..0]
Text HLabel 1950 2100 0    60   Output ~ 0
Data_out[1..0]
Text HLabel 1950 2400 0    60   Output ~ 0
C5_SEL
Text HLabel 1950 2300 0    60   Input ~ 0
C5_IN[1..0]
Text HLabel 1950 2500 0    60   Output ~ 0
C5_OUT[1..0]
Text HLabel 1850 4050 0    60   Input ~ 0
SC_IN
Text HLabel 1850 4150 0    60   Output ~ 0
SC_OUT
Text HLabel 1850 3950 0    60   Input ~ 0
_SC_PRESENT
Text HLabel 1850 4250 0    60   Output ~ 0
SC_OE
Text HLabel 1950 3250 0    60   Output ~ 0
SFP_SCL
Text HLabel 1950 3350 0    60   BiDi ~ 0
SFP_SDA
Text HLabel 1950 3450 0    60   Input ~ 0
Present
Text HLabel 1950 3550 0    60   Output ~ 0
Tx_Disable
Text HLabel 1950 3650 0    60   Input ~ 0
LOS
$Sheet
S 2500 1250 1750 1750
U 5461B1E8
F0 "Bank 0" 50
F1 "sch/FPGA_Bank0.sch" 50
F2 "Data_out[1..0]" O L 2500 2100 60 
F3 "C5_SEL" O L 2500 2400 60 
F4 "C5_IN[1..0]" I L 2500 2300 60 
F5 "C5_OUT[1..0]" O L 2500 2500 60 
F6 "SCL" O L 2500 1800 60 
F7 "SDA" B L 2500 1900 60 
F8 "TDC_P2" I L 2500 1300 60 
F9 "TDC_N2" I L 2500 1400 60 
F10 "TDC_P0" I L 2500 1500 60 
F11 "TDC_N0" I L 2500 1600 60 
F12 "LED[3..0]" O L 2500 2800 60 
$EndSheet
$Sheet
S 8250 1550 1450 1450
U 5461B237
F0 "Bank power" 50
F1 "sch/FPGA_Bank_Pwr.sch" 50
$EndSheet
Text Notes 3500 1400 0    60   ~ 0
LVDS out
Text Notes 4350 4450 0    60   ~ 0
LVDS out
$Sheet
S 8400 3400 1150 1050
U 54610156
F0 "FPGA programming" 50
F1 "sch/FPGA_Programming.sch" 50
$EndSheet
$Sheet
S 4450 3200 1750 1750
U 5461B19F
F0 "Bank 2" 50
F1 "sch/FPGA_Bank2.sch" 50
F2 "EN1V6" O R 6200 3550 60 
F3 "CLK_IN" I R 6200 4400 60 
F4 "TDC_TP_EN[3..1]" O R 6200 3950 60 
F5 "TDC_TP[7..2]" O R 6200 3850 60 
F6 "GPIO[3..0]" B R 6200 4850 60 
$EndSheet
Text HLabel 6500 4400 2    60   Input ~ 0
CLK_IN
Wire Wire Line
	6200 4400 6500 4400
Text HLabel 2250 4500 0    60   Output ~ 0
DB_SCL
Text HLabel 2250 4600 0    60   BiDi ~ 0
DB_SDA
Text HLabel 2250 4400 0    60   Input ~ 0
DB_INT
$Sheet
S 4450 1250 1750 1750
U 5461B187
F0 "Bank 1" 50
F1 "sch/FPGA_Bank1.sch" 50
F2 "EN-1V6" O R 6200 2600 60 
F3 "TEMP[1..0]" B R 6200 2350 60 
F4 "TEMP_PWR[1..0]" O R 6200 2450 60 
F5 "TDO[1..0]" I R 6200 1600 60 
F6 "TDI[1..0]" O R 6200 1500 60 
F7 "TMS[1..0]" O R 6200 1400 60 
F8 "TCK[1..0]" O R 6200 1300 60 
F9 "TDC_P1" I R 6200 1750 60 
F10 "TDC_N1" I R 6200 1850 60 
F11 "TDC_P3" I R 6200 1950 60 
F12 "TDC_N3" I R 6200 2050 60 
F13 "TDC_TP_EN0" I R 6200 2750 60 
F14 "TDC_TP0" I R 6200 2850 60 
F15 "TDC_TP1" I R 6200 2950 60 
F16 "TDC_I2C_CLK" O R 6200 2150 60 
F17 "TDC_I2C_DATA" B R 6200 2250 60 
$EndSheet
Wire Wire Line
	2500 4400 2250 4400
Wire Wire Line
	2500 4500 2250 4500
Wire Wire Line
	2500 4600 2250 4600
Text HLabel 6550 3550 2    60   Output ~ 0
EN1V6
Wire Wire Line
	6850 2600 6200 2600
Text HLabel 6850 2600 2    60   Output ~ 0
EN-1V6
Wire Wire Line
	6550 3550 6200 3550
Text HLabel 7300 1000 2    60   Input ~ 0
TDC_P[3..0]
Text HLabel 7300 900  2    60   Input ~ 0
TDC_N[3..0]
$Sheet
S 2500 3200 1750 1750
U 5461B208
F0 "Bank 3" 50
F1 "sch/FPGA_Bank3.sch" 50
F2 "SC_IN" I L 2500 4050 60 
F3 "SC_OUT" O L 2500 4150 60 
F4 "_SC_PRESENT" I L 2500 3950 60 
F5 "SC_OE" O L 2500 4250 60 
F6 "DB_SCL" O L 2500 4500 60 
F7 "DB_SDA" B L 2500 4600 60 
F8 "DB_INT" I L 2500 4400 60 
F9 "SFP_SCL" O L 2500 3250 60 
F10 "SFP_SDA" B L 2500 3350 60 
F11 "Present" I L 2500 3450 60 
F12 "Tx_Disable" O L 2500 3550 60 
F13 "LOS" I L 2500 3650 60 
$EndSheet
Text Label 6750 1000 0    60   ~ 0
TDC_P[3..0]
Text Label 6750 900  0    60   ~ 0
TDC_N[3..0]
Text Label 6250 1950 0    60   ~ 0
TDC_P3
Text Label 6250 2050 0    60   ~ 0
TDC_N3
Text Label 2100 1300 0    60   ~ 0
TDC_P2
Text Label 2100 1400 0    60   ~ 0
TDC_N2
Text Label 2100 1500 0    60   ~ 0
TDC_P0
Text Label 2100 1600 0    60   ~ 0
TDC_N0
Wire Wire Line
	2100 1300 2500 1300
Wire Wire Line
	2100 1400 2500 1400
Wire Wire Line
	2100 1500 2500 1500
Wire Wire Line
	2100 1600 2500 1600
Wire Wire Line
	6200 1950 6600 1950
Wire Wire Line
	6200 2050 6600 2050
Wire Bus Line
	7300 900  6750 900 
Wire Bus Line
	6750 1000 7300 1000
Text Label 6250 1750 0    60   ~ 0
TDC_P1
Text Label 6250 1850 0    60   ~ 0
TDC_N1
Wire Wire Line
	6200 1750 6600 1750
Wire Wire Line
	6200 1850 6600 1850
Wire Bus Line
	6200 3850 6550 3850
Wire Bus Line
	6200 3950 6550 3950
Wire Wire Line
	6200 2750 6800 2750
Wire Wire Line
	6200 2950 6800 2950
Wire Wire Line
	6200 2850 6800 2850
Text Label 6800 2750 2    60   ~ 0
TDC_TP_EN0
Text Label 6650 2850 2    60   ~ 0
TDC_TP0
Text Label 6650 2950 2    60   ~ 0
TDC_TP1
Text HLabel 6550 2150 2    60   Output ~ 0
TDC_I2C_CLK
Text HLabel 6550 2250 2    60   BiDi ~ 0
TDC_I2C_DATA
Wire Wire Line
	6200 2150 6550 2150
Wire Wire Line
	6200 2250 6550 2250
Text HLabel 6500 4850 2    60   BiDi ~ 0
GPIO[3..0]
Wire Bus Line
	6200 4850 6500 4850
Text HLabel 2300 2800 0    60   Output ~ 0
LED[3..0]
Wire Bus Line
	2500 2800 2300 2800
$EndSCHEMATC
