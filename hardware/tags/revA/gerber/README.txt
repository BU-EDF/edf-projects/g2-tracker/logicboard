
Boston University P/N G2-LOGIC Rev A

6 layers
0.062 final thickness
surface finish ENIG
soldermask both sides (green)
silkscreen both sides (white)

all copper 1/2 oz initial

G2-LOGIC-B_Mask.pho
G2-LOGIC-B_Paste.pho
G2-LOGIC-B_SilkS.pho
G2-LOGIC.drl
G2-LOGIC-drl_map.pho
G2-LOGIC-Fab_dwg.pho
G2-LOGIC-F_Mask.pho
G2-LOGIC-F_Paste.pho
G2-LOGIC-F_SilkS.pho
G2-LOGIC-Layer1.pho
G2-LOGIC-Layer2.pho
G2-LOGIC-Layer3.pho
G2-LOGIC-Layer4.pho
G2-LOGIC-Layer5.pho
G2-LOGIC-Layer6.pho
G2-LOGIC-NPTH.drl
G2-LOGIC-NPTH-drl_map.pho
