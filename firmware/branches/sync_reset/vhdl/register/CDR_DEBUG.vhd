----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package TDC_CDR_Debugging is
  type CDR_history_t is record
    ready : std_logic;
--    full : std_logic;
    valid  : std_logic;
    edges  : std_logic_vector(4 downto 0);
    samples : std_logic_vector(4 downto 0);
    empty : std_logic;
  end record CDR_history_t;
  type CDR_history_control_t is record
    clk    : std_logic;
    enable : std_logic;
    reset  : std_logic;
    rd     : std_logic;
  end record CDR_history_control_t;   
end package TDC_CDR_Debugging;
