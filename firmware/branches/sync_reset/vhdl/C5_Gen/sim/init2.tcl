restart
isim force add /c5_top/clk40 1 -value 0 -time 12.5 ns -repeat 25 ns 
isim force add /c5_top/clk 1 -value 0 -time 4 ns -repeat 8 ns 

isim force add /c5_top/rst_n 0
isim force add /c5_top/c5_en 0
run 1us
isim force add /c5_top/rst_n 1



run 1us


for {set i 0} {$i < 16} {incr i} {
    isim force add {/c5_top/din} $i -radix dec
    isim force add /c5_top/c5_en 1

    run 10ns

    isim force add /c5_top/c5_en 0

    run 10us
}