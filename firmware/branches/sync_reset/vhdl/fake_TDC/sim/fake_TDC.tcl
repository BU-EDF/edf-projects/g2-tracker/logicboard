restart

isim force add {/fake_TDC/clk125}  1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns 
isim force add {/fake_TDC/reset} 1

isim force add {/fake_TDC/send_spill} 0
isim force add {/fake_TDC/spill_number} 0 -radix dec
isim force add {/fake_TDC/spill_time} 0 -radix dec
isim force add {/fake_TDC/spill_hit_count} 10 -radix dec

run 32 ns

isim force add {/fake_TDC/reset} 0

run 100us

isim force add {/fake_TDC/send_spill} 1
run 8ns
isim force add {/fake_TDC/send_spill} 0

run 1ms
