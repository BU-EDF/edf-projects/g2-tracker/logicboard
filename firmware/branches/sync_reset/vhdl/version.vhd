----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
-- Create Date:    
-- Design Name: 
-- Module Name:    top_module - Behavioral
-- Project Name: LogicBoard
-- Target Devices: XC6SLX9-TQFP144
-- Tool versions: 13.3 (lin64)
-- Description: 
--
-- Dependencies: 
--
-- Revision:
-- Revision 0.1 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
use IEEE.std_logic_misc.all;
use ieee.std_logic_arith.all;

entity version is

  port (
    year    : out std_logic_vector(7 downto 0);
    month   : out std_logic_vector(7 downto 0);
    day     : out std_logic_vector(7 downto 0);
    version : out std_logic_vector(7 downto 0));

end entity version;

architecture Behavioral of version is
  constant firmware_year : integer := 15; -- year 20YY
  constant firmware_month : integer := 12;
  constant firmware_day     : integer := 8;
  constant firmware_version : integer := 1;
begin  -- architecture Behavioral
  year    <= conv_std_logic_vector(firmware_year, 8);
  month   <= conv_std_logic_vector(firmware_month, 8);
  day     <= conv_std_logic_vector(firmware_day, 8);
  version <= conv_std_logic_vector(firmware_version, 8);


end architecture Behavioral;
