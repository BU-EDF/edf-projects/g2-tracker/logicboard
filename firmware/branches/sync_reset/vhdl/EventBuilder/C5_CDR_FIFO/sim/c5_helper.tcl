
#generate one period of the c5 clock
proc send_cycle {c5_type c5_stream c5_clock} {
    #loop over the four quarters of the c5 period
    switch $c5_type {
	w {
	    isim force add $c5_stream 1
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns

	    isim force add $c5_stream 0
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns
	}
	n {
	    isim force add $c5_stream 1
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns

	    isim force add $c5_stream 0
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns
	}
	default {
	    isim force add $c5_stream 1
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns


	    isim force add $c5_stream 0
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns
	    isim force add $c5_clock 1
	    run 12.5 ns
	    isim force add $c5_clock 0
	    run 12.5 ns
	}
    }
}


proc send_c5_control {k_char c5_stream c5_clock} {
    switch $k_char {
	3 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	}
	5 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	}
	6 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	}
	7 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
	11 {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	}
	13 {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	}
	14 {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	}
	15 {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
	default {
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
    }
}
proc send_c5_data {data_char c5_stream c5_clock} {
    switch $data_char {
	0 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock 
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	}
	1 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
	2 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
	3 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	}
	4 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
	5 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	}
	6 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	}
	7 {
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
	8 {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	}
	9 {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
	A {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
	B {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	}
	C {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
	D {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	}
	E {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	}
	F {
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle w $c5_stream $c5_clock
	    send_cycle n $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
	default {
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	    send_cycle 0 $c5_stream $c5_clock
	}
    }
}
