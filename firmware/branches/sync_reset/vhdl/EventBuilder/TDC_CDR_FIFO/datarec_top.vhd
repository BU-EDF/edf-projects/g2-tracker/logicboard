-------------------------------------------------------------------------------
-- datarec.vhd : top-level data recovery
-- use mask produced by decoder blocks to pick a bit from shift register
--
-- seems to work ok
-- 15 Aug 2013, esh - add extra register on output
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;
use work.TDC_CDR_Debugging.all;

entity datarec is

  generic (
    W : integer := 10;                  -- input transition window width
    M : integer := 5);                  -- oversampling factor

  port (
    clk   : in  std_logic;              -- oversampling (125MHz) clock
    rst_n : in  std_logic;              -- active low reset
    ser   : in  std_logic;              -- serial input
    dv    : out std_logic;              -- data valid
    edges : out std_logic_vector(4 downto 0);
    histo : out std_logic_vector(4 downto 0);
    multi_valid : out std_logic;
    override_setting : in std_logic_vector(2 downto 0); -- override the auto
                                                        -- detect phase
    lost_alignment : out std_logic_vector(M-1 downto 0);
    phase_switch : out std_logic;
    d     : out std_logic;
    CDR_history   : out CDR_history_t;
    CDR_history_control : in CDR_history_control_t
    );             -- recovered data

end entity datarec;

architecture arch of datarec is

  component edge is
    generic (
      SOFF : integer;
      M : integer);
    port (
      clk   : in  std_logic;
      rst_n : in  std_logic;
      ser   : in  std_logic;
      en    : out std_logic;
      sreg  : out std_logic_vector(M-1 downto 0);
      edges : out std_logic_vector(M-1 downto 0));
  end component edge;

  component deblock is
    generic (
      W   : integer;
      ENO : integer;
      M   : integer);
    port (
      clk     : in  std_logic;
      rst_n   : in  std_logic;
      en      : in  std_logic;
      edges   : in  std_logic_vector(M-1 downto 0);
      enable_multi_phase : std_logic;
      aligned : out std_logic;
      lost_alignment : out std_logic);
  end component deblock;

  component CDR_History_FIFO is
    port (
      srst   : IN  STD_LOGIC;
      clk    : IN  STD_LOGIC;
      din    : IN  STD_LOGIC_VECTOR(10 DOWNTO 0);
      wr_en  : IN  STD_LOGIC;
      rd_en  : IN  STD_LOGIC;
      dout   : OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
      full   : OUT STD_LOGIC;
      empty  : OUT STD_LOGIC;
      valid  : OUT STD_LOGIC);
  end component CDR_History_FIFO;

  component pacd is
    port (
      iPulseA : IN  std_logic;
      iClkA   : IN  std_logic;
      iRSTAn  : IN  std_logic;
      iClkB   : IN  std_logic;
      iRSTBn  : IN  std_logic;
      oPulseB : OUT std_logic);
  end component pacd;
  
  signal enable_8b10b_bit        : std_logic;
  signal local_edges     : std_logic_vector(M-1 downto 0);
  signal bit_valid : std_logic_vector(M-1 downto 0);
  signal bit_valid_last : std_logic_vector(M-1 downto 0) := (others => '0');
  signal sreg      : std_logic_vector(M-1 downto 0);
  signal enable_multi_phase : std_logic := '0';
  
  signal d_r, dv_r : std_logic;

  signal edge_counter : std_logic_vector(M-1 downto 0) := (others => '0');
  signal phase_shift : std_logic := '0';
  signal lost_alignment_buffer : std_logic_vector(4 downto 0) := (others => '0');

  --history
  constant CDR_history_buffer_size : integer := 64;
  signal CDR_history_buffer_valid : std_logic_vector(CDR_history_buffer_size -1 downto 0) := (others => '0');
  type shift_register_array_t is array (0 to 10) of std_logic_vector(CDR_history_buffer_size -1 downto 0);
  signal CDR_history_buffer : shift_register_array_t := (others => (others => '0'));
  signal CDR_history_reset : std_logic := '0';

  signal lost_alignment_shift_register : std_logic_vector(31 downto 0) := x"00000000";
  
  signal reset_CDR_History_FIFO : std_logic := '0';
  signal full_CDR_History_FIFO : std_logic := '1';
  signal din_CDR_history_FIFO : std_logic_vector(9 downto 0) := (others => '0');
  signal dout_CDR_history_FIFO : std_logic_vector(9 downto 0) := (others => '0');
  signal wr_CDR_history_FIFO : std_logic := '0';
  signal rd_CDR_history_FIFO : std_logic := '0';
  
  signal CDR_history_buffer_has_data : std_logic := '0';
  type history_state_t is (history_state_MONITOR,history_state_POST_MONITOR,history_state_PRE_RECORD,history_state_REMOVE_INVALID,history_state_FILL_FIFO,history_state_IDLE,history_state_RESET);
  signal history_state : history_state_t := history_state_RESET;
  
begin  -- architecture arch
  

  -- edge detector
  edge_1 : edge
    generic map (
      M => M,
      SOFF => 2)  -- should be (i+(M+1)/3)mod 5, so  (i + 3) mod(5) but easier
                  -- to do (i - 2) mod (5)
    port map (
      clk   => clk,
--      rst_n => rst_n,
      rst_n => '1',
      ser   => ser,
      en    => enable_8b10b_bit,
      sreg  => sreg,
      edges => local_edges);

  -- M decoder blocks to check bit alignment
  f1 : for k in 0 to M-1 generate
    deblock_1 : deblock
      generic map (
        W   => W,
        ENO => k,
        M   => M)
      port map (
        clk     => clk,
--        rst_n   => rst_n,
        rst_n   => '1',
        en      => enable_8b10b_bit,
        edges   => local_edges,
        enable_multi_phase => enable_multi_phase,
        aligned => bit_valid(k),
        lost_alignment => lost_alignment_buffer(k));
  end generate f1;
  lost_alignment <= lost_alignment_buffer;
  
  -- 1 of M in bit_valid should be set
  process (clk) is
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge

      d  <= d_r;
      dv <= dv_r;  -- extra register in output to ease timing

      dv_r <= '0';

      if enable_8b10b_bit = '1' then

        dv_r <= '1';        
        -- by default of the case statement, this is off
        enable_multi_phase <= '0';
        
        case override_setting is
          -- hard-coded phases
          when "001" => d_r <= sreg(0);
          when "010" => d_r <= sreg(1);
          when "011" => d_r <= sreg(2);
          when "100" => d_r <= sreg(3);
          when "101" => d_r <= sreg(4);
          when "110" =>
            enable_multi_phase <= '1';
            -- new algorithm
            dv_r <= '0';
            if or_reduce(bit_valid) = '1' then
              --  (p_i xor p_{i-1} ) and p_i gives the bit to use (simplified
              --  to p_i and not p_{i-1}
              -- The and with sreg selects the value of that bit and or_reduce
              -- gives us that result as one bit
              d_r <= or_reduce( ( bit_valid and ( not ( bit_valid(M-2 downto 0) & bit_valid(M-1) ) ) ) and sreg);
              dv_r <= '1';
            end if;
          when others =>
            -- old algorithm
            dv_r <= '0';
            if or_reduce(bit_valid) = '1' then
              d_r  <= or_reduce(bit_valid and sreg);
              dv_r <= '1';
            end if;
            
        end case;
      end if;            
    end if;
  end process;

  edge_reporting: process (clk) is
  begin  -- process edge_reporting
    if clk'event and clk = '1' then  -- rising clock edge
      multi_valid <= '0';       
      if enable_8b10b_bit = '1' then
        -- monitor chosen phase and look for shifts
        bit_valid_last <= bit_valid;
        phase_switch <= '0';
        if bit_valid /= bit_valid_last then
          phase_switch <= '1';
        end if;
        
        histo <= bit_valid;
        edges <= local_edges;
        -- report if multiple phases report as valid
        multi_valid <= '0';
        if (not (bit_valid = "00001" or
                 bit_valid = "00010" or
                 bit_valid = "00100" or
                 bit_valid = "01000" or
                 bit_valid = "10000" or
                 bit_valid = "00000" )) then
          multi_valid <= '1';
        end if;
      end if;
    end if;
  end process edge_reporting;




  CDR_history_reset <= CDR_history_control.reset;

  --Keep a history of the last 
  CDR_record_history_proc: process (clk) is
  begin  -- process CDR_history_proc
    if clk'event and clk = '1' then  -- rising clock edge
      reset_CDR_History_FIFO <= '0';
      wr_CDR_history_FIFO <= '0';
      CDR_history.ready <= '0';
      
      if CDR_history_reset = '1' then
        --reset the state machine
        history_state <= history_state_RESET;        
      else
        -- all other state machine actions
        case history_state is
          when history_state_MONITOR =>
            ---------------------------------------------------------------------------
            -- history_state_MONITOR
            -- capture history until an error
            history_state <= history_state_MONITOR;
            
            --record values every 5 125Mhz clocks (1 25Mhz bit)
            if  enable_8b10b_bit = '1' then
              CDR_history_buffer_has_data <= '1';
              CDR_history_buffer_valid <= CDR_history_buffer_valid(CDR_history_buffer_size-2 downto 0) & '1';
              for iBit in 0 to 4 loop            
                CDR_history_buffer(iBit) <= CDR_history_buffer(iBit)(CDR_history_buffer_size-2 downto 0) & local_edges(iBit);
                CDR_history_buffer(iBit+5) <= CDR_history_buffer(iBit+5)(CDR_history_buffer_size-2 downto 0) & sreg(iBit);                
              end loop;  -- iBit
            end if;

            -- stop monitoring if things go wrong
            if or_reduce(lost_alignment_buffer) = '1' then            
              --reset the FIFO for filling
              history_state <= history_state_POST_MONITOR;
              --reset the post monitor sample shift register to zero
              lost_alignment_shift_register <= (others => '0');
            end if;

          when history_state_POST_MONITOR =>
            ---------------------------------------------------------------------------
            -- history_state_POST_MONITOR
            -- capture another CDR_hisotyr_buffer_size/2 samples
            history_state <= history_state_POST_MONITOR;

            -- move on when done capturing post-samples
            if lost_alignment_shift_register(31) = '1' then            
              history_state <= history_state_PRE_RECORD;

            elsif  enable_8b10b_bit = '1' then
              -- shift in '1's untill the fall out the top of the shift register
              lost_alignment_shift_register <= lost_alignment_shift_register(30 downto 0) & '1';

              CDR_history_buffer_valid <= CDR_history_buffer_valid(CDR_history_buffer_size-2 downto 0) & '1';
              for iBit in 0 to 4 loop            
                CDR_history_buffer(iBit) <= CDR_history_buffer(iBit)(CDR_history_buffer_size-2 downto 0) & local_edges(iBit);
                CDR_history_buffer(iBit+5) <= CDR_history_buffer(iBit+5)(CDR_history_buffer_size-2 downto 0) & sreg(iBit);
              end loop;  -- iBit
            end if;
            
            
          when history_state_PRE_RECORD =>
            ---------------------------------------------------------------------------
            -- history_state_PRE_RECORD
            -- reset the output FIFO, go to reset if we have no data, beging processing
            -- procedure if we do
            history_state <= history_state_PRE_RECORD;
            -- check if FIFO has reset
            if CDR_history_buffer_has_data = '1' then              
              history_state <= history_state_REMOVE_INVALID;
            else
              -- no data to save, reset
              history_state <= history_state_RESET;
            end if;

          when history_state_REMOVE_INVALID =>
            ---------------------------------------------------------------------------
            -- history_state_REMOVE_INVALID
            -- go through the CDR history and remove invalid data
            -- when we find valid data, go to the next state
            history_state <= history_state_REMOVE_INVALID;
            if CDR_history_buffer_valid(CDR_history_buffer_size-1) = '0' then
              --Move through data in the shift registers
              CDR_history_buffer_valid <= CDR_history_buffer_valid(CDR_history_buffer_size-2 downto 0) & '0';
              for iBit in 0 to 9 loop
                CDR_history_buffer(iBit) <= CDR_history_buffer(iBit)(CDR_history_buffer_size-2 downto 0) & '0';            
              end loop;  -- iBit
            else
              --read to fill the FIFO
              history_state <= history_state_FILL_FIFO;
            end if;
            


            
          when history_state_FILL_FIFO =>
            ---------------------------------------------------------------------------
            -- history_state_PRE_RECORD
            if full_CDR_history_FIFO = '0' then
              --Move through data in the shift registers
              CDR_history_buffer_valid <= CDR_history_buffer_valid(CDR_history_buffer_size-2 downto 0) & '0';
              for iBit in 0 to 9 loop
                CDR_history_buffer(iBit) <= CDR_history_buffer(iBit)(CDR_history_buffer_size-2 downto 0) & '0';
              end loop;  -- iBit

              -- save data to the FIFO
              if CDR_history_buffer_valid(CDR_history_buffer_size-1) = '1' then
                wr_CDR_history_FIFO <= '1';
                for iBit in 0 to 9 loop
                  din_CDR_history_FIFO(iBit) <= CDR_history_buffer(iBit)(CDR_history_buffer_size-1);
                end loop;  -- iBit
              else
                history_state <= history_state_IDLE;
              end if;
            else
              -- also end if our fifo is full (this happens if the number of
              -- history saves is equal to the size of the FIFO
              history_state <= history_state_IDLE;
            end if;       
          when history_state_IDLE =>
            ---------------------------------------------------------------------------
            -- history_state_IDLE
            -- we have filled the FIFO so the user can read it out
            -- we do nothing until we are reset
            history_state <= history_state_IDLE;
            CDR_history.ready <= '1';
          when history_state_RESET =>
            ---------------------------------------------------------------------------
            -- history_state_RESET
            CDR_history_buffer_has_data <= '0';
            CDR_history_buffer <= (others => (others => '0'));
            history_state <= history_state_MONITOR;
          when others => history_state <= history_state_RESET;
        end case;
      end if;
    end if;
  end process CDR_record_history_proc;

  CDR_History_FIFO_1: entity work.CDR_History_FIFO
    port map (
		srst    => reset_CDR_History_FIFO,
      clk => clk,
      din    => din_CDR_history_FIFO,
      wr_en  => wr_CDR_history_FIFO,
      rd_en  => CDR_history_control.rd,
      dout   => dout_CDR_history_FIFO,
      full   => full_CDR_history_FIFO,
      empty  => CDR_history.empty,
      valid  => CDR_history.valid);
  CDR_history.edges <= dout_CDR_history_FIFO(4 downto 0);
  CDR_history.samples <= dout_CDR_history_FIFO(9 downto 5);
  
end architecture arch;
