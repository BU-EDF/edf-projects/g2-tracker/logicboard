-------------------------------------------------------------------------------
-- g-2 tracker logic board TDC CDR
-- Dan Gastler
-- Process TDC stream and provide a spill FIFO
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

use work.TDC_package.all;
use work.Addressed_FIFO_package.all;
use work.TDC_CDR_Debugging.all;

entity TDC_CDR_FIFO is

  port (
    clk125 : in std_logic;
    reset  : in std_logic;
    enable : in std_logic;
    run_enable : in std_logic;
    
    serial : in std_logic;              -- TDC data in

    -- Stream info
    locked : out std_logic;             -- CDR locked
    spy    : out std_logic;

    -- Spill header
    header_valid : out std_logic;  -- signal stating that the current header
    -- data outputs are valid
    header_info  : out TDC_header_info_t;

    --FIFO parameters
    data        : out std_logic_vector(33 downto 0);  -- TDC data stream
    data_valid  : out std_logic;
    full        : out std_logic;
    empty       : out std_logic;
    read_strobe : in  std_logic;

    --debugging
    debug_monitor : out TDC_debug_monitor_t;
    debug_control : in  TDC_debug_control_t;

    --counters
    counters : out TDC_counters_t;
    counter_control : in TDC_counter_control_t;
    --CDR history
    CDR_history   : out CDR_history_t;
    CDR_history_control : in CDR_history_control_t

    );
end TDC_CDR_FIFO;
architecture arch of TDC_CDR_FIFO is

  -----------------------------------------------------------------------------
  -- components
  -----------------------------------------------------------------------------

  -- clock and data recovery for 8b10b
  component datarec
    generic (
      W : integer := 12;
      M : integer := 5);
    port (
      clk              : in  std_logic;
      rst_n            : in  std_logic;
      ser              : in  std_logic;
      dv               : out std_logic;
      edges            : out std_logic_vector(4 downto 0);
      histo            : out std_logic_vector(4 downto 0);
      multi_valid      : out std_logic;
      override_setting : in  std_logic_vector(2 downto 0);
      lost_alignment   : out std_logic_vector(M-1 downto 0);
      phase_switch : out std_logic;
      d                : out std_logic;
      CDR_history      : out CDR_history_t;
      CDR_history_control : in CDR_history_control_t);
  end component;

  -- deserialize the 8b10b stream
  component bytesynch is
    generic (
      M : integer);
    port (
      clk      : in  std_logic;
      reset    : in  std_logic;
      d        : in  std_logic;
      dv       : in  std_logic;
      K        : out std_logic;
      in_sync  : out std_logic;
      dav_out  : out std_logic;
      bad_sync : out std_logic;
      q        : out std_logic_vector (M-1 downto 0));
  end component bytesynch;

  -- decode the 8b10b
  component decode_8b10b_top
    generic (
      C_HAS_CODE_ERR : integer;
      C_HAS_DISP_ERR : integer;
      C_HAS_CE       : integer;
      C_HAS_ND       : integer;
      C_HAS_SINIT    : integer);
    port (
      CLK        : in  std_logic                    := '0';
      DIN        : in  std_logic_vector(9 downto 0) := (others => '0');
      DOUT       : out std_logic_vector(7 downto 0);
      KOUT       : out std_logic;
      CE         : in  std_logic                    := '0';
      CE_B       : in  std_logic                    := '0';
      CLK_B      : in  std_logic                    := '0';
      DIN_B      : in  std_logic_vector(9 downto 0) := (others => '0');
      DISP_IN    : in  std_logic                    := '0';
      DISP_IN_B  : in  std_logic                    := '0';
      SINIT      : in  std_logic                    := '0';
      SINIT_B    : in  std_logic                    := '0';
      CODE_ERR   : out std_logic                    := '0';                     
      CODE_ERR_B : out std_logic                    := '0';
      DISP_ERR   : out std_logic                    := '0';
      DISP_ERR_B : out std_logic                    := '0';
      DOUT_B     : out std_logic_vector(7 downto 0);
      KOUT_B     : out std_logic;
      ND         : out std_logic                    := '0';
      ND_B       : out std_logic                    := '0';
      RUN_DISP   : out std_logic;
      RUN_DISP_B : out std_logic;
      SYM_DISP   : out std_logic_vector(1 downto 0);
      SYM_DISP_B : out std_logic_vector(1 downto 0));
  end component;

  component Addressed_FIFO is
    port (
      clk             : in  std_logic;
      reset           : in  std_logic;
      fifo_full       : out std_logic;
      fifo_in         : in  std_logic_vector(33 downto 0);
      fifo_wr         : in std_logic;
      fifo_empty      : out std_logic;
      fifo_out        : out std_logic_vector(33 downto 0);
      fifo_out_valid  : out std_logic;
      fifo_rd         : in  std_logic;
      data            : out data_array;
      data_valid      : out std_logic;
      buffer_overflow : out std_logic);
  end component Addressed_FIFO;
  
  component TDC_Event_Size_FIFO is
    port (
      clk   : in  std_logic;
      rst   : in  std_logic;
      din   : in  std_logic_vector(15 downto 0);
      wr_en : in  std_logic;
      rd_en : in  std_logic;
      dout  : out std_logic_vector(15 downto 0);
      full  : out std_logic;
      empty : out std_logic;
      valid : out std_logic);
  end component TDC_Event_Size_FIFO;

  component counter is
    generic (
          roll_over   : std_logic := '1';
          end_value   : std_logic_vector := x"FFFFFFFF";
          start_value : std_logic_vector := x"00000000";
          DATA_WIDTH  : integer   := 32);
    port (
      clk    : in  std_logic;
      reset  : in  std_logic;
      enable : in  std_logic;
      event  : in  std_logic;
      count  : out unsigned(DATA_WIDTH-1 downto 0);
      at_max : out std_logic);
  end component counter;

  -----------------------------------------------------------------------------
  -- constants
  -----------------------------------------------------------------------------
  constant IDLE_CHAR           : std_logic_vector(8 downto 0) := '1'&x"BC";
  constant START_OF_SPILL_CHAR : std_logic_vector(8 downto 0) := '1'&x"3C";

  constant LOCK_TIMEOUT     : unsigned(15 downto 0)        := x"4000";
  constant LOCK_TIME_START  : unsigned(15 downto 0)        := x"0000";

  constant DECODE_FIND_LOCK     : std_logic_vector(2 downto 0) := "001";
  constant DECODE_LOCKED        : std_logic_vector(2 downto 0) := "010";
  constant DECODE_PROCESS_SPILL : std_logic_vector(2 downto 0) := "100"; 
  -----------------------------------------------------------------------------
  -- signals
  -----------------------------------------------------------------------------
  signal local_reset_buffer : std_logic_vector(5 downto 0) := (others => '1');
  signal local_reset        : std_logic                    := '1';
  signal reset_n            : std_logic                    := '0';
  signal lost_lock_counter  : unsigned(15 downto 0)        := LOCK_TIME_START;
  signal lost_lock_reset    : std_logic                    := '0';

  signal multi_valid : std_logic := '0';
  
  -- serial stream
  signal recovered_serial       : std_logic                    := '0';
  signal recovered_serial_valid : std_logic                    := '0';
  signal recovered_serial_sync  : std_logic                    := '0';
  signal recovered_in_sync      : std_logic                    := '0';
  -- 10b words
  signal recovered_10b_valid    : std_logic                    := '0';
  signal recovered_10b_data     : std_logic_vector(9 downto 0) := "00"&x"00";

  -- 8b words
  signal locked_local             : std_logic                    := '0';
  signal recovered_8b_data        : std_logic_vector(8 downto 0) := '0'&x"00";
  signal recovered_8b_valid       : std_logic                    := '0';
  signal invalid_8b10b_conversion : std_logic                    := '0';
  signal data_disparity_invalid   : std_logic                    := '0';
  signal misaligned_idle_char     : std_logic                    := '0';
  signal lost_alignment : std_logic := '0';
  signal lost_alignment_buffer : std_logic_vector(4 downto 0) := "00000";
  
  -- 32 + 2 bit data words
  signal building_word                : std_logic_vector(33 downto 0) := "00"&x"00000000";
  signal decode_state                 : std_logic_vector(2 downto 0) := DECODE_FIND_LOCK;  -- 
  signal spill_overflow               : std_logic                     := '0';
  signal word_byte                    : integer range 0 to 3          := 0;
  signal current_word                 : std_logic_vector(33 downto 0) := "00"&x"00000000";
  signal expected_tdc_size            : std_logic_vector(14 downto 0) := "000" & x"000";

  
  -- fifo interface
  signal word_wr           : std_logic                     := '0';
  signal spill_rd          : std_logic                     := '0';
  signal spill_out         : std_logic_vector(33 downto 0) := "00"&x"00000000";
  signal spill_almost_full : std_logic                     := '0';
  signal spill_empty       : std_logic                     := '1';
  signal data_valid_buf    : std_logic                     := '0';
  signal spill_full        : std_logic                     := '0';

  signal header_data : data_array := (others => "11"&x"DEADBEEF");
  signal header_data_valid : std_logic := '0';
  signal unread_trailer_count : std_logic_vector(3 downto 0) := x"0";

  signal wait_for_next_event  : std_logic := '0';
  
  signal tdc_fifo_rd              : std_logic                     := '0';
  signal tdc_size_wr              : std_logic                     := '0';
  signal tdc_size_out             : std_logic_vector(15 downto 0) := x"0000";
  signal tdc_size_fifo_full       : std_logic                     := '0';
  signal tdc_size_fifo_empty      : std_logic                     := '1';
  signal tdc_size                 : unsigned(15 downto 0)         := x"0000";
  signal tdc_size_counter         : unsigned(14 downto 0)         := (others => '0');
  signal tdc_size_max             : unsigned(14 downto 0)         := "000"&x"800";
  signal tdc_size_valid           : std_logic                     := '0';
  signal empty_local : std_logic := '0';
  
  signal data_buffer : std_logic_vector(33 downto 0) := "11"&x"DEADBEEF";
  signal data_valid_buffer : std_logic := '0';
  
  -- counters
  signal bad_char_event           : std_logic             := '0';
  signal d_char_event             : std_logic             := '0';
  signal k_char_event : std_logic := '0';
  signal k_char_BC_event : std_logic := '0';
  signal k_char_3c_event : std_logic := '0';
  signal k_char_in_data_event : std_logic := '0';
  signal stream_start : std_logic := '0';
  signal stream_end : std_logic := '0';

begin  -- arch

  -----------------------------------------------------------------------------
  -- Reset manager
  -----------------------------------------------------------------------------
  stream_sync_monitor : process (clk125, reset) is
  begin  -- process stream_sync_monitor
    if reset = '1' then                 -- asynchronous reset (active high)
      local_reset_buffer <= (others => '1');
      lost_lock_counter  <= LOCK_TIME_START;
      lost_lock_reset    <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge

      -- monitor data valid for serial locked timeout
      if locked_local = '1' then
        lost_lock_counter <= lost_lock_counter + 1;
        if recovered_10b_valid = '1' then
          lost_lock_counter <= LOCK_TIME_START;
        end if;
      else
        lost_lock_counter <= LOCK_TIME_START;
      end if;

      -- monitor the system for error conditions and call reset if we find them.      
      lost_lock_reset <= '0';
      if misaligned_idle_char = '1' then
        local_reset_buffer <= (others => '1');
      elsif lost_lock_counter = LOCK_TIMEOUT then
        lost_lock_reset    <= '1';
        local_reset_buffer <= (others => '1');
      end if;

    end if;
  end process stream_sync_monitor;
  local_reset <= local_reset_buffer(0);
  reset_n     <= not reset;

  -----------------------------------------------------------------------------
  -- data and clock recovery
  -----------------------------------------------------------------------------  
  datarec_1 : datarec
    port map (
      clk              => clk125,
      rst_n            => '1',
      ser              => serial,
      dv               => recovered_serial_valid,
      edges            => debug_monitor.cdr_edges,
      histo            => debug_monitor.cdr_histogram,
      multi_valid      => multi_valid,
      override_setting => debug_control.cdr_override_setting,
      lost_alignment   => lost_alignment_buffer,
      phase_switch     => debug_monitor.cdr_phase_switch,
      d                => recovered_serial,
      CDR_history      => CDR_history,
      CDR_history_control => CDR_history_control
  );    

  debug_monitor.lost_alignment <= lost_alignment_buffer ;
  spy_proc : process (recovered_serial_valid, recovered_serial) is
  begin  -- process spy_proc
    if recovered_serial_valid = '1' then
      spy <= recovered_serial;
    end if;
  end process spy_proc;
--  spy <= recovered_serial;

  -----------------------------------------------------------------------------
  -- deserializer
  -----------------------------------------------------------------------------
  bytesynch_1 : bytesynch
    generic map (
      M => 10)
    port map (
      clk      => clk125,
      reset    => '0',
      d        => recovered_serial,
      dv       => recovered_serial_valid,
      K        => open,
      in_sync  => recovered_in_sync,    -- we se a K.28.5
      dav_out  => recovered_10b_valid,
      bad_sync => misaligned_idle_char,
      q        => recovered_10b_data);

  -- debug Monitor output of the 10b_char
  process (clk125) is
  begin  -- process
    if clk125'event and clk125 = '1' then  -- rising clock edge
      if recovered_10b_valid = '1' then
        debug_monitor.current_10b_char <= '1'&recovered_10b_data;
      else
        debug_monitor.current_10b_char <= "00000000000";
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- decoder
  -----------------------------------------------------------------------------

  decode_8b10b_top_1 : decode_8b10b_top
    generic map (
      C_HAS_CE       => 1,
      C_HAS_CODE_ERR => 1,
      C_HAS_DISP_ERR => 1,
      C_HAS_ND       => 1,
      C_HAS_SINIT    => 1)
    port map (
      CLK      => clk125,
      CE       => recovered_10b_valid,
      SINIT    => reset,
      DIN      => recovered_10b_data,
      DOUT     => recovered_8b_data(7 downto 0),  
      KOUT     => recovered_8b_data(8),           
      ND       => recovered_8b_valid,             
      CODE_ERR => invalid_8b10b_conversion,
      DISP_ERR => data_disparity_invalid);

  --debug monitor of the 8b char
  process (clk125) is
  begin  -- process
    if clk125'event and clk125 = '1' then  -- rising clock edge
      if recovered_8b_valid = '1' then
        debug_monitor.current_8b_char <= '1'&recovered_8b_data;
      else
        debug_monitor.current_8b_char <= "0000000000";
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- 8b char level stuff
  -- look for idle lock-on and start of spill
  -- pipe-line the data and give the other processes a heads up about new data
  -----------------------------------------------------------------------------
  decoder_processor : process (clk125, reset)
  begin  -- process decoder_processor
    if reset = '1' then           -- asynchronous reset (active high)
      locked_local                   <= '0';
      header_info.fatal_error        <= '0';

      decode_state <= DECODE_FIND_LOCK;            
      
      word_wr <= '0';
      k_char_in_data_event <= '0';
      stream_end <= '0';
      stream_start <= '0';
      k_char_in_data_event <= '0';
      k_char_3c_event <= '0';
      k_char_Bc_event <= '0';
      tdc_size_wr <= '0';

    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      word_wr <= '0';
      k_char_in_data_event <= '0';
      stream_end <= '0';
      stream_start <= '0';
      k_char_in_data_event <= '0';
      k_char_3c_event <= '0';
      k_char_bc_event <= '0';
      tdc_size_wr <= '0';
      
      if recovered_8b_valid = '1' then

        --count the number of idle chars
        if recovered_8b_data = IDLE_CHAR  then
          k_char_bc_event <= '1';
        end if;
        
        case decode_state is
          ---------------------------------------------------------------------
          ---------------------------------------------------------------------
          when DECODE_FIND_LOCK =>
            -- look at the most recent word for IDLE char lockon
            if ( (invalid_8b10b_conversion = '1') or
                 (data_disparity_invalid   = '1') ) then
              decode_state <= DECODE_FIND_LOCK;
              locked_local <= '0';
            elsif recovered_8b_data = IDLE_CHAR then
              decode_state <= DECODE_LOCKED;
              locked_local <= '1';
            end if;
          ---------------------------------------------------------------------
          ---------------------------------------------------------------------
          when DECODE_LOCKED =>

            
            -- look for start of spill
            if ( (recovered_8b_data = START_OF_SPILL_CHAR) and
                 (invalid_8b10b_conversion = '0')          and
                 (data_disparity_invalid = '0') ) then
              -- process new spill (if enabled)
              if run_enable = '1' then
                stream_start <= '1';
                k_char_3c_event <= '1';
                decode_state <= DECODE_PROCESS_SPILL;
                -- start saving data
                tdc_size_counter <= "000"&x"000";
                building_word(33 downto 32) <= "10";
                building_word(31 downto 8)  <= x"000000";
                building_word(7 downto 0) <= recovered_8b_data(7 downto 0);
                word_byte <= 1;
              end if;
            elsif ((recovered_8b_data /= IDLE_CHAR) or
                   (invalid_8b10b_conversion = '1') or
                   (data_disparity_invalid = '1') ) then
              -- Go back to DECODE_FIND_LOCK if we get a character other
              -- than IDLE or START_OF_SPILL
              locked_local <= '0';
              decode_state <= DECODE_FIND_LOCK;
            end if;
          ---------------------------------------------------------------------
          ---------------------------------------------------------------------
          when DECODE_PROCESS_SPILL =>
            if ( (recovered_8b_data = IDLE_CHAR) or
                 (recovered_8b_data(8) = '1' ) or
                 (spill_full = '1') or
                 (tdc_size_fifo_full = '1') or
                 (TDC_size_counter > tdc_size_max) or
                 (invalid_8b10b_conversion = '1') or
                 (data_disparity_invalid = '1') )
                 then

              --------------------
              -- end of spill, finish up and go back to waiting
              stream_end <= '1';
              decode_state <= DECODE_LOCKED;            
              -- write the old word to the fifo
              current_word(31 downto  0) <= building_word(31 downto 0);
              current_word(33 downto 32) <= "01";
              word_wr <= '1';
              tdc_size <= '0' & tdc_size_counter;
              tdc_size_wr <= '1';
              
              --------------------
              -- error conditions (go back to finding lock)
              -- unexpected control character.. end the spill, flag
              if recovered_8b_data /= IDLE_CHAR and recovered_8b_data(8) = '1' then                
                k_char_in_data_event <= '1';
                locked_local <= '0';
                decode_state <= DECODE_FIND_LOCK;
              end if;
              -- data fifo full
              if spill_full = '1' then
                header_info.fatal_error <= '1';
                decode_state <= DECODE_FIND_LOCK;
              end if;
              -- max size
              if TDC_size_counter > tdc_size_max then
                spill_overflow <= '1';
                tdc_size(15)   <= '1';
                decode_state <= DECODE_FIND_LOCK;
              end if;

              if (invalid_8b10b_conversion = '1') or
                 (data_disparity_invalid = '1') then
                decode_state <= DECODE_FIND_LOCK;
              end if;
              
            else
              case word_byte is
                when 0      =>
                  -- write the old word to the fifo
                  current_word <= building_word;
                  word_wr <= '1';
 
                  -- build new word
                  building_word(33 downto 32) <= "00";
                  building_word(31 downto 8)  <= x"000000";
                  building_word(7 downto 0) <= recovered_8b_data(7 downto 0);
                  word_byte <= 1;
                when 1      =>
                  building_word(15 downto 8)  <= recovered_8b_data(7 downto 0);
                  word_byte <= 2;
                when 2      =>
                  building_word(23 downto 16) <= recovered_8b_data(7 downto 0);
                  word_byte <= 3;
                when 3      =>
                  building_word(31 downto 24) <= recovered_8b_data(7 downto 0);
                  word_byte           <= 0; -- wrap around our counter
                                                      
                  -- keep track of the size of our event
                  if TDC_size_counter /= "111"&x"111" then
                    tdc_size_counter <= tdc_size_counter + 1;
                  end if;
                  
                when others => null;
              end case;
            end if;
          when others =>
            locked_local <= '0';
            decode_state <= DECODE_FIND_LOCK;
        end case;
      end if;
    end if;
  end process;
  locked <= locked_local;
  
  -----------------------------------------------------------------------------
  -- FIFO of spill data
  -----------------------------------------------------------------------------
  Addressed_FIFO_1: Addressed_FIFO
    port map (
      clk             => clk125,
      reset           => reset,
      fifo_full       => spill_full,
      fifo_in         => current_word,
      fifo_wr         => word_wr,
      fifo_empty      => empty_local,
      fifo_out        => data_buffer,
      fifo_out_valid  => data_valid_buffer,
      fifo_rd         => read_strobe,
      data            => header_data,
      data_valid      => header_data_valid,
      buffer_overflow => open);

  full       <= spill_full;
  data       <= data_buffer;
  data_valid <= data_valid_buffer;
  empty      <= empty_local;
  -----------------------------------------------------------------------------
  -- Compute the true (IDLE -> IDLE) event size of the last event
  -- Store the size in a 16 bit number using the MSB as an error bit
  -- (overflow is an error)
  -----------------------------------------------------------------------------  

  
  TDC_Event_Size_FIFO_1 : TDC_Event_Size_FIFO
    port map (
      clk   => clk125,
      rst   => reset,
      din   => std_logic_vector(tdc_size),
      wr_en => tdc_size_wr,
      rd_en => tdc_fifo_rd,
      dout  => tdc_size_out,
      full  => tdc_size_fifo_full,
      empty => tdc_size_fifo_empty,
      valid => tdc_size_valid);
 

  -----------------------------------------------------------------------------
  -- External interface
  -----------------------------------------------------------------------------
  ------------------------------------------------------------------------
  debug_monitor.addr_fifo_header_valid <= header_data_valid;
  debug_monitor.tdc_size_valid         <= tdc_size_valid;
  debug_monitor.tdc_size_fifo_empty    <= tdc_size_fifo_empty;
  present_spill : process (clk125, reset) is
  begin  -- process present_spill
    if reset = '1' then           -- asynchronous reset (active high)
      tdc_fifo_rd         <= '0';
      wait_for_next_event <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge

      --per clock signal reset
      tdc_fifo_rd <= '0';

      if (wait_for_next_event = '0' and -- waiting for a header
          header_data_valid = '1'    and --header_data
          tdc_size_fifo_empty = '0' and tdc_size_valid = '1') then
        
        wait_for_next_event <= '1'; -- make sure we read the event out the fifo
                                    -- before we present a new event
        --update data for the event builder to use to pre-process the event
        header_info.spill_number <= header_data(9)(23 downto 0);
        header_info.spill_time <= header_data(10)(31 downto 0) & header_data(11)(11 downto 0);
        header_info.size <= unsigned(tdc_size_out(14 downto 0));
        header_info.size_error <= tdc_size_out(15);
        header_info.debug_expected_size <= unsigned(header_data(8)(30 downto 16));
        header_valid <= '1';
        -- we just got some data from the spill size fifo, so do a read on that
        -- fifo
        tdc_fifo_rd <= '1';
      elsif wait_for_next_event = '1' then
        -- if we start reading this event, we can go back to looking for a
        -- new event.
        if ( header_data_valid = '0' or
            (empty_local = '1'))then
          wait_for_next_event <= '0';
          header_valid <= '0';
        end if;         
      end if;
    end if;
  end process present_spill;
      









  -----------------------------------------------------------------------------
  -- Counters
  -----------------------------------------------------------------------------

  ------------------------------------------------------------------------
  -- clock tick counter (for timing things)
  time_counter : counter
    generic map (
      roll_over => '0')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_time,
      enable => counter_control.enable_time,
      event  => '1',
      count  => counters.time_count,
      at_max => open);

  ------------------------------------------------------------------------
  -- lock timeout counter
  lock_timeout_counter : counter
    generic map (
      roll_over => '0')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_lock_timeouts,
      enable => counter_control.enable_lock_timeouts,
      event  => lost_lock_reset,
      count  => counters.lock_timeouts,
      at_max => open);
  ------------------------------------------------------------------------
  -- misaligned sync counter
  misaligned_sync_counter : counter
    generic map (
      roll_over => '0')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_misaligned_syncs,
      enable => counter_control.enable_misaligned_syncs,
      event  => misaligned_idle_char,
      count  => counters.misaligned_syncs,
      at_max => open);
  ------------------------------------------------------------------------
  -- bad char counter
  bad_char_counter : counter
    generic map (
      roll_over => '0')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_invalid_chars,
      enable => counter_control.enable_invalid_chars,
      event  => invalid_8b10b_conversion,
      count  => counters.invalid_chars,
      at_max => open);
  ------------------------------------------------------------------------
  -- size counters  
  malformed_counter : counter
    generic map (
      roll_over => '0')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_malformed_spills,
      enable => counter_control.enable_malformed_spills,
      event  => spill_overflow,
      count  => counters.malformed_spills,
      at_max => open);
  ------------------------------------------------------------------------
  -- bad disparity
  bad_disparity_counter : counter
    generic map (
      roll_over => '0')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_bad_disparity,
      enable => counter_control.enable_bad_disparity,
      event  => data_disparity_invalid,
      count  => counters.bad_disparity,
      at_max => open);

  ------------------------------------------------------------------------
  --char counters
  k_char_event <= recovered_8b_valid and recovered_8b_data(8);
  k_char_counter : counter
    generic map (
      roll_over => '1')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_k_chars,
      enable => counter_control.enable_k_chars,
      event  => k_char_event,
      count  => counters.k_chars,
      at_max => open);

  k_bc_char_counter : counter
    generic map (
      roll_over => '1')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_k_chars_bC,
      enable => counter_control.enable_k_chars_bC,
      event  => k_char_bc_event,
      count  => counters.k_chars_bC,
      at_max => open);


  k_3c_char_counter : counter
    generic map (
      roll_over => '1')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_k_chars_3C,
      enable => counter_control.enable_k_chars_3C,
      event  => k_char_3c_event,
      count  => counters.k_chars_3C,
      at_max => open);
  
  k_char_in_data_counter : counter
    generic map (
      roll_over => '0')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_k_chars_in_data,
      enable => counter_control.enable_k_chars_in_data,
      event  => k_char_in_data_event,
      count  => counters.k_chars_in_data,
      at_max => open);

  d_char_event <= recovered_8b_valid and (not recovered_8b_data(8));
  d_chars_counter : counter
    generic map (
      roll_over => '1')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_d_chars,
      enable => counter_control.enable_d_chars,
      event  => d_char_event,
      count  => counters.d_chars,
      at_max => open);

  multi_valid_counter : counter
    generic map (
      roll_over => '0')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_multi_valids,
      enable => counter_control.enable_multi_valids,
      event  => multi_valid,
      count  => counters.multi_valids,
      at_max => open);

  lost_alignment <= or_reduce(lost_alignment_buffer);
  lost_alignment_counter : counter
    generic map (
      roll_over => '0')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_lost_alignment,
      enable => counter_control.enable_lost_alignment,
      event  => lost_alignment,
      count  => counters.lost_alignment,
      at_max => open);
  
  DECODE_FIND_LOCK_time_counter : counter
    generic map (
      roll_over => '1')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_DECODE_FIND_LOCK_time,
      enable => counter_control.enable_DECODE_FIND_LOCK_time,
      event  => decode_state(0),
      count  => counters.DECODE_FIND_LOCK_time,
      at_max => open);

  DECODE_LOCKED_time_counter : counter
    generic map (
      roll_over => '1')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_DECODE_LOCKED_time,
      enable => counter_control.enable_DECODE_LOCKED_time,
      event  => decode_state(1),
      count  => counters.DECODE_LOCKED_time,
      at_max => open);
  DECODE_PROCESS_SPILL_time_counter : counter
    generic map (
      roll_over => '1')
    port map (
      clk    => clk125,
      reset  => counter_control.reset_DECODE_PROCESS_SPILL_time,
      enable => counter_control.enable_DECODE_PROCESS_SPILL_time,
      event  => decode_state(2),
      count  => counters.DECODE_PROCESS_SPILL_time,
      at_max => open);


  
  stream_starts_counter : counter
    generic map (
      roll_over => '0',
      end_value => x"FFFF",
      start_value => x"0000",
      DATA_WIDTH       => 16)
    port map (
      clk    => clk125,
      reset  => counter_control.reset_stream_starts,
      enable => counter_control.enable_stream_starts,
      event  => stream_start,
      count  => counters.stream_starts,
      at_max => open);


  stream_ends_counter : counter
    generic map (
      roll_over => '0',
      end_value => x"FFFF",
      start_value => x"0000",
      DATA_WIDTH       => 16)
    port map (
      clk    => clk125,
      reset  => counter_control.reset_stream_ends,
      enable => counter_control.enable_stream_ends,
      event  => stream_end,
      count  => counters.stream_ends,
      at_max => open);

end arch;


