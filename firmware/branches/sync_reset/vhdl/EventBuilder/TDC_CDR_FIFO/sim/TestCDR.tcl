source vhdl/sim/TDC_spill_helper.tcl
restart



#------------------------------------------------------------
# main simulation
#------------------------------------------------------------
# set up clock
isim force add /datarec/clk 1 -value 0 -time 4 ns -repeat 8 ns 
isim force add /datarec/rst_n 1
isim force add /datarec/override_setting 110 -radix bin
#isim force add {/datarec/cdr_history_control.reset} 1 -radix bin
run 100 ns
set disp "-1"
#isim force add {/datarec/cdr_history_control.reset} 0 -radix bin

#send some idle chars to lock on to
set disp [send_8b10b_idle $disp /datarec/ser 25 ]
for {set i_delay 1} { $i_delay < 50 } {incr i_delay} {
    set disp [send_8b10b_idle $disp /datarec/ser 10 ]
    run 250 ps
}

for {set i_delay 1} { $i_delay < 50 } {incr i_delay} {
    isim force add {/datarec/cdr_history_control.rd} 1 -radix bin
    run 8ns
    isim force add {/datarec/cdr_history_control.rd} 0 -radix bin
    run 8ns
}
#source vhdl/EventBuilder/TDC_CDR_FIFO/sim/TestCDR.tcl







