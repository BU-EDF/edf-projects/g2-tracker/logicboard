source vhdl/sim/UART_helper.tcl
restart

isim force add {/uc/reg_data_in} DEADBEEF -radix hex
isim force add {/uc/reg_data_in_dv} 1

#turn off reset
isim force add {/uc/reset} 0

#set initial conditions for UART
isim force add {/uc/sc_rx} 1
#isim force add {/uc/reset} 0

#isim force add {/uc/tdc_sda} 1

#set clock on /uc/clk
isim force add {/uc/clk} 1 -radix bin -value 0 -radix bin -time 8 ns -repeat 16 ns 

#let everything start up
run 10 ms

#send tdc_write command
sendUART_str tdc_shell 125000 /uc/sc_rx;
#send space (0x20)
sendUART_hex 20 125000 /uc/sc_rx;
sendUART_str 0 125000 /uc/sc_rx;

#send CR (0x0D)
sendUART_hex 0D 125000 /uc/sc_rx;

run 1ms

#send test data
sendUART_str asdfasdfasdf 125000 /uc/sc_rx;


run 10 ms
