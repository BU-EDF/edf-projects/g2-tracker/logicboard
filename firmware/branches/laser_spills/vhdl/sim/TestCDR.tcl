source vhdl/sim/TDC_spill_helper.tcl
source vhdl/sim/UART_helper.tcl

restart


#set initial conditions for UART
isim force add {/top/sc_in} 1
#isim force add {/top/reset} 0

#isim force add {/top/tdc_sda} 1

#set clock on /top/clk
isim force add {/top/clk125_in} 1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns
#isim force add {/top/reset_daq_clocking} 0
#isim force add {/top/C5_Raw_stream} 1 -radix bin -value 0 -radix bin -time 50 ns -repeat 100 ns
isim force add {/top/clk40_ext} 1 -radix bin -value 0 -radix bin -time 12.5 ns -repeat 25 ns
isim force add {/top/lb_monitor.ext_clock_locked} 1




#------------------------------------------------------------
# main simulation
#------------------------------------------------------------
# set up clock

#isim force add /datarec/clk 1 -value 0 -time 4 ns -repeat 8 ns 
#isim force add /datarec/rst_n 1
#isim force add /datarec/override_setting 000 -radix bin
#isim force add {/datarec/cdr_history_control.reset} 1 -radix bin
run 100 ns
set disp "-1"
#isim force add {/datarec/cdr_history_control.reset} 0 -radix bin

#send some idle chars to lock on to
set disp [send_8b10b_idle $disp /top/tdc_serial(0) 25 ]
for {set i_delay 1} { $i_delay < 50 } {incr i_delay} {
    set disp [send_8b10b_idle $disp /top/tdc_serial(0) 10 ]
    run 250 ps
}

#send daq_reg_rd command
sendUART_str dr 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str b6 115200 /top/sc_in;

#send CR (0x0D)
sendUART_hex 0D 115200 /top/sc_in;


#send daq_reg_rd command
sendUART_str dw 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str b6 115200 /top/sc_in;

sendUART_hex 20 115200 /top/sc_in;
sendUART_str 2 115200 /top/sc_in;

#send CR (0x0D)
sendUART_hex 0D 115200 /top/sc_in;


#source vhdl/sim/TestCDR.tcl







