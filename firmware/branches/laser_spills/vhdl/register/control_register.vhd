------------------------------------------------------------------------------
-- control_register :
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed."+";
use ieee.std_logic_signed."=";

use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;


use work.LogicBoard_IO.all;
use work.EventBuilder_IO.all;
use work.C5_IO.all;
use work.Fake_spill_data.all;
use work.TestPulse_IO.all;

entity control_register is

  port (
    clk : in std_logic;
    reset : in std_logic;

    -- Logicboard firmware version
    Firmware_Version : in std_logic_vector(31 downto 0);
    
    -- interface to Logic board values
    LB_monitor : in LogicBoard_Monitor_t;
    LB_control : out LogicBoard_Control_t;

    -- interface to Event builder
    EB_monitor : in EventBuilder_Monitor_t;
    EB_control : out EventBuilder_Control_t;
    EB_reset   : out std_logic := '0';
    EB_clk     : in std_logic;

    -- interface to C5 (both DAQ and fake source)
    C5_control : out C5_cmd_Control_t;
    
    --interface to fake TDC generator
    control_fake_TDC  : out Fake_TDC_Control_t;
    monitor_fake_TDC : in Fake_TDC_Control_t;
    enable_fake_TDCs_out : out std_logic;
    enable_fake_TDCs_in : in std_logic;
    -- interface to test pulser
    control_testpulse : out TestPulseControl_t;
    
    --interface to uC
    clk_uC  : in std_logic;
    address : in std_logic_vector(7 downto 0);
    out_port_rd : in std_logic := '0';
    out_port : out std_logic_vector(31 downto 0);
    out_port_dv : out std_logic := '0';
    in_port : in std_logic_vector(31 downto 0);
    in_port_wr : in std_logic := '0'
    );


end entity control_register;

architecture arch of control_register is

  component register_fifo is
    port (
      wr_clk : IN  STD_LOGIC;
      rd_clk : IN  STD_LOGIC;
      din    : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
      wr_en  : IN  STD_LOGIC;
      rd_en  : IN  STD_LOGIC;
      dout   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      full   : OUT STD_LOGIC;
      empty  : OUT STD_LOGIC;
      valid  : OUT STD_LOGIC);
  end component register_fifo;
  
  component pacd is
    port (
      iPulseA : IN  std_logic;
      iClkA   : IN  std_logic;
      iRSTAn  : IN  std_logic;
      iClkB   : IN  std_logic;
      iRSTBn  : IN  std_logic;
      oPulseB : OUT std_logic);
  end component pacd;
  component pass_std_logic_vector is
    generic (
      DATA_WIDTH : integer;
      RESET_VAL  : std_logic_vector);
    port (
      clk_in   : in  std_logic;
      clk_out  : in  std_logic;
      reset    : in  std_logic;
      pass_in  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
      pass_out : out std_logic_vector(DATA_WIDTH-1 downto 0));
  end component pass_std_logic_vector;
-----------------------------------------------------------------------------
-- Address space
-----------------------------------------------------------------------------

  ----------------------------
  -- Logic board
  ----------------------------
  constant LB_CLK_STATUS              : std_logic_vector(7 downto 0) := x"00";
  -- 27 (r) ext clock locked change
  -- 26..24 (r) ext clock status(2..0) change
  -- 23 (r) ext clock valid change
  -- 22..20 (r) ext clock status(2..0)
  -- 19 (r) ext clock valid
  -- 18 (r) daq clock locked
  -- 17 (r) ext clock locked
  -- 16 (r) local clock locked
  -- 3 (a)   reset external clock monitoring
  -- 2 (r/w) reset daq clocking
  -- 1 (r/w) select daq source clock
  -- 0 (r/w) external clock reset
  constant LB_Firmware_Version : std_logic_vector(7 downto 0) := x"01";
  -- 31..24 (r) year (20YY)
  -- 23..16 (r) month (MM)
  -- 15..8  (r) day (DD)
  -- 7..0   (r) minor version  
  
  constant LB_TP_status           : std_logic_vector(7 downto 0) := x"05";
  -- 3..0 (r/w) ASDQ pulse driver enable
  -- 4 (r/w) Enable 1.6V
  -- 5 (r/w) Enable -1.6V

  constant LB_TP_pulses1          : std_logic_vector(7 downto 0) := x"06";
  -- 31 (r/w) send test pulses for all spills
  -- 27..20 (r/w) test pulse channel mask
  -- 19..0 (r/w) start time delay

  constant LB_TP_pulses2          : std_logic_vector(7 downto 0) := x"07";
  -- 27..20 (r/w) pulse count
  -- 19..0 (r/w) pulse to pulse count


  constant LB_SFP                 : std_logic_vector(7 downto 0) := x"09";
  -- 4 (r/w) select SFP output (8b10b or C5)
  -- 2 (r/w) disable SFP output
  -- 1 (r) los of signals
  -- 0 (r) SFP present

  constant LB_LED_GPIO            : std_logic_vector(7 downto 0) := x"0a";
  -- 19..16 (r/w) GPIO control
  -- 15..8 (r/w) GPIO mux sel (0 = GPIO ; 1,2,3,4 = TDC debug 0,1,2,3 ; 5 SFP debug ; others = GPIO)
  -- 3..0 (r/w) LED control

  
  ----------------------------
  -- Fake TDCs
  ----------------------------  
  constant FAKE_TDCS_ctrl      : std_logic_vector(7 downto 0) := x"10";
  -- 31 (a)
  -- 0 (r/w) switch to fake TDC stream
  constant FAKE_TDCS_spill_n   : std_logic_vector(7 downto 0) := x"11";
  -- 23..0 (r/w) fake spill event number
  constant FAKE_TDCS_spill_t1  : std_logic_vector(7 downto 0) := x"12";
  -- 31..0 (r/w) fake spill time 43 downto 12
  constant FAKE_TDCS_spill_t2  : std_logic_vector(7 downto 0) := x"13";
  -- 11..0 (r/w) fake spill time 11 downto 06  
  constant FAKE_TDCS_hit_count : std_logic_vector(7 downto 0) := x"14";    
  -- 11..0 (r/w) number of hits

  ----------------------------
  -- EB values
  ----------------------------
  constant EB_status              : std_logic_vector(7 downto 0) := x"20";
  -- 0 (a) restart Event builder
  -- 1 (r/w) enable run
  -- 2 (r/w) enable event builder counters 
  -- 15..8 (r) event builder state
  constant EB_started_spills      : std_logic_vector(7 downto 0) := x"21";
  -- 31..0 (r/a) new spills pulled from fifo
  constant EB_sent_spills         : std_logic_vector(7 downto 0) := x"22";
  -- 31..0 (r/a) spill trailers sent
  constant EB_debug_fifo          : std_logic_vector(7 downto 0) := x"23";
  -- 16 (a) read
  -- 11 (r) full
  -- 10 (r) empty
  -- 9 (r) data valid
  -- 8..0 (r) data
  constant EB_time_skew_max       : std_logic_vector(7 downto 0) := x"24";
  -- 31..0 (r/w) max timing difference between C5 and TDC timestamps
  constant EB_output_8bchars        : std_logic_vector(7 downto 0) := x"25";
  -- 31..0 (r/a) count of event chars sent (k +d)
  constant EB_output_words        : std_logic_vector(7 downto 0) := x"26";
  -- 31..0 (r/a) count of event words sent (k +d)
  constant EB_output_word_overflows : std_logic_vector(7 downto 0) := x"27";
  -- 31..0 (r/a) count of event words that were written when the fifo is full
  constant EB_output_chars_total    : std_logic_vector(7 downto 0) := x"28";
  -- 31..0 (r/a) count of all 8b10b chars sent

  constant EB_state_SEND_IDLE_Hi    : std_logic_vector(7 downto 0) := x"29";
  -- 31..0 (r/a) time in SEND\_IDLE state upper 32 bits of 64
  constant EB_state_SEND_IDLE_Lo    : std_logic_vector(7 downto 0) := x"2A";
  -- 31..0 (r/a) time in SEND\_IDLE state lower 32 bits of 64
  constant EB_state_CHECK_NEW_DATA  : std_logic_vector(7 downto 0) := x"2B";
  -- 31..0 (r/a) time in CHECK\_NEW\_DATA state
  constant EB_state_BUILD_HEADER    : std_logic_vector(7 downto 0) := x"2C";
  -- 31..0 (r/a) time in BUILD\_HEADER state
  constant EB_state_SEND_HEADER     : std_logic_vector(7 downto 0) := x"2D";
  -- 31..0 (r/a) time in BUILD\_SEND state
  constant EB_state_SEND_TDC_DATA   : std_logic_vector(7 downto 0) := x"2E";
  -- 31..0 (r/a) time in SEND\_TDC\_DATA state
  constant EB_state_SEND_END        : std_logic_vector(7 downto 0) := x"2F";
  -- 31..0 (r/a) time in SEND\_END state
  constant EB_state_SEND_CRC        : std_logic_vector(7 downto 0) := x"A0";
  -- 31..0 (r/a) time in SEND\_CRC state
  constant EB_state_SEND_TRAILER    : std_logic_vector(7 downto 0) := x"A1";
  -- 31..0 (r/a) time in SEND\_TRAILER state
  constant EB_output_10bchars       : std_logic_vector(7 downto 0) := x"A2";
  -- 31..0 (r/a) count of event chars sent (k +d)
  constant EB_missed_10bchars        : std_logic_vector(7 downto 0) := x"A3";
  -- 31..0 (r/a) count of event chars sent (k +d)
  

  
  ----------------------------
  -- C5 values
  ----------------------------
  constant C5_status              : std_logic_vector(7 downto 0) := x"30";
  -- 7 (r) fifo full
  -- 6 (r) fifo empty
  -- 3 (r/w) TDC clock source

  constant C5_CMD                 : std_logic_vector(7 downto 0) := x"31";
  -- 8 (a) write C5
  -- 4..0 (r/w) kchar,data

  constant C5_CMD_COUNTER_0_1     : std_logic_vector(7 downto 0) := x"32";
  -- 31..16 (r) C5 CMD counter 1
  -- 15..0 (r) C5 CMD counter 0
  constant C5_CMD_COUNTER_2_3     : std_logic_vector(7 downto 0) := x"33";
  -- 31..16 (r) C5 CMD counter 3
  -- 15..0 (r) C5 CMD counter 2
  constant C5_CMD_COUNTER_4_5     : std_logic_vector(7 downto 0) := x"34";
  -- 31..16 (r) C5 CMD counter 5
  -- 15..0 (r) C5 CMD counter 4
  constant C5_CMD_COUNTER_6_7     : std_logic_vector(7 downto 0) := x"35";
  -- 31..16 (r) C5 CMD counter 7
  -- 15..0 (r) C5 CMD counter 6
  constant C5_CMD_COUNTER_8_9     : std_logic_vector(7 downto 0) := x"36";
  -- 31..16 (r) C5 CMD counter 9
  -- 15..0 (r) C5 CMD counter 8
  constant C5_CMD_COUNTER_10_11     : std_logic_vector(7 downto 0) := x"37";
  -- 31..16 (r) C5 CMD counter 11
  -- 15..0 (r) C5 CMD counter 10
  constant C5_CMD_COUNTER_12_13     : std_logic_vector(7 downto 0) := x"38";
  -- 31..16 (r) C5 CMD counter 13
  -- 15..0 (r) C5 CMD counter 12
  constant C5_CMD_COUNTER_14_15     : std_logic_vector(7 downto 0) := x"39";
  -- 31..16 (r) C5 CMD counter 15
  -- 15..0 (r) C5 CMD counter 14


  ----------------------------
  -- TDC values
  ----------------------------
  constant TDC_control            : std_logic_vector(7 downto 0) := x"40";  
  -- 23..20 (r) TDC had header
  -- 19..16 (r) TDC 8b10b locked on
  -- 15..12 (r) TDC fifo empty
  -- 11..8 (r) TDC fifo full
  -- 7..4 (r) nothing
  -- 3..0 (r/w) TDC enable mask

  
  constant TDC_0_FIFO_control     : std_logic_vector(7 downto 0) := x"50";
  -- 31 (a) fifo read (only valid when TDC is not enabled)
  -- 30 (r) fifo data valid
--  -- 28..24 (r) CDR edges
--  -- 20..16 (r) CDR histogram  
  -- 9 (r/w) enable counters
  -- 8 (a) reset counters
  -- 7 (r) addr FIFO header is valid
--  -- 6..4 (r/w) Override CDR setting (0 = auto)
  -- 3 (r) size valid
  -- 2 (r) size fifo empty
  -- 1..0 (r) current fifo word trailer + header bit
  constant TDC_0_FIFO_data        : std_logic_vector(7 downto 0) := x"51";   
  -- 31..0 (r) word from TDC fifo
  constant TDC_0_k_chars          : std_logic_vector(7 downto 0) := x"52";
  -- 31..0 (r/a) count (rolls over)
  constant TDC_0_k_chars_3C       : std_logic_vector(7 downto 0) := x"53";
  -- 31..0 (r/a) count
  constant TDC_0_k_chars_in_data  : std_logic_vector(7 downto 0) := x"54";
  -- 31..0 (r/a) count
  constant TDC_0_malformed_spills : std_logic_vector(7 downto 0) := x"55";
  -- 31..0 (r/a) count
  constant TDC_0_invalid_chars    : std_logic_vector(7 downto 0) := x"56";
  -- 31..0 (r/a) count
  constant TDC_0_misaligned_syncs : std_logic_vector(7 downto 0) := x"57"; 
  -- 31..0 (r/a) count
  constant TDC_0_d_chars          : std_logic_vector(7 downto 0) := x"58";
  -- 31..0 (r/a) count (rolls over)
--  constant TDC_0_chars            : std_logic_vector(7 downto 0) := x"59";
--  -- 25..16 (r) current 10b char
--  -- 8..0 (r) current 8b char
  constant TDC_0_debug_size       : std_logic_vector(7 downto 0) := x"5A";
  -- 30..16 (r) actual spill size
  -- 14..0 (r) expected spill size
  constant TDC_0_missing_tr_bits  : std_logic_vector(7 downto 0) := x"5B";
  -- 31..0 (r/a) count
  constant TDC_0_lock_timeouts    : std_logic_vector(7 downto 0) := x"5C";
  -- 31..0 (r/a) count
--  constant TDC_0_multi_valids     : std_logic_vector(7 downto 0) := x"5D";
--  -- 31..0 (r/a) count
  constant TDC_0_DECODE_FIND_LOCK_time  : std_logic_vector(7 downto 0) := x"5E";
  -- 31..0 (r/a) count
  constant TDC_0_stream_start_end : std_logic_vector(7 downto 0) := x"5F";
  -- 15..0 (r) start count
  -- 31..16 (r) end  count
  constant TDC_0_DECODE_LOCKED_time  : std_logic_vector(7 downto 0) := x"B0";
  -- 31..0 (r/a) count
  constant TDC_0_DECODE_PROCESS_SPILL_time  : std_logic_vector(7 downto 0) := x"B1";
  -- 31..0 (r/a) count
  constant TDC_0_k_chars_BC       : std_logic_vector(7 downto 0) := x"B2";
  -- 31..0 (r/a) count
  constant TDC_0_bad_disparity    : std_logic_vector(7 downto 0) := x"B3";
  -- 31..0 (r/a) count
  constant TDC_0_time             : std_logic_vector(7 downto 0) := x"B4";
  -- 31..0 (r/a) timer count
  constant TDC_0_lost_alignments  : std_logic_vector(7 downto 0) := x"B5";
  -- 31..0 (r/a) timer count
  constant TDC_0_CDR_history  : std_logic_vector(7 downto 0) := x"B6";
  -- 20..16 (r) samples sample for this bit from the 5 phases
  -- 12..8 (r) edges xor outputs for finding transitions for the 5 phases
  -- 7 (r) valid FIFO data valid
  -- 6 (r) ready read to be read out
  -- 2 (r) empty 
  -- 1 (a) rd
  -- 0 (a) reset

  
  constant TDC_1_FIFO_control     : std_logic_vector(7 downto 0) := x"60";
  -- 31 (a) fifo read (only valid when TDC is not enabled)
  -- 30 (r) fifo data valid
--  -- 28..24 (r) CDR edges
--  -- 20..16 (r) CDR histogram
  -- 9 (r/w) enable counters
  -- 8 (a) reset counters
  -- 7 (r) addr FIFO header is valid
--  -- 6..4 (r/w) Override CDR setting  (0 = auto)
  -- 3 (r) size valid
  -- 2 (r) size fifo empty
  -- 1..0 (r) current fifo word trailer + header bit
  constant TDC_1_FIFO_data        : std_logic_vector(7 downto 0) := x"61";   
  -- 31..0 (r) word from TDC fifo
  constant TDC_1_k_chars          : std_logic_vector(7 downto 0) := x"62";
  -- 31..0 (r/a) count (rolls over)
  constant TDC_1_k_chars_3C       : std_logic_vector(7 downto 0) := x"63";
  -- 31..0 (r/a) count
  constant TDC_1_k_chars_in_data  : std_logic_vector(7 downto 0) := x"64";
  -- 31..0 (r/a) count
  constant TDC_1_malformed_spills : std_logic_vector(7 downto 0) := x"65";
  -- 31..0 (r/a) count
  constant TDC_1_invalid_chars    : std_logic_vector(7 downto 0) := x"66";
  -- 31..0 (r/a) count
  constant TDC_1_misaligned_syncs : std_logic_vector(7 downto 0) := x"67"; 
  -- 31..0 (r/a) count
  constant TDC_1_d_chars          : std_logic_vector(7 downto 0) := x"68";
  -- 31..0 (r/a) count (rolls over)
--  constant TDC_1_chars            : std_logic_vector(7 downto 0) := x"69";
--  -- 25..16 (r) current 10b char
--  -- 8..0 (r) current 8b char
  constant TDC_1_debug_size       : std_logic_vector(7 downto 0) := x"6A";
  -- 30..16 (r) actual spill size
  -- 14..0 (r) expected spill size
  constant TDC_1_missing_tr_bits  : std_logic_vector(7 downto 0) := x"6B";
  -- 31..0 (r/a) count
  constant TDC_1_lock_timeouts    : std_logic_vector(7 downto 0) := x"6C";
  -- 31..0 (r/a) count
--  constant TDC_1_multi_valids     : std_logic_vector(7 downto 0) := x"6D";
--  -- 31..0 (r/a) count
  constant TDC_1_DECODE_FIND_LOCK_time  : std_logic_vector(7 downto 0) := x"6E";
  -- 31..0 (r/a) count
  constant TDC_1_stream_start_end : std_logic_vector(7 downto 0) := x"6F";
  -- 15..0 (r) start count
  -- 31..16 (r) end  count
  constant TDC_1_DECODE_LOCKED_time  : std_logic_vector(7 downto 0) := x"C0";
  -- 31..0 (r/a) count
  constant TDC_1_DECODE_PROCESS_SPILL_time  : std_logic_vector(7 downto 0) := x"C1";
  -- 31..0 (r/a) count
  constant TDC_1_k_chars_BC       : std_logic_vector(7 downto 0) := x"C2";
  -- 31..0 (r/a) count
  constant TDC_1_bad_disparity    : std_logic_vector(7 downto 0) := x"C3";
  -- 31..0 (r/a) count
  constant TDC_1_time             : std_logic_vector(7 downto 0) := x"C4";
  -- 31..0 (r/a) timer count
  constant TDC_1_lost_alignments  : std_logic_vector(7 downto 0) := x"C5";
  -- 31..0 (r/a) timer count
  constant TDC_1_CDR_history  : std_logic_vector(7 downto 0) := x"C6";
  -- 20..16 (r) samples sample for this bit from the 5 phases
  -- 12..8 (r) edges xor outputs for finding transitions for the 5 phases
  -- 7 (r) valid FIFO data valid
  -- 6 (r) ready read to be read out
  -- 2 (r) empty 
  -- 1 (a) rd
  -- 0 (a) reset

  
  constant TDC_2_FIFO_control     : std_logic_vector(7 downto 0) := x"70";
  -- 31 (a) fifo read (only valid when TDC is not enabled)
  -- 30 (r) fifo data valid
--  -- 28..24 (r) CDR edges
--  -- 20..16 (r) CDR histogram
  -- 9 (r/w) enable counters
  -- 8 (a) reset counters
  -- 7 (r) addr FIFO header is valid
--  -- 6..4 (r/w) Override CDR setting (0 = auto)
  -- 3 (r) size valid
  -- 2 (r) size fifo empty
  -- 1..0 (r) current fifo word trailer + header bit
  constant TDC_2_FIFO_data        : std_logic_vector(7 downto 0) := x"71";   
  -- 31..0 (r) word from TDC fifo
  constant TDC_2_k_chars          : std_logic_vector(7 downto 0) := x"72";
  -- 31..0 (r/a) count (rolls over)
  constant TDC_2_k_chars_3C       : std_logic_vector(7 downto 0) := x"73";
  -- 31..0 (r/a) count
  constant TDC_2_k_chars_in_data  : std_logic_vector(7 downto 0) := x"74";
  -- 31..0 (r/a) count
  constant TDC_2_malformed_spills : std_logic_vector(7 downto 0) := x"75";
  -- 31..0 (r/a) count
  constant TDC_2_invalid_chars    : std_logic_vector(7 downto 0) := x"76";
  -- 31..0 (r/a) count
  constant TDC_2_misaligned_syncs : std_logic_vector(7 downto 0) := x"77";
  -- 31..0 (r/a) count
  constant TDC_2_d_chars          : std_logic_vector(7 downto 0) := x"78";
  -- 31..0 (r/a) count (rolls over)
--  constant TDC_2_chars            : std_logic_vector(7 downto 0) := x"79";
--  -- 25..16 (r) current 10b char
--  -- 8..0 (r) current 8b char
  constant TDC_2_debug_size       : std_logic_vector(7 downto 0) := x"7A";
  -- 30..16 (r) actual spill size
  -- 14..0 (r) expected spill siz
  constant TDC_2_missing_tr_bits  : std_logic_vector(7 downto 0) := x"7B";
  -- 31..0 (r/a) count
  constant TDC_2_lock_timeouts    : std_logic_vector(7 downto 0) := x"7C";
  -- 31..0 (r/a) count
--  constant TDC_2_multi_valids     : std_logic_vector(7 downto 0) := x"7D";
--  -- 31..0 (r/a) count
  constant TDC_2_DECODE_FIND_LOCK_time  : std_logic_vector(7 downto 0) := x"7E";
  -- 31..0 (r/a) count
  constant TDC_2_stream_start_end : std_logic_vector(7 downto 0) := x"7F";
  -- 15..0 (r) start count
  -- 31..16 (r) end  count
  constant TDC_2_DECODE_LOCKED_time  : std_logic_vector(7 downto 0) := x"D0";
  -- 31..0 (r/a) count
  constant TDC_2_DECODE_PROCESS_SPILL_time  : std_logic_vector(7 downto 0) := x"D1";
  -- 31..0 (r/a) count
  constant TDC_2_k_chars_BC       : std_logic_vector(7 downto 0) := x"D2";
  -- 31..0 (r/a) count
  constant TDC_2_bad_disparity    : std_logic_vector(7 downto 0) := x"D3";
  -- 31..0 (r/a) count
  constant TDC_2_time             : std_logic_vector(7 downto 0) := x"D4";
  -- 31..0 (r/a) timer count
  constant TDC_2_lost_alignments  : std_logic_vector(7 downto 0) := x"D5";
  -- 31..0 (r/a) timer count
  constant TDC_2_CDR_history  : std_logic_vector(7 downto 0) := x"D6";
  -- 20..16 (r)  sample for this bit from the 5 phases
  -- 12..8 (r) edges xor outputs for finding transitions for the 5 phases
  -- 7 (r) valid FIFO data valid
  -- 6 (r) ready read to be read out
  -- 2 (r) empty 
  -- 1 (a) rd
  -- 0 (a) reset


  constant TDC_3_FIFO_control     : std_logic_vector(7 downto 0) := x"80";
  -- 31 (a) fifo read (only valid when TDC is not enabled)
  -- 30 (r) fifo data valid
--  -- 28..24 (r) CDR edges
--  -- 20..16 (r) CDR histogram
  -- 9 (r/w) enable counters
  -- 8 (a) reset counters
  -- 7 (r) addr FIFO header is valid
--  -- 6..4 (r/w) Override CDR setting (0 = auto)
  -- 3 (r) size valid
  -- 2 (r) size fifo empty
  -- 1..0 (r) current fifo word trailer + header bit
  constant TDC_3_FIFO_data        : std_logic_vector(7 downto 0) := x"81";   
  -- 31..0 (r) word from TDC fifo
  constant TDC_3_k_chars          : std_logic_vector(7 downto 0) := x"82";
  -- 31..0 (r/a) count (rolls over)
  constant TDC_3_k_chars_3C       : std_logic_vector(7 downto 0) := x"83";
  -- 31..0 (r/a) count
  constant TDC_3_k_chars_in_data  : std_logic_vector(7 downto 0) := x"84";
  -- 31..0 (r/a) count
  constant TDC_3_malformed_spills : std_logic_vector(7 downto 0) := x"85";
  -- 31..0 (r/a) count
  constant TDC_3_invalid_chars    : std_logic_vector(7 downto 0) := x"86";
  -- 31..0 (r/a) count
  constant TDC_3_misaligned_syncs : std_logic_vector(7 downto 0) := x"87";
  -- 31..0 (r/a) count
  constant TDC_3_d_chars          : std_logic_vector(7 downto 0) := x"88";
  -- 31..0 (r/a) count (rolls over)
--  constant TDC_3_chars            : std_logic_vector(7 downto 0) := x"89";
--  -- 25..16 (r) current 10b char
--  -- 8..0 (r) current 8b char
  constant TDC_3_debug_size       : std_logic_vector(7 downto 0) := x"8A";
  -- 30..16 (r) actual spill size
  -- 14..0 (r) expected spill size
  constant TDC_3_missing_tr_bits  : std_logic_vector(7 downto 0) := x"8B";
  -- 31..0 (r/a) count
  constant TDC_3_lock_timeouts    : std_logic_vector(7 downto 0) := x"8C";
  -- 31..0 (r/a) count
--  constant TDC_3_multi_valids     : std_logic_vector(7 downto 0) := x"8D";
--  -- 31..0 (r/a) count
  constant TDC_3_DECODE_FIND_LOCK_time  : std_logic_vector(7 downto 0) := x"8E";
  -- 31..0 (r/a) count
  constant TDC_3_stream_start_end : std_logic_vector(7 downto 0) := x"8F";
  -- 15..0 (r) start count
  -- 31..16 (r) end  count
  constant TDC_3_DECODE_LOCKED_time  : std_logic_vector(7 downto 0) := x"E0";
  -- 31..0 (r/a) count
  constant TDC_3_DECODE_PROCESS_SPILL_time  : std_logic_vector(7 downto 0) := x"E1";
  -- 31..0 (r/a) count
  constant TDC_3_k_chars_BC       : std_logic_vector(7 downto 0) := x"E2";
  -- 31..0 (r/a) count
  constant TDC_3_bad_disparity    : std_logic_vector(7 downto 0) := x"E3";
  -- 31..0 (r/a) count
  constant TDC_3_time             : std_logic_vector(7 downto 0) := x"E4";
  -- 31..0 (r/a) timer count
  constant TDC_3_lost_alignments  : std_logic_vector(7 downto 0) := x"E5";
  -- 31..0 (r/a) timer count
  constant TDC_3_CDR_history  : std_logic_vector(7 downto 0) := x"E6";
  -- 20..16 (r) samples sample for this bit from the 5 phases
  -- 12..8 (r) edges xor outputs for finding transitions for the 5 phases
  -- 7 (r) valid FIFO data valid
  -- 6 (r) ready read to be read out
  -- 2 (r) empty 
  -- 1 (a) rd
  -- 0 (a) reset

  

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------

  signal local_fake_TDC_send_spill : std_logic := '0';

  signal local_testpulse : TestPulseControl_t;

  signal local_LB_control :  LogicBoard_Control_t; 
  
  signal local_EB_control : EventBuilder_Control_t;
--  signal local_EB_control_LB_FIFO_rd : std_logic;
  signal local_EB_reset : std_logic := '0';
--  signal local_EB_control_TDC_FIFO_rd : std_logic_vector(3 downto 0) := x"0";

  signal local_C5_control : C5_cmd_Control_t;

  signal local_address : std_logic_vector(7 downto 0) := x"00";
  --fifo
  signal local_in_rd : std_logic := '0';
  signal local_in_empty : std_logic := '1';
  signal local_in_valid : std_logic := '0';
  signal local_in : std_logic_vector(31 downto 0) := x"00000000";

  signal local_out_rd : std_logic := '0';
  signal local_out_port_rd : std_logic := '0';
  signal local_out_empty : std_logic := '1';
  signal local_out_wr : std_logic := '0';
  signal local_out : std_logic_vector(31 downto 0) := x"00000000";

  signal local_TDC_CDR_history_reset : std_logic_vector(3 downto 0) := x"0";
  signal local_TDC_CDR_history_rd    : std_logic_vector(3 downto 0) := x"0";
  
  
begin  -- architecture arch
  
  control_testpulse <= local_testpulse;

  LB_control <= local_LB_control;
  EB_control <= local_EB_control;
  C5_control <= local_C5_control;


  pass_std_logic_vector_1: pass_std_logic_vector
    generic map (
      DATA_WIDTH => 8,
      RESET_VAL  => x"00")
    port map (
      clk_in   => clk_uC,
      clk_out  => clk,
      reset    => '0',
      pass_in  => address,
      pass_out => local_address);

  
  --input buffer
  local_rd_buffer: process (clk) is
  begin  -- process local_rd_buffer
    if clk'event and clk = '1' then  -- rising clock edge
      local_in_rd <= not local_in_empty;
    end if;
  end process local_rd_buffer;
  register_fifo_in: register_fifo
    port map (
      wr_clk => clk_uc,
      rd_clk => clk,
      din    => in_port,
      wr_en  => in_port_wr,
      rd_en  => local_in_rd,
      dout   => local_in,
      full   => open,
      empty  => local_in_empty,
      valid  => local_in_valid);

  -- output buffer
  local_wr_buffer: process (clk_uc) is
  begin  -- process local_wr_buffer
    if clk_uc'event and clk_uc = '1' then  -- rising clock edge
      local_out_rd <= not local_out_empty;      
    end if;
  end process local_wr_buffer;

  register_fifo_out: register_fifo
    port map (
      wr_clk => clk,
      rd_clk => clk_uc,
      din    => local_out,
      wr_en  => local_out_wr,
      rd_en  => local_out_rd,
      dout   => out_port,
      full   => open,
      empty  => local_out_empty,
      valid  => out_port_dv);

  -----------------------------------------------------------------------------
  -- read
  -----------------------------------------------------------------------------
  pacd_3: pacd
    port map (
      iPulseA => out_port_rd ,
      iClkA   => clk_uc,
      iRSTAn  => '1',
      iClkB   => clk,
      iRSTBn  => '1',
      oPulseB => local_out_port_rd);

  read_access: process (clk, reset)
  begin  -- process read_access
    if reset = '1' then                 -- asynchronous reset (active high)
--      out_port_dv <= '0';
      local_out_wr <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      --out_port_dv <= '0';
      local_out_wr <= '0';
      if local_out_port_rd = '1' then
        --out_port_dv <= '1';
        local_out_wr <= '1';
        case local_address is
          when LB_CLK_STATUS              =>
            local_out(31 downto 28)  <= x"0";
            local_out(27)            <= LB_monitor.ext_clock_locked_ch;
            local_out(26 downto 24)  <= LB_monitor.ext_clock_status_ch;
            local_out(23)            <= LB_monitor.ext_clock_valid_ch;
            local_out(22 downto 20)  <= LB_monitor.ext_clock_status;
            local_out(19)            <= LB_monitor.ext_clock_valid;
            local_out(18)            <= LB_monitor.daq_clock_locked;
            local_out(17)            <= LB_monitor.ext_clock_locked;
            local_out(16)            <= LB_monitor.local_clock_locked;
            local_out(15 downto 3)   <= x"000"&"0";          
            local_out(2)             <= LB_monitor.daq_clock_reset;
            local_out(1)             <= local_LB_control.daq_clock_source;
            local_out(0)             <= LB_monitor.ext_clock_reset;
          when LB_Firmware_Version    =>
            local_out                <= Firmware_Version;
          when LB_SFP                 =>
            local_out(31 downto 8)   <= x"000000";
            local_out(7 downto 5)    <= "000";
            local_out(4)            <= LB_monitor.SFP_out_mux;
            local_out(3)            <= '0';
            local_out(2)            <= local_LB_control.SFP_TxDSBL;
            local_out(1)            <= LB_monitor.SFP_LOS;
            local_out(0)            <= LB_monitor.SFP_present;
          when LB_LED_GPIO            =>
            local_out(31 downto 20) <= x"000";
            local_out(19 downto 16) <= local_LB_control.GPIO;
            local_out(15 downto  8) <= local_LB_control.GPIO_mux_sel;
            local_out(7  downto  4) <= x"0";
            local_out(3  downto  0) <= local_LB_control.LED;
          when LB_TP_status           =>
            local_out(31 downto 6) <= x"000000"&"00";
            local_out(5 downto 4)  <= local_LB_control.Test_pulse_power;
            local_out(3 downto 0)  <= local_LB_control.Test_pulse_en;
          when LB_TP_pulses1          =>
            local_out(31) <= local_testpulse.trigger_all_types;
            local_out(27 downto 20) <= local_testpulse.pulse_channel_mask;
            local_out(19 downto 0)  <= local_testpulse.start_time_count;
          when LB_TP_pulses2          =>
            local_out(27 downto 20) <= local_testpulse.pulse_count;
            local_out(19 downto 0)  <= local_testpulse.pulse_pulse_count;
          when FAKE_TDCS_ctrl      =>
            local_out(0)  <= enable_fake_TDCs_in;
            local_out(31 downto 1) <= "000"&x"0000000";
          when FAKE_TDCS_spill_n   =>
            local_out <= x"00" & monitor_fake_TDC.spill_number;
          when FAKE_TDCS_spill_t1  =>
            local_out <= monitor_fake_TDC.spill_time(43 downto 12);
          when FAKE_TDCS_spill_t2  =>
            local_out <= x"00000" & monitor_fake_TDC.spill_time(11 downto 0);
          when FAKE_TDCS_hit_count =>
            local_out(31 downto 11) <=  "0" & x"00000";
            local_out(10 downto 0)  <= std_logic_vector(monitor_fake_TDC.spill_hit_count);
          when EB_status              =>        
            local_out(31 downto 16) <= x"0000";
            local_out(15 downto 8) <= EB_monitor.EB_state;
            local_out(7 downto 1)  <= "0000000";
            --use this signal because they are all set together---\|/
            local_out(2) <= local_EB_control.enable_EB_output_8bchars; 
            local_out(1) <= local_EB_control.run_enable;
            local_out(0) <= EB_monitor.EB_reset;
          when EB_started_spills      => local_out <= EB_monitor.new_spill_count;
          when EB_sent_spills         => local_out <= EB_monitor.sent_spill_count;                                     

          when EB_debug_fifo          =>
            local_out(31 downto 12)<= x"00000";
            local_out(11)          <= EB_monitor.LB_FIFO_full;
            local_out(10)          <= EB_monitor.LB_FIFO_empty;
            local_out(9)           <= EB_monitor.LB_FIFO_data_valid;
            local_out(8 downto 0)  <= EB_monitor.LB_fifo_data;

          when EB_time_skew_max       =>                                          
            local_out(31 downto 0) <= local_EB_control.time_skew_max;

          when EB_output_8bchars        =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_output_8bchars);
          when EB_output_words        =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_output_words);
          when EB_output_word_overflows        =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_output_word_overflows);
          when EB_output_chars_total        =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_output_chars_total);
          when EB_state_SEND_IDLE_Hi        =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_state_time_idle(63 downto 32));
          when EB_state_SEND_IDLE_Lo        =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_state_time_idle(31 downto 0));
          when EB_state_CHECK_NEW_DATA      =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_state_time(1)(31 downto 0));
          when EB_state_BUILD_HEADER        =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_state_time(2)(31 downto 0));
          when EB_state_SEND_HEADER         =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_state_time(3)(31 downto 0));
          when EB_state_SEND_TDC_DATA        =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_state_time(4)(31 downto 0));
          when EB_state_SEND_END             =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_state_time(5)(31 downto 0));
          when EB_state_SEND_CRC             =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_state_time(6)(31 downto 0));
          when EB_state_SEND_TRAILER         =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_state_time(7)(31 downto 0));
          when EB_output_10bchars        =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_output_10bchars);
          when EB_missed_10bchars        =>
            local_out(31 downto 0) <= std_logic_vector(EB_monitor.EB_missed_10bchars);


            
          when C5_status              =>
            local_out(31 downto 8) <= x"000000";
            local_out(7)           <= EB_monitor.C5_fifo_full;
            local_out(6)           <= EB_monitor.C5_fifo_empty;
            local_out(5 downto 4)  <= "00";
            local_out(3)           <= local_LB_control.TDC_clock_select;
            local_out(2)           <= '0';
            local_out(1)           <= '0';
          when C5_CMD =>
            local_out(4 downto 0)  <= local_C5_control.cmd_data;          

          when C5_CMD_COUNTER_0_1      =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.C5_CMD_counters(1));
            local_out(15 downto 0)  <= std_logic_vector(EB_monitor.C5_CMD_counters(0));
          when C5_CMD_COUNTER_2_3      =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.C5_CMD_counters(3));
            local_out(15 downto 0)  <= std_logic_vector(EB_monitor.C5_CMD_counters(2));
          when C5_CMD_COUNTER_4_5      =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.C5_CMD_counters(5));
            local_out(15 downto 0)  <= std_logic_vector(EB_monitor.C5_CMD_counters(4));
          when C5_CMD_COUNTER_6_7      =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.C5_CMD_counters(7));
            local_out(15 downto 0)  <= std_logic_vector(EB_monitor.C5_CMD_counters(6));
          when C5_CMD_COUNTER_8_9      =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.C5_CMD_counters(9));
            local_out(15 downto 0)  <= std_logic_vector(EB_monitor.C5_CMD_counters(8));
          when C5_CMD_COUNTER_10_11      =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.C5_CMD_counters(11));
            local_out(15 downto 0)  <= std_logic_vector(EB_monitor.C5_CMD_counters(10));
          when C5_CMD_COUNTER_12_13      =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.C5_CMD_counters(13));
            local_out(15 downto 0)  <= std_logic_vector(EB_monitor.C5_CMD_counters(12));
          when C5_CMD_COUNTER_14_15      =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.C5_CMD_counters(15));
            local_out(15 downto 0)  <= std_logic_vector(EB_monitor.C5_CMD_counters(14));
          when TDC_control             =>
            local_out(31 downto 24) <= x"00";
            local_out(23 downto 20) <= EB_monitor.TDC_has_header;
            local_out(19 downto 16) <= EB_monitor.TDC_locked;
            local_out(15 downto 12) <= EB_monitor.TDC_fifo_empty;
            local_out(11 downto  8) <= EB_monitor.TDC_fifo_full;
            local_out(7  downto  4) <= x"0";
            local_out(3  downto  0) <= local_EB_control.TDC_enable_mask;--EB_monitor.TDC_enable_mask;

          when TDC_0_FIFO_control     =>
            local_out(31) <= '0';
            local_out(30) <= EB_monitor.TDC_fifo_data_valid(0);
            local_out(29) <= '0';
--            local_out(28 downto 24) <= EB_monitor.TDC_CDR_edges(0);
            local_out(23 downto 21) <= "000";
--            local_out(20 downto 16) <= EB_monitor.TDC_CDR_histogram(0);
            local_out(15 downto 8)  <= "00000000";
            --use the one signal for all enables
            local_out(9)            <= local_EB_control.TDC_Counter_control(0).enable_d_chars;
            local_out(7)            <= EB_monitor.TDC_addr_fifo_header_valid(0);
            local_out(6 downto 4)   <= local_EB_control.TDC_CDR_override_setting(0);
            local_out(3 downto 2)   <= EB_monitor.TDC_size_valid(0) & EB_monitor.TDC_size_fifo_empty(0);
            local_out(1 downto 0)   <= EB_monitor.TDC_fifo_data(0)(33 downto 32);
          when TDC_0_FIFO_data        => local_out <= EB_monitor.TDC_fifo_data(0)(31 downto 0);          
          when TDC_0_k_chars          => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).k_chars);
          when TDC_0_k_chars_3C       => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).k_chars_3C);                                         
          when TDC_0_k_chars_in_data  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).k_chars_in_data);
          when TDC_0_malformed_spills => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).malformed_spills);
          when TDC_0_invalid_chars    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).invalid_chars);
          when TDC_0_misaligned_syncs => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).misaligned_syncs);
          when TDC_0_d_chars          => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).d_chars);
--          when TDC_0_chars            => local_out <= "000000" & EB_monitor.TDC_10b(0) & "0000000" & EB_monitor.TDC_8b(0);
          when TDC_0_debug_size       =>
            local_out(31)           <= '0';
            local_out(30 downto 16) <= EB_monitor.TDC_actual_size(0);
            local_out(15)           <= '0';
            local_out(14 downto 0)  <= EB_monitor.TDC_expected_size(0);
          when TDC_0_missing_tr_bits  => local_out <= std_logic_vector(EB_monitor.TDC_missing_trailer_bits(0));
          when TDC_0_lock_timeouts    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).lock_timeouts);     
--          when TDC_0_multi_valids     => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).multi_valids);
          when TDC_0_DECODE_FIND_LOCK_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).DECODE_FIND_LOCK_time);
          when TDC_0_stream_start_end =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.TDC_Counters(0).stream_ends);
            local_out(15 downto  0) <= std_logic_vector(EB_monitor.TDC_Counters(0).stream_starts);
          when TDC_0_DECODE_LOCKED_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).DECODE_LOCKED_time);
          when TDC_0_DECODE_PROCESS_SPILL_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).DECODE_PROCESS_SPILL_time);
          when TDC_0_k_chars_BC       => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).k_chars_BC);
          when TDC_0_bad_disparity    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).bad_disparity);
          when TDC_0_time             => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).time_count);
          when TDC_0_lost_alignments  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(0).lost_alignment);
          when TDC_0_CDR_history      =>
            local_out <= (others => '0');
            local_out(20 downto 16) <= EB_monitor.TDC_CDR_history(0).samples;
            local_out(12 downto  8) <= EB_monitor.TDC_CDR_history(0).edges;
            local_out(7)            <= EB_monitor.TDC_CDR_history(0).valid;
            local_out(6)            <= EB_monitor.TDC_CDR_history(0).ready;
            local_out(2)            <= EB_monitor.TDC_CDR_history(0).empty;
            
          when TDC_1_FIFO_control     =>
            local_out(31) <= '0';
            local_out(30) <= EB_monitor.TDC_fifo_data_valid(1);
            local_out(29) <= '0';
--            local_out(28 downto 24) <= EB_monitor.TDC_CDR_edges(1);
            local_out(23 downto 21) <= "000";
--            local_out(20 downto 16) <= EB_monitor.TDC_CDR_histogram(1);
            local_out(15 downto 8 ) <= "00000000";
            --use the one signal for all enables
            local_out(9)            <= local_EB_control.TDC_Counter_control(1).enable_d_chars;
            local_out(7)            <= EB_monitor.TDC_addr_fifo_header_valid(1);
            local_out(6 downto 4)   <= local_EB_control.TDC_CDR_override_setting(1);
            local_out(3 downto 2)   <= EB_monitor.TDC_size_valid(1) & EB_monitor.TDC_size_fifo_empty(1);
            local_out(1 downto 0) <= EB_monitor.TDC_fifo_data(1)(33 downto 32);
          when TDC_1_FIFO_data        => local_out <= EB_monitor.TDC_fifo_data(1)(31 downto 0);                                      
          when TDC_1_k_chars          => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).k_chars);
          when TDC_1_k_chars_3C       => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).k_chars_3C);
          when TDC_1_k_chars_in_data  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).k_chars_in_data);
          when TDC_1_malformed_spills => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).malformed_spills);
          when TDC_1_invalid_chars    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).invalid_chars);
          when TDC_1_misaligned_syncs => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).misaligned_syncs);
          when TDC_1_d_chars          => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).d_chars);
--          when TDC_1_chars            => local_out <= "000000" & EB_monitor.TDC_10b(1) & "0000000" & EB_monitor.TDC_8b(1);  
          when TDC_1_debug_size       =>
            local_out(31)           <= '0';
            local_out(30 downto 16) <= EB_monitor.TDC_actual_size(1);
            local_out(15)           <= '0';
            local_out(14 downto 0)  <= EB_monitor.TDC_expected_size(1);
          when TDC_1_missing_tr_bits  => local_out <= std_logic_vector(EB_monitor.TDC_missing_trailer_bits(1));
          when TDC_1_lock_timeouts    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).lock_timeouts);
--          when TDC_1_multi_valids     => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).multi_valids);
          when TDC_1_DECODE_FIND_LOCK_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).DECODE_FIND_LOCK_time);
          when TDC_1_stream_start_end =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.TDC_Counters(1).stream_ends);
            local_out(15 downto  0) <= std_logic_vector(EB_monitor.TDC_Counters(1).stream_starts);
          when TDC_1_DECODE_LOCKED_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).DECODE_LOCKED_time);
          when TDC_1_DECODE_PROCESS_SPILL_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).DECODE_PROCESS_SPILL_time);
          when TDC_1_k_chars_BC       => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).k_chars_BC);          
          when TDC_1_bad_disparity    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).bad_disparity);
          when TDC_1_time             => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).time_count);
          when TDC_1_lost_alignments  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(1).lost_alignment);
          when TDC_1_CDR_history      =>
            local_out <= (others => '0');
            local_out(20 downto 16) <= EB_monitor.TDC_CDR_history(1).samples;
            local_out(12 downto  8) <= EB_monitor.TDC_CDR_history(1).edges;
            local_out(7)            <= EB_monitor.TDC_CDR_history(1).valid;
            local_out(6)            <= EB_monitor.TDC_CDR_history(1).ready;
            local_out(2)            <= EB_monitor.TDC_CDR_history(1).empty;
                                         
          when TDC_2_FIFO_control     =>
            local_out(31) <= '0';
            local_out(30) <= EB_monitor.TDC_fifo_data_valid(2);
            local_out(29) <= '0';
--            local_out(28 downto 24) <= EB_monitor.TDC_CDR_edges(2);
            local_out(23 downto 21) <= "000";
--            local_out(20 downto 16) <= EB_monitor.TDC_CDR_histogram(2);
            local_out(15 downto  8) <= "00000000";
            --use the one signal for all enables
            local_out(9)            <= local_EB_control.TDC_Counter_control(2).enable_d_chars;
            local_out(7)            <= EB_monitor.TDC_addr_fifo_header_valid(2);
            local_out(6 downto 4)   <= local_EB_control.TDC_CDR_override_setting(2);
            local_out(3 downto 2)   <= EB_monitor.TDC_size_valid(2) & EB_monitor.TDC_size_fifo_empty(2);
            local_out(1 downto 0) <= EB_monitor.TDC_fifo_data(2)(33 downto 32);
          when TDC_2_FIFO_data        => local_out <= EB_monitor.TDC_fifo_data(2)(31 downto 0);
          when TDC_2_k_chars          => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).k_chars);
          when TDC_2_k_chars_3C       => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).k_chars_3C);
          when TDC_2_k_chars_in_data  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).k_chars_in_data);
          when TDC_2_malformed_spills => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).malformed_spills);
          when TDC_2_invalid_chars    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).invalid_chars);
          when TDC_2_misaligned_syncs => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).misaligned_syncs);
          when TDC_2_d_chars          => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).d_chars);
--          when TDC_2_chars            => local_out <= "000000" & EB_monitor.TDC_10b(2) & "0000000" & EB_monitor.TDC_8b(2);
          when TDC_2_debug_size       =>
            local_out(31)           <= '0';
            local_out(30 downto 16) <= EB_monitor.TDC_actual_size(2);
            local_out(15)           <= '0';
            local_out(14 downto 0)  <= EB_monitor.TDC_expected_size(2);
          when TDC_2_missing_tr_bits  => local_out <= std_logic_vector(EB_monitor.TDC_missing_trailer_bits(2));
          when TDC_2_lock_timeouts    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).lock_timeouts);            
--          when TDC_2_multi_valids     => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).multi_valids);
          when TDC_2_DECODE_FIND_LOCK_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).DECODE_FIND_LOCK_time);
          when TDC_2_stream_start_end =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.TDC_Counters(2).stream_ends);
            local_out(15 downto  0) <= std_logic_vector(EB_monitor.TDC_Counters(2).stream_starts);
          when TDC_2_DECODE_LOCKED_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).DECODE_LOCKED_time);
          when TDC_2_DECODE_PROCESS_SPILL_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).DECODE_PROCESS_SPILL_time);
          when TDC_2_k_chars_BC       => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).k_chars_BC);
          when TDC_2_bad_disparity    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).bad_disparity);
          when TDC_2_time             => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).time_count);
          when TDC_2_lost_alignments  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(2).lost_alignment);
          when TDC_2_CDR_history      =>
            local_out <= (others => '0');
            local_out(20 downto 16) <= EB_monitor.TDC_CDR_history(2).samples;
            local_out(12 downto  8) <= EB_monitor.TDC_CDR_history(2).edges;
            local_out(7)            <= EB_monitor.TDC_CDR_history(2).valid;
            local_out(6)            <= EB_monitor.TDC_CDR_history(2).ready;
            local_out(2)            <= EB_monitor.TDC_CDR_history(2).empty;

                                         
          when TDC_3_FIFO_control     =>
            local_out(31) <= '0';
            local_out(30) <= EB_monitor.TDC_fifo_data_valid(3);
            local_out(29) <= '0';
--            local_out(28 downto 24) <= EB_monitor.TDC_CDR_edges(3);
            local_out(23 downto 21) <= "000";
--            local_out(20 downto 16) <= EB_monitor.TDC_CDR_histogram(3);
            local_out(15 downto  8) <= "00000000";
            --use the one signal for all enables
            local_out(9)            <= local_EB_control.TDC_Counter_control(3).enable_d_chars;
            local_out(7)            <= EB_monitor.TDC_addr_fifo_header_valid(3);            
            local_out(6 downto 4)   <= local_EB_control.TDC_CDR_override_setting(3);
            local_out(3 downto 2)   <= EB_monitor.TDC_size_valid(3) & EB_monitor.TDC_size_fifo_empty(3);
            local_out(1 downto 0) <= EB_monitor.TDC_fifo_data(3)(33 downto 32);
          when TDC_3_FIFO_data        => local_out <= EB_monitor.TDC_fifo_data(3)(31 downto 0);
          when TDC_3_k_chars          => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).k_chars);
          when TDC_3_k_chars_3C       => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).k_chars_3C);
          when TDC_3_k_chars_in_data  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).k_chars_in_data);
          when TDC_3_malformed_spills => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).malformed_spills);
          when TDC_3_invalid_chars    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).invalid_chars);
          when TDC_3_misaligned_syncs => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).misaligned_syncs);
          when TDC_3_d_chars          => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).d_chars);
--          when TDC_3_chars            => local_out <= "000000" & EB_monitor.TDC_10b(3) & "0000000" & EB_monitor.TDC_8b(3);
          when TDC_3_debug_size       =>
            local_out(31)           <= '0';
            local_out(30 downto 16) <= EB_monitor.TDC_actual_size(3);
            local_out(15)           <= '0';
            local_out(14 downto 0)  <= EB_monitor.TDC_expected_size(3);
          when TDC_3_missing_tr_bits  => local_out <= std_logic_vector(EB_monitor.TDC_missing_trailer_bits(3));
          when TDC_3_lock_timeouts    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).lock_timeouts);
--          when TDC_3_multi_valids     => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).multi_valids);
          when TDC_3_DECODE_FIND_LOCK_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).DECODE_FIND_LOCK_time);
          when TDC_3_stream_start_end =>
            local_out(31 downto 16) <= std_logic_vector(EB_monitor.TDC_Counters(3).stream_ends);
            local_out(15 downto  0) <= std_logic_vector(EB_monitor.TDC_Counters(3).stream_starts);
          when TDC_3_DECODE_LOCKED_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).DECODE_LOCKED_time);
          when TDC_3_DECODE_PROCESS_SPILL_time  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).DECODE_PROCESS_SPILL_time);
          when TDC_3_k_chars_BC       => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).k_chars_BC);
          when TDC_3_bad_disparity    => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).bad_disparity);
          when TDC_3_time             => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).time_count);
          when TDC_3_lost_alignments  => local_out <= std_logic_vector(EB_monitor.TDC_Counters(3).lost_alignment);
          when TDC_3_CDR_history      =>
            local_out <= (others => '0');
            local_out(20 downto 16) <= EB_monitor.TDC_CDR_history(3).samples;
            local_out(12 downto  8) <= EB_monitor.TDC_CDR_history(3).edges;
            local_out(7)            <= EB_monitor.TDC_CDR_history(3).valid;
            local_out(6)            <= EB_monitor.TDC_CDR_history(3).ready;
            local_out(2)            <= EB_monitor.TDC_CDR_history(3).empty;
                                         
          when others => null;
        end case;
      end if;
    end if;
  end process read_access;
  
  -----------------------------------------------------------------------------
  -- write
  -----------------------------------------------------------------------------
  
  write_access: process (clk, reset)
  begin  -- process read_access
    if reset = '1' then                 -- asynchronous reset (active high)
      --LB_CLK_STATUS
      local_LB_control.daq_clock_reset <= '0';
      local_LB_control.ext_clock_reset <= '0';        
      local_LB_control.daq_clock_source <= '0';
      --LB_SFP
      local_LB_control.SFP_out_mux <= '0';    -- 8b10 out (default)
      local_LB_control.SFP_TxDSBL  <= '0';
      --LB_LED_GPIO
      local_LB_control.GPIO  <= x"0";
      local_LB_control.GPIO_mux_sel <= x"00";
      local_LB_control.LED   <= x"0";
      --LB_TP_status
      local_LB_control.Test_pulse_power <= "00";
      local_LB_control.Test_pulse_en    <= x"0";
      local_testpulse.trigger_all_types <= '0';
      local_testpulse.pulse_channel_mask <= x"00";
      local_testpulse.start_time_count <= x"00000";
      local_testpulse.pulse_count <= x"00";
      local_testpulse.pulse_pulse_count <= x"00000";
      -- EB_time_skew_max
      local_EB_control.time_skew_max <= x"00000010";
      --C5_CMD
      local_C5_control.cmd_data  <= "00000";
      --C5_status
      local_LB_control.TDC_clock_select <= '0';  -- SFP C5 (default)
      --TDC_control
      local_EB_Control.TDC_enable_mask <= x"0";
      local_EB_Control.TDC_CDR_override_setting <= (others => "110");
      -- Fake TDCs
      enable_fake_TDCs_out <= '0';
      control_fake_TDC.spill_number <= x"000000";
      control_fake_TDC.spill_time(43 downto 0) <= x"00000000000";
      control_fake_TDC.spill_hit_count(10 downto 0) <= "00000001000";
      --EB STATUS
      local_EB_control.run_enable <= '0';

      --COUTNER enables
      local_EB_control.enable_EB_output_8bchars        <= '1';
      local_EB_control.enable_EB_output_words          <= '1';
      local_EB_control.enable_EB_output_word_overflows <= '1';
      local_EB_control.enable_EB_output_chars_total    <= '1';
      local_EB_control.enable_EB_state_time(0)         <= '1';
      local_EB_control.enable_EB_state_time(0)         <= '1';
      local_EB_control.enable_EB_state_time(1)         <= '1';
      local_EB_control.enable_EB_state_time(2)         <= '1';
      local_EB_control.enable_EB_state_time(3)         <= '1';
      local_EB_control.enable_EB_state_time(4)         <= '1';
      local_EB_control.enable_EB_state_time(5)         <= '1';
      local_EB_control.enable_EB_state_time(6)         <= '1';
      local_EB_control.enable_EB_state_time(7)         <= '1';
      local_EB_control.enable_EB_output_10bchars       <= '1';
      local_EB_control.enable_EB_missed_10bchars       <= '1';      
      for iTDC in 0 to 3 loop
        local_EB_control.enable_TDC_missing_trailer_bits(iTDC) <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_time                       <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_d_chars                    <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_k_chars                    <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_k_chars_3C                 <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_k_chars_BC                 <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_malformed_spills           <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_k_chars_in_data            <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_invalid_chars              <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_bad_disparity              <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_misaligned_syncs           <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_lock_timeouts              <= '1';
--        local_EB_control.TDC_Counter_control(iTDC).enable_multi_valids               <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_DECODE_FIND_LOCK_time      <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_DECODE_LOCKED_time         <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_DECODE_PROCESS_SPILL_time  <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_stream_starts              <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_stream_ends                <= '1';
        local_EB_control.TDC_Counter_control(iTDC).enable_lost_alignment             <= '1';
      end loop;

      
      
    elsif clk'event and clk = '1' then  -- rising clock edge      
--      if in_port_wr = '1' then
      if local_in_valid = '1' then
        case local_address is
          when LB_CLK_STATUS              =>
            local_LB_control.Ext_clock_reset <= local_in(0);
            local_LB_control.daq_clock_source <= local_in(1);
            local_LB_control.daq_clock_reset <= local_in(2);            
          when LB_TP_pulses1          =>
            local_testpulse.trigger_all_types <= local_in(31);
            local_testpulse.pulse_channel_mask <= local_in(27 downto 20);
            local_testpulse.start_time_count <= local_in(19 downto 0);
          when LB_TP_pulses2          =>
            local_testpulse.pulse_count <= local_in(27 downto 20);
            local_testpulse.pulse_pulse_count <= local_in(19 downto 0);
          when LB_SFP                 =>
            local_LB_control.SFP_out_mux <= local_in(4);
            local_LB_control.SFP_TxDSBL  <= local_in(2);
          when LB_LED_GPIO            =>
            local_LB_control.GPIO  <= local_in(19 downto 16);
            local_LB_control.GPIO_mux_sel <= local_in(15 downto 8);
            local_LB_control.LED   <= local_in(3  downto  0);
          when LB_TP_status           =>
            local_LB_control.Test_pulse_power <= local_in(5 downto 4);
            local_LB_control.Test_pulse_en    <= local_in(3 downto 0);
          when EB_time_skew_max       =>                                          
            local_EB_control.time_skew_max <= local_in(31 downto 0);
          when C5_CMD =>
            local_C5_control.cmd_data  <= local_in(4 downto 0);
          when TDC_control          =>
            local_EB_control.TDC_enable_mask <= local_in(3 downto 0) ;
          when TDC_0_FIFO_control   =>
            local_EB_control.TDC_CDR_override_setting(0) <= local_in(6 downto 4);
            
            local_EB_control.TDC_Counter_control(0).enable_d_chars                    <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_time                       <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_k_chars                    <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_k_chars_3C                 <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_k_chars_BC                 <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_malformed_spills           <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_k_chars_in_data            <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_invalid_chars              <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_bad_disparity              <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_misaligned_syncs           <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_lock_timeouts              <= local_in(9);
--            local_EB_control.TDC_Counter_control(0).enable_multi_valids               <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_DECODE_FIND_LOCK_time      <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_DECODE_LOCKED_time         <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_DECODE_PROCESS_SPILL_time  <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_stream_starts              <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_stream_ends                <= local_in(9);
            local_EB_control.TDC_Counter_control(0).enable_lost_alignment             <= local_in(9);
            
          when TDC_1_FIFO_control   =>
            local_EB_control.TDC_CDR_override_setting(1) <= local_in(6 downto 4);
            local_EB_control.TDC_Counter_control(1).enable_d_chars                    <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_time                       <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_k_chars                    <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_k_chars_3C                 <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_k_chars_BC                 <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_malformed_spills           <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_k_chars_in_data            <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_invalid_chars              <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_bad_disparity              <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_misaligned_syncs           <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_lock_timeouts              <= local_in(9);
--            local_EB_control.TDC_Counter_control(1).enable_multi_valids               <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_DECODE_FIND_LOCK_time      <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_DECODE_LOCKED_time         <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_DECODE_PROCESS_SPILL_time  <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_stream_starts              <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_stream_ends                <= local_in(9);
            local_EB_control.TDC_Counter_control(1).enable_lost_alignment             <= local_in(9);
          when TDC_2_FIFO_control   =>
            local_EB_control.TDC_CDR_override_setting(2) <= local_in(6 downto 4);
            local_EB_control.TDC_Counter_control(2).enable_d_chars                    <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_time                       <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_k_chars                    <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_k_chars_3C                 <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_k_chars_BC                 <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_malformed_spills           <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_k_chars_in_data            <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_invalid_chars              <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_bad_disparity              <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_misaligned_syncs           <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_lock_timeouts              <= local_in(9);
--            local_EB_control.TDC_Counter_control(2).enable_multi_valids               <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_DECODE_FIND_LOCK_time      <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_DECODE_LOCKED_time         <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_DECODE_PROCESS_SPILL_time  <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_stream_starts              <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_stream_ends                <= local_in(9);
            local_EB_control.TDC_Counter_control(2).enable_lost_alignment             <= local_in(9);
          when TDC_3_FIFO_control   =>
            local_EB_control.TDC_CDR_override_setting(3) <= local_in(6 downto 4);
            local_EB_control.TDC_Counter_control(3).enable_d_chars                    <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_time                       <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_k_chars                    <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_k_chars_3C                 <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_k_chars_BC                 <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_malformed_spills           <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_k_chars_in_data            <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_invalid_chars              <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_bad_disparity              <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_misaligned_syncs           <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_lock_timeouts              <= local_in(9);
--            local_EB_control.TDC_Counter_control(3).enable_multi_valids               <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_DECODE_FIND_LOCK_time      <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_DECODE_LOCKED_time         <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_DECODE_PROCESS_SPILL_time  <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_stream_starts              <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_stream_ends                <= local_in(9);
            local_EB_control.TDC_Counter_control(3).enable_lost_alignment             <= local_in(9);
          when FAKE_TDCS_ctrl     =>
            enable_fake_TDCs_out <= local_in(0);
          when FAKE_TDCS_spill_n              =>
            control_fake_TDC.spill_number <= local_in(23 downto 0);
          when FAKE_TDCS_spill_t1 =>
            control_fake_TDC.spill_time(43 downto 12) <= local_in;
          when FAKE_TDCS_spill_t2 =>
            control_fake_TDC.spill_time(11 downto 0) <= local_in(11 downto 0);
          when FAKE_TDCS_hit_count  =>                                     
            control_fake_TDC.spill_hit_count(10 downto 0) <= unsigned(local_in(10 downto 0));
          when C5_status            =>
            local_LB_control.TDC_clock_select <= local_in(3);
          when EB_status              =>        
            local_EB_control.run_enable <= local_in(1);
            
            local_EB_control.enable_EB_output_8bchars        <= local_in(2);
            local_EB_control.enable_EB_output_words          <= local_in(2);
            local_EB_control.enable_EB_output_word_overflows <= local_in(2);
            local_EB_control.enable_EB_output_chars_total    <= local_in(2);
            local_EB_control.enable_EB_state_time(0)         <= local_in(2);
            local_EB_control.enable_EB_state_time(0)         <= local_in(2);
            local_EB_control.enable_EB_state_time(1)         <= local_in(2);
            local_EB_control.enable_EB_state_time(2)         <= local_in(2);
            local_EB_control.enable_EB_state_time(3)         <= local_in(2);
            local_EB_control.enable_EB_state_time(4)         <= local_in(2);
            local_EB_control.enable_EB_state_time(5)         <= local_in(2);
            local_EB_control.enable_EB_state_time(6)         <= local_in(2);
            local_EB_control.enable_EB_state_time(7)         <= local_in(2);
            local_EB_control.enable_EB_output_10bchars       <= local_in(2);
            local_EB_control.enable_EB_missed_10bchars       <= local_in(2);

          when others => null;
        end case;        
      end if;      
    end if;
  end process write_access;

  -----------------------------------------------------------------------------
  -- actions
  -----------------------------------------------------------------------------
  

  action_access: process (clk, reset)
  begin  -- process action_access
    if reset = '1' then                 -- asynchronous reset (active high)
      local_C5_control.cmd_strobe <= '0';
      local_EB_reset <= '0';
      local_EB_control.TDC_FIFO_rd <= x"0";
      local_fake_TDC_send_spill <= '0';
      local_LB_control.reset_ext_monitor <= '0';
      local_EB_control.LB_FIFO_rd <= '0';

      local_TDC_CDR_history_reset <= x"0";
      local_TDC_CDR_history_rd <= x"0";
--      local_EB_control.TDC_CDR_history_control(0).rd <= '0';
--    local_EB_control.TDC_CDR_history_control(0).reset <= '0';

      
      local_EB_control.reset_EB_output_8bchars        <= '1';
      local_EB_control.reset_EB_output_words          <= '1';
      local_EB_control.reset_EB_output_word_overflows <= '1';
      local_EB_control.reset_EB_output_chars_total    <= '1';
      local_EB_control.reset_EB_state_time(0)         <= '1';
      local_EB_control.reset_EB_state_time(0)         <= '1';
      local_EB_control.reset_EB_state_time(1)         <= '1';
      local_EB_control.reset_EB_state_time(2)         <= '1';
      local_EB_control.reset_EB_state_time(3)         <= '1';
      local_EB_control.reset_EB_state_time(4)         <= '1';
      local_EB_control.reset_EB_state_time(5)         <= '1';
      local_EB_control.reset_EB_state_time(6)         <= '1';
      local_EB_control.reset_EB_state_time(7)         <= '1';
      local_EB_control.reset_EB_output_10bchars       <= '1';
      local_EB_control.reset_EB_missed_10bchars       <= '1';
      for iTDC in 0 to 3 loop
        local_EB_control.reset_TDC_missing_trailer_bits(iTDC) <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_d_chars                    <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_k_chars                    <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_k_chars_3C                 <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_k_chars_BC                 <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_malformed_spills           <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_k_chars_in_data            <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_invalid_chars              <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_bad_disparity              <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_misaligned_syncs           <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_lock_timeouts              <= '1';
--        local_EB_control.TDC_Counter_control(iTDC).reset_multi_valids               <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_DECODE_FIND_LOCK_time      <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_DECODE_LOCKED_time         <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_DECODE_PROCESS_SPILL_time  <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_stream_starts              <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_stream_ends                <= '1';
        local_EB_control.TDC_Counter_control(iTDC).reset_lost_alignment             <= '1';
      end loop;


    elsif clk'event and clk = '1' then  -- rising clock edge
      --zero action register signals
      local_C5_control.cmd_strobe <= '0';
      local_EB_reset <= '0';
      local_EB_control.TDC_FIFO_rd <= x"0";
      local_fake_TDC_send_spill <= '0';
      local_LB_control.reset_ext_monitor <= '0';
      local_EB_control.LB_FIFO_rd <= '0';
      local_TDC_CDR_history_reset <= x"0";
      local_TDC_CDR_history_rd <= x"0";

      

      -- counter resets
      local_EB_control.reset_EB_output_8bchars        <= '0';
      local_EB_control.reset_EB_output_words          <= '0';
      local_EB_control.reset_EB_output_word_overflows <= '0';
      local_EB_control.reset_EB_output_chars_total    <= '0';
      local_EB_control.reset_EB_state_time(0)         <= '0';
      local_EB_control.reset_EB_state_time(0)         <= '0';
      local_EB_control.reset_EB_state_time(1)         <= '0';
      local_EB_control.reset_EB_state_time(2)         <= '0';
      local_EB_control.reset_EB_state_time(3)         <= '0';
      local_EB_control.reset_EB_state_time(4)         <= '0';
      local_EB_control.reset_EB_state_time(5)         <= '0';
      local_EB_control.reset_EB_state_time(6)         <= '0';
      local_EB_control.reset_EB_state_time(7)         <= '0';
      local_EB_control.reset_EB_output_10bchars       <= '0';
      local_EB_control.reset_EB_missed_10bchars       <= '0';
      for iTDC in 0 to 3 loop
        local_EB_control.reset_TDC_missing_trailer_bits(iTDC) <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_time                       <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_d_chars                    <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_k_chars                    <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_k_chars_3C                 <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_k_chars_BC                 <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_malformed_spills           <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_k_chars_in_data            <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_invalid_chars              <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_bad_disparity              <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_misaligned_syncs           <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_lock_timeouts              <= '0';
--        local_EB_control.TDC_Counter_control(iTDC).reset_multi_valids               <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_DECODE_FIND_LOCK_time      <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_DECODE_LOCKED_time         <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_DECODE_PROCESS_SPILL_time  <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_stream_starts              <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_stream_ends                <= '0';
        local_EB_control.TDC_Counter_control(iTDC).reset_lost_alignment             <= '0';
      end loop;


      if local_in_valid = '1' then
        case local_address is
          when LB_CLK_STATUS  =>
            if local_in(3) = '1' then
              local_LB_control.reset_ext_monitor <= '1';
            end if;
          when EB_status              =>
            if local_in(0) = '1' then 
              local_EB_reset <= '1';
            end if;
          when EB_DEBUG_FIFO =>
            local_EB_control.LB_FIFO_rd <= '1';
          when FAKE_TDCS_ctrl     =>
            local_fake_TDC_send_spill <= local_in(31);
          when C5_CMD =>
            if local_in(8) = '1' then
              local_C5_control.cmd_strobe <= '1';
            end if;
          when TDC_0_FIFO_control =>
            if local_in(31) = '1' then
              local_EB_control.TDC_FIFO_rd(0) <= '1';
            elsif local_in(8) = '1' then
              local_EB_control.reset_TDC_missing_trailer_bits(0) <= '1';
              local_EB_control.TDC_Counter_control(0).reset_time                       <= '1';
              local_EB_control.TDC_Counter_control(0).reset_d_chars                    <= '1';
              local_EB_control.TDC_Counter_control(0).reset_k_chars                    <= '1';
              local_EB_control.TDC_Counter_control(0).reset_k_chars_3C                 <= '1';
              local_EB_control.TDC_Counter_control(0).reset_k_chars_BC                 <= '1';
              local_EB_control.TDC_Counter_control(0).reset_malformed_spills           <= '1';
              local_EB_control.TDC_Counter_control(0).reset_k_chars_in_data            <= '1';
              local_EB_control.TDC_Counter_control(0).reset_invalid_chars              <= '1';
              local_EB_control.TDC_Counter_control(0).reset_bad_disparity              <= '1';
              local_EB_control.TDC_Counter_control(0).reset_misaligned_syncs           <= '1';
              local_EB_control.TDC_Counter_control(0).reset_lock_timeouts              <= '1';
--              local_EB_control.TDC_Counter_control(0).reset_multi_valids               <= '1';
              local_EB_control.TDC_Counter_control(0).reset_DECODE_FIND_LOCK_time      <= '1';
              local_EB_control.TDC_Counter_control(0).reset_DECODE_LOCKED_time         <= '1';
              local_EB_control.TDC_Counter_control(0).reset_DECODE_PROCESS_SPILL_time  <= '1';
              local_EB_control.TDC_Counter_control(0).reset_stream_starts              <= '1';
              local_EB_control.TDC_Counter_control(0).reset_stream_ends                <= '1';
              local_EB_control.TDC_Counter_control(0).reset_lost_alignment             <= '1';
            end if;
          when TDC_1_FIFO_control =>
            if local_in(31) = '1' then
              local_EB_control.TDC_FIFO_rd(1) <= '1';
            elsif local_in(8) = '1' then
              local_EB_control.reset_TDC_missing_trailer_bits(1) <= '1';
              local_EB_control.TDC_Counter_control(1).reset_time                       <= '1';
              local_EB_control.TDC_Counter_control(1).reset_d_chars                    <= '1';
              local_EB_control.TDC_Counter_control(1).reset_k_chars                    <= '1';
              local_EB_control.TDC_Counter_control(1).reset_k_chars_3C                 <= '1';
              local_EB_control.TDC_Counter_control(1).reset_k_chars_BC                 <= '1';
              local_EB_control.TDC_Counter_control(1).reset_malformed_spills           <= '1';
              local_EB_control.TDC_Counter_control(1).reset_k_chars_in_data            <= '1';
              local_EB_control.TDC_Counter_control(1).reset_invalid_chars              <= '1';
              local_EB_control.TDC_Counter_control(1).reset_bad_disparity              <= '1';
              local_EB_control.TDC_Counter_control(1).reset_misaligned_syncs           <= '1';
              local_EB_control.TDC_Counter_control(1).reset_lock_timeouts              <= '1';
--              local_EB_control.TDC_Counter_control(1).reset_multi_valids               <= '1';
              local_EB_control.TDC_Counter_control(1).reset_DECODE_FIND_LOCK_time      <= '1';
              local_EB_control.TDC_Counter_control(1).reset_DECODE_LOCKED_time         <= '1';
              local_EB_control.TDC_Counter_control(1).reset_DECODE_PROCESS_SPILL_time  <= '1';
              local_EB_control.TDC_Counter_control(1).reset_stream_starts              <= '1';
              local_EB_control.TDC_Counter_control(1).reset_stream_ends                <= '1';
              local_EB_control.TDC_Counter_control(1).reset_lost_alignment             <= '1';
            end if;
          when TDC_2_FIFO_control =>
            if local_in(31) = '1' then
              local_EB_control.TDC_FIFO_rd(2) <= '1';
            elsif local_in(8) = '1' then
              local_EB_control.reset_TDC_missing_trailer_bits(2) <= '1';
              local_EB_control.TDC_Counter_control(2).reset_time                       <= '1';
              local_EB_control.TDC_Counter_control(2).reset_d_chars                    <= '1';
              local_EB_control.TDC_Counter_control(2).reset_k_chars                    <= '1';
              local_EB_control.TDC_Counter_control(2).reset_k_chars_3C                 <= '1';
              local_EB_control.TDC_Counter_control(2).reset_k_chars_BC                 <= '1';
              local_EB_control.TDC_Counter_control(2).reset_malformed_spills           <= '1';
              local_EB_control.TDC_Counter_control(2).reset_k_chars_in_data            <= '1';
              local_EB_control.TDC_Counter_control(2).reset_invalid_chars              <= '1';
              local_EB_control.TDC_Counter_control(2).reset_bad_disparity              <= '1';
              local_EB_control.TDC_Counter_control(2).reset_misaligned_syncs           <= '1';
              local_EB_control.TDC_Counter_control(2).reset_lock_timeouts              <= '1';
--              local_EB_control.TDC_Counter_control(2).reset_multi_valids               <= '1';
              local_EB_control.TDC_Counter_control(2).reset_DECODE_FIND_LOCK_time      <= '1';
              local_EB_control.TDC_Counter_control(2).reset_DECODE_LOCKED_time         <= '1';
              local_EB_control.TDC_Counter_control(2).reset_DECODE_PROCESS_SPILL_time  <= '1';
              local_EB_control.TDC_Counter_control(2).reset_stream_starts              <= '1';
              local_EB_control.TDC_Counter_control(2).reset_stream_ends                <= '1';
              local_EB_control.TDC_Counter_control(2).reset_lost_alignment             <= '1';
            end if;
          when TDC_3_FIFO_control =>
            if local_in(31) = '1' then
              local_EB_control.TDC_FIFO_rd(3) <= '1';
            elsif local_in(8) = '1' then
              local_EB_control.reset_TDC_missing_trailer_bits(3) <= '1';
              local_EB_control.TDC_Counter_control(3).reset_time                       <= '1';
              local_EB_control.TDC_Counter_control(3).reset_d_chars                    <= '1';
              local_EB_control.TDC_Counter_control(3).reset_k_chars                    <= '1';
              local_EB_control.TDC_Counter_control(3).reset_k_chars_3C                 <= '1';
              local_EB_control.TDC_Counter_control(3).reset_k_chars_BC                 <= '1';
              local_EB_control.TDC_Counter_control(3).reset_malformed_spills           <= '1';
              local_EB_control.TDC_Counter_control(3).reset_k_chars_in_data            <= '1';
              local_EB_control.TDC_Counter_control(3).reset_invalid_chars              <= '1';
              local_EB_control.TDC_Counter_control(3).reset_bad_disparity              <= '1';
              local_EB_control.TDC_Counter_control(3).reset_misaligned_syncs           <= '1';
              local_EB_control.TDC_Counter_control(3).reset_lock_timeouts              <= '1';
--              local_EB_control.TDC_Counter_control(3).reset_multi_valids               <= '1';
              local_EB_control.TDC_Counter_control(3).reset_DECODE_FIND_LOCK_time      <= '1';
              local_EB_control.TDC_Counter_control(3).reset_DECODE_LOCKED_time         <= '1';
              local_EB_control.TDC_Counter_control(3).reset_DECODE_PROCESS_SPILL_time  <= '1';
              local_EB_control.TDC_Counter_control(3).reset_stream_starts              <= '1';
              local_EB_control.TDC_Counter_control(3).reset_stream_ends                <= '1';
              local_EB_control.TDC_Counter_control(3).reset_lost_alignment             <= '1';
            end if;
            
          when EB_output_8bchars        => local_EB_control.reset_EB_output_8bchars        <= '1';
          when EB_output_words          => local_EB_control.reset_EB_output_words          <= '1';
          when EB_output_word_overflows => local_EB_control.reset_EB_output_word_overflows <= '1';
          when EB_output_chars_total    => local_EB_control.reset_EB_output_chars_total    <= '1';
          when EB_state_SEND_IDLE_Hi    => local_EB_control.reset_EB_state_time(0)         <= '1';
          when EB_state_SEND_IDLE_Lo    => local_EB_control.reset_EB_state_time(0)         <= '1';
          when EB_state_CHECK_NEW_DATA  => local_EB_control.reset_EB_state_time(1)         <= '1';
          when EB_state_BUILD_HEADER    => local_EB_control.reset_EB_state_time(2)         <= '1';
          when EB_state_SEND_HEADER     => local_EB_control.reset_EB_state_time(3)         <= '1';
          when EB_state_SEND_TDC_DATA   => local_EB_control.reset_EB_state_time(4)         <= '1';
          when EB_state_SEND_END        => local_EB_control.reset_EB_state_time(5)         <= '1';
          when EB_state_SEND_CRC        => local_EB_control.reset_EB_state_time(6)         <= '1';
          when EB_state_SEND_TRAILER    => local_EB_control.reset_EB_state_time(7)         <= '1';
          when EB_output_10bchars       => local_EB_control.reset_EB_output_10bchars       <= '1';
          when EB_missed_10bchars       => local_EB_control.reset_EB_missed_10bchars       <= '1';

            
          when TDC_0_missing_tr_bits           => local_EB_control.reset_TDC_missing_trailer_bits(0)                            <= '1';
          when TDC_0_k_chars                   => local_EB_control.TDC_Counter_control(0).reset_k_chars                   <= '1';
          when TDC_0_k_chars_3C                => local_EB_control.TDC_Counter_control(0).reset_k_chars_3C                <= '1';
          when TDC_0_k_chars_in_data           => local_EB_control.TDC_Counter_control(0).reset_k_chars_in_data           <= '1';
          when TDC_0_malformed_spills          => local_EB_control.TDC_Counter_control(0).reset_malformed_spills          <= '1';
          when TDC_0_invalid_chars             => local_EB_control.TDC_Counter_control(0).reset_invalid_chars             <= '1';
          when TDC_0_misaligned_syncs          => local_EB_control.TDC_Counter_control(0).reset_misaligned_syncs          <= '1';
          when TDC_0_d_chars                   => local_EB_control.TDC_Counter_control(0).reset_d_chars                   <= '1';
          when TDC_0_lock_timeouts             => local_EB_control.TDC_Counter_control(0).reset_lock_timeouts             <= '1';
--          when TDC_0_multi_valids              => local_EB_control.TDC_Counter_control(0).reset_multi_valids              <= '1';
          when TDC_0_DECODE_FIND_LOCK_time     => local_EB_control.TDC_Counter_control(0).reset_DECODE_FIND_LOCK_time     <= '1';
          when TDC_0_stream_start_end          =>         
            local_EB_control.TDC_Counter_control(0).reset_stream_ends                                                     <= '1';
            local_EB_control.TDC_Counter_control(0).reset_stream_starts                                                   <= '1';  
          when TDC_0_DECODE_LOCKED_time        => local_EB_control.TDC_Counter_control(0).reset_DECODE_LOCKED_time        <= '1';
          when TDC_0_DECODE_PROCESS_SPILL_time => local_EB_control.TDC_Counter_control(0).reset_DECODE_PROCESS_SPILL_time <= '1';
          when TDC_0_k_chars_BC                => local_EB_control.TDC_Counter_control(0).reset_k_chars_BC                <= '1';
          when TDC_0_bad_disparity             => local_EB_control.TDC_Counter_control(0).reset_bad_disparity             <= '1';
          when TDC_0_time                      => local_EB_control.TDC_Counter_control(0).reset_time                      <= '1';
          when TDC_0_CDR_history               =>
--            local_EB_control.TDC_CDR_history_control(0).rd <= local_out(1);
--            local_EB_control.TDC_CDR_history_control(0).reset <= local_out(0);
            local_TDC_CDR_history_reset(0) <= local_in(0);
            local_TDC_CDR_history_rd(0)    <= local_in(1);

                                                
          when TDC_1_missing_tr_bits           => local_EB_control.reset_TDC_missing_trailer_bits(1)                            <= '1';
          when TDC_1_k_chars                   => local_EB_control.TDC_Counter_control(1).reset_k_chars                   <= '1';
          when TDC_1_k_chars_3C                => local_EB_control.TDC_Counter_control(1).reset_k_chars_3C                <= '1';
          when TDC_1_k_chars_in_data           => local_EB_control.TDC_Counter_control(1).reset_k_chars_in_data           <= '1';
          when TDC_1_malformed_spills          => local_EB_control.TDC_Counter_control(1).reset_malformed_spills          <= '1';
          when TDC_1_invalid_chars             => local_EB_control.TDC_Counter_control(1).reset_invalid_chars             <= '1';
          when TDC_1_misaligned_syncs          => local_EB_control.TDC_Counter_control(1).reset_misaligned_syncs          <= '1';
          when TDC_1_d_chars                   => local_EB_control.TDC_Counter_control(1).reset_d_chars                   <= '1';
          when TDC_1_lock_timeouts             => local_EB_control.TDC_Counter_control(1).reset_lock_timeouts             <= '1';
--          when TDC_1_multi_valids              => local_EB_control.TDC_Counter_control(1).reset_multi_valids              <= '1';
          when TDC_1_DECODE_FIND_LOCK_time     => local_EB_control.TDC_Counter_control(1).reset_DECODE_FIND_LOCK_time     <= '1';
          when TDC_1_stream_start_end          =>
            local_EB_control.TDC_Counter_control(1).reset_stream_ends                                                     <= '1';
            local_EB_control.TDC_Counter_control(1).reset_stream_starts                                                   <= '1';  
          when TDC_1_DECODE_LOCKED_time        => local_EB_control.TDC_Counter_control(1).reset_DECODE_LOCKED_time        <= '1';
          when TDC_1_DECODE_PROCESS_SPILL_time => local_EB_control.TDC_Counter_control(1).reset_DECODE_PROCESS_SPILL_time <= '1';
          when TDC_1_k_chars_BC                => local_EB_control.TDC_Counter_control(1).reset_k_chars_BC                <= '1';
          when TDC_1_bad_disparity             => local_EB_control.TDC_Counter_control(1).reset_bad_disparity             <= '1';
          when TDC_1_time                      => local_EB_control.TDC_Counter_control(1).reset_time                      <= '1';
          when TDC_1_CDR_history               =>
--            local_EB_control.TDC_CDR_history_control(0).rd <= local_out(1);
--            local_EB_control.TDC_CDR_history_control(0).reset <= local_out(0);                                                
            local_TDC_CDR_history_reset(1) <= local_in(0);
            local_TDC_CDR_history_rd(1)    <= local_in(1);

          when TDC_2_missing_tr_bits           => local_EB_control.reset_TDC_missing_trailer_bits(2)                            <= '1';
          when TDC_2_k_chars                   => local_EB_control.TDC_Counter_control(2).reset_k_chars                   <= '1';
          when TDC_2_k_chars_3C                => local_EB_control.TDC_Counter_control(2).reset_k_chars_3C                <= '1';
          when TDC_2_k_chars_in_data           => local_EB_control.TDC_Counter_control(2).reset_k_chars_in_data           <= '1';
          when TDC_2_malformed_spills          => local_EB_control.TDC_Counter_control(2).reset_malformed_spills          <= '1';
          when TDC_2_invalid_chars             => local_EB_control.TDC_Counter_control(2).reset_invalid_chars             <= '1';
          when TDC_2_misaligned_syncs          => local_EB_control.TDC_Counter_control(2).reset_misaligned_syncs          <= '1';
          when TDC_2_d_chars                   => local_EB_control.TDC_Counter_control(2).reset_d_chars                   <= '1';
          when TDC_2_lock_timeouts             => local_EB_control.TDC_Counter_control(2).reset_lock_timeouts             <= '1';
--          when TDC_2_multi_valids              => local_EB_control.TDC_Counter_control(2).reset_multi_valids              <= '1';
          when TDC_2_DECODE_FIND_LOCK_time     => local_EB_control.TDC_Counter_control(2).reset_DECODE_FIND_LOCK_time     <= '1';
          when TDC_2_stream_start_end          =>
            local_EB_control.TDC_Counter_control(2).reset_stream_ends                                                     <= '1';
            local_EB_control.TDC_Counter_control(2).reset_stream_starts                                                   <= '1';  
          when TDC_2_DECODE_LOCKED_time        => local_EB_control.TDC_Counter_control(2).reset_DECODE_LOCKED_time        <= '1';
          when TDC_2_DECODE_PROCESS_SPILL_time => local_EB_control.TDC_Counter_control(2).reset_DECODE_PROCESS_SPILL_time <= '1';
          when TDC_2_k_chars_BC                => local_EB_control.TDC_Counter_control(2).reset_k_chars_BC                <= '1';
          when TDC_2_bad_disparity             => local_EB_control.TDC_Counter_control(2).reset_bad_disparity             <= '1';
          when TDC_2_time                      => local_EB_control.TDC_Counter_control(2).reset_time                      <= '1';
          when TDC_2_CDR_history               =>
--            local_EB_control.TDC_CDR_history_control(0).rd <= local_out(1);
--            local_EB_control.TDC_CDR_history_control(0).reset <= local_out(0);
            local_TDC_CDR_history_reset(2) <= local_in(0);
            local_TDC_CDR_history_rd(2)    <= local_in(1);

                                                  
          when TDC_3_missing_tr_bits           => local_EB_control.reset_TDC_missing_trailer_bits(3)                            <= '1';
          when TDC_3_k_chars                   => local_EB_control.TDC_Counter_control(3).reset_k_chars                   <= '1';
          when TDC_3_k_chars_3C                => local_EB_control.TDC_Counter_control(3).reset_k_chars_3C                <= '1';
          when TDC_3_k_chars_in_data           => local_EB_control.TDC_Counter_control(3).reset_k_chars_in_data           <= '1';
          when TDC_3_malformed_spills          => local_EB_control.TDC_Counter_control(3).reset_malformed_spills          <= '1';
          when TDC_3_invalid_chars             => local_EB_control.TDC_Counter_control(3).reset_invalid_chars             <= '1';
          when TDC_3_misaligned_syncs          => local_EB_control.TDC_Counter_control(3).reset_misaligned_syncs          <= '1';
          when TDC_3_d_chars                   => local_EB_control.TDC_Counter_control(3).reset_d_chars                   <= '1';
          when TDC_3_lock_timeouts             => local_EB_control.TDC_Counter_control(3).reset_lock_timeouts             <= '1';
--          when TDC_3_multi_valids              => local_EB_control.TDC_Counter_control(3).reset_multi_valids              <= '1';
          when TDC_3_DECODE_FIND_LOCK_time     => local_EB_control.TDC_Counter_control(3).reset_DECODE_FIND_LOCK_time     <= '1';
          when TDC_3_stream_start_end          =>
            local_EB_control.TDC_Counter_control(3).reset_stream_ends                                                     <= '1';
            local_EB_control.TDC_Counter_control(3).reset_stream_starts                                                   <= '1';  
          when TDC_3_DECODE_LOCKED_time        => local_EB_control.TDC_Counter_control(3).reset_DECODE_LOCKED_time        <= '1';
          when TDC_3_DECODE_PROCESS_SPILL_time => local_EB_control.TDC_Counter_control(3).reset_DECODE_PROCESS_SPILL_time <= '1';
          when TDC_3_k_chars_BC                => local_EB_control.TDC_Counter_control(3).reset_k_chars_BC                <= '1';
          when TDC_3_bad_disparity             => local_EB_control.TDC_Counter_control(3).reset_bad_disparity             <= '1';
          when TDC_3_time                      => local_EB_control.TDC_Counter_control(3).reset_time                      <= '1';
          when TDC_3_CDR_history               =>
--            local_EB_control.TDC_CDR_history_control(0).rd <= local_out(1);
--            local_EB_control.TDC_CDR_history_control(0).reset <= local_out(0);
            local_TDC_CDR_history_reset(3) <= local_in(0);
            local_TDC_CDR_history_rd(3)    <= local_in(1);
            
          when others => null;
        end case;
      end if;
    end if;
  end process action_access;

  -- clocks for running FIFO in CDR history
--  local_EB_control.TDC_CDR_history_control(0).clk <= clk;
--  local_EB_control.TDC_CDR_history_control(1).clk <= clk;
--  local_EB_control.TDC_CDR_history_control(2).clk <= clk;
--  local_EB_control.TDC_CDR_history_control(3).clk <= clk;

  TDC_CDR: for iTDC in 0 to 3 generate
    pacd_1: pacd
      port map (
        iPulseA => local_TDC_CDR_history_reset(iTDC),
        iClkA   => clk,
        iRSTAn  => '1',
        iClkB   => EB_clk,
        iRSTBn  => '1',
        oPulseB => local_EB_control.TDC_CDR_history_control(iTDC).reset);
    pacd_2: pacd
      port map (
        iPulseA => local_TDC_CDR_history_rd(iTDC),
        iClkA   => clk,
        iRSTAn  => '1',
        iClkB   => EB_clk,
        iRSTBn  => '1',
        oPulseB => local_EB_control.TDC_CDR_history_control(iTDC).rd);

  end generate TDC_CDR;
  

  
  EB_reset <= local_EB_reset;
  
  pacd_6: pacd
    port map (
      iPulseA => local_fake_TDC_send_spill,
      iClkA   => clk,
      iRSTAn  => '1',
      iClkB   => EB_clk,
      iRSTBn  => '1',
      oPulseB => control_fake_TDC.send_spill);


  
end architecture arch;
