--
-------------------------------------------------------------------------------------------
-- Copyright � 2010-2013, Xilinx, Inc.
-- This file contains confidential and proprietary information of Xilinx, Inc. and is
-- protected under U.S. and international copyright and other intellectual property laws.
-------------------------------------------------------------------------------------------
--
-- Disclaimer:
-- This disclaimer is not a license and does not grant any rights to the materials
-- distributed herewith. Except as otherwise provided in a valid license issued to
-- you by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE
-- MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY
-- DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
-- INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT,
-- OR FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable
-- (whether in contract or tort, including negligence, or under any other theory
-- of liability) for any loss or damage of any kind or nature related to, arising
-- under or in connection with these materials, including for any direct, or any
-- indirect, special, incidental, or consequential loss or damage (including loss
-- of data, profits, goodwill, or any type of loss or damage suffered as a result
-- of any action brought by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-safe, or for use in any
-- application requiring fail-safe performance, such as life-support or safety
-- devices or systems, Class III medical devices, nuclear facilities, applications
-- related to the deployment of airbags, or any other applications that could lead
-- to death, personal injury, or severe property or environmental damage
-- (individually and collectively, "Critical Applications"). Customer assumes the
-- sole risk and liability of any use of Xilinx products in Critical Applications,
-- subject only to applicable laws and regulations governing limitations on product
-- liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
-------------------------------------------------------------------------------------------
--
--
-- Definition of a program memory for KCPSM6 including generic parameters for the 
-- convenient selection of device family, program memory size and the ability to include 
-- the JTAG Loader hardware for rapid software development.
--
-- This file is primarily for use during code development and it is recommended that the 
-- appropriate simplified program memory definition be used in a final production design. 
--
--    Generic                  Values             Comments
--    Parameter                Supported
--  
--    C_FAMILY                 "S6"               Spartan-6 device
--                             "V6"               Virtex-6 device
--                             "7S"               7-Series device 
--                                                  (Artix-7, Kintex-7, Virtex-7 or Zynq)
--
--    C_RAM_SIZE_KWORDS        1, 2 or 4          Size of program memory in K-instructions
--
--    C_JTAG_LOADER_ENABLE     0 or 1             Set to '1' to include JTAG Loader
--
-- Notes
--
-- If your design contains MULTIPLE KCPSM6 instances then only one should have the 
-- JTAG Loader enabled at a time (i.e. make sure that C_JTAG_LOADER_ENABLE is only set to 
-- '1' on one instance of the program memory). Advanced users may be interested to know 
-- that it is possible to connect JTAG Loader to multiple memories and then to use the 
-- JTAG Loader utility to specify which memory contents are to be modified. However, 
-- this scheme does require some effort to set up and the additional connectivity of the 
-- multiple BRAMs can impact the placement, routing and performance of the complete 
-- design. Please contact the author at Xilinx for more detailed information. 
--
-- Regardless of the size of program memory specified by C_RAM_SIZE_KWORDS, the complete 
-- 12-bit address bus is connected to KCPSM6. This enables the generic to be modified 
-- without requiring changes to the fundamental hardware definition. However, when the 
-- program memory is 1K then only the lower 10-bits of the address are actually used and 
-- the valid address range is 000 to 3FF hex. Likewise, for a 2K program only the lower 
-- 11-bits of the address are actually used and the valid address range is 000 to 7FF hex.
--
-- Programs are stored in Block Memory (BRAM) and the number of BRAM used depends on the 
-- size of the program and the device family. 
--
-- In a Spartan-6 device a BRAM is capable of holding 1K instructions. Hence a 2K program 
-- will require 2 BRAMs to be used and a 4K program will require 4 BRAMs to be used. It 
-- should be noted that a 4K program is not such a natural fit in a Spartan-6 device and 
-- the implementation also requires a small amount of logic resulting in slightly lower 
-- performance. A Spartan-6 BRAM can also be split into two 9k-bit memories suggesting 
-- that a program containing up to 512 instructions could be implemented. However, there 
-- is a silicon errata which makes this unsuitable and therefore it is not supported by 
-- this file.
--
-- In a Virtex-6 or any 7-Series device a BRAM is capable of holding 2K instructions so 
-- obviously a 2K program requires only a single BRAM. Each BRAM can also be divided into 
-- 2 smaller memories supporting programs of 1K in half of a 36k-bit BRAM (generally 
-- reported as being an 18k-bit BRAM). For a program of 4K instructions, 2 BRAMs are used.
--
--
-- Program defined by 'Z:\home\dan\work\g-2\NewElectronics\LogicBoard\firmware\trunk\vhdl\interface_uC\picoblaze\cli.psm'.
--
-- Generated by KCPSM6 Assembler: 06 Jun 2016 - 10:31:28. 
--
-- Assembler used ROM_form template: ROM_form_JTAGLoader_14March13.vhd
--
-- Standard IEEE libraries
--
--
package jtag_loader_pkg is
 function addr_width_calc (size_in_k: integer) return integer;
end jtag_loader_pkg;
--
package body jtag_loader_pkg is
  function addr_width_calc (size_in_k: integer) return integer is
   begin
    if (size_in_k = 1) then return 10;
      elsif (size_in_k = 2) then return 11;
      elsif (size_in_k = 4) then return 12;
      else report "Invalid BlockRAM size. Please set to 1, 2 or 4 K words." severity FAILURE;
    end if;
    return 0;
  end function addr_width_calc;
end package body;
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.jtag_loader_pkg.ALL;
--
-- The Unisim Library is used to define Xilinx primitives. It is also used during
-- simulation. The source can be viewed at %XILINX%\vhdl\src\unisims\unisim_VCOMP.vhd
--  
library unisim;
use unisim.vcomponents.all;
--
--
entity cli is
  generic(             C_FAMILY : string := "S6"; 
              C_RAM_SIZE_KWORDS : integer := 1;
           C_JTAG_LOADER_ENABLE : integer := 0);
  Port (      address : in std_logic_vector(11 downto 0);
          instruction : out std_logic_vector(17 downto 0);
               enable : in std_logic;
                  rdl : out std_logic;                    
                  clk : in std_logic);
  end cli;
--
architecture low_level_definition of cli is
--
signal       address_a : std_logic_vector(15 downto 0);
signal        pipe_a11 : std_logic;
signal       data_in_a : std_logic_vector(35 downto 0);
signal      data_out_a : std_logic_vector(35 downto 0);
signal    data_out_a_l : std_logic_vector(35 downto 0);
signal    data_out_a_h : std_logic_vector(35 downto 0);
signal   data_out_a_ll : std_logic_vector(35 downto 0);
signal   data_out_a_lh : std_logic_vector(35 downto 0);
signal   data_out_a_hl : std_logic_vector(35 downto 0);
signal   data_out_a_hh : std_logic_vector(35 downto 0);
signal       address_b : std_logic_vector(15 downto 0);
signal       data_in_b : std_logic_vector(35 downto 0);
signal     data_in_b_l : std_logic_vector(35 downto 0);
signal    data_in_b_ll : std_logic_vector(35 downto 0);
signal    data_in_b_hl : std_logic_vector(35 downto 0);
signal      data_out_b : std_logic_vector(35 downto 0);
signal    data_out_b_l : std_logic_vector(35 downto 0);
signal   data_out_b_ll : std_logic_vector(35 downto 0);
signal   data_out_b_hl : std_logic_vector(35 downto 0);
signal     data_in_b_h : std_logic_vector(35 downto 0);
signal    data_in_b_lh : std_logic_vector(35 downto 0);
signal    data_in_b_hh : std_logic_vector(35 downto 0);
signal    data_out_b_h : std_logic_vector(35 downto 0);
signal   data_out_b_lh : std_logic_vector(35 downto 0);
signal   data_out_b_hh : std_logic_vector(35 downto 0);
signal        enable_b : std_logic;
signal           clk_b : std_logic;
signal            we_b : std_logic_vector(7 downto 0);
signal          we_b_l : std_logic_vector(3 downto 0);
signal          we_b_h : std_logic_vector(3 downto 0);
-- 
signal       jtag_addr : std_logic_vector(11 downto 0);
signal         jtag_we : std_logic;
signal       jtag_we_l : std_logic;
signal       jtag_we_h : std_logic;
signal        jtag_clk : std_logic;
signal        jtag_din : std_logic_vector(17 downto 0);
signal       jtag_dout : std_logic_vector(17 downto 0);
signal     jtag_dout_1 : std_logic_vector(17 downto 0);
signal         jtag_en : std_logic_vector(0 downto 0);
-- 
signal picoblaze_reset : std_logic_vector(0 downto 0);
signal         rdl_bus : std_logic_vector(0 downto 0);
--
constant BRAM_ADDRESS_WIDTH  : integer := addr_width_calc(C_RAM_SIZE_KWORDS);
--
--
component jtag_loader_6
generic(                C_JTAG_LOADER_ENABLE : integer := 1;
                                    C_FAMILY : string  := "V6";
                             C_NUM_PICOBLAZE : integer := 1;
                       C_BRAM_MAX_ADDR_WIDTH : integer := 10;
          C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                                C_JTAG_CHAIN : integer := 2;
                              C_ADDR_WIDTH_0 : integer := 10;
                              C_ADDR_WIDTH_1 : integer := 10;
                              C_ADDR_WIDTH_2 : integer := 10;
                              C_ADDR_WIDTH_3 : integer := 10;
                              C_ADDR_WIDTH_4 : integer := 10;
                              C_ADDR_WIDTH_5 : integer := 10;
                              C_ADDR_WIDTH_6 : integer := 10;
                              C_ADDR_WIDTH_7 : integer := 10);
port(              picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                           jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                          jtag_din : out STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                         jtag_addr : out STD_LOGIC_VECTOR(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
                          jtag_clk : out std_logic;
                           jtag_we : out std_logic;
                       jtag_dout_0 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_1 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_2 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_3 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_4 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_5 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_6 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_7 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end component;
--
begin
  --
  --  
  ram_1k_generate : if (C_RAM_SIZE_KWORDS = 1) generate
 
    s6: if (C_FAMILY = "S6") generate 
      --
      address_a(13 downto 0) <= address(9 downto 0) & "0000";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "0000000000000000000000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "0000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB16BWER
      generic map ( DATA_WIDTH_A => 18,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 18,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D00490002C110081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"F020100050000060013AE0879201E087930112FF13FFB001B031033E008E5000",
                    INIT_09 => X"103A0002005300410030130050000059E100B130B02C607CD002B0235000F023",
                    INIT_0A => X"60A5D00030030030D1010002D20100020041A03093011304D00110200002D001",
                    INIT_0B => X"0BE50B960210B12BB227A0C1D004B023017E50000059209B20B5D30013040059",
                    INIT_0C => X"5000970896079506940520C8D010900331FFD1031108D004500000815000017E",
                    INIT_0D => X"ACC01C2C1D01607CD00120E1D002B0235000D10311F4D708D607D506D405D004",
                    INIT_0E => X"D00110780002D0011030000200C400C01D0160E8DD00ADD01D30ACC01C2C20E8",
                    INIT_0F => X"00600002005300700002D00110780002D00110300002D00110200002005300C0",
                    INIT_10 => X"1030607CD002B023500060E89D011C0100590053004000020053005000020053",
                    INIT_11 => X"1167500000D0A000102C0710A10010010610A10010010510A10010010410A100",
                    INIT_12 => X"116311691167116F114C112011721165116B116311611172115411201132112D",
                    INIT_13 => X"007000C4100100641A1F1B0111001120113A11561120116411721161116F1142",
                    INIT_14 => X"00530040D001102E000200530050D001102E000200530060D001102E00020053",
                    INIT_15 => X"112011641172116F115711001120113A11721165116611661175114250000059",
                    INIT_16 => X"117A11691173112011641172116F115711001120113A1174116E1175116F1163",
                    INIT_17 => X"1A521B011100112011641172116F115711001120112011201120113A11731165",
                    INIT_18 => X"D00110280002D0011020000221831101D001A0100002E18AC120B22011000064",
                    INIT_19 => X"0041B023000200641A5B1B010059D00110290002D1010002D201000200410020",
                    INIT_1A => X"01301124D00110400002D00110200002E1B8C3401300B423D1010002D2010002",
                    INIT_1B => X"E1CDC3401300B42300641A681B01005921A61301D1010002D20100020041A010",
                    INIT_1C => X"1300B423005921BE1301D1010002D20100020041A01001301128D00110200002",
                    INIT_1D => X"0530D00110200002D1010002D20100020041003000641A781B010059E1F0C340",
                    INIT_1E => X"21E69601D1010002D20100020041A060A1D0C65016030650152C450645061301",
                    INIT_1F => X"1A03005921F43B001A01D10100022202D1FF21FED1004BA01A9A1B0950000059",
                    INIT_20 => X"9E011E3E50007000DD024D003DF0400740073003700160005000005921F43B00",
                    INIT_21 => X"DD025D025000DD023DFE020E020E020E020EDD023DFDDD025D025D015000E20F",
                    INIT_22 => X"020E020E020EDD023DFE020E020E020EDD023DFD020E020EDD025D01020E020E",
                    INIT_23 => X"020E5000DD025D02020E020E020E020EDD025D01020E020E020EDD023DFD5000",
                    INIT_24 => X"5D01020EDD023DFD5000020E020E020EDD023DFE020E020EDD025D01DD025D02",
                    INIT_25 => X"3DFDA2624406020E15805000020E020EDD025D02020EDD023DFE020E020EDD02",
                    INIT_26 => X"5D02020E225CA26E450EDD023DFE020E020EDD025D01020EDD025D022264DD02",
                    INIT_27 => X"14005D025000D502020E020E020EDD023DFE020E020EDD025D019502020EDD02",
                    INIT_28 => X"5000020EE281450EDD023DFE020E020EDD025D01020E4400D6029602020E1580",
                    INIT_29 => X"A2AC025B0420A2AA025B0410A2A8025B0400021230FE70016330622061106000",
                    INIT_2A => X"22B0100422B0100322B0100222B0100150007000400610000231A2AE025B0430",
                    INIT_2B => X"0410A2CA025B0400021230FE7001622061106000500070004006108002310053",
                    INIT_2C => X"22D0100322D0100222D0100150007000400610000231A2CE025B0420A2CC025B",
                    INIT_2D => X"025B0410A2F3025B0400021230FE700161106000500070004006108002310053",
                    INIT_2E => X"6220400610000231023F0340027E024C0240027EA2F7025B04005001021EA2AA",
                    INIT_2F => X"114150007000400610800231005322F9100322F9100222F91001500070006330",
                    INIT_30 => X"B0235000005900641AFF1B021100112E1172116F1172117211651120116B1163",
                    INIT_31 => X"A323D503A2401401A340A31DD503A550152A1434A1401430A040142C607CD003",
                    INIT_32 => X"0020A30A02D6A1401430A040142C607CD002B0235000A30A02B65000A30A0290",
                    INIT_33 => X"6D0010F350000204100250000204100150000204100050000059005300300053",
                    INIT_34 => X"1174116E1165116D11751167117211611120116411611142500003350338033B",
                    INIT_35 => X"1DFF5000D003301FA040142C6352D001B0235000005900641A441B0311001173",
                    INIT_36 => X"00C410B2303040064006400640065000E3629D01E3629E01E3629F011FFF1EFF",
                    INIT_37 => X"9400005900530040005300500053006000530070D00110200002D00110490002",
                    INIT_38 => X"104B000200C41052303040064006400640065000C54015809000B700B607B589",
                    INIT_39 => X"B607B5899400005900530040005300500053006000530070D00110200002D001",
                    INIT_3A => X"0002D001104C000200C410B0303040064006400640065000C54015809000B700",
                    INIT_3B => X"9000B700B607B5899400005900530040005300500053006000530070D0011020",
                    INIT_3C => X"D00110200002D001105A000200C40020303040064006400640065000C5401580",
                    INIT_3D => X"9700100096001000950010009400005900530040005300500053006000530070",
                    INIT_3E => X"40064006400600B00059005300C00002D001105000021A000B001C0150001000",
                    INIT_3F => X"400640064006400600B04410348F3170410641064106410601C000C410504006",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"28A28A288A2834020374A2AA803022AAA434AAD6D0AA858A2A0A834A8AB742AA",
                   INITP_02 => X"0AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAAAAAAAAA801041034B5A28A",
                   INITP_03 => X"696B760A9AA8D45522AA882D0A6AA122D0829AA848A2D0AA8A0A2AA88A298B42",
                   INITP_04 => X"08AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA2A8A8A8A2AA20B4B814DA9",
                   INITP_05 => X"AD2A222D528A2E0B8E235B4A888B4B8E388D5B4A8888B4B8E38E2355AD8A8908",
                   INITP_06 => X"688888A29156DDD0280D2A0AAAAAAAAA4A28A2A238034BAED134000D2A0AAAAA",
                   INITP_07 => X"5500552554A2880B777688888A291564D5688888A291564D5688888A291564D5")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a(31 downto 0),
                  DOPA => data_out_a(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b(31 downto 0),
                  DOPB => data_out_b(35 downto 32), 
                   DIB => data_in_b(31 downto 0),
                  DIPB => data_in_b(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --               
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D00490002C110081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"F020100050000060013AE0879201E087930112FF13FFB001B031033E008E5000",
                    INIT_09 => X"103A0002005300410030130050000059E100B130B02C607CD002B0235000F023",
                    INIT_0A => X"60A5D00030030030D1010002D20100020041A03093011304D00110200002D001",
                    INIT_0B => X"0BE50B960210B12BB227A0C1D004B023017E50000059209B20B5D30013040059",
                    INIT_0C => X"5000970896079506940520C8D010900331FFD1031108D004500000815000017E",
                    INIT_0D => X"ACC01C2C1D01607CD00120E1D002B0235000D10311F4D708D607D506D405D004",
                    INIT_0E => X"D00110780002D0011030000200C400C01D0160E8DD00ADD01D30ACC01C2C20E8",
                    INIT_0F => X"00600002005300700002D00110780002D00110300002D00110200002005300C0",
                    INIT_10 => X"1030607CD002B023500060E89D011C0100590053004000020053005000020053",
                    INIT_11 => X"1167500000D0A000102C0710A10010010610A10010010510A10010010410A100",
                    INIT_12 => X"116311691167116F114C112011721165116B116311611172115411201132112D",
                    INIT_13 => X"007000C4100100641A1F1B0111001120113A11561120116411721161116F1142",
                    INIT_14 => X"00530040D001102E000200530050D001102E000200530060D001102E00020053",
                    INIT_15 => X"112011641172116F115711001120113A11721165116611661175114250000059",
                    INIT_16 => X"117A11691173112011641172116F115711001120113A1174116E1175116F1163",
                    INIT_17 => X"1A521B011100112011641172116F115711001120112011201120113A11731165",
                    INIT_18 => X"D00110280002D0011020000221831101D001A0100002E18AC120B22011000064",
                    INIT_19 => X"0041B023000200641A5B1B010059D00110290002D1010002D201000200410020",
                    INIT_1A => X"01301124D00110400002D00110200002E1B8C3401300B423D1010002D2010002",
                    INIT_1B => X"E1CDC3401300B42300641A681B01005921A61301D1010002D20100020041A010",
                    INIT_1C => X"1300B423005921BE1301D1010002D20100020041A01001301128D00110200002",
                    INIT_1D => X"0530D00110200002D1010002D20100020041003000641A781B010059E1F0C340",
                    INIT_1E => X"21E69601D1010002D20100020041A060A1D0C65016030650152C450645061301",
                    INIT_1F => X"1A03005921F43B001A01D10100022202D1FF21FED1004BA01A9A1B0950000059",
                    INIT_20 => X"9E011E3E50007000DD024D003DF0400740073003700160005000005921F43B00",
                    INIT_21 => X"DD025D025000DD023DFE020E020E020E020EDD023DFDDD025D025D015000E20F",
                    INIT_22 => X"020E020E020EDD023DFE020E020E020EDD023DFD020E020EDD025D01020E020E",
                    INIT_23 => X"020E5000DD025D02020E020E020E020EDD025D01020E020E020EDD023DFD5000",
                    INIT_24 => X"5D01020EDD023DFD5000020E020E020EDD023DFE020E020EDD025D01DD025D02",
                    INIT_25 => X"3DFDA2624406020E15805000020E020EDD025D02020EDD023DFE020E020EDD02",
                    INIT_26 => X"5D02020E225CA26E450EDD023DFE020E020EDD025D01020EDD025D022264DD02",
                    INIT_27 => X"14005D025000D502020E020E020EDD023DFE020E020EDD025D019502020EDD02",
                    INIT_28 => X"5000020EE281450EDD023DFE020E020EDD025D01020E4400D6029602020E1580",
                    INIT_29 => X"A2AC025B0420A2AA025B0410A2A8025B0400021230FE70016330622061106000",
                    INIT_2A => X"22B0100422B0100322B0100222B0100150007000400610000231A2AE025B0430",
                    INIT_2B => X"0410A2CA025B0400021230FE7001622061106000500070004006108002310053",
                    INIT_2C => X"22D0100322D0100222D0100150007000400610000231A2CE025B0420A2CC025B",
                    INIT_2D => X"025B0410A2F3025B0400021230FE700161106000500070004006108002310053",
                    INIT_2E => X"6220400610000231023F0340027E024C0240027EA2F7025B04005001021EA2AA",
                    INIT_2F => X"114150007000400610800231005322F9100322F9100222F91001500070006330",
                    INIT_30 => X"B0235000005900641AFF1B021100112E1172116F1172117211651120116B1163",
                    INIT_31 => X"A323D503A2401401A340A31DD503A550152A1434A1401430A040142C607CD003",
                    INIT_32 => X"0020A30A02D6A1401430A040142C607CD002B0235000A30A02B65000A30A0290",
                    INIT_33 => X"6D0010F350000204100250000204100150000204100050000059005300300053",
                    INIT_34 => X"1174116E1165116D11751167117211611120116411611142500003350338033B",
                    INIT_35 => X"1DFF5000D003301FA040142C6352D001B0235000005900641A441B0311001173",
                    INIT_36 => X"00C410B2303040064006400640065000E3629D01E3629E01E3629F011FFF1EFF",
                    INIT_37 => X"9400005900530040005300500053006000530070D00110200002D00110490002",
                    INIT_38 => X"104B000200C41052303040064006400640065000C54015809000B700B607B589",
                    INIT_39 => X"B607B5899400005900530040005300500053006000530070D00110200002D001",
                    INIT_3A => X"0002D001104C000200C410B0303040064006400640065000C54015809000B700",
                    INIT_3B => X"9000B700B607B5899400005900530040005300500053006000530070D0011020",
                    INIT_3C => X"D00110200002D001105A000200C40020303040064006400640065000C5401580",
                    INIT_3D => X"9700100096001000950010009400005900530040005300500053006000530070",
                    INIT_3E => X"40064006400600B00059005300C00002D001105000021A000B001C0150001000",
                    INIT_3F => X"400640064006400600B04410348F3170410641064106410601C000C410504006",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"28A28A288A2834020374A2AA803022AAA434AAD6D0AA858A2A0A834A8AB742AA",
                   INITP_02 => X"0AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAAAAAAAAA801041034B5A28A",
                   INITP_03 => X"696B760A9AA8D45522AA882D0A6AA122D0829AA848A2D0AA8A0A2AA88A298B42",
                   INITP_04 => X"08AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA2A8A8A8A2AA20B4B814DA9",
                   INITP_05 => X"AD2A222D528A2E0B8E235B4A888B4B8E388D5B4A8888B4B8E38E2355AD8A8908",
                   INITP_06 => X"688888A29156DDD0280D2A0AAAAAAAAA4A28A2A238034BAED134000D2A0AAAAA",
                   INITP_07 => X"5500552554A2880B777688888A291564D5688888A291564D5688888A291564D5")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D00490002C110081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"F020100050000060013AE0879201E087930112FF13FFB001B031033E008E5000",
                    INIT_09 => X"103A0002005300410030130050000059E100B130B02C607CD002B0235000F023",
                    INIT_0A => X"60A5D00030030030D1010002D20100020041A03093011304D00110200002D001",
                    INIT_0B => X"0BE50B960210B12BB227A0C1D004B023017E50000059209B20B5D30013040059",
                    INIT_0C => X"5000970896079506940520C8D010900331FFD1031108D004500000815000017E",
                    INIT_0D => X"ACC01C2C1D01607CD00120E1D002B0235000D10311F4D708D607D506D405D004",
                    INIT_0E => X"D00110780002D0011030000200C400C01D0160E8DD00ADD01D30ACC01C2C20E8",
                    INIT_0F => X"00600002005300700002D00110780002D00110300002D00110200002005300C0",
                    INIT_10 => X"1030607CD002B023500060E89D011C0100590053004000020053005000020053",
                    INIT_11 => X"1167500000D0A000102C0710A10010010610A10010010510A10010010410A100",
                    INIT_12 => X"116311691167116F114C112011721165116B116311611172115411201132112D",
                    INIT_13 => X"007000C4100100641A1F1B0111001120113A11561120116411721161116F1142",
                    INIT_14 => X"00530040D001102E000200530050D001102E000200530060D001102E00020053",
                    INIT_15 => X"112011641172116F115711001120113A11721165116611661175114250000059",
                    INIT_16 => X"117A11691173112011641172116F115711001120113A1174116E1175116F1163",
                    INIT_17 => X"1A521B011100112011641172116F115711001120112011201120113A11731165",
                    INIT_18 => X"D00110280002D0011020000221831101D001A0100002E18AC120B22011000064",
                    INIT_19 => X"0041B023000200641A5B1B010059D00110290002D1010002D201000200410020",
                    INIT_1A => X"01301124D00110400002D00110200002E1B8C3401300B423D1010002D2010002",
                    INIT_1B => X"E1CDC3401300B42300641A681B01005921A61301D1010002D20100020041A010",
                    INIT_1C => X"1300B423005921BE1301D1010002D20100020041A01001301128D00110200002",
                    INIT_1D => X"0530D00110200002D1010002D20100020041003000641A781B010059E1F0C340",
                    INIT_1E => X"21E69601D1010002D20100020041A060A1D0C65016030650152C450645061301",
                    INIT_1F => X"1A03005921F43B001A01D10100022202D1FF21FED1004BA01A9A1B0950000059",
                    INIT_20 => X"9E011E3E50007000DD024D003DF0400740073003700160005000005921F43B00",
                    INIT_21 => X"DD025D025000DD023DFE020E020E020E020EDD023DFDDD025D025D015000E20F",
                    INIT_22 => X"020E020E020EDD023DFE020E020E020EDD023DFD020E020EDD025D01020E020E",
                    INIT_23 => X"020E5000DD025D02020E020E020E020EDD025D01020E020E020EDD023DFD5000",
                    INIT_24 => X"5D01020EDD023DFD5000020E020E020EDD023DFE020E020EDD025D01DD025D02",
                    INIT_25 => X"3DFDA2624406020E15805000020E020EDD025D02020EDD023DFE020E020EDD02",
                    INIT_26 => X"5D02020E225CA26E450EDD023DFE020E020EDD025D01020EDD025D022264DD02",
                    INIT_27 => X"14005D025000D502020E020E020EDD023DFE020E020EDD025D019502020EDD02",
                    INIT_28 => X"5000020EE281450EDD023DFE020E020EDD025D01020E4400D6029602020E1580",
                    INIT_29 => X"A2AC025B0420A2AA025B0410A2A8025B0400021230FE70016330622061106000",
                    INIT_2A => X"22B0100422B0100322B0100222B0100150007000400610000231A2AE025B0430",
                    INIT_2B => X"0410A2CA025B0400021230FE7001622061106000500070004006108002310053",
                    INIT_2C => X"22D0100322D0100222D0100150007000400610000231A2CE025B0420A2CC025B",
                    INIT_2D => X"025B0410A2F3025B0400021230FE700161106000500070004006108002310053",
                    INIT_2E => X"6220400610000231023F0340027E024C0240027EA2F7025B04005001021EA2AA",
                    INIT_2F => X"114150007000400610800231005322F9100322F9100222F91001500070006330",
                    INIT_30 => X"B0235000005900641AFF1B021100112E1172116F1172117211651120116B1163",
                    INIT_31 => X"A323D503A2401401A340A31DD503A550152A1434A1401430A040142C607CD003",
                    INIT_32 => X"0020A30A02D6A1401430A040142C607CD002B0235000A30A02B65000A30A0290",
                    INIT_33 => X"6D0010F350000204100250000204100150000204100050000059005300300053",
                    INIT_34 => X"1174116E1165116D11751167117211611120116411611142500003350338033B",
                    INIT_35 => X"1DFF5000D003301FA040142C6352D001B0235000005900641A441B0311001173",
                    INIT_36 => X"00C410B2303040064006400640065000E3629D01E3629E01E3629F011FFF1EFF",
                    INIT_37 => X"9400005900530040005300500053006000530070D00110200002D00110490002",
                    INIT_38 => X"104B000200C41052303040064006400640065000C54015809000B700B607B589",
                    INIT_39 => X"B607B5899400005900530040005300500053006000530070D00110200002D001",
                    INIT_3A => X"0002D001104C000200C410B0303040064006400640065000C54015809000B700",
                    INIT_3B => X"9000B700B607B5899400005900530040005300500053006000530070D0011020",
                    INIT_3C => X"D00110200002D001105A000200C40020303040064006400640065000C5401580",
                    INIT_3D => X"9700100096001000950010009400005900530040005300500053006000530070",
                    INIT_3E => X"40064006400600B00059005300C00002D001105000021A000B001C0150001000",
                    INIT_3F => X"400640064006400600B04410348F3170410641064106410601C000C410504006",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"28A28A288A2834020374A2AA803022AAA434AAD6D0AA858A2A0A834A8AB742AA",
                   INITP_02 => X"0AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAAAAAAAAA801041034B5A28A",
                   INITP_03 => X"696B760A9AA8D45522AA882D0A6AA122D0829AA848A2D0AA8A0A2AA88A298B42",
                   INITP_04 => X"08AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA2A8A8A8A2AA20B4B814DA9",
                   INITP_05 => X"AD2A222D528A2E0B8E235B4A888B4B8E388D5B4A8888B4B8E38E2355AD8A8908",
                   INITP_06 => X"688888A29156DDD0280D2A0AAAAAAAAA4A28A2A238034BAED134000D2A0AAAAA",
                   INITP_07 => X"5500552554A2880B777688888A291564D5688888A291564D5688888A291564D5")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate akv7;
    --
  end generate ram_1k_generate;
  --
  --
  --
  ram_2k_generate : if (C_RAM_SIZE_KWORDS = 2) generate
    --
    --
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004001181",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"3A0253413000005900302C7C02230023200000603A87018701FFFF01313E8E00",
                    INIT_05 => X"E596102B27C104237E00599BB5000459A5000330010201024130010401200201",
                    INIT_06 => X"C02C017C01E102230003F408070605040008070605C81003FF0308040081007E",
                    INIT_07 => X"600253700201780201300201200253C0017802013002C4C001E800D030C02CE8",
                    INIT_08 => X"6700D0002C1000011000011000011000307C022300E801015953400253500253",
                    INIT_09 => X"70C401641F0100203A56206472616F426369676F4C2072656B6361725420322D",
                    INIT_0A => X"2064726F5700203A72656666754200595340012E025350012E025360012E0253",
                    INIT_0B => X"5201002064726F5700202020203A73657A69732064726F5700203A746E756F63",
                    INIT_0C => X"412302645B015901290201020102412001280201200283010110028A20200064",
                    INIT_0D => X"CD40002364680159A6010102010241103024014002012002B840002301020102",
                    INIT_0E => X"3001200201020102413064780159F040002359BE010102010241103028012002",
                    INIT_0F => X"0359F40001010202FFFE00A09A090059E601010201024160D05003502C060601",
                    INIT_10 => X"02020002FE0E0E0E0E02FD020201000F013E00000200F007070301000059F400",
                    INIT_11 => X"0E0002020E0E0E0E02010E0E0E02FD000E0E0E02FE0E0E0E02FD0E0E02010E0E",
                    INIT_12 => X"FD62060E80000E0E02020E02FE0E0E02010E02FD000E0E0E02FE0E0E02010202",
                    INIT_13 => X"000200020E0E0E02FE0E0E0201020E02020E5C6E0E02FE0E0E02010E02026402",
                    INIT_14 => X"AC5B20AA5B10A85B0012FE0130201000000E810E02FE0E0E02010E0002020E80",
                    INIT_15 => X"10CA5B0012FE01201000000006803153B004B003B002B0010000060031AE5B30",
                    INIT_16 => X"5B10F35B0012FE011000000006803153D003D002D0010000060031CE5B20CC5B",
                    INIT_17 => X"41000006803153F903F902F901000030200600313F407E4C407EF75B00011EAA",
                    INIT_18 => X"23034001401D03502A344030402C7C0323005964FF02002E726F727265206B63",
                    INIT_19 => X"00F30004020004010004000059533053200AD64030402C7C0223000AB6000A90",
                    INIT_1A => X"FF00031F402C52012300596444030073746E656D75677261206461420035383B",
                    INIT_1B => X"00595340535053605370012002014902C4B2300606060600620162016201FFFF",
                    INIT_1C => X"078900595340535053605370012002014B02C452300606060600408000000789",
                    INIT_1D => X"0000078900595340535053605370012002014C02C4B030060606060040800000",
                    INIT_1E => X"00000000000000595340535053605370012002015A02C4203006060606004080",
                    INIT_1F => X"06060606B0108F7006060606C0C45006060606B05953C0020150020000010000",
                    INIT_20 => X"06060606B0FC5FD0503006060606B0035FC45006060606B0D00000000120D050",
                    INIT_21 => X"69B087B0A5B0595340535053605370012002015402C4B43006060606B0D05030",
                    INIT_22 => X"06A01F1FE5060107590659C357B0590652C358B059064CC356B0590646C3B5B0",
                    INIT_23 => X"06060759531010007305790E01000E0E66E0020F6D0E6C10A000070000E00606",
                    INIT_24 => X"02A104C440E2019B02C440E2009501C440005959D0C07FC45006060606B00606",
                    INIT_25 => X"01C30209FFFF000920AC0AB20109000900AD01FF00AC098000E203A708C440E2",
                    INIT_26 => X"5040E7BA00E5BA00E3BAB2100F060606060607A8000E0100060009100ABCC900",
                    INIT_27 => X"53B00201023A01025359010201023A01025201025201024533E832E831000E00",
                    INIT_28 => X"010252010252010245000E0017BAB220B230B2100F060606060606A8000E0159",
                    INIT_29 => X"0B208000474A0134020B00000B0208003C01000E015953B00201023A01025359",
                    INIT_2A => X"0B0B20C059200B0055020B0B020C4F020B5901000E01000E004A1040200B0000",
                    INIT_2B => X"8501800084760E08850080007C0100716C0EA7018071660EA700806B01005F20",
                    INIT_2C => X"200B0B0B0B40200095200B000E8E020B0B0B0B04020087020B950100847E0E08",
                    INIT_2D => X"0606000E00B8200B00B2020B0B0B0B0002000E00A9020BB801000E0E0E0E0E9C",
                    INIT_2E => X"30102C7C0223005953BA00B2102C7C0123005953B000C5200B0B0B0B00200606",
                    INIT_2F => X"20656369766564206F4E00041030102C100110347C0323005953305340E8CC10",
                    INIT_30 => X"59014502014E02014F02014E02212E102C7C0123005964F6055900646E756F66",
                    INIT_31 => X"107230633330672E30102C7C01230059014402014E02015502014F0201460200",
                    INIT_32 => X"6053705380539053A053B0107230107230107230107230107230107230107230",
                    INIT_33 => X"7C0123006340301030102C7C0223005901520201520201450200595340535053",
                    INIT_34 => X"0810297C032300594D3063443063CC30062E30102C7C0123005953107230102C",
                    INIT_35 => X"40303063304001633040016330400163403034635530062E30102C7C08102A7C",
                    INIT_36 => X"3063CC30062E30102C7C012300594D3063443063304001633040016330400163",
                    INIT_37 => X"635530062E30102C7C08102A7C0810297C0323005953405310723010723063BE",
                    INIT_38 => X"6330400163304001633040016340303063304001633040016330400163403034",
                    INIT_39 => X"85200063F0200001062E209F00102C7C0123005953405310723010723063BE30",
                    INIT_3A => X"A720605D30005D305D000076005A305D0151305D000E0E004D006B0300852000",
                    INIT_3B => X"070130A00E0E0E01304000354D20B0A0B15930207575B1596C00403D41018860",
                    INIT_3C => X"0000B010D0950106079B00FEB0070130A00E0E0E01304001000E810E018600B0",
                    INIT_3D => X"102C7C01230059B4010153C00707400000010001000100010001000100010040",
                    INIT_3E => X"102C4A7C0123CDD51D010600590142014C0D00D50D020100CD01D50D0100C600",
                    INIT_3F => X"000300102C0F4A7C0123F500590142014C0D00F50D020100CD01F50D0100E600",
                   INITP_00 => X"000000B8207450E1D086083004C008A41FFFE536008278000000008F80000000",
                   INITP_01 => X"34F4A08F880B9C18E2E0C0E80420031A7FFFFFFFFFFC000007FFFFFF86590200",
                   INITP_02 => X"800104000080000000800000000A1CC1519D8E6D88D9B0CF30C618CCD87C0E03",
                   INITP_03 => X"01F80008880000035000000D40000035000000EE8207FFF780005012CFA007FF",
                   INITP_04 => X"16AB6DA841080030B58C334104100000014C00C008104104A800000006018050",
                   INITP_05 => X"FFDDD005A0040200144029010084024D9D99A692088240500D00985AB6880806",
                   INITP_06 => X"CB11004B6582D5559555996220B6580B0CE000000019659659600000000280BF",
                   INITP_07 => X"8C27FFBE43C7FFBE4188AAAA85D08375105AAEFFB29776FDB95A0096AAACAAAC")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_l(31 downto 0),
                  DOPA => data_out_a_l(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_l(31 downto 0),
                  DOPB => data_out_b_l(35 downto 32), 
                   DIB => data_in_b_l(31 downto 0),
                  DIPB => data_in_b_l(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_h: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481600",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"0800000000092800705858B0E85828787808280000F0C9F0C909095858010028",
                    INIT_05 => X"0505815859D0E8580028001090E98900B0E81800680069000050C98968080068",
                    INIT_06 => X"560E0EB0E890E8582868086B6B6A6A68284B4B4A4A9068481868086828002800",
                    INIT_07 => X"0000000000680800680800680800000068080068080000000EB0EE560E560E10",
                    INIT_08 => X"0828005008035088035088025088025008B0E85828B0CE8E0000000000000000",
                    INIT_09 => X"000008000D0D0808080808080808080808080808080808080808080808080808",
                    INIT_0A => X"0808080808080808080808080808280000006808000000680800000068080000",
                    INIT_0B => X"0D0D080808080808080808080808080808080808080808080808080808080808",
                    INIT_0C => X"005800000D0D006808006800690000006808006808001088685000F0E0590800",
                    INIT_0D => X"F0E1095A000D0D0010896800690000508008680800680800F0E1095A68006900",
                    INIT_0E => X"02680800680069000000000D0D00F0E1095A0010896800690000508008680800",
                    INIT_0F => X"8D00109D8D680091E890E8250D0D280010CB680069000050D0E38B038AA2A289",
                    INIT_10 => X"6E2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A018B8B02800109D",
                    INIT_11 => X"01286E2E010101016E2E0101016E1E280101016E1E0101016E1E01016E2E0101",
                    INIT_12 => X"1ED1A2010A2801016E2E016E1E01016E2E016E1E280101016E1E01016E2E6E2E",
                    INIT_13 => X"0A2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E01016E2E016E2E116E",
                    INIT_14 => X"D10102D10102D101020118B8B1B1B0B02801F1A26E1E01016E2E01A26B4B010A",
                    INIT_15 => X"02D101020118B8B1B0B028B8A0080100110811081108110828B8A00801D10102",
                    INIT_16 => X"0102D101020118B8B0B028B8A008010011081108110828B8A00801D10102D101",
                    INIT_17 => X"0828B8A008010011081108110828B8B1B1A00801010101010101D101022801D1",
                    INIT_18 => X"D1EA518A51D1EA520A0A500A500AB0E8582800000D0D08080808080808080808",
                    INIT_19 => X"B608280108280108280108280000000000D101500A500AB0E85828D10128D101",
                    INIT_1A => X"0E286818500AB1E8582800000D0D080808080808080808080808080828010101",
                    INIT_1B => X"CA000000000000000000680800680800008818A0A0A0A028F1CEF1CFF1CF0F0F",
                    INIT_1C => X"DBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDBDA",
                    INIT_1D => X"C8DBDBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DB",
                    INIT_1E => X"CB88CB88CA88CA000000000000000000680800680800008018A0A0A0A028E20A",
                    INIT_1F => X"A0A0A0A000221A18A0A0A0A0000088A0A0A0A000000000006808000D050E2888",
                    INIT_20 => X"A0A0A0A0001A01008818A0A0A0A0002A010088A0A0A0A000000B0B0A0A080088",
                    INIT_21 => X"010001000100000000000000000000680800680800008818A0A0A0A000008818",
                    INIT_22 => X"A0001DEDB1EE8EA512A59201090012A59201090012A59201090012A592010900",
                    INIT_23 => X"A6A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E0100008092518A0A0",
                    INIT_24 => X"08926A00080108926A00080108926A000828000000121A0088A0A0A0A000A6A6",
                    INIT_25 => X"CFB218480F0F2868080268921848284828F2CF0F28026F0F280108926A000801",
                    INIT_26 => X"0201D20202D20202D202022018A0A0A0A0A0A00228A10928A1096808481292DF",
                    INIT_27 => X"000200680008680008006900680008680008680008680008091209120928A008",
                    INIT_28 => X"68000868000868000828A008D20202000200022018A0A0A0A0A0A00228A10900",
                    INIT_29 => X"68282808129268926848080868282808D26828A1090000020068000868000800",
                    INIT_2A => X"4868280892684828926848682808926848D26828A00828A00892689268480808",
                    INIT_2B => X"0208090812F2A1A002080908D2682812F2A102080912F2A1020809D268289268",
                    INIT_2C => X"684848486828280892684828A0926848484868282808926848D2682812F2A1A0",
                    INIT_2D => X"A0A0A0A008926848289268484848682828A0A008926848D26828A0A0A0A0A092",
                    INIT_2E => X"085008B0E8582800000228025008B0E85828000002289268484848682828A0A0",
                    INIT_2F => X"0808080808080808080828025008500851885108B0E858280000000000D20250",
                    INIT_30 => X"00680800680800680800680800D3025008B0E8582800000D0D00080808080808",
                    INIT_31 => X"020200020800F302005108B0E858280068080068080068080068080068080028",
                    INIT_32 => X"0000000000000000000000050200050200040200040200030200030200020200",
                    INIT_33 => X"B0E8582802000052085108B0E858280068080068080068080028000000000000",
                    INIT_34 => X"E85008B0E85828000200020800020800F302005108B0E8582800000002005108",
                    INIT_35 => X"50000A0200508A0200508A0200508A0250000A020800F302005108B0E85008B0",
                    INIT_36 => X"00020800F302005108B0E858280002000208000200508A0200508A0200508A02",
                    INIT_37 => X"020800F302005108B0E85008B0E85008B0E85828000000000002000202000208",
                    INIT_38 => X"0200508A0200508A0200508A0250000A0200508A0200508A0200508A0250000A",
                    INIT_39 => X"0200080208000A09F30200030B5108B0E8582800000000000200020200020800",
                    INIT_3A => X"02000013020BB302B3EBA3030BD3E3130BB3E313A3A0A00B93E893E8A00200A0",
                    INIT_3B => X"1DCD0585A5A5A5CD050D2813020001010300050513130300B3EB03B3E9890300",
                    INIT_3C => X"082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D1828A513A5CD93ED55",
                    INIT_3D => X"5108B0E8582800F3CECE00500E8E0E2870887088708870887088708870887008",
                    INIT_3E => X"510804B0E85813B3E8480028006808680868081368282808B3E913682808B3E9",
                    INIT_3F => X"08B4E951080904B0E8581328006808680868081368282808B3E913682808B3E9",
                   INITP_00 => X"6DB6B64114DF845FC4F98F8B7393BD1FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"6753BE805FA637C589BE2D8FB37EB6B13FFFFFFFFFFFAD6B53FFFFFFE0004CDB",
                   INITP_02 => X"E7561B73B533AB3B6A33AACEDB50EBA22F7375DB57B76FBAEFBDF7BBB7D3382E",
                   INITP_03 => X"00040DA355AAB60486AAD8121AAB60486AAD81A86273FFFF36DD613F840273FF",
                   INITP_04 => X"FB7DB65436E019264372CED55555790018A08A0008B2CB2CAB556C040300C082",
                   INITP_05 => X"FFF009D613F27E1004C209410894225C8C8BA696499252508D08A7EDDB4EA019",
                   INITP_06 => X"98889D4A4C4E91111111131113A4C4E89813B6F5554924925313B6DBDB6E4E7F",
                   INITP_07 => X"433D58B2333D58B22721AAAA644000A40038CF92929550A490D13A9488888888")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_h(31 downto 0),
                  DOPA => data_out_a_h(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_h(31 downto 0),
                  DOPB => data_out_b_h(35 downto 32), 
                   DIB => data_in_b_h(31 downto 0),
                  DIPB => data_in_b_h(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D00490002C110081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"F020100050000060013AE0879201E087930112FF13FFB001B031033E008E5000",
                    INIT_09 => X"103A0002005300410030130050000059E100B130B02C607CD002B0235000F023",
                    INIT_0A => X"60A5D00030030030D1010002D20100020041A03093011304D00110200002D001",
                    INIT_0B => X"0BE50B960210B12BB227A0C1D004B023017E50000059209B20B5D30013040059",
                    INIT_0C => X"5000970896079506940520C8D010900331FFD1031108D004500000815000017E",
                    INIT_0D => X"ACC01C2C1D01607CD00120E1D002B0235000D10311F4D708D607D506D405D004",
                    INIT_0E => X"D00110780002D0011030000200C400C01D0160E8DD00ADD01D30ACC01C2C20E8",
                    INIT_0F => X"00600002005300700002D00110780002D00110300002D00110200002005300C0",
                    INIT_10 => X"1030607CD002B023500060E89D011C0100590053004000020053005000020053",
                    INIT_11 => X"1167500000D0A000102C0710A10010010610A10010010510A10010010410A100",
                    INIT_12 => X"116311691167116F114C112011721165116B116311611172115411201132112D",
                    INIT_13 => X"007000C4100100641A1F1B0111001120113A11561120116411721161116F1142",
                    INIT_14 => X"00530040D001102E000200530050D001102E000200530060D001102E00020053",
                    INIT_15 => X"112011641172116F115711001120113A11721165116611661175114250000059",
                    INIT_16 => X"117A11691173112011641172116F115711001120113A1174116E1175116F1163",
                    INIT_17 => X"1A521B011100112011641172116F115711001120112011201120113A11731165",
                    INIT_18 => X"D00110280002D0011020000221831101D001A0100002E18AC120B22011000064",
                    INIT_19 => X"0041B023000200641A5B1B010059D00110290002D1010002D201000200410020",
                    INIT_1A => X"01301124D00110400002D00110200002E1B8C3401300B423D1010002D2010002",
                    INIT_1B => X"E1CDC3401300B42300641A681B01005921A61301D1010002D20100020041A010",
                    INIT_1C => X"1300B423005921BE1301D1010002D20100020041A01001301128D00110200002",
                    INIT_1D => X"0530D00110200002D1010002D20100020041003000641A781B010059E1F0C340",
                    INIT_1E => X"21E69601D1010002D20100020041A060A1D0C65016030650152C450645061301",
                    INIT_1F => X"1A03005921F43B001A01D10100022202D1FF21FED1004BA01A9A1B0950000059",
                    INIT_20 => X"9E011E3E50007000DD024D003DF0400740073003700160005000005921F43B00",
                    INIT_21 => X"DD025D025000DD023DFE020E020E020E020EDD023DFDDD025D025D015000E20F",
                    INIT_22 => X"020E020E020EDD023DFE020E020E020EDD023DFD020E020EDD025D01020E020E",
                    INIT_23 => X"020E5000DD025D02020E020E020E020EDD025D01020E020E020EDD023DFD5000",
                    INIT_24 => X"5D01020EDD023DFD5000020E020E020EDD023DFE020E020EDD025D01DD025D02",
                    INIT_25 => X"3DFDA2624406020E15805000020E020EDD025D02020EDD023DFE020E020EDD02",
                    INIT_26 => X"5D02020E225CA26E450EDD023DFE020E020EDD025D01020EDD025D022264DD02",
                    INIT_27 => X"14005D025000D502020E020E020EDD023DFE020E020EDD025D019502020EDD02",
                    INIT_28 => X"5000020EE281450EDD023DFE020E020EDD025D01020E4400D6029602020E1580",
                    INIT_29 => X"A2AC025B0420A2AA025B0410A2A8025B0400021230FE70016330622061106000",
                    INIT_2A => X"22B0100422B0100322B0100222B0100150007000400610000231A2AE025B0430",
                    INIT_2B => X"0410A2CA025B0400021230FE7001622061106000500070004006108002310053",
                    INIT_2C => X"22D0100322D0100222D0100150007000400610000231A2CE025B0420A2CC025B",
                    INIT_2D => X"025B0410A2F3025B0400021230FE700161106000500070004006108002310053",
                    INIT_2E => X"6220400610000231023F0340027E024C0240027EA2F7025B04005001021EA2AA",
                    INIT_2F => X"114150007000400610800231005322F9100322F9100222F91001500070006330",
                    INIT_30 => X"B0235000005900641AFF1B021100112E1172116F1172117211651120116B1163",
                    INIT_31 => X"A323D503A2401401A340A31DD503A550152A1434A1401430A040142C607CD003",
                    INIT_32 => X"0020A30A02D6A1401430A040142C607CD002B0235000A30A02B65000A30A0290",
                    INIT_33 => X"6D0010F350000204100250000204100150000204100050000059005300300053",
                    INIT_34 => X"1174116E1165116D11751167117211611120116411611142500003350338033B",
                    INIT_35 => X"1DFF5000D003301FA040142C6352D001B0235000005900641A441B0311001173",
                    INIT_36 => X"00C410B2303040064006400640065000E3629D01E3629E01E3629F011FFF1EFF",
                    INIT_37 => X"9400005900530040005300500053006000530070D00110200002D00110490002",
                    INIT_38 => X"104B000200C41052303040064006400640065000C54015809000B700B607B589",
                    INIT_39 => X"B607B5899400005900530040005300500053006000530070D00110200002D001",
                    INIT_3A => X"0002D001104C000200C410B0303040064006400640065000C54015809000B700",
                    INIT_3B => X"9000B700B607B5899400005900530040005300500053006000530070D0011020",
                    INIT_3C => X"D00110200002D001105A000200C40020303040064006400640065000C5401580",
                    INIT_3D => X"9700100096001000950010009400005900530040005300500053006000530070",
                    INIT_3E => X"40064006400600B00059005300C00002D001105000021A000B001C0150001000",
                    INIT_3F => X"400640064006400600B04410348F3170410641064106410601C000C410504006",
                    INIT_40 => X"035F00C41050400640064006400600B000D01700160015001401102000D01050",
                    INIT_41 => X"400640064006400600B035FC035F00D010503030400640064006400600B05503",
                    INIT_42 => X"10200002D0011054000200C410B43030400640064006400600B000D010503030",
                    INIT_43 => X"036900B0038700B003A500B0005900530040005300500053006000530070D001",
                    INIT_44 => X"245203C3125800B024594A06244C03C3125600B024594A06244603C312B500B0",
                    INIT_45 => X"400600A03A1FDA1F63E5DC061C014A0724594A06245903C3125700B024594A06",
                    INIT_46 => X"6466D0E04002420F246D420E246CC01021A00100100712004A0030E040064006",
                    INIT_47 => X"4C064C063C070059005300100C101100A473D105A479420E11011100420E420E",
                    INIT_48 => X"104050000059005900D024C0347F00C41050400640064006400600B04C064C06",
                    INIT_49 => X"100224A1D40400C4104003E21001249BD40200C4104003E210002495D40100C4",
                    INIT_4A => X"5000E4AD9F011FFF500004ACDF091F80500003E2100324A7D40800C4104003E2",
                    INIT_4B => X"9F0164C3310291091EFF1FFF5000D109112004ACD00A24B23101910950009009",
                    INIT_4C => X"40064006400704A85000420E1201500042061200D1091110900A24BC24C9BE00",
                    INIT_4D => X"04500340A4E704BA0400A4E504BA0500A4E304BA04B24010310F400640064006",
                    INIT_4E => X"00021152D10100021152D10100021145133324E8133224E813315000400E1000",
                    INIT_4F => X"005304B00002D1010002113AD101000211530059D3010002D1010002113AD101",
                    INIT_50 => X"04B2003004B24010310F40064006400640064006400604A85000430E13010059",
                    INIT_51 => X"D10100021152D10100021152D101000211455000400E1000A51704BA04B20020",
                    INIT_52 => X"A53CD0015000430E13010059005304B00002D1010002113AD101000211530059",
                    INIT_53 => X"D00B5020508010002547254AD0012534D002900B10001000D00B500250081000",
                    INIT_54 => X"900BA559D0015000400E10015000400E1000254AD0102540D020900B10001000",
                    INIT_55 => X"900BD00B502010C02559D020900B50002555D002900BD00B5002100C254FD002",
                    INIT_56 => X"E56C420E05A7100112802571E566420E05A710001280A56BD0015000255FD020",
                    INIT_57 => X"05851001128011002584E576420E41080585100012801100A57CD00150002571",
                    INIT_58 => X"900B900BD00B5004500210002587D002900BA595D00150002584E57E420E4108",
                    INIT_59 => X"D020900B900B900BD00B5040502010002595D020900B5000400E258ED002900B",
                    INIT_5A => X"50024000410E100025A9D002900BA5B8D0015000400E400E400E400E400E259C",
                    INIT_5B => X"400640064000410E100025B8D020900B500025B2D002900B900B900BD00B5000",
                    INIT_5C => X"B02350000059005304B0500025C5D020900B900B900BD00B5000502040064006",
                    INIT_5D => X"1130A010112C607CD002B02350000059005304BA500004B2A010112C607CD001",
                    INIT_5E => X"A3101101A2101134607CD003B023500000590053003000530040A5E804CCA110",
                    INIT_5F => X"11201165116311691176116511641120116F114E50000504A1101130A010112C",
                    INIT_60 => X"112C607CD001B0235000005900641AF61B05005911001164116E1175116F1166",
                    INIT_61 => X"0059D00110450002D001104E0002D001104F0002D001104E0002A621052EA010",
                    INIT_62 => X"D00110440002D001104E0002D00110550002D001104F0002D001104600025000",
                    INIT_63 => X"041005720030056311330030E667052E0030A310112C607CD001B02350000059",
                    INIT_64 => X"0030091005720030081005720030071005720030061005720030051005720030",
                    INIT_65 => X"0060005300700053008000530090005300A0005300B00B10057200300A100572",
                    INIT_66 => X"D00110520002D00110520002D001104500025000005900530040005300500053",
                    INIT_67 => X"607CD001B0235000056301400030A4101130A310112C607CD002B02350000059",
                    INIT_68 => X"E606052E0030A310112C607CD001B023500000590053001005720030A310112C",
                    INIT_69 => X"D008A0101129607CD003B02350000059054D0030056311440030056311CC0030",
                    INIT_6A => X"A14000301434056311550030E606052E0030A310112C607CD008A010112A607C",
                    INIT_6B => X"A1400030143005630030A140140105630030A140140105630030A14014010563",
                    INIT_6C => X"05631144003005630030A140140105630030A140140105630030A14014010563",
                    INIT_6D => X"0030056311CC0030E606052E0030A310112C607CD001B02350000059054D0030",
                    INIT_6E => X"607CD003B02350000059005300400053001005720030041005720030056311BE",
                    INIT_6F => X"056311550030E606052E0030A310112C607CD008A010112A607CD008A0101129",
                    INIT_70 => X"05630030A140140105630030A140140105630030A14014010563A14000301434",
                    INIT_71 => X"05630030A140140105630030A140140105630030A14014010563A14000301430",
                    INIT_72 => X"D001B02350000059005300400053001005720030041005720030056311BE0030",
                    INIT_73 => X"058500201100056311F0002014001301E606052E0020079F1700A210112C607C",
                    INIT_74 => X"16016751C730275D4600410E410E1600274DD100276BD1034100058500204100",
                    INIT_75 => X"05A700200160275D04301600675D0430675DD600460007761600A75AC730275D",
                    INIT_76 => X"07B100590B300A202775277507B10059676CD7000740673DD341130107880160",
                    INIT_77 => X"3A079A010A300BA04A0E4A0E4A0E9A010A301B4050002735054D002003B002A0",
                    INIT_78 => X"0BA04A0E4A0E4A0E9A010A301B40310150004B0E27814B0E9A012786DA00ABB0",
                    INIT_79 => X"11005000ECB04C102CD027959A0141064D07279BDA001DFEACB03A079A010A30",
                    INIT_7A => X"E1001001E1001001E1001001E1001001E1001001E1001001E1001001E1001040",
                    INIT_7B => X"A210112C607CD001B02350000059E7B49D019C010053A0C01D071C071C405000",
                    INIT_7C => X"114CD10D110027D5D10D51025101110067CDD20127D5D10D5101110067C6D200",
                    INIT_7D => X"A210112C084A607CD001B02327CD67D5D11D9101000650000059D1011142D101",
                    INIT_7E => X"114CD10D110027F5D10D51025101110067CDD20127F5D10D5101110067E6D200",
                    INIT_7F => X"11006803D200A210112C130F084A607CD001B02327F550000059D1011142D101",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"28A28A288A2834020374A2AA803022AAA434AAD6D0AA858A2A0A834A8AB742AA",
                   INITP_02 => X"0AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAAAAAAAAA801041034B5A28A",
                   INITP_03 => X"696B760A9AA8D45522AA882D0A6AA122D0829AA848A2D0AA8A0A2AA88A298B42",
                   INITP_04 => X"08AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA2A8A8A8A2AA20B4B814DA9",
                   INITP_05 => X"AD2A222D528A2E0B8E235B4A888B4B8E388D5B4A8888B4B8E38E2355AD8A8908",
                   INITP_06 => X"688888A29156DDD0280D2A0AAAAAAAAA4A28A2A238034BAED134000D2A0AAAAA",
                   INITP_07 => X"5500552554A2880B777688888A291564D5688888A291564D5688888A291564D5",
                   INITP_08 => X"5280DD45D59D000541D59E09E09E09E0888A222228A45524550A4550A5548009",
                   INITP_09 => X"AA8A2AA28A2822240E38E8155692482D700A2B08B4A8A322322322322A825545",
                   INITP_0A => X"80B580CAD82D832C20C2C20C3249330080B30080C92AA8A2A28A24E888155692",
                   INITP_0B => X"AAAAAA0010D2A238034AAA0D2AAC02055530B00814C325570080C270080C32B5",
                   INITP_0C => X"D280034A8A28AA222222208208208208220E034A8A28A28AA28A28B834A82AAA",
                   INITP_0D => X"8380D0D0D2A2208820E034A88206060602060606020E0343434A8820E034A880",
                   INITP_0E => X"11550A80A0AAD358820CD6363654DD618200E2034A8882208181818081818180",
                   INITP_0F => X"340B4AA22280DA0D0B4B4AA22280DA0D0D2B5812999999982825740455409974")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D00490002C110081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"F020100050000060013AE0879201E087930112FF13FFB001B031033E008E5000",
                    INIT_09 => X"103A0002005300410030130050000059E100B130B02C607CD002B0235000F023",
                    INIT_0A => X"60A5D00030030030D1010002D20100020041A03093011304D00110200002D001",
                    INIT_0B => X"0BE50B960210B12BB227A0C1D004B023017E50000059209B20B5D30013040059",
                    INIT_0C => X"5000970896079506940520C8D010900331FFD1031108D004500000815000017E",
                    INIT_0D => X"ACC01C2C1D01607CD00120E1D002B0235000D10311F4D708D607D506D405D004",
                    INIT_0E => X"D00110780002D0011030000200C400C01D0160E8DD00ADD01D30ACC01C2C20E8",
                    INIT_0F => X"00600002005300700002D00110780002D00110300002D00110200002005300C0",
                    INIT_10 => X"1030607CD002B023500060E89D011C0100590053004000020053005000020053",
                    INIT_11 => X"1167500000D0A000102C0710A10010010610A10010010510A10010010410A100",
                    INIT_12 => X"116311691167116F114C112011721165116B116311611172115411201132112D",
                    INIT_13 => X"007000C4100100641A1F1B0111001120113A11561120116411721161116F1142",
                    INIT_14 => X"00530040D001102E000200530050D001102E000200530060D001102E00020053",
                    INIT_15 => X"112011641172116F115711001120113A11721165116611661175114250000059",
                    INIT_16 => X"117A11691173112011641172116F115711001120113A1174116E1175116F1163",
                    INIT_17 => X"1A521B011100112011641172116F115711001120112011201120113A11731165",
                    INIT_18 => X"D00110280002D0011020000221831101D001A0100002E18AC120B22011000064",
                    INIT_19 => X"0041B023000200641A5B1B010059D00110290002D1010002D201000200410020",
                    INIT_1A => X"01301124D00110400002D00110200002E1B8C3401300B423D1010002D2010002",
                    INIT_1B => X"E1CDC3401300B42300641A681B01005921A61301D1010002D20100020041A010",
                    INIT_1C => X"1300B423005921BE1301D1010002D20100020041A01001301128D00110200002",
                    INIT_1D => X"0530D00110200002D1010002D20100020041003000641A781B010059E1F0C340",
                    INIT_1E => X"21E69601D1010002D20100020041A060A1D0C65016030650152C450645061301",
                    INIT_1F => X"1A03005921F43B001A01D10100022202D1FF21FED1004BA01A9A1B0950000059",
                    INIT_20 => X"9E011E3E50007000DD024D003DF0400740073003700160005000005921F43B00",
                    INIT_21 => X"DD025D025000DD023DFE020E020E020E020EDD023DFDDD025D025D015000E20F",
                    INIT_22 => X"020E020E020EDD023DFE020E020E020EDD023DFD020E020EDD025D01020E020E",
                    INIT_23 => X"020E5000DD025D02020E020E020E020EDD025D01020E020E020EDD023DFD5000",
                    INIT_24 => X"5D01020EDD023DFD5000020E020E020EDD023DFE020E020EDD025D01DD025D02",
                    INIT_25 => X"3DFDA2624406020E15805000020E020EDD025D02020EDD023DFE020E020EDD02",
                    INIT_26 => X"5D02020E225CA26E450EDD023DFE020E020EDD025D01020EDD025D022264DD02",
                    INIT_27 => X"14005D025000D502020E020E020EDD023DFE020E020EDD025D019502020EDD02",
                    INIT_28 => X"5000020EE281450EDD023DFE020E020EDD025D01020E4400D6029602020E1580",
                    INIT_29 => X"A2AC025B0420A2AA025B0410A2A8025B0400021230FE70016330622061106000",
                    INIT_2A => X"22B0100422B0100322B0100222B0100150007000400610000231A2AE025B0430",
                    INIT_2B => X"0410A2CA025B0400021230FE7001622061106000500070004006108002310053",
                    INIT_2C => X"22D0100322D0100222D0100150007000400610000231A2CE025B0420A2CC025B",
                    INIT_2D => X"025B0410A2F3025B0400021230FE700161106000500070004006108002310053",
                    INIT_2E => X"6220400610000231023F0340027E024C0240027EA2F7025B04005001021EA2AA",
                    INIT_2F => X"114150007000400610800231005322F9100322F9100222F91001500070006330",
                    INIT_30 => X"B0235000005900641AFF1B021100112E1172116F1172117211651120116B1163",
                    INIT_31 => X"A323D503A2401401A340A31DD503A550152A1434A1401430A040142C607CD003",
                    INIT_32 => X"0020A30A02D6A1401430A040142C607CD002B0235000A30A02B65000A30A0290",
                    INIT_33 => X"6D0010F350000204100250000204100150000204100050000059005300300053",
                    INIT_34 => X"1174116E1165116D11751167117211611120116411611142500003350338033B",
                    INIT_35 => X"1DFF5000D003301FA040142C6352D001B0235000005900641A441B0311001173",
                    INIT_36 => X"00C410B2303040064006400640065000E3629D01E3629E01E3629F011FFF1EFF",
                    INIT_37 => X"9400005900530040005300500053006000530070D00110200002D00110490002",
                    INIT_38 => X"104B000200C41052303040064006400640065000C54015809000B700B607B589",
                    INIT_39 => X"B607B5899400005900530040005300500053006000530070D00110200002D001",
                    INIT_3A => X"0002D001104C000200C410B0303040064006400640065000C54015809000B700",
                    INIT_3B => X"9000B700B607B5899400005900530040005300500053006000530070D0011020",
                    INIT_3C => X"D00110200002D001105A000200C40020303040064006400640065000C5401580",
                    INIT_3D => X"9700100096001000950010009400005900530040005300500053006000530070",
                    INIT_3E => X"40064006400600B00059005300C00002D001105000021A000B001C0150001000",
                    INIT_3F => X"400640064006400600B04410348F3170410641064106410601C000C410504006",
                    INIT_40 => X"035F00C41050400640064006400600B000D01700160015001401102000D01050",
                    INIT_41 => X"400640064006400600B035FC035F00D010503030400640064006400600B05503",
                    INIT_42 => X"10200002D0011054000200C410B43030400640064006400600B000D010503030",
                    INIT_43 => X"036900B0038700B003A500B0005900530040005300500053006000530070D001",
                    INIT_44 => X"245203C3125800B024594A06244C03C3125600B024594A06244603C312B500B0",
                    INIT_45 => X"400600A03A1FDA1F63E5DC061C014A0724594A06245903C3125700B024594A06",
                    INIT_46 => X"6466D0E04002420F246D420E246CC01021A00100100712004A0030E040064006",
                    INIT_47 => X"4C064C063C070059005300100C101100A473D105A479420E11011100420E420E",
                    INIT_48 => X"104050000059005900D024C0347F00C41050400640064006400600B04C064C06",
                    INIT_49 => X"100224A1D40400C4104003E21001249BD40200C4104003E210002495D40100C4",
                    INIT_4A => X"5000E4AD9F011FFF500004ACDF091F80500003E2100324A7D40800C4104003E2",
                    INIT_4B => X"9F0164C3310291091EFF1FFF5000D109112004ACD00A24B23101910950009009",
                    INIT_4C => X"40064006400704A85000420E1201500042061200D1091110900A24BC24C9BE00",
                    INIT_4D => X"04500340A4E704BA0400A4E504BA0500A4E304BA04B24010310F400640064006",
                    INIT_4E => X"00021152D10100021152D10100021145133324E8133224E813315000400E1000",
                    INIT_4F => X"005304B00002D1010002113AD101000211530059D3010002D1010002113AD101",
                    INIT_50 => X"04B2003004B24010310F40064006400640064006400604A85000430E13010059",
                    INIT_51 => X"D10100021152D10100021152D101000211455000400E1000A51704BA04B20020",
                    INIT_52 => X"A53CD0015000430E13010059005304B00002D1010002113AD101000211530059",
                    INIT_53 => X"D00B5020508010002547254AD0012534D002900B10001000D00B500250081000",
                    INIT_54 => X"900BA559D0015000400E10015000400E1000254AD0102540D020900B10001000",
                    INIT_55 => X"900BD00B502010C02559D020900B50002555D002900BD00B5002100C254FD002",
                    INIT_56 => X"E56C420E05A7100112802571E566420E05A710001280A56BD0015000255FD020",
                    INIT_57 => X"05851001128011002584E576420E41080585100012801100A57CD00150002571",
                    INIT_58 => X"900B900BD00B5004500210002587D002900BA595D00150002584E57E420E4108",
                    INIT_59 => X"D020900B900B900BD00B5040502010002595D020900B5000400E258ED002900B",
                    INIT_5A => X"50024000410E100025A9D002900BA5B8D0015000400E400E400E400E400E259C",
                    INIT_5B => X"400640064000410E100025B8D020900B500025B2D002900B900B900BD00B5000",
                    INIT_5C => X"B02350000059005304B0500025C5D020900B900B900BD00B5000502040064006",
                    INIT_5D => X"1130A010112C607CD002B02350000059005304BA500004B2A010112C607CD001",
                    INIT_5E => X"A3101101A2101134607CD003B023500000590053003000530040A5E804CCA110",
                    INIT_5F => X"11201165116311691176116511641120116F114E50000504A1101130A010112C",
                    INIT_60 => X"112C607CD001B0235000005900641AF61B05005911001164116E1175116F1166",
                    INIT_61 => X"0059D00110450002D001104E0002D001104F0002D001104E0002A621052EA010",
                    INIT_62 => X"D00110440002D001104E0002D00110550002D001104F0002D001104600025000",
                    INIT_63 => X"041005720030056311330030E667052E0030A310112C607CD001B02350000059",
                    INIT_64 => X"0030091005720030081005720030071005720030061005720030051005720030",
                    INIT_65 => X"0060005300700053008000530090005300A0005300B00B10057200300A100572",
                    INIT_66 => X"D00110520002D00110520002D001104500025000005900530040005300500053",
                    INIT_67 => X"607CD001B0235000056301400030A4101130A310112C607CD002B02350000059",
                    INIT_68 => X"E606052E0030A310112C607CD001B023500000590053001005720030A310112C",
                    INIT_69 => X"D008A0101129607CD003B02350000059054D0030056311440030056311CC0030",
                    INIT_6A => X"A14000301434056311550030E606052E0030A310112C607CD008A010112A607C",
                    INIT_6B => X"A1400030143005630030A140140105630030A140140105630030A14014010563",
                    INIT_6C => X"05631144003005630030A140140105630030A140140105630030A14014010563",
                    INIT_6D => X"0030056311CC0030E606052E0030A310112C607CD001B02350000059054D0030",
                    INIT_6E => X"607CD003B02350000059005300400053001005720030041005720030056311BE",
                    INIT_6F => X"056311550030E606052E0030A310112C607CD008A010112A607CD008A0101129",
                    INIT_70 => X"05630030A140140105630030A140140105630030A14014010563A14000301434",
                    INIT_71 => X"05630030A140140105630030A140140105630030A14014010563A14000301430",
                    INIT_72 => X"D001B02350000059005300400053001005720030041005720030056311BE0030",
                    INIT_73 => X"058500201100056311F0002014001301E606052E0020079F1700A210112C607C",
                    INIT_74 => X"16016751C730275D4600410E410E1600274DD100276BD1034100058500204100",
                    INIT_75 => X"05A700200160275D04301600675D0430675DD600460007761600A75AC730275D",
                    INIT_76 => X"07B100590B300A202775277507B10059676CD7000740673DD341130107880160",
                    INIT_77 => X"3A079A010A300BA04A0E4A0E4A0E9A010A301B4050002735054D002003B002A0",
                    INIT_78 => X"0BA04A0E4A0E4A0E9A010A301B40310150004B0E27814B0E9A012786DA00ABB0",
                    INIT_79 => X"11005000ECB04C102CD027959A0141064D07279BDA001DFEACB03A079A010A30",
                    INIT_7A => X"E1001001E1001001E1001001E1001001E1001001E1001001E1001001E1001040",
                    INIT_7B => X"A210112C607CD001B02350000059E7B49D019C010053A0C01D071C071C405000",
                    INIT_7C => X"114CD10D110027D5D10D51025101110067CDD20127D5D10D5101110067C6D200",
                    INIT_7D => X"A210112C084A607CD001B02327CD67D5D11D9101000650000059D1011142D101",
                    INIT_7E => X"114CD10D110027F5D10D51025101110067CDD20127F5D10D5101110067E6D200",
                    INIT_7F => X"11006803D200A210112C130F084A607CD001B02327F550000059D1011142D101",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"28A28A288A2834020374A2AA803022AAA434AAD6D0AA858A2A0A834A8AB742AA",
                   INITP_02 => X"0AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAAAAAAAAA801041034B5A28A",
                   INITP_03 => X"696B760A9AA8D45522AA882D0A6AA122D0829AA848A2D0AA8A0A2AA88A298B42",
                   INITP_04 => X"08AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA2A8A8A8A2AA20B4B814DA9",
                   INITP_05 => X"AD2A222D528A2E0B8E235B4A888B4B8E388D5B4A8888B4B8E38E2355AD8A8908",
                   INITP_06 => X"688888A29156DDD0280D2A0AAAAAAAAA4A28A2A238034BAED134000D2A0AAAAA",
                   INITP_07 => X"5500552554A2880B777688888A291564D5688888A291564D5688888A291564D5",
                   INITP_08 => X"5280DD45D59D000541D59E09E09E09E0888A222228A45524550A4550A5548009",
                   INITP_09 => X"AA8A2AA28A2822240E38E8155692482D700A2B08B4A8A322322322322A825545",
                   INITP_0A => X"80B580CAD82D832C20C2C20C3249330080B30080C92AA8A2A28A24E888155692",
                   INITP_0B => X"AAAAAA0010D2A238034AAA0D2AAC02055530B00814C325570080C270080C32B5",
                   INITP_0C => X"D280034A8A28AA222222208208208208220E034A8A28A28AA28A28B834A82AAA",
                   INITP_0D => X"8380D0D0D2A2208820E034A88206060602060606020E0343434A8820E034A880",
                   INITP_0E => X"11550A80A0AAD358820CD6363654DD618200E2034A8882208181818081818180",
                   INITP_0F => X"340B4AA22280DA0D0B4B4AA22280DA0D0D2B5812999999982825740455409974")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_2k_generate;
  --
  --	
  ram_4k_generate : if (C_RAM_SIZE_KWORDS = 4) generate
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      data_in_a <= "000000000000000000000000000000000000";
      --
      s6_a11_flop: FD
      port map (  D => address(11),
                  Q => pipe_a11,
                  C => clk);
      --
      s6_4k_mux0_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(0),
                I1 => data_out_a_hl(0),
                I2 => data_out_a_ll(1),
                I3 => data_out_a_hl(1),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(0),
                O6 => instruction(1));
      --
      s6_4k_mux2_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(2),
                I1 => data_out_a_hl(2),
                I2 => data_out_a_ll(3),
                I3 => data_out_a_hl(3),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(2),
                O6 => instruction(3));
      --
      s6_4k_mux4_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(4),
                I1 => data_out_a_hl(4),
                I2 => data_out_a_ll(5),
                I3 => data_out_a_hl(5),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(4),
                O6 => instruction(5));
      --
      s6_4k_mux6_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(6),
                I1 => data_out_a_hl(6),
                I2 => data_out_a_ll(7),
                I3 => data_out_a_hl(7),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(6),
                O6 => instruction(7));
      --
      s6_4k_mux8_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(32),
                I1 => data_out_a_hl(32),
                I2 => data_out_a_lh(0),
                I3 => data_out_a_hh(0),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(8),
                O6 => instruction(9));
      --
      s6_4k_mux10_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(1),
                I1 => data_out_a_hh(1),
                I2 => data_out_a_lh(2),
                I3 => data_out_a_hh(2),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(10),
                O6 => instruction(11));
      --
      s6_4k_mux12_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(3),
                I1 => data_out_a_hh(3),
                I2 => data_out_a_lh(4),
                I3 => data_out_a_hh(4),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(12),
                O6 => instruction(13));
      --
      s6_4k_mux14_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(5),
                I1 => data_out_a_hh(5),
                I2 => data_out_a_lh(6),
                I3 => data_out_a_hh(6),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(14),
                O6 => instruction(15));
      --
      s6_4k_mux16_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(7),
                I1 => data_out_a_hh(7),
                I2 => data_out_a_lh(32),
                I3 => data_out_a_hh(32),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(16),
                O6 => instruction(17));
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_ll <= "000" & data_out_b_ll(32) & "000000000000000000000000" & data_out_b_ll(7 downto 0);
        data_in_b_lh <= "000" & data_out_b_lh(32) & "000000000000000000000000" & data_out_b_lh(7 downto 0);
        data_in_b_hl <= "000" & data_out_b_hl(32) & "000000000000000000000000" & data_out_b_hl(7 downto 0);
        data_in_b_hh <= "000" & data_out_b_hh(32) & "000000000000000000000000" & data_out_b_hh(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b_l(3 downto 0) <= "0000";
        we_b_h(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
        jtag_dout <= data_out_b_lh(32) & data_out_b_lh(7 downto 0) & data_out_b_ll(32) & data_out_b_ll(7 downto 0);
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_lh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_ll <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        data_in_b_hh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_hl <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        --
        s6_4k_jtag_we_lut: LUT6_2
        generic map (INIT => X"8000000020000000")
        port map( I0 => jtag_we,
                  I1 => jtag_addr(11),
                  I2 => '1',
                  I3 => '1',
                  I4 => '1',
                  I5 => '1',
                  O5 => jtag_we_l,
                  O6 => jtag_we_h);
        --
        we_b_l(3 downto 0) <= jtag_we_l & jtag_we_l & jtag_we_l & jtag_we_l;
        we_b_h(3 downto 0) <= jtag_we_h & jtag_we_h & jtag_we_h & jtag_we_h;
        --
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
        --
        s6_4k_jtag_mux0_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(0),
                  I1 => data_out_b_hl(0),
                  I2 => data_out_b_ll(1),
                  I3 => data_out_b_hl(1),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(0),
                  O6 => jtag_dout(1));
        --
        s6_4k_jtag_mux2_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(2),
                  I1 => data_out_b_hl(2),
                  I2 => data_out_b_ll(3),
                  I3 => data_out_b_hl(3),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(2),
                  O6 => jtag_dout(3));
        --
        s6_4k_jtag_mux4_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(4),
                  I1 => data_out_b_hl(4),
                  I2 => data_out_b_ll(5),
                  I3 => data_out_b_hl(5),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(4),
                  O6 => jtag_dout(5));
        --
        s6_4k_jtag_mux6_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(6),
                  I1 => data_out_b_hl(6),
                  I2 => data_out_b_ll(7),
                  I3 => data_out_b_hl(7),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(6),
                  O6 => jtag_dout(7));
        --
        s6_4k_jtag_mux8_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(32),
                  I1 => data_out_b_hl(32),
                  I2 => data_out_b_lh(0),
                  I3 => data_out_b_hh(0),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(8),
                  O6 => jtag_dout(9));
        --
        s6_4k_jtag_mux10_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(1),
                  I1 => data_out_b_hh(1),
                  I2 => data_out_b_lh(2),
                  I3 => data_out_b_hh(2),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(10),
                  O6 => jtag_dout(11));
        --
        s6_4k_jtag_mux12_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(3),
                  I1 => data_out_b_hh(3),
                  I2 => data_out_b_lh(4),
                  I3 => data_out_b_hh(4),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(12),
                  O6 => jtag_dout(13));
        --
        s6_4k_jtag_mux14_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(5),
                  I1 => data_out_b_hh(5),
                  I2 => data_out_b_lh(6),
                  I3 => data_out_b_hh(6),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(14),
                  O6 => jtag_dout(15));
        --
        s6_4k_jtag_mux16_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(7),
                  I1 => data_out_b_hh(7),
                  I2 => data_out_b_lh(32),
                  I3 => data_out_b_hh(32),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(16),
                  O6 => jtag_dout(17));
      --
      end generate loader;
      --
      kcpsm6_rom_ll: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004001181",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"3A0253413000005900302C7C02230023200000603A87018701FFFF01313E8E00",
                    INIT_05 => X"E596102B27C104237E00599BB5000459A5000330010201024130010401200201",
                    INIT_06 => X"C02C017C01E102230003F408070605040008070605C81003FF0308040081007E",
                    INIT_07 => X"600253700201780201300201200253C0017802013002C4C001E800D030C02CE8",
                    INIT_08 => X"6700D0002C1000011000011000011000307C022300E801015953400253500253",
                    INIT_09 => X"70C401641F0100203A56206472616F426369676F4C2072656B6361725420322D",
                    INIT_0A => X"2064726F5700203A72656666754200595340012E025350012E025360012E0253",
                    INIT_0B => X"5201002064726F5700202020203A73657A69732064726F5700203A746E756F63",
                    INIT_0C => X"412302645B015901290201020102412001280201200283010110028A20200064",
                    INIT_0D => X"CD40002364680159A6010102010241103024014002012002B840002301020102",
                    INIT_0E => X"3001200201020102413064780159F040002359BE010102010241103028012002",
                    INIT_0F => X"0359F40001010202FFFE00A09A090059E601010201024160D05003502C060601",
                    INIT_10 => X"02020002FE0E0E0E0E02FD020201000F013E00000200F007070301000059F400",
                    INIT_11 => X"0E0002020E0E0E0E02010E0E0E02FD000E0E0E02FE0E0E0E02FD0E0E02010E0E",
                    INIT_12 => X"FD62060E80000E0E02020E02FE0E0E02010E02FD000E0E0E02FE0E0E02010202",
                    INIT_13 => X"000200020E0E0E02FE0E0E0201020E02020E5C6E0E02FE0E0E02010E02026402",
                    INIT_14 => X"AC5B20AA5B10A85B0012FE0130201000000E810E02FE0E0E02010E0002020E80",
                    INIT_15 => X"10CA5B0012FE01201000000006803153B004B003B002B0010000060031AE5B30",
                    INIT_16 => X"5B10F35B0012FE011000000006803153D003D002D0010000060031CE5B20CC5B",
                    INIT_17 => X"41000006803153F903F902F901000030200600313F407E4C407EF75B00011EAA",
                    INIT_18 => X"23034001401D03502A344030402C7C0323005964FF02002E726F727265206B63",
                    INIT_19 => X"00F30004020004010004000059533053200AD64030402C7C0223000AB6000A90",
                    INIT_1A => X"FF00031F402C52012300596444030073746E656D75677261206461420035383B",
                    INIT_1B => X"00595340535053605370012002014902C4B2300606060600620162016201FFFF",
                    INIT_1C => X"078900595340535053605370012002014B02C452300606060600408000000789",
                    INIT_1D => X"0000078900595340535053605370012002014C02C4B030060606060040800000",
                    INIT_1E => X"00000000000000595340535053605370012002015A02C4203006060606004080",
                    INIT_1F => X"06060606B0108F7006060606C0C45006060606B05953C0020150020000010000",
                    INIT_20 => X"06060606B0FC5FD0503006060606B0035FC45006060606B0D00000000120D050",
                    INIT_21 => X"69B087B0A5B0595340535053605370012002015402C4B43006060606B0D05030",
                    INIT_22 => X"06A01F1FE5060107590659C357B0590652C358B059064CC356B0590646C3B5B0",
                    INIT_23 => X"06060759531010007305790E01000E0E66E0020F6D0E6C10A000070000E00606",
                    INIT_24 => X"02A104C440E2019B02C440E2009501C440005959D0C07FC45006060606B00606",
                    INIT_25 => X"01C30209FFFF000920AC0AB20109000900AD01FF00AC098000E203A708C440E2",
                    INIT_26 => X"5040E7BA00E5BA00E3BAB2100F060606060607A8000E0100060009100ABCC900",
                    INIT_27 => X"53B00201023A01025359010201023A01025201025201024533E832E831000E00",
                    INIT_28 => X"010252010252010245000E0017BAB220B230B2100F060606060606A8000E0159",
                    INIT_29 => X"0B208000474A0134020B00000B0208003C01000E015953B00201023A01025359",
                    INIT_2A => X"0B0B20C059200B0055020B0B020C4F020B5901000E01000E004A1040200B0000",
                    INIT_2B => X"8501800084760E08850080007C0100716C0EA7018071660EA700806B01005F20",
                    INIT_2C => X"200B0B0B0B40200095200B000E8E020B0B0B0B04020087020B950100847E0E08",
                    INIT_2D => X"0606000E00B8200B00B2020B0B0B0B0002000E00A9020BB801000E0E0E0E0E9C",
                    INIT_2E => X"30102C7C0223005953BA00B2102C7C0123005953B000C5200B0B0B0B00200606",
                    INIT_2F => X"20656369766564206F4E00041030102C100110347C0323005953305340E8CC10",
                    INIT_30 => X"59014502014E02014F02014E02212E102C7C0123005964F6055900646E756F66",
                    INIT_31 => X"107230633330672E30102C7C01230059014402014E02015502014F0201460200",
                    INIT_32 => X"6053705380539053A053B0107230107230107230107230107230107230107230",
                    INIT_33 => X"7C0123006340301030102C7C0223005901520201520201450200595340535053",
                    INIT_34 => X"0810297C032300594D3063443063CC30062E30102C7C0123005953107230102C",
                    INIT_35 => X"40303063304001633040016330400163403034635530062E30102C7C08102A7C",
                    INIT_36 => X"3063CC30062E30102C7C012300594D3063443063304001633040016330400163",
                    INIT_37 => X"635530062E30102C7C08102A7C0810297C0323005953405310723010723063BE",
                    INIT_38 => X"6330400163304001633040016340303063304001633040016330400163403034",
                    INIT_39 => X"85200063F0200001062E209F00102C7C0123005953405310723010723063BE30",
                    INIT_3A => X"A720605D30005D305D000076005A305D0151305D000E0E004D006B0300852000",
                    INIT_3B => X"070130A00E0E0E01304000354D20B0A0B15930207575B1596C00403D41018860",
                    INIT_3C => X"0000B010D0950106079B00FEB0070130A00E0E0E01304001000E810E018600B0",
                    INIT_3D => X"102C7C01230059B4010153C00707400000010001000100010001000100010040",
                    INIT_3E => X"102C4A7C0123CDD51D010600590142014C0D00D50D020100CD01D50D0100C600",
                    INIT_3F => X"000300102C0F4A7C0123F500590142014C0D00F50D020100CD01F50D0100E600",
                   INITP_00 => X"000000B8207450E1D086083004C008A41FFFE536008278000000008F80000000",
                   INITP_01 => X"34F4A08F880B9C18E2E0C0E80420031A7FFFFFFFFFFC000007FFFFFF86590200",
                   INITP_02 => X"800104000080000000800000000A1CC1519D8E6D88D9B0CF30C618CCD87C0E03",
                   INITP_03 => X"01F80008880000035000000D40000035000000EE8207FFF780005012CFA007FF",
                   INITP_04 => X"16AB6DA841080030B58C334104100000014C00C008104104A800000006018050",
                   INITP_05 => X"FFDDD005A0040200144029010084024D9D99A692088240500D00985AB6880806",
                   INITP_06 => X"CB11004B6582D5559555996220B6580B0CE000000019659659600000000280BF",
                   INITP_07 => X"8C27FFBE43C7FFBE4188AAAA85D08375105AAEFFB29776FDB95A0096AAACAAAC")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_ll(31 downto 0),
                  DOPA => data_out_a_ll(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_ll(31 downto 0),
                  DOPB => data_out_b_ll(35 downto 32), 
                   DIB => data_in_b_ll(31 downto 0),
                  DIPB => data_in_b_ll(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_lh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481600",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"0800000000092800705858B0E85828787808280000F0C9F0C909095858010028",
                    INIT_05 => X"0505815859D0E8580028001090E98900B0E81800680069000050C98968080068",
                    INIT_06 => X"560E0EB0E890E8582868086B6B6A6A68284B4B4A4A9068481868086828002800",
                    INIT_07 => X"0000000000680800680800680800000068080068080000000EB0EE560E560E10",
                    INIT_08 => X"0828005008035088035088025088025008B0E85828B0CE8E0000000000000000",
                    INIT_09 => X"000008000D0D0808080808080808080808080808080808080808080808080808",
                    INIT_0A => X"0808080808080808080808080808280000006808000000680800000068080000",
                    INIT_0B => X"0D0D080808080808080808080808080808080808080808080808080808080808",
                    INIT_0C => X"005800000D0D006808006800690000006808006808001088685000F0E0590800",
                    INIT_0D => X"F0E1095A000D0D0010896800690000508008680800680800F0E1095A68006900",
                    INIT_0E => X"02680800680069000000000D0D00F0E1095A0010896800690000508008680800",
                    INIT_0F => X"8D00109D8D680091E890E8250D0D280010CB680069000050D0E38B038AA2A289",
                    INIT_10 => X"6E2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A018B8B02800109D",
                    INIT_11 => X"01286E2E010101016E2E0101016E1E280101016E1E0101016E1E01016E2E0101",
                    INIT_12 => X"1ED1A2010A2801016E2E016E1E01016E2E016E1E280101016E1E01016E2E6E2E",
                    INIT_13 => X"0A2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E01016E2E016E2E116E",
                    INIT_14 => X"D10102D10102D101020118B8B1B1B0B02801F1A26E1E01016E2E01A26B4B010A",
                    INIT_15 => X"02D101020118B8B1B0B028B8A0080100110811081108110828B8A00801D10102",
                    INIT_16 => X"0102D101020118B8B0B028B8A008010011081108110828B8A00801D10102D101",
                    INIT_17 => X"0828B8A008010011081108110828B8B1B1A00801010101010101D101022801D1",
                    INIT_18 => X"D1EA518A51D1EA520A0A500A500AB0E8582800000D0D08080808080808080808",
                    INIT_19 => X"B608280108280108280108280000000000D101500A500AB0E85828D10128D101",
                    INIT_1A => X"0E286818500AB1E8582800000D0D080808080808080808080808080828010101",
                    INIT_1B => X"CA000000000000000000680800680800008818A0A0A0A028F1CEF1CFF1CF0F0F",
                    INIT_1C => X"DBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDBDA",
                    INIT_1D => X"C8DBDBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DB",
                    INIT_1E => X"CB88CB88CA88CA000000000000000000680800680800008018A0A0A0A028E20A",
                    INIT_1F => X"A0A0A0A000221A18A0A0A0A0000088A0A0A0A000000000006808000D050E2888",
                    INIT_20 => X"A0A0A0A0001A01008818A0A0A0A0002A010088A0A0A0A000000B0B0A0A080088",
                    INIT_21 => X"010001000100000000000000000000680800680800008818A0A0A0A000008818",
                    INIT_22 => X"A0001DEDB1EE8EA512A59201090012A59201090012A59201090012A592010900",
                    INIT_23 => X"A6A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E0100008092518A0A0",
                    INIT_24 => X"08926A00080108926A00080108926A000828000000121A0088A0A0A0A000A6A6",
                    INIT_25 => X"CFB218480F0F2868080268921848284828F2CF0F28026F0F280108926A000801",
                    INIT_26 => X"0201D20202D20202D202022018A0A0A0A0A0A00228A10928A1096808481292DF",
                    INIT_27 => X"000200680008680008006900680008680008680008680008091209120928A008",
                    INIT_28 => X"68000868000868000828A008D20202000200022018A0A0A0A0A0A00228A10900",
                    INIT_29 => X"68282808129268926848080868282808D26828A1090000020068000868000800",
                    INIT_2A => X"4868280892684828926848682808926848D26828A00828A00892689268480808",
                    INIT_2B => X"0208090812F2A1A002080908D2682812F2A102080912F2A1020809D268289268",
                    INIT_2C => X"684848486828280892684828A0926848484868282808926848D2682812F2A1A0",
                    INIT_2D => X"A0A0A0A008926848289268484848682828A0A008926848D26828A0A0A0A0A092",
                    INIT_2E => X"085008B0E8582800000228025008B0E85828000002289268484848682828A0A0",
                    INIT_2F => X"0808080808080808080828025008500851885108B0E858280000000000D20250",
                    INIT_30 => X"00680800680800680800680800D3025008B0E8582800000D0D00080808080808",
                    INIT_31 => X"020200020800F302005108B0E858280068080068080068080068080068080028",
                    INIT_32 => X"0000000000000000000000050200050200040200040200030200030200020200",
                    INIT_33 => X"B0E8582802000052085108B0E858280068080068080068080028000000000000",
                    INIT_34 => X"E85008B0E85828000200020800020800F302005108B0E8582800000002005108",
                    INIT_35 => X"50000A0200508A0200508A0200508A0250000A020800F302005108B0E85008B0",
                    INIT_36 => X"00020800F302005108B0E858280002000208000200508A0200508A0200508A02",
                    INIT_37 => X"020800F302005108B0E85008B0E85008B0E85828000000000002000202000208",
                    INIT_38 => X"0200508A0200508A0200508A0250000A0200508A0200508A0200508A0250000A",
                    INIT_39 => X"0200080208000A09F30200030B5108B0E8582800000000000200020200020800",
                    INIT_3A => X"02000013020BB302B3EBA3030BD3E3130BB3E313A3A0A00B93E893E8A00200A0",
                    INIT_3B => X"1DCD0585A5A5A5CD050D2813020001010300050513130300B3EB03B3E9890300",
                    INIT_3C => X"082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D1828A513A5CD93ED55",
                    INIT_3D => X"5108B0E8582800F3CECE00500E8E0E2870887088708870887088708870887008",
                    INIT_3E => X"510804B0E85813B3E8480028006808680868081368282808B3E913682808B3E9",
                    INIT_3F => X"08B4E951080904B0E8581328006808680868081368282808B3E913682808B3E9",
                   INITP_00 => X"6DB6B64114DF845FC4F98F8B7393BD1FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"6753BE805FA637C589BE2D8FB37EB6B13FFFFFFFFFFFAD6B53FFFFFFE0004CDB",
                   INITP_02 => X"E7561B73B533AB3B6A33AACEDB50EBA22F7375DB57B76FBAEFBDF7BBB7D3382E",
                   INITP_03 => X"00040DA355AAB60486AAD8121AAB60486AAD81A86273FFFF36DD613F840273FF",
                   INITP_04 => X"FB7DB65436E019264372CED55555790018A08A0008B2CB2CAB556C040300C082",
                   INITP_05 => X"FFF009D613F27E1004C209410894225C8C8BA696499252508D08A7EDDB4EA019",
                   INITP_06 => X"98889D4A4C4E91111111131113A4C4E89813B6F5554924925313B6DBDB6E4E7F",
                   INITP_07 => X"433D58B2333D58B22721AAAA644000A40038CF92929550A490D13A9488888888")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_lh(31 downto 0),
                  DOPA => data_out_a_lh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_lh(31 downto 0),
                  DOPB => data_out_b_lh(35 downto 32), 
                   DIB => data_in_b_lh(31 downto 0),
                  DIPB => data_in_b_lh(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      --
      kcpsm6_rom_hl: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"3441383001067C590142014C0D0000590142014C0D001A0D020100CD011A0D01",
                    INIT_01 => X"06120001061A0106388101061A0106010601061A120147513B642D5538753850",
                    INIT_02 => X"000D08007C01650D005501000D04005A00102C7C01230A0106421A0101060201",
                    INIT_03 => X"010C020202801007006C01000C0C0C0C1C07070C07003C001C3C00650D006001",
                    INIT_04 => X"0069990101406B78669F0380000287010C0C010C02020280100C07007901020C",
                    INIT_05 => X"660600A6697866C7BE00697840786601BE000069A90C6B78660500696B786605",
                    INIT_06 => X"01406B80784001784001784078660302407CCF80016003400069786604006978",
                    INIT_07 => X"0266BE7C80017C005003400040014001F440014001F44060404060034069DD01",
                    INIT_08 => X"4001400120400140012040504040500340690901017840800340017840024078",
                    INIT_09 => X"1034800606062C7C082A7C08297C01287C032300C80080034000F50080034000",
                    INIT_0A => X"005953A00059533001533001533002809400B700463401012010303F38010120",
                    INIT_0B => X"00595340015340015340024000596C010101201003402C7C012300AE2C7C0123",
                    INIT_0C => X"6169746C756D0059930101535080400340C80059810959820707400153400780",
                    INIT_0D => X"76C20000746573657292000072776D656D9A0000706D75646D656DB700006772",
                    INIT_0E => X"72775F6765725F633269F20100706C65687E01007379733A01006E6F69737265",
                    INIT_0F => X"32693503006364745F6C65735F63326926030064725F6765725F6332690F0300",
                    INIT_10 => X"6765725F71616438030062645F6C65735F6332693B03006361645F6C65735F63",
                    INIT_11 => X"CB05007375746174735F636474D8000064725F6765725F7161640C010072775F",
                    INIT_12 => X"775F6765725F636474D60500646165725F636474CF050065746972775F636474",
                    INIT_13 => X"740C06006863726165735F706D6574DA050064725F6765725F636474E9050072",
                    INIT_14 => X"706D657432060064695F7465675F706D65742E07007364695F7465675F706D65",
                    INIT_15 => X"6E5F766E6F635F706D65747D0600646165725F706D657472060065746972775F",
                    INIT_16 => X"9A0600766E6F635F706D6574D40600616E5F746E6972705F706D657488060061",
                    INIT_17 => X"7273664D090065664F090069668F0400726463ED0600746E6972705F706D6574",
                    INIT_18 => X"09007261668009007277666609007761662D09007777666009007773665C0900",
                    INIT_19 => X"74657365725F636474BB07006C6C6568735F6364742709007266210900776674",
                    INIT_1A => X"01007764F60700676F72705F636474DA0700726E5F676F72705F6364744A0800",
                    INIT_1B => X"0001810092FFA06A00010179102079002092FF8500A0009A09FFD8000072640C",
                    INIT_1C => X"01982F0120C0102023000060CD8E00608E10A0000110A00001E5966A00000379",
                    INIT_1D => X"9800012823001001242320AE01B52F20B52010AE1020082010002323C0050123",
                    INIT_1E => X"02412064D20B00203A726F727245005964C10B00646E616D6D6F632064614200",
                    INIT_1F => X"50EE02012C0606405010402800402410002300E8200110002C04380059010201",
                    INIT_20 => X"764F1167302820200120100020010600EE702F60EE5001FD01701C0160016009",
                    INIT_21 => X"0C0020012041204D08460A460D00012011608E59641E0C590021776F6C667265",
                    INIT_22 => X"002001202001200108020120020108025C0220000C0120012059000C00010200",
                    INIT_23 => X"000000000000000000000000000000000000000000000000000000000000000C",
                    INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"8DEE73804492000841090441146580660130680C718E21133265455558FCFDF3",
                   INITP_01 => X"FFFFFFFFFFFFFFFFFFFFFFFFFCA4090000034A0000930D3A40000618EF7BB288",
                   INITP_02 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_03 => X"4F381C8407FC3FFEC5F4FF6845083375BF927CFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_04 => X"000000000000000000000000000000000000000001B6000402AC02FFD3D88988",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hl(31 downto 0),
                  DOPA => data_out_a_hl(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hl(31 downto 0),
                  DOPB => data_out_b_hl(35 downto 32), 
                   DIB => data_in_b_hl(31 downto 0),
                  DIPB => data_in_b_hl(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_hh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"94E894E84800100068086808680828006808680868081468282808B3E9146828",
                    INIT_01 => X"00B4E8480014480094E84800144800480048001494C994E894E894E894E894E8",
                    INIT_02 => X"08682808B0E9146808F48808682808B4E95108B0E8581448001494C849008848",
                    INIT_03 => X"3969A1A1A119010928F4C9A0A1A1A14958E9E95809285828585828146808F488",
                    INIT_04 => X"2804B4CA8A70040404080A0A28A0F4C9A0693969A1A1A11901A00928F4C9A069",
                    INIT_05 => X"0408280404040408042804040004040804022804D4A004040408280404040408",
                    INIT_06 => X"8A70040A0450CA0450CA04500404088A0A10D4EBCB538B0B2804040408280404",
                    INIT_07 => X"080404F0EACA90EA528A0A28728A528AF4728A528AF47282520A538B0B04F4CB",
                    INIT_08 => X"7189518AF47189518AF47181510A528A0A04F4CA8A04500AF4EACA04508A0A04",
                    INIT_09 => X"500889A1A1A159B0E858B0E858B0E858B0E85828047008880828047008880828",
                    INIT_0A => X"2800000428000050C90050C90050890904280428D4E88988705008D4E8898870",
                    INIT_0B => X"28000050CA0050CA00508A0A2800B4C989887050090908B0E858280458B0E858",
                    INIT_0C => X"0808080808082800B4CA8A00500A528A0A042800F48A00B4E81800CA00508A0A",
                    INIT_0D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_0E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_0F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_10 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_11 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_12 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_13 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_14 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_15 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_16 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_17 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_18 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_19 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1B => X"9D8D95E895E825159D8D89B5E050F5E15895E895E825090D0D08080808080808",
                    INIT_1C => X"C9F5008950F5E15878082800050028000021259D8D01259D8D050515099D8D15",
                    INIT_1D => X"1571C88858C150C88858011589F5005095E101D5E15889017180087895E88858",
                    INIT_1E => X"000000000D0D08080808080808082800000D0D08080808080808080808080828",
                    INIT_1F => X"E3D5CB8A8BA3A30383538008528008F6E2580AD5E08870080889092800680069",
                    INIT_20 => X"080816C506F6E878885870885848002815700050B5E38B158B7000CB508B51D6",
                    INIT_21 => X"A00878C858F6E896E896E896E850C85816000000000D0D000808080808080808",
                    INIT_22 => X"0878C85878C858680800680800680800D6C85828A00878C8580028A008680028",
                    INIT_23 => X"00000000000000000000000000000000000000000000000000000000000028A0",
                    INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"7218C606692C60F7BEF6BBBEE78A501940C097FA4B4912E4CD9ABAAAA7ABAB16",
                   INITP_01 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF907B08E48E2134FE48F89101249C718C606491",
                   INITP_02 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_03 => X"4001121FD3FF9FFFC016909864BFE2712B12547FFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_04 => X"0000000000000000000000000000000000000002496D92672550F9FFFD23E961",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hh(31 downto 0),
                  DOPA => data_out_a_hh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hh(31 downto 0),
                  DOPB => data_out_b_hh(35 downto 32), 
                   DIB => data_in_b_hh(31 downto 0),
                  DIPB => data_in_b_hh(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004001181",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"3A0253413000005900302C7C02230023200000603A87018701FFFF01313E8E00",
                    INIT_05 => X"E596102B27C104237E00599BB5000459A5000330010201024130010401200201",
                    INIT_06 => X"C02C017C01E102230003F408070605040008070605C81003FF0308040081007E",
                    INIT_07 => X"600253700201780201300201200253C0017802013002C4C001E800D030C02CE8",
                    INIT_08 => X"6700D0002C1000011000011000011000307C022300E801015953400253500253",
                    INIT_09 => X"70C401641F0100203A56206472616F426369676F4C2072656B6361725420322D",
                    INIT_0A => X"2064726F5700203A72656666754200595340012E025350012E025360012E0253",
                    INIT_0B => X"5201002064726F5700202020203A73657A69732064726F5700203A746E756F63",
                    INIT_0C => X"412302645B015901290201020102412001280201200283010110028A20200064",
                    INIT_0D => X"CD40002364680159A6010102010241103024014002012002B840002301020102",
                    INIT_0E => X"3001200201020102413064780159F040002359BE010102010241103028012002",
                    INIT_0F => X"0359F40001010202FFFE00A09A090059E601010201024160D05003502C060601",
                    INIT_10 => X"02020002FE0E0E0E0E02FD020201000F013E00000200F007070301000059F400",
                    INIT_11 => X"0E0002020E0E0E0E02010E0E0E02FD000E0E0E02FE0E0E0E02FD0E0E02010E0E",
                    INIT_12 => X"FD62060E80000E0E02020E02FE0E0E02010E02FD000E0E0E02FE0E0E02010202",
                    INIT_13 => X"000200020E0E0E02FE0E0E0201020E02020E5C6E0E02FE0E0E02010E02026402",
                    INIT_14 => X"AC5B20AA5B10A85B0012FE0130201000000E810E02FE0E0E02010E0002020E80",
                    INIT_15 => X"10CA5B0012FE01201000000006803153B004B003B002B0010000060031AE5B30",
                    INIT_16 => X"5B10F35B0012FE011000000006803153D003D002D0010000060031CE5B20CC5B",
                    INIT_17 => X"41000006803153F903F902F901000030200600313F407E4C407EF75B00011EAA",
                    INIT_18 => X"23034001401D03502A344030402C7C0323005964FF02002E726F727265206B63",
                    INIT_19 => X"00F30004020004010004000059533053200AD64030402C7C0223000AB6000A90",
                    INIT_1A => X"FF00031F402C52012300596444030073746E656D75677261206461420035383B",
                    INIT_1B => X"00595340535053605370012002014902C4B2300606060600620162016201FFFF",
                    INIT_1C => X"078900595340535053605370012002014B02C452300606060600408000000789",
                    INIT_1D => X"0000078900595340535053605370012002014C02C4B030060606060040800000",
                    INIT_1E => X"00000000000000595340535053605370012002015A02C4203006060606004080",
                    INIT_1F => X"06060606B0108F7006060606C0C45006060606B05953C0020150020000010000",
                    INIT_20 => X"06060606B0FC5FD0503006060606B0035FC45006060606B0D00000000120D050",
                    INIT_21 => X"69B087B0A5B0595340535053605370012002015402C4B43006060606B0D05030",
                    INIT_22 => X"06A01F1FE5060107590659C357B0590652C358B059064CC356B0590646C3B5B0",
                    INIT_23 => X"06060759531010007305790E01000E0E66E0020F6D0E6C10A000070000E00606",
                    INIT_24 => X"02A104C440E2019B02C440E2009501C440005959D0C07FC45006060606B00606",
                    INIT_25 => X"01C30209FFFF000920AC0AB20109000900AD01FF00AC098000E203A708C440E2",
                    INIT_26 => X"5040E7BA00E5BA00E3BAB2100F060606060607A8000E0100060009100ABCC900",
                    INIT_27 => X"53B00201023A01025359010201023A01025201025201024533E832E831000E00",
                    INIT_28 => X"010252010252010245000E0017BAB220B230B2100F060606060606A8000E0159",
                    INIT_29 => X"0B208000474A0134020B00000B0208003C01000E015953B00201023A01025359",
                    INIT_2A => X"0B0B20C059200B0055020B0B020C4F020B5901000E01000E004A1040200B0000",
                    INIT_2B => X"8501800084760E08850080007C0100716C0EA7018071660EA700806B01005F20",
                    INIT_2C => X"200B0B0B0B40200095200B000E8E020B0B0B0B04020087020B950100847E0E08",
                    INIT_2D => X"0606000E00B8200B00B2020B0B0B0B0002000E00A9020BB801000E0E0E0E0E9C",
                    INIT_2E => X"30102C7C0223005953BA00B2102C7C0123005953B000C5200B0B0B0B00200606",
                    INIT_2F => X"20656369766564206F4E00041030102C100110347C0323005953305340E8CC10",
                    INIT_30 => X"59014502014E02014F02014E02212E102C7C0123005964F6055900646E756F66",
                    INIT_31 => X"107230633330672E30102C7C01230059014402014E02015502014F0201460200",
                    INIT_32 => X"6053705380539053A053B0107230107230107230107230107230107230107230",
                    INIT_33 => X"7C0123006340301030102C7C0223005901520201520201450200595340535053",
                    INIT_34 => X"0810297C032300594D3063443063CC30062E30102C7C0123005953107230102C",
                    INIT_35 => X"40303063304001633040016330400163403034635530062E30102C7C08102A7C",
                    INIT_36 => X"3063CC30062E30102C7C012300594D3063443063304001633040016330400163",
                    INIT_37 => X"635530062E30102C7C08102A7C0810297C0323005953405310723010723063BE",
                    INIT_38 => X"6330400163304001633040016340303063304001633040016330400163403034",
                    INIT_39 => X"85200063F0200001062E209F00102C7C0123005953405310723010723063BE30",
                    INIT_3A => X"A720605D30005D305D000076005A305D0151305D000E0E004D006B0300852000",
                    INIT_3B => X"070130A00E0E0E01304000354D20B0A0B15930207575B1596C00403D41018860",
                    INIT_3C => X"0000B010D0950106079B00FEB0070130A00E0E0E01304001000E810E018600B0",
                    INIT_3D => X"102C7C01230059B4010153C00707400000010001000100010001000100010040",
                    INIT_3E => X"102C4A7C0123CDD51D010600590142014C0D00D50D020100CD01D50D0100C600",
                    INIT_3F => X"000300102C0F4A7C0123F500590142014C0D00F50D020100CD01F50D0100E600",
                    INIT_40 => X"3441383001067C590142014C0D0000590142014C0D001A0D020100CD011A0D01",
                    INIT_41 => X"06120001061A0106388101061A0106010601061A120147513B642D5538753850",
                    INIT_42 => X"000D08007C01650D005501000D04005A00102C7C01230A0106421A0101060201",
                    INIT_43 => X"010C020202801007006C01000C0C0C0C1C07070C07003C001C3C00650D006001",
                    INIT_44 => X"0069990101406B78669F0380000287010C0C010C02020280100C07007901020C",
                    INIT_45 => X"660600A6697866C7BE00697840786601BE000069A90C6B78660500696B786605",
                    INIT_46 => X"01406B80784001784001784078660302407CCF80016003400069786604006978",
                    INIT_47 => X"0266BE7C80017C005003400040014001F440014001F44060404060034069DD01",
                    INIT_48 => X"4001400120400140012040504040500340690901017840800340017840024078",
                    INIT_49 => X"1034800606062C7C082A7C08297C01287C032300C80080034000F50080034000",
                    INIT_4A => X"005953A00059533001533001533002809400B700463401012010303F38010120",
                    INIT_4B => X"00595340015340015340024000596C010101201003402C7C012300AE2C7C0123",
                    INIT_4C => X"6169746C756D0059930101535080400340C80059810959820707400153400780",
                    INIT_4D => X"76C20000746573657292000072776D656D9A0000706D75646D656DB700006772",
                    INIT_4E => X"72775F6765725F633269F20100706C65687E01007379733A01006E6F69737265",
                    INIT_4F => X"32693503006364745F6C65735F63326926030064725F6765725F6332690F0300",
                    INIT_50 => X"6765725F71616438030062645F6C65735F6332693B03006361645F6C65735F63",
                    INIT_51 => X"CB05007375746174735F636474D8000064725F6765725F7161640C010072775F",
                    INIT_52 => X"775F6765725F636474D60500646165725F636474CF050065746972775F636474",
                    INIT_53 => X"740C06006863726165735F706D6574DA050064725F6765725F636474E9050072",
                    INIT_54 => X"706D657432060064695F7465675F706D65742E07007364695F7465675F706D65",
                    INIT_55 => X"6E5F766E6F635F706D65747D0600646165725F706D657472060065746972775F",
                    INIT_56 => X"9A0600766E6F635F706D6574D40600616E5F746E6972705F706D657488060061",
                    INIT_57 => X"7273664D090065664F090069668F0400726463ED0600746E6972705F706D6574",
                    INIT_58 => X"09007261668009007277666609007761662D09007777666009007773665C0900",
                    INIT_59 => X"74657365725F636474BB07006C6C6568735F6364742709007266210900776674",
                    INIT_5A => X"01007764F60700676F72705F636474DA0700726E5F676F72705F6364744A0800",
                    INIT_5B => X"0001810092FFA06A00010179102079002092FF8500A0009A09FFD8000072640C",
                    INIT_5C => X"01982F0120C0102023000060CD8E00608E10A0000110A00001E5966A00000379",
                    INIT_5D => X"9800012823001001242320AE01B52F20B52010AE1020082010002323C0050123",
                    INIT_5E => X"02412064D20B00203A726F727245005964C10B00646E616D6D6F632064614200",
                    INIT_5F => X"50EE02012C0606405010402800402410002300E8200110002C04380059010201",
                    INIT_60 => X"764F1167302820200120100020010600EE702F60EE5001FD01701C0160016009",
                    INIT_61 => X"0C0020012041204D08460A460D00012011608E59641E0C590021776F6C667265",
                    INIT_62 => X"002001202001200108020120020108025C0220000C0120012059000C00010200",
                    INIT_63 => X"000000000000000000000000000000000000000000000000000000000000000C",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"000000B8207450E1D086083004C008A41FFFE536008278000000008F80000000",
                   INITP_01 => X"34F4A08F880B9C18E2E0C0E80420031A7FFFFFFFFFFC000007FFFFFF86590200",
                   INITP_02 => X"800104000080000000800000000A1CC1519D8E6D88D9B0CF30C618CCD87C0E03",
                   INITP_03 => X"01F80008880000035000000D40000035000000EE8207FFF780005012CFA007FF",
                   INITP_04 => X"16AB6DA841080030B58C334104100000014C00C008104104A800000006018050",
                   INITP_05 => X"FFDDD005A0040200144029010084024D9D99A692088240500D00985AB6880806",
                   INITP_06 => X"CB11004B6582D5559555996220B6580B0CE000000019659659600000000280BF",
                   INITP_07 => X"8C27FFBE43C7FFBE4188AAAA85D08375105AAEFFB29776FDB95A0096AAACAAAC",
                   INITP_08 => X"8DEE73804492000841090441146580660130680C718E21133265455558FCFDF3",
                   INITP_09 => X"FFFFFFFFFFFFFFFFFFFFFFFFFCA4090000034A0000930D3A40000618EF7BB288",
                   INITP_0A => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0B => X"4F381C8407FC3FFEC5F4FF6845083375BF927CFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0C => X"000000000000000000000000000000000000000001B6000402AC02FFD3D88988",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481600",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"0800000000092800705858B0E85828787808280000F0C9F0C909095858010028",
                    INIT_05 => X"0505815859D0E8580028001090E98900B0E81800680069000050C98968080068",
                    INIT_06 => X"560E0EB0E890E8582868086B6B6A6A68284B4B4A4A9068481868086828002800",
                    INIT_07 => X"0000000000680800680800680800000068080068080000000EB0EE560E560E10",
                    INIT_08 => X"0828005008035088035088025088025008B0E85828B0CE8E0000000000000000",
                    INIT_09 => X"000008000D0D0808080808080808080808080808080808080808080808080808",
                    INIT_0A => X"0808080808080808080808080808280000006808000000680800000068080000",
                    INIT_0B => X"0D0D080808080808080808080808080808080808080808080808080808080808",
                    INIT_0C => X"005800000D0D006808006800690000006808006808001088685000F0E0590800",
                    INIT_0D => X"F0E1095A000D0D0010896800690000508008680800680800F0E1095A68006900",
                    INIT_0E => X"02680800680069000000000D0D00F0E1095A0010896800690000508008680800",
                    INIT_0F => X"8D00109D8D680091E890E8250D0D280010CB680069000050D0E38B038AA2A289",
                    INIT_10 => X"6E2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A018B8B02800109D",
                    INIT_11 => X"01286E2E010101016E2E0101016E1E280101016E1E0101016E1E01016E2E0101",
                    INIT_12 => X"1ED1A2010A2801016E2E016E1E01016E2E016E1E280101016E1E01016E2E6E2E",
                    INIT_13 => X"0A2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E01016E2E016E2E116E",
                    INIT_14 => X"D10102D10102D101020118B8B1B1B0B02801F1A26E1E01016E2E01A26B4B010A",
                    INIT_15 => X"02D101020118B8B1B0B028B8A0080100110811081108110828B8A00801D10102",
                    INIT_16 => X"0102D101020118B8B0B028B8A008010011081108110828B8A00801D10102D101",
                    INIT_17 => X"0828B8A008010011081108110828B8B1B1A00801010101010101D101022801D1",
                    INIT_18 => X"D1EA518A51D1EA520A0A500A500AB0E8582800000D0D08080808080808080808",
                    INIT_19 => X"B608280108280108280108280000000000D101500A500AB0E85828D10128D101",
                    INIT_1A => X"0E286818500AB1E8582800000D0D080808080808080808080808080828010101",
                    INIT_1B => X"CA000000000000000000680800680800008818A0A0A0A028F1CEF1CFF1CF0F0F",
                    INIT_1C => X"DBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDBDA",
                    INIT_1D => X"C8DBDBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DB",
                    INIT_1E => X"CB88CB88CA88CA000000000000000000680800680800008018A0A0A0A028E20A",
                    INIT_1F => X"A0A0A0A000221A18A0A0A0A0000088A0A0A0A000000000006808000D050E2888",
                    INIT_20 => X"A0A0A0A0001A01008818A0A0A0A0002A010088A0A0A0A000000B0B0A0A080088",
                    INIT_21 => X"010001000100000000000000000000680800680800008818A0A0A0A000008818",
                    INIT_22 => X"A0001DEDB1EE8EA512A59201090012A59201090012A59201090012A592010900",
                    INIT_23 => X"A6A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E0100008092518A0A0",
                    INIT_24 => X"08926A00080108926A00080108926A000828000000121A0088A0A0A0A000A6A6",
                    INIT_25 => X"CFB218480F0F2868080268921848284828F2CF0F28026F0F280108926A000801",
                    INIT_26 => X"0201D20202D20202D202022018A0A0A0A0A0A00228A10928A1096808481292DF",
                    INIT_27 => X"000200680008680008006900680008680008680008680008091209120928A008",
                    INIT_28 => X"68000868000868000828A008D20202000200022018A0A0A0A0A0A00228A10900",
                    INIT_29 => X"68282808129268926848080868282808D26828A1090000020068000868000800",
                    INIT_2A => X"4868280892684828926848682808926848D26828A00828A00892689268480808",
                    INIT_2B => X"0208090812F2A1A002080908D2682812F2A102080912F2A1020809D268289268",
                    INIT_2C => X"684848486828280892684828A0926848484868282808926848D2682812F2A1A0",
                    INIT_2D => X"A0A0A0A008926848289268484848682828A0A008926848D26828A0A0A0A0A092",
                    INIT_2E => X"085008B0E8582800000228025008B0E85828000002289268484848682828A0A0",
                    INIT_2F => X"0808080808080808080828025008500851885108B0E858280000000000D20250",
                    INIT_30 => X"00680800680800680800680800D3025008B0E8582800000D0D00080808080808",
                    INIT_31 => X"020200020800F302005108B0E858280068080068080068080068080068080028",
                    INIT_32 => X"0000000000000000000000050200050200040200040200030200030200020200",
                    INIT_33 => X"B0E8582802000052085108B0E858280068080068080068080028000000000000",
                    INIT_34 => X"E85008B0E85828000200020800020800F302005108B0E8582800000002005108",
                    INIT_35 => X"50000A0200508A0200508A0200508A0250000A020800F302005108B0E85008B0",
                    INIT_36 => X"00020800F302005108B0E858280002000208000200508A0200508A0200508A02",
                    INIT_37 => X"020800F302005108B0E85008B0E85008B0E85828000000000002000202000208",
                    INIT_38 => X"0200508A0200508A0200508A0250000A0200508A0200508A0200508A0250000A",
                    INIT_39 => X"0200080208000A09F30200030B5108B0E8582800000000000200020200020800",
                    INIT_3A => X"02000013020BB302B3EBA3030BD3E3130BB3E313A3A0A00B93E893E8A00200A0",
                    INIT_3B => X"1DCD0585A5A5A5CD050D2813020001010300050513130300B3EB03B3E9890300",
                    INIT_3C => X"082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D1828A513A5CD93ED55",
                    INIT_3D => X"5108B0E8582800F3CECE00500E8E0E2870887088708870887088708870887008",
                    INIT_3E => X"510804B0E85813B3E8480028006808680868081368282808B3E913682808B3E9",
                    INIT_3F => X"08B4E951080904B0E8581328006808680868081368282808B3E913682808B3E9",
                    INIT_40 => X"94E894E84800100068086808680828006808680868081468282808B3E9146828",
                    INIT_41 => X"00B4E8480014480094E84800144800480048001494C994E894E894E894E894E8",
                    INIT_42 => X"08682808B0E9146808F48808682808B4E95108B0E8581448001494C849008848",
                    INIT_43 => X"3969A1A1A119010928F4C9A0A1A1A14958E9E95809285828585828146808F488",
                    INIT_44 => X"2804B4CA8A70040404080A0A28A0F4C9A0693969A1A1A11901A00928F4C9A069",
                    INIT_45 => X"0408280404040408042804040004040804022804D4A004040408280404040408",
                    INIT_46 => X"8A70040A0450CA0450CA04500404088A0A10D4EBCB538B0B2804040408280404",
                    INIT_47 => X"080404F0EACA90EA528A0A28728A528AF4728A528AF47282520A538B0B04F4CB",
                    INIT_48 => X"7189518AF47189518AF47181510A528A0A04F4CA8A04500AF4EACA04508A0A04",
                    INIT_49 => X"500889A1A1A159B0E858B0E858B0E858B0E85828047008880828047008880828",
                    INIT_4A => X"2800000428000050C90050C90050890904280428D4E88988705008D4E8898870",
                    INIT_4B => X"28000050CA0050CA00508A0A2800B4C989887050090908B0E858280458B0E858",
                    INIT_4C => X"0808080808082800B4CA8A00500A528A0A042800F48A00B4E81800CA00508A0A",
                    INIT_4D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_4E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_4F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_50 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_51 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_52 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_53 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_54 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_55 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_56 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_57 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_58 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_59 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5B => X"9D8D95E895E825159D8D89B5E050F5E15895E895E825090D0D08080808080808",
                    INIT_5C => X"C9F5008950F5E15878082800050028000021259D8D01259D8D050515099D8D15",
                    INIT_5D => X"1571C88858C150C88858011589F5005095E101D5E15889017180087895E88858",
                    INIT_5E => X"000000000D0D08080808080808082800000D0D08080808080808080808080828",
                    INIT_5F => X"E3D5CB8A8BA3A30383538008528008F6E2580AD5E08870080889092800680069",
                    INIT_60 => X"080816C506F6E878885870885848002815700050B5E38B158B7000CB508B51D6",
                    INIT_61 => X"A00878C858F6E896E896E896E850C85816000000000D0D000808080808080808",
                    INIT_62 => X"0878C85878C858680800680800680800D6C85828A00878C8580028A008680028",
                    INIT_63 => X"00000000000000000000000000000000000000000000000000000000000028A0",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"6DB6B64114DF845FC4F98F8B7393BD1FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"6753BE805FA637C589BE2D8FB37EB6B13FFFFFFFFFFFAD6B53FFFFFFE0004CDB",
                   INITP_02 => X"E7561B73B533AB3B6A33AACEDB50EBA22F7375DB57B76FBAEFBDF7BBB7D3382E",
                   INITP_03 => X"00040DA355AAB60486AAD8121AAB60486AAD81A86273FFFF36DD613F840273FF",
                   INITP_04 => X"FB7DB65436E019264372CED55555790018A08A0008B2CB2CAB556C040300C082",
                   INITP_05 => X"FFF009D613F27E1004C209410894225C8C8BA696499252508D08A7EDDB4EA019",
                   INITP_06 => X"98889D4A4C4E91111111131113A4C4E89813B6F5554924925313B6DBDB6E4E7F",
                   INITP_07 => X"433D58B2333D58B22721AAAA644000A40038CF92929550A490D13A9488888888",
                   INITP_08 => X"7218C606692C60F7BEF6BBBEE78A501940C097FA4B4912E4CD9ABAAAA7ABAB16",
                   INITP_09 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF907B08E48E2134FE48F89101249C718C606491",
                   INITP_0A => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0B => X"4001121FD3FF9FFFC016909864BFE2712B12547FFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0C => X"0000000000000000000000000000000000000002496D92672550F9FFFD23E961",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004001181",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"3A0253413000005900302C7C02230023200000603A87018701FFFF01313E8E00",
                    INIT_05 => X"E596102B27C104237E00599BB5000459A5000330010201024130010401200201",
                    INIT_06 => X"C02C017C01E102230003F408070605040008070605C81003FF0308040081007E",
                    INIT_07 => X"600253700201780201300201200253C0017802013002C4C001E800D030C02CE8",
                    INIT_08 => X"6700D0002C1000011000011000011000307C022300E801015953400253500253",
                    INIT_09 => X"70C401641F0100203A56206472616F426369676F4C2072656B6361725420322D",
                    INIT_0A => X"2064726F5700203A72656666754200595340012E025350012E025360012E0253",
                    INIT_0B => X"5201002064726F5700202020203A73657A69732064726F5700203A746E756F63",
                    INIT_0C => X"412302645B015901290201020102412001280201200283010110028A20200064",
                    INIT_0D => X"CD40002364680159A6010102010241103024014002012002B840002301020102",
                    INIT_0E => X"3001200201020102413064780159F040002359BE010102010241103028012002",
                    INIT_0F => X"0359F40001010202FFFE00A09A090059E601010201024160D05003502C060601",
                    INIT_10 => X"02020002FE0E0E0E0E02FD020201000F013E00000200F007070301000059F400",
                    INIT_11 => X"0E0002020E0E0E0E02010E0E0E02FD000E0E0E02FE0E0E0E02FD0E0E02010E0E",
                    INIT_12 => X"FD62060E80000E0E02020E02FE0E0E02010E02FD000E0E0E02FE0E0E02010202",
                    INIT_13 => X"000200020E0E0E02FE0E0E0201020E02020E5C6E0E02FE0E0E02010E02026402",
                    INIT_14 => X"AC5B20AA5B10A85B0012FE0130201000000E810E02FE0E0E02010E0002020E80",
                    INIT_15 => X"10CA5B0012FE01201000000006803153B004B003B002B0010000060031AE5B30",
                    INIT_16 => X"5B10F35B0012FE011000000006803153D003D002D0010000060031CE5B20CC5B",
                    INIT_17 => X"41000006803153F903F902F901000030200600313F407E4C407EF75B00011EAA",
                    INIT_18 => X"23034001401D03502A344030402C7C0323005964FF02002E726F727265206B63",
                    INIT_19 => X"00F30004020004010004000059533053200AD64030402C7C0223000AB6000A90",
                    INIT_1A => X"FF00031F402C52012300596444030073746E656D75677261206461420035383B",
                    INIT_1B => X"00595340535053605370012002014902C4B2300606060600620162016201FFFF",
                    INIT_1C => X"078900595340535053605370012002014B02C452300606060600408000000789",
                    INIT_1D => X"0000078900595340535053605370012002014C02C4B030060606060040800000",
                    INIT_1E => X"00000000000000595340535053605370012002015A02C4203006060606004080",
                    INIT_1F => X"06060606B0108F7006060606C0C45006060606B05953C0020150020000010000",
                    INIT_20 => X"06060606B0FC5FD0503006060606B0035FC45006060606B0D00000000120D050",
                    INIT_21 => X"69B087B0A5B0595340535053605370012002015402C4B43006060606B0D05030",
                    INIT_22 => X"06A01F1FE5060107590659C357B0590652C358B059064CC356B0590646C3B5B0",
                    INIT_23 => X"06060759531010007305790E01000E0E66E0020F6D0E6C10A000070000E00606",
                    INIT_24 => X"02A104C440E2019B02C440E2009501C440005959D0C07FC45006060606B00606",
                    INIT_25 => X"01C30209FFFF000920AC0AB20109000900AD01FF00AC098000E203A708C440E2",
                    INIT_26 => X"5040E7BA00E5BA00E3BAB2100F060606060607A8000E0100060009100ABCC900",
                    INIT_27 => X"53B00201023A01025359010201023A01025201025201024533E832E831000E00",
                    INIT_28 => X"010252010252010245000E0017BAB220B230B2100F060606060606A8000E0159",
                    INIT_29 => X"0B208000474A0134020B00000B0208003C01000E015953B00201023A01025359",
                    INIT_2A => X"0B0B20C059200B0055020B0B020C4F020B5901000E01000E004A1040200B0000",
                    INIT_2B => X"8501800084760E08850080007C0100716C0EA7018071660EA700806B01005F20",
                    INIT_2C => X"200B0B0B0B40200095200B000E8E020B0B0B0B04020087020B950100847E0E08",
                    INIT_2D => X"0606000E00B8200B00B2020B0B0B0B0002000E00A9020BB801000E0E0E0E0E9C",
                    INIT_2E => X"30102C7C0223005953BA00B2102C7C0123005953B000C5200B0B0B0B00200606",
                    INIT_2F => X"20656369766564206F4E00041030102C100110347C0323005953305340E8CC10",
                    INIT_30 => X"59014502014E02014F02014E02212E102C7C0123005964F6055900646E756F66",
                    INIT_31 => X"107230633330672E30102C7C01230059014402014E02015502014F0201460200",
                    INIT_32 => X"6053705380539053A053B0107230107230107230107230107230107230107230",
                    INIT_33 => X"7C0123006340301030102C7C0223005901520201520201450200595340535053",
                    INIT_34 => X"0810297C032300594D3063443063CC30062E30102C7C0123005953107230102C",
                    INIT_35 => X"40303063304001633040016330400163403034635530062E30102C7C08102A7C",
                    INIT_36 => X"3063CC30062E30102C7C012300594D3063443063304001633040016330400163",
                    INIT_37 => X"635530062E30102C7C08102A7C0810297C0323005953405310723010723063BE",
                    INIT_38 => X"6330400163304001633040016340303063304001633040016330400163403034",
                    INIT_39 => X"85200063F0200001062E209F00102C7C0123005953405310723010723063BE30",
                    INIT_3A => X"A720605D30005D305D000076005A305D0151305D000E0E004D006B0300852000",
                    INIT_3B => X"070130A00E0E0E01304000354D20B0A0B15930207575B1596C00403D41018860",
                    INIT_3C => X"0000B010D0950106079B00FEB0070130A00E0E0E01304001000E810E018600B0",
                    INIT_3D => X"102C7C01230059B4010153C00707400000010001000100010001000100010040",
                    INIT_3E => X"102C4A7C0123CDD51D010600590142014C0D00D50D020100CD01D50D0100C600",
                    INIT_3F => X"000300102C0F4A7C0123F500590142014C0D00F50D020100CD01F50D0100E600",
                    INIT_40 => X"3441383001067C590142014C0D0000590142014C0D001A0D020100CD011A0D01",
                    INIT_41 => X"06120001061A0106388101061A0106010601061A120147513B642D5538753850",
                    INIT_42 => X"000D08007C01650D005501000D04005A00102C7C01230A0106421A0101060201",
                    INIT_43 => X"010C020202801007006C01000C0C0C0C1C07070C07003C001C3C00650D006001",
                    INIT_44 => X"0069990101406B78669F0380000287010C0C010C02020280100C07007901020C",
                    INIT_45 => X"660600A6697866C7BE00697840786601BE000069A90C6B78660500696B786605",
                    INIT_46 => X"01406B80784001784001784078660302407CCF80016003400069786604006978",
                    INIT_47 => X"0266BE7C80017C005003400040014001F440014001F44060404060034069DD01",
                    INIT_48 => X"4001400120400140012040504040500340690901017840800340017840024078",
                    INIT_49 => X"1034800606062C7C082A7C08297C01287C032300C80080034000F50080034000",
                    INIT_4A => X"005953A00059533001533001533002809400B700463401012010303F38010120",
                    INIT_4B => X"00595340015340015340024000596C010101201003402C7C012300AE2C7C0123",
                    INIT_4C => X"6169746C756D0059930101535080400340C80059810959820707400153400780",
                    INIT_4D => X"76C20000746573657292000072776D656D9A0000706D75646D656DB700006772",
                    INIT_4E => X"72775F6765725F633269F20100706C65687E01007379733A01006E6F69737265",
                    INIT_4F => X"32693503006364745F6C65735F63326926030064725F6765725F6332690F0300",
                    INIT_50 => X"6765725F71616438030062645F6C65735F6332693B03006361645F6C65735F63",
                    INIT_51 => X"CB05007375746174735F636474D8000064725F6765725F7161640C010072775F",
                    INIT_52 => X"775F6765725F636474D60500646165725F636474CF050065746972775F636474",
                    INIT_53 => X"740C06006863726165735F706D6574DA050064725F6765725F636474E9050072",
                    INIT_54 => X"706D657432060064695F7465675F706D65742E07007364695F7465675F706D65",
                    INIT_55 => X"6E5F766E6F635F706D65747D0600646165725F706D657472060065746972775F",
                    INIT_56 => X"9A0600766E6F635F706D6574D40600616E5F746E6972705F706D657488060061",
                    INIT_57 => X"7273664D090065664F090069668F0400726463ED0600746E6972705F706D6574",
                    INIT_58 => X"09007261668009007277666609007761662D09007777666009007773665C0900",
                    INIT_59 => X"74657365725F636474BB07006C6C6568735F6364742709007266210900776674",
                    INIT_5A => X"01007764F60700676F72705F636474DA0700726E5F676F72705F6364744A0800",
                    INIT_5B => X"0001810092FFA06A00010179102079002092FF8500A0009A09FFD8000072640C",
                    INIT_5C => X"01982F0120C0102023000060CD8E00608E10A0000110A00001E5966A00000379",
                    INIT_5D => X"9800012823001001242320AE01B52F20B52010AE1020082010002323C0050123",
                    INIT_5E => X"02412064D20B00203A726F727245005964C10B00646E616D6D6F632064614200",
                    INIT_5F => X"50EE02012C0606405010402800402410002300E8200110002C04380059010201",
                    INIT_60 => X"764F1167302820200120100020010600EE702F60EE5001FD01701C0160016009",
                    INIT_61 => X"0C0020012041204D08460A460D00012011608E59641E0C590021776F6C667265",
                    INIT_62 => X"002001202001200108020120020108025C0220000C0120012059000C00010200",
                    INIT_63 => X"000000000000000000000000000000000000000000000000000000000000000C",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"000000B8207450E1D086083004C008A41FFFE536008278000000008F80000000",
                   INITP_01 => X"34F4A08F880B9C18E2E0C0E80420031A7FFFFFFFFFFC000007FFFFFF86590200",
                   INITP_02 => X"800104000080000000800000000A1CC1519D8E6D88D9B0CF30C618CCD87C0E03",
                   INITP_03 => X"01F80008880000035000000D40000035000000EE8207FFF780005012CFA007FF",
                   INITP_04 => X"16AB6DA841080030B58C334104100000014C00C008104104A800000006018050",
                   INITP_05 => X"FFDDD005A0040200144029010084024D9D99A692088240500D00985AB6880806",
                   INITP_06 => X"CB11004B6582D5559555996220B6580B0CE000000019659659600000000280BF",
                   INITP_07 => X"8C27FFBE43C7FFBE4188AAAA85D08375105AAEFFB29776FDB95A0096AAACAAAC",
                   INITP_08 => X"8DEE73804492000841090441146580660130680C718E21133265455558FCFDF3",
                   INITP_09 => X"FFFFFFFFFFFFFFFFFFFFFFFFFCA4090000034A0000930D3A40000618EF7BB288",
                   INITP_0A => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0B => X"4F381C8407FC3FFEC5F4FF6845083375BF927CFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0C => X"000000000000000000000000000000000000000001B6000402AC02FFD3D88988",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481600",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"0800000000092800705858B0E85828787808280000F0C9F0C909095858010028",
                    INIT_05 => X"0505815859D0E8580028001090E98900B0E81800680069000050C98968080068",
                    INIT_06 => X"560E0EB0E890E8582868086B6B6A6A68284B4B4A4A9068481868086828002800",
                    INIT_07 => X"0000000000680800680800680800000068080068080000000EB0EE560E560E10",
                    INIT_08 => X"0828005008035088035088025088025008B0E85828B0CE8E0000000000000000",
                    INIT_09 => X"000008000D0D0808080808080808080808080808080808080808080808080808",
                    INIT_0A => X"0808080808080808080808080808280000006808000000680800000068080000",
                    INIT_0B => X"0D0D080808080808080808080808080808080808080808080808080808080808",
                    INIT_0C => X"005800000D0D006808006800690000006808006808001088685000F0E0590800",
                    INIT_0D => X"F0E1095A000D0D0010896800690000508008680800680800F0E1095A68006900",
                    INIT_0E => X"02680800680069000000000D0D00F0E1095A0010896800690000508008680800",
                    INIT_0F => X"8D00109D8D680091E890E8250D0D280010CB680069000050D0E38B038AA2A289",
                    INIT_10 => X"6E2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A018B8B02800109D",
                    INIT_11 => X"01286E2E010101016E2E0101016E1E280101016E1E0101016E1E01016E2E0101",
                    INIT_12 => X"1ED1A2010A2801016E2E016E1E01016E2E016E1E280101016E1E01016E2E6E2E",
                    INIT_13 => X"0A2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E01016E2E016E2E116E",
                    INIT_14 => X"D10102D10102D101020118B8B1B1B0B02801F1A26E1E01016E2E01A26B4B010A",
                    INIT_15 => X"02D101020118B8B1B0B028B8A0080100110811081108110828B8A00801D10102",
                    INIT_16 => X"0102D101020118B8B0B028B8A008010011081108110828B8A00801D10102D101",
                    INIT_17 => X"0828B8A008010011081108110828B8B1B1A00801010101010101D101022801D1",
                    INIT_18 => X"D1EA518A51D1EA520A0A500A500AB0E8582800000D0D08080808080808080808",
                    INIT_19 => X"B608280108280108280108280000000000D101500A500AB0E85828D10128D101",
                    INIT_1A => X"0E286818500AB1E8582800000D0D080808080808080808080808080828010101",
                    INIT_1B => X"CA000000000000000000680800680800008818A0A0A0A028F1CEF1CFF1CF0F0F",
                    INIT_1C => X"DBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDBDA",
                    INIT_1D => X"C8DBDBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DB",
                    INIT_1E => X"CB88CB88CA88CA000000000000000000680800680800008018A0A0A0A028E20A",
                    INIT_1F => X"A0A0A0A000221A18A0A0A0A0000088A0A0A0A000000000006808000D050E2888",
                    INIT_20 => X"A0A0A0A0001A01008818A0A0A0A0002A010088A0A0A0A000000B0B0A0A080088",
                    INIT_21 => X"010001000100000000000000000000680800680800008818A0A0A0A000008818",
                    INIT_22 => X"A0001DEDB1EE8EA512A59201090012A59201090012A59201090012A592010900",
                    INIT_23 => X"A6A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E0100008092518A0A0",
                    INIT_24 => X"08926A00080108926A00080108926A000828000000121A0088A0A0A0A000A6A6",
                    INIT_25 => X"CFB218480F0F2868080268921848284828F2CF0F28026F0F280108926A000801",
                    INIT_26 => X"0201D20202D20202D202022018A0A0A0A0A0A00228A10928A1096808481292DF",
                    INIT_27 => X"000200680008680008006900680008680008680008680008091209120928A008",
                    INIT_28 => X"68000868000868000828A008D20202000200022018A0A0A0A0A0A00228A10900",
                    INIT_29 => X"68282808129268926848080868282808D26828A1090000020068000868000800",
                    INIT_2A => X"4868280892684828926848682808926848D26828A00828A00892689268480808",
                    INIT_2B => X"0208090812F2A1A002080908D2682812F2A102080912F2A1020809D268289268",
                    INIT_2C => X"684848486828280892684828A0926848484868282808926848D2682812F2A1A0",
                    INIT_2D => X"A0A0A0A008926848289268484848682828A0A008926848D26828A0A0A0A0A092",
                    INIT_2E => X"085008B0E8582800000228025008B0E85828000002289268484848682828A0A0",
                    INIT_2F => X"0808080808080808080828025008500851885108B0E858280000000000D20250",
                    INIT_30 => X"00680800680800680800680800D3025008B0E8582800000D0D00080808080808",
                    INIT_31 => X"020200020800F302005108B0E858280068080068080068080068080068080028",
                    INIT_32 => X"0000000000000000000000050200050200040200040200030200030200020200",
                    INIT_33 => X"B0E8582802000052085108B0E858280068080068080068080028000000000000",
                    INIT_34 => X"E85008B0E85828000200020800020800F302005108B0E8582800000002005108",
                    INIT_35 => X"50000A0200508A0200508A0200508A0250000A020800F302005108B0E85008B0",
                    INIT_36 => X"00020800F302005108B0E858280002000208000200508A0200508A0200508A02",
                    INIT_37 => X"020800F302005108B0E85008B0E85008B0E85828000000000002000202000208",
                    INIT_38 => X"0200508A0200508A0200508A0250000A0200508A0200508A0200508A0250000A",
                    INIT_39 => X"0200080208000A09F30200030B5108B0E8582800000000000200020200020800",
                    INIT_3A => X"02000013020BB302B3EBA3030BD3E3130BB3E313A3A0A00B93E893E8A00200A0",
                    INIT_3B => X"1DCD0585A5A5A5CD050D2813020001010300050513130300B3EB03B3E9890300",
                    INIT_3C => X"082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D1828A513A5CD93ED55",
                    INIT_3D => X"5108B0E8582800F3CECE00500E8E0E2870887088708870887088708870887008",
                    INIT_3E => X"510804B0E85813B3E8480028006808680868081368282808B3E913682808B3E9",
                    INIT_3F => X"08B4E951080904B0E8581328006808680868081368282808B3E913682808B3E9",
                    INIT_40 => X"94E894E84800100068086808680828006808680868081468282808B3E9146828",
                    INIT_41 => X"00B4E8480014480094E84800144800480048001494C994E894E894E894E894E8",
                    INIT_42 => X"08682808B0E9146808F48808682808B4E95108B0E8581448001494C849008848",
                    INIT_43 => X"3969A1A1A119010928F4C9A0A1A1A14958E9E95809285828585828146808F488",
                    INIT_44 => X"2804B4CA8A70040404080A0A28A0F4C9A0693969A1A1A11901A00928F4C9A069",
                    INIT_45 => X"0408280404040408042804040004040804022804D4A004040408280404040408",
                    INIT_46 => X"8A70040A0450CA0450CA04500404088A0A10D4EBCB538B0B2804040408280404",
                    INIT_47 => X"080404F0EACA90EA528A0A28728A528AF4728A528AF47282520A538B0B04F4CB",
                    INIT_48 => X"7189518AF47189518AF47181510A528A0A04F4CA8A04500AF4EACA04508A0A04",
                    INIT_49 => X"500889A1A1A159B0E858B0E858B0E858B0E85828047008880828047008880828",
                    INIT_4A => X"2800000428000050C90050C90050890904280428D4E88988705008D4E8898870",
                    INIT_4B => X"28000050CA0050CA00508A0A2800B4C989887050090908B0E858280458B0E858",
                    INIT_4C => X"0808080808082800B4CA8A00500A528A0A042800F48A00B4E81800CA00508A0A",
                    INIT_4D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_4E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_4F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_50 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_51 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_52 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_53 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_54 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_55 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_56 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_57 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_58 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_59 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5B => X"9D8D95E895E825159D8D89B5E050F5E15895E895E825090D0D08080808080808",
                    INIT_5C => X"C9F5008950F5E15878082800050028000021259D8D01259D8D050515099D8D15",
                    INIT_5D => X"1571C88858C150C88858011589F5005095E101D5E15889017180087895E88858",
                    INIT_5E => X"000000000D0D08080808080808082800000D0D08080808080808080808080828",
                    INIT_5F => X"E3D5CB8A8BA3A30383538008528008F6E2580AD5E08870080889092800680069",
                    INIT_60 => X"080816C506F6E878885870885848002815700050B5E38B158B7000CB508B51D6",
                    INIT_61 => X"A00878C858F6E896E896E896E850C85816000000000D0D000808080808080808",
                    INIT_62 => X"0878C85878C858680800680800680800D6C85828A00878C8580028A008680028",
                    INIT_63 => X"00000000000000000000000000000000000000000000000000000000000028A0",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"6DB6B64114DF845FC4F98F8B7393BD1FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"6753BE805FA637C589BE2D8FB37EB6B13FFFFFFFFFFFAD6B53FFFFFFE0004CDB",
                   INITP_02 => X"E7561B73B533AB3B6A33AACEDB50EBA22F7375DB57B76FBAEFBDF7BBB7D3382E",
                   INITP_03 => X"00040DA355AAB60486AAD8121AAB60486AAD81A86273FFFF36DD613F840273FF",
                   INITP_04 => X"FB7DB65436E019264372CED55555790018A08A0008B2CB2CAB556C040300C082",
                   INITP_05 => X"FFF009D613F27E1004C209410894225C8C8BA696499252508D08A7EDDB4EA019",
                   INITP_06 => X"98889D4A4C4E91111111131113A4C4E89813B6F5554924925313B6DBDB6E4E7F",
                   INITP_07 => X"433D58B2333D58B22721AAAA644000A40038CF92929550A490D13A9488888888",
                   INITP_08 => X"7218C606692C60F7BEF6BBBEE78A501940C097FA4B4912E4CD9ABAAAA7ABAB16",
                   INITP_09 => X"FFFFFFFFFFFFFFFFFFFFFFFFFF907B08E48E2134FE48F89101249C718C606491",
                   INITP_0A => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0B => X"4001121FD3FF9FFFC016909864BFE2712B12547FFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0C => X"0000000000000000000000000000000000000002496D92672550F9FFFD23E961",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_4k_generate;	              
  --
  --
  --
  --
  -- JTAG Loader
  --
  instantiate_loader : if (C_JTAG_LOADER_ENABLE = 1) generate
  --
    jtag_loader_6_inst : jtag_loader_6
    generic map(              C_FAMILY => C_FAMILY,
                       C_NUM_PICOBLAZE => 1,
                  C_JTAG_LOADER_ENABLE => C_JTAG_LOADER_ENABLE,
                 C_BRAM_MAX_ADDR_WIDTH => BRAM_ADDRESS_WIDTH,
	                  C_ADDR_WIDTH_0 => BRAM_ADDRESS_WIDTH)
    port map( picoblaze_reset => rdl_bus,
                      jtag_en => jtag_en,
                     jtag_din => jtag_din,
                    jtag_addr => jtag_addr(BRAM_ADDRESS_WIDTH-1 downto 0),
                     jtag_clk => jtag_clk,
                      jtag_we => jtag_we,
                  jtag_dout_0 => jtag_dout,
                  jtag_dout_1 => jtag_dout, -- ports 1-7 are not used
                  jtag_dout_2 => jtag_dout, -- in a 1 device debug 
                  jtag_dout_3 => jtag_dout, -- session.  However, Synplify
                  jtag_dout_4 => jtag_dout, -- etc require all ports to
                  jtag_dout_5 => jtag_dout, -- be connected
                  jtag_dout_6 => jtag_dout,
                  jtag_dout_7 => jtag_dout);
    --  
  end generate instantiate_loader;
  --
end low_level_definition;
--
--
-------------------------------------------------------------------------------------------
--
-- JTAG Loader 
--
-------------------------------------------------------------------------------------------
--
--
-- JTAG Loader 6 - Version 6.00
-- Kris Chaplin 4 February 2010
-- Ken Chapman 15 August 2011 - Revised coding style
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--
library unisim;
use unisim.vcomponents.all;
--
entity jtag_loader_6 is
generic(              C_JTAG_LOADER_ENABLE : integer := 1;
                                  C_FAMILY : string := "V6";
                           C_NUM_PICOBLAZE : integer := 1;
                     C_BRAM_MAX_ADDR_WIDTH : integer := 10;
        C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                              C_JTAG_CHAIN : integer := 2;
                            C_ADDR_WIDTH_0 : integer := 10;
                            C_ADDR_WIDTH_1 : integer := 10;
                            C_ADDR_WIDTH_2 : integer := 10;
                            C_ADDR_WIDTH_3 : integer := 10;
                            C_ADDR_WIDTH_4 : integer := 10;
                            C_ADDR_WIDTH_5 : integer := 10;
                            C_ADDR_WIDTH_6 : integer := 10;
                            C_ADDR_WIDTH_7 : integer := 10);
port(   picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
               jtag_din : out std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
              jtag_addr : out std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0) := (others => '0');
               jtag_clk : out std_logic := '0';
                jtag_we : out std_logic := '0';
            jtag_dout_0 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_1 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_2 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_3 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_4 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_5 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_6 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_7 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end jtag_loader_6;
--
architecture Behavioral of jtag_loader_6 is
  --
  signal num_picoblaze       : std_logic_vector(2 downto 0);
  signal picoblaze_instruction_data_width : std_logic_vector(4 downto 0);
  --
  signal drck                : std_logic;
  signal shift_clk           : std_logic;
  signal shift_din           : std_logic;
  signal shift_dout          : std_logic;
  signal shift               : std_logic;
  signal capture             : std_logic;
  --
  signal control_reg_ce      : std_logic;
  signal bram_ce             : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal bus_zero            : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  signal jtag_en_int         : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal jtag_en_expanded    : std_logic_vector(7 downto 0) := (others => '0');
  signal jtag_addr_int       : std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
  signal jtag_din_int        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal control_din         : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout_int    : std_logic_vector(7 downto 0):= (others => '0');
  signal bram_dout_int       : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
  signal jtag_we_int         : std_logic;
  signal jtag_clk_int        : std_logic;
  signal bram_ce_valid       : std_logic;
  signal din_load            : std_logic;
  --
  signal jtag_dout_0_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_1_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_2_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_3_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_4_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_5_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_6_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_7_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal picoblaze_reset_int : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  --        
begin
  bus_zero <= (others => '0');
  --
  jtag_loader_gen: if (C_JTAG_LOADER_ENABLE = 1) generate
    --
    -- Insert BSCAN primitive for target device architecture.
    --
    BSCAN_SPARTAN6_gen: if (C_FAMILY="S6") generate
    begin
      BSCAN_BLOCK_inst : BSCAN_SPARTAN6
      generic map ( JTAG_CHAIN => C_JTAG_CHAIN)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_SPARTAN6_gen;   
    --
    BSCAN_VIRTEX6_gen: if (C_FAMILY="V6") generate
    begin
      BSCAN_BLOCK_inst: BSCAN_VIRTEX6
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => FALSE)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_VIRTEX6_gen;   
    --
    BSCAN_7SERIES_gen: if (C_FAMILY="7S") generate
    begin
      BSCAN_BLOCK_inst: BSCANE2
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => "FALSE")
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_7SERIES_gen;   
    --
    --
    -- Insert clock buffer to ensure reliable shift operations.
    --
    upload_clock: BUFG
    port map( I => drck,
              O => shift_clk);
    --        
    --        
    --  Shift Register      
    --        
    --
    control_reg_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk = '1' then
        if (shift = '1') then
          control_reg_ce <= shift_din;
        end if;
      end if;
    end process control_reg_ce_shift;
    --        
    bram_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          if(C_NUM_PICOBLAZE > 1) then
            for i in 0 to C_NUM_PICOBLAZE-2 loop
              bram_ce(i+1) <= bram_ce(i);
            end loop;
          end if;
          bram_ce(0) <= control_reg_ce;
        end if;
      end if;
    end process bram_ce_shift;
    --        
    bram_we_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          jtag_we_int <= bram_ce(C_NUM_PICOBLAZE-1);
        end if;
      end if;
    end process bram_we_shift;
    --        
    bram_a_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          for i in 0 to C_BRAM_MAX_ADDR_WIDTH-2 loop
            jtag_addr_int(i+1) <= jtag_addr_int(i);
          end loop;
          jtag_addr_int(0) <= jtag_we_int;
        end if;
      end if;
    end process bram_a_shift;
    --        
    bram_d_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (din_load = '1') then
          jtag_din_int <= bram_dout_int;
         elsif (shift = '1') then
          for i in 0 to C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-2 loop
            jtag_din_int(i+1) <= jtag_din_int(i);
          end loop;
          jtag_din_int(0) <= jtag_addr_int(C_BRAM_MAX_ADDR_WIDTH-1);
        end if;
      end if;
    end process bram_d_shift;
    --
    shift_dout <= jtag_din_int(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1);
    --
    --
    din_load_select:process (bram_ce, din_load, capture, bus_zero, control_reg_ce) 
    begin
      if ( bram_ce = bus_zero ) then
        din_load <= capture and control_reg_ce;
       else
        din_load <= capture;
      end if;
    end process din_load_select;
    --
    --
    -- Control Registers 
    --
    num_picoblaze <= conv_std_logic_vector(C_NUM_PICOBLAZE-1,3);
    picoblaze_instruction_data_width <= conv_std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1,5);
    --	
    control_registers: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '0') and (control_reg_ce = '1') then
          case (jtag_addr_int(3 downto 0)) is 
            when "0000" => -- 0 = version - returns (7 downto 4) illustrating number of PB
                           --               and (3 downto 0) picoblaze instruction data width
                           control_dout_int <= num_picoblaze & picoblaze_instruction_data_width;
            when "0001" => -- 1 = PicoBlaze 0 reset / status
                           if (C_NUM_PICOBLAZE >= 1) then 
                            control_dout_int <= picoblaze_reset_int(0) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_0-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0010" => -- 2 = PicoBlaze 1 reset / status
                           if (C_NUM_PICOBLAZE >= 2) then 
                             control_dout_int <= picoblaze_reset_int(1) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_1-1,5) );
                            else 
                             control_dout_int <= (others => '0');
                           end if;
            when "0011" => -- 3 = PicoBlaze 2 reset / status
                           if (C_NUM_PICOBLAZE >= 3) then 
                            control_dout_int <= picoblaze_reset_int(2) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_2-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0100" => -- 4 = PicoBlaze 3 reset / status
                           if (C_NUM_PICOBLAZE >= 4) then 
                            control_dout_int <= picoblaze_reset_int(3) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_3-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0101" => -- 5 = PicoBlaze 4 reset / status
                           if (C_NUM_PICOBLAZE >= 5) then 
                            control_dout_int <= picoblaze_reset_int(4) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_4-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0110" => -- 6 = PicoBlaze 5 reset / status
                           if (C_NUM_PICOBLAZE >= 6) then 
                            control_dout_int <= picoblaze_reset_int(5) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_5-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0111" => -- 7 = PicoBlaze 6 reset / status
                           if (C_NUM_PICOBLAZE >= 7) then 
                            control_dout_int <= picoblaze_reset_int(6) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_6-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1000" => -- 8 = PicoBlaze 7 reset / status
                           if (C_NUM_PICOBLAZE >= 8) then 
                            control_dout_int <= picoblaze_reset_int(7) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_7-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1111" => control_dout_int <= conv_std_logic_vector(C_BRAM_MAX_ADDR_WIDTH -1,8);
            when others => control_dout_int <= (others => '1');
          end case;
        else 
          control_dout_int <= (others => '0');
        end if;
      end if;
    end process control_registers;
    -- 
    control_dout(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-8) <= control_dout_int;
    --
    pb_reset: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '1') and (control_reg_ce = '1') then
          picoblaze_reset_int(C_NUM_PICOBLAZE-1 downto 0) <= control_din(C_NUM_PICOBLAZE-1 downto 0);
        end if;
      end if;
    end process pb_reset;    
    --
    --
    -- Assignments 
    --
    control_dout (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-9 downto 0) <= (others => '0') when (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH > 8);
    --
    -- Qualify the blockram CS signal with bscan select output
    jtag_en_int <= bram_ce when bram_ce_valid = '1' else (others => '0');
    --      
    jtag_en_expanded(C_NUM_PICOBLAZE-1 downto 0) <= jtag_en_int;
    jtag_en_expanded(7 downto C_NUM_PICOBLAZE) <= (others => '0') when (C_NUM_PICOBLAZE < 8);
    --        
    bram_dout_int <= control_dout or jtag_dout_0_masked or jtag_dout_1_masked or jtag_dout_2_masked or jtag_dout_3_masked or jtag_dout_4_masked or jtag_dout_5_masked or jtag_dout_6_masked or jtag_dout_7_masked;
    --
    control_din <= jtag_din_int;
    --        
    jtag_dout_0_masked <= jtag_dout_0 when jtag_en_expanded(0) = '1' else (others => '0');
    jtag_dout_1_masked <= jtag_dout_1 when jtag_en_expanded(1) = '1' else (others => '0');
    jtag_dout_2_masked <= jtag_dout_2 when jtag_en_expanded(2) = '1' else (others => '0');
    jtag_dout_3_masked <= jtag_dout_3 when jtag_en_expanded(3) = '1' else (others => '0');
    jtag_dout_4_masked <= jtag_dout_4 when jtag_en_expanded(4) = '1' else (others => '0');
    jtag_dout_5_masked <= jtag_dout_5 when jtag_en_expanded(5) = '1' else (others => '0');
    jtag_dout_6_masked <= jtag_dout_6 when jtag_en_expanded(6) = '1' else (others => '0');
    jtag_dout_7_masked <= jtag_dout_7 when jtag_en_expanded(7) = '1' else (others => '0');
    --
    jtag_en <= jtag_en_int;
    jtag_din <= jtag_din_int;
    jtag_addr <= jtag_addr_int;
    jtag_clk <= jtag_clk_int;
    jtag_we <= jtag_we_int;
    picoblaze_reset <= picoblaze_reset_int;
    --        
  end generate jtag_loader_gen;
--
end Behavioral;
--
--
------------------------------------------------------------------------------------
--
-- END OF FILE cli.vhd
--
------------------------------------------------------------------------------------
