;;; ============================================================================
;;; Register map
;;; ============================================================================
;;; | name                 | s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7 | s8 | s9 | sA | sB | sC | sD |
;;; | CMD_TDC_UART_STATUS  | y  | y  | y  | y  |    |    |    |    |    |    |    |    |    |    |
;;; | CMD_TDC_UART_WRITE   | x  | x  |    |    |    |    |    |    |    |    |    |    |    |    |
;;; | CMD_TEMP_SEARCH      | x  | x  |    |    |    |    |    |    |    |    |    |    |    |    |
;;; | CMD_TEMP_GET_ID      | x  | x  | y  | x  | x  | x  | x  | x  | x  | x  | x  | x  |    |    |
;;; | CMD_TEMP_WRITE       | x  | x  | y  | x  | x  |    |    |    |    |    |    |    |    |    |
;;; | CMD_TEMP_READ        | x  | x  | y  | x  |    |    |    |    |    |    |    |    |    |    |
;;; | CMD_TEMP_CONV        | x  | x  | y  | x  | x  |    |    |    |    |    |    |    |    |    |
;;; | CMD_TEMP_READ_TEMP   | x  | x  | y  | x  | x  |    |    |    |    |    |    |    |    |    |
;;; | CMD_TEMP_CONV_NA     | x  | x  | y  | x  | x  |    |    |    |    |    |    |    |    |    |
;;; | CMD_TEMP_READ_TEMP_NA| x  | x  | y  | x  | x  |    |    |    |    |    |    |    |    |    |
;;; | CMD_TEMP_ROM_WR      |    | x  |    |    |    |    |    |    |    |    | x  | x  | x  | x  |
;;; | CMD_TEMP_ROM_RD      |    | x  |    |    |    |    |    |    |    |    | x  | x  |    |    |
;;; | CMD_TEMP_ROM_ZERO    | x  |    |    |    |    |    |    |    |    |    |    |    |    |    |
;;; | CMD_TDC_UART_PROGRAM | x  | x  | x  | x  |    |    |    |    |    |    |    |    |    |    |



	
INCLUDE "TDC_UART.psm"
INCLUDE "onewire.psm"



	
;;; ============================================================================
;;; Read TDC UART status
;;; ============================================================================
CMD_TDC_UART_STATUS:
	CALL tdc_uart_status
	CALL util_print_hex_byte
	CALL util_print_EOL
	RETURN

;;; ============================================================================
;;; write a byte to the UART
;;; ============================================================================
CMD_TDC_UART_WRITE:
	FETCH s0, CLI_COUNT	; load the number of arguments
	COMPARE s0, 01
	JUMP NZ, ERROR_BAD_ARG

	;; Read the byte
	LOAD s1, CLI_WORD_1
	FETCH s0, (s1)
		
	CALL tdc_uart_write
	RETURN
	
	
;;; ============================================================================
;;; read a byte from the UART
;;; ============================================================================
CMD_TDC_UART_READ:
	CALL tdc_uart_read
	CALL util_print_hex_byte
	CALL util_print_EOL
	RETURN
	
;;; ============================================================================
;;; TDC reg rd
;;; ============================================================================
CMD_TDC_REG_RD:
	FETCH s0, CLI_COUNT	; load the number of arguments
	COMPARE s0, 02
	JUMP NZ, ERROR_BAD_ARG

	LOAD s1, CLI_WORD_1
	FETCH s0, (s1)
	LOAD s1, CLI_WORD_2
	FETCH s1, (s1)

	;; do the rad
	CALL tdc_reg_rd
	JUMP C, CMD_TDC_REG_RD_END ; error message already printed
	;; print response
	LOAD s0,s4
	CALL util_print_hex_byte
	LOAD s0,s3
	CALL util_print_hex_byte
	CALL util_print_EOL
CMD_TDC_REG_RD_END:
	RETURN
	
;;; ============================================================================
;;; TDC reg wr
;;; ============================================================================
CMD_TDC_REG_WR:
	FETCH s0, CLI_COUNT	; load the number of arguments
	COMPARE s0, 03
	JUMP NZ, ERROR_BAD_ARG

	LOAD s1, CLI_WORD_3
	FETCH s2, (s1)
	ADD s1, 01
	FETCH s3, (s1)
	LOAD s1, CLI_WORD_1
	FETCH s0, (s1)
	LOAD s1, CLI_WORD_2
	FETCH s1, (s1)

	CALL tdc_reg_wr
	RETURN


;;; ============================================================================
;;; Error for no device
;;; ===========================================================================
STRING nodev$, "No device found"
str_nodev:
	LOAD&RETURN s1, nodev$
	LOAD&RETURN s1, 00
CMD_GET_TEMP_no_device:	
	CALL util_print_EOL
	LOAD sB, str_nodev'upper
	LOAD sA, str_nodev'lower
	CALL util_print_string
	CALL util_print_EOL
	RETURN

;;; ============================================================================
;;; Temp search a
;;; ============================================================================
;CMD_TEMP_SEARCH:
;	CALL onewire_search
;	RETURN

;;; ============================================================================
;;; Temp search
;;; argument 0/1 for group a/B
;;; ============================================================================
CMD_TEMP_SEARCH:
	FETCH s0, CLI_COUNT	; load the number of arguments
	COMPARE s0, 01
	JUMP NZ, ERROR_BAD_ARG

	LOAD s1, CLI_WORD_1
	FETCH s0, (s1)

	CALL onewire_presence
	JUMP C, CMD_TEMP_SEARCH_found

	CALL uart_output_wait
	LOAD s0, "N"
	OUTPUT s0, UART_OUTPUT
	CALL uart_output_wait
	LOAD s0, "O"
	OUTPUT s0, UART_OUTPUT
	CALL uart_output_wait
	LOAD s0, "N"
	OUTPUT s0, UART_OUTPUT
	CALL uart_output_wait
	LOAD s0, "E"
	OUTPUT s0, UART_OUTPUT
	CALL util_print_EOL
	RETURN

CMD_TEMP_SEARCH_found:	
	CALL uart_output_wait
	LOAD s0, "F"
	OUTPUT s0, UART_OUTPUT
	CALL uart_output_wait
	LOAD s0, "O"
	OUTPUT s0, UART_OUTPUT
	CALL uart_output_wait
	LOAD s0, "U"
	OUTPUT s0, UART_OUTPUT
	CALL uart_output_wait
	LOAD s0, "N"
	OUTPUT s0, UART_OUTPUT
	CALL uart_output_wait
	LOAD s0, "D"
	OUTPUT s0, UART_OUTPUT
	CALL util_print_EOL
	RETURN

;;; ============================================================================
;;; Temp search
;;; argument 0/1 for group a/B
;;; ============================================================================
CMD_TEMP_GET_ID:
	FETCH s0, CLI_COUNT	; load the number of arguments
	COMPARE s0, 01
	JUMP NZ, ERROR_BAD_ARG

	LOAD s1, CLI_WORD_1
	FETCH s3, (s1)		;store A/B in s3

	;; check if anything is there
	LOAD s0, s3
	CALL onewire_presence
	JUMP NC, CMD_TEMP_GET_ID_ERR 

	;; write the match rom command
	LOAD s0, s3
	LOAD s1, 33
	call onewire_write_byte

	;; read the LSB
	LOAD s0, s3
	call onewire_read_byte	
	LOAD s4,s1

	LOAD s0, s3
	call onewire_read_byte	
	LOAD s5,s1
	
	LOAD s0, s3
	call onewire_read_byte	
	LOAD s6,s1
	
	LOAD s0, s3
	call onewire_read_byte	
	LOAD s7,s1
	
	LOAD s0, s3
	call onewire_read_byte	
	LOAD s8,s1
	
	LOAD s0, s3
	call onewire_read_byte	
	LOAD s9,s1
	
	LOAD s0, s3
	call onewire_read_byte	
	LOAD sA,s1
	;; read the MSB
	LOAD s0, s3
	call onewire_read_byte	
	LOAD sB,s1

	;; print
	LOAD s0,sB
	CALL util_print_hex_byte
	LOAD s0,sA
	CALL util_print_hex_byte
	LOAD s0,s9
	CALL util_print_hex_byte
	LOAD s0,s8
	CALL util_print_hex_byte
	LOAD s0,s7
	CALL util_print_hex_byte
	LOAD s0,s6
	CALL util_print_hex_byte
	LOAD s0,s5
	CALL util_print_hex_byte
	LOAD s0,s4
	CALL util_print_hex_byte
	CALL util_print_EOL
	RETURN
	

CMD_TEMP_GET_ID_ERR:
	CALL uart_output_wait
	LOAD s0, "E"
	OUTPUT s0, UART_OUTPUT
	CALL uart_output_wait
	LOAD s0, "R"
	OUTPUT s0, UART_OUTPUT
	CALL uart_output_wait
	LOAD s0, "R"
	OUTPUT s0, UART_OUTPUT
	CALL util_print_EOL
	RETURN


;;; ============================================================================
;;; Temp write byte
;;; argument 0/1 for group a/B	
;;; ============================================================================
CMD_TEMP_WRITE:
	FETCH s0, CLI_COUNT	; load the number of arguments
	COMPARE s0, 02
	JUMP NZ, ERROR_BAD_ARG

	LOAD s1, CLI_WORD_1
	FETCH s3, (s1)		;store A/B in s3
	LOAD s1, CLI_WORD_2
	FETCH s4, (s1)

	;; write the match rom command
	LOAD s0, s3
	LOAD s1, s4
	call onewire_write_byte
	RETURN

;;; ============================================================================
;;; Temp read byte
;;; argument 0/1 for group a/B
;;; ============================================================================
CMD_TEMP_READ:
	FETCH s0, CLI_COUNT	; load the number of arguments
	COMPARE s0, 01
	JUMP NZ, ERROR_BAD_ARG

	LOAD s1, CLI_WORD_1
	FETCH s3, (s1)		;store A/B in s3

	;; read the LSB
	LOAD s0, s3
	call onewire_read_byte	
	LOAD s0,s1
	CALL util_print_hex_byte
	CALL util_print_EOL
	RETURN

;;; ============================================================================
;;; TEmp sensor measure and store temp (no address)
;;; input:
;;; 	CLIWORD1: argument 0/1 for group a/B
;;; ============================================================================
CMD_TEMP_CONV_NO_ADDR:	
	;; check arguments
	FETCH s0, CLI_COUNT
	COMPARE s0, 01		;there must be one argument
	JUMP NZ, ERROR_BAD_ARG

	;; load A/B bus and store in s3
	LOAD s1, CLI_WORD_1
	FETCH s3, (s1)
	LOAD s0, s3

	;; call init and check that someone is there
	CALL onewire_presence
	JUMP NC, CMD_GET_TEMP_no_device

	;; write no match address command
	LOAD s0, s3		;a/B
	LOAD s1, cc		;skip rom command
	CALL onewire_write_byte

	;; Instruct slave to convert temperature
	LOAD s0, s3
	LOAD s1, 44
	CALL onewire_write_byte

	;; turn on power for convert
	LOAD s0, s3
	CALL onewire_pwr
	CALL util_print_EOL	
	;; temp sensor should have save the temp by now
	RETURN
	
;;; ============================================================================
;;; TEmp sensor measure and store temp
;;; input:
;;; 	CLIWORD1: argument 0/1 for group a/B
;;;     CLIWORD2: Upper half of address
;;; 	CLIWORD3: Lower half of address
;;; ============================================================================
CMD_TEMP_CONV:	
	;; check arguments
	FETCH s0, CLI_COUNT
	COMPARE s0, 03		;there must be three arguments
	JUMP NZ, ERROR_BAD_ARG
	;; check length of address words to make sure we have all of our 64 bits
	LOAD s1, CLI_WORD_2_SIZE
	FETCH s0, (s1)
	COMPARE s0, 08
	JUMP NZ, ERROR_BAD_ARG
	LOAD s1, CLI_WORD_3_SIZE
	FETCH s0, (s1)
	COMPARE s0, 08
	JUMP NZ, ERROR_BAD_ARG

	
	;; load A/B bus and store in s3
	LOAD s1, CLI_WORD_1
	FETCH s3, (s1)
	LOAD s0, s3
	;; call init and check that someone is there
	CALL onewire_presence
	JUMP NC, CMD_GET_TEMP_no_device
	;; write match address command
	LOAD s0, s3		;a/B
	LOAD s1, 55		;match rom command
	CALL onewire_write_byte

	;; write address LSB first

	;; lowest 4 bytes in CLI_WORD_3
	LOAD s4, CLI_WORD_3
	;; byte 0
	LOAD s0,s3
	FETCH s1, (s4)
	CALL onewire_write_byte
	;; byte 1
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte
	;; byte 2
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte
	;; byte 3
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte

	;; upper 4 bytes in CLI_WORD_2
	LOAD s4, CLI_WORD_2
	;; byte 4
	LOAD s0,s3
	FETCH s1, (s4)
	CALL onewire_write_byte
	;; byte 5
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte
	;; byte 6
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte
	;; byte 7
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte

	
	;; Instruct slave to convert temperature
	LOAD s0, s3
	LOAD s1, 44
	CALL onewire_write_byte

	;; turn on power for convert
	LOAD s0, s3
	CALL onewire_pwr
	CALL util_print_EOL	
	;; temp sensor should have save the temp by now
	RETURN



;;; ============================================================================
;;; Temp sensor readout
;;; input:
;;; 	CLIWORD1: argument 0/1 for group a/B
;;; ============================================================================
CMD_TEMP_READ_TEMP_NO_ADDR:	
	;; check arguments
	FETCH s0, CLI_COUNT
	COMPARE s0, 01		;there must be one argument
	JUMP NZ, ERROR_BAD_ARG

	;; load A/B bus and store in s3
	LOAD s1, CLI_WORD_1
	FETCH s3, (s1)
	LOAD s0, s3
	;; call init and check that someone is there
	CALL onewire_presence
	JUMP NC, CMD_GET_TEMP_no_device
	;; write skip address command
	LOAD s0, s3		;a/B
	LOAD s1, cc		;skip rom command
	CALL onewire_write_byte

	;; Instruct slave to send data
	LOAD s0, s3
	LOAD s1, BE
	CALL onewire_write_byte

	;; read data
	LOAD s0, s3
	CALL onewire_read_byte	;LSB
	LOAD s4,s1		;cache LSB
	LOAD s0, s3
	CALL onewire_read_byte	;MSB

	;; print
	LOAD s0, s1
	CALL util_print_hex_byte
	LOAD s0, s4
	CALL util_print_hex_byte

	CALL util_print_EOL
	
	
	;; temp sensor should have save the temp by now
	RETURN
;;; ============================================================================
;;; Temp sensor readout
;;; input:
;;; 	CLIWORD1: argument 0/1 for group a/B
;;;     CLIWORD2: Upper half of address
;;; 	CLIWORD3: Lower half of address
;;; ============================================================================
CMD_TEMP_READ_TEMP:	
	;; check arguments
	FETCH s0, CLI_COUNT
	COMPARE s0, 03		;there must be three arguments
	JUMP NZ, ERROR_BAD_ARG
	;; check length of address words to make sure we have all of our 64 bits
	LOAD s1, CLI_WORD_2_SIZE
	FETCH s0, (s1)
	COMPARE s0, 08
	JUMP NZ, ERROR_BAD_ARG
	LOAD s1, CLI_WORD_3_SIZE
	FETCH s0, (s1)
	COMPARE s0, 08
	JUMP NZ, ERROR_BAD_ARG

	
	;; load A/B bus and store in s3
	LOAD s1, CLI_WORD_1
	FETCH s3, (s1)
	LOAD s0, s3
	;; call init and check that someone is there
	CALL onewire_presence
	JUMP NC, CMD_GET_TEMP_no_device
	;; write match address command
	LOAD s0, s3		;a/B
	LOAD s1, 55		;match rom command
	CALL onewire_write_byte

	;; write address LSB first

	;; lowest 4 bytes in CLI_WORD_3
	LOAD s4, CLI_WORD_3
	;; byte 0
	LOAD s0,s3
	FETCH s1, (s4)
	CALL onewire_write_byte
	;; byte 1
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte
	;; byte 2
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte
	;; byte 3
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte

	;; upper 4 bytes in CLI_WORD_2
	LOAD s4, CLI_WORD_2
	;; byte 4
	LOAD s0,s3
	FETCH s1, (s4)
	CALL onewire_write_byte
	;; byte 5
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte
	;; byte 6
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte
	;; byte 7
	ADD s4,01
	FETCH s1, (s4)
	LOAD s0, s3
	CALL onewire_write_byte

	
	;; Instruct slave to send data
	LOAD s0, s3
	LOAD s1, BE
	CALL onewire_write_byte

	;; read data
	LOAD s0, s3
	CALL onewire_read_byte	;LSB
	LOAD s4,s1		;cache LSB
	LOAD s0, s3
	CALL onewire_read_byte	;MSB

	;; print
	LOAD s0, s1
	CALL util_print_hex_byte
	LOAD s0, s4
	CALL util_print_hex_byte

	CALL util_print_EOL
	
	
	;; temp sensor should have save the temp by now
	RETURN


;;; ============================================================================
;;; Temp sensor ADDR search
;;; input:
;;; 	CLIWORD1: argument 0/1 for group a/B
;;; ============================================================================
CMD_TEMP_GET_ADDRS:
	FETCH s0, CLI_COUNT
	COMPARE s0, 01		;there must be one argument
	JUMP NZ, ERROR_BAD_ARG
	
	;; load A/B bus and store in s2
	LOAD s1, CLI_WORD_1
	FETCH s2, (s1)

	LOAD s7, 00		;last discrepancy
	CALL CMD_TEMP_ROM_ZERO

CMD_TEMP_GET_ADDRS_start:	
	LOAD s0,s2
	;; call init and check that someone is there
	CALL onewire_presence
	JUMP NC, CMD_GET_TEMP_no_device
	
	LOAD s3, 01		;id_bit_number
	LOAD s4, 00		;last_zero
	
	;; start search
	LOAD s0, s2
	LOAD s1, F0
	CALL onewire_write_byte
CMD_TEMP_GET_ADDRS_bit:
	LOAD s1, 00
	;; read and of all sensor addresses for this bit
	LOAD s0,s2 		;bus number
	CALL onewire_read_bit
	SLA s1			;store in s1
	;; read and of all sensor compliment addresses for this bit
	LOAD s0,s2		;bus number
	CALL onewire_read_bit
	SLA s1
	
	COMPARE s1, 03
	JUMP Z, CMD_TEMP_GET_ADDRS_LAST_DEVICE
	COMPARE s1, 00
	JUMP Z, CMD_TEMP_GET_ADDRS_SET_DIR

	;; no collision, use bit 1 of s1 as addr
	LOAD s6, 00
	SR0 s1			;shift off compliment bit
	SR0 s1			;shift off bit
	SLA s6			;shift in bit s6
	JUMP CMD_TEMP_GET_ADDRS_SAVE_BIT

CMD_TEMP_GET_ADDRS_SET_DIR:
	
	COMPARE s7,s3		; lastdiscrepancy == id_bit_number?
	JUMP NZ, CMD_TEMP_GET_ADDRS_SET_DIR_NOT_EQUAL
	;; match
	LOAD s6,01
	JUMP CMD_TEMP_GET_ADDRS_SAVE_BIT
CMD_TEMP_GET_ADDRS_SET_DIR_NOT_EQUAL:
	COMPARE s7,s3			     ;check if last discrepancy is less than index
	JUMP C, CMD_TEMP_GET_ADDRS_SET_DIR_LESS_THAN ;jump if id_bit is greater than last discrepancy
	
	LOAD s6,00	
	CALL CMD_TEMP_ROM_RD
	SLA s6			;set bit to last search value
	;; check if the bit is zero
	COMPARE s6,00
	JUMP NZ, CMD_TEMP_GET_ADDRS_SAVE_BIT
	;; update last zero if it is
	LOAD s4,s3
	JUMP NZ, CMD_TEMP_GET_ADDRS_SAVE_BIT


CMD_TEMP_GET_ADDRS_SET_DIR_LESS_THAN:
	LOAD s6,00			     ;set bit to zero
	;; update last zero
	LOAD s4,s3
	JUMP CMD_TEMP_GET_ADDRS_SAVE_BIT
	
CMD_TEMP_GET_ADDRS_SAVE_BIT:
	;; save bit into ROM
	LOAD s1, s6		;buffer bit

	;; write bit
	LOAD s0,s2 		;bus number
	CALL onewire_write_bit 	;uses s1

	;; save bit to ROM addr
	LOAD s1,s6
	CALL CMD_TEMP_ROM_WR
		
	;; move to the next bit
	ADD s3,01

	
	COMPARE s3,41		;if less than or equal to bit 64 (0x41), keep going
	JUMP NZ, CMD_TEMP_GET_ADDRS_bit
	LOAD s7,s4		;set lst discrepancy to last zero
	
	COMPARE s7,00
	JUMP NZ, CMD_TEMP_GET_ADDRS_PRINT
	;; we are at the end, print and return
	CALL util_print_EOL
	CALL CMD_TEMP_ROM_PRINT
	JUMP CMD_TEMP_GET_ADDRS_END
		

CMD_TEMP_GET_ADDRS_LAST_DEVICE:
	JUMP CMD_TEMP_GET_ADDRS_END


CMD_TEMP_GET_ADDRS_PRINT:
	;; save s2,s3
	LOAD sA,s2
	LOAD sB,s3
	CALL util_print_EOL
	CALL CMD_TEMP_ROM_PRINT
	LOAD s2,sA
	LOAD s3,sB

	;; power up
	LOAD s0,s2
	CALL onewire_pwr
	
	JUMP CMD_TEMP_GET_ADDRS_start	

CMD_TEMP_GET_ADDRS_END:	
	RETURN
	
;;; ============================================================================
;;; TEMP_ROM_RD
;;; input:
;;; 	s3: id_bit number into rom
;;; uses:
;;; 	MEM_USR_1 (8bytes)
;;; ouput:
;;; 	C: bit value at s3 in ROM
;;; ============================================================================	
CMD_TEMP_ROM_RD:	
	LOAD sB,MEM_USER_1
	LOAD sA,s3		;local copy of bit index
	SUB sA,01		;bit index is 1 based and not 0 based, so subtract 1
	SR0 sA			;divide bit index by 8
	SR0 sA
	SR0 sA
	ADD sB,sA		;move to byte that we care about

	LOAD sA,s3		;load bit index again and mask off what's above the byte bit position
	SUB sA,01		;convert to 0 LSB from 1LSB
	AND sA,07		;mask off lowest bits

	FETCH sB, (sB)		;load into sB, the byte at address sB
CMD_TEMP_ROM_RD_BIT_POS:
	COMPARE sA, 00		;check if we've shifted the needed bit to the LSB pos
	JUMP Z, CMD_TEMP_ROM_RD_BIT_END ;finish if we have
	SUB sA, 01			;move down by one if we haven't	
	SR0 sB				;shift bits by one
	JUMP CMD_TEMP_ROM_RD_BIT_POS
CMD_TEMP_ROM_RD_BIT_END:
	SR0 sB
	RETURN
	
;;; ============================================================================
;;; TEMP ROM WR
;;; input:
;;; 	s3: id_bit number into rom
;;; 	s1: bit we want to write
;;; uses:
;;; 	MEM_USR_1 (8bytes)
;;; 	s1
;;; ============================================================================	
CMD_TEMP_ROM_WR:
	;; clear all but the LSB of s1 (the bit we are writing)
	AND s1, 01

	;; load the existing ROM value
	LOAD sB, MEM_USER_1
	LOAD sA,s3		;local copy of bit index
	SUB sA,01		;convert from LSB 1 to LSB 0
	SR0 sA			;divide bit index by 8
	SR0 sA
	SR0 sA
	ADD sB,sA		;move to byte that we care about

	LOAD sA,s3		;load bit index again and mask off what's above the byte bit position
	SUB sA,01		;convert from LSB 1 to LSB 0
	AND sA,07

	FETCH sC, (sB)		;load into sC, the byte we care about
	LOAD sD,FE		;mask for clearing of bit we care about
	;; get bits into position
CMD_TEMP_ROM_WR_BIT_POS:	
	COMPARE sA, 00
	JUMP Z, CMD_TEMP_ROM_WR_BIT_POS_CORRECT
	SL1 sD			;shift our mask over
	SL0 s1			;shift our bit over
	SUB sA, 01
	JUMP CMD_TEMP_ROM_WR_BIT_POS
CMD_TEMP_ROM_WR_BIT_POS_CORRECT:
	AND sC,sD		;clear the bit we care about in sC
	OR sC,s1		;load the bit we want to write (in s1) into the correct pos in sC (leaveing the rest unmodified)
	STORE sC, (sB)
	RETURN
	
	
;;; ============================================================================
;;; TEMP ROM ZERO
;;; uses:
;;; 	MEM_USR_1 (8bytes)
;;; 	s0
;;; 	s1
;;; ============================================================================	
CMD_TEMP_ROM_ZERO:
	LOAD s1, 00
	;; byte 0
	LOAD s0, MEM_USER_1
	STORE s1, (s0)
	;; byte 1
	ADD s0,01
	STORE s1, (s0)
	;; byte 2
	ADD s0,01
	STORE s1, (s0)
	;; byte 3
	ADD s0,01
	STORE s1, (s0)
	;; byte 4
	ADD s0,01
	STORE s1, (s0)
	;; byte 5
	ADD s0,01
	STORE s1, (s0)
	;; byte 6
	ADD s0,01
	STORE s1, (s0)
	;; byte 7
	ADD s0,01
	STORE s1, (s0)
	RETURN

;;; ============================================================================
;;; TEMP ROM PRINT
;;; uses:
;;; 	MEM_USR_1 (8bytes)
;;; 	s0
;;; 	s1
;;;     s2
;;; ============================================================================	
CMD_TEMP_ROM_PRINT:
	LOAD sC, MEM_USER_1
	ADD sC,07
	LOAD sD, 07
CMD_TEMP_ROM_PRINT_LOOP:	
	FETCH s0,(sC)
	CALL util_print_hex_byte
	SUB sC,01
	SUB sD,01
	JUMP NC, CMD_TEMP_ROM_PRINT_LOOP
	CALL util_print_EOL
	RETURN

;;; ============================================================================
;;; TDC uC UART PASSTHROUGH
;;; input 
;;; 	CLIWORD1: argument 0/1 for TDC uC 0/1
;;; uses:
;;; 	s0
;;; 	s1
;;;     s2
;;; ============================================================================
	CONSTANT TDC_UC_CONFIG, 0D
	CONSTANT TDC_UC_PASS_EN, 01
	CONSTANT TDC_UC_PASS, 02
	CONSTANT TDC_UC_RESET_0, 04
	CONSTANT TDC_UC_RESET_1, 08
	CONSTANT TDC_UC_DEFAULT, 00
	CONSTANT TDC_UC_ESCAPE_CHAR, 1D
CMD_TDC_UC_UART:
	FETCH s0, CLI_COUNT
	COMPARE s0, 01		;there must be one argument
	JUMP NZ, ERROR_BAD_ARG
	
	;; load 0/1 TDC UC and store in s2
	LOAD s1, CLI_WORD_1
	FETCH s2, (s1)

	;; check if we want to talk to TDC 0
	COMPARE s2, 00
	JUMP NZ, CMD_TDC_UC_UART1 ;nope
	LOAD s1, TDC_UC_DEFAULT
	OR s1, TDC_UC_PASS_EN
	OUTPUT s1, TDC_UC_CONFIG ;switch io
	JUMP CMD_TDC_UC_UART_MON
CMD_TDC_UC_UART1:
	;; check if we want to talk to TDC 1
	COMPARE s2, 01
	JUMP NZ, CMD_TDC_UC_UART_END	;nope
	LOAD s1, TDC_UC_DEFAULT
	OR s1, TDC_UC_PASS_EN
	OR s1, TDC_UC_PASS
	OUTPUT s1, TDC_UC_CONFIG ;switch io
	JUMP CMD_TDC_UC_UART_MON

CMD_TDC_UC_UART_END:
	;; reset to normal
	LOAD s1, TDC_UC_DEFAULT
	OUTPUT s1, TDC_UC_CONFIG
	LOAD s1, "L"
	OUTPUT s1, UART_OUTPUT
	LOAD s1, "B"
	OUTPUT s1, UART_OUTPUT
	CALL util_print_EOL
	RETURN
	
CMD_TDC_UC_UART_MON:
	;; monitor what the user is sending to the TDC for an escape char
	call uart_input_wait
	INPUT s1, UART_INPUT
	COMPARE s1, TDC_UC_ESCAPE_CHAR
	JUMP NZ, CMD_TDC_UC_UART_MON
	;; escape char ends command
	JUMP CMD_TDC_UC_UART_END

;;; ============================================================================
;;; TDC uC UART PROGRAM NO RETURN
;;; input 
;;; 	CLIWORD1: argument 0/1 for TDC uC 0/1
;;; uses:
;;; 	s0
;;; 	s1
;;;     s2
;;; ============================================================================
CMD_TDC_UC_UART_PROGRAM_NR:
	FETCH s0, CLI_COUNT
	COMPARE s0, 01		;there must be one argument
	JUMP NZ, ERROR_BAD_ARG

	CALL CMD_TDC_UC_UART_RESET

	;; load 0/1 TDC UC and store in s2
	LOAD s1, CLI_WORD_1
	FETCH s2, (s1)

	;; check if we want to talk to TDC 0
	COMPARE s2, 00
	JUMP NZ, CMD_TDC_UC_UART1_PROGRAM_NR ;nope
	LOAD s1, TDC_UC_DEFAULT
	OR s1, TDC_UC_PASS_EN
	OUTPUT s1, TDC_UC_CONFIG ;switch io
	JUMP CMD_TDC_UC_UART_MON_PROGRAM_NR
CMD_TDC_UC_UART1_PROGRAM_NR:
	;; check if we want to talk to TDC 1
	COMPARE s2, 01
	JUMP NZ, CMD_TDC_UC_UART_END	;nope
	LOAD s1, TDC_UC_DEFAULT
	OR s1, TDC_UC_PASS_EN
	OR s1, TDC_UC_PASS
	OUTPUT s1, TDC_UC_CONFIG ;switch io
	JUMP CMD_TDC_UC_UART_MON_PROGRAM_NR

CMD_TDC_UC_UART_END_PROGRAM_NR:
	;; reset to normal
	LOAD s1, TDC_UC_DEFAULT
	OUTPUT s1, TDC_UC_CONFIG
	LOAD s1, "L"
	OUTPUT s1, UART_OUTPUT
	LOAD s1, "B"
	OUTPUT s1, UART_OUTPUT
	CALL util_print_EOL
	RETURN

CMD_TDC_UC_UART_MON_PROGRAM_NR:
	;; Stay in this loop forever
	JUMP CMD_TDC_UC_UART_MON_PROGRAM_NR


;;; ============================================================================
;;; TDC uC UART PROGRAM
;;; input 
;;; 	CLIWORD1: argument 0/1 for TDC uC 0/1
;;; uses:
;;; 	s0
;;; 	s1
;;;     s2
;;; ============================================================================
CMD_TDC_UC_UART_PROGRAM:
	FETCH s0, CLI_COUNT
	COMPARE s0, 01		;there must be one argument
	JUMP NZ, ERROR_BAD_ARG
	
	CALL CMD_TDC_UC_UART_RESET
	LOAD s3, 0F
	
	;; load 0/1 TDC UC and store in s2
	LOAD s1, CLI_WORD_1
	FETCH s2, (s1)

	;; check if we want to talk to TDC 0
	COMPARE s2, 00
	JUMP NZ, CMD_TDC_UC_UART1_PROGRAM ;nope
	LOAD s1, TDC_UC_DEFAULT
	OR s1, TDC_UC_PASS_EN
	OUTPUT s1, TDC_UC_CONFIG ;switch io
	JUMP CMD_TDC_UC_UART_MON_PROGRAM
CMD_TDC_UC_UART1_PROGRAM:
	;; check if we want to talk to TDC 1
	COMPARE s2, 01
	JUMP NZ, CMD_TDC_UC_UART_END	;nope
	LOAD s1, TDC_UC_DEFAULT
	OR s1, TDC_UC_PASS_EN
	OR s1, TDC_UC_PASS
	OUTPUT s1, TDC_UC_CONFIG ;switch io
	JUMP CMD_TDC_UC_UART_MON_PROGRAM

CMD_TDC_UC_UART_END_PROGRAM:
	;; reset to normal
	LOAD s1, TDC_UC_DEFAULT
	OUTPUT s1, TDC_UC_CONFIG
	LOAD s1, "L"
	OUTPUT s1, UART_OUTPUT
	LOAD s1, "B"
	OUTPUT s1, UART_OUTPUT
	CALL util_print_EOL
	RETURN

CMD_TDC_UC_UART_END_PROGRAM_FAIL:
	;; exit and print an error
	LOAD s1, TDC_UC_DEFAULT
	OUTPUT s1, TDC_UC_CONFIG
	LOAD s1, "L"
	OUTPUT s1, UART_OUTPUT
	LOAD s1, "B"
	OUTPUT s1, UART_OUTPUT
	CALL util_print_EOL
	JUMP ERROR_BAD_ARG
	
CMD_TDC_UC_UART_MON_PROGRAM:
	;; code:
	CALL  uart_input_wait
	INPUT s1, UART_INPUT ; monitor what is sent to the TDC
	COMPARE s1, 30
	JUMP Z, PROC_BOOT_SYNC
	COMPARE s1, 41
	JUMP Z, PROC_BOOT_VERSION
	COMPARE s1, 50
	JUMP Z, PROC_BOOT_PROGMODE
	COMPARE s1, 75
	JUMP Z, PROC_BOOT_SIGN
	COMPARE s1, 55
	JUMP Z, PROC_BOOT_ADDR
	COMPARE s1, 64
	JUMP Z, PROC_BOOT_DATA
	COMPARE s1, 51
	JUMP Z, PROC_BOOT_EXIT
	SUB s3, 01
	JUMP Z, CMD_TDC_UC_UART_END_PROGRAM_FAIL ; exit the tdc if no command has been entered 15 times
	;; Next the page size will be sent, ignore all of those
	JUMP CMD_TDC_UC_UART_MON_PROGRAM ; just loop endlessly

PROC_BOOT_ADDR:
	;;wait for laddress
	call uart_input_wait
	INPUT s1, UART_INPUT ; monitor what is sent to the TDC
	;;wait for haddress
	call uart_input_wait
	INPUT s1, UART_INPUT ; monitor what is sent to the TDC
	;;wait for 0x20
	call uart_input_wait
	INPUT s1, UART_INPUT ; monitor what is sent to the TDC
	JUMP CMD_TDC_UC_UART_MON_PROGRAM
PROC_BOOT_VERSION:
	call uart_input_wait
	INPUT s1, UART_INPUT ; monitor what is sent to the TDC
	COMPARE s1, 81
	JUMP Z, PROC_BOOT_SYNC ; skip the next check if the compare matches
PROC_BOOT_PROGMODE:
PROC_BOOT_SIGN:
PROC_BOOT_SYNC:
	call uart_input_wait
	INPUT s1, UART_INPUT ; monitor what is sent to the TDC
	JUMP CMD_TDC_UC_UART_MON_PROGRAM
PROC_BOOT_DATA:
	call uart_input_wait
	INPUT s1, UART_INPUT ; monitor what is sent to the TDC
	COMPARE s1, 00
	JUMP NZ, CMD_TDC_UC_UART_END_PROGRAM_FAIL ; exit the tdc if the expected command isn't printed
	call uart_input_wait
	INPUT s1, UART_INPUT ; monitor what is sent to the TDC
	;;s1 should now be the page size (usually 0x80)	
	ADD s1, 02 ; these are 2 extra writes after a  data write
DATA_WRITING_LOOP:
	call uart_input_wait
	INPUT s2, UART_INPUT ; monitor what is sent to the TDC	
	SUB s1, 01
	JUMP Z, CMD_TDC_UC_UART_MON_PROGRAM
	JUMP DATA_WRITING_LOOP
PROC_BOOT_EXIT:
	CALL uart_input_wait
	INPUT s1, UART_INPUT ; monitor what is sent to the TDC
	JUMP CMD_TDC_UC_UART_END_PROGRAM
	
;;; ============================================================================
;;; TDC uC UART reset
;;; input 
;;; 	CLIWORD1: argument 0/1 for TDC uC 0/1
;;; uses:
;;; 	s0
;;; 	s1
;;;     s2
;;; ============================================================================
CMD_TDC_UC_UART_RESET:
	FETCH s0, CLI_COUNT
	COMPARE s0, 01		;there must be one argument
	JUMP NZ, ERROR_BAD_ARG
	
	;; load 0/1 TDC UC and store in s2
	LOAD s1, CLI_WORD_1
	FETCH s2, (s1)
	;; check if we want to talk to TDC 0
	COMPARE s2, 00
	JUMP NZ, CMD_TDC_UC_UART1_RESET ;nope
	LOAD s1, TDC_UC_DEFAULT
	OR s1, TDC_UC_RESET_0
	OUTPUT s1, TDC_UC_CONFIG ;switch io
	LOAD s0, 00
CMD_TDC_UC_UART0_DELAY:
	ADD s0,01
	JUMP NC, CMD_TDC_UC_UART0_DELAY

	LOAD s1, TDC_UC_DEFAULT
	OUTPUT s1, TDC_UC_CONFIG
	JUMP CMD_TDC_UC_UART_RESET_END
	
CMD_TDC_UC_UART1_RESET:
	;; check if we want to talk to TDC 1
	COMPARE s2, 01
	JUMP NZ, ERROR_BAD_ARG	;nope
	LOAD s1, TDC_UC_DEFAULT
	OR s1, TDC_UC_RESET_1
	OUTPUT s1, TDC_UC_CONFIG ;switch io
	LOAD s0, 00
CMD_TDC_UC_UART1_DELAY:
	ADD s0,01
	JUMP NC, CMD_TDC_UC_UART1_DELAY

	LOAD s1, TDC_UC_DEFAULT
	OUTPUT s1, TDC_UC_CONFIG
	JUMP CMD_TDC_UC_UART_RESET_END
	
CMD_TDC_UC_UART_RESET_END:
	RETURN
	
