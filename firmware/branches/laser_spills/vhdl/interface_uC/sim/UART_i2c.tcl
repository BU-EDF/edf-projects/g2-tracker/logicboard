source vhdl/interface_uC/sim/UART_helper.tcl

#set initial conditions for UART
isim force add {/uc/sc_rx} 1
isim force add {/uc/reset} 0

isim force add {/uc/tdc_sda_in} 1

#set clock on /uc/clk
isim force add {/uc/clk} 1 -radix bin -value 0 -radix bin -time 5 ns -repeat 10 ns 

#let everything start up
run 1 ms


#send tdc_sel command
sendUART_str i2c_sel_dac 115200 /uc/sc_rx;
#send CR (0x0D)
sendUART_hex 0D 115200 /uc/sc_rx;

run 2 ms

#send i2c_reg_rd command
sendUART_str i2c_reg_rd 115200 /uc/sc_rx;
#send space (0x20)
sendUART_hex 20 115200 /uc/sc_rx;
sendUART_str ac 115200 /uc/sc_rx;
#send space (0x20)
sendUART_hex 20 115200 /uc/sc_rx;
sendUART_str 00 115200 /uc/sc_rx;

#send CR (0x0D)
sendUART_hex 0D 115200 /uc/sc_rx;


run 10 ms
