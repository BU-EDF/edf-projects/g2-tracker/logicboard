library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- we want to generate a clock enable that goes off every micro-second
entity one_wire_timing is

  port (
    clk_in         : in  std_logic;
    reset          : in  std_logic;
    count_max      : in  unsigned(31 downto 0);
    clk_en_onewire : out std_logic);

end entity one_wire_timing;

architecture Behaviour of one_wire_timing is

  constant counter_start : unsigned(31 downto 0) := x"00000000";
  signal counter : unsigned(31 downto 0) := counter_start;
  
begin  -- architecture Behaviour

  en_counter : process (clk_in, reset) is
  begin  -- process en_counter
    if reset = '1' then                 -- asynchronous reset (active high)
      counter <= counter_start;
    elsif clk_in'event and clk_in = '1' then  -- rising clock edge
      -- send out a pulse for one clock tick every count_max clock ticks
      clk_en_onewire <= '0';
      counter        <= counter + 1;
      if counter = count_max then
        clk_en_onewire <= '1';
        counter        <= counter_start;
      end if;
    end if;
  end process en_counter;
  

end architecture Behaviour;
