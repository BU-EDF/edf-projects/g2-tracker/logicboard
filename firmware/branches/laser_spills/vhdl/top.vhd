----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
-- Create Date:    
-- Design Name: 
-- Module Name:    top_module - Behavioral
-- Project Name: LogicBoard
-- Target Devices: XC6SLX9-TQFP144
-- Tool versions: 13.3 (lin64)
-- Description: 
--
-- Dependencies: 
--
-- Revision:
-- Revision 0.1 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
use IEEE.std_logic_misc.all;
use ieee.std_logic_arith.all;

use work.LogicBoard_IO.all;
use work.EventBuilder_IO.all;
use work.C5_IO.all;
use work.Fake_spill_data.all;
use work.TestPulse_IO.all;

entity top is
  port (
    clk125_in : in std_logic;           -- 125Mhz osc in

    -----------------
    -- SFP
    --SFP controls
    SFP_SCL     : out   std_logic;
    SFP_SDA     : inout std_logic;
    SFP_LOS     : in    std_logic;
    SFP_Present : in    std_logic;
    SFP_TxDSBL  : out   std_logic;
    --SFP out
    data_out_P  : out   std_logic;      -- 8b10b out to TRM
    data_out_N  : out   std_logic;
    --SFP in
    c5_IN_P     : in    std_logic;      -- C5 clock differential signal in
    c5_IN_N     : in    std_logic;


    -----------------
    -- TDC signals
    -- i2c
    TDC_Rx       : in  std_logic;
    TDC_Tx       : out std_logic;
    -- 8b10b in
    TDC_serial_P : in  std_logic_vector(3 downto 0);  -- TDC 8b10b in 25Mhz
    TDC_serial_N : in  std_logic_vector(3 downto 0);
    -- test pulse
    ASDQ_TP_EN   : out std_logic_vector(3 downto 0);  -- enable test pulse circuits
    ASDQ_TP      : out std_logic_vector(7 downto 0);  -- send test pulses
    EN_P1V6      : out std_logic;
    EN_N1V6      : out std_logic;

    -----------------
    --clock out and control
    c5_stream_p   : out std_logic;      -- C5 out
    c5_stream_n   : out std_logic;
    TDC_C5_select : out std_logic;      -- Select C5 hardware distribution
                                        -- source signal
    -----------------
    -- RS422 interface
    SC_OE         : out std_logic;
    SC_N_Present  : in  std_logic;
    SC_IN         : in  std_logic;
    SC_OUT        : out std_logic;

    -----------------
    -- ASDQ/TDC monitoring & control
    DB_SDA  : inout std_logic;
    DB_SCL  : inout std_logic;
    DAC_SDA : inout std_logic;
    DAC_SCL : inout std_logic;

    -----------------    
    -- Temperature sensors
    TEMP_SENSE             : inout std_logic_vector(1 downto 0) := "ZZ";
--    TEMP_SENSE_PWR_DISABLE : inout std_logic_vector(1 downto 0) := "ZZ";

    -----------------    
    -- Debugging IOs
    LED  : out std_logic_vector(3 downto 0);  -- LEDs
    GPIO : out std_logic_vector(3 downto 0);

    SPI_CLK  : out std_logic;           -- clock for SPI serial commands    
    SPI_MOSI : out std_logic;           -- serial data line for writing to SPI flash
    SPI_MISO : in  std_logic;           -- serial data line for reading from SPI flash
    SPI_CS   : out std_logic;            -- SPI chip select

    --TDC_UART_TX : out std_logic_vector(1 downto 0);
    --TDC_UART_RX : out std_logic_vector(1 downto 0);
    --TDC_RESET : out std_logic_vector(1 downto 0);
    TDC_uC_UART_Tx  : out std_logic_vector(1 downto 0);
    TDC_uC_UART_Rx  : in  std_logic_vector(1 downto 0);
    TDC_uC_reset    : out std_logic_vector(1 downto 0);
    TDC_BOARD_ID : out std_logic_vector(1 downto 0)

    );
end top;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- ARCHITECTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
architecture Behavioral of top is

  -----------------------------------------------------------------------------
  -- Components
  -----------------------------------------------------------------------------
  component version is
    port (
      year    : out std_logic_vector(7 downto 0);
      month   : out std_logic_vector(7 downto 0);
      day     : out std_logic_vector(7 downto 0);
      version : out std_logic_vector(7 downto 0));
  end component version;

  component C5_clocking is
    port (
      CLK_IN_C5 : in  std_logic;
      CLK_40    : out std_logic;
      RESET     : in  std_logic;
      STATUS    : out std_logic_vector(2 downto 0);
      LOCKED    : out std_logic;
      CLK_VALID : out std_logic);
  end component C5_clocking;

  component DAQ_clocking
    port (
      CLK40_IN : in  std_logic;
      CLK_125  : out std_logic;
      CLK_40   : out std_logic;
      RESET    : in  std_logic;
      LOCKED   : out std_logic);
  end component;

  component local_clocking
    port (
      CLK125_IN : in  std_logic;
      CLK125    : out std_logic;
      CLK40     : out std_logic;
      CLK62_5   : out std_logic;
      RESET     : in  std_logic;
      LOCKED    : out std_logic);
  end component;

  component c5_top
    port (
      clk    : in  std_logic;
      reset  : in  std_logic;
      clk40  : in  std_logic;
      din    : in  std_logic_vector(4 downto 0);
      c5_en  : in  std_logic;
      c5_busy: out std_logic;    
      c5_out : out std_logic);
  end component;

  component event_builder
    generic (
      TDC_COUNT : integer);
    port (
      C5_stream  : in  std_logic;
      clk40      : in  std_logic;
      reset40    : in  std_logic;
      clk125     : in  std_logic;
      reset125   : in  std_logic;
      TDC_serial : in  std_logic_vector(3 downto 0);
      TDC_spy    : out std_logic_vector(3 downto 0);
      Firmware_v : in  std_logic_vector(31 downto 0);
      control    : in  EventBuilder_Control_t;
      data_out   : out std_logic;
      monitoring : out EventBuilder_Monitor_t;
      new_spill  : out std_logic;       -- for test pulse sync
      spill_type : out std_logic_vector(3 downto 0);
      sending_spill : out std_logic);
  end component;

  component control_register
    port (
      clk                  : in  std_logic;
      reset                : in  std_logic;
      Firmware_Version     : in  std_logic_vector(31 downto 0);
      LB_monitor           : in  LogicBoard_Monitor_t;
      LB_control           : out LogicBoard_Control_t;
      EB_monitor           : in  EventBuilder_Monitor_t;
      EB_control           : out EventBuilder_Control_t;
      EB_reset             : out std_logic;
      EB_clk               : in  std_logic;
      C5_control           : out C5_cmd_Control_t;
      control_fake_TDC     : out Fake_TDC_Control_t;
      monitor_fake_TDC     : in  Fake_TDC_Control_t;
      enable_fake_TDCs_out : out std_logic;
      enable_fake_TDCs_in  : in  std_logic;
      control_testpulse    : out TestPulseControl_t;
      clk_uc               : in std_logic;
      address              : in  std_logic_vector(7 downto 0);
      out_port_rd          : in std_logic;
      out_port             : out std_logic_vector(31 downto 0);
      out_port_dv          : out std_logic;
      in_port              : in  std_logic_vector(31 downto 0);
      in_port_wr           : in  std_logic);
  end component;

  component reset_resync is
    generic (
      RESET_LENGTH : integer;
      RESET_VALUE  : std_logic);
    port (
      reset_in_async : in  std_logic;
      reset_in_sync  : in  std_logic;
      clk            : in  std_logic;
      reset_out      : out std_logic);
  end component reset_resync;
  
  component pacd
    port (
      iPulseA : in  std_logic;
      iClkA   : in  std_logic;
      iRSTAn  : in  std_logic;
      iClkB   : in  std_logic;
      iRSTBn  : in  std_logic;
      oPulseB : out std_logic);
  end component;

  component uC is
    port (
      clk             : in  std_logic;
      reset           : in  std_logic;
      SC_Rx           : in  std_logic;
      SC_Tx           : out std_logic;
      SC_NC           : in  std_logic;
      SC_OE           : out std_logic;
      TDC_UART_Rx          : in  std_logic;
      TDC_UART_Tx          : out std_logic;
      DAC_SDA_in      : in  std_logic;
      DAC_SDA_out     : out std_logic;
      DAC_SDA_en      : out std_logic;
      DAC_SCL         : out std_logic                    := '1';
      DB_SDA_in       : in  std_logic;
      DB_SDA_out      : out std_logic;
      DB_SDA_en       : out std_logic;
      DB_SCL          : out std_logic                    := '1';
      TDC_uC_UART_Tx  : out std_logic_vector(1 downto 0);
      TDC_uC_UART_Rx  : in  std_logic_vector(1 downto 0);
      TDC_uC_reset    : out std_logic_vector(1 downto 0);
      Wire_DS1820_in  : in  std_logic_vector(1 downto 0);
      Wire_DS1820_out : out std_logic_vector(1 downto 0) := "11";
      Wire_DS1820_T   : out std_logic_vector(1 downto 0) := "11";
      FLASH_SPI_CLK   : out std_logic;
      FLASH_SPI_MOSI  : out std_logic;
      FLASH_SPI_MISO  : in  std_logic;
      FLASH_SPI_CS    : out std_logic;
      reg_addr        : out std_logic_vector(7 downto 0);
      reg_data_in_rd  : out std_logic;
      reg_data_in     : in  std_logic_vector(31 downto 0);
      reg_data_in_dv  : in  std_logic;
      reg_data_out    : out std_logic_vector(31 downto 0);
      reg_wr          : out std_logic);
  end component uC;

  component fake_spill
    port (
      clk125   : in  std_logic;
      reset    : in  std_logic;
      control  : in  Fake_TDC_Control_t;
      data_out : out std_logic_vector(3 downto 0));
  end component;

  component TestPulse_top is
    port (
      clk40      : in  std_logic;
      reset      : in  std_logic;
      spill_type : in  std_logic_vector(3 downto 0);
      new_spill  : in  std_logic;
      TestPulse  : out std_logic_vector(7 downto 0);
      clkdaq     : in  std_logic;
      control    : in  TestPulseControl_t);
  end component;
--  end component TestPulse_top;

  -----------------------------------------------------------------------------
  -- signals
  -----------------------------------------------------------------------------  
  signal reset        : std_logic;
  signal reset_EB_125 : std_logic;
  signal reset_EB_40  : std_logic;
--  signal reset_C5_clk : std_logic;

  -- clocking from clk125
  signal clk125_local : std_logic;
  signal clk40_local  : std_logic;
  signal clk62_5_local : std_logic;
  signal local_clock_not_locked : std_logic;

  -- daq clocks
--  signal daq_clock_source   : std_logic;
  signal clk40              : std_logic;
  signal clk125_daq         : std_logic;
  signal clk40_daq          : std_logic;
  signal locked_daq         : std_logic;
  signal daq_clock_not_locked : std_logic;
  signal daq_clock_src_not_locked : std_logic;
  
  -- C5 output clock source  
  signal clk_C5_fake        : std_logic;
--  signal clk40_c5           : std_logic;
  signal reset_ext_clk      : std_logic;
--  signal clk125_ext         : std_logic;
  signal clk40_ext          : std_logic;
  signal ext_clk_mon_locked : std_logic                    := '0';
  signal ext_clk_mon_valid  : std_logic                    := '0';
  signal ext_clk_mon_status : std_logic_vector(2 downto 0) := "000";

  -- C5 generator signals
  signal C5_Raw_stream      : std_logic;
--  signal c5_source_for_TDCs : std_logic;

  -- TDC
  signal TDC_serial      : std_logic_vector(3 downto 0);
  signal TDC_serial_real : std_logic_vector(3 downto 0);
  signal TDC_serial_fake : std_logic_vector(3 downto 0);
  signal TDC_source      : std_logic := '0';
  signal TDC_spy         : std_logic_vector(3 downto 0);

  -- data out
  signal data_out        : std_logic;
  signal data_out_driver : std_logic;
  signal data_out_mux    : std_logic := '0';  -- '0'= normal; '1'=c5 loopback
  signal SFP_sending_spill : std_logic := '0';
  
  -- TDC UART
  signal TDC_Tx_local : std_logic := '1';

  --I2C busses

  signal DAC_SDA_in  : std_logic;
  signal DAC_SDA_out : std_logic := '1';
  signal DAC_SDA_en  : std_logic := '0';

  signal DB_SDA_in  : std_logic;
  signal DB_SDA_out : std_logic := '1';
  signal DB_SDA_en  : std_logic := '0';

  -- control / monitor register signals
  signal uC_GPIO           : std_logic_vector(3 downto 0);
  signal LB_monitor        : LogicBoard_Monitor_t;
  signal LB_control        : LogicBoard_Control_t;
  signal EB_monitor        : EventBuilder_Monitor_t;
  signal EB_control        : EventBuilder_Control_t;
  signal EB_reset          : std_logic        := '0';
  signal control_fake_TDCs : Fake_TDC_control_t := ('0', x"000000", x"00000000000", "00000010000");
  signal C5_control        : C5_cmd_Control_t;
--  signal c5_reset          : std_logic := '1';
  
  signal daq_reg_addr     : std_logic_vector(7 downto 0) := x"00";
  signal daq_reg_data_rd  : std_logic := '0';
  signal daq_reg_data_out : std_logic_vector(31 downto 0);
  signal daq_reg_data_dv  : std_logic := '0';
  signal daq_reg_data_in  : std_logic_vector(31 downto 0);
  signal daq_reg_wr       : std_logic                    := '0';

  -- slow control interface 
  signal SC_OE_buffer  : std_logic;
  signal SC_OUT_buffer : std_logic;
  signal SC_IN_buffer  : std_logic;


  -- interface to test pulse injector
  signal C5_new_spill      : std_logic;
  signal C5_spill_type     : std_logic_vector(3 downto 0);
  signal TestPulse_control : TestPulseControl_t;

  -- firmware version
  signal Firmware_version : std_logic_vector(31 downto 0);

  -- temperature sensors
  signal Temp_sense_in : std_logic_vector(1 downto 0) := "11";
  signal Temp_sense_out : std_logic_vector(1 downto 0) := "11";
  signal Temp_sense_T : std_logic_vector(1 downto 0) := "11";
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- BEGIN
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
begin
  -- firmware version
--  version_1 : version
  version_1 : version
    port map (
      year    => Firmware_version(31 downto 24),
      month   => Firmware_version(23 downto 16),
      day     => Firmware_version(15 downto 8),
      version => Firmware_version(7 downto 0));



  ---TDC Addressing (bit zero is the iTDC furthest from the LB on the feedthrough board)
  TDC_BOARD_ID(0) <= '0';
  TDC_BOARD_ID(1) <= '1';
  
  --Logic board monitoring
  LED            <= LB_control.LED;
--  LB_monitor.LED <= LB_control.LED;

--  GPIO <= TDC_serial_real(0) &TDC_serial(0) & C5_Raw_stream & "0";--LB_control.GPIO;
--  GPIO            <= TDC_serial_real(0) & TDC_spy(0) & TDC_serial_real(3) & TDC_spy(3);

  GPIO_mux: process (LB_control.GPIO_mux_sel,TDC_spy,EB_monitor.TDC_CDR_lost_alignment,EB_monitor.TDC_CDR_phase_switch,TDC_serial_real,data_out_driver,C5_Raw_stream,C5_new_spill) is
  begin  -- process GPIO_mux
    case LB_control.GPIO_mux_sel is
      when x"05" => GPIO <= data_out_driver & SFP_sending_spill & C5_Raw_stream & C5_new_spill;
      when x"04" => GPIO <= TDC_spy(3) & or_reduce(EB_monitor.TDC_CDR_lost_alignment(3))  & EB_monitor.TDC_CDR_phase_switch(3) & TDC_serial_real(3) ;
      when x"03" => GPIO <= TDC_spy(2) & or_reduce(EB_monitor.TDC_CDR_lost_alignment(2))  & EB_monitor.TDC_CDR_phase_switch(2) & TDC_serial_real(2) ;
      when x"02" => GPIO <= TDC_spy(1) & or_reduce(EB_monitor.TDC_CDR_lost_alignment(1))  & EB_monitor.TDC_CDR_phase_switch(1) & TDC_serial_real(1) ;
      when x"01" => GPIO <= TDC_spy(0) & or_reduce(EB_monitor.TDC_CDR_lost_alignment(0))  & EB_monitor.TDC_CDR_phase_switch(0) & TDC_serial_real(0) ;                   
      when x"00" => GPIO <= LB_control.GPIO;
      when others => GPIO <= LB_control.GPIO;
    end case;
  end process GPIO_mux;
  
--  LB_monitor.GPIO <= LB_control.GPIO;

  LB_monitor.SFP_present <= SFP_Present;
  LB_monitor.SFP_LOS     <= SFP_LOS;
--  LB_monitor.SFP_TxDSBL  <= LB_control.SFP_TxDSBL;

  SFP_TxDSBL <= LB_control.SFP_TxDSBL;

  -- SFP
  SFP_SCL <= '1';
  SFP_SDA <= '1';

  -- TDC clock source
  TDC_C5_select               <= LB_control.TDC_clock_select;
  --LB_monitor.TDC_clock_select <= LB_control.TDC_clock_select;


  -----------------------------------------------------------------------------
  -- local clock generation from 125Mhz oscillator
  -----------------------------------------------------------------------------
  -- Input buffering
  --------------------------------------
  local_clocking_1 : local_clocking
    port map (
      CLK125_IN => clk125_in,
      CLK125    => clk125_local,
      CLK40     => clk40_local,
      CLK62_5   => clk62_5_local,
      RESET     => '0',
      LOCKED    => LB_monitor.local_clock_locked);

  --global reset controlled by 125Mhz clock lock
  local_clock_not_locked <= not LB_monitor.local_clock_locked;
  --  reset_resync_1: reset_resync
  reset_resync_1: reset_resync
    generic map (
      RESET_LENGTH => 25,
      RESET_VALUE  => '1')
    port map (
      reset_in_async => local_clock_not_locked,
      reset_in_sync  => '0', -- no sync reset
      clk            => clk125_local,
      reset_out      => reset);



  -----------------------------------------------------------------------------
  -- external Input clock (C5)
  -----------------------------------------------------------------------------
  --reset C5 based things on 125Mhz clock bad or internal signal
--  reset_ext_clk              <= (not LB_monitor.local_clock_locked) or LB_control.ext_clock_reset;
  --LB_monitor.ext_clock_reset <= reset_ext_clk;
  LB_monitor.ext_clock_reset <= LB_control.ext_clock_reset;

  -- Take in external C5 clock, lock on and generate clocks needed to parse C5
  -- commands and run the daq clocking processor
  clkin1_buf : IBUFGDS
    port map
    (I  => C5_in_P,
     IB => C5_in_N,
     O  => C5_Raw_stream);

  C5_clocking_1 : C5_clocking
    port map (
      CLK_IN_C5   => C5_Raw_stream,
      CLK_40      => clk40_ext,
      RESET       => LB_control.ext_clock_reset,
      LOCKED      => LB_monitor.ext_clock_locked,
      STATUS      => LB_monitor.ext_clock_status,
      CLK_VALID   => LB_monitor.ext_clock_valid);

  --monitor the external clock
  C5_clock_monitor : process (clk125_local, reset)
  begin  -- process C5_clock_monitor
    if reset = '1' then                 -- asynchronous reset (active low)
      LB_monitor.ext_clock_locked_ch <= '0';
      LB_monitor.ext_clock_status_ch <= "000";
      LB_monitor.ext_clock_valid_ch  <= '0';
    elsif clk125_local'event and clk125_local = '1' then  -- rising clock edge
      ext_clk_mon_locked <= LB_monitor.ext_clock_locked;
      ext_clk_mon_status <= LB_monitor.ext_clock_status;
      ext_clk_mon_valid  <= LB_monitor.ext_clock_valid;

      if LB_control.reset_ext_monitor = '1' then
        LB_monitor.ext_clock_locked_ch <= '0';
        LB_monitor.ext_clock_status_ch <= "000";
        LB_monitor.ext_clock_valid_ch  <= '0';
      else
        if ext_clk_mon_locked /= LB_monitor.ext_clock_locked then
          LB_monitor.ext_clock_locked_ch <= '1';
        end if;
        for iBit in 2 downto 0 loop
          if ext_clk_mon_status(iBit) /= LB_monitor.ext_clock_status(iBit) then
            LB_monitor.ext_clock_status_ch(iBit) <= '1';
          end if;
        end loop;  -- iBit
        if ext_clk_mon_valid /= LB_monitor.ext_clock_valid then
          LB_monitor.ext_clock_valid_ch <= '1';
        end if;
      end if;
    end if;
  end process C5_clock_monitor;

  -----------------------------------------------------------------------------
  -- muxing external and internal clocks into daq clock generation
  -----------------------------------------------------------------------------
--  clock_mux_control : process (clk125_local) is
--  begin  -- process clock_mux_control
--    if clk125_local'event and clk125_local = '1' then  -- rising clock edge
--      daq_clock_source <= LB_control.daq_clock_source;
--    end if;
--  end process clock_mux_control;

  clk40_mux : BUFGMUX
    generic map (
      CLK_SEL_TYPE => "ASYNC")
    port map (
      I0 => clk40_ext,
      I1 => clk40_local,
      O  => clk40,
      S  => LB_control.daq_clock_source);
--  LB_monitor.daq_clock_source <= daq_clock_source;

  -- keep the reset condition for the daq clocks in line with the clock source
  -- it is using.
  daq_clock_src_monitoring: process (LB_control.daq_clock_source) is
  begin  -- process daq_clock_src_monitoring
    if LB_control.daq_clock_source = '1' then
      daq_clock_src_not_locked <= not LB_monitor.local_clock_locked;
    else
      daq_clock_src_not_locked <= not LB_monitor.ext_clock_locked; 
    end if;
  end process daq_clock_src_monitoring;

  --  reset_resync_2: reset_resync
  -- hold reset out for 25 125Mhz clocks, which equals 8 40Mhz clocks
  reset_resync_2: reset_resync
    generic map (
      RESET_LENGTH => 25,
      RESET_VALUE  => '1')
    port map (
      reset_in_async => daq_clock_src_not_locked,
      reset_in_sync  => LB_Control.daq_clock_reset,
      clk            => clk125_local,
      reset_out      => LB_monitor.daq_clock_reset);
  
  -- generate the clocks for the daq connection
  DAQ_clocking_1 : DAQ_clocking
    port map (
      CLK40_IN => clk40,
      CLK_125  => clk125_daq,
      CLK_40   => clk40_daq,
      RESET    => LB_monitor.daq_clock_reset,
      LOCKED   => LB_monitor.daq_clock_locked);

  -----------------------------------------------------------------------------
  -- Generate fake C5 to send to the 
  -----------------------------------------------------------------------------
  -- Local "fake" C5 generation for debugging
  c5_top_1 : c5_top
    port map (
      clk    => clk125_local,
      reset  => LB_monitor.daq_clock_reset,
      clk40  => clk40_daq,
      din    => C5_control.cmd_data,
      c5_busy=> open,
      c5_en  => C5_control.cmd_strobe,
      c5_out => clk_C5_fake);

  -- output buffer for Logic board's fake C5
  c5_out : OBUFDS_LVDS_33
    port map (
      I  => clk_c5_fake,
      O  => c5_stream_P,
      OB => c5_stream_N);



  -----------------------------------------------------------------------------
  -- TDC input/output
  -----------------------------------------------------------------------------

  -- capture TDC dif signals
  TDC_input : for iTDC in 0 to 3 generate
    ibufds : IBUFDS_LVDS_33
      port map (
        O  => TDC_serial_real(iTDC),
        I  => TDC_serial_P(iTDC),
        IB => TDC_serial_N(iTDC));
  end generate TDC_input;

  -- generate fake TDC data
  fake_spill_1 : fake_spill
    port map (
      clk125   => clk125_daq,
      reset    => reset,
      control  => control_fake_TDCs,
      data_out => TDC_serial_fake);


  select_TDC_source : process (TDC_source, TDC_serial_real, TDC_serial_fake)
  begin  -- process select_TDC_source
    if TDC_source = '0' then
      TDC_serial <= TDC_serial_real;
    else
      TDC_serial <= TDC_serial_fake;
    end if;
  end process select_TDC_source;

  -----------------------------------------------------------------------------
  -- TDC/C5 event building
  -----------------------------------------------------------------------------
  --reset EB on logic board or clock not locked
  daq_clock_not_locked <= not LB_monitor.daq_clock_locked;
--  reset_EB_125Mhz: reset_resync
  reset_EB_125Mhz: reset_resync
    generic map (
      RESET_LENGTH => 25, -- reset for 25 clk125 periods (200ns) (common to 40
      -- and 125
      RESET_VALUE  => '1')
    port map (
      reset_in_async => daq_clock_not_locked,
      reset_in_sync  => EB_reset, -- sync control of reset
      clk            => clk125_daq,
      reset_out      => reset_EB_125);
--  reset_EB_40Mhz: reset_resync
  reset_EB_40Mhz: reset_resync
    generic map (
      RESET_LENGTH => 8,
      RESET_VALUE  => '1')
    port map (
      reset_in_async => daq_clock_not_locked,
      reset_in_sync  => EB_reset,
      clk            => clk40_daq,
      reset_out      => reset_EB_40);

  event_builder_1 : event_builder
    generic map (
      TDC_COUNT => 4)
    port map (
      C5_stream  => C5_Raw_stream,
      clk40      => clk40_daq,
      reset40    => reset_EB_40,
      clk125     => clk125_daq,
      reset125   => reset_EB_125,
      TDC_serial => TDC_serial,
      TDC_spy    => TDC_spy,
      Firmware_v => Firmware_Version,
      control    => EB_control,
      data_out   => data_out,
      monitoring => EB_monitor,
      new_spill  => C5_new_spill,
      spill_type => C5_spill_type,
      sending_spill => SFP_sending_spill);


  -----------------------------------------------------------------------------
  -- Logicboard SFP out (DAQ OUT)
  -----------------------------------------------------------------------------
  LB_monitor.SFP_out_mux <= LB_control.SFP_out_mux;

  -- switch between the data_out stream (normal) or the fake c5 stream (debugging)
  output_mux : process (data_out, clk_C5_fake, LB_control.SFP_out_mux)
  begin  -- process output_mux
    if LB_control.SFP_out_mux = '0' then
      data_out_driver <= data_out;
    else
      data_out_driver <= clk_C5_fake;
    end if;
  end process output_mux;

  -- output the stream to the SFP
  obufds : OBUFDS_LVDS_33
    port map (
      I  => data_out_driver,
      O  => data_out_p,
      OB => data_out_n);



  -----------------------------------------------------------------------------
  -- EB register
  -----------------------------------------------------------------------------
  control_register_1 : control_register
    port map (
      --clk                  => clk125_daq,
      clk                  => clk125_local,
      reset                => reset,
      Firmware_Version     => Firmware_version,
      LB_monitor           => LB_monitor,
      LB_control           => LB_control,
      EB_monitor           => EB_monitor,
      EB_control           => EB_control,
      EB_clk               => clk125_daq,
      C5_control           => C5_control,
      EB_reset             => EB_reset,
      control_fake_TDC     => control_fake_TDCs,
      monitor_fake_TDC     => control_fake_TDCs,
      enable_fake_TDCs_out => TDC_source,
      enable_fake_TDCs_in  => TDC_source,
      control_testpulse    => TestPulse_control,
      clk_uC               => clk62_5_local,
      address              => daq_reg_addr,
      out_port_rd          => daq_reg_data_rd,
      out_port             => daq_reg_data_out,
      out_port_dv          => daq_reg_data_dv,
      in_port              => daq_reg_data_in,
      in_port_wr           => daq_reg_wr);


  -----------------------------------------------------------------------------
  -- Test pulse
  -----------------------------------------------------------------------------  
  -- ASDQ TP enable
  ASDQ_TP_EN               <= LB_control.Test_pulse_en;
  --  LB_monitor.Test_pulse_en <= LB_control.Test_pulse_en;

  -- power
  EN_P1V6                     <= LB_control.Test_pulse_power(0);
  EN_N1V6                     <= LB_control.Test_pulse_power(1);
--
--  LB_monitor.Test_pulse_power <= LB_control.Test_pulse_power;

  -----------------------------------------------------------------------------
  -- Slow control interface & monitoring (for now)
  -----------------------------------------------------------------------------

  uC_1 : uC
    port map (
      clk                  => clk62_5_local,--clk125_local,
      reset                => reset,
      SC_Rx                => SC_IN,
      SC_Tx                => SC_OUT,
      SC_NC                => SC_N_Present,
      SC_OE                => SC_OE,
      TDC_UART_Rx          => TDC_Rx,
      TDC_UART_Tx          => TDC_Tx_local,
      DAC_SDA_in           => DAC_SDA_in,
      DAC_SDA_out          => DAC_SDA_out,
      DAC_SDA_en           => DAC_SDA_en,
      DAC_SCL              => DAC_SCL,
      DB_SDA_in            => DB_SDA_in,
      DB_SDA_out           => DB_SDA_out,
      DB_SDA_en            => DB_SDA_en,
      DB_SCL               => DB_SCL,
      TDC_uC_UART_Tx       => TDC_uC_UART_Tx,
      TDC_uC_UART_Rx       => TDC_uC_UART_Rx,   
      TDC_uC_reset         => TDC_uC_reset,
      Wire_DS1820_in       => Temp_sense_in,
      Wire_DS1820_out      => Temp_sense_out,
      Wire_DS1820_T        => Temp_sense_T,
      FLASH_SPI_CLK        => SPI_CLK,
      FLASH_SPI_MOSI       => SPI_MOSI,
      FLASH_SPI_MISO       => SPI_MISO,
      FLASH_SPI_CS         => SPI_CS,
      reg_addr             => daq_reg_addr,
      reg_data_in_rd       => daq_reg_data_rd,
      reg_data_in          => daq_reg_data_out,
      reg_data_in_dv       => daq_reg_data_dv,
      reg_data_out         => daq_reg_data_in,
      reg_wr               => daq_reg_wr);


  -----------------------------------------------------------------------------
  -- Onewire sensors
  -----------------------------------------------------------------------------
  OneWire_inout0 : IOBUF
    port map (
      O  => Temp_sense_in(0),
      IO => TEMP_SENSE(0),
      I  => Temp_sense_out(0),
      T  => Temp_sense_T(0) );
  OneWire_inout1 : IOBUF
    port map (
      O  => Temp_sense_in(1),
      IO => TEMP_SENSE(1),
      I  => Temp_sense_out(1),
      T  => Temp_sense_T(1) );
  

  -----------------------------------------------------------------------------
  -- I2C tristates
  -----------------------------------------------------------------------------
  TDC_Tx <= 'Z' when TDC_Tx_local = '1' else '0';

  DAC_SDA_inout : IOBUF
    port map (
      O  => DAC_SDA_in,
      IO => DAC_SDA,
      I  => DAC_SDA_out,
      T  => DAC_SDA_out);

  DB_SDA_inout : IOBUF
    port map (
      O  => DB_SDA_in,
      IO => DB_SDA,
      I  => DB_SDA_out,
      T  => DB_SDA_out);

  -----------------------------------------------------------------------------
  -- Test pulse injection
  -----------------------------------------------------------------------------
  TestPulse_top_1 : TestPulse_top
    port map (
      clk40      => clk40_daq,
      reset      => reset,
      spill_type => C5_spill_type,
      new_spill  => C5_new_spill,
      TestPulse  => ASDQ_TP,
      clkdaq     => clk125_daq,
      control    => TestPulse_control);




end Behavioral;
