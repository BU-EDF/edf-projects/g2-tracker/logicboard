----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;


package LogicBoard_IO is
  type LogicBoard_Monitor_t is
  record
    SFP_present : std_logic;
    SFP_LOS     : std_logic;
    SFP_TxDSBL  : std_logic;
    SFP_out_mux : std_logic;

    -- local 125Mhz oscillators DCM
    local_clock_locked : std_logic;

    -- external 40Mhz DCM
    ext_clock_reset    : std_logic;
    ext_clock_locked   : std_logic;
    ext_clock_valid    : std_logic;
    ext_clock_status   : std_logic_vector(2 downto 0);
    ext_clock_locked_ch: std_logic;
    ext_clock_valid_ch : std_logic;
    ext_clock_status_ch: std_logic_vector(2 downto 0);

    -- control the source of the TDC C5 fanout
--    TDC_clock_select   : std_logic;

    -- local DAQ clock DCM
--    daq_clock_source : std_logic;
    daq_clock_reset  : std_logic;
    daq_clock_locked : std_logic;

--    Test_pulse_power : std_logic_vector(1 downto 0);
--    Test_pulse_en : std_logic_vector(3 downto 0);
    
--    LED         : std_logic_vector(3 downto 0);
    GPIO        : std_logic_vector(3 downto 0);
  end record;
  
  type LogicBoard_Control_t is record
    SFP_TxDSBL  : std_logic;
    SFP_out_mux : std_logic;

    ext_clock_reset    : std_logic;
    reset_ext_monitor  : std_logic;
    
    TDC_clock_select   : std_logic;
    
    daq_clock_source : std_logic;
    daq_clock_reset  : std_logic;
    
    Test_pulse_power : std_logic_vector(1 downto 0);
    Test_pulse_en : std_logic_vector(3 downto 0);
--    Test_pulse    : std_logic_vector(7 downto 0);
    
    LED         : std_logic_vector(3 downto 0);
    GPIO        : std_logic_vector(3 downto 0);
  end record;
end LogicBoard_IO;
