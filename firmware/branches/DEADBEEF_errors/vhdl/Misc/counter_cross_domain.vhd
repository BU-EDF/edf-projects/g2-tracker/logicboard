-------------------------------------------------------------------------------
-- Generic counter
-- Dan Gastler
-- Process count pulses and provide a buffered value of count
-- pass output to another clock domain
-- changes on the input clock domain should be slower than the output clock domain
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity counter_cross_domain is
  
  generic (
    roll_over   : std_logic := '1';
    end_value   : std_logic_vector := x"FFFFFFFF";
    start_value : std_logic_vector := x"00000000";
    DATA_WIDTH       : integer   := 32);
  port (
    clk   : in  std_logic; 
    reset : in  std_logic; --async
    event : in  std_logic; --@clk
    clkout : in std_logic;
    count : out unsigned(DATA_WIDTH-1 downto 0); --@clkout
    at_max : out std_logic --@clkout
    );

end entity counter_cross_domain;

architecture behavioral of counter_cross_domain is

  component pass_unsigned is
    generic (
      DATA_WIDTH : integer;
      RESET_VAL  : unsigned);
    port (
      clk_in   : in  std_logic;
      clk_out  : in  std_logic;
      reset    : in  std_logic;
      pass_in  : in  unsigned(DATA_WIDTH-1 downto 0);
      pass_out : out unsigned(DATA_WIDTH-1 downto 0));
  end component pass_unsigned;

  component pass_std_logic is
    generic (
      RESET_VAL : std_logic);
    port (
      clk_in   : in  std_logic;
      clk_out  : in  std_logic;
      reset    : in  std_logic;
      pass_in  : in  std_logic;
      pass_out : out std_logic);
  end component pass_std_logic;
  
  constant max_count : unsigned(DATA_WIDTH-1 downto 0) := unsigned(end_value);
  constant min_count : unsigned(DATA_WIDTH-1 downto 0) := unsigned(start_value);
  signal local_count : unsigned(DATA_WIDTH-1 downto 0) := min_count;  
  signal local_at_max : std_logic := '0';
  
begin  -- architecture behavioral

  event_counter : process (clk, reset)
  begin  -- process malformed_counter
    if reset = '1' then                 -- asynchronous reset (active high)
      local_count <= min_count;
      local_at_max      <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- reset local_at_max
      local_at_max <= '0';

      -- count
      if event = '1' then
        if local_count = max_count then
          local_at_max <= '1';
          --roll over if requested
          if roll_over = '1' then
            local_count <= min_count;
          end if;
        else          
          local_count <= local_count + 1;
        end if;
      end if;
    end if;
  end process event_counter;

  pass_unsigned_1: entity work.pass_unsigned
    generic map (
      DATA_WIDTH => DATA_WIDTH,
      RESET_VAL  => min_count)
    port map (
      clk_in   => clk,
      clk_out  => clkout,
      reset    => reset,
      pass_in  => local_count,
      pass_out => count);

  pass_std_logic_1: entity work.pass_std_logic
    generic map (
      RESET_VAL => '0')
    port map (
      clk_in   => clk,
      clk_out  => clkout,
      reset    => reset,
      pass_in  => local_at_max,
      pass_out => at_max);
  
end architecture behavioral;

