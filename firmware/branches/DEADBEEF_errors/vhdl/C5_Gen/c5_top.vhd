-------------------------------------------------------------------------------
-- c5_top.vhd : top-level interface for C5 sender
-- manages clock domain transition from 125 to 40MHz
--
-- may eventually contain serializer for register access
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity c5_top is

  port (
    clk : in  std_logic;             -- ~100MHz clock for computer interface
    reset  : in std_logic;           -- reset fifo
    clk40  : in  std_logic;             -- 40MHz clock to run C5 output
    din    : in  std_logic_vector(4 downto 0);  -- data in (0:3) plus CTRL
    c5_en  : in  std_logic;             -- transmit enable
    c5_busy: out std_logic;             -- can't accept new commands
    c5_out : out std_logic);            -- encoded C5 output

end entity c5_top;

architecture arch of c5_top is

  component c5_sender is
    port (
      clk40 : in  std_logic;
      reset : in  std_logic;
      en    : in  std_logic;
      B     : in  std_logic_vector (3 downto 0);
      cd    : in  std_logic;
      q0    : in  std_logic;
      c5    : out std_logic;
      busy  : out std_logic;
      frame : out std_logic);
  end component c5_sender;

  component C5_command_fifo is
    port (
      rst    : IN  STD_LOGIC;
      wr_clk : IN  STD_LOGIC;
      rd_clk : IN  STD_LOGIC;
      din    : IN  STD_LOGIC_VECTOR(4 DOWNTO 0);
      wr_en  : IN  STD_LOGIC;
      rd_en  : IN  STD_LOGIC;
      dout   : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
      full   : OUT STD_LOGIC;
      empty  : OUT STD_LOGIC;
      valid  : OUT STD_LOGIC);
  end component C5_command_fifo;
  

  signal data_r : std_logic_vector(3 downto 0);
  signal cd_r   : std_logic;
  signal c5_send : std_logic := '0';
  signal c5_sending : std_logic := '0';
  signal dout : std_logic_vector(4 downto 0);
  signal dout_valid : std_logic := '0';

begin  -- architecture arch


  C5_command_fifo_1: entity work.C5_command_fifo
    port map (
      rst    => reset,
      wr_clk => clk,
      rd_clk => clk40,
      din    => din,
      wr_en  => c5_en,
      rd_en  => c5_send,
      dout   => dout,
      full   => c5_busy,
      empty  => open,
      valid  => dout_valid);

  process (clk40,reset) is
  begin -- process
    if reset = '1' then
      c5_send <= '0';
      data_r <= (others => '0');
      cd_r <= '0';
    elsif clk40'event and clk40 = '1' then
      c5_send <= '0';
      if dout_valid = '1' and c5_send = '0' and c5_sending = '0' then
        data_r <= dout(3 downto 0);
        cd_r <= dout(4);
        c5_send <= '1';
      end if;
    end if;
  end process;

  c5_sender_2 : entity work.c5_sender
    port map (
      clk40 => clk40,      
      reset => reset,
      en    => c5_send,
      B     => data_r,
      cd    => cd_r,
      q0    => '0',
      c5    => c5_out,
      busy  => c5_sending,
      frame => open);

end architecture arch;
