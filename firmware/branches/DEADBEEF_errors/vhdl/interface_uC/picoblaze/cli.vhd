--
-------------------------------------------------------------------------------------------
-- Copyright � 2010-2013, Xilinx, Inc.
-- This file contains confidential and proprietary information of Xilinx, Inc. and is
-- protected under U.S. and international copyright and other intellectual property laws.
-------------------------------------------------------------------------------------------
--
-- Disclaimer:
-- This disclaimer is not a license and does not grant any rights to the materials
-- distributed herewith. Except as otherwise provided in a valid license issued to
-- you by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE
-- MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY
-- DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
-- INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT,
-- OR FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable
-- (whether in contract or tort, including negligence, or under any other theory
-- of liability) for any loss or damage of any kind or nature related to, arising
-- under or in connection with these materials, including for any direct, or any
-- indirect, special, incidental, or consequential loss or damage (including loss
-- of data, profits, goodwill, or any type of loss or damage suffered as a result
-- of any action brought by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-safe, or for use in any
-- application requiring fail-safe performance, such as life-support or safety
-- devices or systems, Class III medical devices, nuclear facilities, applications
-- related to the deployment of airbags, or any other applications that could lead
-- to death, personal injury, or severe property or environmental damage
-- (individually and collectively, "Critical Applications"). Customer assumes the
-- sole risk and liability of any use of Xilinx products in Critical Applications,
-- subject only to applicable laws and regulations governing limitations on product
-- liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
-------------------------------------------------------------------------------------------
--
--
-- Definition of a program memory for KCPSM6 including generic parameters for the 
-- convenient selection of device family, program memory size and the ability to include 
-- the JTAG Loader hardware for rapid software development.
--
-- This file is primarily for use during code development and it is recommended that the 
-- appropriate simplified program memory definition be used in a final production design. 
--
--    Generic                  Values             Comments
--    Parameter                Supported
--  
--    C_FAMILY                 "S6"               Spartan-6 device
--                             "V6"               Virtex-6 device
--                             "7S"               7-Series device 
--                                                  (Artix-7, Kintex-7, Virtex-7 or Zynq)
--
--    C_RAM_SIZE_KWORDS        1, 2 or 4          Size of program memory in K-instructions
--
--    C_JTAG_LOADER_ENABLE     0 or 1             Set to '1' to include JTAG Loader
--
-- Notes
--
-- If your design contains MULTIPLE KCPSM6 instances then only one should have the 
-- JTAG Loader enabled at a time (i.e. make sure that C_JTAG_LOADER_ENABLE is only set to 
-- '1' on one instance of the program memory). Advanced users may be interested to know 
-- that it is possible to connect JTAG Loader to multiple memories and then to use the 
-- JTAG Loader utility to specify which memory contents are to be modified. However, 
-- this scheme does require some effort to set up and the additional connectivity of the 
-- multiple BRAMs can impact the placement, routing and performance of the complete 
-- design. Please contact the author at Xilinx for more detailed information. 
--
-- Regardless of the size of program memory specified by C_RAM_SIZE_KWORDS, the complete 
-- 12-bit address bus is connected to KCPSM6. This enables the generic to be modified 
-- without requiring changes to the fundamental hardware definition. However, when the 
-- program memory is 1K then only the lower 10-bits of the address are actually used and 
-- the valid address range is 000 to 3FF hex. Likewise, for a 2K program only the lower 
-- 11-bits of the address are actually used and the valid address range is 000 to 7FF hex.
--
-- Programs are stored in Block Memory (BRAM) and the number of BRAM used depends on the 
-- size of the program and the device family. 
--
-- In a Spartan-6 device a BRAM is capable of holding 1K instructions. Hence a 2K program 
-- will require 2 BRAMs to be used and a 4K program will require 4 BRAMs to be used. It 
-- should be noted that a 4K program is not such a natural fit in a Spartan-6 device and 
-- the implementation also requires a small amount of logic resulting in slightly lower 
-- performance. A Spartan-6 BRAM can also be split into two 9k-bit memories suggesting 
-- that a program containing up to 512 instructions could be implemented. However, there 
-- is a silicon errata which makes this unsuitable and therefore it is not supported by 
-- this file.
--
-- In a Virtex-6 or any 7-Series device a BRAM is capable of holding 2K instructions so 
-- obviously a 2K program requires only a single BRAM. Each BRAM can also be divided into 
-- 2 smaller memories supporting programs of 1K in half of a 36k-bit BRAM (generally 
-- reported as being an 18k-bit BRAM). For a program of 4K instructions, 2 BRAMs are used.
--
--
-- Program defined by 'Z:\home\dan\Logicboard\branches\DEADBEEF_errors\vhdl\interface_uC\picoblaze\cli.psm'.
--
-- Generated by KCPSM6 Assembler: 01 Oct 2015 - 13:46:13. 
--
-- Assembler used ROM_form template: ROM_form_JTAGLoader_14March13.vhd
--
-- Standard IEEE libraries
--
--
package jtag_loader_pkg is
 function addr_width_calc (size_in_k: integer) return integer;
end jtag_loader_pkg;
--
package body jtag_loader_pkg is
  function addr_width_calc (size_in_k: integer) return integer is
   begin
    if (size_in_k = 1) then return 10;
      elsif (size_in_k = 2) then return 11;
      elsif (size_in_k = 4) then return 12;
      else report "Invalid BlockRAM size. Please set to 1, 2 or 4 K words." severity FAILURE;
    end if;
    return 0;
  end function addr_width_calc;
end package body;
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.jtag_loader_pkg.ALL;
--
-- The Unisim Library is used to define Xilinx primitives. It is also used during
-- simulation. The source can be viewed at %XILINX%\vhdl\src\unisims\unisim_VCOMP.vhd
--  
library unisim;
use unisim.vcomponents.all;
--
--
entity cli is
  generic(             C_FAMILY : string := "S6"; 
              C_RAM_SIZE_KWORDS : integer := 1;
           C_JTAG_LOADER_ENABLE : integer := 0);
  Port (      address : in std_logic_vector(11 downto 0);
          instruction : out std_logic_vector(17 downto 0);
               enable : in std_logic;
                  rdl : out std_logic;                    
                  clk : in std_logic);
  end cli;
--
architecture low_level_definition of cli is
--
signal       address_a : std_logic_vector(15 downto 0);
signal        pipe_a11 : std_logic;
signal       data_in_a : std_logic_vector(35 downto 0);
signal      data_out_a : std_logic_vector(35 downto 0);
signal    data_out_a_l : std_logic_vector(35 downto 0);
signal    data_out_a_h : std_logic_vector(35 downto 0);
signal   data_out_a_ll : std_logic_vector(35 downto 0);
signal   data_out_a_lh : std_logic_vector(35 downto 0);
signal   data_out_a_hl : std_logic_vector(35 downto 0);
signal   data_out_a_hh : std_logic_vector(35 downto 0);
signal       address_b : std_logic_vector(15 downto 0);
signal       data_in_b : std_logic_vector(35 downto 0);
signal     data_in_b_l : std_logic_vector(35 downto 0);
signal    data_in_b_ll : std_logic_vector(35 downto 0);
signal    data_in_b_hl : std_logic_vector(35 downto 0);
signal      data_out_b : std_logic_vector(35 downto 0);
signal    data_out_b_l : std_logic_vector(35 downto 0);
signal   data_out_b_ll : std_logic_vector(35 downto 0);
signal   data_out_b_hl : std_logic_vector(35 downto 0);
signal     data_in_b_h : std_logic_vector(35 downto 0);
signal    data_in_b_lh : std_logic_vector(35 downto 0);
signal    data_in_b_hh : std_logic_vector(35 downto 0);
signal    data_out_b_h : std_logic_vector(35 downto 0);
signal   data_out_b_lh : std_logic_vector(35 downto 0);
signal   data_out_b_hh : std_logic_vector(35 downto 0);
signal        enable_b : std_logic;
signal           clk_b : std_logic;
signal            we_b : std_logic_vector(7 downto 0);
signal          we_b_l : std_logic_vector(3 downto 0);
signal          we_b_h : std_logic_vector(3 downto 0);
-- 
signal       jtag_addr : std_logic_vector(11 downto 0);
signal         jtag_we : std_logic;
signal       jtag_we_l : std_logic;
signal       jtag_we_h : std_logic;
signal        jtag_clk : std_logic;
signal        jtag_din : std_logic_vector(17 downto 0);
signal       jtag_dout : std_logic_vector(17 downto 0);
signal     jtag_dout_1 : std_logic_vector(17 downto 0);
signal         jtag_en : std_logic_vector(0 downto 0);
-- 
signal picoblaze_reset : std_logic_vector(0 downto 0);
signal         rdl_bus : std_logic_vector(0 downto 0);
--
constant BRAM_ADDRESS_WIDTH  : integer := addr_width_calc(C_RAM_SIZE_KWORDS);
--
--
component jtag_loader_6
generic(                C_JTAG_LOADER_ENABLE : integer := 1;
                                    C_FAMILY : string  := "V6";
                             C_NUM_PICOBLAZE : integer := 1;
                       C_BRAM_MAX_ADDR_WIDTH : integer := 10;
          C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                                C_JTAG_CHAIN : integer := 2;
                              C_ADDR_WIDTH_0 : integer := 10;
                              C_ADDR_WIDTH_1 : integer := 10;
                              C_ADDR_WIDTH_2 : integer := 10;
                              C_ADDR_WIDTH_3 : integer := 10;
                              C_ADDR_WIDTH_4 : integer := 10;
                              C_ADDR_WIDTH_5 : integer := 10;
                              C_ADDR_WIDTH_6 : integer := 10;
                              C_ADDR_WIDTH_7 : integer := 10);
port(              picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                           jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                          jtag_din : out STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                         jtag_addr : out STD_LOGIC_VECTOR(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
                          jtag_clk : out std_logic;
                           jtag_we : out std_logic;
                       jtag_dout_0 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_1 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_2 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_3 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_4 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_5 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_6 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_7 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end component;
--
begin
  --
  --  
  ram_1k_generate : if (C_RAM_SIZE_KWORDS = 1) generate
 
    s6: if (C_FAMILY = "S6") generate 
      --
      address_a(13 downto 0) <= address(9 downto 0) & "0000";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "0000000000000000000000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "0000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB16BWER
      generic map ( DATA_WIDTH_A => 18,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 18,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900029180081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"0041003013005000F023F02010005000006001100335B001B031031400895000",
                    INIT_09 => X"D1010002D20100020041A030D00110200002D001103A0002D1010002D2010002",
                    INIT_0A => X"B227A0B5D004B023015450000059208E0059609AD00330031301003020A9D3FF",
                    INIT_0B => X"940520BCD010900331FFD1031108D004500000815000015408EC089D0210B12B",
                    INIT_0C => X"102C607CD001B0235000D10311F4D708D607D506D405D0045000970896079506",
                    INIT_0D => X"00530040005300500053006000530070D00110780002D0011030000200B8A000",
                    INIT_0E => X"A10010010610A10010010510A10010010410A1001030607CD002B02350000059",
                    INIT_0F => X"11721165116B116311611172115411201132112D1167500000C4A000102C0710",
                    INIT_10 => X"11001120113A11561120116411721161116F1142116311691167116F114C1120",
                    INIT_11 => X"0050D001102E000200530060D001102E00020053007000B8100100641AF51B00",
                    INIT_12 => X"1120113A1172116511661166117511425000005900530040D001102E00020053",
                    INIT_13 => X"116F115711001120113A1174116E1175116F1163112011641172116F11571100",
                    INIT_14 => X"116F115711001120112011201120113A11731165117A11691173112011641172",
                    INIT_15 => X"21591101D001A0100002E160C120B220110000641A281B011100112011641172",
                    INIT_16 => X"0059D00110290002D1010002D201000200410020D00110280002D00110200002",
                    INIT_17 => X"10200002E18EC3401300B423D1010002D20100020041B023000200641A311B01",
                    INIT_18 => X"1B010059217C1301D1010002D20100020041A01001301124D00110400002D001",
                    INIT_19 => X"0002D20100020041A01001301128D00110200002E1A3C3401300B42300641A3E",
                    INIT_1A => X"D20100020041003000641A4E1B010059E1C6C3401300B423005921941301D101",
                    INIT_1B => X"0041A060A1A6C65016030650152C4506450613010530D00110200002D1010002",
                    INIT_1C => X"000221D8D1FF21D4D1004BA01A121B075000005921BC9601D1010002D2010002",
                    INIT_1D => X"3DF0400740073003700160005000005921CA3B001A03005921CA3B001A01D101",
                    INIT_1E => X"01E401E401E4DD023DFDDD025D025D015000E1E59E011E3E50007000DD024D00",
                    INIT_1F => X"01E401E4DD023DFD01E401E4DD025D0101E401E4DD025D025000DD023DFE01E4",
                    INIT_20 => X"01E401E4DD025D0101E401E401E4DD023DFD500001E401E401E4DD023DFE01E4",
                    INIT_21 => X"01E401E4DD023DFE01E401E4DD025D01DD025D0201E45000DD025D0201E401E4",
                    INIT_22 => X"01E401E4DD025D0201E4DD023DFE01E401E4DD025D0101E4DD023DFD500001E4",
                    INIT_23 => X"3DFE01E401E4DD025D0101E4DD025D02223ADD023DFDA238440601E415805000",
                    INIT_24 => X"01E4DD023DFE01E401E4DD025D01950201E4DD025D0201E42232A244450EDD02",
                    INIT_25 => X"01E401E4DD025D0101E44400D602960201E4158014005D025000D50201E401E4",
                    INIT_26 => X"A27E0231040001E830FE70016330622061106000500001E4E257450EDD023DFE",
                    INIT_27 => X"2286100150007000400610000207A28402310430A28202310420A28002310410",
                    INIT_28 => X"7001622061106000500070004006108002070053228610042286100322861002",
                    INIT_29 => X"50007000400610000207A2A402310420A2A202310410A2A00231040001E830FE",
                    INIT_2A => X"30FE70016110600050007000400610800207005322A6100322A6100222A61001",
                    INIT_2B => X"0254022202400254A2CD02310400500101F4A28002310410A2C90231040001E8",
                    INIT_2C => X"005322CF100322CF100222CF1001500070006330622040061000020702150340",
                    INIT_2D => X"1100112E1172116F1172117211651120116B1163114150007000400610800207",
                    INIT_2E => X"D503A550152A1434A1401430A040142C607CD003B0235000005900641AD51B02",
                    INIT_2F => X"142C607CD002B0235000A2E0028C5000A2E00266A2F9D503A2401401A340A2F3",
                    INIT_30 => X"01DA1001500001DA1000500000590053003000530020A2E002ACA1401430A040",
                    INIT_31 => X"1172116111201164116111425000030B030E03116D0010F3500001DA10025000",
                    INIT_32 => X"6328D001B0235000005900641A1A1B03110011731174116E1165116D11751167",
                    INIT_33 => X"400640064006130F5000E3379E01E3379F011FFF1EFF5000D003301FA040142C",
                    INIT_34 => X"D528A7B0A6A0A590848000B803350B700A600950084000B812FF105230304006",
                    INIT_35 => X"084000B812FF002030304006400640064006130F5000E34493011201A3536352",
                    INIT_36 => X"E35E93011201636EA7B0636EA6A0636EA590636E848000B803350B700A600950",
                    INIT_37 => X"00D01257D0011020D001103A0059005300C0D0011050D00110200D001C005000",
                    INIT_38 => X"00D01258D0011032D001102C639BD20F035600D01256D0011031639BD20F0356",
                    INIT_39 => X"00D023B3DC061C01005923CCD20F033C00D0D0011033D001102C639BD20F0356",
                    INIT_3A => X"400640064006400600D0440644064406440604C000B810504006400640064006",
                    INIT_3B => X"10200002005300D0D00110430002D00110440002D00110540002237300C41050",
                    INIT_3C => X"D0011034D001102C50000059D00110520002D00110520002D00110450002D001",
                    INIT_3D => X"0002D00110200002005300D0D00110430002D00110440002D001105400020059",
                    INIT_3E => X"23F2D40200B810400371100023ECD40100B8104050000059005300C0D0011040",
                    INIT_3F => X"1F8050000371100323FED40800B810400371100223F8D40400B8104003711001",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"AAAAAA801041034A88888A2834A2AA803022AAA434AAB44DAA88A2AA82A2AAAA",
                   INITP_02 => X"2D0AA8A0A2AA88A298B420AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAAA",
                   INITP_03 => X"A8A8A8A2AA20B4B814DA9696B760A9AA8D45522AA882D0A6AA122D0829AA848A",
                   INITP_04 => X"8B4B8E38E2355AD8A890808AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA2",
                   INITP_05 => X"34BAED134000D2A0AAAAAAD2A222D528A2E0B8E235B4A888B4B8E388D5B4A888",
                   INITP_06 => X"088A2202D777768021154B5F5568021154B74280D2A0AAAAAAAAA4A28A2A2380",
                   INITP_07 => X"28C88C88C88C8A88A288A28A88A8A28A288A28A95515495535B62236088D8236")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a(31 downto 0),
                  DOPA => data_out_a(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b(31 downto 0),
                  DOPB => data_out_b(35 downto 32), 
                   DIB => data_in_b(31 downto 0),
                  DIPB => data_in_b(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --               
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900029180081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"0041003013005000F023F02010005000006001100335B001B031031400895000",
                    INIT_09 => X"D1010002D20100020041A030D00110200002D001103A0002D1010002D2010002",
                    INIT_0A => X"B227A0B5D004B023015450000059208E0059609AD00330031301003020A9D3FF",
                    INIT_0B => X"940520BCD010900331FFD1031108D004500000815000015408EC089D0210B12B",
                    INIT_0C => X"102C607CD001B0235000D10311F4D708D607D506D405D0045000970896079506",
                    INIT_0D => X"00530040005300500053006000530070D00110780002D0011030000200B8A000",
                    INIT_0E => X"A10010010610A10010010510A10010010410A1001030607CD002B02350000059",
                    INIT_0F => X"11721165116B116311611172115411201132112D1167500000C4A000102C0710",
                    INIT_10 => X"11001120113A11561120116411721161116F1142116311691167116F114C1120",
                    INIT_11 => X"0050D001102E000200530060D001102E00020053007000B8100100641AF51B00",
                    INIT_12 => X"1120113A1172116511661166117511425000005900530040D001102E00020053",
                    INIT_13 => X"116F115711001120113A1174116E1175116F1163112011641172116F11571100",
                    INIT_14 => X"116F115711001120112011201120113A11731165117A11691173112011641172",
                    INIT_15 => X"21591101D001A0100002E160C120B220110000641A281B011100112011641172",
                    INIT_16 => X"0059D00110290002D1010002D201000200410020D00110280002D00110200002",
                    INIT_17 => X"10200002E18EC3401300B423D1010002D20100020041B023000200641A311B01",
                    INIT_18 => X"1B010059217C1301D1010002D20100020041A01001301124D00110400002D001",
                    INIT_19 => X"0002D20100020041A01001301128D00110200002E1A3C3401300B42300641A3E",
                    INIT_1A => X"D20100020041003000641A4E1B010059E1C6C3401300B423005921941301D101",
                    INIT_1B => X"0041A060A1A6C65016030650152C4506450613010530D00110200002D1010002",
                    INIT_1C => X"000221D8D1FF21D4D1004BA01A121B075000005921BC9601D1010002D2010002",
                    INIT_1D => X"3DF0400740073003700160005000005921CA3B001A03005921CA3B001A01D101",
                    INIT_1E => X"01E401E401E4DD023DFDDD025D025D015000E1E59E011E3E50007000DD024D00",
                    INIT_1F => X"01E401E4DD023DFD01E401E4DD025D0101E401E4DD025D025000DD023DFE01E4",
                    INIT_20 => X"01E401E4DD025D0101E401E401E4DD023DFD500001E401E401E4DD023DFE01E4",
                    INIT_21 => X"01E401E4DD023DFE01E401E4DD025D01DD025D0201E45000DD025D0201E401E4",
                    INIT_22 => X"01E401E4DD025D0201E4DD023DFE01E401E4DD025D0101E4DD023DFD500001E4",
                    INIT_23 => X"3DFE01E401E4DD025D0101E4DD025D02223ADD023DFDA238440601E415805000",
                    INIT_24 => X"01E4DD023DFE01E401E4DD025D01950201E4DD025D0201E42232A244450EDD02",
                    INIT_25 => X"01E401E4DD025D0101E44400D602960201E4158014005D025000D50201E401E4",
                    INIT_26 => X"A27E0231040001E830FE70016330622061106000500001E4E257450EDD023DFE",
                    INIT_27 => X"2286100150007000400610000207A28402310430A28202310420A28002310410",
                    INIT_28 => X"7001622061106000500070004006108002070053228610042286100322861002",
                    INIT_29 => X"50007000400610000207A2A402310420A2A202310410A2A00231040001E830FE",
                    INIT_2A => X"30FE70016110600050007000400610800207005322A6100322A6100222A61001",
                    INIT_2B => X"0254022202400254A2CD02310400500101F4A28002310410A2C90231040001E8",
                    INIT_2C => X"005322CF100322CF100222CF1001500070006330622040061000020702150340",
                    INIT_2D => X"1100112E1172116F1172117211651120116B1163114150007000400610800207",
                    INIT_2E => X"D503A550152A1434A1401430A040142C607CD003B0235000005900641AD51B02",
                    INIT_2F => X"142C607CD002B0235000A2E0028C5000A2E00266A2F9D503A2401401A340A2F3",
                    INIT_30 => X"01DA1001500001DA1000500000590053003000530020A2E002ACA1401430A040",
                    INIT_31 => X"1172116111201164116111425000030B030E03116D0010F3500001DA10025000",
                    INIT_32 => X"6328D001B0235000005900641A1A1B03110011731174116E1165116D11751167",
                    INIT_33 => X"400640064006130F5000E3379E01E3379F011FFF1EFF5000D003301FA040142C",
                    INIT_34 => X"D528A7B0A6A0A590848000B803350B700A600950084000B812FF105230304006",
                    INIT_35 => X"084000B812FF002030304006400640064006130F5000E34493011201A3536352",
                    INIT_36 => X"E35E93011201636EA7B0636EA6A0636EA590636E848000B803350B700A600950",
                    INIT_37 => X"00D01257D0011020D001103A0059005300C0D0011050D00110200D001C005000",
                    INIT_38 => X"00D01258D0011032D001102C639BD20F035600D01256D0011031639BD20F0356",
                    INIT_39 => X"00D023B3DC061C01005923CCD20F033C00D0D0011033D001102C639BD20F0356",
                    INIT_3A => X"400640064006400600D0440644064406440604C000B810504006400640064006",
                    INIT_3B => X"10200002005300D0D00110430002D00110440002D00110540002237300C41050",
                    INIT_3C => X"D0011034D001102C50000059D00110520002D00110520002D00110450002D001",
                    INIT_3D => X"0002D00110200002005300D0D00110430002D00110440002D001105400020059",
                    INIT_3E => X"23F2D40200B810400371100023ECD40100B8104050000059005300C0D0011040",
                    INIT_3F => X"1F8050000371100323FED40800B810400371100223F8D40400B8104003711001",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"AAAAAA801041034A88888A2834A2AA803022AAA434AAB44DAA88A2AA82A2AAAA",
                   INITP_02 => X"2D0AA8A0A2AA88A298B420AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAAA",
                   INITP_03 => X"A8A8A8A2AA20B4B814DA9696B760A9AA8D45522AA882D0A6AA122D0829AA848A",
                   INITP_04 => X"8B4B8E38E2355AD8A890808AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA2",
                   INITP_05 => X"34BAED134000D2A0AAAAAAD2A222D528A2E0B8E235B4A888B4B8E388D5B4A888",
                   INITP_06 => X"088A2202D777768021154B5F5568021154B74280D2A0AAAAAAAAA4A28A2A2380",
                   INITP_07 => X"28C88C88C88C8A88A288A28A88A8A28A288A28A95515495535B62236088D8236")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900029180081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"0041003013005000F023F02010005000006001100335B001B031031400895000",
                    INIT_09 => X"D1010002D20100020041A030D00110200002D001103A0002D1010002D2010002",
                    INIT_0A => X"B227A0B5D004B023015450000059208E0059609AD00330031301003020A9D3FF",
                    INIT_0B => X"940520BCD010900331FFD1031108D004500000815000015408EC089D0210B12B",
                    INIT_0C => X"102C607CD001B0235000D10311F4D708D607D506D405D0045000970896079506",
                    INIT_0D => X"00530040005300500053006000530070D00110780002D0011030000200B8A000",
                    INIT_0E => X"A10010010610A10010010510A10010010410A1001030607CD002B02350000059",
                    INIT_0F => X"11721165116B116311611172115411201132112D1167500000C4A000102C0710",
                    INIT_10 => X"11001120113A11561120116411721161116F1142116311691167116F114C1120",
                    INIT_11 => X"0050D001102E000200530060D001102E00020053007000B8100100641AF51B00",
                    INIT_12 => X"1120113A1172116511661166117511425000005900530040D001102E00020053",
                    INIT_13 => X"116F115711001120113A1174116E1175116F1163112011641172116F11571100",
                    INIT_14 => X"116F115711001120112011201120113A11731165117A11691173112011641172",
                    INIT_15 => X"21591101D001A0100002E160C120B220110000641A281B011100112011641172",
                    INIT_16 => X"0059D00110290002D1010002D201000200410020D00110280002D00110200002",
                    INIT_17 => X"10200002E18EC3401300B423D1010002D20100020041B023000200641A311B01",
                    INIT_18 => X"1B010059217C1301D1010002D20100020041A01001301124D00110400002D001",
                    INIT_19 => X"0002D20100020041A01001301128D00110200002E1A3C3401300B42300641A3E",
                    INIT_1A => X"D20100020041003000641A4E1B010059E1C6C3401300B423005921941301D101",
                    INIT_1B => X"0041A060A1A6C65016030650152C4506450613010530D00110200002D1010002",
                    INIT_1C => X"000221D8D1FF21D4D1004BA01A121B075000005921BC9601D1010002D2010002",
                    INIT_1D => X"3DF0400740073003700160005000005921CA3B001A03005921CA3B001A01D101",
                    INIT_1E => X"01E401E401E4DD023DFDDD025D025D015000E1E59E011E3E50007000DD024D00",
                    INIT_1F => X"01E401E4DD023DFD01E401E4DD025D0101E401E4DD025D025000DD023DFE01E4",
                    INIT_20 => X"01E401E4DD025D0101E401E401E4DD023DFD500001E401E401E4DD023DFE01E4",
                    INIT_21 => X"01E401E4DD023DFE01E401E4DD025D01DD025D0201E45000DD025D0201E401E4",
                    INIT_22 => X"01E401E4DD025D0201E4DD023DFE01E401E4DD025D0101E4DD023DFD500001E4",
                    INIT_23 => X"3DFE01E401E4DD025D0101E4DD025D02223ADD023DFDA238440601E415805000",
                    INIT_24 => X"01E4DD023DFE01E401E4DD025D01950201E4DD025D0201E42232A244450EDD02",
                    INIT_25 => X"01E401E4DD025D0101E44400D602960201E4158014005D025000D50201E401E4",
                    INIT_26 => X"A27E0231040001E830FE70016330622061106000500001E4E257450EDD023DFE",
                    INIT_27 => X"2286100150007000400610000207A28402310430A28202310420A28002310410",
                    INIT_28 => X"7001622061106000500070004006108002070053228610042286100322861002",
                    INIT_29 => X"50007000400610000207A2A402310420A2A202310410A2A00231040001E830FE",
                    INIT_2A => X"30FE70016110600050007000400610800207005322A6100322A6100222A61001",
                    INIT_2B => X"0254022202400254A2CD02310400500101F4A28002310410A2C90231040001E8",
                    INIT_2C => X"005322CF100322CF100222CF1001500070006330622040061000020702150340",
                    INIT_2D => X"1100112E1172116F1172117211651120116B1163114150007000400610800207",
                    INIT_2E => X"D503A550152A1434A1401430A040142C607CD003B0235000005900641AD51B02",
                    INIT_2F => X"142C607CD002B0235000A2E0028C5000A2E00266A2F9D503A2401401A340A2F3",
                    INIT_30 => X"01DA1001500001DA1000500000590053003000530020A2E002ACA1401430A040",
                    INIT_31 => X"1172116111201164116111425000030B030E03116D0010F3500001DA10025000",
                    INIT_32 => X"6328D001B0235000005900641A1A1B03110011731174116E1165116D11751167",
                    INIT_33 => X"400640064006130F5000E3379E01E3379F011FFF1EFF5000D003301FA040142C",
                    INIT_34 => X"D528A7B0A6A0A590848000B803350B700A600950084000B812FF105230304006",
                    INIT_35 => X"084000B812FF002030304006400640064006130F5000E34493011201A3536352",
                    INIT_36 => X"E35E93011201636EA7B0636EA6A0636EA590636E848000B803350B700A600950",
                    INIT_37 => X"00D01257D0011020D001103A0059005300C0D0011050D00110200D001C005000",
                    INIT_38 => X"00D01258D0011032D001102C639BD20F035600D01256D0011031639BD20F0356",
                    INIT_39 => X"00D023B3DC061C01005923CCD20F033C00D0D0011033D001102C639BD20F0356",
                    INIT_3A => X"400640064006400600D0440644064406440604C000B810504006400640064006",
                    INIT_3B => X"10200002005300D0D00110430002D00110440002D00110540002237300C41050",
                    INIT_3C => X"D0011034D001102C50000059D00110520002D00110520002D00110450002D001",
                    INIT_3D => X"0002D00110200002005300D0D00110430002D00110440002D001105400020059",
                    INIT_3E => X"23F2D40200B810400371100023ECD40100B8104050000059005300C0D0011040",
                    INIT_3F => X"1F8050000371100323FED40800B810400371100223F8D40400B8104003711001",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"AAAAAA801041034A88888A2834A2AA803022AAA434AAB44DAA88A2AA82A2AAAA",
                   INITP_02 => X"2D0AA8A0A2AA88A298B420AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAAA",
                   INITP_03 => X"A8A8A8A2AA20B4B814DA9696B760A9AA8D45522AA882D0A6AA122D0829AA848A",
                   INITP_04 => X"8B4B8E38E2355AD8A890808AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA2",
                   INITP_05 => X"34BAED134000D2A0AAAAAAD2A222D528A2E0B8E235B4A888B4B8E388D5B4A888",
                   INITP_06 => X"088A2202D777768021154B5F5568021154B74280D2A0AAAAAAAAA4A28A2A2380",
                   INITP_07 => X"28C88C88C88C8A88A288A28A88A8A28A288A28A95515495535B62236088D8236")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate akv7;
    --
  end generate ram_1k_generate;
  --
  --
  --
  ram_2k_generate : if (C_RAM_SIZE_KWORDS = 2) generate
    --
    --
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004001881",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"010201024130012002013A020102010241300000232000006010350131148900",
                    INIT_05 => X"05BC1003FF03080400810054EC9D102B27B504235400598E599A03030130A9FF",
                    INIT_06 => X"5340535053605370017802013002B8002C7C01230003F4080706050400080706",
                    INIT_07 => X"72656B6361725420322D6700C4002C1000011000011000011000307C02230059",
                    INIT_08 => X"50012E025360012E025370B80164F50000203A56206472616F426369676F4C20",
                    INIT_09 => X"6F5700203A746E756F632064726F5700203A72656666754200595340012E0253",
                    INIT_0A => X"590101100260202000642801002064726F5700202020203A73657A6973206472",
                    INIT_0B => X"20028E4000230102010241230264310159012902010201024120012802012002",
                    INIT_0C => X"02010241103028012002A3400023643E01597C01010201024110302401400201",
                    INIT_0D => X"4160A65003502C06060130012002010201024130644E0159C640002359940101",
                    INIT_0E => X"F007070301000059CA000359CA00010102D8FFD400A012070059BC0101020102",
                    INIT_0F => X"E4E402FDE4E40201E4E402020002FEE4E4E4E402FD02020100E5013E00000200",
                    INIT_10 => X"E4E402FEE4E402010202E4000202E4E4E4E40201E4E4E402FD00E4E4E402FEE4",
                    INIT_11 => X"FEE4E40201E402023A02FD3806E48000E4E40202E402FEE4E40201E402FD00E4",
                    INIT_12 => X"E4E40201E4000202E48000020002E4E4E402FEE4E4020102E40202E432440E02",
                    INIT_13 => X"860100000600078431308231208031107E3100E8FE013020100000E4570E02FE",
                    INIT_14 => X"0000060007A43120A23110A03100E8FE01201000000006800753860486038602",
                    INIT_15 => X"54224054CD310001F4803110C93100E8FE011000000006800753A603A602A601",
                    INIT_16 => X"002E726F727265206B6341000006800753CF03CF02CF01000030200600071540",
                    INIT_17 => X"2C7C022300E08C00E066F903400140F303502A344030402C7C0323005964D502",
                    INIT_18 => X"726120646142000B0E1100F300DA0200DA0100DA00005953305320E0AC403040",
                    INIT_19 => X"0606060F0037013701FFFF00031F402C2801230059641A030073746E656D7567",
                    INIT_1A => X"40B8FF2030060606060F00440101535228B0A09080B83570605040B8FF523006",
                    INIT_1B => X"D0570120013A5953C0015001200000005E01016EB06EA06E906E80B835706050",
                    INIT_1C => X"D0B3060159CC0F3CD00133012C9B0F56D0580132012C9B0F56D05601319B0F56",
                    INIT_1D => X"200253D001430201440201540273C45006060606D006060606C0B85006060606",
                    INIT_1E => X"0201200253D0014302014402015402590134012C005901520201520201450201",
                    INIT_1F => X"80007103FE08B8407102F804B8407101F202B8407100EC01B840005953C00140",
                    INIT_20 => X"00060009100A132000011A0209FFFF000920030A0901090009000401FF000309",
                    INIT_21 => X"45333F323F31000E0050403E11003C11003A1109100F060606060607FF000E01",
                    INIT_22 => X"06060606FF000E015953070201023A01025359010201023A0102520102520102",
                    INIT_23 => X"070201023A01025359010252010252010245000E006E110920093009100F0606",
                    INIT_24 => X"0E00A11097200B00000B2080009EA1018B020B00000B0208009301000E015953",
                    INIT_25 => X"0EFE0080C20100B6200B0B20C0B0200B00AC020B0B020CA6020BB001000E0100",
                    INIT_26 => X"020BEC0100DBD50E08DC018000DBCD0E08DC008000D30100C8C30EFE0180C8BD",
                    INIT_27 => X"0F01000E0E0E0E0EF3200B0B0B0B402000EC200B000EE5020B0B0B0B040200DE",
                    INIT_28 => X"200B0B0B0B002006060606000E000F200B0009020B0B0B0B0002000E0000020B",
                    INIT_29 => X"0059533053403F231030102C7C0223005953110009102C7C012300595307001C",
                    INIT_2A => X"4D055900646E756F6620656369766564206F4E005B1030102C100110347C0323",
                    INIT_2B => X"5502014F020146020059014502014E02014F02014E027885102C7C0123005964",
                    INIT_2C => X"10C93010C93010C93010C930BA3330BE8530102C7C01230059014402014E0201",
                    INIT_2D => X"4502005953405350536053705380539053A053B010C93010C93010C93010C930",
                    INIT_2E => X"2300595310C930102C7C012300BA40301030102C7C0223005901520201520201",
                    INIT_2F => X"8530102C7C08102A7C0810297C03230059A430BA4430BACC305D8530102C7C01",
                    INIT_30 => X"BA304001BA304001BA403030BA304001BA304001BA304001BA403034BA55305D",
                    INIT_31 => X"5310C93010C930BABE30BACC305D8530102C7C01230059A430BA4430BA304001",
                    INIT_32 => X"01BA304001BA403034BA55305D8530102C7C08102A7C0810297C032300595340",
                    INIT_33 => X"10C93010C930BABE30BA304001BA304001BA304001BA403030BA304001BA3040",
                    INIT_34 => X"00A400C20300DC2000DC2000BAF02000015D8520F600102C7C01230059534053",
                    INIT_35 => X"59C30040944101DF60FE2060B43000B430B40000CD00B130B401A830B4000E0E",
                    INIT_36 => X"01000ED80E01DD00B0070130A00E0E0E013040008CA420B0A008593020CCCC08",
                    INIT_37 => X"0100010001000100400000B010D0EC010607F200FEB0070130A00E0E0E013040",
                    INIT_38 => X"6D656DAB000067726169746C756D00590B010153C00707400000010001000100",
                    INIT_39 => X"685401007379731001006E6F6973726576B6000074657365728D0000706D7564",
                    INIT_3A => X"FC020064725F6765725F633269E5020072775F6765725F633269C80100706C65",
                    INIT_3B => X"5F6332691103006361645F6C65735F6332690B03006364745F6C65735F633269",
                    INIT_3C => X"64725F6765725F716164E2000072775F6765725F7161640E030062645F6C6573",
                    INIT_3D => X"5F63647426050065746972775F6364742205007375746174735F636474CC0000",
                    INIT_3E => X"050064725F6765725F63647440050072775F6765725F6364742D050064616572",
                    INIT_3F => X"65748506007364695F7465675F706D65746305006863726165735F706D657431",
                   INITP_00 => X"FFE19640000007450E110809800820641FFFE536008278000000008F80000002",
                   INITP_01 => X"FFF7FF4380CD7D2823E202E70638B8303A010800C69FFFFFFFFFFF000001FFFF",
                   INITP_02 => X"0012E801FFE00041008120000002200000001297F8D7FFF3FF66FFFDFFEFFFBF",
                   INITP_03 => X"A8A28A000000000000040000450502850004DDCD005BD34015C081FFFDE49004",
                   INITP_04 => X"8000000000888800000000000000000C2D5B40040B0B55B6D4208408185AC619",
                   INITP_05 => X"3110094C018270000000082082053000000002405FFFE6E802D00201000A2014",
                   INITP_06 => X"5540C841A8882151369001132C94C50001111211122988800094C02222422245",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC455")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_l(31 downto 0),
                  DOPA => data_out_a_l(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_l(31 downto 0),
                  DOPB => data_out_b_l(35 downto 32), 
                   DIB => data_in_b_l(31 downto 0),
                  DIPB => data_in_b_l(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_h: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"6800690000506808006808006800690000000928787808280000015858010028",
                    INIT_05 => X"4A90684818680868280028000404815859D0E8580028001000B0E818890090E9",
                    INIT_06 => X"0000000000000000680800680800005008B0E8582868086B6B6A6A68284B4B4A",
                    INIT_07 => X"080808080808080808080828005008035088035088025088025008B0E8582800",
                    INIT_08 => X"00680800000068080000000008000D0D08080808080808080808080808080808",
                    INIT_09 => X"0808080808080808080808080808080808080808080808082800000068080000",
                    INIT_0A => X"1088685000F0E05908000D0D0808080808080808080808080808080808080808",
                    INIT_0B => X"0800F0E1095A68006900005800000D0D00680800680069000000680800680800",
                    INIT_0C => X"00690000508008680800F0E1095A000D0D001089680069000050800868080068",
                    INIT_0D => X"0050D0E38B038AA2A28902680800680069000000000D0D00F0E1095A00108968",
                    INIT_0E => X"1EA0A018B8B02800109D8D00109D8D680090E890E8250D0D280010CB68006900",
                    INIT_0F => X"00006E1E00006E2E00006E2E286E1E000000006E1E6E2E2E28F0CF0F28B86E26",
                    INIT_10 => X"00006E1E00006E2E6E2E00286E2E000000006E2E0000006E1E280000006E1E00",
                    INIT_11 => X"1E00006E2E006E2E116E1ED1A2000A2800006E2E006E1E00006E2E006E1E2800",
                    INIT_12 => X"00006E2E00A26B4B000A0A2E286A0000006E1E00006E2E4A006E2E0011D1A26E",
                    INIT_13 => X"110828B8A00801D10102D10102D10102D101020018B8B1B1B0B02800F1A26E1E",
                    INIT_14 => X"28B8A00801D10102D10102D101020018B8B1B0B028B8A0080100110811081108",
                    INIT_15 => X"01010101D101022800D10102D101020018B8B0B028B8A0080100110811081108",
                    INIT_16 => X"080808080808080808080828B8A008010011081108110828B8B1B1A008010101",
                    INIT_17 => X"0AB0E85828D10128D101D1EA518A51D1EA520A0A500A500AB0E8582800000D0D",
                    INIT_18 => X"08080808080828010101B608280008280008280008280000000000D101500A50",
                    INIT_19 => X"A0A0A00928F1CFF1CF0F0F286818500AB1E8582800000D0D0808080808080808",
                    INIT_1A => X"0400098018A0A0A0A00928F1C989D1B1EAD3D3D2C200010505040400098818A0",
                    INIT_1B => X"00096808680800000068086808060E28F1C989B1D3B1D3B1D2B1C20001050504",
                    INIT_1C => X"0091EE8E0091E9010068086808B1E901000968086808B1E90100096808B1E901",
                    INIT_1D => X"08000000680800680800680800110088A0A0A0A000A2A2A2A2020088A0A0A0A0",
                    INIT_1E => X"0068080000006808006808006808000068086808280068080068080068080068",
                    INIT_1F => X"0F280108916A00080108916A00080108916A00080108916A0008280000006808",
                    INIT_20 => X"28A1096808481292DFCFB218480F0F2868080268921848284828F2CF0F28026F",
                    INIT_21 => X"08091209120928A0080201D20202D20202D202022018A0A0A0A0A0A00128A109",
                    INIT_22 => X"A0A0A0A00128A109000002006800086800080069006800086800086800086800",
                    INIT_23 => X"02006800086800080068000868000868000828A008D20202000200022018A0A0",
                    INIT_24 => X"A0089268926848080868282808129268926848080868282808D26828A1090000",
                    INIT_25 => X"A1020809D2682892684868280892684828926848682808926848D26828A00828",
                    INIT_26 => X"6848D2682812F2A1A00208090812F2A1A002080908D2682812F2A102080912F2",
                    INIT_27 => X"D26828A0A0A0A0A092684848486828280892684828A092684848486828280892",
                    INIT_28 => X"68484848682828A0A0A0A0A0A008926848289268484848682828A0A008926848",
                    INIT_29 => X"280000000000D20250085008B0E8582800000228025008B0E858280000022892",
                    INIT_2A => X"0D0D000808080808080808080808080808080828025008500851885108B0E858",
                    INIT_2B => X"08006808006808002800680800680800680800680800D2025008B0E858280000",
                    INIT_2C => X"030200030200020200020200020800F202005108B0E858280068080068080068",
                    INIT_2D => X"0800280000000000000000000000000000000000050200050200040200040200",
                    INIT_2E => X"582800000002005108B0E8582802000052085108B0E858280068080068080068",
                    INIT_2F => X"02005108B0E85008B0E85008B0E85828000200020800020800F202005108B0E8",
                    INIT_30 => X"0200508A0200508A0250000A0200508A0200508A0200508A0250000A020800F2",
                    INIT_31 => X"00000200020200020800020800F202005108B0E858280002000208000200508A",
                    INIT_32 => X"8A0200508A0250000A020800F202005108B0E85008B0E85008B0E85828000000",
                    INIT_33 => X"0002000202000208000200508A0200508A0200508A0250000A0200508A020050",
                    INIT_34 => X"0B93E893E8A00200A00200080208000A09F20200030B5108B0E8582800000000",
                    INIT_35 => X"00B3EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3A0A0",
                    INIT_36 => X"1828A513A5CD93ED551DCD0585A5A5A5CD050D28130200010103000505131303",
                    INIT_37 => X"887088708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D",
                    INIT_38 => X"08080808080808080808080808082800F3CECE00500E8E0E2870887088708870",
                    INIT_39 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                   INITP_00 => X"FFF80013AAB64DF845FC4FC2FADF9DFFCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"EEEDF4CE0B99D4EFA017E98DF1626F8B63ECDFADAC4FFFFFFFFFFFEB5AD4FFFF",
                   INITP_02 => X"4FE1009CFFF9D586DCED4CEACEDA8CEAB3B6D43AE88BDCDD76D5EDDBEEBBEF7D",
                   INITP_03 => X"6AAAAABADADBAEDB6B6E00204D552A952B519558403306100D189CFFFFCDB758",
                   INITP_04 => X"A0844A112E4645D34B24C92928468453F6EDA7500CFDBEDB2A1B700C9321B967",
                   INITP_05 => X"8889D262744C09DB7AAAA492492989DB6DEDB7273FFFF804EB09F93F08026104",
                   INITP_06 => X"5532200052001C67C9494AA85248689D4A444444444C444EA526274888888889",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF90D5")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_h(31 downto 0),
                  DOPA => data_out_a_h(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_h(31 downto 0),
                  DOPB => data_out_b_h(35 downto 32), 
                   DIB => data_in_b_h(31 downto 0),
                  DIPB => data_in_b_h(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900029180081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"0041003013005000F023F02010005000006001100335B001B031031400895000",
                    INIT_09 => X"D1010002D20100020041A030D00110200002D001103A0002D1010002D2010002",
                    INIT_0A => X"B227A0B5D004B023015450000059208E0059609AD00330031301003020A9D3FF",
                    INIT_0B => X"940520BCD010900331FFD1031108D004500000815000015408EC089D0210B12B",
                    INIT_0C => X"102C607CD001B0235000D10311F4D708D607D506D405D0045000970896079506",
                    INIT_0D => X"00530040005300500053006000530070D00110780002D0011030000200B8A000",
                    INIT_0E => X"A10010010610A10010010510A10010010410A1001030607CD002B02350000059",
                    INIT_0F => X"11721165116B116311611172115411201132112D1167500000C4A000102C0710",
                    INIT_10 => X"11001120113A11561120116411721161116F1142116311691167116F114C1120",
                    INIT_11 => X"0050D001102E000200530060D001102E00020053007000B8100100641AF51B00",
                    INIT_12 => X"1120113A1172116511661166117511425000005900530040D001102E00020053",
                    INIT_13 => X"116F115711001120113A1174116E1175116F1163112011641172116F11571100",
                    INIT_14 => X"116F115711001120112011201120113A11731165117A11691173112011641172",
                    INIT_15 => X"21591101D001A0100002E160C120B220110000641A281B011100112011641172",
                    INIT_16 => X"0059D00110290002D1010002D201000200410020D00110280002D00110200002",
                    INIT_17 => X"10200002E18EC3401300B423D1010002D20100020041B023000200641A311B01",
                    INIT_18 => X"1B010059217C1301D1010002D20100020041A01001301124D00110400002D001",
                    INIT_19 => X"0002D20100020041A01001301128D00110200002E1A3C3401300B42300641A3E",
                    INIT_1A => X"D20100020041003000641A4E1B010059E1C6C3401300B423005921941301D101",
                    INIT_1B => X"0041A060A1A6C65016030650152C4506450613010530D00110200002D1010002",
                    INIT_1C => X"000221D8D1FF21D4D1004BA01A121B075000005921BC9601D1010002D2010002",
                    INIT_1D => X"3DF0400740073003700160005000005921CA3B001A03005921CA3B001A01D101",
                    INIT_1E => X"01E401E401E4DD023DFDDD025D025D015000E1E59E011E3E50007000DD024D00",
                    INIT_1F => X"01E401E4DD023DFD01E401E4DD025D0101E401E4DD025D025000DD023DFE01E4",
                    INIT_20 => X"01E401E4DD025D0101E401E401E4DD023DFD500001E401E401E4DD023DFE01E4",
                    INIT_21 => X"01E401E4DD023DFE01E401E4DD025D01DD025D0201E45000DD025D0201E401E4",
                    INIT_22 => X"01E401E4DD025D0201E4DD023DFE01E401E4DD025D0101E4DD023DFD500001E4",
                    INIT_23 => X"3DFE01E401E4DD025D0101E4DD025D02223ADD023DFDA238440601E415805000",
                    INIT_24 => X"01E4DD023DFE01E401E4DD025D01950201E4DD025D0201E42232A244450EDD02",
                    INIT_25 => X"01E401E4DD025D0101E44400D602960201E4158014005D025000D50201E401E4",
                    INIT_26 => X"A27E0231040001E830FE70016330622061106000500001E4E257450EDD023DFE",
                    INIT_27 => X"2286100150007000400610000207A28402310430A28202310420A28002310410",
                    INIT_28 => X"7001622061106000500070004006108002070053228610042286100322861002",
                    INIT_29 => X"50007000400610000207A2A402310420A2A202310410A2A00231040001E830FE",
                    INIT_2A => X"30FE70016110600050007000400610800207005322A6100322A6100222A61001",
                    INIT_2B => X"0254022202400254A2CD02310400500101F4A28002310410A2C90231040001E8",
                    INIT_2C => X"005322CF100322CF100222CF1001500070006330622040061000020702150340",
                    INIT_2D => X"1100112E1172116F1172117211651120116B1163114150007000400610800207",
                    INIT_2E => X"D503A550152A1434A1401430A040142C607CD003B0235000005900641AD51B02",
                    INIT_2F => X"142C607CD002B0235000A2E0028C5000A2E00266A2F9D503A2401401A340A2F3",
                    INIT_30 => X"01DA1001500001DA1000500000590053003000530020A2E002ACA1401430A040",
                    INIT_31 => X"1172116111201164116111425000030B030E03116D0010F3500001DA10025000",
                    INIT_32 => X"6328D001B0235000005900641A1A1B03110011731174116E1165116D11751167",
                    INIT_33 => X"400640064006130F5000E3379E01E3379F011FFF1EFF5000D003301FA040142C",
                    INIT_34 => X"D528A7B0A6A0A590848000B803350B700A600950084000B812FF105230304006",
                    INIT_35 => X"084000B812FF002030304006400640064006130F5000E34493011201A3536352",
                    INIT_36 => X"E35E93011201636EA7B0636EA6A0636EA590636E848000B803350B700A600950",
                    INIT_37 => X"00D01257D0011020D001103A0059005300C0D0011050D00110200D001C005000",
                    INIT_38 => X"00D01258D0011032D001102C639BD20F035600D01256D0011031639BD20F0356",
                    INIT_39 => X"00D023B3DC061C01005923CCD20F033C00D0D0011033D001102C639BD20F0356",
                    INIT_3A => X"400640064006400600D0440644064406440604C000B810504006400640064006",
                    INIT_3B => X"10200002005300D0D00110430002D00110440002D00110540002237300C41050",
                    INIT_3C => X"D0011034D001102C50000059D00110520002D00110520002D00110450002D001",
                    INIT_3D => X"0002D00110200002005300D0D00110430002D00110440002D001105400020059",
                    INIT_3E => X"23F2D40200B810400371100023ECD40100B8104050000059005300C0D0011040",
                    INIT_3F => X"1F8050000371100323FED40800B810400371100223F8D40400B8104003711001",
                    INIT_40 => X"D10911200403D00A240931019109500090095000E4049F011FFF50000403DF09",
                    INIT_41 => X"500042061200D1091110900A24132420BE009F01641A310291091EFF1FFF5000",
                    INIT_42 => X"0500A43A041104094010310F40064006400640064006400703FF5000420E1201",
                    INIT_43 => X"11451333243F1332243F13315000400E100004500340A43E04110400A43C0411",
                    INIT_44 => X"000211530059D3010002D1010002113AD10100021152D10100021152D1010002",
                    INIT_45 => X"400640064006400603FF5000430E13010059005304070002D1010002113AD101",
                    INIT_46 => X"000211455000400E1000A46E0411040900200409003004094010310F40064006",
                    INIT_47 => X"04070002D1010002113AD101000211530059D10100021152D10100021152D101",
                    INIT_48 => X"248BD002900B10001000D00B500250081000A493D0015000430E130100590053",
                    INIT_49 => X"400E100024A1D0102497D020900B10001000D00B502050801000249E24A1D001",
                    INIT_4A => X"500024ACD002900BD00B5002100C24A6D002900BA4B0D0015000400E10015000",
                    INIT_4B => X"420E04FE10001280A4C2D001500024B6D020900BD00B502010C024B0D020900B",
                    INIT_4C => X"410804DC100012801100A4D3D001500024C8E4C3420E04FE1001128024C8E4BD",
                    INIT_4D => X"D002900BA4ECD001500024DBE4D5420E410804DC10011280110024DBE4CD420E",
                    INIT_4E => X"100024ECD020900B5000400E24E5D002900B900B900BD00B50045002100024DE",
                    INIT_4F => X"A50FD0015000400E400E400E400E400E24F3D020900B900B900BD00B50405020",
                    INIT_50 => X"900B50002509D002900B900B900BD00B500050024000410E10002500D002900B",
                    INIT_51 => X"D020900B900B900BD00B5000502040064006400640064000410E1000250FD020",
                    INIT_52 => X"00590053041150000409A010112C607CD001B02350000059005304075000251C",
                    INIT_53 => X"500000590053003000530040A53F0423A1101130A010112C607CD002B0235000",
                    INIT_54 => X"1120116F114E5000045BA1101130A010112CA3101101A2101134607CD003B023",
                    INIT_55 => X"1A4D1B05005911001164116E1175116F11661120116511631169117611651164",
                    INIT_56 => X"D001104F0002D001104E0002A5780485A010112C607CD001B023500000590064",
                    INIT_57 => X"10550002D001104F0002D0011046000250000059D00110450002D001104E0002",
                    INIT_58 => X"04850030A310112C607CD001B02350000059D00110440002D001104E0002D001",
                    INIT_59 => X"071004C90030061004C90030051004C90030041004C9003004BA11330030E5BE",
                    INIT_5A => X"005300A0005300B00B1004C900300A1004C90030091004C90030081004C90030",
                    INIT_5B => X"1045000250000059005300400053005000530060005300700053008000530090",
                    INIT_5C => X"A4101130A310112C607CD002B02350000059D00110520002D00110520002D001",
                    INIT_5D => X"B023500000590053001004C90030A310112C607CD001B023500004BA01400030",
                    INIT_5E => X"005904A4003004BA1144003004BA11CC0030E55D04850030A310112C607CD001",
                    INIT_5F => X"04850030A310112C607CD008A010112A607CD008A0101129607CD003B0235000",
                    INIT_60 => X"04BA0030A140140104BA0030A140140104BAA1400030143404BA11550030E55D",
                    INIT_61 => X"04BA0030A140140104BA0030A140140104BAA1400030143004BA0030A1401401",
                    INIT_62 => X"A310112C607CD001B0235000005904A4003004BA1144003004BA0030A1401401",
                    INIT_63 => X"0053001004C90030041004C9003004BA11BE003004BA11CC0030E55D04850030",
                    INIT_64 => X"112C607CD008A010112A607CD008A0101129607CD003B0235000005900530040",
                    INIT_65 => X"140104BA0030A140140104BAA1400030143404BA11550030E55D04850030A310",
                    INIT_66 => X"140104BA0030A140140104BAA1400030143004BA0030A140140104BA0030A140",
                    INIT_67 => X"001004C90030041004C9003004BA11BE003004BA0030A140140104BA0030A140",
                    INIT_68 => X"1301E55D0485002006F61700A210112C607CD001B02350000059005300400053",
                    INIT_69 => X"160026A4D10026C2D103410004DC0020410004DC0020110004BA11F000201400",
                    INIT_6A => X"043066B4D600460006CD1600A6B1C73026B4160166A8C73026B44600410E410E",
                    INIT_6B => X"005966C3D70007406694D341130106DF016004FE0020016026B40430160066B4",
                    INIT_6C => X"9A010A301B405000268C04A4002003B002A0070800590B300A2026CC26CC0708",
                    INIT_6D => X"310150004B0E26D84B0E9A0126DDDA00ABB03A079A010A300BA04A0E4A0E4A0E",
                    INIT_6E => X"41064D0726F2DA001DFEACB03A079A010A300BA04A0E4A0E4A0E9A010A301B40",
                    INIT_6F => X"1001E1001001E1001001E1001001E100104011005000ECB04C102CD026EC9A01",
                    INIT_70 => X"E70B9D019C010053A0C01D071C071C405000E1001001E1001001E1001001E100",
                    INIT_71 => X"116D1165116D11AB1100110011671172116111691174116C1175116D50000059",
                    INIT_72 => X"117611B61100110011741165117311651172118D110011001170116D11751164",
                    INIT_73 => X"1168115411011100117311791173111011011100116E116F1169117311721165",
                    INIT_74 => X"11721177115F116711651172115F11631132116911C8110111001170116C1165",
                    INIT_75 => X"11FC1102110011641172115F116711651172115F11631132116911E511021100",
                    INIT_76 => X"11321169110B11031100116311641174115F116C11651173115F116311321169",
                    INIT_77 => X"115F116311321169111111031100116311611164115F116C11651173115F1163",
                    INIT_78 => X"116711651172115F117111611164110E1103110011621164115F116C11651173",
                    INIT_79 => X"11641172115F116711651172115F11711161116411E21100110011721177115F",
                    INIT_7A => X"112211051100117311751174116111741173115F11631164117411CC11001100",
                    INIT_7B => X"115F11631164117411261105110011651174116911721177115F116311641174",
                    INIT_7C => X"1177115F116711651172115F116311641174112D110511001164116111651172",
                    INIT_7D => X"1105110011641172115F116711651172115F1163116411741140110511001172",
                    INIT_7E => X"1174116311051100116811631172116111651173115F1170116D116511741131",
                    INIT_7F => X"11651174118511061100117311641169115F117411651167115F1170116D1165",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"AAAAAA801041034A88888A2834A2AA803022AAA434AAB44DAA88A2AA82A2AAAA",
                   INITP_02 => X"2D0AA8A0A2AA88A298B420AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAAA",
                   INITP_03 => X"A8A8A8A2AA20B4B814DA9696B760A9AA8D45522AA882D0A6AA122D0829AA848A",
                   INITP_04 => X"8B4B8E38E2355AD8A890808AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA2",
                   INITP_05 => X"34BAED134000D2A0AAAAAAD2A222D528A2E0B8E235B4A888B4B8E388D5B4A888",
                   INITP_06 => X"088A2202D777768021154B5F5568021154B74280D2A0AAAAAAAAA4A28A2A2380",
                   INITP_07 => X"28C88C88C88C8A88A288A28A88A8A28A288A28A95515495535B62236088D8236",
                   INITP_08 => X"AA28A8A2893A220555A4AAA28AA8A28A0889038E3A0555A4920B5C028AC22D2A",
                   INITP_09 => X"C955C020309C02030CAD602D6032B60B60CB0830B0830C924CC0202CC020324A",
                   INITP_0A => X"28A2A8A28A2E0D2A0AAAAAAAAA800434A88E00D2AA834AAB0081554C2C020530",
                   INITP_0B => X"80D0D0D2A208380D2A2034A000D2A28A2A888888882082082082088380D2A28A",
                   INITP_0C => X"2088206060602060606020E0343434A8882208380D2A20818181808181818083",
                   INITP_0D => X"66660A095D011550265D045542A0282AB4D62083358D8D95375860803880D2A2",
                   INITP_0E => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD604A666",
                   INITP_0F => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900029180081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"0041003013005000F023F02010005000006001100335B001B031031400895000",
                    INIT_09 => X"D1010002D20100020041A030D00110200002D001103A0002D1010002D2010002",
                    INIT_0A => X"B227A0B5D004B023015450000059208E0059609AD00330031301003020A9D3FF",
                    INIT_0B => X"940520BCD010900331FFD1031108D004500000815000015408EC089D0210B12B",
                    INIT_0C => X"102C607CD001B0235000D10311F4D708D607D506D405D0045000970896079506",
                    INIT_0D => X"00530040005300500053006000530070D00110780002D0011030000200B8A000",
                    INIT_0E => X"A10010010610A10010010510A10010010410A1001030607CD002B02350000059",
                    INIT_0F => X"11721165116B116311611172115411201132112D1167500000C4A000102C0710",
                    INIT_10 => X"11001120113A11561120116411721161116F1142116311691167116F114C1120",
                    INIT_11 => X"0050D001102E000200530060D001102E00020053007000B8100100641AF51B00",
                    INIT_12 => X"1120113A1172116511661166117511425000005900530040D001102E00020053",
                    INIT_13 => X"116F115711001120113A1174116E1175116F1163112011641172116F11571100",
                    INIT_14 => X"116F115711001120112011201120113A11731165117A11691173112011641172",
                    INIT_15 => X"21591101D001A0100002E160C120B220110000641A281B011100112011641172",
                    INIT_16 => X"0059D00110290002D1010002D201000200410020D00110280002D00110200002",
                    INIT_17 => X"10200002E18EC3401300B423D1010002D20100020041B023000200641A311B01",
                    INIT_18 => X"1B010059217C1301D1010002D20100020041A01001301124D00110400002D001",
                    INIT_19 => X"0002D20100020041A01001301128D00110200002E1A3C3401300B42300641A3E",
                    INIT_1A => X"D20100020041003000641A4E1B010059E1C6C3401300B423005921941301D101",
                    INIT_1B => X"0041A060A1A6C65016030650152C4506450613010530D00110200002D1010002",
                    INIT_1C => X"000221D8D1FF21D4D1004BA01A121B075000005921BC9601D1010002D2010002",
                    INIT_1D => X"3DF0400740073003700160005000005921CA3B001A03005921CA3B001A01D101",
                    INIT_1E => X"01E401E401E4DD023DFDDD025D025D015000E1E59E011E3E50007000DD024D00",
                    INIT_1F => X"01E401E4DD023DFD01E401E4DD025D0101E401E4DD025D025000DD023DFE01E4",
                    INIT_20 => X"01E401E4DD025D0101E401E401E4DD023DFD500001E401E401E4DD023DFE01E4",
                    INIT_21 => X"01E401E4DD023DFE01E401E4DD025D01DD025D0201E45000DD025D0201E401E4",
                    INIT_22 => X"01E401E4DD025D0201E4DD023DFE01E401E4DD025D0101E4DD023DFD500001E4",
                    INIT_23 => X"3DFE01E401E4DD025D0101E4DD025D02223ADD023DFDA238440601E415805000",
                    INIT_24 => X"01E4DD023DFE01E401E4DD025D01950201E4DD025D0201E42232A244450EDD02",
                    INIT_25 => X"01E401E4DD025D0101E44400D602960201E4158014005D025000D50201E401E4",
                    INIT_26 => X"A27E0231040001E830FE70016330622061106000500001E4E257450EDD023DFE",
                    INIT_27 => X"2286100150007000400610000207A28402310430A28202310420A28002310410",
                    INIT_28 => X"7001622061106000500070004006108002070053228610042286100322861002",
                    INIT_29 => X"50007000400610000207A2A402310420A2A202310410A2A00231040001E830FE",
                    INIT_2A => X"30FE70016110600050007000400610800207005322A6100322A6100222A61001",
                    INIT_2B => X"0254022202400254A2CD02310400500101F4A28002310410A2C90231040001E8",
                    INIT_2C => X"005322CF100322CF100222CF1001500070006330622040061000020702150340",
                    INIT_2D => X"1100112E1172116F1172117211651120116B1163114150007000400610800207",
                    INIT_2E => X"D503A550152A1434A1401430A040142C607CD003B0235000005900641AD51B02",
                    INIT_2F => X"142C607CD002B0235000A2E0028C5000A2E00266A2F9D503A2401401A340A2F3",
                    INIT_30 => X"01DA1001500001DA1000500000590053003000530020A2E002ACA1401430A040",
                    INIT_31 => X"1172116111201164116111425000030B030E03116D0010F3500001DA10025000",
                    INIT_32 => X"6328D001B0235000005900641A1A1B03110011731174116E1165116D11751167",
                    INIT_33 => X"400640064006130F5000E3379E01E3379F011FFF1EFF5000D003301FA040142C",
                    INIT_34 => X"D528A7B0A6A0A590848000B803350B700A600950084000B812FF105230304006",
                    INIT_35 => X"084000B812FF002030304006400640064006130F5000E34493011201A3536352",
                    INIT_36 => X"E35E93011201636EA7B0636EA6A0636EA590636E848000B803350B700A600950",
                    INIT_37 => X"00D01257D0011020D001103A0059005300C0D0011050D00110200D001C005000",
                    INIT_38 => X"00D01258D0011032D001102C639BD20F035600D01256D0011031639BD20F0356",
                    INIT_39 => X"00D023B3DC061C01005923CCD20F033C00D0D0011033D001102C639BD20F0356",
                    INIT_3A => X"400640064006400600D0440644064406440604C000B810504006400640064006",
                    INIT_3B => X"10200002005300D0D00110430002D00110440002D00110540002237300C41050",
                    INIT_3C => X"D0011034D001102C50000059D00110520002D00110520002D00110450002D001",
                    INIT_3D => X"0002D00110200002005300D0D00110430002D00110440002D001105400020059",
                    INIT_3E => X"23F2D40200B810400371100023ECD40100B8104050000059005300C0D0011040",
                    INIT_3F => X"1F8050000371100323FED40800B810400371100223F8D40400B8104003711001",
                    INIT_40 => X"D10911200403D00A240931019109500090095000E4049F011FFF50000403DF09",
                    INIT_41 => X"500042061200D1091110900A24132420BE009F01641A310291091EFF1FFF5000",
                    INIT_42 => X"0500A43A041104094010310F40064006400640064006400703FF5000420E1201",
                    INIT_43 => X"11451333243F1332243F13315000400E100004500340A43E04110400A43C0411",
                    INIT_44 => X"000211530059D3010002D1010002113AD10100021152D10100021152D1010002",
                    INIT_45 => X"400640064006400603FF5000430E13010059005304070002D1010002113AD101",
                    INIT_46 => X"000211455000400E1000A46E0411040900200409003004094010310F40064006",
                    INIT_47 => X"04070002D1010002113AD101000211530059D10100021152D10100021152D101",
                    INIT_48 => X"248BD002900B10001000D00B500250081000A493D0015000430E130100590053",
                    INIT_49 => X"400E100024A1D0102497D020900B10001000D00B502050801000249E24A1D001",
                    INIT_4A => X"500024ACD002900BD00B5002100C24A6D002900BA4B0D0015000400E10015000",
                    INIT_4B => X"420E04FE10001280A4C2D001500024B6D020900BD00B502010C024B0D020900B",
                    INIT_4C => X"410804DC100012801100A4D3D001500024C8E4C3420E04FE1001128024C8E4BD",
                    INIT_4D => X"D002900BA4ECD001500024DBE4D5420E410804DC10011280110024DBE4CD420E",
                    INIT_4E => X"100024ECD020900B5000400E24E5D002900B900B900BD00B50045002100024DE",
                    INIT_4F => X"A50FD0015000400E400E400E400E400E24F3D020900B900B900BD00B50405020",
                    INIT_50 => X"900B50002509D002900B900B900BD00B500050024000410E10002500D002900B",
                    INIT_51 => X"D020900B900B900BD00B5000502040064006400640064000410E1000250FD020",
                    INIT_52 => X"00590053041150000409A010112C607CD001B02350000059005304075000251C",
                    INIT_53 => X"500000590053003000530040A53F0423A1101130A010112C607CD002B0235000",
                    INIT_54 => X"1120116F114E5000045BA1101130A010112CA3101101A2101134607CD003B023",
                    INIT_55 => X"1A4D1B05005911001164116E1175116F11661120116511631169117611651164",
                    INIT_56 => X"D001104F0002D001104E0002A5780485A010112C607CD001B023500000590064",
                    INIT_57 => X"10550002D001104F0002D0011046000250000059D00110450002D001104E0002",
                    INIT_58 => X"04850030A310112C607CD001B02350000059D00110440002D001104E0002D001",
                    INIT_59 => X"071004C90030061004C90030051004C90030041004C9003004BA11330030E5BE",
                    INIT_5A => X"005300A0005300B00B1004C900300A1004C90030091004C90030081004C90030",
                    INIT_5B => X"1045000250000059005300400053005000530060005300700053008000530090",
                    INIT_5C => X"A4101130A310112C607CD002B02350000059D00110520002D00110520002D001",
                    INIT_5D => X"B023500000590053001004C90030A310112C607CD001B023500004BA01400030",
                    INIT_5E => X"005904A4003004BA1144003004BA11CC0030E55D04850030A310112C607CD001",
                    INIT_5F => X"04850030A310112C607CD008A010112A607CD008A0101129607CD003B0235000",
                    INIT_60 => X"04BA0030A140140104BA0030A140140104BAA1400030143404BA11550030E55D",
                    INIT_61 => X"04BA0030A140140104BA0030A140140104BAA1400030143004BA0030A1401401",
                    INIT_62 => X"A310112C607CD001B0235000005904A4003004BA1144003004BA0030A1401401",
                    INIT_63 => X"0053001004C90030041004C9003004BA11BE003004BA11CC0030E55D04850030",
                    INIT_64 => X"112C607CD008A010112A607CD008A0101129607CD003B0235000005900530040",
                    INIT_65 => X"140104BA0030A140140104BAA1400030143404BA11550030E55D04850030A310",
                    INIT_66 => X"140104BA0030A140140104BAA1400030143004BA0030A140140104BA0030A140",
                    INIT_67 => X"001004C90030041004C9003004BA11BE003004BA0030A140140104BA0030A140",
                    INIT_68 => X"1301E55D0485002006F61700A210112C607CD001B02350000059005300400053",
                    INIT_69 => X"160026A4D10026C2D103410004DC0020410004DC0020110004BA11F000201400",
                    INIT_6A => X"043066B4D600460006CD1600A6B1C73026B4160166A8C73026B44600410E410E",
                    INIT_6B => X"005966C3D70007406694D341130106DF016004FE0020016026B40430160066B4",
                    INIT_6C => X"9A010A301B405000268C04A4002003B002A0070800590B300A2026CC26CC0708",
                    INIT_6D => X"310150004B0E26D84B0E9A0126DDDA00ABB03A079A010A300BA04A0E4A0E4A0E",
                    INIT_6E => X"41064D0726F2DA001DFEACB03A079A010A300BA04A0E4A0E4A0E9A010A301B40",
                    INIT_6F => X"1001E1001001E1001001E1001001E100104011005000ECB04C102CD026EC9A01",
                    INIT_70 => X"E70B9D019C010053A0C01D071C071C405000E1001001E1001001E1001001E100",
                    INIT_71 => X"116D1165116D11AB1100110011671172116111691174116C1175116D50000059",
                    INIT_72 => X"117611B61100110011741165117311651172118D110011001170116D11751164",
                    INIT_73 => X"1168115411011100117311791173111011011100116E116F1169117311721165",
                    INIT_74 => X"11721177115F116711651172115F11631132116911C8110111001170116C1165",
                    INIT_75 => X"11FC1102110011641172115F116711651172115F11631132116911E511021100",
                    INIT_76 => X"11321169110B11031100116311641174115F116C11651173115F116311321169",
                    INIT_77 => X"115F116311321169111111031100116311611164115F116C11651173115F1163",
                    INIT_78 => X"116711651172115F117111611164110E1103110011621164115F116C11651173",
                    INIT_79 => X"11641172115F116711651172115F11711161116411E21100110011721177115F",
                    INIT_7A => X"112211051100117311751174116111741173115F11631164117411CC11001100",
                    INIT_7B => X"115F11631164117411261105110011651174116911721177115F116311641174",
                    INIT_7C => X"1177115F116711651172115F116311641174112D110511001164116111651172",
                    INIT_7D => X"1105110011641172115F116711651172115F1163116411741140110511001172",
                    INIT_7E => X"1174116311051100116811631172116111651173115F1170116D116511741131",
                    INIT_7F => X"11651174118511061100117311641169115F117411651167115F1170116D1165",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"AAAAAA801041034A88888A2834A2AA803022AAA434AAB44DAA88A2AA82A2AAAA",
                   INITP_02 => X"2D0AA8A0A2AA88A298B420AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAAA",
                   INITP_03 => X"A8A8A8A2AA20B4B814DA9696B760A9AA8D45522AA882D0A6AA122D0829AA848A",
                   INITP_04 => X"8B4B8E38E2355AD8A890808AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA2",
                   INITP_05 => X"34BAED134000D2A0AAAAAAD2A222D528A2E0B8E235B4A888B4B8E388D5B4A888",
                   INITP_06 => X"088A2202D777768021154B5F5568021154B74280D2A0AAAAAAAAA4A28A2A2380",
                   INITP_07 => X"28C88C88C88C8A88A288A28A88A8A28A288A28A95515495535B62236088D8236",
                   INITP_08 => X"AA28A8A2893A220555A4AAA28AA8A28A0889038E3A0555A4920B5C028AC22D2A",
                   INITP_09 => X"C955C020309C02030CAD602D6032B60B60CB0830B0830C924CC0202CC020324A",
                   INITP_0A => X"28A2A8A28A2E0D2A0AAAAAAAAA800434A88E00D2AA834AAB0081554C2C020530",
                   INITP_0B => X"80D0D0D2A208380D2A2034A000D2A28A2A888888882082082082088380D2A28A",
                   INITP_0C => X"2088206060602060606020E0343434A8882208380D2A20818181808181818083",
                   INITP_0D => X"66660A095D011550265D045542A0282AB4D62083358D8D95375860803880D2A2",
                   INITP_0E => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD604A666",
                   INITP_0F => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_2k_generate;
  --
  --	
  ram_4k_generate : if (C_RAM_SIZE_KWORDS = 4) generate
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      data_in_a <= "000000000000000000000000000000000000";
      --
      s6_a11_flop: FD
      port map (  D => address(11),
                  Q => pipe_a11,
                  C => clk);
      --
      s6_4k_mux0_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(0),
                I1 => data_out_a_hl(0),
                I2 => data_out_a_ll(1),
                I3 => data_out_a_hl(1),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(0),
                O6 => instruction(1));
      --
      s6_4k_mux2_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(2),
                I1 => data_out_a_hl(2),
                I2 => data_out_a_ll(3),
                I3 => data_out_a_hl(3),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(2),
                O6 => instruction(3));
      --
      s6_4k_mux4_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(4),
                I1 => data_out_a_hl(4),
                I2 => data_out_a_ll(5),
                I3 => data_out_a_hl(5),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(4),
                O6 => instruction(5));
      --
      s6_4k_mux6_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(6),
                I1 => data_out_a_hl(6),
                I2 => data_out_a_ll(7),
                I3 => data_out_a_hl(7),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(6),
                O6 => instruction(7));
      --
      s6_4k_mux8_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(32),
                I1 => data_out_a_hl(32),
                I2 => data_out_a_lh(0),
                I3 => data_out_a_hh(0),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(8),
                O6 => instruction(9));
      --
      s6_4k_mux10_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(1),
                I1 => data_out_a_hh(1),
                I2 => data_out_a_lh(2),
                I3 => data_out_a_hh(2),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(10),
                O6 => instruction(11));
      --
      s6_4k_mux12_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(3),
                I1 => data_out_a_hh(3),
                I2 => data_out_a_lh(4),
                I3 => data_out_a_hh(4),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(12),
                O6 => instruction(13));
      --
      s6_4k_mux14_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(5),
                I1 => data_out_a_hh(5),
                I2 => data_out_a_lh(6),
                I3 => data_out_a_hh(6),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(14),
                O6 => instruction(15));
      --
      s6_4k_mux16_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(7),
                I1 => data_out_a_hh(7),
                I2 => data_out_a_lh(32),
                I3 => data_out_a_hh(32),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(16),
                O6 => instruction(17));
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_ll <= "000" & data_out_b_ll(32) & "000000000000000000000000" & data_out_b_ll(7 downto 0);
        data_in_b_lh <= "000" & data_out_b_lh(32) & "000000000000000000000000" & data_out_b_lh(7 downto 0);
        data_in_b_hl <= "000" & data_out_b_hl(32) & "000000000000000000000000" & data_out_b_hl(7 downto 0);
        data_in_b_hh <= "000" & data_out_b_hh(32) & "000000000000000000000000" & data_out_b_hh(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b_l(3 downto 0) <= "0000";
        we_b_h(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
        jtag_dout <= data_out_b_lh(32) & data_out_b_lh(7 downto 0) & data_out_b_ll(32) & data_out_b_ll(7 downto 0);
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_lh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_ll <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        data_in_b_hh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_hl <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        --
        s6_4k_jtag_we_lut: LUT6_2
        generic map (INIT => X"8000000020000000")
        port map( I0 => jtag_we,
                  I1 => jtag_addr(11),
                  I2 => '1',
                  I3 => '1',
                  I4 => '1',
                  I5 => '1',
                  O5 => jtag_we_l,
                  O6 => jtag_we_h);
        --
        we_b_l(3 downto 0) <= jtag_we_l & jtag_we_l & jtag_we_l & jtag_we_l;
        we_b_h(3 downto 0) <= jtag_we_h & jtag_we_h & jtag_we_h & jtag_we_h;
        --
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
        --
        s6_4k_jtag_mux0_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(0),
                  I1 => data_out_b_hl(0),
                  I2 => data_out_b_ll(1),
                  I3 => data_out_b_hl(1),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(0),
                  O6 => jtag_dout(1));
        --
        s6_4k_jtag_mux2_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(2),
                  I1 => data_out_b_hl(2),
                  I2 => data_out_b_ll(3),
                  I3 => data_out_b_hl(3),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(2),
                  O6 => jtag_dout(3));
        --
        s6_4k_jtag_mux4_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(4),
                  I1 => data_out_b_hl(4),
                  I2 => data_out_b_ll(5),
                  I3 => data_out_b_hl(5),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(4),
                  O6 => jtag_dout(5));
        --
        s6_4k_jtag_mux6_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(6),
                  I1 => data_out_b_hl(6),
                  I2 => data_out_b_ll(7),
                  I3 => data_out_b_hl(7),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(6),
                  O6 => jtag_dout(7));
        --
        s6_4k_jtag_mux8_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(32),
                  I1 => data_out_b_hl(32),
                  I2 => data_out_b_lh(0),
                  I3 => data_out_b_hh(0),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(8),
                  O6 => jtag_dout(9));
        --
        s6_4k_jtag_mux10_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(1),
                  I1 => data_out_b_hh(1),
                  I2 => data_out_b_lh(2),
                  I3 => data_out_b_hh(2),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(10),
                  O6 => jtag_dout(11));
        --
        s6_4k_jtag_mux12_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(3),
                  I1 => data_out_b_hh(3),
                  I2 => data_out_b_lh(4),
                  I3 => data_out_b_hh(4),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(12),
                  O6 => jtag_dout(13));
        --
        s6_4k_jtag_mux14_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(5),
                  I1 => data_out_b_hh(5),
                  I2 => data_out_b_lh(6),
                  I3 => data_out_b_hh(6),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(14),
                  O6 => jtag_dout(15));
        --
        s6_4k_jtag_mux16_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(7),
                  I1 => data_out_b_hh(7),
                  I2 => data_out_b_lh(32),
                  I3 => data_out_b_hh(32),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(16),
                  O6 => jtag_dout(17));
      --
      end generate loader;
      --
      kcpsm6_rom_ll: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004001881",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"010201024130012002013A020102010241300000232000006010350131148900",
                    INIT_05 => X"05BC1003FF03080400810054EC9D102B27B504235400598E599A03030130A9FF",
                    INIT_06 => X"5340535053605370017802013002B8002C7C01230003F4080706050400080706",
                    INIT_07 => X"72656B6361725420322D6700C4002C1000011000011000011000307C02230059",
                    INIT_08 => X"50012E025360012E025370B80164F50000203A56206472616F426369676F4C20",
                    INIT_09 => X"6F5700203A746E756F632064726F5700203A72656666754200595340012E0253",
                    INIT_0A => X"590101100260202000642801002064726F5700202020203A73657A6973206472",
                    INIT_0B => X"20028E4000230102010241230264310159012902010201024120012802012002",
                    INIT_0C => X"02010241103028012002A3400023643E01597C01010201024110302401400201",
                    INIT_0D => X"4160A65003502C06060130012002010201024130644E0159C640002359940101",
                    INIT_0E => X"F007070301000059CA000359CA00010102D8FFD400A012070059BC0101020102",
                    INIT_0F => X"E4E402FDE4E40201E4E402020002FEE4E4E4E402FD02020100E5013E00000200",
                    INIT_10 => X"E4E402FEE4E402010202E4000202E4E4E4E40201E4E4E402FD00E4E4E402FEE4",
                    INIT_11 => X"FEE4E40201E402023A02FD3806E48000E4E40202E402FEE4E40201E402FD00E4",
                    INIT_12 => X"E4E40201E4000202E48000020002E4E4E402FEE4E4020102E40202E432440E02",
                    INIT_13 => X"860100000600078431308231208031107E3100E8FE013020100000E4570E02FE",
                    INIT_14 => X"0000060007A43120A23110A03100E8FE01201000000006800753860486038602",
                    INIT_15 => X"54224054CD310001F4803110C93100E8FE011000000006800753A603A602A601",
                    INIT_16 => X"002E726F727265206B6341000006800753CF03CF02CF01000030200600071540",
                    INIT_17 => X"2C7C022300E08C00E066F903400140F303502A344030402C7C0323005964D502",
                    INIT_18 => X"726120646142000B0E1100F300DA0200DA0100DA00005953305320E0AC403040",
                    INIT_19 => X"0606060F0037013701FFFF00031F402C2801230059641A030073746E656D7567",
                    INIT_1A => X"40B8FF2030060606060F00440101535228B0A09080B83570605040B8FF523006",
                    INIT_1B => X"D0570120013A5953C0015001200000005E01016EB06EA06E906E80B835706050",
                    INIT_1C => X"D0B3060159CC0F3CD00133012C9B0F56D0580132012C9B0F56D05601319B0F56",
                    INIT_1D => X"200253D001430201440201540273C45006060606D006060606C0B85006060606",
                    INIT_1E => X"0201200253D0014302014402015402590134012C005901520201520201450201",
                    INIT_1F => X"80007103FE08B8407102F804B8407101F202B8407100EC01B840005953C00140",
                    INIT_20 => X"00060009100A132000011A0209FFFF000920030A0901090009000401FF000309",
                    INIT_21 => X"45333F323F31000E0050403E11003C11003A1109100F060606060607FF000E01",
                    INIT_22 => X"06060606FF000E015953070201023A01025359010201023A0102520102520102",
                    INIT_23 => X"070201023A01025359010252010252010245000E006E110920093009100F0606",
                    INIT_24 => X"0E00A11097200B00000B2080009EA1018B020B00000B0208009301000E015953",
                    INIT_25 => X"0EFE0080C20100B6200B0B20C0B0200B00AC020B0B020CA6020BB001000E0100",
                    INIT_26 => X"020BEC0100DBD50E08DC018000DBCD0E08DC008000D30100C8C30EFE0180C8BD",
                    INIT_27 => X"0F01000E0E0E0E0EF3200B0B0B0B402000EC200B000EE5020B0B0B0B040200DE",
                    INIT_28 => X"200B0B0B0B002006060606000E000F200B0009020B0B0B0B0002000E0000020B",
                    INIT_29 => X"0059533053403F231030102C7C0223005953110009102C7C012300595307001C",
                    INIT_2A => X"4D055900646E756F6620656369766564206F4E005B1030102C100110347C0323",
                    INIT_2B => X"5502014F020146020059014502014E02014F02014E027885102C7C0123005964",
                    INIT_2C => X"10C93010C93010C93010C930BA3330BE8530102C7C01230059014402014E0201",
                    INIT_2D => X"4502005953405350536053705380539053A053B010C93010C93010C93010C930",
                    INIT_2E => X"2300595310C930102C7C012300BA40301030102C7C0223005901520201520201",
                    INIT_2F => X"8530102C7C08102A7C0810297C03230059A430BA4430BACC305D8530102C7C01",
                    INIT_30 => X"BA304001BA304001BA403030BA304001BA304001BA304001BA403034BA55305D",
                    INIT_31 => X"5310C93010C930BABE30BACC305D8530102C7C01230059A430BA4430BA304001",
                    INIT_32 => X"01BA304001BA403034BA55305D8530102C7C08102A7C0810297C032300595340",
                    INIT_33 => X"10C93010C930BABE30BA304001BA304001BA304001BA403030BA304001BA3040",
                    INIT_34 => X"00A400C20300DC2000DC2000BAF02000015D8520F600102C7C01230059534053",
                    INIT_35 => X"59C30040944101DF60FE2060B43000B430B40000CD00B130B401A830B4000E0E",
                    INIT_36 => X"01000ED80E01DD00B0070130A00E0E0E013040008CA420B0A008593020CCCC08",
                    INIT_37 => X"0100010001000100400000B010D0EC010607F200FEB0070130A00E0E0E013040",
                    INIT_38 => X"6D656DAB000067726169746C756D00590B010153C00707400000010001000100",
                    INIT_39 => X"685401007379731001006E6F6973726576B6000074657365728D0000706D7564",
                    INIT_3A => X"FC020064725F6765725F633269E5020072775F6765725F633269C80100706C65",
                    INIT_3B => X"5F6332691103006361645F6C65735F6332690B03006364745F6C65735F633269",
                    INIT_3C => X"64725F6765725F716164E2000072775F6765725F7161640E030062645F6C6573",
                    INIT_3D => X"5F63647426050065746972775F6364742205007375746174735F636474CC0000",
                    INIT_3E => X"050064725F6765725F63647440050072775F6765725F6364742D050064616572",
                    INIT_3F => X"65748506007364695F7465675F706D65746305006863726165735F706D657431",
                   INITP_00 => X"FFE19640000007450E110809800820641FFFE536008278000000008F80000002",
                   INITP_01 => X"FFF7FF4380CD7D2823E202E70638B8303A010800C69FFFFFFFFFFF000001FFFF",
                   INITP_02 => X"0012E801FFE00041008120000002200000001297F8D7FFF3FF66FFFDFFEFFFBF",
                   INITP_03 => X"A8A28A000000000000040000450502850004DDCD005BD34015C081FFFDE49004",
                   INITP_04 => X"8000000000888800000000000000000C2D5B40040B0B55B6D4208408185AC619",
                   INITP_05 => X"3110094C018270000000082082053000000002405FFFE6E802D00201000A2014",
                   INITP_06 => X"5540C841A8882151369001132C94C50001111211122988800094C02222422245",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC455")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_ll(31 downto 0),
                  DOPA => data_out_a_ll(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_ll(31 downto 0),
                  DOPB => data_out_b_ll(35 downto 32), 
                   DIB => data_in_b_ll(31 downto 0),
                  DIPB => data_in_b_ll(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_lh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"6800690000506808006808006800690000000928787808280000015858010028",
                    INIT_05 => X"4A90684818680868280028000404815859D0E8580028001000B0E818890090E9",
                    INIT_06 => X"0000000000000000680800680800005008B0E8582868086B6B6A6A68284B4B4A",
                    INIT_07 => X"080808080808080808080828005008035088035088025088025008B0E8582800",
                    INIT_08 => X"00680800000068080000000008000D0D08080808080808080808080808080808",
                    INIT_09 => X"0808080808080808080808080808080808080808080808082800000068080000",
                    INIT_0A => X"1088685000F0E05908000D0D0808080808080808080808080808080808080808",
                    INIT_0B => X"0800F0E1095A68006900005800000D0D00680800680069000000680800680800",
                    INIT_0C => X"00690000508008680800F0E1095A000D0D001089680069000050800868080068",
                    INIT_0D => X"0050D0E38B038AA2A28902680800680069000000000D0D00F0E1095A00108968",
                    INIT_0E => X"1EA0A018B8B02800109D8D00109D8D680090E890E8250D0D280010CB68006900",
                    INIT_0F => X"00006E1E00006E2E00006E2E286E1E000000006E1E6E2E2E28F0CF0F28B86E26",
                    INIT_10 => X"00006E1E00006E2E6E2E00286E2E000000006E2E0000006E1E280000006E1E00",
                    INIT_11 => X"1E00006E2E006E2E116E1ED1A2000A2800006E2E006E1E00006E2E006E1E2800",
                    INIT_12 => X"00006E2E00A26B4B000A0A2E286A0000006E1E00006E2E4A006E2E0011D1A26E",
                    INIT_13 => X"110828B8A00801D10102D10102D10102D101020018B8B1B1B0B02800F1A26E1E",
                    INIT_14 => X"28B8A00801D10102D10102D101020018B8B1B0B028B8A0080100110811081108",
                    INIT_15 => X"01010101D101022800D10102D101020018B8B0B028B8A0080100110811081108",
                    INIT_16 => X"080808080808080808080828B8A008010011081108110828B8B1B1A008010101",
                    INIT_17 => X"0AB0E85828D10128D101D1EA518A51D1EA520A0A500A500AB0E8582800000D0D",
                    INIT_18 => X"08080808080828010101B608280008280008280008280000000000D101500A50",
                    INIT_19 => X"A0A0A00928F1CFF1CF0F0F286818500AB1E8582800000D0D0808080808080808",
                    INIT_1A => X"0400098018A0A0A0A00928F1C989D1B1EAD3D3D2C200010505040400098818A0",
                    INIT_1B => X"00096808680800000068086808060E28F1C989B1D3B1D3B1D2B1C20001050504",
                    INIT_1C => X"0091EE8E0091E9010068086808B1E901000968086808B1E90100096808B1E901",
                    INIT_1D => X"08000000680800680800680800110088A0A0A0A000A2A2A2A2020088A0A0A0A0",
                    INIT_1E => X"0068080000006808006808006808000068086808280068080068080068080068",
                    INIT_1F => X"0F280108916A00080108916A00080108916A00080108916A0008280000006808",
                    INIT_20 => X"28A1096808481292DFCFB218480F0F2868080268921848284828F2CF0F28026F",
                    INIT_21 => X"08091209120928A0080201D20202D20202D202022018A0A0A0A0A0A00128A109",
                    INIT_22 => X"A0A0A0A00128A109000002006800086800080069006800086800086800086800",
                    INIT_23 => X"02006800086800080068000868000868000828A008D20202000200022018A0A0",
                    INIT_24 => X"A0089268926848080868282808129268926848080868282808D26828A1090000",
                    INIT_25 => X"A1020809D2682892684868280892684828926848682808926848D26828A00828",
                    INIT_26 => X"6848D2682812F2A1A00208090812F2A1A002080908D2682812F2A102080912F2",
                    INIT_27 => X"D26828A0A0A0A0A092684848486828280892684828A092684848486828280892",
                    INIT_28 => X"68484848682828A0A0A0A0A0A008926848289268484848682828A0A008926848",
                    INIT_29 => X"280000000000D20250085008B0E8582800000228025008B0E858280000022892",
                    INIT_2A => X"0D0D000808080808080808080808080808080828025008500851885108B0E858",
                    INIT_2B => X"08006808006808002800680800680800680800680800D2025008B0E858280000",
                    INIT_2C => X"030200030200020200020200020800F202005108B0E858280068080068080068",
                    INIT_2D => X"0800280000000000000000000000000000000000050200050200040200040200",
                    INIT_2E => X"582800000002005108B0E8582802000052085108B0E858280068080068080068",
                    INIT_2F => X"02005108B0E85008B0E85008B0E85828000200020800020800F202005108B0E8",
                    INIT_30 => X"0200508A0200508A0250000A0200508A0200508A0200508A0250000A020800F2",
                    INIT_31 => X"00000200020200020800020800F202005108B0E858280002000208000200508A",
                    INIT_32 => X"8A0200508A0250000A020800F202005108B0E85008B0E85008B0E85828000000",
                    INIT_33 => X"0002000202000208000200508A0200508A0200508A0250000A0200508A020050",
                    INIT_34 => X"0B93E893E8A00200A00200080208000A09F20200030B5108B0E8582800000000",
                    INIT_35 => X"00B3EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3A0A0",
                    INIT_36 => X"1828A513A5CD93ED551DCD0585A5A5A5CD050D28130200010103000505131303",
                    INIT_37 => X"887088708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D",
                    INIT_38 => X"08080808080808080808080808082800F3CECE00500E8E0E2870887088708870",
                    INIT_39 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                   INITP_00 => X"FFF80013AAB64DF845FC4FC2FADF9DFFCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"EEEDF4CE0B99D4EFA017E98DF1626F8B63ECDFADAC4FFFFFFFFFFFEB5AD4FFFF",
                   INITP_02 => X"4FE1009CFFF9D586DCED4CEACEDA8CEAB3B6D43AE88BDCDD76D5EDDBEEBBEF7D",
                   INITP_03 => X"6AAAAABADADBAEDB6B6E00204D552A952B519558403306100D189CFFFFCDB758",
                   INITP_04 => X"A0844A112E4645D34B24C92928468453F6EDA7500CFDBEDB2A1B700C9321B967",
                   INITP_05 => X"8889D262744C09DB7AAAA492492989DB6DEDB7273FFFF804EB09F93F08026104",
                   INITP_06 => X"5532200052001C67C9494AA85248689D4A444444444C444EA526274888888889",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF90D5")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_lh(31 downto 0),
                  DOPA => data_out_a_lh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_lh(31 downto 0),
                  DOPB => data_out_b_lh(35 downto 32), 
                   DIB => data_in_b_lh(31 downto 0),
                  DIPB => data_in_b_lh(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      --
      kcpsm6_rom_hl: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"65725F706D6574C9050065746972775F706D657489050064695F7465675F706D",
                    INIT_01 => X"6E5F746E6972705F706D6574DF0500616E5F766E6F635F706D6574D405006461",
                    INIT_02 => X"726463440600746E6972705F706D6574F10500766E6F635F706D65742B060061",
                    INIT_03 => X"7100010180102080002099FF8C00A0001207FFCC00007264E200007764E60300",
                    INIT_04 => X"2023000060D48900608910A0000110A00001EC9D71000003800001880099FFA0",
                    INIT_05 => X"01242320B501BC2F20BC2010B51020082010002323C7050123019F2F0120C710",
                    INIT_06 => X"203A726F727245005964C80800646E616D6D6F6320646142009F000128230010",
                    INIT_07 => X"405010402800402417002300EF200110002C0438005901020102412064D90800",
                    INIT_08 => X"200120100020010600F5702F60F550010401701C016001601050F502012C0606",
                    INIT_09 => X"54084D0A4D0D00012018608959642509590021776F6C667265764F186E372F20",
                    INIT_0A => X"010802012002010802630220000C0120012059000C000102000C002001204820",
                    INIT_0B => X"0000000000000000000000000000000000000000000000000C00200120200120",
                    INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"9C864203FE1FFF22F037B0008019824B40167FFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_01 => X"0000000000000000000000000000000000000000DB400203FE417FF7EC00C487",
                   INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hl(31 downto 0),
                  DOPA => data_out_a_hl(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hl(31 downto 0),
                  DOPB => data_out_b_hl(35 downto 32), 
                   DIB => data_in_b_hl(31 downto 0),
                  DIPB => data_in_b_hl(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_hh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_01 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_02 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_03 => X"149D8D89B4E050F4E15894E894E825090D0D0808080808080808080808080808",
                    INIT_04 => X"5878082800040028000021259D8D01259D8D040414099D8D149D8D94E894E825",
                    INIT_05 => X"C88858011489F4005094E101D4E15889017180087894E88858C9F4008950F4E1",
                    INIT_06 => X"080808080808082800000D0D080808080808080808080808281471C88858C150",
                    INIT_07 => X"0383538008528008F4E2580AD4E08870080889092800680069000000000D0D08",
                    INIT_08 => X"78885870885848002814700050B4E38B148B7000CB508B51D4E3D4CB8A8BA3A3",
                    INIT_09 => X"94E894E894E850C85814000000000D0D000808080808080808080814C404F4E8",
                    INIT_0A => X"680800680800680800D4C85828A00878C8580028A008680028A00878C858F4E8",
                    INIT_0B => X"000000000000000000000000000000000000000000000028A00878C85878C858",
                    INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"00890FE9FFCFFFE00B484C325FF13895892A3FFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_01 => X"0000000000000000000000000000000000000124B6C93392A87CFFFE91F4B0A0",
                   INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hh(31 downto 0),
                  DOPA => data_out_a_hh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hh(31 downto 0),
                  DOPB => data_out_b_hh(35 downto 32), 
                   DIB => data_in_b_hh(31 downto 0),
                  DIPB => data_in_b_hh(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004001881",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"010201024130012002013A020102010241300000232000006010350131148900",
                    INIT_05 => X"05BC1003FF03080400810054EC9D102B27B504235400598E599A03030130A9FF",
                    INIT_06 => X"5340535053605370017802013002B8002C7C01230003F4080706050400080706",
                    INIT_07 => X"72656B6361725420322D6700C4002C1000011000011000011000307C02230059",
                    INIT_08 => X"50012E025360012E025370B80164F50000203A56206472616F426369676F4C20",
                    INIT_09 => X"6F5700203A746E756F632064726F5700203A72656666754200595340012E0253",
                    INIT_0A => X"590101100260202000642801002064726F5700202020203A73657A6973206472",
                    INIT_0B => X"20028E4000230102010241230264310159012902010201024120012802012002",
                    INIT_0C => X"02010241103028012002A3400023643E01597C01010201024110302401400201",
                    INIT_0D => X"4160A65003502C06060130012002010201024130644E0159C640002359940101",
                    INIT_0E => X"F007070301000059CA000359CA00010102D8FFD400A012070059BC0101020102",
                    INIT_0F => X"E4E402FDE4E40201E4E402020002FEE4E4E4E402FD02020100E5013E00000200",
                    INIT_10 => X"E4E402FEE4E402010202E4000202E4E4E4E40201E4E4E402FD00E4E4E402FEE4",
                    INIT_11 => X"FEE4E40201E402023A02FD3806E48000E4E40202E402FEE4E40201E402FD00E4",
                    INIT_12 => X"E4E40201E4000202E48000020002E4E4E402FEE4E4020102E40202E432440E02",
                    INIT_13 => X"860100000600078431308231208031107E3100E8FE013020100000E4570E02FE",
                    INIT_14 => X"0000060007A43120A23110A03100E8FE01201000000006800753860486038602",
                    INIT_15 => X"54224054CD310001F4803110C93100E8FE011000000006800753A603A602A601",
                    INIT_16 => X"002E726F727265206B6341000006800753CF03CF02CF01000030200600071540",
                    INIT_17 => X"2C7C022300E08C00E066F903400140F303502A344030402C7C0323005964D502",
                    INIT_18 => X"726120646142000B0E1100F300DA0200DA0100DA00005953305320E0AC403040",
                    INIT_19 => X"0606060F0037013701FFFF00031F402C2801230059641A030073746E656D7567",
                    INIT_1A => X"40B8FF2030060606060F00440101535228B0A09080B83570605040B8FF523006",
                    INIT_1B => X"D0570120013A5953C0015001200000005E01016EB06EA06E906E80B835706050",
                    INIT_1C => X"D0B3060159CC0F3CD00133012C9B0F56D0580132012C9B0F56D05601319B0F56",
                    INIT_1D => X"200253D001430201440201540273C45006060606D006060606C0B85006060606",
                    INIT_1E => X"0201200253D0014302014402015402590134012C005901520201520201450201",
                    INIT_1F => X"80007103FE08B8407102F804B8407101F202B8407100EC01B840005953C00140",
                    INIT_20 => X"00060009100A132000011A0209FFFF000920030A0901090009000401FF000309",
                    INIT_21 => X"45333F323F31000E0050403E11003C11003A1109100F060606060607FF000E01",
                    INIT_22 => X"06060606FF000E015953070201023A01025359010201023A0102520102520102",
                    INIT_23 => X"070201023A01025359010252010252010245000E006E110920093009100F0606",
                    INIT_24 => X"0E00A11097200B00000B2080009EA1018B020B00000B0208009301000E015953",
                    INIT_25 => X"0EFE0080C20100B6200B0B20C0B0200B00AC020B0B020CA6020BB001000E0100",
                    INIT_26 => X"020BEC0100DBD50E08DC018000DBCD0E08DC008000D30100C8C30EFE0180C8BD",
                    INIT_27 => X"0F01000E0E0E0E0EF3200B0B0B0B402000EC200B000EE5020B0B0B0B040200DE",
                    INIT_28 => X"200B0B0B0B002006060606000E000F200B0009020B0B0B0B0002000E0000020B",
                    INIT_29 => X"0059533053403F231030102C7C0223005953110009102C7C012300595307001C",
                    INIT_2A => X"4D055900646E756F6620656369766564206F4E005B1030102C100110347C0323",
                    INIT_2B => X"5502014F020146020059014502014E02014F02014E027885102C7C0123005964",
                    INIT_2C => X"10C93010C93010C93010C930BA3330BE8530102C7C01230059014402014E0201",
                    INIT_2D => X"4502005953405350536053705380539053A053B010C93010C93010C93010C930",
                    INIT_2E => X"2300595310C930102C7C012300BA40301030102C7C0223005901520201520201",
                    INIT_2F => X"8530102C7C08102A7C0810297C03230059A430BA4430BACC305D8530102C7C01",
                    INIT_30 => X"BA304001BA304001BA403030BA304001BA304001BA304001BA403034BA55305D",
                    INIT_31 => X"5310C93010C930BABE30BACC305D8530102C7C01230059A430BA4430BA304001",
                    INIT_32 => X"01BA304001BA403034BA55305D8530102C7C08102A7C0810297C032300595340",
                    INIT_33 => X"10C93010C930BABE30BA304001BA304001BA304001BA403030BA304001BA3040",
                    INIT_34 => X"00A400C20300DC2000DC2000BAF02000015D8520F600102C7C01230059534053",
                    INIT_35 => X"59C30040944101DF60FE2060B43000B430B40000CD00B130B401A830B4000E0E",
                    INIT_36 => X"01000ED80E01DD00B0070130A00E0E0E013040008CA420B0A008593020CCCC08",
                    INIT_37 => X"0100010001000100400000B010D0EC010607F200FEB0070130A00E0E0E013040",
                    INIT_38 => X"6D656DAB000067726169746C756D00590B010153C00707400000010001000100",
                    INIT_39 => X"685401007379731001006E6F6973726576B6000074657365728D0000706D7564",
                    INIT_3A => X"FC020064725F6765725F633269E5020072775F6765725F633269C80100706C65",
                    INIT_3B => X"5F6332691103006361645F6C65735F6332690B03006364745F6C65735F633269",
                    INIT_3C => X"64725F6765725F716164E2000072775F6765725F7161640E030062645F6C6573",
                    INIT_3D => X"5F63647426050065746972775F6364742205007375746174735F636474CC0000",
                    INIT_3E => X"050064725F6765725F63647440050072775F6765725F6364742D050064616572",
                    INIT_3F => X"65748506007364695F7465675F706D65746305006863726165735F706D657431",
                    INIT_40 => X"65725F706D6574C9050065746972775F706D657489050064695F7465675F706D",
                    INIT_41 => X"6E5F746E6972705F706D6574DF0500616E5F766E6F635F706D6574D405006461",
                    INIT_42 => X"726463440600746E6972705F706D6574F10500766E6F635F706D65742B060061",
                    INIT_43 => X"7100010180102080002099FF8C00A0001207FFCC00007264E200007764E60300",
                    INIT_44 => X"2023000060D48900608910A0000110A00001EC9D71000003800001880099FFA0",
                    INIT_45 => X"01242320B501BC2F20BC2010B51020082010002323C7050123019F2F0120C710",
                    INIT_46 => X"203A726F727245005964C80800646E616D6D6F6320646142009F000128230010",
                    INIT_47 => X"405010402800402417002300EF200110002C0438005901020102412064D90800",
                    INIT_48 => X"200120100020010600F5702F60F550010401701C016001601050F502012C0606",
                    INIT_49 => X"54084D0A4D0D00012018608959642509590021776F6C667265764F186E372F20",
                    INIT_4A => X"010802012002010802630220000C0120012059000C000102000C002001204820",
                    INIT_4B => X"0000000000000000000000000000000000000000000000000C00200120200120",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"FFE19640000007450E110809800820641FFFE536008278000000008F80000002",
                   INITP_01 => X"FFF7FF4380CD7D2823E202E70638B8303A010800C69FFFFFFFFFFF000001FFFF",
                   INITP_02 => X"0012E801FFE00041008120000002200000001297F8D7FFF3FF66FFFDFFEFFFBF",
                   INITP_03 => X"A8A28A000000000000040000450502850004DDCD005BD34015C081FFFDE49004",
                   INITP_04 => X"8000000000888800000000000000000C2D5B40040B0B55B6D4208408185AC619",
                   INITP_05 => X"3110094C018270000000082082053000000002405FFFE6E802D00201000A2014",
                   INITP_06 => X"5540C841A8882151369001132C94C50001111211122988800094C02222422245",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC455",
                   INITP_08 => X"9C864203FE1FFF22F037B0008019824B40167FFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"0000000000000000000000000000000000000000DB400203FE417FF7EC00C487",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"6800690000506808006808006800690000000928787808280000015858010028",
                    INIT_05 => X"4A90684818680868280028000404815859D0E8580028001000B0E818890090E9",
                    INIT_06 => X"0000000000000000680800680800005008B0E8582868086B6B6A6A68284B4B4A",
                    INIT_07 => X"080808080808080808080828005008035088035088025088025008B0E8582800",
                    INIT_08 => X"00680800000068080000000008000D0D08080808080808080808080808080808",
                    INIT_09 => X"0808080808080808080808080808080808080808080808082800000068080000",
                    INIT_0A => X"1088685000F0E05908000D0D0808080808080808080808080808080808080808",
                    INIT_0B => X"0800F0E1095A68006900005800000D0D00680800680069000000680800680800",
                    INIT_0C => X"00690000508008680800F0E1095A000D0D001089680069000050800868080068",
                    INIT_0D => X"0050D0E38B038AA2A28902680800680069000000000D0D00F0E1095A00108968",
                    INIT_0E => X"1EA0A018B8B02800109D8D00109D8D680090E890E8250D0D280010CB68006900",
                    INIT_0F => X"00006E1E00006E2E00006E2E286E1E000000006E1E6E2E2E28F0CF0F28B86E26",
                    INIT_10 => X"00006E1E00006E2E6E2E00286E2E000000006E2E0000006E1E280000006E1E00",
                    INIT_11 => X"1E00006E2E006E2E116E1ED1A2000A2800006E2E006E1E00006E2E006E1E2800",
                    INIT_12 => X"00006E2E00A26B4B000A0A2E286A0000006E1E00006E2E4A006E2E0011D1A26E",
                    INIT_13 => X"110828B8A00801D10102D10102D10102D101020018B8B1B1B0B02800F1A26E1E",
                    INIT_14 => X"28B8A00801D10102D10102D101020018B8B1B0B028B8A0080100110811081108",
                    INIT_15 => X"01010101D101022800D10102D101020018B8B0B028B8A0080100110811081108",
                    INIT_16 => X"080808080808080808080828B8A008010011081108110828B8B1B1A008010101",
                    INIT_17 => X"0AB0E85828D10128D101D1EA518A51D1EA520A0A500A500AB0E8582800000D0D",
                    INIT_18 => X"08080808080828010101B608280008280008280008280000000000D101500A50",
                    INIT_19 => X"A0A0A00928F1CFF1CF0F0F286818500AB1E8582800000D0D0808080808080808",
                    INIT_1A => X"0400098018A0A0A0A00928F1C989D1B1EAD3D3D2C200010505040400098818A0",
                    INIT_1B => X"00096808680800000068086808060E28F1C989B1D3B1D3B1D2B1C20001050504",
                    INIT_1C => X"0091EE8E0091E9010068086808B1E901000968086808B1E90100096808B1E901",
                    INIT_1D => X"08000000680800680800680800110088A0A0A0A000A2A2A2A2020088A0A0A0A0",
                    INIT_1E => X"0068080000006808006808006808000068086808280068080068080068080068",
                    INIT_1F => X"0F280108916A00080108916A00080108916A00080108916A0008280000006808",
                    INIT_20 => X"28A1096808481292DFCFB218480F0F2868080268921848284828F2CF0F28026F",
                    INIT_21 => X"08091209120928A0080201D20202D20202D202022018A0A0A0A0A0A00128A109",
                    INIT_22 => X"A0A0A0A00128A109000002006800086800080069006800086800086800086800",
                    INIT_23 => X"02006800086800080068000868000868000828A008D20202000200022018A0A0",
                    INIT_24 => X"A0089268926848080868282808129268926848080868282808D26828A1090000",
                    INIT_25 => X"A1020809D2682892684868280892684828926848682808926848D26828A00828",
                    INIT_26 => X"6848D2682812F2A1A00208090812F2A1A002080908D2682812F2A102080912F2",
                    INIT_27 => X"D26828A0A0A0A0A092684848486828280892684828A092684848486828280892",
                    INIT_28 => X"68484848682828A0A0A0A0A0A008926848289268484848682828A0A008926848",
                    INIT_29 => X"280000000000D20250085008B0E8582800000228025008B0E858280000022892",
                    INIT_2A => X"0D0D000808080808080808080808080808080828025008500851885108B0E858",
                    INIT_2B => X"08006808006808002800680800680800680800680800D2025008B0E858280000",
                    INIT_2C => X"030200030200020200020200020800F202005108B0E858280068080068080068",
                    INIT_2D => X"0800280000000000000000000000000000000000050200050200040200040200",
                    INIT_2E => X"582800000002005108B0E8582802000052085108B0E858280068080068080068",
                    INIT_2F => X"02005108B0E85008B0E85008B0E85828000200020800020800F202005108B0E8",
                    INIT_30 => X"0200508A0200508A0250000A0200508A0200508A0200508A0250000A020800F2",
                    INIT_31 => X"00000200020200020800020800F202005108B0E858280002000208000200508A",
                    INIT_32 => X"8A0200508A0250000A020800F202005108B0E85008B0E85008B0E85828000000",
                    INIT_33 => X"0002000202000208000200508A0200508A0200508A0250000A0200508A020050",
                    INIT_34 => X"0B93E893E8A00200A00200080208000A09F20200030B5108B0E8582800000000",
                    INIT_35 => X"00B3EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3A0A0",
                    INIT_36 => X"1828A513A5CD93ED551DCD0585A5A5A5CD050D28130200010103000505131303",
                    INIT_37 => X"887088708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D",
                    INIT_38 => X"08080808080808080808080808082800F3CECE00500E8E0E2870887088708870",
                    INIT_39 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_40 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_41 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_42 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_43 => X"149D8D89B4E050F4E15894E894E825090D0D0808080808080808080808080808",
                    INIT_44 => X"5878082800040028000021259D8D01259D8D040414099D8D149D8D94E894E825",
                    INIT_45 => X"C88858011489F4005094E101D4E15889017180087894E88858C9F4008950F4E1",
                    INIT_46 => X"080808080808082800000D0D080808080808080808080808281471C88858C150",
                    INIT_47 => X"0383538008528008F4E2580AD4E08870080889092800680069000000000D0D08",
                    INIT_48 => X"78885870885848002814700050B4E38B148B7000CB508B51D4E3D4CB8A8BA3A3",
                    INIT_49 => X"94E894E894E850C85814000000000D0D000808080808080808080814C404F4E8",
                    INIT_4A => X"680800680800680800D4C85828A00878C8580028A008680028A00878C858F4E8",
                    INIT_4B => X"000000000000000000000000000000000000000000000028A00878C85878C858",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"FFF80013AAB64DF845FC4FC2FADF9DFFCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"EEEDF4CE0B99D4EFA017E98DF1626F8B63ECDFADAC4FFFFFFFFFFFEB5AD4FFFF",
                   INITP_02 => X"4FE1009CFFF9D586DCED4CEACEDA8CEAB3B6D43AE88BDCDD76D5EDDBEEBBEF7D",
                   INITP_03 => X"6AAAAABADADBAEDB6B6E00204D552A952B519558403306100D189CFFFFCDB758",
                   INITP_04 => X"A0844A112E4645D34B24C92928468453F6EDA7500CFDBEDB2A1B700C9321B967",
                   INITP_05 => X"8889D262744C09DB7AAAA492492989DB6DEDB7273FFFF804EB09F93F08026104",
                   INITP_06 => X"5532200052001C67C9494AA85248689D4A444444444C444EA526274888888889",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF90D5",
                   INITP_08 => X"00890FE9FFCFFFE00B484C325FF13895892A3FFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"0000000000000000000000000000000000000124B6C93392A87CFFFE91F4B0A0",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004001881",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"010201024130012002013A020102010241300000232000006010350131148900",
                    INIT_05 => X"05BC1003FF03080400810054EC9D102B27B504235400598E599A03030130A9FF",
                    INIT_06 => X"5340535053605370017802013002B8002C7C01230003F4080706050400080706",
                    INIT_07 => X"72656B6361725420322D6700C4002C1000011000011000011000307C02230059",
                    INIT_08 => X"50012E025360012E025370B80164F50000203A56206472616F426369676F4C20",
                    INIT_09 => X"6F5700203A746E756F632064726F5700203A72656666754200595340012E0253",
                    INIT_0A => X"590101100260202000642801002064726F5700202020203A73657A6973206472",
                    INIT_0B => X"20028E4000230102010241230264310159012902010201024120012802012002",
                    INIT_0C => X"02010241103028012002A3400023643E01597C01010201024110302401400201",
                    INIT_0D => X"4160A65003502C06060130012002010201024130644E0159C640002359940101",
                    INIT_0E => X"F007070301000059CA000359CA00010102D8FFD400A012070059BC0101020102",
                    INIT_0F => X"E4E402FDE4E40201E4E402020002FEE4E4E4E402FD02020100E5013E00000200",
                    INIT_10 => X"E4E402FEE4E402010202E4000202E4E4E4E40201E4E4E402FD00E4E4E402FEE4",
                    INIT_11 => X"FEE4E40201E402023A02FD3806E48000E4E40202E402FEE4E40201E402FD00E4",
                    INIT_12 => X"E4E40201E4000202E48000020002E4E4E402FEE4E4020102E40202E432440E02",
                    INIT_13 => X"860100000600078431308231208031107E3100E8FE013020100000E4570E02FE",
                    INIT_14 => X"0000060007A43120A23110A03100E8FE01201000000006800753860486038602",
                    INIT_15 => X"54224054CD310001F4803110C93100E8FE011000000006800753A603A602A601",
                    INIT_16 => X"002E726F727265206B6341000006800753CF03CF02CF01000030200600071540",
                    INIT_17 => X"2C7C022300E08C00E066F903400140F303502A344030402C7C0323005964D502",
                    INIT_18 => X"726120646142000B0E1100F300DA0200DA0100DA00005953305320E0AC403040",
                    INIT_19 => X"0606060F0037013701FFFF00031F402C2801230059641A030073746E656D7567",
                    INIT_1A => X"40B8FF2030060606060F00440101535228B0A09080B83570605040B8FF523006",
                    INIT_1B => X"D0570120013A5953C0015001200000005E01016EB06EA06E906E80B835706050",
                    INIT_1C => X"D0B3060159CC0F3CD00133012C9B0F56D0580132012C9B0F56D05601319B0F56",
                    INIT_1D => X"200253D001430201440201540273C45006060606D006060606C0B85006060606",
                    INIT_1E => X"0201200253D0014302014402015402590134012C005901520201520201450201",
                    INIT_1F => X"80007103FE08B8407102F804B8407101F202B8407100EC01B840005953C00140",
                    INIT_20 => X"00060009100A132000011A0209FFFF000920030A0901090009000401FF000309",
                    INIT_21 => X"45333F323F31000E0050403E11003C11003A1109100F060606060607FF000E01",
                    INIT_22 => X"06060606FF000E015953070201023A01025359010201023A0102520102520102",
                    INIT_23 => X"070201023A01025359010252010252010245000E006E110920093009100F0606",
                    INIT_24 => X"0E00A11097200B00000B2080009EA1018B020B00000B0208009301000E015953",
                    INIT_25 => X"0EFE0080C20100B6200B0B20C0B0200B00AC020B0B020CA6020BB001000E0100",
                    INIT_26 => X"020BEC0100DBD50E08DC018000DBCD0E08DC008000D30100C8C30EFE0180C8BD",
                    INIT_27 => X"0F01000E0E0E0E0EF3200B0B0B0B402000EC200B000EE5020B0B0B0B040200DE",
                    INIT_28 => X"200B0B0B0B002006060606000E000F200B0009020B0B0B0B0002000E0000020B",
                    INIT_29 => X"0059533053403F231030102C7C0223005953110009102C7C012300595307001C",
                    INIT_2A => X"4D055900646E756F6620656369766564206F4E005B1030102C100110347C0323",
                    INIT_2B => X"5502014F020146020059014502014E02014F02014E027885102C7C0123005964",
                    INIT_2C => X"10C93010C93010C93010C930BA3330BE8530102C7C01230059014402014E0201",
                    INIT_2D => X"4502005953405350536053705380539053A053B010C93010C93010C93010C930",
                    INIT_2E => X"2300595310C930102C7C012300BA40301030102C7C0223005901520201520201",
                    INIT_2F => X"8530102C7C08102A7C0810297C03230059A430BA4430BACC305D8530102C7C01",
                    INIT_30 => X"BA304001BA304001BA403030BA304001BA304001BA304001BA403034BA55305D",
                    INIT_31 => X"5310C93010C930BABE30BACC305D8530102C7C01230059A430BA4430BA304001",
                    INIT_32 => X"01BA304001BA403034BA55305D8530102C7C08102A7C0810297C032300595340",
                    INIT_33 => X"10C93010C930BABE30BA304001BA304001BA304001BA403030BA304001BA3040",
                    INIT_34 => X"00A400C20300DC2000DC2000BAF02000015D8520F600102C7C01230059534053",
                    INIT_35 => X"59C30040944101DF60FE2060B43000B430B40000CD00B130B401A830B4000E0E",
                    INIT_36 => X"01000ED80E01DD00B0070130A00E0E0E013040008CA420B0A008593020CCCC08",
                    INIT_37 => X"0100010001000100400000B010D0EC010607F200FEB0070130A00E0E0E013040",
                    INIT_38 => X"6D656DAB000067726169746C756D00590B010153C00707400000010001000100",
                    INIT_39 => X"685401007379731001006E6F6973726576B6000074657365728D0000706D7564",
                    INIT_3A => X"FC020064725F6765725F633269E5020072775F6765725F633269C80100706C65",
                    INIT_3B => X"5F6332691103006361645F6C65735F6332690B03006364745F6C65735F633269",
                    INIT_3C => X"64725F6765725F716164E2000072775F6765725F7161640E030062645F6C6573",
                    INIT_3D => X"5F63647426050065746972775F6364742205007375746174735F636474CC0000",
                    INIT_3E => X"050064725F6765725F63647440050072775F6765725F6364742D050064616572",
                    INIT_3F => X"65748506007364695F7465675F706D65746305006863726165735F706D657431",
                    INIT_40 => X"65725F706D6574C9050065746972775F706D657489050064695F7465675F706D",
                    INIT_41 => X"6E5F746E6972705F706D6574DF0500616E5F766E6F635F706D6574D405006461",
                    INIT_42 => X"726463440600746E6972705F706D6574F10500766E6F635F706D65742B060061",
                    INIT_43 => X"7100010180102080002099FF8C00A0001207FFCC00007264E200007764E60300",
                    INIT_44 => X"2023000060D48900608910A0000110A00001EC9D71000003800001880099FFA0",
                    INIT_45 => X"01242320B501BC2F20BC2010B51020082010002323C7050123019F2F0120C710",
                    INIT_46 => X"203A726F727245005964C80800646E616D6D6F6320646142009F000128230010",
                    INIT_47 => X"405010402800402417002300EF200110002C0438005901020102412064D90800",
                    INIT_48 => X"200120100020010600F5702F60F550010401701C016001601050F502012C0606",
                    INIT_49 => X"54084D0A4D0D00012018608959642509590021776F6C667265764F186E372F20",
                    INIT_4A => X"010802012002010802630220000C0120012059000C000102000C002001204820",
                    INIT_4B => X"0000000000000000000000000000000000000000000000000C00200120200120",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"FFE19640000007450E110809800820641FFFE536008278000000008F80000002",
                   INITP_01 => X"FFF7FF4380CD7D2823E202E70638B8303A010800C69FFFFFFFFFFF000001FFFF",
                   INITP_02 => X"0012E801FFE00041008120000002200000001297F8D7FFF3FF66FFFDFFEFFFBF",
                   INITP_03 => X"A8A28A000000000000040000450502850004DDCD005BD34015C081FFFDE49004",
                   INITP_04 => X"8000000000888800000000000000000C2D5B40040B0B55B6D4208408185AC619",
                   INITP_05 => X"3110094C018270000000082082053000000002405FFFE6E802D00201000A2014",
                   INITP_06 => X"5540C841A8882151369001132C94C50001111211122988800094C02222422245",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC455",
                   INITP_08 => X"9C864203FE1FFF22F037B0008019824B40167FFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"0000000000000000000000000000000000000000DB400203FE417FF7EC00C487",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"6800690000506808006808006800690000000928787808280000015858010028",
                    INIT_05 => X"4A90684818680868280028000404815859D0E8580028001000B0E818890090E9",
                    INIT_06 => X"0000000000000000680800680800005008B0E8582868086B6B6A6A68284B4B4A",
                    INIT_07 => X"080808080808080808080828005008035088035088025088025008B0E8582800",
                    INIT_08 => X"00680800000068080000000008000D0D08080808080808080808080808080808",
                    INIT_09 => X"0808080808080808080808080808080808080808080808082800000068080000",
                    INIT_0A => X"1088685000F0E05908000D0D0808080808080808080808080808080808080808",
                    INIT_0B => X"0800F0E1095A68006900005800000D0D00680800680069000000680800680800",
                    INIT_0C => X"00690000508008680800F0E1095A000D0D001089680069000050800868080068",
                    INIT_0D => X"0050D0E38B038AA2A28902680800680069000000000D0D00F0E1095A00108968",
                    INIT_0E => X"1EA0A018B8B02800109D8D00109D8D680090E890E8250D0D280010CB68006900",
                    INIT_0F => X"00006E1E00006E2E00006E2E286E1E000000006E1E6E2E2E28F0CF0F28B86E26",
                    INIT_10 => X"00006E1E00006E2E6E2E00286E2E000000006E2E0000006E1E280000006E1E00",
                    INIT_11 => X"1E00006E2E006E2E116E1ED1A2000A2800006E2E006E1E00006E2E006E1E2800",
                    INIT_12 => X"00006E2E00A26B4B000A0A2E286A0000006E1E00006E2E4A006E2E0011D1A26E",
                    INIT_13 => X"110828B8A00801D10102D10102D10102D101020018B8B1B1B0B02800F1A26E1E",
                    INIT_14 => X"28B8A00801D10102D10102D101020018B8B1B0B028B8A0080100110811081108",
                    INIT_15 => X"01010101D101022800D10102D101020018B8B0B028B8A0080100110811081108",
                    INIT_16 => X"080808080808080808080828B8A008010011081108110828B8B1B1A008010101",
                    INIT_17 => X"0AB0E85828D10128D101D1EA518A51D1EA520A0A500A500AB0E8582800000D0D",
                    INIT_18 => X"08080808080828010101B608280008280008280008280000000000D101500A50",
                    INIT_19 => X"A0A0A00928F1CFF1CF0F0F286818500AB1E8582800000D0D0808080808080808",
                    INIT_1A => X"0400098018A0A0A0A00928F1C989D1B1EAD3D3D2C200010505040400098818A0",
                    INIT_1B => X"00096808680800000068086808060E28F1C989B1D3B1D3B1D2B1C20001050504",
                    INIT_1C => X"0091EE8E0091E9010068086808B1E901000968086808B1E90100096808B1E901",
                    INIT_1D => X"08000000680800680800680800110088A0A0A0A000A2A2A2A2020088A0A0A0A0",
                    INIT_1E => X"0068080000006808006808006808000068086808280068080068080068080068",
                    INIT_1F => X"0F280108916A00080108916A00080108916A00080108916A0008280000006808",
                    INIT_20 => X"28A1096808481292DFCFB218480F0F2868080268921848284828F2CF0F28026F",
                    INIT_21 => X"08091209120928A0080201D20202D20202D202022018A0A0A0A0A0A00128A109",
                    INIT_22 => X"A0A0A0A00128A109000002006800086800080069006800086800086800086800",
                    INIT_23 => X"02006800086800080068000868000868000828A008D20202000200022018A0A0",
                    INIT_24 => X"A0089268926848080868282808129268926848080868282808D26828A1090000",
                    INIT_25 => X"A1020809D2682892684868280892684828926848682808926848D26828A00828",
                    INIT_26 => X"6848D2682812F2A1A00208090812F2A1A002080908D2682812F2A102080912F2",
                    INIT_27 => X"D26828A0A0A0A0A092684848486828280892684828A092684848486828280892",
                    INIT_28 => X"68484848682828A0A0A0A0A0A008926848289268484848682828A0A008926848",
                    INIT_29 => X"280000000000D20250085008B0E8582800000228025008B0E858280000022892",
                    INIT_2A => X"0D0D000808080808080808080808080808080828025008500851885108B0E858",
                    INIT_2B => X"08006808006808002800680800680800680800680800D2025008B0E858280000",
                    INIT_2C => X"030200030200020200020200020800F202005108B0E858280068080068080068",
                    INIT_2D => X"0800280000000000000000000000000000000000050200050200040200040200",
                    INIT_2E => X"582800000002005108B0E8582802000052085108B0E858280068080068080068",
                    INIT_2F => X"02005108B0E85008B0E85008B0E85828000200020800020800F202005108B0E8",
                    INIT_30 => X"0200508A0200508A0250000A0200508A0200508A0200508A0250000A020800F2",
                    INIT_31 => X"00000200020200020800020800F202005108B0E858280002000208000200508A",
                    INIT_32 => X"8A0200508A0250000A020800F202005108B0E85008B0E85008B0E85828000000",
                    INIT_33 => X"0002000202000208000200508A0200508A0200508A0250000A0200508A020050",
                    INIT_34 => X"0B93E893E8A00200A00200080208000A09F20200030B5108B0E8582800000000",
                    INIT_35 => X"00B3EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3A0A0",
                    INIT_36 => X"1828A513A5CD93ED551DCD0585A5A5A5CD050D28130200010103000505131303",
                    INIT_37 => X"887088708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D",
                    INIT_38 => X"08080808080808080808080808082800F3CECE00500E8E0E2870887088708870",
                    INIT_39 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_40 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_41 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_42 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_43 => X"149D8D89B4E050F4E15894E894E825090D0D0808080808080808080808080808",
                    INIT_44 => X"5878082800040028000021259D8D01259D8D040414099D8D149D8D94E894E825",
                    INIT_45 => X"C88858011489F4005094E101D4E15889017180087894E88858C9F4008950F4E1",
                    INIT_46 => X"080808080808082800000D0D080808080808080808080808281471C88858C150",
                    INIT_47 => X"0383538008528008F4E2580AD4E08870080889092800680069000000000D0D08",
                    INIT_48 => X"78885870885848002814700050B4E38B148B7000CB508B51D4E3D4CB8A8BA3A3",
                    INIT_49 => X"94E894E894E850C85814000000000D0D000808080808080808080814C404F4E8",
                    INIT_4A => X"680800680800680800D4C85828A00878C8580028A008680028A00878C858F4E8",
                    INIT_4B => X"000000000000000000000000000000000000000000000028A00878C85878C858",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"FFF80013AAB64DF845FC4FC2FADF9DFFCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"EEEDF4CE0B99D4EFA017E98DF1626F8B63ECDFADAC4FFFFFFFFFFFEB5AD4FFFF",
                   INITP_02 => X"4FE1009CFFF9D586DCED4CEACEDA8CEAB3B6D43AE88BDCDD76D5EDDBEEBBEF7D",
                   INITP_03 => X"6AAAAABADADBAEDB6B6E00204D552A952B519558403306100D189CFFFFCDB758",
                   INITP_04 => X"A0844A112E4645D34B24C92928468453F6EDA7500CFDBEDB2A1B700C9321B967",
                   INITP_05 => X"8889D262744C09DB7AAAA492492989DB6DEDB7273FFFF804EB09F93F08026104",
                   INITP_06 => X"5532200052001C67C9494AA85248689D4A444444444C444EA526274888888889",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF90D5",
                   INITP_08 => X"00890FE9FFCFFFE00B484C325FF13895892A3FFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"0000000000000000000000000000000000000124B6C93392A87CFFFE91F4B0A0",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_4k_generate;	              
  --
  --
  --
  --
  -- JTAG Loader
  --
  instantiate_loader : if (C_JTAG_LOADER_ENABLE = 1) generate
  --
    jtag_loader_6_inst : jtag_loader_6
    generic map(              C_FAMILY => C_FAMILY,
                       C_NUM_PICOBLAZE => 1,
                  C_JTAG_LOADER_ENABLE => C_JTAG_LOADER_ENABLE,
                 C_BRAM_MAX_ADDR_WIDTH => BRAM_ADDRESS_WIDTH,
	                  C_ADDR_WIDTH_0 => BRAM_ADDRESS_WIDTH)
    port map( picoblaze_reset => rdl_bus,
                      jtag_en => jtag_en,
                     jtag_din => jtag_din,
                    jtag_addr => jtag_addr(BRAM_ADDRESS_WIDTH-1 downto 0),
                     jtag_clk => jtag_clk,
                      jtag_we => jtag_we,
                  jtag_dout_0 => jtag_dout,
                  jtag_dout_1 => jtag_dout, -- ports 1-7 are not used
                  jtag_dout_2 => jtag_dout, -- in a 1 device debug 
                  jtag_dout_3 => jtag_dout, -- session.  However, Synplify
                  jtag_dout_4 => jtag_dout, -- etc require all ports to
                  jtag_dout_5 => jtag_dout, -- be connected
                  jtag_dout_6 => jtag_dout,
                  jtag_dout_7 => jtag_dout);
    --  
  end generate instantiate_loader;
  --
end low_level_definition;
--
--
-------------------------------------------------------------------------------------------
--
-- JTAG Loader 
--
-------------------------------------------------------------------------------------------
--
--
-- JTAG Loader 6 - Version 6.00
-- Kris Chaplin 4 February 2010
-- Ken Chapman 15 August 2011 - Revised coding style
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--
library unisim;
use unisim.vcomponents.all;
--
entity jtag_loader_6 is
generic(              C_JTAG_LOADER_ENABLE : integer := 1;
                                  C_FAMILY : string := "V6";
                           C_NUM_PICOBLAZE : integer := 1;
                     C_BRAM_MAX_ADDR_WIDTH : integer := 10;
        C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                              C_JTAG_CHAIN : integer := 2;
                            C_ADDR_WIDTH_0 : integer := 10;
                            C_ADDR_WIDTH_1 : integer := 10;
                            C_ADDR_WIDTH_2 : integer := 10;
                            C_ADDR_WIDTH_3 : integer := 10;
                            C_ADDR_WIDTH_4 : integer := 10;
                            C_ADDR_WIDTH_5 : integer := 10;
                            C_ADDR_WIDTH_6 : integer := 10;
                            C_ADDR_WIDTH_7 : integer := 10);
port(   picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
               jtag_din : out std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
              jtag_addr : out std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0) := (others => '0');
               jtag_clk : out std_logic := '0';
                jtag_we : out std_logic := '0';
            jtag_dout_0 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_1 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_2 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_3 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_4 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_5 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_6 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_7 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end jtag_loader_6;
--
architecture Behavioral of jtag_loader_6 is
  --
  signal num_picoblaze       : std_logic_vector(2 downto 0);
  signal picoblaze_instruction_data_width : std_logic_vector(4 downto 0);
  --
  signal drck                : std_logic;
  signal shift_clk           : std_logic;
  signal shift_din           : std_logic;
  signal shift_dout          : std_logic;
  signal shift               : std_logic;
  signal capture             : std_logic;
  --
  signal control_reg_ce      : std_logic;
  signal bram_ce             : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal bus_zero            : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  signal jtag_en_int         : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal jtag_en_expanded    : std_logic_vector(7 downto 0) := (others => '0');
  signal jtag_addr_int       : std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
  signal jtag_din_int        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal control_din         : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout_int    : std_logic_vector(7 downto 0):= (others => '0');
  signal bram_dout_int       : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
  signal jtag_we_int         : std_logic;
  signal jtag_clk_int        : std_logic;
  signal bram_ce_valid       : std_logic;
  signal din_load            : std_logic;
  --
  signal jtag_dout_0_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_1_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_2_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_3_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_4_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_5_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_6_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_7_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal picoblaze_reset_int : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  --        
begin
  bus_zero <= (others => '0');
  --
  jtag_loader_gen: if (C_JTAG_LOADER_ENABLE = 1) generate
    --
    -- Insert BSCAN primitive for target device architecture.
    --
    BSCAN_SPARTAN6_gen: if (C_FAMILY="S6") generate
    begin
      BSCAN_BLOCK_inst : BSCAN_SPARTAN6
      generic map ( JTAG_CHAIN => C_JTAG_CHAIN)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_SPARTAN6_gen;   
    --
    BSCAN_VIRTEX6_gen: if (C_FAMILY="V6") generate
    begin
      BSCAN_BLOCK_inst: BSCAN_VIRTEX6
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => FALSE)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_VIRTEX6_gen;   
    --
    BSCAN_7SERIES_gen: if (C_FAMILY="7S") generate
    begin
      BSCAN_BLOCK_inst: BSCANE2
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => "FALSE")
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_7SERIES_gen;   
    --
    --
    -- Insert clock buffer to ensure reliable shift operations.
    --
    upload_clock: BUFG
    port map( I => drck,
              O => shift_clk);
    --        
    --        
    --  Shift Register      
    --        
    --
    control_reg_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk = '1' then
        if (shift = '1') then
          control_reg_ce <= shift_din;
        end if;
      end if;
    end process control_reg_ce_shift;
    --        
    bram_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          if(C_NUM_PICOBLAZE > 1) then
            for i in 0 to C_NUM_PICOBLAZE-2 loop
              bram_ce(i+1) <= bram_ce(i);
            end loop;
          end if;
          bram_ce(0) <= control_reg_ce;
        end if;
      end if;
    end process bram_ce_shift;
    --        
    bram_we_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          jtag_we_int <= bram_ce(C_NUM_PICOBLAZE-1);
        end if;
      end if;
    end process bram_we_shift;
    --        
    bram_a_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          for i in 0 to C_BRAM_MAX_ADDR_WIDTH-2 loop
            jtag_addr_int(i+1) <= jtag_addr_int(i);
          end loop;
          jtag_addr_int(0) <= jtag_we_int;
        end if;
      end if;
    end process bram_a_shift;
    --        
    bram_d_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (din_load = '1') then
          jtag_din_int <= bram_dout_int;
         elsif (shift = '1') then
          for i in 0 to C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-2 loop
            jtag_din_int(i+1) <= jtag_din_int(i);
          end loop;
          jtag_din_int(0) <= jtag_addr_int(C_BRAM_MAX_ADDR_WIDTH-1);
        end if;
      end if;
    end process bram_d_shift;
    --
    shift_dout <= jtag_din_int(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1);
    --
    --
    din_load_select:process (bram_ce, din_load, capture, bus_zero, control_reg_ce) 
    begin
      if ( bram_ce = bus_zero ) then
        din_load <= capture and control_reg_ce;
       else
        din_load <= capture;
      end if;
    end process din_load_select;
    --
    --
    -- Control Registers 
    --
    num_picoblaze <= conv_std_logic_vector(C_NUM_PICOBLAZE-1,3);
    picoblaze_instruction_data_width <= conv_std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1,5);
    --	
    control_registers: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '0') and (control_reg_ce = '1') then
          case (jtag_addr_int(3 downto 0)) is 
            when "0000" => -- 0 = version - returns (7 downto 4) illustrating number of PB
                           --               and (3 downto 0) picoblaze instruction data width
                           control_dout_int <= num_picoblaze & picoblaze_instruction_data_width;
            when "0001" => -- 1 = PicoBlaze 0 reset / status
                           if (C_NUM_PICOBLAZE >= 1) then 
                            control_dout_int <= picoblaze_reset_int(0) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_0-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0010" => -- 2 = PicoBlaze 1 reset / status
                           if (C_NUM_PICOBLAZE >= 2) then 
                             control_dout_int <= picoblaze_reset_int(1) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_1-1,5) );
                            else 
                             control_dout_int <= (others => '0');
                           end if;
            when "0011" => -- 3 = PicoBlaze 2 reset / status
                           if (C_NUM_PICOBLAZE >= 3) then 
                            control_dout_int <= picoblaze_reset_int(2) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_2-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0100" => -- 4 = PicoBlaze 3 reset / status
                           if (C_NUM_PICOBLAZE >= 4) then 
                            control_dout_int <= picoblaze_reset_int(3) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_3-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0101" => -- 5 = PicoBlaze 4 reset / status
                           if (C_NUM_PICOBLAZE >= 5) then 
                            control_dout_int <= picoblaze_reset_int(4) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_4-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0110" => -- 6 = PicoBlaze 5 reset / status
                           if (C_NUM_PICOBLAZE >= 6) then 
                            control_dout_int <= picoblaze_reset_int(5) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_5-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0111" => -- 7 = PicoBlaze 6 reset / status
                           if (C_NUM_PICOBLAZE >= 7) then 
                            control_dout_int <= picoblaze_reset_int(6) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_6-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1000" => -- 8 = PicoBlaze 7 reset / status
                           if (C_NUM_PICOBLAZE >= 8) then 
                            control_dout_int <= picoblaze_reset_int(7) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_7-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1111" => control_dout_int <= conv_std_logic_vector(C_BRAM_MAX_ADDR_WIDTH -1,8);
            when others => control_dout_int <= (others => '1');
          end case;
        else 
          control_dout_int <= (others => '0');
        end if;
      end if;
    end process control_registers;
    -- 
    control_dout(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-8) <= control_dout_int;
    --
    pb_reset: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '1') and (control_reg_ce = '1') then
          picoblaze_reset_int(C_NUM_PICOBLAZE-1 downto 0) <= control_din(C_NUM_PICOBLAZE-1 downto 0);
        end if;
      end if;
    end process pb_reset;    
    --
    --
    -- Assignments 
    --
    control_dout (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-9 downto 0) <= (others => '0') when (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH > 8);
    --
    -- Qualify the blockram CS signal with bscan select output
    jtag_en_int <= bram_ce when bram_ce_valid = '1' else (others => '0');
    --      
    jtag_en_expanded(C_NUM_PICOBLAZE-1 downto 0) <= jtag_en_int;
    jtag_en_expanded(7 downto C_NUM_PICOBLAZE) <= (others => '0') when (C_NUM_PICOBLAZE < 8);
    --        
    bram_dout_int <= control_dout or jtag_dout_0_masked or jtag_dout_1_masked or jtag_dout_2_masked or jtag_dout_3_masked or jtag_dout_4_masked or jtag_dout_5_masked or jtag_dout_6_masked or jtag_dout_7_masked;
    --
    control_din <= jtag_din_int;
    --        
    jtag_dout_0_masked <= jtag_dout_0 when jtag_en_expanded(0) = '1' else (others => '0');
    jtag_dout_1_masked <= jtag_dout_1 when jtag_en_expanded(1) = '1' else (others => '0');
    jtag_dout_2_masked <= jtag_dout_2 when jtag_en_expanded(2) = '1' else (others => '0');
    jtag_dout_3_masked <= jtag_dout_3 when jtag_en_expanded(3) = '1' else (others => '0');
    jtag_dout_4_masked <= jtag_dout_4 when jtag_en_expanded(4) = '1' else (others => '0');
    jtag_dout_5_masked <= jtag_dout_5 when jtag_en_expanded(5) = '1' else (others => '0');
    jtag_dout_6_masked <= jtag_dout_6 when jtag_en_expanded(6) = '1' else (others => '0');
    jtag_dout_7_masked <= jtag_dout_7 when jtag_en_expanded(7) = '1' else (others => '0');
    --
    jtag_en <= jtag_en_int;
    jtag_din <= jtag_din_int;
    jtag_addr <= jtag_addr_int;
    jtag_clk <= jtag_clk_int;
    jtag_we <= jtag_we_int;
    picoblaze_reset <= picoblaze_reset_int;
    --        
  end generate jtag_loader_gen;
--
end Behavioral;
--
--
------------------------------------------------------------------------------------
--
-- END OF FILE cli.vhd
--
------------------------------------------------------------------------------------
