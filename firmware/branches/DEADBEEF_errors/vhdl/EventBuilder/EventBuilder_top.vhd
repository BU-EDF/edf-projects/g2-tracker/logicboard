-------------------------------------------------------------------------------
-- g-2 tracker logic board event builder
-- Dan Gastler
-- Merge and write TDC events
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;

-- for monitoring and controlling of the event builder
use work.EventBuilder_IO.all;
use work.TDC_package.all;
use work.C5_package.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity event_builder is
  generic (
    TDC_COUNT : integer := 4);
  port (

    C5_stream : in std_logic;
    clk40     : in std_logic;
    reset40   : in std_logic;

    clk125     : in std_logic;
    reset125   : in std_logic;
    TDC_serial : in std_logic_vector(3 downto 0);
    TDC_spy    : out std_logic_vector(3 downto 0);
    Firmware_v : in std_logic_vector(31 downto 0);
    control    : in EventBuilder_Control_t;

    data_out : out std_logic;

    monitoring : out EventBuilder_Monitor_t;
    new_spill  : out std_logic;         -- for test pulse sync
    spill_type : out std_logic_vector(3 downto 0)

    );

end event_builder;

architecture arch of event_builder is

  -----------------------------------------------------------------------------
  -- components
  -----------------------------------------------------------------------------

  -- TDC CDR and FIFO
  component TDC_CDR_FIFO is
    port (
      clk125        : in  std_logic;
      reset         : in  std_logic;
      enable        : in  std_logic;
      run_enable    : in  std_logic;
      serial        : in  std_logic;
      locked        : out std_logic;
      spy           : out std_logic;
      header_valid  : out std_logic;
      header_info   : out TDC_header_info_t;
      data          : out std_logic_vector(33 downto 0);
      data_valid    : out std_logic;
      full          : out std_logic;
      empty         : out std_logic;
      read_strobe   : in  std_logic;
      debug_monitor : out TDC_debug_monitor_t;
      debug_control : in  TDC_debug_control_t;
      counters      : out TDC_counters_t);
  end component TDC_CDR_FIFO;
  -- C5 CDR and FIFO
  component C5_CDR_FIFO
    port (
      clk40           : in  std_logic;
      C5_raw          : in  std_logic;
      clk125          : in  std_logic;
      reset           : in  std_logic;
      run_enable      : in  std_logic;
      new_spill       : out std_logic;
      spill_type      : out std_logic_vector(3 downto 0);
      spill_rd        : in  std_logic;
      spill_data      : out std_logic_vector(71 downto 0);
      fifo_full       : out std_logic;
      fifo_empty      : out std_logic;
      fifo_data_valid : out std_logic;
      counters        : out c5_counters_t);
  end component;

  component spill_8b10b is
    port (
      rst         : in  std_logic;
      rd_clk      : in  std_logic;
      wr_clk      : in  std_logic;
      din         : in  std_logic_vector(35 downto 0);
      wr_en       : in  std_logic;
      rd_en       : in  std_logic;
      dout        : out std_logic_vector(8 downto 0);
      full        : out std_logic;
      almost_full : out std_logic;
      empty       : out std_logic;
      valid       : out std_logic);
  end component spill_8b10b;


  -- 8b10b encoder
  component encode_8b10b_top
    generic (
      C_HAS_CE : integer;
      C_HAS_ND : integer);
    port (
      CLK          : in  std_logic                    := '0';
      DIN          : in  std_logic_vector(7 downto 0) := (others => '0');
      KIN          : in  std_logic                    := '0';
      DOUT         : out std_logic_vector(9 downto 0);
      CE           : in  std_logic                    := '0';
      FORCE_CODE   : in  std_logic                    := '0';
      FORCE_DISP   : in  std_logic                    := '0';
      DISP_IN      : in  std_logic                    := '0';
      DISP_OUT     : out std_logic;
      ND           : out std_logic                    := '0';
      KERR         : out std_logic                    := '0';
      CLK_B        : in  std_logic                    := '0';
      DIN_B        : in  std_logic_vector(7 downto 0) := (others => '0');
      KIN_B        : in  std_logic                    := '0';
      DOUT_B       : out std_logic_vector(9 downto 0);
      CE_B         : in  std_logic                    := '0';
      FORCE_CODE_B : in  std_logic                    := '0';
      FORCE_DISP_B : in  std_logic                    := '0';
      DISP_IN_B    : in  std_logic                    := '0';
      DISP_OUT_B   : out std_logic;
      ND_B         : out std_logic                    := '0';
      KERR_B       : out std_logic                    := '0');
  end component;

  component Debug_Output_FIFO is
    port (
      clk   : IN  STD_LOGIC;
      rst   : IN  STD_LOGIC;
      din   : IN  STD_LOGIC_VECTOR(8 DOWNTO 0);
      wr_en : IN  STD_LOGIC;
      rd_en : IN  STD_LOGIC;
      dout  : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
      full  : OUT STD_LOGIC;
      empty : OUT STD_LOGIC;
      valid : OUT STD_LOGIC);
  end component Debug_Output_FIFO;

  component counter is
    generic (
      roll_over   : std_logic;
      end_value   : std_logic_vector;
      start_value : std_logic_vector;
      width       : integer);
    port (
      clk    : in  std_logic;
      reset  : in  std_logic;
      event  : in  std_logic;
      count  : out unsigned(width-1 downto 0);
      at_max : out std_logic);
  end component counter;
  
  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------
  constant OUTPUT_IDLE  : std_logic_vector(8 downto 0) := '1'&x"BC";
  constant OUTPUT_START : std_logic_vector(8 downto 0) := '1'&x"3C";
  constant OUTPUT_END   : std_logic_vector(8 downto 0) := '1'&x"5C";

  -----------------------------------------------------------------------------
  -- signals
  -----------------------------------------------------------------------------
--  signal reset_local : std_logic_vector(3 downto 0) := x"F";
  signal control_local : EventBuilder_Control_t;
  signal monitor_local : EventBuilder_Monitor_t;
  
  signal C5_fifo_read       : std_logic := '0';
  signal C5_fifo_data       : std_logic_vector(71 downto 0);
  signal C5_fifo_data_valid : std_logic;
  signal C5_fifo_full       : std_logic;
  signal C5_fifo_empty      : std_logic;
  signal C5_full_spill      : std_logic;
  signal C5_spill_number    : std_logic_vector(23 downto 0);
  signal C5_spill_time      : std_logic_vector(43 downto 0);
  signal C5_spill_type      : std_logic_vector(2 downto 0);


  type TDC_data_array_t is array (0 to 3) of std_logic_vector(33 downto 0);
  type TDC_Size_t is array (0 to 3) of unsigned(14 downto 0);
  type TDC_debug_monitor_array_t is array (0 to 3) of TDC_debug_monitor_t;
  signal debug_TDC_monitor : TDC_debug_monitor_array_t := ( others => DEFAULT_TDC_DEBUG_MONITOR);
  type TDC_debug_control_array_t is array (0 to 3) of TDC_debug_control_t;
  signal debug_TDC_control : TDC_debug_control_array_t := ( others => (others => "000"));
  type TDC_counters_array_t is array (0 to 3) of TDC_counters_t;
  signal TDC_monitor_counters : TDC_counters_array_t := (others => (others => (others => '0')));

  type TDC_header_info_array_t is array (0 to 3) of TDC_header_info_t;
  signal TDC_header         : TDC_header_info_array_t := (others => ((others => '0'),(others => '0'),(others => '0'),'0',(others => '0'),'0'));
  signal TDC_header_valid : std_logic_vector(0 to 3) := x"0";
  signal TDC_has_header     : std_logic_vector(3 downto 0) := x"0";
  signal TDC_data_valid     : std_logic_vector(3 downto 0);

  
  signal TDC_locked      : std_logic_vector(3 downto 0);
  signal TDC_data        : TDC_data_array_t;
  signal TDC_spill_size  : TDC_size_t := (others => (others => '0'));
  signal TDC_spill_size_error : std_logic_vector(3 downto 0) := x"0";
  signal TDC_fatal_error : std_logic_vector(3 downto 0) := x"0";
  signal TDC_empty       : std_logic_vector(3 downto 0);
  signal TDC_full        : std_logic_vector(3 downto 0);
  signal TDC_has_trailer : std_logic_vector(3 downto 0) := x"0";
  signal TDC_read        : std_logic_vector(3 downto 0);

  signal C5_valid : std_logic := '0';

  signal TDC_enable         : std_logic_vector(3 downto 0) := x"0";
  signal run_enable : std_logic := '0';

  signal Active_TDC_stream  : integer range 0 to 4         := 4;
  signal TDC_timing_error   : std_logic_vector(3 downto 0) := x"0";
  type time_stamp_array_t is array (0 to 3) of std_logic_vector(43 downto 0);
  signal spill_time_diff    : time_stamp_array_t := (others => (others => '0'));
  
  signal total_spill_size    : unsigned(15 downto 0);
  signal tdc_size_error      : std_logic_vector(3 downto 0) := x"0";
  signal fatal_error         : std_logic := '0';
  signal sending_to_TRM      : std_logic                     := '0';
--  type Send_State_t is (SEND_IDLE,CHECK_NEW_DATA,BUILD_HEADER, SEND_HEADER, SEND_TDC_DATA, SEND_END,SEND_CRC, SEND_TRAILER);
--  signal send_state          : Send_State_t                  := SEND_IDLE;
  constant SEND_IDLE         : std_logic_vector(7 downto 0) := x"01";
  constant CHECK_NEW_DATA    : std_logic_vector(7 downto 0) := x"02";
  constant BUILD_HEADER      : std_logic_vector(7 downto 0) := x"04";
  constant SEND_HEADER       : std_logic_vector(7 downto 0) := x"08";
  constant SEND_TDC_DATA     : std_logic_vector(7 downto 0) := x"10";
  constant SEND_END          : std_logic_vector(7 downto 0) := x"20";
  constant SEND_CRC          : std_logic_vector(7 downto 0) := x"40";
  constant SEND_TRAILER      : std_logic_vector(7 downto 0) := x"80";
  signal send_state          : std_logic_vector(7 downto 0) := SEND_IDLE;

  signal TDC_Size            : TDC_Size_t                    := (others => (others => '0'));
  signal TDC_Size_left       : TDC_Size_t                    := (others => (others => '0'));
  signal spill_numbers_match : std_logic_vector(3 downto 0)  := x"0";
  signal LB_data_word_count  : unsigned(31 downto 0)         := x"00000000";
  signal LB_data_word        : std_logic_vector(31 downto 0) := x"00000000";
  signal LB_data_word_k_mask : std_logic_vector(3 downto 0)  := x"0";
  signal send_header_counter : integer range 0 to 10          := 0;

  --8b10b fifo 32 +4 bit down to 9 bit (1+8)
  signal spill_8b10b_wr      : std_logic                     := '0';
  signal spill_8b10b_in      : std_logic_vector(35 downto 0) := x"000000000";
  signal fifo_8b_rd          : std_logic                     := '0';
  signal fifo_8b_out         : std_logic_vector(8 downto 0);
  signal fifo_8b_full        : std_logic := '1';
  signal fifo_8b_almost_full : std_logic := '1';
  signal fifo_8b_empty       : std_logic := '1';
  signal fifo_8b_valid       : std_logic := '0';
  signal output_word_overflow : std_logic := '0';

  signal in_8b              : std_logic_vector(8 downto 0) := OUTPUT_IDLE;
  signal encode_8b_to_10b   : std_logic                    := '0';
  signal out_10b            : std_logic_vector(9 downto 0);
  signal out_10b_shift      : std_logic_vector(9 downto 0);
  signal clk_en_10b_sr      : std_logic_vector(9 downto 0) := "00"&x"01";
  signal valid_out_10b : std_logic := '0';
  signal missed_10b_char : std_logic := '0';


  -- debugging output data signals
  type LB_8b10b_t is array (0 to 6) of std_logic_vector(8 downto 0);
  signal debug_8b : LB_8b10b_t := (others => (others => '0'));
  signal debug_encode_buffer : std_logic_vector(6 downto 0) := (others => '0');
  signal debug_wr : std_logic := '0';
  signal debug_reset : std_logic := '1';
  signal debug_fifo_full : std_logic;
  signal debug_fifo_active : std_logic := '0';
  
  signal events_out : std_logic_vector(31 downto 0) := x"00000000";
  signal events_in  : std_logic_vector(31 downto 0) := x"00000000";

  -- additional counters for TDC streams
  signal tdc_missing_trailer_bit : std_logic_vector(3 downto 0) := x"0";
  type unsigned_array_t is array (0 to 3) of unsigned(31 downto 0);
  signal TDC_missing_trailer_bit_counter : unsigned_array_t := (others =>( others => '0'));

  signal counter0l_overflow : std_logic := '0';
  
begin  -- arch

  -- control/monitoring buffer
  monitor_control_buffer: process (clk125) is
  begin  -- process monitor_control_buffer
    if clk125'event and clk125 = '1' then  -- rising clock edge
      control_local <= control;
      monitoring <= monitor_local;
    end if;
  end process monitor_control_buffer;

  -- monitoring
  monitoring_proc: process (clk125) is
  begin  -- process monitoring_proc
    if clk125'event and clk125 = '1' then  -- rising clock edge
      monitor_local.sent_spill_count <= events_out;
      monitor_local.new_spill_count  <= events_in;

      monitor_local.C5_fifo_full  <= C5_fifo_full;
      monitor_local.C5_fifo_empty <= C5_fifo_empty;
      
      monitor_local.TDC_fifo_full   <= TDC_full;
      monitor_local.TDC_fifo_empty  <= TDC_empty;
      monitor_local.TDC_locked      <= TDC_locked;

      monitor_local.EB_reset <=  reset125 ;

    end if;
  end process monitoring_proc;


  -- control
  TDC_enable_sync : process (clk125) is
  begin  -- process TDC_enable_sync
    if clk125'event and clk125 = '1' then  -- rising clock edge
      TDC_enable <= control_local.TDC_enable_mask;
      run_enable <= control_local.run_enable;
    end if;
  end process TDC_enable_sync;



  --loop over TDC components
  TDC_CDR : for iTDC in 0 to 3 generate
    debug_TDC_control(iTDC).cdr_override_setting <= control_local.TDC_CDR_override_setting(iTDC);

    monitor_local.TDC_CDR_edges(iTDC) <= debug_TDC_monitor(iTDC).cdr_edges;
    monitor_local.TDC_CDR_histogram(iTDC) <= debug_TDC_monitor(iTDC).cdr_histogram;
    monitor_local.TDC_10b(iTDC) <= debug_TDC_monitor(iTDC).current_10b_char(9 downto 0);
    monitor_local.TDC_8b(iTDC) <= debug_TDC_monitor(iTDC).current_8b_char(8 downto 0);
    
    TDC_CDR_FIFO_1: entity work.TDC_CDR_FIFO
      port map (
        clk125        => clk125,
        reset         =>  reset125 ,
        enable        => TDC_enable(iTDC),
        run_enable    => run_enable,
        serial        => TDC_serial(iTDC),
        locked        => TDC_locked(iTDC),
        spy           => TDC_spy(iTDC),
        header_valid  => TDC_header_valid(iTDC),
        header_info   => TDC_header(iTDC),
        data          => TDC_data(iTDC),
        data_valid    => TDC_data_valid(iTDC),
        full          => TDC_full(iTDC),
        empty         => TDC_empty(iTDC),
        read_strobe   => TDC_read(iTDC),
        debug_monitor => debug_TDC_monitor(iTDC),
        debug_control => debug_TDC_control(iTDC),
        counters      => monitor_local.TDC_Counters(iTDC));    
  end generate TDC_CDR;

  -- C5 CDR & FIFO

  C5_CDR_FIFO_1 : C5_CDR_FIFO
    port map (
      clk40           => clk40,
      C5_raw          => C5_stream,
      clk125          => clk125,
      reset           => reset40 ,
      run_enable      => run_enable,
      new_spill       => new_spill,
      spill_type      => spill_type,
      spill_rd        => C5_fifo_read,
      spill_data      => C5_fifo_data,
      fifo_full       => C5_fifo_full,
      fifo_empty      => C5_fifo_empty,
      fifo_data_valid => C5_fifo_data_valid,
      counters        => monitor_local.C5_CMD_counters);



  -----------------------------------------------------------------------------
  -- Read out each of the TDCs to fill up their headers
  -----------------------------------------------------------------------------
  monitor_local.TDC_has_header <= TDC_has_header;
  TDC_Readouts : for iTDC in 0 to 3 generate
    TDC_Readout : process (clk125,  reset125 )
    begin  -- process TDC_Readout
      if  reset125  = '1' then               -- asynchronous reset (active high)
        monitor_local.TDC_fifo_data(iTDC)       <= "00"&x"00000000";
        monitor_local.TDC_fifo_data_valid(iTDC) <= '0';
        TDC_has_header(iTDC) <= '0';
        TDC_read(iTDC) <= '0';
        TDC_Size(iTDC) <= (others => '0');
        TDC_Size_left(iTDC) <= (others => '0');
      elsif clk125'event and clk125 = '1' then  -- rising clock edge
        TDC_read(iTDC) <= '0';            -- default is not reading from the FIFO
        --Readout the header of a TDC so that the Event_Builder can build
        if TDC_enable(iTDC) = '1' then
          if TDC_locked(iTDC) = '1' then           
            if TDC_has_header(iTDC) = '0' and TDC_header_valid(iTDC) = '1' then
              TDC_has_header(iTDC) <= '1';
              ------------------------------------------
              -- cached copies of the TDC_Sizes to use for counting down during
              -- sending
              TDC_Size_left(iTDC) <= TDC_header(iTDC).size;
              TDC_Size(iTDC) <= TDC_header(iTDC).size;
            elsif iTDC = Active_TDC_stream then
              -- we grabbed new data from the fifo, so lower the size left
              if TDC_data_valid(iTDC) = '1' then
                TDC_Size_left(iTDC) <= TDC_Size_left(iTDC) - 1;
              end if;
              
              if fifo_8b_almost_full = '0' and TDC_data_valid(iTDC) = '0' and TDC_read(iTDC) = '0'then
                TDC_read(iTDC) <= '1';
              end if;
            elsif send_state = SEND_TRAILER then            
              TDC_has_header(iTDC) <= '0';
            end if;
          else
            TDC_read(iTDC) <= '0';
            TDC_has_header(iTDC) <= '0';
            TDC_Size_left(iTDC) <= (others => '0');
            TDC_Size(iTDC) <= (others => '0');
          end if;
        else
          TDC_Size_left(iTDC) <= (others => '0');
          TDC_Size(iTDC) <= (others => '0');
          -- slow control readout
          if TDC_empty(iTDC) = '0' and control_local.TDC_FIFO_rd(iTDC) = '1' then
            TDC_read(iTDC) <= '1';
            
          end if;
          if TDC_data_valid(iTDC) = '1' then
            monitor_local.TDC_fifo_data(iTDC)       <= TDC_data(iTDC);
            monitor_local.TDC_fifo_data_valid(iTDC) <= '1';
          end if;          
        end if;
      end if;
    end process TDC_Readout;
  end generate TDC_Readouts;

-------------------------------------------------------------------------------
-- Read out the next C5 header
-------------------------------------------------------------------------------
  C5_readout : process (clk125,  reset125 )
  begin  -- process C5_readout
    if  reset125  = '1' then                 -- asynchronous reset (active high)
      events_in <= x"00000000";
      C5_spill_time <= x"00000000000";
      C5_spill_number <= x"000000";
      C5_spill_type <= "000";
      C5_valid <= '0';
      C5_fifo_read <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      --default the C5fifo reader to zero
      C5_fifo_read <= '0';
      if C5_valid = '0' then
        -- read from fifo if not empty
        if C5_fifo_empty = '0' then
          C5_fifo_read <= '1';
        end if;

        if C5_fifo_data_valid = '1' then
          C5_spill_time   <= C5_fifo_data(43 downto 0);
          C5_spill_number <= C5_fifo_data(67 downto 44);
          C5_spill_type   <= C5_fifo_data(70 downto 68);
          C5_valid        <= '1';
          -- update processed events_in
          events_in       <= std_logic_vector(unsigned(events_in) + 1);
        end if;
      else
        if sending_to_TRM = '1' then
          C5_valid <= '0';
        end if;
      end if;
    end if;
  end process C5_readout;



  -----------------------------------------------------------------------------
  -- State machine that waits for a new event and then adds it to the 8b10b sender
  -----------------------------------------------------------------------------
  monitor_local.EB_state <= std_logic_vector(send_state);

  spill_checking : process (clk125,  reset125 )
  begin  -- process spill_checking
    if  reset125  = '1' then                 -- asynchronous reset (active high)
      events_out        <= x"00000000";

      Active_TDC_stream <= 4; -- 4 = no TDC since TDC number goes 0 to 3
      send_state <= SEND_IDLE;
      sending_to_TRM    <= '0';
      send_header_counter <= 0;
      
      
      spill_8b10b_wr <= '0';
      LB_data_word_k_mask <= x"0";
      LB_data_word <= x"00000000";
      LB_data_word_count <= x"00000000";

      total_spill_size <= x"0000";
      fatal_error <= '0';
      spill_numbers_match <= x"0";
      TDC_size_error <= x"0";      
      
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      spill_8b10b_wr      <= '0';       -- default is not to be writing to the
                                    -- FIFO
      LB_data_word_k_mask <= x"0";

      TDC_missing_trailer_bit <= x"0"; -- missing trailer bit flag
      
      -- only add new words when the fifo has space
--      if fifo_8b_almost_full = '0' then
      if fifo_8b_full = '0' then
        case send_state is
          -----------------------------------------------------------------------
          -- IDLE state: wait for new event
          when SEND_IDLE =>
            if C5_valid = '1' and  -- The C5 header has been loaded            
              (TDC_has_header and TDC_enable) = TDC_enable then  -- All TDC data has arrived

              -- change our state so we start sending data
              send_state <= CHECK_NEW_DATA;
            else
              send_state <= SEND_IDLE;
            end if;
          -----------------------------------------------------------------------
          -- CHECK_NEW_DATA state: latch the tdc attributes
          when CHECK_NEW_DATA =>
            ------------------------------------------
            -- fatal error (we only care about this on active TDCs)
            fatal_error <= or_reduce(TDC_header(0).fatal_error & TDC_header(1).fatal_error & TDC_header(2).fatal_error & TDC_header(3).fatal_error and TDC_enable); 

            for iTDC in 0 to 3 loop              
              if TDC_enable(iTDC) = '1' then
                -------------------------------------------------------
                -- check spill numbers
                spill_numbers_match(iTDC) <= '0';
                if C5_spill_number = TDC_header(iTDC).spill_number then
                  spill_numbers_match(iTDC) <= '1';
                end if;

                -------------------------------------------------------
                -- Find the absolute value of the difference of the C5's
                -- timestamp and each TDC's timestamp
                if C5_spill_time > TDC_header(iTDC).spill_time then
                  spill_time_diff(iTDC) <= std_logic_vector(unsigned(C5_spill_time) - unsigned(TDC_header(iTDC).spill_time));
                else
                  spill_time_diff(iTDC) <= std_logic_vector(unsigned(TDC_header(iTDC).spill_time) - unsigned(C5_spill_time));
                end if;

                -------------------------------------------------------
                -- Check if the received size is the same as the header size                
                tdc_size_error(iTDC) <= '0';
                if TDC_header(iTDC).size /= TDC_header(iTDC).debug_expected_size then
                  tdc_size_error(iTDC) <= '1';                  
                end if;

                
                
              else
                tdc_size_error(iTDC)      <= '0';
                spill_numbers_match(iTDC) <= '1';       -- since we use the not
                                                        -- of this as an error,
                                                        -- we put this to 1
                                                        -- since this tdc is disabled
                spill_time_diff(iTDC) <= (others => '0'); --say the time is in
                                                          --agreement to skip
                                                          --the error

              end if;
            end loop;  -- iTDC              

            send_state <= BUILD_HEADER;
          -----------------------------------------------------------------------
          -- BUILD_HEADER state: put together the final values we need to send
          -- the data
          when BUILD_HEADER =>
          
            ------------------------------------------
            -- compute numbers used in the header
            -- compute the total size of the data (header + trailer + TDC(i))
            total_spill_size <= x"000A" + x"0003"
                                + TDC_size(0)
                                + TDC_size(1)
                                + TDC_size(2)
                                + TDC_size(3);

     
            -----------------------------------------
            -- note any timing errors
            for iTDC in 0 to 3 loop
              TDC_timing_error(iTDC) <= '0';
              if spill_time_diff(iTDC) > (x"000" & control_local.time_skew_max) then 
                TDC_timing_error(iTDC) <= '1';
              end if;
            end loop;              
           
            ------------------------------------------
            -- start the data flow 
            send_header_counter <= 0;
            sending_to_TRM      <= '1';
            send_state          <= SEND_HEADER;

          -----------------------------------------------------------------------
          -- SEND_HEADER state: send the LogicBoard header
          when SEND_HEADER =>
            spill_8b10b_wr      <= '1';
            send_header_counter <= send_header_counter + 1;
            LB_data_word_count  <= LB_data_word_count + 1;
            case send_header_counter is
              when 0 =>
                -- send start of data and three zero bytes
                LB_data_word_k_mask <= "1000";
                LB_data_word        <= OUTPUT_START(7 downto 0) & x"000000";
                LB_data_word_count  <= x"00000004"; -- stores the initial 3C
                                                    -- word (this word), the
                                                    -- trailing count word,
                                                    -- trailing CRC word and
                                                    -- trailing end word
              when 1 =>                 -- send total size
                LB_data_word(31 downto 16) <= x"0000";
                LB_data_word(15 downto 0)  <= std_logic_vector(total_spill_size);
              when 2 =>
                LB_data_word <= Firmware_v;
              when 3 =>  -- send the spill number of the C5 system
                LB_data_word <= x"00" & C5_spill_number;
              when 4 =>                 -- sent hit time bits 43 downto 12
                LB_data_word <= C5_spill_time(43 downto 12);
              when 5 =>                 -- send spill summary and LSbs timing
                -- lsbs of the time
                LB_data_word(11 downto 0)  <= C5_spill_time(11 downto 0);
                -- TDC enable mask
                LB_data_word(15 downto 12) <= TDC_enable;
                -- In sync
                LB_data_word(19 downto 16) <= not spill_numbers_match;                
                -- Error
                if (TDC_enable and spill_numbers_match) = TDC_enable then
                  LB_data_word(20) <= '0';
                else
                  LB_data_word(20) <= '1';
                end if;
                -- Fatal error
                LB_data_word(21)           <= fatal_error;
                -- spill type
                LB_data_word(24 downto 22) <= C5_spill_type;
                LB_data_word(27 downto 25) <= "000";
                LB_data_word(31 downto 28) <= TDC_timing_error;

              when 6 to 9 =>            --TDC summaries
                -- enabled
                LB_data_word(31)          <= TDC_enable(send_header_counter - 6);
                -- out of sync
                LB_data_word(30)          <= TDC_enable(send_header_counter - 6) and (not spill_numbers_match(send_header_counter - 6));                
                LB_data_word(29)          <= TDC_enable(send_header_counter - 6) and TDC_size_error(send_header_counter - 6);
                LB_data_word(28)          <= TDC_enable(send_header_counter - 6) and TDC_header(send_header_counter -6).size_error;
                LB_data_word(27 downto 15) <= "0"&x"000";
                LB_data_word(14 downto 0) <= std_logic_vector(TDC_Size(send_header_counter - 6)(14 downto 0));

                if send_header_counter = 9 then
                  --Move on to next state
                  send_state <= SEND_TDC_DATA;
                  if TDC_Size_left(0) /= x"0000" then
                    Active_TDC_stream <= 0;
                  elsif TDC_Size_left(1) /= x"0000" then
                    Active_TDC_stream <= 1;
                  elsif TDC_Size_left(2) /= x"0000" then
                    Active_TDC_stream <= 2;
                  elsif TDC_Size_left(3) /= x"0000" then
                    Active_TDC_stream <= 3;
                  else
                    Active_TDC_stream <= 4;  -- turn off tdc streaming
                    send_state        <= SEND_TRAILER;
                  end if;
                  
                end if;
              when others => null;
            end case;



          -----------------------------------------------------------------------
          -- SEND_TDC_DATA state: send the data from the TDCs
          when SEND_TDC_DATA =>
            ---------------------------------------------
            -- process new data from this TDC's fifo
            if TDC_data_valid(Active_TDC_stream) = '1' then
              spill_8b10b_wr     <= '1';
              LB_data_word       <= TDC_data(Active_TDC_stream)(31 downto 0);
              LB_data_word_count <= LB_data_word_count + 1;
--              TDC_Size_left(Active_TDC_stream) <= TDC_Size_left(Active_TDC_stream) - 1;

              if TDC_Size_left(Active_TDC_stream) = x"0001" then              
                --choose next thing to stream
                if Active_TDC_stream < 1 and TDC_Size_left(1) /= x"0000" then
                  Active_TDC_stream <= 1;
                elsif Active_TDC_stream < 2 and TDC_Size_left(2) /= x"0000" then
                  Active_TDC_stream <= 2;
                elsif Active_TDC_stream < 3 and TDC_Size_left(3) /= x"0000" then
                  Active_TDC_stream <= 3;
                else
                  Active_TDC_stream <= 4;  -- turn off tdc streaming
                  send_state        <= SEND_TRAILER;
                end if;
                
                --check that this was a trailer word
                if TDC_data(Active_TDC_stream)(32) = '0' then
                  TDC_missing_trailer_bit(Active_TDC_stream) <= '1';
                end if;                
              end if;              
            end if;
          when SEND_TRAILER =>
            if fifo_8b_almost_full = '0' then
              spill_8b10b_wr      <= '1';
              LB_data_word_k_mask <= x"0";
              LB_data_word  <= std_logic_vector(LB_data_word_count);
              LB_data_word_count  <= x"00000000";
              send_state          <= SEND_CRC;
            end if;
          when SEND_CRC =>
            if fifo_8b_almost_full = '0' then
              spill_8b10b_wr      <= '1';
              LB_data_word_k_mask <= x"0";
              LB_data_word        <= x"00000000";
              send_state          <= SEND_END;
            end if;
          when SEND_END =>
            if fifo_8b_almost_full = '0' then
              spill_8b10b_wr      <= '1';
              LB_data_word_k_mask <= x"1";
              LB_data_word        <= x"000000" & OUTPUT_END(7 downto 0);
              sending_to_TRM      <= '0';
              send_state          <= SEND_IDLE;
              -- update processed events_out
              events_out          <= std_logic_vector(unsigned(events_out) + 1);
            end if;
          when others => null;
        end case;
      end if;
    end if;
  end process spill_checking;


  -- 8b10b streamer @125Mhz
  spill_8b10b_in(35 downto 27) <= LB_data_word_k_mask(3) & LB_data_word(31 downto 24);
  spill_8b10b_in(26 downto 18) <= LB_data_word_k_mask(2) & LB_data_word(23 downto 16);
  spill_8b10b_in(17 downto 9)  <= LB_data_word_k_mask(1) & LB_data_word(15 downto 8);
  spill_8b10b_in(8 downto 0)   <= LB_data_word_k_mask(0) & LB_data_word(7 downto 0);

  spill_8b10b_1 : spill_8b10b
    port map (
      rst         =>  reset125 ,
      rd_clk      => clk125,
      wr_clk      => clk125,
      din         => spill_8b10b_in,
      wr_en       => spill_8b10b_wr,
      rd_en       => fifo_8b_rd,
      dout        => fifo_8b_out,
      full        => fifo_8b_full,
      almost_full => fifo_8b_almost_full,
      empty       => fifo_8b_empty,
      valid       => fifo_8b_valid);


  clk_en_10b: process (clk125, reset125) is
  begin  -- process clk_en_10b
    if reset125 = '1' then              -- asynchronous reset (active high)
      clk_en_10b_sr <= "00"&x"01";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      -- shift the bits to the left and around
      clk_en_10b_sr <= clk_en_10b_sr(8 downto 0) & clk_en_10b_sr(9);
    end if;
  end process clk_en_10b;

  generate_8b_stream: process (clk125, reset125) is
  begin  -- process generate_8b_stream
    if reset125 = '1' then              -- asynchronous reset (active high)
      fifo_8b_rd <= '0';
      encode_8b_to_10b <= '0';
      in_8b <= OUTPUT_IDLE;      
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      -- per clock pulse resets
      fifo_8b_rd <= '0';
      encode_8b_to_10b <= '0';
      in_8b <= OUTPUT_IDLE;

      -----------------------------------
      -- prep for next 8b word conversion
      if clk_en_10b_sr(0) = '1' then
        --load next byte if it is available
        if fifo_8b_empty = '0' then
          fifo_8b_rd <= '1';
        end if;
      -----------------------------------
      -- Convert a new word from 8b to 10b
      elsif clk_en_10b_sr(2) = '1' then
        encode_8b_to_10b <= '1';
        -- assign real data if available, or IDLE pattern if not
        if fifo_8b_valid = '1' then
          in_8b <= fifo_8b_out;
        else
          in_8b <= OUTPUT_IDLE;
        end if;
      end if;
    end if;
  end process generate_8b_stream;

  
  
--  clk_en_10b : process (clk125,  reset125 )
--  begin  -- process clk_en_10b
--    if  reset125  = '1' then                 -- asynchronous reset125 (active high)
--      clk_en_10b_counter <= 0;
--    elsif clk125'event and clk125 = '1' then  -- rising clock edge      
--      if clk_en_10b_counter = 9 then
--        clk_en_10b_counter <= 0;        
--      else
--        clk_en_10b_counter <= clk_en_10b_counter + 1;
--      end if;
--    end if;
--  end process clk_en_10b;
--
--  Generate_bytes : process (clk125,  reset125 )
--  begin  -- process Generate_bytes
--    if  reset125  = '1' then                 -- asynchronous reset (active high)
--      fifo_8b_rd       <= '0';
--      in_8b            <= OUTPUT_IDLE;
--      encode_8b_to_10b <= '0';
--    elsif clk125'event and clk125 = '1' then  -- rising clock edge     
--      if clk_en_10b_counter = 7 then
--        --load next byte if it is available
--        if fifo_8b_empty = '0' then
--          fifo_8b_rd <= '1';
--        end if;
--      elsif clk_en_10b_counter = 9 then
--        encode_8b_to_10b <= '1';
--        -- assign real data if available, or IDLE pattern if not
--        if fifo_8b_valid = '1' then
--          in_8b <= fifo_8b_out;
--        else
--          in_8b <= OUTPUT_IDLE;
--        end if;
--      else
--        -- reset stream read to zero and 8b10b byte to IDLE pattern
--        fifo_8b_rd       <= '0';
--        encode_8b_to_10b <= '0';
--      end if;
--    end if;
--  end process Generate_bytes;


  encode_8b10b_top_1 : encode_8b10b_top
    generic map (
      C_HAS_CE => 1,
      C_HAS_ND => 1)
    port map (
      CLK  => clk125,
      DIN  => in_8b(7 downto 0),
      KIN  => in_8b(8),
      DOUT => out_10b,
      CE   => encode_8b_to_10b,
      ND   => valid_out_10b
      );
  
  serialize_8b10b : process (clk125,  reset125 )
  begin  -- process serialize_8b10b
    if  reset125  = '1' then                 -- asynchronous reset (active high)
      out_10b_shift <= "00"&x"00";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      -- serialize out 10b data
      data_out      <= out_10b_shift(0);
      out_10b_shift <= '0' & out_10b_shift(9 downto 1);

      -----------------------------------
      -- Capture and clock out new 10b word
      missed_10b_char <= '0';
      if clk_en_10b_sr(4) = '1' then
        if valid_out_10b  = '1' then
          out_10b_shift <= out_10b;
        else
          missed_10b_char <= '1';
        end if;
      end if;

    end if;
  end process serialize_8b10b;


  Debug_Output_FIFO_control: process (clk125,  reset125 ) is
  begin  -- process Debug_Output_FIFO_control
    if  reset125  = '1' then        -- asynchronous reset (active high)
      debug_8b <= (others => (others => '0'));
      debug_wr <= '0';
      debug_encode_buffer <= (others => '0');
      debug_reset <= '1';
      debug_fifo_active <= '0';      
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      debug_reset <= '0';      
      debug_wr <= '0';
      if encode_8b_to_10b = '1' then
        --buffer the data land latch signals
        debug_8b(0 to 6) <= in_8b & debug_8b(0 to 5);


        -- fill the fifo with data until full
        if debug_fifo_active = '1' then
          if debug_fifo_full = '0' then
            -- stream data into the fifo not full
            debug_wr <= '1';
          else
            -- turn off recording data now that the fifo is full
            debug_fifo_active <= '0';
          end if;
        elsif in_8b = OUTPUT_START then
          -- reset the fifo for the new event and wait several buffered writes
          -- for the FIFO to properly reset
          debug_reset <= '1';
        elsif debug_8b(5) = OUTPUT_START then
          -- Begin taking data for the incoming event
          debug_fifo_active <= '1';                    
        end if;
      end if;
    end if;
  end process Debug_Output_FIFO_control;
  
  Debug_Output_FIFO_1: entity work.Debug_Output_FIFO
    port map (
      clk   => clk125,
      rst   => debug_reset,
      din   => debug_8b(6),
      wr_en => debug_wr,
      rd_en => control_local.LB_FIFO_rd,
      dout  => monitor_local.LB_FIFO_data,
      full  => debug_fifo_full,
      empty => monitor_local.LB_fifo_empty,
      valid => monitor_local.LB_fifo_data_valid);
  monitor_local.LB_fifo_full <= debug_fifo_full;

  

  missing_trailer_counter: process (clk125,  reset125 ) is
  begin  -- process missing_trailer_counter
    if  reset125  = '1' then        -- asynchronous reset (active high)
      
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      for iTDC in 0 to 3 loop
        if TDC_missing_trailer_bit(iTDC) = '1' then
          TDC_missing_trailer_bit_counter(iTDC) <= TDC_missing_trailer_bit_counter(iTDC) + 1;
        end if;
        monitor_local.TDC_missing_trailer_bits(iTDC) <= TDC_missing_trailer_bit_counter(iTDC);
      end loop;  -- iTDC
    end if;
  end process missing_trailer_counter;

  output_8bchar_counter: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => clk125,
      reset  => reset125,
      event  => fifo_8b_valid,
      count  => monitor_local.EB_output_8bchars,
      at_max => open);
  output_10bchar_counter: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => clk125,
      reset  => reset125,
      event  => valid_out_10b,
      count  => monitor_local.EB_output_10bchars,
      at_max => open);
  missed_10bchar_counter: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => clk125,
      reset  => reset125,
      event  => missed_10b_char,
      count  => monitor_local.EB_missed_10bchars,
      at_max => open);

  
  output_word_counter: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => clk125,
      reset  => reset125,
      event  => spill_8b10b_wr,
      count  => monitor_local.EB_output_words,
      at_max => open);

  output_word_overflow <= spill_8b10b_wr and fifo_8b_full;
  output_word_overflow_counter: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => clk125,
      reset  => reset125,
      event  => output_word_overflow,
      count  => monitor_local.EB_output_word_overflows,
      at_max => open);

  output_char_total_counter: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => clk125,
      reset  => reset125,
      event  => encode_8b_to_10b,
      count  => monitor_local.EB_output_chars_total,
      at_max => open);
  
  state_counter0h: entity work.counter
      generic map (
        roll_over   => '0',
        end_value   => x"FFFFFFFF",
        start_value => x"00000000",
        width       => 32)
      port map (
        clk    => clk125,
        reset  => reset125,
        event  => counter0l_overflow,
        count  => monitor_local.EB_state_time_idle(63 downto 32),
        at_max => open);
  state_counter0l: entity work.counter
      generic map (
        roll_over   => '1',
        end_value   => x"FFFFFFFF",
        start_value => x"00000000",
        width       => 32)
      port map (
        clk    => clk125,
        reset  => reset125,
        event  => send_state(0),
        count  => monitor_local.EB_state_time_idle(31 downto 0),
        at_max => counter0l_overflow);    
  state_counters: for iState in 1 to 7 generate
    state_counter: entity work.counter
      generic map (
        roll_over   => '1',
        end_value   => x"FFFFFFFF",
        start_value => x"00000000",
        width       => 32)
      port map (
        clk    => clk125,
        reset  => reset125,
        event  => send_state(iState),
        count  => monitor_local.EB_state_time(iState),
        at_max => open);    
  end generate state_counters;
  
end arch;

