------------------------------------------------------------------------------
-- g-2 tracker logic C5 CDR
-- Dan Gastler
-- CDR for C5 and provide a spill FIFO
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

use work.SpillTypes.all;
use work.C5_package.all;

entity C5_CDR_FIFO is
  
  port (
    clk40        : in  std_logic;
    C5_raw       : in  std_logic;       -- C5_clock

    clk125       : in std_logic;
    reset        : in std_logic;
    run_enable   : in  std_logic;
    
    new_spill    : out std_logic := '0'; -- @clk40
    spill_type   : out std_logic_vector(3 downto 0); -- @clk40
    
    spill_rd : in  std_logic;
    spill_data : out std_logic_vector(71 downto 0);
    fifo_full : out std_logic;
    fifo_empty: out std_logic;
    fifo_data_valid: out std_logic;

    counters : out c5_counters_t -- @clk125
    );

end C5_CDR_FIFO;

architecture arch of C5_CDR_FIFO is

  -- C5 CDR
  component C5_CDR
    port (
      clk_40        : in  std_logic;
      C5_stream     : in  std_logic;
      c5_data       : out std_logic_vector(3 downto 0);
      c5_is_k_char  : out std_logic;
      c5_data_valid : out std_logic);
  end component;
  -- FIFO
  component spill_header_FIFO
    port (
      rd_clk    : IN  STD_LOGIC;
      wr_clk    : IN  STD_LOGIC;
      rst    : IN  STD_LOGIC;
      din    : IN  STD_LOGIC_VECTOR(71 DOWNTO 0);
      wr_en  : IN  STD_LOGIC;
      rd_en  : IN  STD_LOGIC;
      dout   : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
      full   : OUT STD_LOGIC;
      empty  : OUT STD_LOGIC;
      valid  : out STD_LOGIC);
  end component;

  component counter_cross_domain is
    generic (
      roll_over   : std_logic;
      end_value   : std_logic_vector;
      start_value : std_logic_vector;
      width       : integer);
    port (
      clk    : in  std_logic;
      reset  : in  std_logic;
      event  : in  std_logic;
      clkout : in std_logic;
      count  : out unsigned(width-1 downto 0);
      at_max : out std_logic);
  end component counter_cross_domain;

  component pass_std_logic_vector is
    generic (
      DATA_WIDTH : integer;
      RESET_VAL  : std_logic_vector);
    port (
      clk_in   : in  std_logic;
      clk_out  : in  std_logic;
      reset    : in  std_logic;
      pass_in  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
      pass_out : out std_logic_vector(DATA_WIDTH-1 downto 0));
  end component pass_std_logic_vector;

  component pacd is
    port (
      iPulseA : IN  std_logic;
      iClkA   : IN  std_logic;
      iRSTAn  : IN  std_logic;
      iClkB   : IN  std_logic;
      iRSTBn  : IN  std_logic;
      oPulseB : OUT std_logic);
  end component pacd;
  -----------------------------------------------------------------------------
  -- Constants


  -- Commands
  constant DAQ_CMD_NEW_SPILL : std_logic_vector(3 downto 0) := x"C";
  constant DAQ_CMD_SPILL_RESET : std_logic_vector(3 downto 0) := x"F";
  constant DAQ_CMD_TIME_RESET : std_logic_vector(3 downto 0) := x"E";
  constant DAQ_CMD_SET_NORMAL : std_logic_vector(3 downto 0) := x"D";
  constant DAQ_CMD_SET_CAL1 : std_logic_vector(3 downto 0) := x"B";
  constant DAQ_CMD_SET_CAL2 : std_logic_vector(3 downto 0) := x"A";
  constant DAQ_CMD_SET_CAL3 : std_logic_vector(3 downto 0) := x"9";

  -- Initial values
  constant FIRST_SPILL_NUMBER : std_logic_vector(23 downto 0) := x"000001";
  constant DETECTOR_TIME_ZERO : std_logic_vector(43 downto 0) := x"00000000000";

  
  -----------------------------------------------------------------------------
  -- signals
  signal local_reset : std_logic := '1';
  signal short_reset : std_logic := '0';
  
  signal c5_data : std_logic_vector(3 downto 0);
  signal c5_is_k_char : std_logic;
  signal c5_data_valid : std_logic := '0';

  signal local_new_spill : std_logic := '0';
  signal local_spill_type : std_logic_vector(3 downto 0) := SPILL_NORMAL;
  signal spill_number : std_logic_vector(23 downto 0) := FIRST_SPILL_NUMBER;
  signal spill_time : std_logic_vector(43 downto 0) := DETECTOR_TIME_ZERO;

  signal spill_in : std_logic_vector(71 downto 0);
  signal fifo_full_local : std_logic := '1';
  signal spill_fifo_wr : std_logic := '0';

  signal c5_event : std_logic_vector(15 downto 0) := x"0000";
  
begin  -- arch
  
  -----------------------------------------------------------------------------
  -- C5 data path recovery
  -----------------------------------------------------------------------------
  C5_CDR_1: C5_CDR
    port map (
      clk_40        => clk40,
      C5_stream     => C5_raw,
      c5_data       => c5_data,
      c5_is_k_char  => c5_is_k_char,
      c5_data_valid => c5_data_valid);

  
  -----------------------------------------------------------------------------
  -- Detector time control
  -----------------------------------------------------------------------------
  DetectorTime: process (clk40, reset) is
  begin  -- process DetectorTime
    if reset = '1' then                 -- asynchronous reset (active high)
      spill_time <=  DETECTOR_TIME_ZERO;
    elsif clk40'event and clk40 = '1' then  -- rising clock edge
      --increment detector time
      spill_time <= std_logic_vector(unsigned(spill_time) + 1);
      -- Look for DAQ_CMD_TIME_RESET
      if c5_data_valid = '1' then
        if c5_is_k_char = '0' and c5_data(3 downto 0) = DAQ_CMD_TIME_RESET then
          spill_time <=  DETECTOR_TIME_ZERO;          
        end if;
      end if;
    end if;
  end process DetectorTime;


  -----------------------------------------------------------------------------
  -- Spill type
  -----------------------------------------------------------------------------
  SpillType: process (clk40, reset) is
  begin  -- process SpillType
    if reset = '1' then           -- asynchronous reset (active high)
      local_spill_type <= SPILL_NORMAL;
    elsif clk40'event and clk40 = '1' then  -- rising clock edge
      -- wake up if we have new data
      if c5_data_valid = '1' then
        if c5_is_k_char = '0' then
          case c5_data(3 downto 0) is
            -- Next spill is NORMAL  
            when DAQ_CMD_SET_NORMAL =>
              local_spill_type <= SPILL_NORMAL;
            -- Next spill is CAL1  
            when DAQ_CMD_SET_CAL1 =>
              local_spill_type <= SPILL_CAL_1;
            -- Next spill is CAL2  
            when DAQ_CMD_SET_CAL2 =>
              local_spill_type <= SPILL_CAL_2;
            -- Next spill is CAL3  
            when DAQ_CMD_SET_CAL3 =>
              local_spill_type <= SPILL_CAL_3;

            when others => null;
          end case;
        end if;
      end if;
    end if;
  end process SpillType;
  
  -----------------------------------------------------------------------------
  -- Spill number, type, and new spill control
  -----------------------------------------------------------------------------
  SpillNumber: process (clk40, reset) is
  begin  -- process DetectorTime
    if reset = '1' then                 -- asynchronous reset (active high)
      spill_number <=  FIRST_SPILL_NUMBER;
      spill_fifo_wr <= '0';
      local_new_spill <= '0';
    elsif clk40'event and clk40 = '1' then  -- rising clock edge
      -- reset new spill signal
      local_new_spill <= '0';
      --reset fifo write
      spill_fifo_wr <= '0';
      -- wake up if we have new data
      if c5_data_valid = '1' then
        if c5_is_k_char = '0' then
          case c5_data(3 downto 0) is
            -- Spill number reset
            when DAQ_CMD_SPILL_RESET  =>
              spill_number <= FIRST_SPILL_NUMBER;
            -- New spill number
            when DAQ_CMD_NEW_SPILL =>
              if ((fifo_full_local = '0') and
                  (run_enable = '1'))then
                local_new_spill <= '1';
              
                spill_fifo_wr <= '1';
                spill_in <= local_spill_type & spill_number & spill_time;
                --Update the spill number for the next spill
                spill_number <= std_logic_vector(unsigned(spill_number)+1);
              else
                --count lost triggers
              end if;

            when others => null;
          end case;
        end if;
      end if;
    end if;
  end process SpillNumber;
  
  -----------------------------------------------------------------------------
  -- FIFO for spill header
  -----------------------------------------------------------------------------
--  fifo_full <= fifo_full_local;
  fifo_full <= '0';
  spill_header_FIFO_1: spill_header_FIFO
    port map (
      rd_clk    => clk125,
      wr_clk    => clk40,
      rst    => reset,
      din    => spill_in,
      wr_en  => spill_fifo_wr,
      rd_en  => spill_rd,
      dout   => spill_data,
      full   => fifo_full_local,
      empty  => fifo_empty,
      valid  => fifo_data_valid);

  -----------------------------------------------------------------------------
  -- Counters
  -----------------------------------------------------------------------------
  c5_event_gen: process (clk40) is
  begin  -- process c5_event_gen
    if clk40'event and clk40 = '1' then  -- rising clock edge
      c5_event <= (others => '0');
      if c5_data_valid = '1' and c5_is_k_char = '0' then
        c5_event(to_integer(unsigned(C5_data))) <= '1';
      end if;
    end if;
  end process c5_event_gen;
  C5_command_counters: for iC5_data in 0 to 15 generate   
--    c5_event(iC5_data) <= '1' when c5_data_valid = '1' and c5_is_k_char = '0' and c5_data = std_logic_vector(to_unsigned(iC5_data,4)) else
--                          '0';
    counter_1: entity work.counter_cross_domain
      generic map (
        roll_over   => '0',
        end_value   => x"FFFF",
        start_value => x"0000",
        DATA_WIDTH  => 16)
      port map (
        clk    => clk40,
        reset  => reset,
        event  => c5_event(iC5_data),
        clkout => clk125,
        count  => counters(iC5_data),
        at_max => open);    
  end generate C5_command_counters;

  spill_type <= local_spill_type;
  new_spill <= local_new_spill;
  
end arch;
