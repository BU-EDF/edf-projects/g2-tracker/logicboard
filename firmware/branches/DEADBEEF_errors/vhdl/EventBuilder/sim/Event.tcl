restart
source vhdl/sim/c5_helper.tcl
source vhdl/sim/8b10b_helper.tcl
source vhdl/sim/TDC_spill_helper.tcl


isim force add {/event_builder/clk125} 1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns
#isim force add {/event_builder/clk100} 1 -radix bin -value 0 -radix bin -time 5 ns -repeat 10 ns
#isim force add {/event_builder/clk10} 1 -radix bin -value 0 -radix bin -time 50 ns -repeat 100 ns
isim force add {/event_builder/reset125} 1 -radix bin
isim force add {/event_builder/reset40} 1 -radix bin 
isim force add {/event_builder/TDC_enable} 1111 -radix bin 
isim force add {/event_builder/TDC_serial} 0000 -radix bin
isim force add {/event_builder/control_local.tdc_enable_mask} 1111 -radix bin
isim force add {/event_builder/control_local.tdc_cdr_override_setting[0]} 000 -radix bin
isim force add {/event_builder/control_local.tdc_cdr_override_setting[1]} 000 -radix bin
isim force add {/event_builder/control_local.tdc_cdr_override_setting[2]} 000 -radix bin
isim force add {/event_builder/control_local.tdc_cdr_override_setting[3]} 000 -radix bin

run 100ns
isim force add {/event_builder/reset125} 0 -radix bin
isim force add {/event_builder/reset40} 0 -radix bin 


for { set i 0} {$i < 50 } {incr i} {
send_cycle 0 /event_builder/c5_stream /event_builder/clk40
}
#send spill
send_c5_data C /event_builder/c5_stream /event_builder/clk40
for { set i 0} {$i < 50 } {incr i} {
send_cycle 0 /event_builder/c5_stream /event_builder/clk40
}
isim force add {/event_builder/c5_stream} 1 -radix bin -value 0 -radix bin -time 50 ns -repeat 100 ns
isim force add {/event_builder/clk40} 1 -radix bin -value 0 -radix bin -time 12.5 ns -repeat 25 ns

set disp [send_8b10b_idle -1 /event_builder/TDC_serial(0) 25]
set disp [send_TDC_spill $disp /event_builder/TDC_serial(0) 01 3]
#set disp [send_real_TDC_spill $disp /event_builder/TDC_serial(0)]
set disp [send_8b10b_idle $disp /event_builder/TDC_serial(0) 25]

set disp [send_8b10b_idle -1 /event_builder/TDC_serial(1) 25]
set disp [send_TDC_spill $disp /event_builder/TDC_serial(1) 01 8]
#set disp [send_real_TDC_spill $disp /event_builder/TDC_serial(1)]
set disp [send_8b10b_idle $disp /event_builder/TDC_serial(1) 25]

set disp [send_8b10b_idle -1 /event_builder/TDC_serial(2) 25]
set disp [send_TDC_spill $disp /event_builder/TDC_serial(2) 01 0]
#set disp [send_real_TDC_spill $disp /event_builder/TDC_serial(2)]
set disp [send_8b10b_idle $disp /event_builder/TDC_serial(2) 25]

set disp [send_8b10b_idle -1 /event_builder/TDC_serial(3) 25]
set disp [send_TDC_spill $disp /event_builder/TDC_serial(3) 01 1]
#set disp [send_real_TDC_spill $disp /event_builder/TDC_serial(3)]
set disp [send_8b10b_idle $disp /event_builder/TDC_serial(3) 25]


#sendTDCEvent -1 /event_builder/TDC_serial(0) 01
#sendTDCEvent -1 /event_builder/TDC_serial(1) 01
#sendTDCEvent -1 /event_builder/TDC_serial(2) 01
#sendTDCEvent -1 /event_builder/TDC_serial(3) 01

#for { set i 0} {$i < 50 } {incr i} {
#send_cycle 0 /event_builder/c5_stream /event_builder/clk40
#}
##send spill
#send_c5_data C /event_builder/c5_stream /event_builder/clk40
#for { set i 0} {$i < 50 } {incr i} {
#send_cycle 0 /event_builder/c5_stream /event_builder/clk40
#}
#isim force add {/event_builder/c5_stream} 1 -radix bin -value 0 -radix bin -time 50 ns -repeat 100 ns
#isim force add {/event_builder/clk40} 1 -radix bin -value 0 -radix bin -time 12.5 ns -repeat 25 ns
#
#set disp [send_8b10b_idle -1 /event_builder/TDC_serial(0) 25]
#set disp [send_TDC_spill $disp /event_builder/TDC_serial(0) 02 3]
#set disp [send_8b10b_idle $disp /event_builder/TDC_serial(0) 25]
#
#set disp [send_8b10b_idle -1 /event_builder/TDC_serial(1) 25]
#set disp [send_TDC_spill $disp /event_builder/TDC_serial(1) 02 8]
#set disp [send_8b10b_idle $disp /event_builder/TDC_serial(1) 25]
#
#set disp [send_8b10b_idle -1 /event_builder/TDC_serial(2) 25]
#set disp [send_TDC_spill $disp /event_builder/TDC_serial(2) 02 0]
#set disp [send_8b10b_idle $disp /event_builder/TDC_serial(2) 25]
#
#set disp [send_8b10b_idle -1 /event_builder/TDC_serial(3) 25]
#set disp [send_TDC_spill $disp /event_builder/TDC_serial(3) 02 1]
#set disp [send_8b10b_idle $disp /event_builder/TDC_serial(3) 25]


run 500 us



