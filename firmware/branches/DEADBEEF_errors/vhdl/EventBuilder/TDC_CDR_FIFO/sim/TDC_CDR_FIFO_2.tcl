source vhdl/EventBuilder/TDC_CDR_FIFO/sim/8b10b_helper.tcl

#------------------------------------------------------------
# main simulation
#------------------------------------------------------------
# set up clock

isim force add /TDC_CDR_FIFO/clk125 1 -value 0 -time 4 ns -repeat 8 ns 
isim force add /TDC_CDR_FIFO/reset 0
isim force add /TDC_CDR_FIFO/enable 1

set disp "-1"

for { set i 0 } { $i < 10 } {incr i} {
    set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
}
#0
set disp [gen_8b10b_control_code 3C $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
#1
set disp [gen_8b10b_data_code 103 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
#2
set disp [gen_8b10b_data_code 45 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
#3
set disp [gen_8b10b_data_code 50 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
#4
set disp [gen_8b10b_data_code 84 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
#5
set disp [gen_8b10b_data_code 68 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 17 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 136 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 181 $disp /TDC_CDR_FIFO/serial]
#6
set disp [gen_8b10b_data_code 67 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 32 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 19 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 18 $disp /TDC_CDR_FIFO/serial]
#7
set disp [gen_8b10b_data_code 1 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 255 $disp /TDC_CDR_FIFO/serial]
#8
set disp [gen_8b10b_data_code 255 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 144 $disp /TDC_CDR_FIFO/serial]
#9
set disp [gen_8b10b_data_code 96 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 72 $disp /TDC_CDR_FIFO/serial]
#10
set disp [gen_8b10b_data_code 98 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 255 $disp /TDC_CDR_FIFO/serial]
#11
set disp [gen_8b10b_data_code 255 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 72 $disp /TDC_CDR_FIFO/serial]
#12
set disp [gen_8b10b_data_code 98 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
#13
set disp [gen_8b10b_data_code 100 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 17 $disp /TDC_CDR_FIFO/serial]
#14
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 192 $disp /TDC_CDR_FIFO/serial]
#15
set disp [gen_8b10b_data_code 48 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]

set disp [gen_8b10b_data_code 118 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 151 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 217 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 152 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 179 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 153 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 16 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 147 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 151 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 92 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 184 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 56 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 125 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 122 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 58 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 125 $disp /TDC_CDR_FIFO/serial]
set disp [gen_8b10b_data_code 121 $disp /TDC_CDR_FIFO/serial]

for { set i 0 } { $i < 100 } {incr i} {
    set disp [gen_8b10b_data_code 0 $disp /TDC_CDR_FIFO/serial]
}





#source vhdl/EventBuilder/TDC_CDR_FIFO/sim/TDC_CDR_FIFO_2.tcl
