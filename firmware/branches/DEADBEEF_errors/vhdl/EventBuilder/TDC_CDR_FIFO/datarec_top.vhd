-------------------------------------------------------------------------------
-- datarec.vhd : top-level data recovery
-- use mask produced by decoder blocks to pick a bit from shift register
--
-- seems to work ok
-- 15 Aug 2013, esh - add extra register on output
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;

entity datarec is

  generic (
    W : integer := 10;                  -- input transition window width
    M : integer := 5);                  -- oversampling factor

  port (
    clk   : in  std_logic;              -- oversampling (125MHz) clock
    rst_n : in  std_logic;              -- active low reset
    ser   : in  std_logic;              -- serial input
    dv    : out std_logic;              -- data valid
    edges : out std_logic_vector(4 downto 0);
    histo : out std_logic_vector(4 downto 0);
    multi_valid : out std_logic;
    override_setting : in std_logic_vector(2 downto 0); -- override the auto
                                                        -- detect phase
    d     : out std_logic);             -- recovered data

end entity datarec;

architecture arch of datarec is

  component edge is
    generic (
      SOFF : integer;
      M : integer);
    port (
      clk   : in  std_logic;
      rst_n : in  std_logic;
      ser   : in  std_logic;
      en    : out std_logic;
      sreg  : out std_logic_vector(M-1 downto 0);
      edges : out std_logic_vector(M-1 downto 0));
  end component edge;

  component deblock is
    generic (
      W   : integer;
      ENO : integer;
      M   : integer);
    port (
      clk     : in  std_logic;
      rst_n   : in  std_logic;
      en      : in  std_logic;
      edges   : in  std_logic_vector(M-1 downto 0);
      aligned : out std_logic);
  end component deblock;

  signal en        : std_logic;
  signal local_edges     : std_logic_vector(M-1 downto 0);
  signal bit_valid : std_logic_vector(M-1 downto 0);
  signal sreg      : std_logic_vector(M-1 downto 0);

  signal d_r, dv_r : std_logic;

  signal edge_counter : std_logic_vector(M-1 downto 0) := (others => '0');
  
begin  -- architecture arch
  

  -- edge detector
  edge_1 : entity work.edge
    generic map (
      M => M,
      SOFF => 2)  -- should be (i+(M+1)/3)mod 5, so  (i + 3) mod(5) but easier
                  -- to do (i - 2) mod (5)
    port map (
      clk   => clk,
--      rst_n => rst_n,
      rst_n => '1',
      ser   => ser,
      en    => en,
      sreg  => sreg,
      edges => local_edges);

  -- M decoder blocks to check bit alignment
  f1 : for k in 0 to M-1 generate
    deblock_1 : entity work.deblock
      generic map (
        W   => W,
        ENO => k,
        M   => M)
      port map (
        clk     => clk,
--        rst_n   => rst_n,
        rst_n   => '1',
        en      => en,
        edges   => local_edges,
        aligned => bit_valid(k));
  end generate f1;

  -- 1 of M in bit_valid should be set
  process (clk) is
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge

      d  <= d_r;
      dv <= dv_r;  -- extra register in output to ease timing

      dv_r <= '0';


      if en = '1' then
        dv_r <= '1';          
        case override_setting is
          when "001" => d_r <= sreg(0);
          when "010" => d_r <= sreg(1);
          when "011" => d_r <= sreg(2);
          when "100" => d_r <= sreg(3);
          when "101" => d_r <= sreg(4);                         
          when others =>
            dv_r <= '0';
            if or_reduce(bit_valid) = '1' then
              d_r  <= or_reduce(bit_valid and sreg);
              dv_r <= '1';
            end if;
        end case;
      end if;            
    end if;
  end process;

  edge_reporting: process (clk) is
  begin  -- process edge_reporting
    if clk'event and clk = '1' then  -- rising clock edge
      if en = '1' then
        histo <= bit_valid;
        edges <= local_edges;
        -- report if multiple phases report as valid
        multi_valid <= '0';
        if (not (bit_valid = "00001" or
                 bit_valid = "00010" or
                 bit_valid = "00100" or
                 bit_valid = "01000" or
                 bit_valid = "10000")) then
          multi_valid <= '1';
        end if;
      end if;
    end if;
  end process edge_reporting;


  
end architecture arch;
