proc daq_wr { address data } {
    isim force add {/top/daq_reg_addr} $address -radix hex
    isim force add {/top/daq_reg_data_in} $data -radix hex
    isim force add {/top/daq_reg_wr} 1 -radix hex
    run 8ns
    isim force add {/top/daq_reg_wr} 0 -radix hex
}


restart

isim force add {/top/TDC_serial_real} 0 -radix hex

#set clock on /top/clk
isim force add {/top/clk125_in} 1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns 
isim force add {/top/reset} 1
isim force add {/top/reset_eb} 1
run 200ns
isim force add {/top/reset} 0
isim force add {/top/reset_eb} 0
#let everything start up
run 1 ms

daq_wr 10 1
run 100 ns
daq_wr 10 80000001
run 1 ms

