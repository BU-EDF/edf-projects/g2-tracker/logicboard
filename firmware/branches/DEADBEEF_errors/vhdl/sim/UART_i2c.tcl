source vhdl/sim/UART_helper.tcl

#set initial conditions for UART
isim force add {/top/sc_in} 1
#isim force add {/top/reset} 0

#isim force add {/top/tdc_sda} 1

#set clock on /top/clk
isim force add {/top/clk125_in} 1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns 

#let everything start up
run 1 ms


#send tdc_sel command
sendUART_str i2c_sel_dac 115200 /top/sc_in;
#send CR (0x0D)
sendUART_hex 0D 115200 /top/sc_in;

run 2 ms

#send i2c_reg_rd command
sendUART_str i2c_reg_rd 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str ac 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str 00 115200 /top/sc_in;

#send CR (0x0D)
sendUART_hex 0D 115200 /top/sc_in;


run 10 ms
