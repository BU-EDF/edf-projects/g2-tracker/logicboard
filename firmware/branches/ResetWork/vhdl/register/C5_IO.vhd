----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package C5_IO is
  
  type C5_Control is record
    cmd_strobe         : std_logic;
    cmd_data           : std_logic_vector(4 downto 0);
  end record;

  type C5_Monitor is record
    cmd_data           : std_logic_vector(4 downto 0);
  end record;

end C5_IO;
