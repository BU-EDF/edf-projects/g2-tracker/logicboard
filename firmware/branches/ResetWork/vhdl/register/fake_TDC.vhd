----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package Fake_spill_data is
  type Fake_TDC_Control is record
    send_spill : std_logic;
    spill_number : std_logic_vector(23 downto 0);
    spill_time : std_logic_vector(43 downto 0);
    spill_hit_count : unsigned(10 downto 0);    
  end record;
end Fake_spill_data;
