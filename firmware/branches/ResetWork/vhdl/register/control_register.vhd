------------------------------------------------------------------------------
-- control_register :
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed."+";
use ieee.std_logic_signed."=";

use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;


use work.LogicBoard_IO.all;
use work.EventBuilder_IO.all;
use work.C5_IO.all;
use work.Fake_spill_data.all;
use work.TestPulse_IO.all;

entity control_register is

  port (
    clk : in std_logic;
    reset : in std_logic;

    -- Logicboard firmware version
    Firmware_Version : in std_logic_vector(31 downto 0);
    
    -- interface to Logic board values
    LB_monitor : in LogicBoard_Monitor;
    LB_control : out LogicBoard_Control;

    -- interface to Event builder
    EB_monitor : in EventBuilder_Monitor;
    EB_control : out EventBuilder_Control;
    EB_reset   : out std_logic;
    EB_clk     : in std_logic;

    -- interface to C5 (both DAQ and fake source)
    C5_control : out C5_Control;
    C5_monitor : in  C5_Monitor;
    
    --interface to fake TDC generator
    control_fake_TDC  : out Fake_TDC_Control;
    monitor_fake_TDC : in Fake_TDC_Control;
    enable_fake_TDCs_out : out std_logic;
    enable_fake_TDCs_in : in std_logic;
    -- interface to test pulser
    control_testpulse : out TestPulseControl;
    
    --interface to uC
    address : in std_logic_vector(7 downto 0);
    out_port_rd : in std_logic := '0';
    out_port : out std_logic_vector(31 downto 0);
    out_port_dv : out std_logic := '0';
    in_port : in std_logic_vector(31 downto 0);
    in_port_wr : in std_logic := '0'
    );


end entity control_register;

architecture arch of control_register is

  component pacd is
    port (
      iPulseA : IN  std_logic;
      iClkA   : IN  std_logic;
      iRSTAn  : IN  std_logic;
      iClkB   : IN  std_logic;
      iRSTBn  : IN  std_logic;
      oPulseB : OUT std_logic);
  end component pacd;
-----------------------------------------------------------------------------
-- Address space
-----------------------------------------------------------------------------

  ----------------------------
  -- Logic board
  ----------------------------
  constant LB_CLK_STATUS              : std_logic_vector(7 downto 0) := x"00";
  -- 27 (r) ext clock locked change
  -- 26..24 (r) ext clock status(2..0) change
  -- 23 (r) ext clock valid change
  -- 22..20 (r) ext clock status(2..0)
  -- 19 (r) ext clock valid
  -- 18 (r) daq clock locked
  -- 17 (r) ext clock locked
  -- 16 (r) local clock locked
  -- 3 (a)   reset external clock monitoring
  -- 2 (r/w) reset daq clocking
  -- 1 (r/w) select daq source clock
  -- 0 (r/w) external clock reset
  constant LB_Firmware_Version : std_logic_vector(7 downto 0) := x"01";
  -- 31..24 (r) year (20YY)
  -- 23..16 (r) month (MM)
  -- 15..8  (r) day (DD)
  -- 7..0   (r) minor version  
  
  constant LB_TP_status           : std_logic_vector(7 downto 0) := x"05";
  -- 3..0 (r/w) ASDQ pulse driver enable
  -- 4 (r/w) Enable 1.6V
  -- 5 (r/w) Enable -1.6V

  constant LB_TP_pulses1          : std_logic_vector(7 downto 0) := x"06";
  -- 31 (r/w) send test pulses for all spills
  -- 27..20 (r/w) test pulse channel mask
  -- 19..0 (r/w) start time delay

  constant LB_TP_pulses2          : std_logic_vector(7 downto 0) := x"07";
  -- 27..20 (r/w) pulse count
  -- 19..0 (r/w) pulse to pulse count


  constant LB_SFP                 : std_logic_vector(7 downto 0) := x"09";
  -- 4 (r/w) select SFP output (8b10b or C5)
  -- 2 (r/w) disable SFP output
  -- 1 (r) los of signals
  -- 0 (r) SFP present

  constant LB_LED_GPIO            : std_logic_vector(7 downto 0) := x"0a";
  -- 19..16 (r/w) GPIO control
  -- 3..0 (r/w) LED control

  
  ----------------------------
  -- Fake TDCs
  ----------------------------  
  constant FAKE_TDCS_ctrl      : std_logic_vector(7 downto 0) := x"10";
  -- 31 (a)
  -- 0 (r/w) switch to fake TDC stream
  constant FAKE_TDCS_spill_n   : std_logic_vector(7 downto 0) := x"11";
  -- 23..0 (r/w) fake spill event number
  constant FAKE_TDCS_spill_t1  : std_logic_vector(7 downto 0) := x"12";
  -- 31..0 (r/w) fake spill time 43 downto 12
  constant FAKE_TDCS_spill_t2  : std_logic_vector(7 downto 0) := x"13";
  -- 11..0 (r/w) fake spill time 11 downto 06  
  constant FAKE_TDCS_hit_count : std_logic_vector(7 downto 0) := x"14";    
  -- 11..0 (r/w) number of hits

  ----------------------------
  -- EB values
  ----------------------------
  constant EB_status              : std_logic_vector(7 downto 0) := x"20";
  -- 0 (a) restart Event builder
  constant EB_started_spills      : std_logic_vector(7 downto 0) := x"21";
  -- 31..0 (r) new spills pulled from fifo
  constant EB_sent_spills         : std_logic_vector(7 downto 0) := x"22";
  -- 31..0 (r) spill trailers sent
  constant EB_debug_fifo          : std_logic_vector(7 downto 0) := x"23";
  -- 16 (a) read
  -- 11 (r) full
  -- 10 (r) empty
  -- 9 (r) data valid
  -- 8..0 (r) data

  
  ----------------------------
  -- C5 values
  ----------------------------
  constant C5_status              : std_logic_vector(7 downto 0) := x"30";
  -- 7 (r) fifo full
  -- 6 (r) fifo empty
  -- 3 (r/w) TDC clock source

  constant C5_CMD                 : std_logic_vector(7 downto 0) := x"31";
  -- 8 (a) write C5
  -- 4..0 (r/w) kchar,data


  ----------------------------
  -- TDC values
  ----------------------------
  constant TDC_control            : std_logic_vector(7 downto 0) := x"40";
  -- 19..16 (r) TDC 8b10b locked on
  -- 15..12 (r) TDC fifo empty
  -- 11..8 (r) TDC fifo full
  -- 7..4 (r) nothing
  -- 3..0 (r/w) TDC enable mask

  
  constant TDC_0_FIFO_control     : std_logic_vector(7 downto 0) := x"50";
  -- 31 (a) fifo read (only valid when TDC is not enabled)
  -- 30 (r) fifo data valid
  -- 28..24 (r) CDR edges
  -- 20..16 (r) CDR histogram
  -- 6..4 (r/w) Override CDR setting (0 = auto)
  -- 1..0 (r) current fifo word trailer + header bit
  constant TDC_0_FIFO_data        : std_logic_vector(7 downto 0) := x"51";   
  -- 31..0 (r) word from TDC fifo
  constant TDC_0_k_chars          : std_logic_vector(7 downto 0) := x"52";
  -- 31..0 (r) count (rolls over)
  constant TDC_0_k_chars_3C       : std_logic_vector(7 downto 0) := x"53";
  -- 31..0 (r) count
  constant TDC_0_k_chars_in_data  : std_logic_vector(7 downto 0) := x"54";
  -- 31..0 (r) count
  constant TDC_0_malformed_spills : std_logic_vector(7 downto 0) := x"55";
  -- 31..0 (r) count
  constant TDC_0_invalid_chars    : std_logic_vector(7 downto 0) := x"56";
  -- 31..0 (r) count
  constant TDC_0_misaligned_syncs : std_logic_vector(7 downto 0) := x"57"; 
  -- 31..0 (r) count
  constant TDC_0_d_chars          : std_logic_vector(7 downto 0) := x"58";
  -- 31..0 (r) count (rolls over)
  constant TDC_0_chars            : std_logic_vector(7 downto 0) := x"59";
  -- 25..16 (r) current 10b char
  -- 8..0 (r) current 8b char
  constant TDC_0_debug_size       : std_logic_vector(7 downto 0) := x"5A";
  -- 30..16 (r) actual spill size
  -- 14..0 (r) expected spill size
  constant TDC_0_missing_tr_bits  : std_logic_vector(7 downto 0) := x"5B";
  -- 31..0 (r) count
  constant TDC_0_lock_timeouts    : std_logic_vector(7 downto 0) := x"5C";
  -- 31..0 (r) count

  
  constant TDC_1_FIFO_control     : std_logic_vector(7 downto 0) := x"60";
  -- 31 (a) fifo read (only valid when TDC is not enabled)
  -- 30 (r) fifo data valid
  -- 28..24 (r) CDR edges
  -- 20..16 (r) CDR histogram
  -- 6..4 (r/w) Override CDR setting  (0 = auto) 
  -- 1..0 (r) current fifo word trailer + header bit
  constant TDC_1_FIFO_data        : std_logic_vector(7 downto 0) := x"61";   
  -- 31..0 (r) word from TDC fifo
  constant TDC_1_k_chars          : std_logic_vector(7 downto 0) := x"62";
  -- 31..0 (r) count (rolls over)
  constant TDC_1_k_chars_3C       : std_logic_vector(7 downto 0) := x"63";
  -- 31..0 (r) count
  constant TDC_1_k_chars_in_data  : std_logic_vector(7 downto 0) := x"64";
  -- 31..0 (r) count
  constant TDC_1_malformed_spills : std_logic_vector(7 downto 0) := x"65";
  -- 31..0 (r) count
  constant TDC_1_invalid_chars    : std_logic_vector(7 downto 0) := x"66";
  -- 31..0 (r) count
  constant TDC_1_misaligned_syncs : std_logic_vector(7 downto 0) := x"67"; 
  -- 31..0 (r) count
  constant TDC_1_d_chars          : std_logic_vector(7 downto 0) := x"68";
  -- 31..0 (r) count (rolls over)
  constant TDC_1_chars            : std_logic_vector(7 downto 0) := x"69";
  -- 25..16 (r) current 10b char
  -- 8..0 (r) current 8b char
  constant TDC_1_debug_size       : std_logic_vector(7 downto 0) := x"6A";
  -- 30..16 (r) actual spill size
  -- 14..0 (r) expected spill size
  constant TDC_1_missing_tr_bits  : std_logic_vector(7 downto 0) := x"6B";
  -- 31..0 (r) count
  constant TDC_1_lock_timeouts    : std_logic_vector(7 downto 0) := x"6C";
  -- 31..0 (r) count

  
  constant TDC_2_FIFO_control     : std_logic_vector(7 downto 0) := x"70";
  -- 31 (a) fifo read (only valid when TDC is not enabled)
  -- 30 (r) fifo data valid
  -- 28..24 (r) CDR edges
  -- 20..16 (r) CDR histogram

  -- 6..4 (r/w) Override CDR setting (0 = auto)
  -- 1..0 (r) current fifo word trailer + header bit
  constant TDC_2_FIFO_data        : std_logic_vector(7 downto 0) := x"71";   
  -- 31..0 (r) word from TDC fifo
  constant TDC_2_k_chars          : std_logic_vector(7 downto 0) := x"72";
  -- 31..0 (r) count (rolls over)
  constant TDC_2_k_chars_3C       : std_logic_vector(7 downto 0) := x"73";
  -- 31..0 (r) count
  constant TDC_2_k_chars_in_data  : std_logic_vector(7 downto 0) := x"74";
  -- 31..0 (r) count
  constant TDC_2_malformed_spills : std_logic_vector(7 downto 0) := x"75";
  -- 31..0 (r) count
  constant TDC_2_invalid_chars    : std_logic_vector(7 downto 0) := x"76";
  -- 31..0 (r) count
  constant TDC_2_misaligned_syncs : std_logic_vector(7 downto 0) := x"77";
  -- 31..0 (r) count
  constant TDC_2_d_chars          : std_logic_vector(7 downto 0) := x"78";
  -- 31..0 (r) count (rolls over)
  constant TDC_2_chars            : std_logic_vector(7 downto 0) := x"79";
  -- 25..16 (r) current 10b char
  -- 8..0 (r) current 8b char
  constant TDC_2_debug_size       : std_logic_vector(7 downto 0) := x"7A";
  -- 30..16 (r) actual spill size
  -- 14..0 (r) expected spill siz
  constant TDC_2_missing_tr_bits  : std_logic_vector(7 downto 0) := x"7B";
  -- 31..0 (r) count
  constant TDC_2_lock_timeouts    : std_logic_vector(7 downto 0) := x"7C";
  -- 31..0 (r) count


  constant TDC_3_FIFO_control     : std_logic_vector(7 downto 0) := x"80";
  -- 31 (a) fifo read (only valid when TDC is not enabled)
  -- 30 (r) fifo data valid
  -- 28..24 (r) CDR edges
  -- 20..16 (r) CDR histogram
  -- 6..4 (r/w) Override CDR setting (0 = auto)
  -- 1..0 (r) current fifo word trailer + header bit
  constant TDC_3_FIFO_data        : std_logic_vector(7 downto 0) := x"81";   
  -- 31..0 (r) word from TDC fifo
  constant TDC_3_k_chars          : std_logic_vector(7 downto 0) := x"82";
  -- 31..0 (r) count (rolls over)
  constant TDC_3_k_chars_3C       : std_logic_vector(7 downto 0) := x"83";
  -- 31..0 (r) count
  constant TDC_3_k_chars_in_data  : std_logic_vector(7 downto 0) := x"84";
  -- 31..0 (r) count
  constant TDC_3_malformed_spills : std_logic_vector(7 downto 0) := x"85";
  -- 31..0 (r) count
  constant TDC_3_invalid_chars    : std_logic_vector(7 downto 0) := x"86";
  -- 31..0 (r) count
  constant TDC_3_misaligned_syncs : std_logic_vector(7 downto 0) := x"87";
  -- 31..0 (r) count
  constant TDC_3_d_chars          : std_logic_vector(7 downto 0) := x"88";
  -- 31..0 (r) count (rolls over)
  constant TDC_3_chars            : std_logic_vector(7 downto 0) := x"89";
  -- 25..16 (r) current 10b char
  -- 8..0 (r) current 8b char
  constant TDC_3_debug_size       : std_logic_vector(7 downto 0) := x"8A";
  -- 30..16 (r) actual spill size
  -- 14..0 (r) expected spill size
  constant TDC_3_missing_tr_bits  : std_logic_vector(7 downto 0) := x"8B";
  -- 31..0 (r) count
  constant TDC_3_lock_timeouts    : std_logic_vector(7 downto 0) := x"8C";
  -- 31..0 (r) count


  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------

  signal local_EB_reset : std_logic := '0';
  signal local_EB_control_TDC_FIFO_rd : std_logic_vector(3 downto 0) := x"0";
  signal local_fake_TDC_send_spill : std_logic := '0';

  signal local_testpulse : TestPulseControl;

  signal local_EB_control_LB_FIFO_rd : std_logic;
begin  -- architecture arch

  control_testpulse <= local_testpulse;
  
  -----------------------------------------------------------------------------
  -- read
  -----------------------------------------------------------------------------
  read_access: process (clk, reset)
  begin  -- process read_access
    if reset = '1' then                 -- asynchronous reset (active high)
      out_port_dv <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      out_port_dv <= '0';
      if out_port_rd = '1' then
        out_port_dv <= '1';
        case address is
          when LB_CLK_STATUS              =>
            out_port(31 downto 28)  <= x"0";
            out_port(27)            <= LB_monitor.ext_clock_locked_ch;
            out_port(26 downto 24)  <= LB_monitor.ext_clock_status_ch;
            out_port(23)            <= LB_monitor.ext_clock_valid_ch;
            out_port(22 downto 20)  <= LB_monitor.ext_clock_status;
            out_port(19)            <= LB_monitor.ext_clock_valid;
            out_port(18)            <= LB_monitor.daq_clock_locked;
            out_port(17)            <= LB_monitor.ext_clock_locked;
            out_port(16)            <= LB_monitor.local_clock_locked;
            out_port(15 downto 3)   <= x"000"&"0";          
            out_port(2)             <= LB_monitor.daq_clock_reset;
            out_port(1)             <= LB_monitor.daq_clock_source;
            out_port(0)             <= LB_monitor.ext_clock_reset;
          when LB_Firmware_Version    =>
            out_port                <= Firmware_Version;
          when LB_SFP                 =>
            out_port(31 downto 8)   <= x"000000";
            out_port(7 downto 5)    <= "000";
            out_port(4)            <= LB_monitor.SFP_out_mux;
            out_port(3)            <= '0';
            out_port(2)            <= LB_monitor.SFP_TxDSBL;
            out_port(1)            <= LB_monitor.SFP_LOS;
            out_port(0)            <= LB_monitor.SFP_present;
          when LB_LED_GPIO            =>
            out_port(31 downto 20) <= x"000";
            out_port(19 downto 16) <= LB_monitor.GPIO;
            out_port(15 downto  4) <= x"000";
            out_port(3  downto  0) <= LB_monitor.LED;
          when LB_TP_status           =>
            out_port(31 downto 6) <= x"000000"&"00";
            out_port(5 downto 4)  <= LB_monitor.Test_pulse_power;
            out_port(3 downto 0)  <= LB_monitor.Test_pulse_en;
          when LB_TP_pulses1          =>
            out_port(31) <= local_testpulse.trigger_all_types;
            out_port(27 downto 20) <= local_testpulse.pulse_channel_mask;
            out_port(19 downto 0)  <= local_testpulse.start_time_count;
          when LB_TP_pulses2          =>
            out_port(27 downto 20) <= local_testpulse.pulse_count;
            out_port(19 downto 0)  <= local_testpulse.pulse_pulse_count;
          when FAKE_TDCS_ctrl      =>
            out_port(0)  <= enable_fake_TDCs_in;
            out_port(31 downto 1) <= "000"&x"0000000";
          when FAKE_TDCS_spill_n   =>
            out_port <= x"00" & monitor_fake_TDC.spill_number;
          when FAKE_TDCS_spill_t1  =>
            out_port <= monitor_fake_TDC.spill_time(43 downto 12);
          when FAKE_TDCS_spill_t2  =>
            out_port <= x"00000" & monitor_fake_TDC.spill_time(11 downto 0);
          when FAKE_TDCS_hit_count =>
            out_port(31 downto 11) <=  "0" & x"00000";
            out_port(10 downto 0)  <= std_logic_vector(monitor_fake_TDC.spill_hit_count);
          when EB_status              =>
            out_port(31 downto 1) <= x"0000000"&"000";
            out_port(0) <= EB_monitor.EB_reset;
          when EB_started_spills      => out_port <= EB_monitor.new_spill_count;
          when EB_sent_spills         => out_port <= EB_monitor.sent_spill_count;                                     

          when EB_debug_fifo          =>
            out_port(31 downto 12)<= x"00000";
            out_port(11)          <= EB_monitor.LB_FIFO_full;
            out_port(10)          <= EB_monitor.LB_FIFO_empty;
            out_port(9)           <= EB_monitor.LB_FIFO_data_valid;
            out_port(8 downto 0)  <= EB_monitor.LB_fifo_data;
            
          when C5_status              =>
            out_port(31 downto 8) <= x"000000";
            out_port(7)           <= EB_monitor.C5_fifo_full;
            out_port(6)           <= EB_monitor.C5_fifo_empty;
            out_port(5 downto 4)  <= "00";
            out_port(3)           <= LB_monitor.TDC_clock_select;
            out_port(2)           <= '0';
            out_port(1)           <= '0';
          when C5_CMD =>
            out_port(4 downto 0)  <= C5_monitor.cmd_data;          

          when TDC_control             =>
            out_port(31 downto 20) <= x"000";
            out_port(19 downto 16) <= EB_monitor.TDC_locked;
            out_port(15 downto 12) <= EB_monitor.TDC_fifo_empty;
            out_port(11 downto  8) <= EB_monitor.TDC_fifo_full;
            out_port(7  downto  4) <= x"0";
            out_port(3  downto  0) <= EB_monitor.TDC_enable_mask;

          when TDC_0_FIFO_control     =>
            out_port(31) <= '0';
            out_port(30) <= EB_monitor.TDC_fifo_data_valid(0);
            out_port(29) <= '0';
            out_port(28 downto 24) <= EB_monitor.TDC_CDR_edges(0);
            out_port(23 downto 21) <= "000";
            out_port(20 downto 16) <= EB_monitor.TDC_CDR_histogram(0);
            out_port(15 downto 7)  <= "000000000";
            out_port(6 downto 4)   <= EB_monitor.TDC_CDR_override_setting(0);
            out_port(3 downto 2)   <= "00";
            out_port(1 downto 0)   <= EB_monitor.TDC_fifo_data(0)(33 downto 32);
          when TDC_0_FIFO_data        => out_port <= EB_monitor.TDC_fifo_data(0)(31 downto 0);          
          when TDC_0_k_chars          => out_port <= std_logic_vector(EB_monitor.TDC_k_chars(0));
          when TDC_0_k_chars_3C       => out_port <= std_logic_vector(EB_monitor.TDC_k_chars_3C(0));
          when TDC_0_k_chars_in_data  => out_port <= std_logic_vector(EB_monitor.TDC_k_chars_in_data(0));
          when TDC_0_malformed_spills => out_port <= std_logic_vector(EB_monitor.TDC_malformed_spills(0));
          when TDC_0_invalid_chars    => out_port <= std_logic_vector(EB_monitor.TDC_invalid_chars(0));
          when TDC_0_misaligned_syncs => out_port <= std_logic_vector(EB_monitor.TDC_misaligned_syncs(0));
          when TDC_0_d_chars          => out_port <= std_logic_vector(EB_monitor.TDC_d_chars(0));
          when TDC_0_chars            => out_port <= "000000" & EB_monitor.TDC_10b(0) & "0000000" & EB_monitor.TDC_8b(0);
          when TDC_0_debug_size       =>
            out_port(31)           <= '0';
            out_port(30 downto 16) <= EB_monitor.TDC_actual_size(0);
            out_port(15)           <= '0';
            out_port(14 downto 0)  <= EB_monitor.TDC_expected_size(0);
          when TDC_0_missing_tr_bits  => out_port <= std_logic_vector(EB_monitor.TDC_missing_trailer_bits(0));
          when TDC_0_lock_timeouts    => out_port <= std_logic_vector(EB_monitor.TDC_lock_timeouts(0));     

                                         
          when TDC_1_FIFO_control     =>
            out_port(31) <= '0';
            out_port(30) <= EB_monitor.TDC_fifo_data_valid(1);
            out_port(29) <= '0';
            out_port(28 downto 24) <= EB_monitor.TDC_CDR_edges(1);
            out_port(23 downto 21) <= "000";
            out_port(20 downto 16) <= EB_monitor.TDC_CDR_histogram(1);
            out_port(15 downto 7 ) <= "000000000";
            out_port(6 downto 4)   <= EB_monitor.TDC_CDR_override_setting(1);
            out_port(3 downto 2)   <= "00";
            out_port(1 downto 0) <= EB_monitor.TDC_fifo_data(1)(33 downto 32);
          when TDC_1_FIFO_data        => out_port <= EB_monitor.TDC_fifo_data(1)(31 downto 0);                                      
          when TDC_1_k_chars          => out_port <= std_logic_vector(EB_monitor.TDC_k_chars(1));
          when TDC_1_k_chars_3C       => out_port <= std_logic_vector(EB_monitor.TDC_k_chars_3C(1));
          when TDC_1_k_chars_in_data  => out_port <= std_logic_vector(EB_monitor.TDC_k_chars_in_data(1));
          when TDC_1_malformed_spills => out_port <= std_logic_vector(EB_monitor.TDC_malformed_spills(1));
          when TDC_1_invalid_chars    => out_port <= std_logic_vector(EB_monitor.TDC_invalid_chars(1));
          when TDC_1_misaligned_syncs => out_port <= std_logic_vector(EB_monitor.TDC_misaligned_syncs(1));
          when TDC_1_d_chars          => out_port <= std_logic_vector(EB_monitor.TDC_d_chars(1));
          when TDC_1_chars            => out_port <= "000000" & EB_monitor.TDC_10b(1) & "0000000" & EB_monitor.TDC_8b(1);  
          when TDC_1_debug_size       =>
            out_port(31)           <= '0';
            out_port(30 downto 16) <= EB_monitor.TDC_actual_size(1);
            out_port(15)           <= '0';
            out_port(14 downto 0)  <= EB_monitor.TDC_expected_size(1);
          when TDC_1_missing_tr_bits  => out_port <= std_logic_vector(EB_monitor.TDC_missing_trailer_bits(1));
          when TDC_1_lock_timeouts    => out_port <= std_logic_vector(EB_monitor.TDC_lock_timeouts(1));

                                         
          when TDC_2_FIFO_control     =>
            out_port(31) <= '0';
            out_port(30) <= EB_monitor.TDC_fifo_data_valid(2);
            out_port(29) <= '0';
            out_port(28 downto 24) <= EB_monitor.TDC_CDR_edges(2);
            out_port(23 downto 21) <= "000";
            out_port(20 downto 16) <= EB_monitor.TDC_CDR_histogram(2);
            out_port(15 downto  7) <= "000000000";
            out_port(6 downto 4)   <= EB_monitor.TDC_CDR_override_setting(2);
            out_port(3 downto 2)   <= "00";
            out_port(1 downto 0) <= EB_monitor.TDC_fifo_data(2)(33 downto 32);
          when TDC_2_FIFO_data        => out_port <= EB_monitor.TDC_fifo_data(2)(31 downto 0);
          when TDC_2_k_chars          => out_port <= std_logic_vector(EB_monitor.TDC_k_chars(2));
          when TDC_2_k_chars_3C       => out_port <= std_logic_vector(EB_monitor.TDC_k_chars_3C(2));
          when TDC_2_k_chars_in_data  => out_port <= std_logic_vector(EB_monitor.TDC_k_chars_in_data(2));
          when TDC_2_malformed_spills => out_port <= std_logic_vector(EB_monitor.TDC_malformed_spills(2));
          when TDC_2_invalid_chars    => out_port <= std_logic_vector(EB_monitor.TDC_invalid_chars(2));
          when TDC_2_misaligned_syncs => out_port <= std_logic_vector(EB_monitor.TDC_misaligned_syncs(2));
          when TDC_2_d_chars          => out_port <= std_logic_vector(EB_monitor.TDC_d_chars(2));
          when TDC_2_chars            => out_port <= "000000" & EB_monitor.TDC_10b(2) & "0000000" & EB_monitor.TDC_8b(2);
          when TDC_2_debug_size       =>
            out_port(31)           <= '0';
            out_port(30 downto 16) <= EB_monitor.TDC_actual_size(2);
            out_port(15)           <= '0';
            out_port(14 downto 0)  <= EB_monitor.TDC_expected_size(2);
          when TDC_2_missing_tr_bits  => out_port <= std_logic_vector(EB_monitor.TDC_missing_trailer_bits(2));
          when TDC_2_lock_timeouts    => out_port <= std_logic_vector(EB_monitor.TDC_lock_timeouts(2));            

                                         
          when TDC_3_FIFO_control     =>
            out_port(31) <= '0';
            out_port(30) <= EB_monitor.TDC_fifo_data_valid(3);
            out_port(29) <= '0';
            out_port(28 downto 24) <= EB_monitor.TDC_CDR_edges(3);
            out_port(23 downto 21) <= "000";
            out_port(20 downto 16) <= EB_monitor.TDC_CDR_histogram(3);
            out_port(15 downto  7) <= "000000000";
            out_port(6 downto 4)   <= EB_monitor.TDC_CDR_override_setting(3);
            out_port(3 downto 2)   <= "00";
            out_port(1 downto 0) <= EB_monitor.TDC_fifo_data(3)(33 downto 32);
          when TDC_3_FIFO_data        => out_port <= EB_monitor.TDC_fifo_data(3)(31 downto 0);
          when TDC_3_k_chars          => out_port <= std_logic_vector(EB_monitor.TDC_k_chars(3));
          when TDC_3_k_chars_3C       => out_port <= std_logic_vector(EB_monitor.TDC_k_chars_3C(3));
          when TDC_3_k_chars_in_data  => out_port <= std_logic_vector(EB_monitor.TDC_k_chars_in_data(3));
          when TDC_3_malformed_spills => out_port <= std_logic_vector(EB_monitor.TDC_malformed_spills(3));
          when TDC_3_invalid_chars    => out_port <= std_logic_vector(EB_monitor.TDC_invalid_chars(3));
          when TDC_3_misaligned_syncs => out_port <= std_logic_vector(EB_monitor.TDC_misaligned_syncs(3));
          when TDC_3_d_chars          => out_port <= std_logic_vector(EB_monitor.TDC_d_chars(3));
          when TDC_3_chars            => out_port <= "000000" & EB_monitor.TDC_10b(3) & "0000000" & EB_monitor.TDC_8b(3);
          when TDC_3_debug_size       =>
            out_port(31)           <= '0';
            out_port(30 downto 16) <= EB_monitor.TDC_actual_size(3);
            out_port(15)           <= '0';
            out_port(14 downto 0)  <= EB_monitor.TDC_expected_size(3);
          when TDC_3_missing_tr_bits  => out_port <= std_logic_vector(EB_monitor.TDC_missing_trailer_bits(3));
          when TDC_3_lock_timeouts    => out_port <= std_logic_vector(EB_monitor.TDC_lock_timeouts(3));     
                                         
          when others => null;
        end case;
      end if;
    end if;
  end process read_access;
  
  -----------------------------------------------------------------------------
  -- write
  -----------------------------------------------------------------------------
  
  write_access: process (clk, reset)
  begin  -- process read_access
    if reset = '1' then                 -- asynchronous reset (active high)
      --LB_CLK_STATUS
      LB_control.daq_clock_reset <= '0';
      LB_control.ext_clock_reset <= '0';        
      LB_control.daq_clock_source <= '0';
      --LB_SFP
      LB_control.SFP_out_mux <= '0';    -- 8b10 out (default)
      LB_control.SFP_TxDSBL  <= '0';
      --LB_LED_GPIO
      LB_control.GPIO  <= x"0";
      LB_control.LED   <= x"0";
      --LB_TP_status
      LB_control.Test_pulse_power <= "00";
      LB_control.Test_pulse_en    <= x"0";
      local_testpulse.trigger_all_types <= '0';
      local_testpulse.pulse_channel_mask <= x"00";
      local_testpulse.start_time_count <= x"00000";
      local_testpulse.pulse_count <= x"00";
      local_testpulse.pulse_pulse_count <= x"00000";
      --C5_CMD
      C5_control.cmd_data  <= "00000";
      --C5_status
      LB_control.TDC_clock_select <= '0';  -- SFP C5 (default)
      --TDC_control
      EB_Control.TDC_enable_mask <= x"0";
      EB_Control.TDC_CDR_override_setting <= (others => (others => '0'));
      -- Fake TDCs
      enable_fake_TDCs_out <= '0';
      control_fake_TDC.spill_number <= x"000000";
      control_fake_TDC.spill_time(43 downto 0) <= x"00000000000";
      control_fake_TDC.spill_hit_count(10 downto 0) <= "00000001000";

      
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      if in_port_wr = '1' then
        case address is
          when LB_CLK_STATUS              =>
            LB_control.Ext_clock_reset <= in_port(0);
            LB_control.daq_clock_source <= in_port(1);
            LB_control.daq_clock_reset <= in_port(2);            
          when LB_TP_pulses1          =>
            local_testpulse.trigger_all_types <= in_port(31);
            local_testpulse.pulse_channel_mask <= in_port(27 downto 20);
            local_testpulse.start_time_count <= in_port(19 downto 0);
          when LB_TP_pulses2          =>
            local_testpulse.pulse_count <= in_port(27 downto 20);
            local_testpulse.pulse_pulse_count <= in_port(19 downto 0);
          when LB_SFP                 =>
            LB_control.SFP_out_mux <= in_port(4);
            LB_control.SFP_TxDSBL  <= in_port(2);
          when LB_LED_GPIO            =>
            LB_control.GPIO  <= in_port(19 downto 16);
            LB_control.LED   <= in_port(3  downto  0);
          when LB_TP_status           =>
            LB_control.Test_pulse_power <= in_port(5 downto 4);
            LB_control.Test_pulse_en    <= in_port(3 downto 0);
          when C5_CMD =>
            C5_control.cmd_data  <= in_port(4 downto 0);
          when TDC_control          =>
            EB_control.TDC_enable_mask <= in_port(3 downto 0) ;
          when TDC_0_FIFO_control   =>
            EB_control.TDC_CDR_override_setting(0) <= in_port(6 downto 4);
          when TDC_1_FIFO_control   =>
            EB_control.TDC_CDR_override_setting(1) <= in_port(6 downto 4);
          when TDC_2_FIFO_control   =>
            EB_control.TDC_CDR_override_setting(2) <= in_port(6 downto 4);
          when TDC_3_FIFO_control   =>
            EB_control.TDC_CDR_override_setting(3) <= in_port(6 downto 4);
          when FAKE_TDCS_ctrl     =>
            enable_fake_TDCs_out <= in_port(0);
          when FAKE_TDCS_spill_n              =>
            control_fake_TDC.spill_number <= in_port(23 downto 0);
          when FAKE_TDCS_spill_t1 =>
            control_fake_TDC.spill_time(43 downto 12) <= in_port;
          when FAKE_TDCS_spill_t2 =>
            control_fake_TDC.spill_time(11 downto 0) <= in_port(11 downto 0);
          when FAKE_TDCS_hit_count  =>                                     
            control_fake_TDC.spill_hit_count(10 downto 0) <= unsigned(in_port(10 downto 0));
          when C5_status            =>
            LB_control.TDC_clock_select <= in_port(3);
          when others => null;
        end case;        
      end if;      
    end if;
  end process write_access;

  -----------------------------------------------------------------------------
  -- actions
  -----------------------------------------------------------------------------
  

  action_access: process (clk, reset)
  begin  -- process action_access
    if reset = '1' then                 -- asynchronous reset (active high)
      C5_control.cmd_strobe <= '0';
      local_EB_reset <= '0';
      local_EB_control_TDC_FIFO_rd <= x"0";
      local_fake_TDC_send_spill <= '0';
      LB_control.reset_ext_monitor <= '0';
      local_EB_control_LB_FIFO_rd <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      --zero action register signals
      C5_control.cmd_strobe <= '0';
      local_EB_reset <= '0';
      local_EB_control_TDC_FIFO_rd <= x"0";
      local_fake_TDC_send_spill <= '0';
      LB_control.reset_ext_monitor <= '0';
      local_EB_control_LB_FIFO_rd <= '0';
      if in_port_wr = '1' then
        case address is
          when LB_CLK_STATUS  =>
            if in_port(3) = '1' then
              LB_control.reset_ext_monitor <= '1';
            end if;
          when EB_status              =>
            if in_port(0) = '1' then 
              local_EB_reset <= '1';
            end if;
          when EB_DEBUG_FIFO =>
            local_EB_control_LB_FIFO_rd <= '1';
          when FAKE_TDCS_ctrl     =>
            local_fake_TDC_send_spill <= in_port(31);
          when C5_CMD =>
            if in_port(8) = '1' then
              C5_control.cmd_strobe <= '1';              
            end if;
          when TDC_0_FIFO_control =>
            local_EB_control_TDC_FIFO_rd(0) <= '1';
          when TDC_1_FIFO_control =>
            local_EB_control_TDC_FIFO_rd(1) <= '1';            
          when TDC_2_FIFO_control =>
            local_EB_control_TDC_FIFO_rd(2) <= '1';
          when TDC_3_FIFO_control =>
            local_EB_control_TDC_FIFO_rd(3) <= '1';
          when others => null;
        end case;
      end if;
    end if;
  end process action_access;

  pacd_1: entity work.pacd
    port map (
      iPulseA => local_EB_reset,
      iClkA   => clk,
      iRSTAn  => '1',
      iClkB   => EB_clk,
      iRSTBn  => '1',
      oPulseB => EB_reset);
  pacd_2: entity work.pacd
    port map (
      iPulseA => local_EB_control_TDC_FIFO_rd(0),
      iClkA   => clk,
      iRSTAn  => '1',
      iClkB   => EB_clk,
      iRSTBn  => '1',
      oPulseB => EB_control.TDC_FIFO_rd(0));
  pacd_3: entity work.pacd
    port map (
      iPulseA => local_EB_control_TDC_FIFO_rd(1),
      iClkA   => clk,
      iRSTAn  => '1',
      iClkB   => EB_clk,
      iRSTBn  => '1',
      oPulseB => EB_control.TDC_FIFO_rd(1));
  pacd_4: entity work.pacd
    port map (
      iPulseA => local_EB_control_TDC_FIFO_rd(2),
      iClkA   => clk,
      iRSTAn  => '1',
      iClkB   => EB_clk,
      iRSTBn  => '1',
      oPulseB => EB_control.TDC_FIFO_rd(2));
  pacd_5: entity work.pacd
    port map (
      iPulseA => local_EB_control_TDC_FIFO_rd(3),
      iClkA   => clk,
      iRSTAn  => '1',
      iClkB   => EB_clk,
      iRSTBn  => '1',
      oPulseB => EB_control.TDC_FIFO_rd(3));

  pacd_6: entity work.pacd
    port map (
      iPulseA => local_fake_TDC_send_spill,
      iClkA   => clk,
      iRSTAn  => '1',
      iClkB   => EB_clk,
      iRSTBn  => '1',
      oPulseB => control_fake_TDC.send_spill);

  pacd_7: entity work.pacd
    port map (
      iPulseA => local_EB_control_LB_FIFO_rd,
      iClkA   => clk,
      iRSTAn  => '1',
      iClkB   => EB_clk,
      iRSTBn  => '1',
      oPulseB => EB_control.LB_FIFO_rd);

  
end architecture arch;
