-------------------------------------------------------------------------------
-- Generic counter
-- Dan Gastler
-- Process count pulses and provide a buffered value of count
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity counter is
  
  generic (
    roll_over : std_logic := '1';
    width     : integer   := 32;
    max_count : unsigned(width-1 downto 0) := (others => '1');
    min_count : unsigned(width-1 downto 0) := (others => '0'));

  port (
    clk   : in  std_logic;
    reset : in  std_logic;
    event : in  std_logic;
    count : out unsigned(width-1 downto 0);
    at_max : out std_logic;
    );

end entity counter;

architecture behavioral of counter is

  signal local_count : unsigned(width-1 downto 0) := min_count;
  
begin  -- architecture behavioral

  event_counter : process (clk, reset)
  begin  -- process malformed_counter
    if reset = '1' then                 -- asynchronous reset (active high)
      local_count <= min_count;
      count       <= min_count;
      at_max      <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      --output current counter;
      count  <= local_count;
      -- reset at_max
      at_max <= '0';

      -- count
      if event = '1' then
        if local_count = max_count then
          at_max <= '1';
          --roll over if requested
          if roll_over = '1' then
            local_count <= min_count
          end if;
        else          
          local_count <= local_count + 1;
        end if;
      end if;
    end if;
  end process malformed_counter;


end architecture behavioral;

