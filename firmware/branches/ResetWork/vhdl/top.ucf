#################################################################################
## Clock signal (nets and constraints)
NET "clk125_in"            LOC = "AB12" |IOSTANDARD = "LVCMOS33";   
Net "clk125_in" TNM_NET = sys_clk_pin;
TIMESPEC TS_sys_clk_pin = PERIOD sys_clk_pin 125000 kHz;

#################################################################################
#C5 clock I/O (nets and constraints)
NET "c5_IN_P" LOC = "B10"  |IOSTANDARD = "LVDS_33"|DIFF_TERM = TRUE;
NET "c5_IN_N" LOC = "A10"  |IOSTANDARD = "LVDS_33"|DIFF_TERM = TRUE;
Net "c5_IN_N" TNM_NET = c5_clk_pin;
Net "c5_IN_P" TNM_NET = c5_clk_pin;
TIMESPEC TS_c5_clk_pin = PERIOD c5_clk_pin 10000 kHz;
NET "TDC_C5_select" LOC = "A6" |IOSTANDARD = "LVCMOS33" ;
NET "c5_stream_P" LOC = "B12" |IOSTANDARD = "LVDS_33";
NET "c5_stream_N" LOC = "A12" |IOSTANDARD = "LVDS_33" ;


#################################################################################
# timing constraints for control register lines
NET "daq_clock_source" TIG;  # clock switching must be async

#NET "control_register_1/LB_control_daq_clock_reset"
NET "c5_top_1/cd_r" TIG;  # slow bus moves between clock domains
NET "c5_top_1/data_r*" TIG;  # slow bus moves between clock domains
#ignore timing of 125MHz reset going to C5_top reset (handled locally)
NET "control_register_1/LB_control_daq_clock_reset*" TIG;
NET "c5_top_1/local_reset*" TIG;

#ingore timing of 125Mhz reset going to C5_CDR (handled locally)
#NET "event_builder_1/C5_CDR_FIFO_1/local_reset_P" TIG;
NET "event_builder_1/reset_local<0>" TIG;

# We don't care about fine timing on this signal in the 125Mhz domain (it is slow)
NET "event_builder_1/monitoring_C5_fifo_full" TIG;  

NET "*/pacd*/t" TIG; # Single clock tick pulse crossing clock boundary
NET "*TDC_CDR_override_setting*" TIG; # Clock phase override option changes on the human time scale
NET "*Test_pulse_power*" TIG; # Test pulse power enable circuits are slow
NET "*Test_pulse_en*" TIG;

NET "control_register_1/enable_fake_TDCs_out" TIG;

# Let the monitoring and control be two clock cycles between the register and the event builder
#NET "control_register_1/EB_monitor*" TNM_NET = "EB_CONTROL_MONITOR_REG_SIDE";
#NET "control_register_1/EB_control*" TNM_NET = "EB_CONTROL_MONITOR_REG_SIDE";
#NET "event_builder_1/monitoring*" TNM_NET = "EB_CONTROL_MONITOR_EB_SIDE";
#NET "event_builder_1/control*" TNM_NET = "EB_CONTROL_MONITOR_EB_SIDE";
#TIMESPEC TS_EB_CONTROL_MONITOR = FROM EB_CONTROL_MONITOR_REG_SIDE TO EB_CONTROL_MONITOR_EB_SIDE 16 ns;



#################################################################################
# general nets
#SFP IO
NET "SFP_SCL" LOC = "H1" |IOSTANDARD = "LVCMOS33";
NET "SFP_SDA" LOC = "G1" |IOSTANDARD = "LVCMOS33";
NET "SFP_LOS" LOC = "K1" |IOSTANDARD = "LVCMOS33";
NET "SFP_Present" LOC = "J1" |IOSTANDARD = "LVCMOS33";
NET "SFP_TxDSBL" LOC = "F1" |IOSTANDARD = "LVCMOS33";

#RS422 interface
NET "SC_OE" LOC = "E1" |IOSTANDARD = "LVCMOS33";
NET "SC_OUT" LOC = "D1" |IOSTANDARD = "LVCMOS33";
NET "SC_IN" LOC = "C1" |IOSTANDARD = "LVCMOS33";
NET "SC_N_Present" LOC = "B1" |IOSTANDARD = "LVCMOS33";

#user I/O
NET "LED<3>" LOC = "B6" |IOSTANDARD = "LVCMOS33";
NET "LED<2>" LOC = "A7" |IOSTANDARD = "LVCMOS33";
NET "LED<1>" LOC = "A8" |IOSTANDARD = "LVCMOS33";
NET "LED<0>" LOC = "A9" |IOSTANDARD = "LVCMOS33";
NET "GPIO<3>" LOC = "AB5" |IOSTANDARD = "LVCMOS33";
NET "GPIO<2>" LOC = "AB4" |IOSTANDARD = "LVCMOS33";
NET "GPIO<1>" LOC = "AB3" |IOSTANDARD = "LVCMOS33";
NET "GPIO<0>" LOC = "AB2" |IOSTANDARD = "LVCMOS33";
#NET "GPIO_Tx" LOC = "AB3" | IOSTANDARD = "LVCMOS33";
#NET "GPIO_Rx" LOC = "AB2" | IOSTANDARD = "LVCMOS33";

#TDC UART
NET "TDC_Tx" LOC = "A20" |IOSTANDARD = "I2C"  |DRIVE = "2" |SLEW = "QUIETIO";
NET "TDC_Rx" LOC = "A21" |IOSTANDARD = "I2C" |DRIVE = "2" |SLEW = "QUIETIO";

#DAC i2c
NET "DAC_SCL" LOC = "A13" |IOSTANDARD = "I2C";
NET "DAC_SDA" LOC = "A14" |IOSTANDARD = "I2c";

#DB i2c
NET "DB_SCL" LOC = "L1" |IOSTANDARD = "I2C";
NET "DB_SDA" LOC = "M1" |IOSTANDARD = "I2C";
#NET "DB_INT" LOC = "N1" | IOSTANDARD = "LVCMOS33";

#temperature sensor bus
#NET "TEMP<0>" LOC = "E22" | IOSTANDARD = "LVCMOS33";
#NET "TEMP<1>" LOC = "G22" | IOSTANDARD = "LVCMOS33";
#NET "TEMP_PWR<0>" LOC = "C22" | IOSTANDARD = "LVCMOS33";
#NET "TEMP_PWR<1>" LOC = "F22" | IOSTANDARD = "LVCMOS33";
#TDC input streams
NET "TDC_serial_N<0>" LOC = "D22" |IOSTANDARD = "LVDS_33" |DIFF_TERM = TRUE;
NET "TDC_serial_P<0>" LOC = "D21" |IOSTANDARD = "LVDS_33" |DIFF_TERM = TRUE;
NET "TDC_serial_N<1>" LOC = "A16" |IOSTANDARD = "LVDS_33" |DIFF_TERM = TRUE;
NET "TDC_serial_P<1>" LOC = "B16" |IOSTANDARD = "LVDS_33" |DIFF_TERM = TRUE;
NET "TDC_serial_N<2>" LOC = "B22" |IOSTANDARD = "LVDS_33" |DIFF_TERM = TRUE;
NET "TDC_serial_P<2>" LOC = "B21" |IOSTANDARD = "LVDS_33" |DIFF_TERM = TRUE;
NET "TDC_serial_N<3>" LOC = "A18" |IOSTANDARD = "LVDS_33" |DIFF_TERM = TRUE;
NET "TDC_serial_P<3>" LOC = "B18" |IOSTANDARD = "LVDS_33" |DIFF_TERM = TRUE;
#NET "TDC_serial_N<0>" LOC = "A18" |IOSTANDARD = "LVDS_33" | DIFF_TERM = TRUE;
#NET "TDC_serial_P<0>" LOC = "B18" |IOSTANDARD = "LVDS_33" | DIFF_TERM = TRUE;
#NET "TDC_serial_N<1>" LOC = "B22" |IOSTANDARD = "LVDS_33" | DIFF_TERM = TRUE;
#NET "TDC_serial_P<1>" LOC = "B21" |IOSTANDARD = "LVDS_33" | DIFF_TERM = TRUE;
#NET "TDC_serial_N<2>" LOC = "A16" |IOSTANDARD = "LVDS_33" | DIFF_TERM = TRUE;
#NET "TDC_serial_P<2>" LOC = "B16" |IOSTANDARD = "LVDS_33" | DIFF_TERM = TRUE;
#NET "TDC_serial_N<3>" LOC = "D22" |IOSTANDARD = "LVDS_33" | DIFF_TERM = TRUE;
#NET "TDC_serial_P<3>" LOC = "D21" |IOSTANDARD = "LVDS_33" | DIFF_TERM = TRUE;


#output to TRM
NET "Data_out_N" LOC = "A5" |IOSTANDARD = "LVDS_33" ;
NET "Data_out_P" LOC = "C5" |IOSTANDARD = "LVDS_33" ;


#JTAG
#NET "TDO<0>" LOC = "J22" | IOSTANDARD = "LVCMOS33";
#NET "TDO<1>" LOC = "T22" | IOSTANDARD = "LVCMOS33";
#NET "TDI<0>" LOC = "M22" | IOSTANDARD = "LVCMOS33";
#NET "TDI<1>" LOC = "R22" | IOSTANDARD = "LVCMOS33";
#NET "TMS<0>" LOC = "L22" | IOSTANDARD = "LVCMOS33";
#NET "TMS<1>" LOC = "P22" | IOSTANDARD = "LVCMOS33";
#NET "TCK<0>" LOC = "K22" | IOSTANDARD = "LVCMOS33";
#NET "TCK<1>" LOC = "N22" | IOSTANDARD = "LVCMOS33";
#Power control
NET "EN_N1V6" LOC = "H22" |IOSTANDARD = "LVCMOS33";
NET "EN_P1V6" LOC = "AA12" |IOSTANDARD = "LVCMOS33";

#Test Pulse
NET "ASDQ_TP<0>" LOC = "V22" |IOSTANDARD = "LVCMOS33";
NET "ASDQ_TP<1>" LOC = "W22" |IOSTANDARD = "LVCMOS33";
NET "ASDQ_TP<2>" LOC = "AB17" |IOSTANDARD = "LVCMOS33";
NET "ASDQ_TP<3>" LOC = "AB16" |IOSTANDARD = "LVCMOS33";
NET "ASDQ_TP<4>" LOC = "AA21" |IOSTANDARD = "LVCMOS33";
NET "ASDQ_TP<5>" LOC = "AB21" |IOSTANDARD = "LVCMOS33";
NET "ASDQ_TP<6>" LOC = "AB15" |IOSTANDARD = "LVCMOS33";
NET "ASDQ_TP<7>" LOC = "AB14" |IOSTANDARD = "LVCMOS33";


NET "ASDQ_TP_EN<0>" LOC = "U22" |IOSTANDARD = "LVCMOS33";
NET "ASDQ_TP_EN<1>" LOC = "AB19" |IOSTANDARD = "LVCMOS33";
NET "ASDQ_TP_EN<2>" LOC = "AB18" |IOSTANDARD = "LVCMOS33";
NET "ASDQ_TP_EN<3>" LOC = "AB13" |IOSTANDARD = "LVCMOS33";

#temp monitors
NET "TEMP_SENSE<0>" LOC = "E22" |IOSTANDARD = "LVCMOS33";
NET "TEMP_SENSE<1>" LOC = "G22" |IOSTANDARD = "LVCMOS33";

#NET "TEMP_SENSE_PWR_DISABLE<0>" LOC = "C22" |IOSTANDARD = "LVCMOS33" | PULLUP;
#NET "TEMP_SENSE_PWR_DISABLE<1>" LOC = "F22" |IOSTANDARD = "LVCMOS33" | PULLUP;
