source vhdl/interface_uC/sim/UART_helper.tcl

#set initial conditions for UART
isim force add {/uc/sc_rx} 1
isim force add {/uc/reset} 0

#set clock on /uc/clk
isim force add {/uc/clk} 1 -radix bin -value 0 -radix bin -time 5 ns -repeat 10 ns 

#let everything start up
run 1 ms


#send reset string
#sendUART_str reset 115200 /uc/sc_rx;
run 2 ms
#send BS (0x08)
sendUART_hex 08 115200 /uc/sc_rx;

#run 10 ms

#send CR (0x0D)
#sendUART_hex 0D 115200 /uc/sc_rx;

run 1 ms
