source vhdl/interface_uC/sim/UART_helper.tcl

#set initial conditions for UART
isim force add {/uc/sc_rx} 1
isim force add {/uc/reset} 0

#set clock on /uc/clk
isim force add {/uc/clk} 1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns 

isim force add {/uc/reg_data_in} BADCAFE0 -radix hex 

#let everything start up
run 2 ms

#send i2c_reg_rd command
sendUART_str daq_reg_rd 115200 /uc/sc_rx;
#send space (0x20)
sendUART_hex 20 115200 /uc/sc_rx;
sendUART_str 00 115200 /uc/sc_rx;
#send CR (0x0D)
sendUART_hex 0D 115200 /uc/sc_rx;
run 1 ms



#send i2c_reg_rd command
sendUART_str daq_reg_wr 115200 /uc/sc_rx;
#send space (0x20)
sendUART_hex 20 115200 /uc/sc_rx;
sendUART_str 00 115200 /uc/sc_rx;
#send space (0x20)
sendUART_hex 20 115200 /uc/sc_rx;
sendUART_str 12345678 115200 /uc/sc_rx;
#send CR (0x0D)
sendUART_hex 0D 115200 /uc/sc_rx;
run 1 ms

#send i2c_reg_rd command
sendUART_str daq_reg_wr 115200 /uc/sc_rx;
#send space (0x20)
sendUART_hex 20 115200 /uc/sc_rx;
sendUART_str 34 115200 /uc/sc_rx;
#send space (0x20)
sendUART_hex 20 115200 /uc/sc_rx;
sendUART_str 12345 115200 /uc/sc_rx;
#send CR (0x0D)
sendUART_hex 0D 115200 /uc/sc_rx;
run 1 ms
