library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

entity one_wire_master is
  port (
    clk62_5        : in  std_logic;
    reset          : in  std_logic;

    wire_in        : in  std_logic;
    wire_out       : out std_logic;
    wire_T         : out std_logic;
    
    bit_rd         : out std_logic;
    bit_wr         : in std_logic;
    bit_mode       : in std_logic_vector(1 downto 0); --3 power on;2 reset; 1 read;0 write   old -- 1 reset, 0: r/w(1/0)
    bit_op_go      : in std_logic;
    bit_op_done    : out std_logic
    );

end entity one_wire_master;

architecture Behaviour of one_wire_master is

  component one_wire_timing is
    port (
      clk_in         : in  std_logic;
      reset          : in  std_logic;
      count_max      : in  unsigned(31 downto 0);
      clk_en_onewire : out std_logic);
  end component one_wire_timing;

  component one_wire_io is
    port (
      clk        : in    std_logic;
      reset      : in    std_logic;
      clk_en     : in    std_logic;
      wire_in    : in    std_logic;
      wire_out   : out   std_logic;
      wire_T     : out   std_logic;
      bit_in     : out   std_logic;
      bit_out    : in    std_logic;      
      go         : in    std_logic;
      bit_mode   : in    std_logic_vector(1 downto 0);
      done       : out   std_logic);
  end component one_wire_io;
  
  signal local_reset : std_logic := '0';
  signal en_clk : std_logic := '0';

begin  -- architecture Behaviour

  local_reset <= reset;

  one_wire_timing_1: entity work.one_wire_timing
    port map (
      clk_in         => clk62_5,
      reset          => local_reset,
      count_max      => x"0000003F",
      clk_en_onewire => en_clk);

  one_wire_io_1: entity work.one_wire_io
    port map (
      clk        => clk62_5,
      reset      => local_reset,
      clk_en     => en_clk,
      wire_in    => wire_in,
      wire_out   => wire_out,
      wire_T     => wire_T,
      bit_in     => bit_rd,
      bit_out    => bit_wr,
      go         => bit_op_go,
      bit_mode   => bit_mode,
      done       => bit_op_done);

  
  
  
end architecture Behaviour;
