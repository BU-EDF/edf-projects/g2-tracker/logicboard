#source vhdl/EventBuilder/C5_CDR_FIFO/sim/c5_helper.tcl
restart
source vhdl/sim/c5_helper.tcl

isim force add {/c5_cdr_fifo/clk125} 1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns 
isim force add {/c5_cdr_fifo/reset} 0 -radix bin 
isim force add {/c5_cdr_fifo/spill_rd} 0 -radix bin 

for { set i 0} {$i < 20 } {incr i} {
send_cycle 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
}

#send spill
send_c5_data C /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
for { set i 0} {$i < 20 } {incr i} {
send_cycle 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
}

#send spill
send_c5_data 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
for { set i 0} {$i < 20 } {incr i} {
send_cycle 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
}

#send nex spill cal
send_c5_data D /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
for { set i 0} {$i < 20 } {incr i} {
send_cycle 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
}

#send spill
send_c5_data C /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
for { set i 0} {$i < 20 } {incr i} {
send_cycle 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
}

#send spill
send_c5_data C /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
for { set i 0} {$i < 20 } {incr i} {
send_cycle 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
}

#send spill number reset
send_c5_data F /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
for { set i 0} {$i < 20 } {incr i} {
send_cycle 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
}
#send time reset
send_c5_data E /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
for { set i 0} {$i < 20 } {incr i} {
send_cycle 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
}

#send spill
send_c5_data C /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
for { set i 0} {$i < 20 } {incr i} {
send_cycle 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
}
#send spill
send_c5_data C /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
for { set i 0} {$i < 20 } {incr i} {
send_cycle 0 /c5_cdr_fifo/c5_raw /c5_cdr_fifo/clk40
}



# source vhdl/EventBuilder/C5_CDR_FIFO/sim/c5_cdr_fifo.tcl
