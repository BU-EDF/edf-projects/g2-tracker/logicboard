#!/usr/bin/env python

import re
import os


def search_picoblaze_file(filename):
    inFile = open(filename,"r")
    inASM = inFile.read()
    regex = re.compile(" *CONSTANT (.*_PORT) *,.*(\w*)")
    port_addrs = regex.findall(inASM)
    for port,addr in port_addrs:
        print port,addr
#       print hex(int(addr,16)), ": ", port
    




#process the uC vhdl file
uC_File= open("uC.vhd","r")
uC_vhdl= uC_File.read()
regex = re.compile("--*..*\n *--(.*)\n *when *x\"(\w*)\"",re.MULTILINE)
port_addrs = regex.findall(uC_vhdl)
print "uC.vhd"
for port,addr in port_addrs:
    print hex(int(addr,16)), ": ", port


#process all picoblaze files
for file in os.listdir("./picoblaze/"):
    if file.endswith(".psm"):
        name="./picoblaze/"+file
        print name
        search_picoblaze_file(name)

