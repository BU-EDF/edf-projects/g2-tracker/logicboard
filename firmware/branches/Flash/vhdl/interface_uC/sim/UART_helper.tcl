# send a binary string to $rx_path signal at baud rate $baud
# ex. send r @ 115200
#     >sendUART_bin 01110010 115200 /top/rsrx;       
proc sendUART_bin { bs baud rx_path} {
    puts "Sending $bs"
    set wait_time [expr 1000000.0 / $baud ]
    set n [string length $bs]
    run 100 us
    isim force add $rx_path 0
    run $wait_time us
    for { set i $n } {$i > 0 } {incr i -1} {
	set index [expr $i - 1]
        set bv [string index $bs $index ]
	if {$bv == "1"} {
	    isim force add $rx_path 1
	} else {
	    isim force add $rx_path 0
	}
	run $wait_time us
    }
    isim force add $rx_path 1
    run $wait_time us
}

# send a hex byte to "$rx_path" signal at baud rate $baud
# ex. send r @ 115200
#     >sendUART_hex 72 115200;       
proc sendUART_hex { hex_byte baud rx_path} {
    set byte [binary format H* $hex_byte]
    binary scan $byte B* bs var2
    sendUART_bin $bs $baud $rx_path
}

# send a ascii char to "$rx_path" signal at baud rate $baud
# ex. send r @ 115200
#     >sendUART_ascii r 115200;       
proc sendUART_ascii { ascii_char baud rx_path} {
    set dec_str [scan $ascii_char %c]
    set hex_byte [format %1X $dec_str]
    sendUART_hex $hex_byte $baud $rx_path
}

# send a string of ascii chars to "$rx_path" signal at baud rate $baud
# ex. send "reset" @ 115200
#     >sendUART_str reset 115200;       
proc sendUART_str { line baud rx_path} {
    set n [string length $line]
    for { set i 0 } {$i < $n } {incr i 1} {
	set char [string index $line $i]
	sendUART_ascii $char $baud $rx_path
    }
}
