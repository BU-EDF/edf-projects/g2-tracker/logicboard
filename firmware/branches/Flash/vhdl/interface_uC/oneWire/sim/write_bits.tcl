restart 
proc write_bit { bit_out bit_go bit_rw bit } {
    isim force add $bit_out $bit
    isim force add $bit_rw 0
    isim force add $bit_go 1
    run 8ns
    isim force add $bit_go 0
    run 66 us
}

proc read_bit { bit_wire bit_go bit_rw bit } {
    isim force add $bit_rw 1
    isim force add $bit_go 1
    run 8ns
    isim force add $bit_go 0
    run 10 us
    isim force add $bit_wire $bit    
    run 56 us
    isim force remove $bit_wire
}

proc ow_reset { reset go } {
    isim force add $reset 1
    isim force add $go 1
    run 8ns
    isim force add $reset 0
    isim force add $go 0
    run 2ms
}

isim force add {/one_wire_master/clk125} 1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns
isim force add {/one_wire_master/send_reset} 0
isim force add {/one_wire_master/reset} 1
run 100 ns
isim force add {/one_wire_master/reset} 0
run 1 ms

ow_reset /one_wire_master/send_reset /one_wire_master/bit_go
run 2ms

write_bit /one_wire_master/bit_out /one_wire_master/bit_go /one_wire_master/bit_rd_wr 0
write_bit /one_wire_master/bit_out /one_wire_master/bit_go /one_wire_master/bit_rd_wr 0
write_bit /one_wire_master/bit_out /one_wire_master/bit_go /one_wire_master/bit_rd_wr 0
write_bit /one_wire_master/bit_out /one_wire_master/bit_go /one_wire_master/bit_rd_wr 1
write_bit /one_wire_master/bit_out /one_wire_master/bit_go /one_wire_master/bit_rd_wr 1
write_bit /one_wire_master/bit_out /one_wire_master/bit_go /one_wire_master/bit_rd_wr 1
write_bit /one_wire_master/bit_out /one_wire_master/bit_go /one_wire_master/bit_rd_wr 0
write_bit /one_wire_master/bit_out /one_wire_master/bit_go /one_wire_master/bit_rd_wr 1
run 1 ms

read_bit /one_wire_master/wire /one_wire_master/bit_go /one_wire_master/bit_rd_wr 0
read_bit /one_wire_master/wire /one_wire_master/bit_go /one_wire_master/bit_rd_wr 0
read_bit /one_wire_master/wire /one_wire_master/bit_go /one_wire_master/bit_rd_wr 0
read_bit /one_wire_master/wire /one_wire_master/bit_go /one_wire_master/bit_rd_wr 1
read_bit /one_wire_master/wire /one_wire_master/bit_go /one_wire_master/bit_rd_wr 1
read_bit /one_wire_master/wire /one_wire_master/bit_go /one_wire_master/bit_rd_wr 1
read_bit /one_wire_master/wire /one_wire_master/bit_go /one_wire_master/bit_rd_wr 0
read_bit /one_wire_master/wire /one_wire_master/bit_go /one_wire_master/bit_rd_wr 1
run 1ms
ow_reset /one_wire_master/send_reset /one_wire_master/bit_go
run 2ms
