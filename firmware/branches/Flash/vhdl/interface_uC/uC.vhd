-------------------------------------------------------------------------------
-- top.vhd :
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed."+";
use ieee.std_logic_signed."=";

use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

use work.EventBuilder_IO.all;
entity uC is

  port (
    clk   : in std_logic;               -- 100MHz oscillator input
    reset : in std_logic;               -- reset to picoblaze

    -- SC_INT
    SC_Rx : in  std_logic;              -- serial in
    SC_Tx : out std_logic := '1';       -- serial out
    SC_NC : in  std_logic;              -- SC not connected
    SC_OE : out std_logic;              -- SC output enable

    TDC_Rx : in  std_logic;
    TDC_Tx : out std_logic;

    -- DAC i2c
    DAC_SDA_in  : in  std_logic;
    DAC_SDA_out : out std_logic;
    DAC_SDA_en  : out std_logic;
    DAC_SCL     : out std_logic := '1';

    -- DB i2c
    DB_SDA_in  : in  std_logic;
    DB_SDA_out : out std_logic;
    DB_SDA_en  : out std_logic;
    DB_SCL     : out std_logic := '1';


--    JTAG_TDO : in std_logic_vector(1 downto 0);
--    JTAG_TDI : out std_logic_vector(1 downto 0);
--    JTAG_TSM : out std_logic_vector(1 downto 0);
--    JTAG_TCK : out std_logic_vector(1 downto 0);

    -- one wire temp sensors DS1820
    Wire_DS1820_in     : in  std_logic_vector(1 downto 0);
    Wire_DS1820_out    : out std_logic_vector(1 downto 0) := "11";
    Wire_DS1820_T      : out   std_logic_vector(1 downto 0) := "11";

    --FLASH interface
    FLASH_SPI_CLK   : out std_logic := '1';
    FLASH_SPI_MOSI  : out std_logic := '1';
    FLASH_SPI_MISO  : in std_logic;
    FLASH_SPI_CS    : out std_logic := '1';

    -- register interface
    reg_addr       : out std_logic_vector(7 downto 0);
    reg_data_in_rd : out std_logic;
    reg_data_in    : in  std_logic_vector(31 downto 0);
    reg_data_in_dv : in  std_logic;
    reg_data_out   : out std_logic_vector(31 downto 0);
    reg_wr         : out std_logic
    );


end entity uC;

architecture arch of uC is

--
-- KCPSM6 uc
-- 
  component kcpsm6
    generic(
      hwbuild                 : std_logic_vector(7 downto 0)  := X"00";
      interrupt_vector        : std_logic_vector(11 downto 0) := X"600";
      scratch_pad_memory_size : integer                       := 256);
    port (
      address        : out std_logic_vector(11 downto 0);
      instruction    : in  std_logic_vector(17 downto 0);
      bram_enable    : out std_logic;
      in_port        : in  std_logic_vector(7 downto 0);
      out_port       : out std_logic_vector(7 downto 0);
      port_id        : out std_logic_vector(7 downto 0);
      write_strobe   : out std_logic;
      k_write_strobe : out std_logic;
      read_strobe    : out std_logic;
      interrupt      : in  std_logic;
      interrupt_ack  : out std_logic;
      sleep          : in  std_logic;
      reset          : in  std_logic;
      clk            : in  std_logic);
  end component;

--
-- KCPSM6 ROM
-- 
  component cli
    generic(
      C_FAMILY             : string  := "S6";
      C_RAM_SIZE_KWORDS    : integer := 1;
      C_JTAG_LOADER_ENABLE : integer := 0);
    port (
      address     : in  std_logic_vector(11 downto 0);
      instruction : out std_logic_vector(17 downto 0);
      enable      : in  std_logic;
      rdl         : out std_logic;
      clk         : in  std_logic);
  end component;

--
-- UART Transmitter     
--
  component uart_tx6
    port (
      data_in             : in  std_logic_vector(7 downto 0);
      en_16_x_baud        : in  std_logic;
      serial_out          : out std_logic;
      buffer_write        : in  std_logic;
      buffer_data_present : out std_logic;
      buffer_half_full    : out std_logic;
      buffer_full         : out std_logic;
      buffer_reset        : in  std_logic;
      clk                 : in  std_logic);
  end component;

--
-- UART Receiver
--
  component uart_rx6
    port (
      serial_in           : in  std_logic;
      en_16_x_baud        : in  std_logic;
      data_out            : out std_logic_vector(7 downto 0);
      buffer_read         : in  std_logic;
      buffer_data_present : out std_logic;
      buffer_half_full    : out std_logic;
      buffer_full         : out std_logic;
      buffer_reset        : in  std_logic;
      clk                 : in  std_logic);
  end component;

  component TDC_UART_master is
    port (
      clk62_5         : in  std_logic;
      reset          : in  std_logic;
      rx             : in  std_logic;
      tx             : out std_logic;
      data_out       : in  std_logic_vector(7 downto 0);
      data_out_wr    : in  std_logic;
      data_out_empty : out std_logic;
      data_in        : out std_logic_vector(7 downto 0);
      data_in_valid  : out std_logic;
      rx_error       : out std_logic_vector(1 downto 0);
      data_in_rd     : in  std_logic);
  end component TDC_UART_master;

  component one_wire_master is
    port (
      clk62_5     : in  std_logic;
      reset       : in  std_logic;
      wire_in     : in  std_logic;
      wire_out    : out std_logic;
      wire_T      : out std_logic;
      bit_rd      : out std_logic;
      bit_wr      : in  std_logic;
      bit_mode    : in  std_logic_vector(1 downto 0);
      bit_op_go   : in  std_logic;
      bit_op_done : out std_logic);
  end component one_wire_master;

-----------------------------------------------------------------------------
-- Signals
-----------------------------------------------------------------------------
  signal rst : std_logic;

--
-- KCPSM6 & ROM signals
--
  signal address        : std_logic_vector(11 downto 0);
  signal instruction    : std_logic_vector(17 downto 0);
  signal bram_enable    : std_logic;
  signal in_port        : std_logic_vector(7 downto 0) := "00000000";
  signal out_port       : std_logic_vector(7 downto 0) := "00000000";
  signal port_id        : std_logic_vector(7 downto 0) := "00000000";
  signal write_strobe   : std_logic                    := '0';
  signal k_write_strobe : std_logic                    := '0';
  signal read_strobe    : std_logic                    := '0';
  signal interrupt      : std_logic;
  signal interrupt_ack  : std_logic;
  signal kcpsm6_sleep   : std_logic;
  signal kcpsm6_reset   : std_logic;

--
-- UART_TX signals
--
--  signal SC_Tx_local          : std_logic := '1';
  signal uart_tx_data_in      : std_logic_vector(7 downto 0);
  signal write_to_uart_tx     : std_logic;
  signal uart_tx_data_present : std_logic;
  signal uart_tx_half_full    : std_logic;
  signal uart_tx_full         : std_logic;
  signal uart_tx_reset        : std_logic := '0';

--
-- UART_RX signals
--
  signal SC_Rx_local          : std_logic := '1';
  signal uart_rx_data_out     : std_logic_vector(7 downto 0);
  signal read_from_uart_rx    : std_logic;
  signal uart_rx_data_present : std_logic;
  signal uart_rx_half_full    : std_logic;
  signal uart_rx_full         : std_logic;
  signal uart_rx_reset        : std_logic := '0';

--
-- UART baud rate signals
--
  signal baud_count   : integer range 0 to 67 := 0;
  signal en_16_x_baud : std_logic             := '0';

--
-- I2c signals
--
  signal i2c_sel     : std_logic_vector(1 downto 0) := "00";
  signal i2c_SDA_in  : std_logic;
  signal i2c_SDA_out : std_logic                    := '1';
  signal i2c_SCL_out : std_logic                    := '1';

--
-- DAQ register signals
--
  signal DAQ_reg_local_addr     : std_logic_vector(7 downto 0) := x"00";
  signal daq_reg_local_data_in  : std_logic_vector(31 downto 0);
  signal daq_reg_local_data_out : std_logic_vector(31 downto 0);
  signal daq_reg_local_valid    : std_logic                    := '0';
  signal DAQ_reg_local_wr       : std_logic                    := '0';
  signal DAQ_reg_local_rd       : std_logic                    := '0';
--  signal DAQ_word_addr          : std_logic_vector(1 downto 0) := "00";


--  signal DAQ_reg_data_in_buf : std_logic_vector(31 downto 0);
--  
--  signal DAQ_addr : std_logic_vector(7 downto 0) := x"00";
--  signal DAQ_word_addr : std_logic_vector(1 downto 0) := "00";
--  signal DAQ_reg_read : std_logic_vector(31 downto 0);  
--  signal DAQ_reg_write : std_logic_vector(31 downto 0);
--
--  signal reg_addr_buf     : std_logic_vector(7 downto 0);
--  signal reg_data_in_buf  : std_logic_vector(31 downto 0);
--  signal reg_data_out_buf : std_logic_vector(31 downto 0);


--
-- TDC UART
--
  signal TDC_data_out       : std_logic_vector(7 downto 0) := x"FF";
  signal TDC_data_out_wr    : std_logic                    := '0';
  signal TDC_data_out_empty : std_logic                    := '1';
  signal TDC_data_in        : std_logic_vector(7 downto 0) := x"FF";
  signal TDC_data_in_valid  : std_logic                    := '0';
  signal TDC_data_in_rd     : std_logic                    := '0';
  signal TDC_rx_error       : std_logic_vector(1 downto 0) := "00";
  signal TDC_reset          : std_logic                    := '1';


--
-- one wire
--
  type DS1820 is record
                   rd    : std_logic;
                   wr    : std_logic;
                   done  : std_logic;
                   mode  : std_logic_vector(1 downto 0);
                   go    : std_logic;
  end record DS1820;

  signal onewire_A : DS1820 := ('0','0','0',"10",'0');
  signal onewire_B : DS1820 := ('0','0','0',"10",'0');

begin  -- architecture arch


-----------------------------------------------------------------------------------------
-- Instantiate KCPSM6 and connect to Program Memory
-----------------------------------------------------------------------------------------
  processor : kcpsm6
    generic map (
      hwbuild                 => X"01",
      interrupt_vector        => X"7FF",
      scratch_pad_memory_size => 256)
    port map(
      address        => address,
      instruction    => instruction,
      bram_enable    => bram_enable,
      port_id        => port_id,
      write_strobe   => write_strobe,
      k_write_strobe => k_write_strobe,
      out_port       => out_port,
      read_strobe    => read_strobe,
      in_port        => in_port,
      interrupt      => interrupt,
      interrupt_ack  => interrupt_ack,
      sleep          => kcpsm6_sleep,
      reset          => rst,
      clk            => clk);

--
--Disable sleep and interrupts on kcpsm6
--
  kcpsm6_sleep <= '0';
  interrupt    <= interrupt_ack;
  rst          <= kcpsm6_reset or reset;

--
-- KCPSM6 ROM
--
  program_rom : cli
    generic map(
      C_FAMILY             => "S6",     --Family 'S6', 'V6' or '7S'
      C_RAM_SIZE_KWORDS    => 4,        --Program size '1', '2' or '4'
      C_JTAG_LOADER_ENABLE => 1)        --Include JTAG Loader when set to '1' 
    port map(
      address     => address,
      instruction => instruction,
      enable      => bram_enable,
      rdl         => kcpsm6_reset,
      clk         => clk);

--
-- UART Transmitter
--

  SC_OE <= '1';
  tx : uart_tx6
    port map (
      data_in             => uart_tx_data_in,
      en_16_x_baud        => en_16_x_baud,
      serial_out          => SC_Tx,
      buffer_write        => write_to_uart_tx,
      buffer_data_present => uart_tx_data_present,
      buffer_half_full    => uart_tx_half_full,
      buffer_full         => uart_tx_full,
      buffer_reset        => uart_tx_reset,
      clk                 => clk);
--
-- UART Receiver
--
  rx : uart_rx6
    port map (
      serial_in           => SC_Rx,
      en_16_x_baud        => en_16_x_baud,
      data_out            => uart_rx_data_out,
      buffer_read         => read_from_uart_rx,
      buffer_data_present => uart_rx_data_present,
      buffer_half_full    => uart_rx_half_full,
      buffer_full         => uart_rx_full,
      buffer_reset        => uart_rx_reset,
      clk                 => clk);

  -- add a buffer to this to ease timing
--  SC_UART_Buffer: process (clk) is
--  begin  -- process SC_UART_Buffer
--    if clk'event and clk = '1' then  -- rising clock edge
--      SC_Rx_local <= SC_Rx;
--      SC_Tx <= SC_Tx_local;
--    end if;
--  end process SC_UART_Buffer;

-----------------------------------------------------------------------------------------
-- RS232 (UART) baud rate 
-----------------------------------------------------------------------------------------
--
-- To set serial communication baud rate to 115,200 then en_16_x_baud must pulse 
-- High at 1,843,200Hz which is every 54 cycles at 100MHz. In this implementation 
-- a pulse is generated every 54 cycles resulting is a baud rate of 110,000 baud 
--

  baud_rate : process(clk)
  begin
    if clk'event and clk = '1' then
--      if baud_count = 67 then           -- counts 68 states including zero
      if baud_count = 33 then           -- counts 34 states including zero
        baud_count   <= 0;
        en_16_x_baud <= '1';            -- single cycle enable pulse
      else
        baud_count   <= baud_count + 1;
        en_16_x_baud <= '0';
      end if;        
    end if;
  end process baud_rate;






-------------------------------------------------------------------------------
-- DAQ interface
-------------------------------------------------------------------------------
  DAQ_interface : process (clk, reset) is
  begin  -- process DAQ_interface
    if reset = '1' then                 -- asynchronous reset (active high)
      DAQ_reg_local_valid <= '0';
      reg_data_in_rd      <= '0';
      reg_addr            <= x"00";
      reg_data_out        <= x"00000000";
      reg_wr              <= '0';
      DAQ_reg_local_valid <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- update address
      reg_addr <= DAQ_reg_local_addr;

      -- request a read from the register
      reg_data_in_rd <= '0';
      if DAQ_reg_local_rd = '1' then
        -- request the read
        reg_data_in_rd      <= '1';
        -- reset the local valid
        DAQ_reg_local_valid <= '0';
        DAQ_reg_local_data_in <= x"ABADCAFE";
      end if;

      -- latch reads when valid
      if reg_data_in_dv = '1' then
        DAQ_reg_local_data_in <= reg_data_in;
        DAQ_reg_local_valid   <= '1';
      end if;

      -- write data
      reg_wr <= '0';
      if DAQ_reg_local_wr = '1' then
        reg_data_out <= DAQ_reg_local_data_out;
        reg_wr       <= '1';
      end if;
    end if;
  end process DAQ_interface;

  
-----------------------------------------------------------------------------
-- Reading interface of the UC to the UART
-----------------------------------------------------------------------------
  input_ports : process(clk)
  begin
    if clk'event and clk = '1' then
      case port_id(3 downto 0) is
        -----------------------------------------------------------------------
        -- UART status port
        when x"0" =>
          in_port(0)          <= uart_tx_data_present;
          in_port(1)          <= uart_tx_half_full;
          in_port(2)          <= uart_tx_full;
          in_port(3)          <= uart_rx_data_present;
          in_port(4)          <= uart_rx_half_full;
          in_port(5)          <= uart_rx_full;
          in_port(6)          <= SC_NC;
          in_port(7)          <= '0';
        -----------------------------------------------------------------------
        -- UART input port
        when x"1" =>
          -- (see 'buffer_read' pulse generation below) 
          in_port <= uart_rx_data_out;
        -----------------------------------------------------------------------
        -- TDC i2c bus;
        when x"2" =>
          -- we do not support clock stretching right now
          in_port(0)          <= '0';   --TDC_SCL;
          --
          in_port(1)          <= i2c_SDA_in;
          in_port(7 downto 2) <= "000000";
        ----------------------------------------------------------------------
        -- DAQ register control
        when x"3" =>
          in_port <= "000" & DAQ_reg_local_valid & "0000";
        ----------------------------------------------------------------------
        -- DAQ register addr
        when x"4" =>
          in_port <= DAQ_reg_local_addr;
        -----------------------------------------------------------------------
        -- DAQ input byte 0
        when x"5" =>
          in_port <= DAQ_reg_local_data_in(7 downto 0);
        -----------------------------------------------------------------------
        -- DAQ input byte 1
        when x"6" =>
          in_port <= DAQ_reg_local_data_in(15 downto 8);
        -----------------------------------------------------------------------
        -- DAQ input byte 2
        when x"7" =>
          in_port <= DAQ_reg_local_data_in(23 downto 16);
        -----------------------------------------------------------------------
        -- DAQ input byte 3
        when x"8" =>
          in_port <= DAQ_reg_local_data_in(31 downto 24);
        -----------------------------------------------------------------------
        -- TDC UART status
        when x"9" =>
          in_port <= x"0" & TDC_rx_error & TDC_data_in_valid & TDC_data_out_empty;
        -----------------------------------------------------------------------
        -- TDC UART data in
        when x"A" =>
          in_port <= TDC_data_in;

        -----------------------------------------------------------------------
        -- one wire sensors
        when x"B" =>
          --A bus
          in_port(0)          <= onewire_A.rd;
          in_port(1)          <= onewire_A.done;
          in_port(3 downto 2) <= onewire_A.mode;
          --B bus
          in_port(4)          <= onewire_B.rd;
          in_port(5)          <= onewire_B.done;
          in_port(7 downto 6) <= onewire_B.mode;

        -----------------------------------------------------------------------
        -- Flash
        when x"C" =>
          in_port(2) <= FLASH_SPI_MISO;
      
        -----------------------------------------------------------------------
        -- others do nothing
        when others => in_port <= x"00";

      end case;

      -------------------------------------------------------------------------
      -- For UART read
      -- Generate 'buffer_read' pulse following read from port address 01
      if (read_strobe = '1') and (port_id = x"01") then
        read_from_uart_rx <= '1';
      else
        read_from_uart_rx <= '0';
      end if;

    end if;
  end process input_ports;

-----------------------------------------------------------------------------
-- Writing interface of the UC to UART and other devices
-----------------------------------------------------------------------------
  output_ports : process(clk, reset)
  begin
    if reset = '1' then
      DAQ_reg_local_rd <= '0';
      DAQ_reg_local_wr <= '0';
      TDC_reset        <= '1';
      TDC_data_out_wr  <= '0';
      TDC_data_in_rd   <= '0';
      onewire_A.go <= '0';
      onewire_B.go <= '0';
      uart_tx_reset <= '1';
      uart_rx_reset <= '1';
    elsif clk'event and clk = '1' then
      -- reset register strobe
      uart_tx_reset <= '0';
      uart_rx_reset <= '0';
      DAQ_reg_local_rd <= '0';
      DAQ_reg_local_wr <= '0';
      TDC_data_out_wr  <= '0';
      TDC_data_in_rd   <= '0';
      TDC_reset        <= '0';
      onewire_A.go <= '0';
      onewire_B.go <= '0';
      -------------------------------------------------------------------------
      -- Constant write strobe
      if k_write_strobe = '1' then
        case port_id(3 downto 0) is
          --------------------
          -- Uart status port 
          when x"0" =>
            uart_tx_reset <= out_port(0);
            uart_rx_reset <= out_port(1);
          -----------------------------------------------------------------------
          -- Flash
          when x"C" =>
            FLASH_SPI_CLK  <= out_port(0);
            FLASH_SPI_CS   <= out_port(1);
            FLASH_SPI_MOSI <= out_port(2);

          when others => null;
        end case;
      -------------------------------------------------------------------------
      -- write strobe
      elsif write_strobe = '1' then
        case port_id(3 downto 0) is
          --------------------
          -- i2c busses;
          when x"2" =>
            i2c_sel     <= out_port(3 downto 2);
            i2c_SDA_out <= out_port(1);
            i2c_SCL_out <= out_port(0);

          --------------------
          -- DAQ register control
          when x"3" =>
            -- latch data at the address for reading in
            if out_port(3) = '1' then
              DAQ_reg_local_rd <= '1';
            end if;
            -- present data and write strobe
            if out_port(2) = '1' then
              DAQ_reg_local_wr <= '1';
            end if;
            --update 32 -> 8 bit read address
--            DAQ_word_addr <= out_port(1 downto 0);
          --------------------
          -- DAQ register address
          when x"4" =>
            DAQ_reg_local_addr <= out_port;
          --------------------
          -- DAQ output byte 0
          when x"5" =>
            DAQ_reg_local_data_out(7 downto 0)   <= out_port;
          --------------------
          -- DAQ output byte 1
          when x"6" =>
            DAQ_reg_local_data_out(15 downto 8)   <= out_port;
          --------------------
          -- DAQ output byte 2
          when x"7" =>
            DAQ_reg_local_data_out(23 downto 16)   <= out_port;
          --------------------
          -- DAQ output byte 3
          when x"8" =>
            DAQ_reg_local_data_out(31 downto 24)   <= out_port;
            
          --------------------
          -- TDC status
          when x"9" =>
            if out_port(5) = '1' then
              TDC_data_out_wr <= '1';
            end if;
            if out_port(4) = '1' then
              TDC_data_in_rd <= '1';
            end if;
            if out_port(7) = '1' then
              TDC_reset <= '1';
            end if;
          --------------------
          -- TDC_data
          when x"A" =>
            TDC_data_out <= out_port;
          -----------------------------------------------------------------------
          -- one wire sensors
          when x"B" =>
            --A bus
            onewire_A.wr                <= out_port(0);
            onewire_A.go                <= out_port(1);
            onewire_A.mode              <= out_port(3 downto 2);
            --B bus
            onewire_B.wr                <= out_port(4);
            onewire_B.go                <= out_port(5);
            onewire_B.mode              <= out_port(7 downto 6);
          -----------------------------------------------------------------------
          -- Flash
          when x"C" =>
            FLASH_SPI_CLK  <= out_port(0);
            FLASH_SPI_CS   <= out_port(1);
            FLASH_SPI_MOSI <= out_port(2);
  

          when others => null;
        end case;
      end if;
    end if;
  end process output_ports;

--
-- Update the uart out signals without the one clock delay
-- 
  uart_tx_data_in  <= out_port;
  write_to_uart_tx <= '1' when (write_strobe = '1') and (port_id = x"01")
                      else '0';

-------------------------------------------------------------------------------
-- TDC_UART
-------------------------------------------------------------------------------
  TDC_reset_proc : process (clk, reset) is
  begin  -- process TDC_reset_proc
    if reset = '1' then                 -- asynchronous reset (active high)

    elsif clk'event and clk = '1' then  -- rising clock edge

    end if;
  end process TDC_reset_proc;

  TDC_UART_master_1 : TDC_UART_master
    port map (
      clk62_5         => clk,
      reset          => TDC_reset,
      rx             => TDC_Rx,
      tx             => TDC_Tx,
      data_out       => TDC_data_out,
      data_out_wr    => TDC_data_out_wr,
      data_out_empty => TDC_data_out_empty,
      data_in        => TDC_data_in,
      data_in_valid  => TDC_data_in_valid,
      rx_error       => TDC_rx_error,
      data_in_rd     => TDC_data_in_rd);


-------------------------------------------------------------------------------
-- I2c mux
-------------------------------------------------------------------------------
  i2c_mux : process (clk)
  begin  -- process i2c_mux
    if clk'event and clk = '1' then     -- rising clock edge
      case i2c_sel is
        when "01" =>
          i2c_SDA_in <= DB_SDA_in;
          DB_SDA_out <= i2c_SDA_out;
          DB_SDA_en  <= not(i2c_SDA_out);
          DB_SCL     <= i2c_SCL_out;
        when "10" =>
          i2c_SDA_in  <= DAC_SDA_in;
          DAC_SDA_out <= i2c_SDA_out;
          DAC_SDA_en  <= not(i2c_SDA_out);
          DAC_SCL     <= i2c_SCL_out;
        when others => null;
      end case;
    end if;
  end process i2c_mux;

-------------------------------------------------------------------------------
-- I2c mux
------------------------------------------------------------------------------- 

  one_wire_master_A: one_wire_master
    port map (
      clk62_5     => clk,
      reset       => reset,
      wire_in     => Wire_DS1820_in(0),
      wire_out    => Wire_DS1820_out(0),
      wire_T      => Wire_DS1820_T(0),
      bit_rd      => onewire_A.rd,
      bit_wr      => onewire_A.wr,
      bit_mode    => onewire_A.mode,
      bit_op_go   => onewire_A.go,
      bit_op_done => onewire_A.done);

  one_wire_master_B: one_wire_master
    port map (
      clk62_5     => clk,
      reset       => reset,
      wire_in     => Wire_DS1820_in(1),
      wire_out    => Wire_DS1820_out(1),
      wire_T      => Wire_DS1820_T(1),
      bit_rd      => onewire_B.rd,
      bit_wr      => onewire_B.wr,
      bit_mode    => onewire_B.mode,
      bit_op_go   => onewire_B.go,
      bit_op_done => onewire_B.done);

end architecture arch;
