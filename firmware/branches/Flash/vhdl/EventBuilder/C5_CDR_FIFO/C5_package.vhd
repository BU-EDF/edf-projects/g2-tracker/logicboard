----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package C5_package is
  type C5_counters_t is array (0 to 15) of unsigned(15 downto 0);
end C5_package;
