cat C5Codes.csv | awk '{print NR-1" { if {$disparity == \"1\"} {\n send10b "$3 " $stream \n return \"-1\"\n } else { \n send10b " $2 " $stream \n return \"1\" \n }\n}\n" }'

cat Codes_8b10b.csv | awk '{count=0;split($2,bits,"");for(i=1;i<=length($2);i++){if(bits[i] == "1"){count=count+1}}; if(count == 5){disp_1 = "1";disp_2="-1"}else{disp_1="-1";disp_2="1"};   print NR-1" { if {$disparity == \"1\"} {\n send10b "$3 " $stream \n return \""disp_1"\"\n } else { \n send10b " $2 " $stream \n return \""disp_2"\" \n }\n}\n" }'
