----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package Addressed_FIFO_package is
  type data_array is array (0 to 15) of std_logic_vector(33 downto 0);
end Addressed_FIFO_package;
