restart



#------------------------------------------------------------
# main simulation
#------------------------------------------------------------
# set up clock

isim force add /addressed_fifo/clk 1 -value 0 -time 4 ns -repeat 8 ns 
isim force add /addressed_fifo/reset 1
run 100 ns
isim force add /addressed_fifo/reset 0
run 200ns

isim force add {/addressed_fifo/fifo_in} 200000000 -radix hex
isim force add /addressed_fifo/fifo_wr 1
run 8ns
isim force add {/addressed_fifo/fifo_in}  1 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}  2 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}  3 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    4 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    5 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    6 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    7 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    8 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    9 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    a -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    b -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    c -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    d -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    e -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    f -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    10 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    11 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    12 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    13 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    14 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    15 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    16 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    17 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    18 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    19 -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    1a -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    1b -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    1c -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    1d -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    1e -radix hex
run 8ns
isim force add {/addressed_fifo/fifo_in}    1f -radix hex
run 8ns
isim force add /addressed_fifo/fifo_wr 0
run 100ns
isim force add /addressed_fifo/fifo_rd 1
run 160ns
isim force add /addressed_fifo/fifo_rd 0
run 16ns
isim force add /addressed_fifo/fifo_rd 1
run 16ns
isim force add /addressed_fifo/fifo_rd 0
run 16ns
isim force add /addressed_fifo/fifo_rd 1
run 1 us





#source vhdl/EventBuilder/TDC_CDR_FIFO/sim/FIFO_test.tcl







