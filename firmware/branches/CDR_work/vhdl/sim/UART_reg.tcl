restart
source vhdl/sim/UART_helper.tcl

#set initial conditions for UART
isim force add {/top/sc_in} 1
#isim force add {/top/reset} 0

#isim force add {/top/tdc_sda} 1

#set clock on /top/clk
isim force add {/top/clk125_in} 1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns
#isim force add {/top/reset_daq_clocking} 0
#isim force add {/top/C5_Raw_stream} 1 -radix bin -value 0 -radix bin -time 50 ns -repeat 100 ns
isim force add {/top/clk40_ext} 1 -radix bin -value 0 -radix bin -time 12.5 ns -repeat 25 ns
isim force add {/top/lb_monitor.ext_clock_locked} 1


#let everything start up
run 2 ms

#send daq_reg_rd command
sendUART_str daq_reg_rd 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str 0 115200 /top/sc_in;

#send CR (0x0D)
sendUART_hex 0D 115200 /top/sc_in;

run 2 ms


#send daq_reg_wr command
sendUART_str daq_reg_wr 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str 20 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str 1  115200 /top/sc_in;
#send CR (0x0D)
sendUART_hex 0D 115200 /top/sc_in;

run 2 ms

#send daq_reg_rd command
sendUART_str daq_reg_rd 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str 21 115200 /top/sc_in;

#send CR (0x0D)
sendUART_hex 0D 115200 /top/sc_in;


run 1 ms
