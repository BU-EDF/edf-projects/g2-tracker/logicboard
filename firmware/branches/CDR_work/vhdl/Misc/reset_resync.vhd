-------------------------------------------------------------------------------
-- Pass async reset or synchronous reset to an async reset
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity reset_resync is
  generic(
    RESET_LENGTH : integer := 1;
    RESET_VALUE: std_logic := '1'
    );
  port (
    reset_in_async  : in  std_logic; --async
    reset_in_sync   : in  std_logic;
    clk             : in  std_logic; 
    reset_out       : out std_logic --synchrnous to clk
    );

end entity reset_resync;

architecture behavioral of reset_resync is

  signal reset_delay : std_logic_vector(RESET_LENGTH downto 0) := (others => RESET_VALUE);
  
begin  -- architecture behavioral

  -- input that latches the synchronous 
  input_ff: process (clk, reset_in_async) is
  begin  -- process input_ff
    if reset_in_async = '1' then              -- asynchronous reset (active high)
      reset_delay(0) <= RESET_VALUE;          -- force a reset
    elsif clk'event and clk = '1' then        -- rising clock edge
      reset_delay(0) <= reset_in_sync;        -- clock in the reset value
    end if;
  end process input_ff;

  ff_pipeline: for iDelay in 0 to RESET_LENGTH-1 generate
    delay_ff: process (clk, reset_in_async) is
    begin  -- process delay_ff
      if reset_in_async = '1' then             -- asynchronous reset (active high)
        reset_delay(iDelay+1) <= RESET_VALUE;  -- force a reset
      elsif clk'event and clk = '1' then       -- rising clock edge
        if reset_in_sync = RESET_VALUE then
          reset_delay(iDelay+1) <= RESET_VALUE;
        else          
          reset_delay(iDelay+1) <= reset_delay(iDelay); -- pipeline the reset to
                                                        -- give RESET_LENGTH
                                                        -- clock ticks of reset
        end if;
      end if;
    end process delay_ff;
  end generate ff_pipeline;
  reset_out <= reset_delay(RESET_LENGTH);
end architecture behavioral;

