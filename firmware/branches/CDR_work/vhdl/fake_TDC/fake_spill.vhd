-------------------------------------------------------------------------------
-- g-2 tracker 
-- Dan Gastler
-- Generate fake spills of TDCs
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

use work.Fake_spill_data.all;

library UNISIM;
use UNISIM.vcomponents.all;



entity fake_spill is
  port (
    clk125       : in std_logic;
    reset        : in std_logic;

    control      : in Fake_TDC_Control_t;
   
    data_out   : out std_logic_vector(3 downto 0)
    );
  
end fake_spill;

architecture arch of fake_spill is
  component fake_TDC
    port (
      clk125          : in  std_logic;
      reset           : in  std_logic;
      send_spill      : in  std_logic;
      spill_number    : in  std_logic_vector(23 downto 0);
      spill_time      : in  std_logic_vector(43 downto 0);
      spill_hit_count : in  unsigned(10 downto 0);
      data_out        : out std_logic);
  end component;

  signal data_out_buffer : std_logic_vector(3 downto 0) := x"0";

  signal control_local : Fake_TDC_Control_t;
  
begin  -- arch

  buffer_control: process (clk125) is
  begin  -- process buffer_control
    if clk125'event and clk125 = '1' then  -- rising clock edge
      control_local <= control;
    end if;
  end process buffer_control;
  
  fake_TDCs: for iTDC in 0 to 3 generate
    fake_TDC_1: fake_TDC
      port map (
        clk125          => clk125,
        reset           => reset,
        send_spill      => control_local.send_spill,
        spill_number    => control_local.spill_number,
        spill_time      => control_local.spill_time,
        spill_hit_count => control_local.spill_hit_count,
        data_out        => data_out_buffer(iTDC));
  end generate fake_TDCs;


  data_buffer: process (clk125,reset) is
  begin  -- process data_buffer
    if reset = '1' then
      data_out <= x"0";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      data_out <= data_out_buffer;
    end if;
  end process data_buffer;
end arch;

