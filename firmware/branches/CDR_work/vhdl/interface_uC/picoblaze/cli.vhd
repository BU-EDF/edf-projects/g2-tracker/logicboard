--
-------------------------------------------------------------------------------------------
-- Copyright � 2010-2013, Xilinx, Inc.
-- This file contains confidential and proprietary information of Xilinx, Inc. and is
-- protected under U.S. and international copyright and other intellectual property laws.
-------------------------------------------------------------------------------------------
--
-- Disclaimer:
-- This disclaimer is not a license and does not grant any rights to the materials
-- distributed herewith. Except as otherwise provided in a valid license issued to
-- you by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE
-- MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY
-- DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
-- INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT,
-- OR FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable
-- (whether in contract or tort, including negligence, or under any other theory
-- of liability) for any loss or damage of any kind or nature related to, arising
-- under or in connection with these materials, including for any direct, or any
-- indirect, special, incidental, or consequential loss or damage (including loss
-- of data, profits, goodwill, or any type of loss or damage suffered as a result
-- of any action brought by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-safe, or for use in any
-- application requiring fail-safe performance, such as life-support or safety
-- devices or systems, Class III medical devices, nuclear facilities, applications
-- related to the deployment of airbags, or any other applications that could lead
-- to death, personal injury, or severe property or environmental damage
-- (individually and collectively, "Critical Applications"). Customer assumes the
-- sole risk and liability of any use of Xilinx products in Critical Applications,
-- subject only to applicable laws and regulations governing limitations on product
-- liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
-------------------------------------------------------------------------------------------
--
--
-- Definition of a program memory for KCPSM6 including generic parameters for the 
-- convenient selection of device family, program memory size and the ability to include 
-- the JTAG Loader hardware for rapid software development.
--
-- This file is primarily for use during code development and it is recommended that the 
-- appropriate simplified program memory definition be used in a final production design. 
--
--    Generic                  Values             Comments
--    Parameter                Supported
--  
--    C_FAMILY                 "S6"               Spartan-6 device
--                             "V6"               Virtex-6 device
--                             "7S"               7-Series device 
--                                                  (Artix-7, Kintex-7, Virtex-7 or Zynq)
--
--    C_RAM_SIZE_KWORDS        1, 2 or 4          Size of program memory in K-instructions
--
--    C_JTAG_LOADER_ENABLE     0 or 1             Set to '1' to include JTAG Loader
--
-- Notes
--
-- If your design contains MULTIPLE KCPSM6 instances then only one should have the 
-- JTAG Loader enabled at a time (i.e. make sure that C_JTAG_LOADER_ENABLE is only set to 
-- '1' on one instance of the program memory). Advanced users may be interested to know 
-- that it is possible to connect JTAG Loader to multiple memories and then to use the 
-- JTAG Loader utility to specify which memory contents are to be modified. However, 
-- this scheme does require some effort to set up and the additional connectivity of the 
-- multiple BRAMs can impact the placement, routing and performance of the complete 
-- design. Please contact the author at Xilinx for more detailed information. 
--
-- Regardless of the size of program memory specified by C_RAM_SIZE_KWORDS, the complete 
-- 12-bit address bus is connected to KCPSM6. This enables the generic to be modified 
-- without requiring changes to the fundamental hardware definition. However, when the 
-- program memory is 1K then only the lower 10-bits of the address are actually used and 
-- the valid address range is 000 to 3FF hex. Likewise, for a 2K program only the lower 
-- 11-bits of the address are actually used and the valid address range is 000 to 7FF hex.
--
-- Programs are stored in Block Memory (BRAM) and the number of BRAM used depends on the 
-- size of the program and the device family. 
--
-- In a Spartan-6 device a BRAM is capable of holding 1K instructions. Hence a 2K program 
-- will require 2 BRAMs to be used and a 4K program will require 4 BRAMs to be used. It 
-- should be noted that a 4K program is not such a natural fit in a Spartan-6 device and 
-- the implementation also requires a small amount of logic resulting in slightly lower 
-- performance. A Spartan-6 BRAM can also be split into two 9k-bit memories suggesting 
-- that a program containing up to 512 instructions could be implemented. However, there 
-- is a silicon errata which makes this unsuitable and therefore it is not supported by 
-- this file.
--
-- In a Virtex-6 or any 7-Series device a BRAM is capable of holding 2K instructions so 
-- obviously a 2K program requires only a single BRAM. Each BRAM can also be divided into 
-- 2 smaller memories supporting programs of 1K in half of a 36k-bit BRAM (generally 
-- reported as being an 18k-bit BRAM). For a program of 4K instructions, 2 BRAMs are used.
--
--
-- Program defined by 'Z:\home\dan\work\g-2\NewElectronics\LogicBoard\firmware\trunk\vhdl\interface_uC\picoblaze\cli.psm'.
--
-- Generated by KCPSM6 Assembler: 01 Dec 2015 - 14:19:22. 
--
-- Assembler used ROM_form template: ROM_form_JTAGLoader_14March13.vhd
--
-- Standard IEEE libraries
--
--
package jtag_loader_pkg is
 function addr_width_calc (size_in_k: integer) return integer;
end jtag_loader_pkg;
--
package body jtag_loader_pkg is
  function addr_width_calc (size_in_k: integer) return integer is
   begin
    if (size_in_k = 1) then return 10;
      elsif (size_in_k = 2) then return 11;
      elsif (size_in_k = 4) then return 12;
      else report "Invalid BlockRAM size. Please set to 1, 2 or 4 K words." severity FAILURE;
    end if;
    return 0;
  end function addr_width_calc;
end package body;
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.jtag_loader_pkg.ALL;
--
-- The Unisim Library is used to define Xilinx primitives. It is also used during
-- simulation. The source can be viewed at %XILINX%\vhdl\src\unisims\unisim_VCOMP.vhd
--  
library unisim;
use unisim.vcomponents.all;
--
--
entity cli is
  generic(             C_FAMILY : string := "S6"; 
              C_RAM_SIZE_KWORDS : integer := 1;
           C_JTAG_LOADER_ENABLE : integer := 0);
  Port (      address : in std_logic_vector(11 downto 0);
          instruction : out std_logic_vector(17 downto 0);
               enable : in std_logic;
                  rdl : out std_logic;                    
                  clk : in std_logic);
  end cli;
--
architecture low_level_definition of cli is
--
signal       address_a : std_logic_vector(15 downto 0);
signal        pipe_a11 : std_logic;
signal       data_in_a : std_logic_vector(35 downto 0);
signal      data_out_a : std_logic_vector(35 downto 0);
signal    data_out_a_l : std_logic_vector(35 downto 0);
signal    data_out_a_h : std_logic_vector(35 downto 0);
signal   data_out_a_ll : std_logic_vector(35 downto 0);
signal   data_out_a_lh : std_logic_vector(35 downto 0);
signal   data_out_a_hl : std_logic_vector(35 downto 0);
signal   data_out_a_hh : std_logic_vector(35 downto 0);
signal       address_b : std_logic_vector(15 downto 0);
signal       data_in_b : std_logic_vector(35 downto 0);
signal     data_in_b_l : std_logic_vector(35 downto 0);
signal    data_in_b_ll : std_logic_vector(35 downto 0);
signal    data_in_b_hl : std_logic_vector(35 downto 0);
signal      data_out_b : std_logic_vector(35 downto 0);
signal    data_out_b_l : std_logic_vector(35 downto 0);
signal   data_out_b_ll : std_logic_vector(35 downto 0);
signal   data_out_b_hl : std_logic_vector(35 downto 0);
signal     data_in_b_h : std_logic_vector(35 downto 0);
signal    data_in_b_lh : std_logic_vector(35 downto 0);
signal    data_in_b_hh : std_logic_vector(35 downto 0);
signal    data_out_b_h : std_logic_vector(35 downto 0);
signal   data_out_b_lh : std_logic_vector(35 downto 0);
signal   data_out_b_hh : std_logic_vector(35 downto 0);
signal        enable_b : std_logic;
signal           clk_b : std_logic;
signal            we_b : std_logic_vector(7 downto 0);
signal          we_b_l : std_logic_vector(3 downto 0);
signal          we_b_h : std_logic_vector(3 downto 0);
-- 
signal       jtag_addr : std_logic_vector(11 downto 0);
signal         jtag_we : std_logic;
signal       jtag_we_l : std_logic;
signal       jtag_we_h : std_logic;
signal        jtag_clk : std_logic;
signal        jtag_din : std_logic_vector(17 downto 0);
signal       jtag_dout : std_logic_vector(17 downto 0);
signal     jtag_dout_1 : std_logic_vector(17 downto 0);
signal         jtag_en : std_logic_vector(0 downto 0);
-- 
signal picoblaze_reset : std_logic_vector(0 downto 0);
signal         rdl_bus : std_logic_vector(0 downto 0);
--
constant BRAM_ADDRESS_WIDTH  : integer := addr_width_calc(C_RAM_SIZE_KWORDS);
--
--
component jtag_loader_6
generic(                C_JTAG_LOADER_ENABLE : integer := 1;
                                    C_FAMILY : string  := "V6";
                             C_NUM_PICOBLAZE : integer := 1;
                       C_BRAM_MAX_ADDR_WIDTH : integer := 10;
          C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                                C_JTAG_CHAIN : integer := 2;
                              C_ADDR_WIDTH_0 : integer := 10;
                              C_ADDR_WIDTH_1 : integer := 10;
                              C_ADDR_WIDTH_2 : integer := 10;
                              C_ADDR_WIDTH_3 : integer := 10;
                              C_ADDR_WIDTH_4 : integer := 10;
                              C_ADDR_WIDTH_5 : integer := 10;
                              C_ADDR_WIDTH_6 : integer := 10;
                              C_ADDR_WIDTH_7 : integer := 10);
port(              picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                           jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                          jtag_din : out STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                         jtag_addr : out STD_LOGIC_VECTOR(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
                          jtag_clk : out std_logic;
                           jtag_we : out std_logic;
                       jtag_dout_0 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_1 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_2 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_3 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_4 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_5 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_6 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_7 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end component;
--
begin
  --
  --  
  ram_1k_generate : if (C_RAM_SIZE_KWORDS = 1) generate
 
    s6: if (C_FAMILY = "S6") generate 
      --
      address_a(13 downto 0) <= address(9 downto 0) & "0000";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "0000000000000000000000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "0000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB16BWER
      generic map ( DATA_WIDTH_A => 18,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 18,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900029BA0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"F0201000500000600133E0879201E087930112FF13FFB001B0310337008E5000",
                    INIT_09 => X"A030D00110200002D001103A0002D1010002D20100020041003013005000F023",
                    INIT_0A => X"5000005920930059609FD00330031301003020AED3FFD1010002D20100020041",
                    INIT_0B => X"D1031108D0045000008150000177098E093F0210B12BB227A0BAD004B0230177",
                    INIT_0C => X"D10311F4D708D607D506D405D0045000970896079506940520C1D010900331FF",
                    INIT_0D => X"60E1DD00ADD01D30ACC01C2C20E1ACC01C2C1D01607CD00120DAD002B0235000",
                    INIT_0E => X"10300002D00110200002005300C0D00110780002D0011030000200BD00C01D01",
                    INIT_0F => X"005300400002005300500002005300600002005300700002D00110780002D001",
                    INIT_10 => X"A10010010510A10010010410A1001030607CD002B023500060E19D011C010059",
                    INIT_11 => X"116311611172115411201132112D1167500000C9A000102C0710A10010010610",
                    INIT_12 => X"11561120116411721161116F1142116311691167116F114C112011721165116B",
                    INIT_13 => X"000200530060D001102E00020053007000BD100100641A181B0111001120113A",
                    INIT_14 => X"116511661166117511425000005900530040D001102E000200530050D001102E",
                    INIT_15 => X"1120113A1174116E1175116F1163112011641172116F115711001120113A1172",
                    INIT_16 => X"1120112011201120113A11731165117A11691173112011641172116F11571100",
                    INIT_17 => X"A0100002E183C120B220110000641A4B1B011100112011641172116F11571100",
                    INIT_18 => X"0002D1010002D201000200410020D00110280002D00110200002217C1101D001",
                    INIT_19 => X"C3401300B423D1010002D20100020041B023000200641A541B010059D0011029",
                    INIT_1A => X"1301D1010002D20100020041A01001301124D00110400002D00110200002E1B1",
                    INIT_1B => X"0041A01001301128D00110200002E1C6C3401300B42300641A611B010059219F",
                    INIT_1C => X"003000641A711B010059E1E9C3401300B423005921B71301D1010002D2010002",
                    INIT_1D => X"C65016030650152C4506450613010530D00110200002D1010002D20100020041",
                    INIT_1E => X"21F7D1004BA01AB41B075000005921DF9601D1010002D20100020041A060A1C9",
                    INIT_1F => X"3003700160005000005921ED3B001A03005921ED3B001A01D101000221FBD1FF",
                    INIT_20 => X"DD023DFDDD025D025D015000E2089E011E3E50007000DD024D003DF040074007",
                    INIT_21 => X"3DFD02070207DD025D0102070207DD025D025000DD023DFE0207020702070207",
                    INIT_22 => X"5D01020702070207DD023DFD5000020702070207DD023DFE020702070207DD02",
                    INIT_23 => X"3DFE02070207DD025D01DD025D0202075000DD025D020207020702070207DD02",
                    INIT_24 => X"5D020207DD023DFE02070207DD025D010207DD023DFD5000020702070207DD02",
                    INIT_25 => X"DD025D010207DD025D02225DDD023DFDA25B440602071580500002070207DD02",
                    INIT_26 => X"02070207DD025D0195020207DD025D0202072255A267450EDD023DFE02070207",
                    INIT_27 => X"5D0102074400D60296020207158014005D025000D502020702070207DD023DFE",
                    INIT_28 => X"020B30FE7001633062206110600050000207E27A450EDD023DFE02070207DD02",
                    INIT_29 => X"700040061000022AA2A702540430A2A502540420A2A302540410A2A102540400",
                    INIT_2A => X"60005000700040061080022A005322A9100422A9100322A9100222A910015000",
                    INIT_2B => X"1000022AA2C702540420A2C502540410A2C302540400020B30FE700162206110",
                    INIT_2C => X"60005000700040061080022A005322C9100322C9100222C91001500070004006",
                    INIT_2D => X"0277A2F00254040050010217A2A302540410A2EC02540400020B30FE70016110",
                    INIT_2E => X"22F2100222F21001500070006330622040061000022A02380340027702450240",
                    INIT_2F => X"116F1172117211651120116B116311415000700040061080022A005322F21003",
                    INIT_30 => X"1434A1401430A040142C607CD003B0235000005900641AF81B021100112E1172",
                    INIT_31 => X"B0235000A30302AF5000A3030289A31CD503A2401401A340A316D503A550152A",
                    INIT_32 => X"01FD1000500000590053003000530020A30302CFA1401430A040142C607CD002",
                    INIT_33 => X"1164116111425000032E033103346D0010F3500001FD1002500001FD10015000",
                    INIT_34 => X"5000005900641A3D1B03110011731174116E1165116D11751167117211611120",
                    INIT_35 => X"9D01E35B9E01E35B9F011FFF1EFF1DFF5000D003301FA040142C634BD001B023",
                    INIT_36 => X"0070D00110200002D0011049000200BD10B2303040064006400640065000E35B",
                    INIT_37 => X"5000C54015809000B700B607B589940000590053004000530050005300600053",
                    INIT_38 => X"006000530070D00110200002D001104B000200BD105230304006400640064006",
                    INIT_39 => X"400640065000C54015809000B700B607B5899400005900530040005300500053",
                    INIT_3A => X"00500053006000530070D00110200002D001104C000200BD10B0303040064006",
                    INIT_3B => X"40064006400640065000C54015809000B700B607B58994000059005300400053",
                    INIT_3C => X"0040005300500053006000530070D00110200002D001105A000200BD00203030",
                    INIT_3D => X"105000021A000B001C0150001000970010009600100095001000940000590053",
                    INIT_3E => X"41064106410601C000BD1050400640064006400600B00059005300C00002D001",
                    INIT_3F => X"1700160015001401102000C91050400640064006400600B04410348F31704106",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"8A28A28A28A228A0D0080DD28AAA00C08AAA90D2AAD136AA228AAA0A8AB742AA",
                   INITP_02 => X"2D082AAAAAAAAAAAAAAAAAAAAAAA2288A228882AAAAAAAAAAAAAA0041040D2D6",
                   INITP_03 => X"36A5A5ADD82A6AA351548AAA20B429AA848B420A6AA1228B42AA2828AAA228A6",
                   INITP_04 => X"242022A8A828AD8A8A28D8AA28A8A2AA2A22A2AA2A8AA8AA2A2A28AA882D2E05",
                   INITP_05 => X"AAAAB4A888B54A28B82E388D6D2A222D2E38E2356D2A2222D2E38E388D56B62A",
                   INITP_06 => X"9355A222228A455B7740A034A82AAAAAAAA928A28A88E00D2EBB44D00034A82A",
                   INITP_07 => X"002554015495528A202DDDDA222228A4559355A222228A4559355A222228A455")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a(31 downto 0),
                  DOPA => data_out_a(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b(31 downto 0),
                  DOPB => data_out_b(35 downto 32), 
                   DIB => data_in_b(31 downto 0),
                  DIPB => data_in_b(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --               
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900029BA0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"F0201000500000600133E0879201E087930112FF13FFB001B0310337008E5000",
                    INIT_09 => X"A030D00110200002D001103A0002D1010002D20100020041003013005000F023",
                    INIT_0A => X"5000005920930059609FD00330031301003020AED3FFD1010002D20100020041",
                    INIT_0B => X"D1031108D0045000008150000177098E093F0210B12BB227A0BAD004B0230177",
                    INIT_0C => X"D10311F4D708D607D506D405D0045000970896079506940520C1D010900331FF",
                    INIT_0D => X"60E1DD00ADD01D30ACC01C2C20E1ACC01C2C1D01607CD00120DAD002B0235000",
                    INIT_0E => X"10300002D00110200002005300C0D00110780002D0011030000200BD00C01D01",
                    INIT_0F => X"005300400002005300500002005300600002005300700002D00110780002D001",
                    INIT_10 => X"A10010010510A10010010410A1001030607CD002B023500060E19D011C010059",
                    INIT_11 => X"116311611172115411201132112D1167500000C9A000102C0710A10010010610",
                    INIT_12 => X"11561120116411721161116F1142116311691167116F114C112011721165116B",
                    INIT_13 => X"000200530060D001102E00020053007000BD100100641A181B0111001120113A",
                    INIT_14 => X"116511661166117511425000005900530040D001102E000200530050D001102E",
                    INIT_15 => X"1120113A1174116E1175116F1163112011641172116F115711001120113A1172",
                    INIT_16 => X"1120112011201120113A11731165117A11691173112011641172116F11571100",
                    INIT_17 => X"A0100002E183C120B220110000641A4B1B011100112011641172116F11571100",
                    INIT_18 => X"0002D1010002D201000200410020D00110280002D00110200002217C1101D001",
                    INIT_19 => X"C3401300B423D1010002D20100020041B023000200641A541B010059D0011029",
                    INIT_1A => X"1301D1010002D20100020041A01001301124D00110400002D00110200002E1B1",
                    INIT_1B => X"0041A01001301128D00110200002E1C6C3401300B42300641A611B010059219F",
                    INIT_1C => X"003000641A711B010059E1E9C3401300B423005921B71301D1010002D2010002",
                    INIT_1D => X"C65016030650152C4506450613010530D00110200002D1010002D20100020041",
                    INIT_1E => X"21F7D1004BA01AB41B075000005921DF9601D1010002D20100020041A060A1C9",
                    INIT_1F => X"3003700160005000005921ED3B001A03005921ED3B001A01D101000221FBD1FF",
                    INIT_20 => X"DD023DFDDD025D025D015000E2089E011E3E50007000DD024D003DF040074007",
                    INIT_21 => X"3DFD02070207DD025D0102070207DD025D025000DD023DFE0207020702070207",
                    INIT_22 => X"5D01020702070207DD023DFD5000020702070207DD023DFE020702070207DD02",
                    INIT_23 => X"3DFE02070207DD025D01DD025D0202075000DD025D020207020702070207DD02",
                    INIT_24 => X"5D020207DD023DFE02070207DD025D010207DD023DFD5000020702070207DD02",
                    INIT_25 => X"DD025D010207DD025D02225DDD023DFDA25B440602071580500002070207DD02",
                    INIT_26 => X"02070207DD025D0195020207DD025D0202072255A267450EDD023DFE02070207",
                    INIT_27 => X"5D0102074400D60296020207158014005D025000D502020702070207DD023DFE",
                    INIT_28 => X"020B30FE7001633062206110600050000207E27A450EDD023DFE02070207DD02",
                    INIT_29 => X"700040061000022AA2A702540430A2A502540420A2A302540410A2A102540400",
                    INIT_2A => X"60005000700040061080022A005322A9100422A9100322A9100222A910015000",
                    INIT_2B => X"1000022AA2C702540420A2C502540410A2C302540400020B30FE700162206110",
                    INIT_2C => X"60005000700040061080022A005322C9100322C9100222C91001500070004006",
                    INIT_2D => X"0277A2F00254040050010217A2A302540410A2EC02540400020B30FE70016110",
                    INIT_2E => X"22F2100222F21001500070006330622040061000022A02380340027702450240",
                    INIT_2F => X"116F1172117211651120116B116311415000700040061080022A005322F21003",
                    INIT_30 => X"1434A1401430A040142C607CD003B0235000005900641AF81B021100112E1172",
                    INIT_31 => X"B0235000A30302AF5000A3030289A31CD503A2401401A340A316D503A550152A",
                    INIT_32 => X"01FD1000500000590053003000530020A30302CFA1401430A040142C607CD002",
                    INIT_33 => X"1164116111425000032E033103346D0010F3500001FD1002500001FD10015000",
                    INIT_34 => X"5000005900641A3D1B03110011731174116E1165116D11751167117211611120",
                    INIT_35 => X"9D01E35B9E01E35B9F011FFF1EFF1DFF5000D003301FA040142C634BD001B023",
                    INIT_36 => X"0070D00110200002D0011049000200BD10B2303040064006400640065000E35B",
                    INIT_37 => X"5000C54015809000B700B607B589940000590053004000530050005300600053",
                    INIT_38 => X"006000530070D00110200002D001104B000200BD105230304006400640064006",
                    INIT_39 => X"400640065000C54015809000B700B607B5899400005900530040005300500053",
                    INIT_3A => X"00500053006000530070D00110200002D001104C000200BD10B0303040064006",
                    INIT_3B => X"40064006400640065000C54015809000B700B607B58994000059005300400053",
                    INIT_3C => X"0040005300500053006000530070D00110200002D001105A000200BD00203030",
                    INIT_3D => X"105000021A000B001C0150001000970010009600100095001000940000590053",
                    INIT_3E => X"41064106410601C000BD1050400640064006400600B00059005300C00002D001",
                    INIT_3F => X"1700160015001401102000C91050400640064006400600B04410348F31704106",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"8A28A28A28A228A0D0080DD28AAA00C08AAA90D2AAD136AA228AAA0A8AB742AA",
                   INITP_02 => X"2D082AAAAAAAAAAAAAAAAAAAAAAA2288A228882AAAAAAAAAAAAAA0041040D2D6",
                   INITP_03 => X"36A5A5ADD82A6AA351548AAA20B429AA848B420A6AA1228B42AA2828AAA228A6",
                   INITP_04 => X"242022A8A828AD8A8A28D8AA28A8A2AA2A22A2AA2A8AA8AA2A2A28AA882D2E05",
                   INITP_05 => X"AAAAB4A888B54A28B82E388D6D2A222D2E38E2356D2A2222D2E38E388D56B62A",
                   INITP_06 => X"9355A222228A455B7740A034A82AAAAAAAA928A28A88E00D2EBB44D00034A82A",
                   INITP_07 => X"002554015495528A202DDDDA222228A4559355A222228A4559355A222228A455")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900029BA0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"F0201000500000600133E0879201E087930112FF13FFB001B0310337008E5000",
                    INIT_09 => X"A030D00110200002D001103A0002D1010002D20100020041003013005000F023",
                    INIT_0A => X"5000005920930059609FD00330031301003020AED3FFD1010002D20100020041",
                    INIT_0B => X"D1031108D0045000008150000177098E093F0210B12BB227A0BAD004B0230177",
                    INIT_0C => X"D10311F4D708D607D506D405D0045000970896079506940520C1D010900331FF",
                    INIT_0D => X"60E1DD00ADD01D30ACC01C2C20E1ACC01C2C1D01607CD00120DAD002B0235000",
                    INIT_0E => X"10300002D00110200002005300C0D00110780002D0011030000200BD00C01D01",
                    INIT_0F => X"005300400002005300500002005300600002005300700002D00110780002D001",
                    INIT_10 => X"A10010010510A10010010410A1001030607CD002B023500060E19D011C010059",
                    INIT_11 => X"116311611172115411201132112D1167500000C9A000102C0710A10010010610",
                    INIT_12 => X"11561120116411721161116F1142116311691167116F114C112011721165116B",
                    INIT_13 => X"000200530060D001102E00020053007000BD100100641A181B0111001120113A",
                    INIT_14 => X"116511661166117511425000005900530040D001102E000200530050D001102E",
                    INIT_15 => X"1120113A1174116E1175116F1163112011641172116F115711001120113A1172",
                    INIT_16 => X"1120112011201120113A11731165117A11691173112011641172116F11571100",
                    INIT_17 => X"A0100002E183C120B220110000641A4B1B011100112011641172116F11571100",
                    INIT_18 => X"0002D1010002D201000200410020D00110280002D00110200002217C1101D001",
                    INIT_19 => X"C3401300B423D1010002D20100020041B023000200641A541B010059D0011029",
                    INIT_1A => X"1301D1010002D20100020041A01001301124D00110400002D00110200002E1B1",
                    INIT_1B => X"0041A01001301128D00110200002E1C6C3401300B42300641A611B010059219F",
                    INIT_1C => X"003000641A711B010059E1E9C3401300B423005921B71301D1010002D2010002",
                    INIT_1D => X"C65016030650152C4506450613010530D00110200002D1010002D20100020041",
                    INIT_1E => X"21F7D1004BA01AB41B075000005921DF9601D1010002D20100020041A060A1C9",
                    INIT_1F => X"3003700160005000005921ED3B001A03005921ED3B001A01D101000221FBD1FF",
                    INIT_20 => X"DD023DFDDD025D025D015000E2089E011E3E50007000DD024D003DF040074007",
                    INIT_21 => X"3DFD02070207DD025D0102070207DD025D025000DD023DFE0207020702070207",
                    INIT_22 => X"5D01020702070207DD023DFD5000020702070207DD023DFE020702070207DD02",
                    INIT_23 => X"3DFE02070207DD025D01DD025D0202075000DD025D020207020702070207DD02",
                    INIT_24 => X"5D020207DD023DFE02070207DD025D010207DD023DFD5000020702070207DD02",
                    INIT_25 => X"DD025D010207DD025D02225DDD023DFDA25B440602071580500002070207DD02",
                    INIT_26 => X"02070207DD025D0195020207DD025D0202072255A267450EDD023DFE02070207",
                    INIT_27 => X"5D0102074400D60296020207158014005D025000D502020702070207DD023DFE",
                    INIT_28 => X"020B30FE7001633062206110600050000207E27A450EDD023DFE02070207DD02",
                    INIT_29 => X"700040061000022AA2A702540430A2A502540420A2A302540410A2A102540400",
                    INIT_2A => X"60005000700040061080022A005322A9100422A9100322A9100222A910015000",
                    INIT_2B => X"1000022AA2C702540420A2C502540410A2C302540400020B30FE700162206110",
                    INIT_2C => X"60005000700040061080022A005322C9100322C9100222C91001500070004006",
                    INIT_2D => X"0277A2F00254040050010217A2A302540410A2EC02540400020B30FE70016110",
                    INIT_2E => X"22F2100222F21001500070006330622040061000022A02380340027702450240",
                    INIT_2F => X"116F1172117211651120116B116311415000700040061080022A005322F21003",
                    INIT_30 => X"1434A1401430A040142C607CD003B0235000005900641AF81B021100112E1172",
                    INIT_31 => X"B0235000A30302AF5000A3030289A31CD503A2401401A340A316D503A550152A",
                    INIT_32 => X"01FD1000500000590053003000530020A30302CFA1401430A040142C607CD002",
                    INIT_33 => X"1164116111425000032E033103346D0010F3500001FD1002500001FD10015000",
                    INIT_34 => X"5000005900641A3D1B03110011731174116E1165116D11751167117211611120",
                    INIT_35 => X"9D01E35B9E01E35B9F011FFF1EFF1DFF5000D003301FA040142C634BD001B023",
                    INIT_36 => X"0070D00110200002D0011049000200BD10B2303040064006400640065000E35B",
                    INIT_37 => X"5000C54015809000B700B607B589940000590053004000530050005300600053",
                    INIT_38 => X"006000530070D00110200002D001104B000200BD105230304006400640064006",
                    INIT_39 => X"400640065000C54015809000B700B607B5899400005900530040005300500053",
                    INIT_3A => X"00500053006000530070D00110200002D001104C000200BD10B0303040064006",
                    INIT_3B => X"40064006400640065000C54015809000B700B607B58994000059005300400053",
                    INIT_3C => X"0040005300500053006000530070D00110200002D001105A000200BD00203030",
                    INIT_3D => X"105000021A000B001C0150001000970010009600100095001000940000590053",
                    INIT_3E => X"41064106410601C000BD1050400640064006400600B00059005300C00002D001",
                    INIT_3F => X"1700160015001401102000C91050400640064006400600B04410348F31704106",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"8A28A28A28A228A0D0080DD28AAA00C08AAA90D2AAD136AA228AAA0A8AB742AA",
                   INITP_02 => X"2D082AAAAAAAAAAAAAAAAAAAAAAA2288A228882AAAAAAAAAAAAAA0041040D2D6",
                   INITP_03 => X"36A5A5ADD82A6AA351548AAA20B429AA848B420A6AA1228B42AA2828AAA228A6",
                   INITP_04 => X"242022A8A828AD8A8A28D8AA28A8A2AA2A22A2AA2A8AA8AA2A2A28AA882D2E05",
                   INITP_05 => X"AAAAB4A888B54A28B82E388D6D2A222D2E38E2356D2A2222D2E38E388D56B62A",
                   INITP_06 => X"9355A222228A455B7740A034A82AAAAAAAA928A28A88E00D2EBB44D00034A82A",
                   INITP_07 => X"002554015495528A202DDDDA222228A4559355A222228A4559355A222228A455")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate akv7;
    --
  end generate ram_1k_generate;
  --
  --
  --
  ram_2k_generate : if (C_RAM_SIZE_KWORDS = 2) generate
    --
    --
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400BA81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"30012002013A02010201024130000023200000603387018701FFFF0131378E00",
                    INIT_05 => X"030804008100778E3F102B27BA042377005993599F03030130AEFF0102010241",
                    INIT_06 => X"E100D030C02CE1C02C017C01DA02230003F408070605040008070605C11003FF",
                    INIT_07 => X"53400253500253600253700201780201300201200253C0017802013002BDC001",
                    INIT_08 => X"6361725420322D6700C9002C1000011000011000011000307C022300E1010159",
                    INIT_09 => X"025360012E025370BD0164180100203A56206472616F426369676F4C2072656B",
                    INIT_0A => X"203A746E756F632064726F5700203A72656666754200595340012E025350012E",
                    INIT_0B => X"100283202000644B01002064726F5700202020203A73657A69732064726F5700",
                    INIT_0C => X"40002301020102412302645401590129020102010241200128020120027C0101",
                    INIT_0D => X"41103028012002C6400023646101599F010102010241103024014002012002B1",
                    INIT_0E => X"5003502C0606013001200201020102413064710159E940002359B70101020102",
                    INIT_0F => X"0301000059ED000359ED00010102FBFFF700A0B4070059DF01010201024160C9",
                    INIT_10 => X"FD07070201070702020002FE0707070702FD0202010008013E00000200F00707",
                    INIT_11 => X"FE0707020102020700020207070707020107070702FD0007070702FE07070702",
                    INIT_12 => X"02010702025D02FD5B06078000070702020702FE070702010702FD0007070702",
                    INIT_13 => X"010700020207800002000207070702FE07070201020702020755670E02FE0707",
                    INIT_14 => X"0006002AA75430A55420A35410A154000BFE013020100000077A0E02FE070702",
                    INIT_15 => X"002AC75420C55410C354000BFE01201000000006802A53A904A903A902A90100",
                    INIT_16 => X"77F054000117A35410EC54000BFE011000000006802A53C903C902C901000006",
                    INIT_17 => X"6F727265206B6341000006802A53F203F202F2010000302006002A3840774540",
                    INIT_18 => X"230003AF0003891C034001401603502A344030402C7C0323005964F802002E72",
                    INIT_19 => X"646142002E313400F300FD0200FD0100FD0000595330532003CF4030402C7C02",
                    INIT_1A => X"015B015B01FFFFFF00031F402C4B01230059643D030073746E656D7567726120",
                    INIT_1B => X"0040800000078900595340535053605370012002014902BDB23006060606005B",
                    INIT_1C => X"06060040800000078900595340535053605370012002014B02BD523006060606",
                    INIT_1D => X"060606060040800000078900595340535053605370012002014C02BDB0300606",
                    INIT_1E => X"5002000001000000000000000000595340535053605370012002015A02BD2030",
                    INIT_1F => X"0000000120C95006060606B0108F7006060606C0BD5006060606B05953C00201",
                    INIT_20 => X"060606B0C9503006060606B0FC58C9503006060606B00358BD5006060606B0C9",
                    INIT_21 => X"B052063FBCB5B062B080B09EB0595340535053605370012002015402BDB43006",
                    INIT_22 => X"00070000E0060606A01F1FDE060107520652BC57B052064BBC58B0520645BC56",
                    INIT_23 => X"06060606B0060606060759531010006C05720E01000E0E5FE0020F660E6510A0",
                    INIT_24 => X"DB03A008BD40DB029A04BD40DB019402BD40DB008E01BD40005959C9C07FBD50",
                    INIT_25 => X"0009100AB5C20001BC0209FFFF000920A50AAB0109000900A601FF00A5098000",
                    INIT_26 => X"E132E131000E005040E0B300DEB300DCB3AB100F060606060607A1000E010006",
                    INIT_27 => X"0606A1000E015953A90201023A01025359010201023A01025201025201024533",
                    INIT_28 => X"01023A01025359010252010252010245000E0010B3AB20AB30AB100F06060606",
                    INIT_29 => X"431039200B00000B2080004043012D020B00000B0208003501000E015953A902",
                    INIT_2A => X"008064010058200B0B20C052200B004E020B0B020C48020B5201000E01000E00",
                    INIT_2B => X"8E01007D770E087E0180007D6F0E087E0080007501006A650EA001806A5F0EA0",
                    INIT_2C => X"000E0E0E0E0E95200B0B0B0B4020008E200B000E87020B0B0B0B04020080020B",
                    INIT_2D => X"0B0B0B002006060606000E00B1200B00AB020B0B0B0B0002000E00A2020BB101",
                    INIT_2E => X"53305340E1C51030102C7C0223005953B300AB102C7C0123005953A900BE200B",
                    INIT_2F => X"5900646E756F6620656369766564206F4E00FD1030102C100110347C03230059",
                    INIT_30 => X"014F020146020059014502014E02014F02014E021A27102C7C0123005964EF05",
                    INIT_31 => X"30106B30106B30106B305C3330602730102C7C01230059014402014E02015502",
                    INIT_32 => X"005953405350536053705380539053A053B0106B30106B30106B30106B30106B",
                    INIT_33 => X"5953106B30102C7C0123005C40301030102C7C02230059015202015202014502",
                    INIT_34 => X"102C7C08102A7C0810297C0323005946305C44305CCC30FF2730102C7C012300",
                    INIT_35 => X"40015C3040015C4030305C3040015C3040015C3040015C4030345C5530FF2730",
                    INIT_36 => X"6B30106B305CBE305CCC30FF2730102C7C0123005946305C44305C3040015C30",
                    INIT_37 => X"3040015C4030345C5530FF2730102C7C08102A7C0810297C0323005953405310",
                    INIT_38 => X"30106B305CBE305C3040015C3040015C3040015C4030305C3040015C3040015C",
                    INIT_39 => X"006403007E20007E20005CF0200001FF27209800102C7C01230059534053106B",
                    INIT_3A => X"00403641018160A0206056300056305600006F00533056014A3056000E0E0046",
                    INIT_3B => X"0E7A0E017F00B0070130A00E0E0E013040002E4620B0A0AA5930206E6EAA5965",
                    INIT_3C => X"010001000100400000B010D08E0106079400FEB0070130A00E0E0E0130400100",
                    INIT_3D => X"6DB0000067726169746C756D0059AD010153C007074000000100010001000100",
                    INIT_3E => X"01007379733301006E6F6973726576BB00007465736572920000706D75646D65",
                    INIT_3F => X"0064725F6765725F63326908030072775F6765725F633269EB0100706C656877",
                   INITP_00 => X"000000017040E8A1C3A10130010408A41FFFE536008278000000008F80000002",
                   INITP_01 => X"066BE9411F10173831C5C181D008400634FFFFFFFFFFF800000FFFFFFF0CB204",
                   INITP_02 => X"FF00020800010000000100000000143982A33B1CDB11B3619E618C3199B0F81C",
                   INITP_03 => X"A003F0001110000006A000001A8000006A000001DD040FFFEF2480A0259F400F",
                   INITP_04 => X"0C2D56DB50821000616B186682082000000298018010208209500000000C0300",
                   INITP_05 => X"7FFF9BA00B40080400288052020108049B3B334D24110480A01A0130B56D1010",
                   INITP_06 => X"59B6220096DB05AAAB2AAB36C4416DB01619C000000032CB2CB2C00000000501",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFF31155550BA106EA20B55DFF652EEDFB73B4012D555955")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_l(31 downto 0),
                  DOPA => data_out_a_l(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_l(31 downto 0),
                  DOPB => data_out_b_l(35 downto 32), 
                   DIB => data_in_b_l(31 downto 0),
                  DIPB => data_in_b_l(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_h: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"506808006808006800690000000928787808280000F0C9F0C909095858010028",
                    INIT_05 => X"680868280028000404815859D0E8580028001000B0E818890090E96800690000",
                    INIT_06 => X"B0EE560E560E10560E0EB0E890E8582868086B6B6A6A68284B4B4A4A90684818",
                    INIT_07 => X"000000000000000000000000680800680800680800000068080068080000000E",
                    INIT_08 => X"080808080808080828005008035088035088025088025008B0E85828B0CE8E00",
                    INIT_09 => X"00000068080000000008000D0D08080808080808080808080808080808080808",
                    INIT_0A => X"0808080808080808080808080808080808080808082800000068080000006808",
                    INIT_0B => X"5000F0E05908000D0D0808080808080808080808080808080808080808080808",
                    INIT_0C => X"E1095A68006900005800000D0D00680800680069000000680800680800108868",
                    INIT_0D => X"00508008680800F0E1095A000D0D0010896800690000508008680800680800F0",
                    INIT_0E => X"E38B038AA2A28902680800680069000000000D0D00F0E1095A00108968006900",
                    INIT_0F => X"18B8B02800109D8D00109D8D680090E890E8250D0D280010CB680069000050D0",
                    INIT_10 => X"1E01016E2E01016E2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A0",
                    INIT_11 => X"1E01016E2E6E2E01286E2E010101016E2E0101016E1E280101016E1E0101016E",
                    INIT_12 => X"6E2E016E2E116E1ED1A2010A2801016E2E016E1E01016E2E016E1E280101016E",
                    INIT_13 => X"2E01A26B4B010A0A2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E0101",
                    INIT_14 => X"B8A00801D10102D10102D10102D101020118B8B1B1B0B02801F1A26E1E01016E",
                    INIT_15 => X"0801D10102D10102D101020118B8B1B0B028B8A0080100110811081108110828",
                    INIT_16 => X"01D101022801D10102D101020118B8B0B028B8A008010011081108110828B8A0",
                    INIT_17 => X"080808080808080828B8A008010011081108110828B8B1B1A008010101010101",
                    INIT_18 => X"5828D10128D101D1EA518A51D1EA520A0A500A500AB0E8582800000D0D080808",
                    INIT_19 => X"08080828010101B608280008280008280008280000000000D101500A500AB0E8",
                    INIT_1A => X"CEF1CFF1CF0F0F0E286818500AB1E8582800000D0D0808080808080808080808",
                    INIT_1B => X"28E20AC8DBDBDACA000000000000000000680800680800008818A0A0A0A028F1",
                    INIT_1C => X"A0A028E20AC8DBDBDACA000000000000000000680800680800008818A0A0A0A0",
                    INIT_1D => X"A0A0A0A028E20AC8DBDBDACA000000000000000000680800680800008818A0A0",
                    INIT_1E => X"08000D050E2888CB88CB88CA88CA000000000000000000680800680800008018",
                    INIT_1F => X"0B0B0A0A080088A0A0A0A000221A18A0A0A0A0000088A0A0A0A0000000000068",
                    INIT_20 => X"A0A0A000008818A0A0A0A0001A01008818A0A0A0A0002A010088A0A0A0A00000",
                    INIT_21 => X"0012A592010900010001000100000000000000000000680800680800008818A0",
                    INIT_22 => X"0008092518A0A0A0001DEDB1EE8EA512A59201090012A59201090012A5920109",
                    INIT_23 => X"A0A0A0A000A6A6A6A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E010",
                    INIT_24 => X"0108926A00080108926A00080108926A00080108926A000828000000121A0088",
                    INIT_25 => X"096808481292DFCFB218480F0F2868080268921848284828F2CF0F28026F0F28",
                    INIT_26 => X"1209120928A0080201D20202D20202D202022018A0A0A0A0A0A00228A10928A1",
                    INIT_27 => X"A0A00228A1090000020068000868000800690068000868000868000868000809",
                    INIT_28 => X"6800086800080068000868000868000828A008D20202000200022018A0A0A0A0",
                    INIT_29 => X"9268926848080868282808129268926848080868282808D26828A10900000200",
                    INIT_2A => X"0809D2682892684868280892684828926848682808926848D26828A00828A008",
                    INIT_2B => X"D2682812F2A1A00208090812F2A1A002080908D2682812F2A102080912F2A102",
                    INIT_2C => X"28A0A0A0A0A092684848486828280892684828A0926848484868282808926848",
                    INIT_2D => X"4848682828A0A0A0A0A0A008926848289268484848682828A0A008926848D268",
                    INIT_2E => X"00000000D20250085008B0E8582800000228025008B0E8582800000228926848",
                    INIT_2F => X"000808080808080808080808080808080828025008500851885108B0E8582800",
                    INIT_30 => X"6808006808002800680800680800680800680800D3025008B0E8582800000D0D",
                    INIT_31 => X"00030200020200020200020800F302005108B0E8582800680800680800680800",
                    INIT_32 => X"2800000000000000000000000000000000000502000502000402000402000302",
                    INIT_33 => X"00000002005108B0E8582802000052085108B0E8582800680800680800680800",
                    INIT_34 => X"5108B0E85008B0E85008B0E85828000200020800020800F202005108B0E85828",
                    INIT_35 => X"508A0200508A0250000A0200508A0200508A0200508A0250000A020800F20200",
                    INIT_36 => X"0200020200020800020800F202005108B0E858280002000208000200508A0200",
                    INIT_37 => X"00508A0250000A020800F202005108B0E85008B0E85008B0E858280000000000",
                    INIT_38 => X"000202000208000200508A0200508A0200508A0250000A0200508A0200508A02",
                    INIT_39 => X"E893E8A00200A00200080208000A09F20200030B5108B0E85828000000000002",
                    INIT_3A => X"EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3A0A00B93",
                    INIT_3B => X"A513A5CD93ED551DCD0585A5A5A5CD050D2813020001010300050513130300B3",
                    INIT_3C => X"88708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D1828",
                    INIT_3D => X"0808080808080808080808082800F3CECE00500E8E0E28708870887088708870",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                   INITP_00 => X"B6DB6D6C8229BF08BF89F85F5BF3BD1FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"5CCEA77D00BF4C6F8B137C5B1F66FD6D627FFFFFFFFFFF5AD6A7FFFFFFC00099",
                   INITP_02 => X"FFCEAC36E76A675676D467559DB6A1D7445EE6EBB6AF6EDF75DF7BEF776FA670",
                   INITP_03 => X"0400081B46AB556C090D55B0243556C090D55B0350C4E7FFFE6DBAC27F0804E7",
                   INITP_04 => X"33F6FB6CA86DC0324C86E59DAAAAAAF200314114001165965956AAD808060181",
                   INITP_05 => X"FFFFE013AC27E4FC2009841282112844B919174D2C9324A4A11A114FDBB69D40",
                   INITP_06 => X"1131113A94989D222222222622274989D130276DEAAA924924A6276DB7B6DC9C",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFE435554C880014800719F25252AA14921A27529111111")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_h(31 downto 0),
                  DOPA => data_out_a_h(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_h(31 downto 0),
                  DOPB => data_out_b_h(35 downto 32), 
                   DIB => data_in_b_h(31 downto 0),
                  DIPB => data_in_b_h(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900029BA0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"F0201000500000600133E0879201E087930112FF13FFB001B0310337008E5000",
                    INIT_09 => X"A030D00110200002D001103A0002D1010002D20100020041003013005000F023",
                    INIT_0A => X"5000005920930059609FD00330031301003020AED3FFD1010002D20100020041",
                    INIT_0B => X"D1031108D0045000008150000177098E093F0210B12BB227A0BAD004B0230177",
                    INIT_0C => X"D10311F4D708D607D506D405D0045000970896079506940520C1D010900331FF",
                    INIT_0D => X"60E1DD00ADD01D30ACC01C2C20E1ACC01C2C1D01607CD00120DAD002B0235000",
                    INIT_0E => X"10300002D00110200002005300C0D00110780002D0011030000200BD00C01D01",
                    INIT_0F => X"005300400002005300500002005300600002005300700002D00110780002D001",
                    INIT_10 => X"A10010010510A10010010410A1001030607CD002B023500060E19D011C010059",
                    INIT_11 => X"116311611172115411201132112D1167500000C9A000102C0710A10010010610",
                    INIT_12 => X"11561120116411721161116F1142116311691167116F114C112011721165116B",
                    INIT_13 => X"000200530060D001102E00020053007000BD100100641A181B0111001120113A",
                    INIT_14 => X"116511661166117511425000005900530040D001102E000200530050D001102E",
                    INIT_15 => X"1120113A1174116E1175116F1163112011641172116F115711001120113A1172",
                    INIT_16 => X"1120112011201120113A11731165117A11691173112011641172116F11571100",
                    INIT_17 => X"A0100002E183C120B220110000641A4B1B011100112011641172116F11571100",
                    INIT_18 => X"0002D1010002D201000200410020D00110280002D00110200002217C1101D001",
                    INIT_19 => X"C3401300B423D1010002D20100020041B023000200641A541B010059D0011029",
                    INIT_1A => X"1301D1010002D20100020041A01001301124D00110400002D00110200002E1B1",
                    INIT_1B => X"0041A01001301128D00110200002E1C6C3401300B42300641A611B010059219F",
                    INIT_1C => X"003000641A711B010059E1E9C3401300B423005921B71301D1010002D2010002",
                    INIT_1D => X"C65016030650152C4506450613010530D00110200002D1010002D20100020041",
                    INIT_1E => X"21F7D1004BA01AB41B075000005921DF9601D1010002D20100020041A060A1C9",
                    INIT_1F => X"3003700160005000005921ED3B001A03005921ED3B001A01D101000221FBD1FF",
                    INIT_20 => X"DD023DFDDD025D025D015000E2089E011E3E50007000DD024D003DF040074007",
                    INIT_21 => X"3DFD02070207DD025D0102070207DD025D025000DD023DFE0207020702070207",
                    INIT_22 => X"5D01020702070207DD023DFD5000020702070207DD023DFE020702070207DD02",
                    INIT_23 => X"3DFE02070207DD025D01DD025D0202075000DD025D020207020702070207DD02",
                    INIT_24 => X"5D020207DD023DFE02070207DD025D010207DD023DFD5000020702070207DD02",
                    INIT_25 => X"DD025D010207DD025D02225DDD023DFDA25B440602071580500002070207DD02",
                    INIT_26 => X"02070207DD025D0195020207DD025D0202072255A267450EDD023DFE02070207",
                    INIT_27 => X"5D0102074400D60296020207158014005D025000D502020702070207DD023DFE",
                    INIT_28 => X"020B30FE7001633062206110600050000207E27A450EDD023DFE02070207DD02",
                    INIT_29 => X"700040061000022AA2A702540430A2A502540420A2A302540410A2A102540400",
                    INIT_2A => X"60005000700040061080022A005322A9100422A9100322A9100222A910015000",
                    INIT_2B => X"1000022AA2C702540420A2C502540410A2C302540400020B30FE700162206110",
                    INIT_2C => X"60005000700040061080022A005322C9100322C9100222C91001500070004006",
                    INIT_2D => X"0277A2F00254040050010217A2A302540410A2EC02540400020B30FE70016110",
                    INIT_2E => X"22F2100222F21001500070006330622040061000022A02380340027702450240",
                    INIT_2F => X"116F1172117211651120116B116311415000700040061080022A005322F21003",
                    INIT_30 => X"1434A1401430A040142C607CD003B0235000005900641AF81B021100112E1172",
                    INIT_31 => X"B0235000A30302AF5000A3030289A31CD503A2401401A340A316D503A550152A",
                    INIT_32 => X"01FD1000500000590053003000530020A30302CFA1401430A040142C607CD002",
                    INIT_33 => X"1164116111425000032E033103346D0010F3500001FD1002500001FD10015000",
                    INIT_34 => X"5000005900641A3D1B03110011731174116E1165116D11751167117211611120",
                    INIT_35 => X"9D01E35B9E01E35B9F011FFF1EFF1DFF5000D003301FA040142C634BD001B023",
                    INIT_36 => X"0070D00110200002D0011049000200BD10B2303040064006400640065000E35B",
                    INIT_37 => X"5000C54015809000B700B607B589940000590053004000530050005300600053",
                    INIT_38 => X"006000530070D00110200002D001104B000200BD105230304006400640064006",
                    INIT_39 => X"400640065000C54015809000B700B607B5899400005900530040005300500053",
                    INIT_3A => X"00500053006000530070D00110200002D001104C000200BD10B0303040064006",
                    INIT_3B => X"40064006400640065000C54015809000B700B607B58994000059005300400053",
                    INIT_3C => X"0040005300500053006000530070D00110200002D001105A000200BD00203030",
                    INIT_3D => X"105000021A000B001C0150001000970010009600100095001000940000590053",
                    INIT_3E => X"41064106410601C000BD1050400640064006400600B00059005300C00002D001",
                    INIT_3F => X"1700160015001401102000C91050400640064006400600B04410348F31704106",
                    INIT_40 => X"3030400640064006400600B05503035800BD1050400640064006400600B000C9",
                    INIT_41 => X"40064006400600B000C910503030400640064006400600B035FC035800C91050",
                    INIT_42 => X"005300500053006000530070D00110200002D0011054000200BD10B430304006",
                    INIT_43 => X"00B024524A06243F03BC12B500B0036200B0038000B0039E00B0005900530040",
                    INIT_44 => X"4A06245203BC125700B024524A06244B03BC125800B024524A06244503BC1256",
                    INIT_45 => X"0100100712004A0030E040064006400600A03A1FDA1F63DEDC061C014A072452",
                    INIT_46 => X"D105A472420E11011100420E420E645FD0E04002420F2466420E2465C01021A0",
                    INIT_47 => X"400640064006400600B04C064C064C064C063C070059005300100C101100A46C",
                    INIT_48 => X"00BD104003DB1000248ED40100BD104050000059005900C924C0347F00BD1050",
                    INIT_49 => X"03DB100324A0D40800BD104003DB1002249AD40400BD104003DB10012494D402",
                    INIT_4A => X"04A5D00A24AB31019109500090095000E4A69F011FFF500004A5DF091F805000",
                    INIT_4B => X"1200D1091110900A24B524C2BE009F0164BC310291091EFF1FFF5000D1091120",
                    INIT_4C => X"04B304AB4010310F40064006400640064006400704A15000420E120150004206",
                    INIT_4D => X"24E1133224E113315000400E100004500340A4E004B30400A4DE04B30500A4DC",
                    INIT_4E => X"0059D3010002D1010002113AD10100021152D10100021152D101000211451333",
                    INIT_4F => X"4006400604A15000430E13010059005304A90002D1010002113AD10100021153",
                    INIT_50 => X"5000400E1000A51004B304AB002004AB003004AB4010310F4006400640064006",
                    INIT_51 => X"D1010002113AD101000211530059D10100021152D10100021152D10100021145",
                    INIT_52 => X"900B10001000D00B500250081000A535D0015000430E13010059005304A90002",
                    INIT_53 => X"2543D0102539D020900B10001000D00B50205080100025402543D001252DD002",
                    INIT_54 => X"D002900BD00B5002100C2548D002900BA552D0015000400E10015000400E1000",
                    INIT_55 => X"10001280A564D00150002558D020900BD00B502010C02552D020900B5000254E",
                    INIT_56 => X"100012801100A575D0015000256AE565420E05A010011280256AE55F420E05A0",
                    INIT_57 => X"A58ED0015000257DE577420E4108057E100112801100257DE56F420E4108057E",
                    INIT_58 => X"D020900B5000400E2587D002900B900B900BD00B5004500210002580D002900B",
                    INIT_59 => X"5000400E400E400E400E400E2595D020900B900B900BD00B504050201000258E",
                    INIT_5A => X"25ABD002900B900B900BD00B500050024000410E100025A2D002900BA5B1D001",
                    INIT_5B => X"900B900BD00B5000502040064006400640064000410E100025B1D020900B5000",
                    INIT_5C => X"04B3500004ABA010112C607CD001B02350000059005304A9500025BED020900B",
                    INIT_5D => X"0053003000530040A5E104C5A1101130A010112C607CD002B023500000590053",
                    INIT_5E => X"114E500004FDA1101130A010112CA3101101A2101134607CD003B02350000059",
                    INIT_5F => X"005911001164116E1175116F116611201165116311691176116511641120116F",
                    INIT_60 => X"0002D001104E0002A61A0527A010112C607CD001B0235000005900641AEF1B05",
                    INIT_61 => X"D001104F0002D0011046000250000059D00110450002D001104E0002D001104F",
                    INIT_62 => X"A310112C607CD001B02350000059D00110440002D001104E0002D00110550002",
                    INIT_63 => X"00300610056B00300510056B00300410056B0030055C11330030E66005270030",
                    INIT_64 => X"005300B00B10056B00300A10056B00300910056B00300810056B00300710056B",
                    INIT_65 => X"50000059005300400053005000530060005300700053008000530090005300A0",
                    INIT_66 => X"A310112C607CD002B02350000059D00110520002D00110520002D00110450002",
                    INIT_67 => X"005900530010056B0030A310112C607CD001B0235000055C01400030A4101130",
                    INIT_68 => X"0030055C11440030055C11CC0030E5FF05270030A310112C607CD001B0235000",
                    INIT_69 => X"A310112C607CD008A010112A607CD008A0101129607CD003B023500000590546",
                    INIT_6A => X"A1401401055C0030A1401401055CA14000301434055C11550030E5FF05270030",
                    INIT_6B => X"A1401401055C0030A1401401055CA14000301430055C0030A1401401055C0030",
                    INIT_6C => X"607CD001B0235000005905460030055C11440030055C0030A1401401055C0030",
                    INIT_6D => X"056B00300410056B0030055C11BE0030055C11CC0030E5FF05270030A310112C",
                    INIT_6E => X"D008A010112A607CD008A0101129607CD003B023500000590053004000530010",
                    INIT_6F => X"0030A1401401055CA14000301434055C11550030E5FF05270030A310112C607C",
                    INIT_70 => X"0030A1401401055CA14000301430055C0030A1401401055C0030A1401401055C",
                    INIT_71 => X"00300410056B0030055C11BE0030055C0030A1401401055C0030A1401401055C",
                    INIT_72 => X"0527002007981700A210112C607CD001B023500000590053004000530010056B",
                    INIT_73 => X"D1002764D1034100057E00204100057E00201100055C11F0002014001301E5FF",
                    INIT_74 => X"D6004600076F1600A753C73027561601674AC73027564600410E410E16002746",
                    INIT_75 => X"D70007406736D34113010781016005A000200160275604301600675604306756",
                    INIT_76 => X"1B405000272E0546002003B002A007AA00590B300A20276E276E07AA00596765",
                    INIT_77 => X"4B0E277A4B0E9A01277FDA00ABB03A079A010A300BA04A0E4A0E4A0E9A010A30",
                    INIT_78 => X"2794DA001DFEACB03A079A010A300BA04A0E4A0E4A0E9A010A301B4031015000",
                    INIT_79 => X"1001E1001001E1001001E100104011005000ECB04C102CD0278E9A0141064D07",
                    INIT_7A => X"9C010053A0C01D071C071C405000E1001001E1001001E1001001E1001001E100",
                    INIT_7B => X"116D11B01100110011671172116111691174116C1175116D50000059E7AD9D01",
                    INIT_7C => X"11001100117411651173116511721192110011001170116D11751164116D1165",
                    INIT_7D => X"11011100117311791173113311011100116E116F1169117311721165117611BB",
                    INIT_7E => X"115F116711651172115F11631132116911EB110111001170116C116511681177",
                    INIT_7F => X"110011641172115F116711651172115F11631132116911081103110011721177",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"8A28A28A28A228A0D0080DD28AAA00C08AAA90D2AAD136AA228AAA0A8AB742AA",
                   INITP_02 => X"2D082AAAAAAAAAAAAAAAAAAAAAAA2288A228882AAAAAAAAAAAAAA0041040D2D6",
                   INITP_03 => X"36A5A5ADD82A6AA351548AAA20B429AA848B420A6AA1228B42AA2828AAA228A6",
                   INITP_04 => X"242022A8A828AD8A8A28D8AA28A8A2AA2A22A2AA2A8AA8AA2A2A28AA882D2E05",
                   INITP_05 => X"AAAAB4A888B54A28B82E388D6D2A222D2E38E2356D2A2222D2E38E388D56B62A",
                   INITP_06 => X"9355A222228A455B7740A034A82AAAAAAAA928A28A88E00D2EBB44D00034A82A",
                   INITP_07 => X"002554015495528A202DDDDA222228A4559355A222228A4559355A222228A455",
                   INITP_08 => X"55154A03751756740015075678278278278222288888A2915491542915429552",
                   INITP_09 => X"5A4AAA28AA8A28A0889038E3A0555A4920B5C028AC22D2A28C88C88C88C8AA09",
                   INITP_0A => X"CAD602D6032B60B60CB0830B0830C924CC0202CC020324AAA28A8A2893A22055",
                   INITP_0B => X"AAAAAAAAA800434A88E00D2AA834AAB0081554C2C020530C955C020309C02030",
                   INITP_0C => X"A2034A000D2A28A2A888888882082082082088380D2A28A28A2A8A28A2E0D2A0",
                   INITP_0D => X"06020E0343434A8882208380D2A2081818180818181808380D0D0D2A208380D2",
                   INITP_0E => X"65D045542A0282AB4D62083358D8D95375860803880D2A220882060606020606",
                   INITP_0F => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD604A66666660A095D0115502")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900029BA0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"F0201000500000600133E0879201E087930112FF13FFB001B0310337008E5000",
                    INIT_09 => X"A030D00110200002D001103A0002D1010002D20100020041003013005000F023",
                    INIT_0A => X"5000005920930059609FD00330031301003020AED3FFD1010002D20100020041",
                    INIT_0B => X"D1031108D0045000008150000177098E093F0210B12BB227A0BAD004B0230177",
                    INIT_0C => X"D10311F4D708D607D506D405D0045000970896079506940520C1D010900331FF",
                    INIT_0D => X"60E1DD00ADD01D30ACC01C2C20E1ACC01C2C1D01607CD00120DAD002B0235000",
                    INIT_0E => X"10300002D00110200002005300C0D00110780002D0011030000200BD00C01D01",
                    INIT_0F => X"005300400002005300500002005300600002005300700002D00110780002D001",
                    INIT_10 => X"A10010010510A10010010410A1001030607CD002B023500060E19D011C010059",
                    INIT_11 => X"116311611172115411201132112D1167500000C9A000102C0710A10010010610",
                    INIT_12 => X"11561120116411721161116F1142116311691167116F114C112011721165116B",
                    INIT_13 => X"000200530060D001102E00020053007000BD100100641A181B0111001120113A",
                    INIT_14 => X"116511661166117511425000005900530040D001102E000200530050D001102E",
                    INIT_15 => X"1120113A1174116E1175116F1163112011641172116F115711001120113A1172",
                    INIT_16 => X"1120112011201120113A11731165117A11691173112011641172116F11571100",
                    INIT_17 => X"A0100002E183C120B220110000641A4B1B011100112011641172116F11571100",
                    INIT_18 => X"0002D1010002D201000200410020D00110280002D00110200002217C1101D001",
                    INIT_19 => X"C3401300B423D1010002D20100020041B023000200641A541B010059D0011029",
                    INIT_1A => X"1301D1010002D20100020041A01001301124D00110400002D00110200002E1B1",
                    INIT_1B => X"0041A01001301128D00110200002E1C6C3401300B42300641A611B010059219F",
                    INIT_1C => X"003000641A711B010059E1E9C3401300B423005921B71301D1010002D2010002",
                    INIT_1D => X"C65016030650152C4506450613010530D00110200002D1010002D20100020041",
                    INIT_1E => X"21F7D1004BA01AB41B075000005921DF9601D1010002D20100020041A060A1C9",
                    INIT_1F => X"3003700160005000005921ED3B001A03005921ED3B001A01D101000221FBD1FF",
                    INIT_20 => X"DD023DFDDD025D025D015000E2089E011E3E50007000DD024D003DF040074007",
                    INIT_21 => X"3DFD02070207DD025D0102070207DD025D025000DD023DFE0207020702070207",
                    INIT_22 => X"5D01020702070207DD023DFD5000020702070207DD023DFE020702070207DD02",
                    INIT_23 => X"3DFE02070207DD025D01DD025D0202075000DD025D020207020702070207DD02",
                    INIT_24 => X"5D020207DD023DFE02070207DD025D010207DD023DFD5000020702070207DD02",
                    INIT_25 => X"DD025D010207DD025D02225DDD023DFDA25B440602071580500002070207DD02",
                    INIT_26 => X"02070207DD025D0195020207DD025D0202072255A267450EDD023DFE02070207",
                    INIT_27 => X"5D0102074400D60296020207158014005D025000D502020702070207DD023DFE",
                    INIT_28 => X"020B30FE7001633062206110600050000207E27A450EDD023DFE02070207DD02",
                    INIT_29 => X"700040061000022AA2A702540430A2A502540420A2A302540410A2A102540400",
                    INIT_2A => X"60005000700040061080022A005322A9100422A9100322A9100222A910015000",
                    INIT_2B => X"1000022AA2C702540420A2C502540410A2C302540400020B30FE700162206110",
                    INIT_2C => X"60005000700040061080022A005322C9100322C9100222C91001500070004006",
                    INIT_2D => X"0277A2F00254040050010217A2A302540410A2EC02540400020B30FE70016110",
                    INIT_2E => X"22F2100222F21001500070006330622040061000022A02380340027702450240",
                    INIT_2F => X"116F1172117211651120116B116311415000700040061080022A005322F21003",
                    INIT_30 => X"1434A1401430A040142C607CD003B0235000005900641AF81B021100112E1172",
                    INIT_31 => X"B0235000A30302AF5000A3030289A31CD503A2401401A340A316D503A550152A",
                    INIT_32 => X"01FD1000500000590053003000530020A30302CFA1401430A040142C607CD002",
                    INIT_33 => X"1164116111425000032E033103346D0010F3500001FD1002500001FD10015000",
                    INIT_34 => X"5000005900641A3D1B03110011731174116E1165116D11751167117211611120",
                    INIT_35 => X"9D01E35B9E01E35B9F011FFF1EFF1DFF5000D003301FA040142C634BD001B023",
                    INIT_36 => X"0070D00110200002D0011049000200BD10B2303040064006400640065000E35B",
                    INIT_37 => X"5000C54015809000B700B607B589940000590053004000530050005300600053",
                    INIT_38 => X"006000530070D00110200002D001104B000200BD105230304006400640064006",
                    INIT_39 => X"400640065000C54015809000B700B607B5899400005900530040005300500053",
                    INIT_3A => X"00500053006000530070D00110200002D001104C000200BD10B0303040064006",
                    INIT_3B => X"40064006400640065000C54015809000B700B607B58994000059005300400053",
                    INIT_3C => X"0040005300500053006000530070D00110200002D001105A000200BD00203030",
                    INIT_3D => X"105000021A000B001C0150001000970010009600100095001000940000590053",
                    INIT_3E => X"41064106410601C000BD1050400640064006400600B00059005300C00002D001",
                    INIT_3F => X"1700160015001401102000C91050400640064006400600B04410348F31704106",
                    INIT_40 => X"3030400640064006400600B05503035800BD1050400640064006400600B000C9",
                    INIT_41 => X"40064006400600B000C910503030400640064006400600B035FC035800C91050",
                    INIT_42 => X"005300500053006000530070D00110200002D0011054000200BD10B430304006",
                    INIT_43 => X"00B024524A06243F03BC12B500B0036200B0038000B0039E00B0005900530040",
                    INIT_44 => X"4A06245203BC125700B024524A06244B03BC125800B024524A06244503BC1256",
                    INIT_45 => X"0100100712004A0030E040064006400600A03A1FDA1F63DEDC061C014A072452",
                    INIT_46 => X"D105A472420E11011100420E420E645FD0E04002420F2466420E2465C01021A0",
                    INIT_47 => X"400640064006400600B04C064C064C064C063C070059005300100C101100A46C",
                    INIT_48 => X"00BD104003DB1000248ED40100BD104050000059005900C924C0347F00BD1050",
                    INIT_49 => X"03DB100324A0D40800BD104003DB1002249AD40400BD104003DB10012494D402",
                    INIT_4A => X"04A5D00A24AB31019109500090095000E4A69F011FFF500004A5DF091F805000",
                    INIT_4B => X"1200D1091110900A24B524C2BE009F0164BC310291091EFF1FFF5000D1091120",
                    INIT_4C => X"04B304AB4010310F40064006400640064006400704A15000420E120150004206",
                    INIT_4D => X"24E1133224E113315000400E100004500340A4E004B30400A4DE04B30500A4DC",
                    INIT_4E => X"0059D3010002D1010002113AD10100021152D10100021152D101000211451333",
                    INIT_4F => X"4006400604A15000430E13010059005304A90002D1010002113AD10100021153",
                    INIT_50 => X"5000400E1000A51004B304AB002004AB003004AB4010310F4006400640064006",
                    INIT_51 => X"D1010002113AD101000211530059D10100021152D10100021152D10100021145",
                    INIT_52 => X"900B10001000D00B500250081000A535D0015000430E13010059005304A90002",
                    INIT_53 => X"2543D0102539D020900B10001000D00B50205080100025402543D001252DD002",
                    INIT_54 => X"D002900BD00B5002100C2548D002900BA552D0015000400E10015000400E1000",
                    INIT_55 => X"10001280A564D00150002558D020900BD00B502010C02552D020900B5000254E",
                    INIT_56 => X"100012801100A575D0015000256AE565420E05A010011280256AE55F420E05A0",
                    INIT_57 => X"A58ED0015000257DE577420E4108057E100112801100257DE56F420E4108057E",
                    INIT_58 => X"D020900B5000400E2587D002900B900B900BD00B5004500210002580D002900B",
                    INIT_59 => X"5000400E400E400E400E400E2595D020900B900B900BD00B504050201000258E",
                    INIT_5A => X"25ABD002900B900B900BD00B500050024000410E100025A2D002900BA5B1D001",
                    INIT_5B => X"900B900BD00B5000502040064006400640064000410E100025B1D020900B5000",
                    INIT_5C => X"04B3500004ABA010112C607CD001B02350000059005304A9500025BED020900B",
                    INIT_5D => X"0053003000530040A5E104C5A1101130A010112C607CD002B023500000590053",
                    INIT_5E => X"114E500004FDA1101130A010112CA3101101A2101134607CD003B02350000059",
                    INIT_5F => X"005911001164116E1175116F116611201165116311691176116511641120116F",
                    INIT_60 => X"0002D001104E0002A61A0527A010112C607CD001B0235000005900641AEF1B05",
                    INIT_61 => X"D001104F0002D0011046000250000059D00110450002D001104E0002D001104F",
                    INIT_62 => X"A310112C607CD001B02350000059D00110440002D001104E0002D00110550002",
                    INIT_63 => X"00300610056B00300510056B00300410056B0030055C11330030E66005270030",
                    INIT_64 => X"005300B00B10056B00300A10056B00300910056B00300810056B00300710056B",
                    INIT_65 => X"50000059005300400053005000530060005300700053008000530090005300A0",
                    INIT_66 => X"A310112C607CD002B02350000059D00110520002D00110520002D00110450002",
                    INIT_67 => X"005900530010056B0030A310112C607CD001B0235000055C01400030A4101130",
                    INIT_68 => X"0030055C11440030055C11CC0030E5FF05270030A310112C607CD001B0235000",
                    INIT_69 => X"A310112C607CD008A010112A607CD008A0101129607CD003B023500000590546",
                    INIT_6A => X"A1401401055C0030A1401401055CA14000301434055C11550030E5FF05270030",
                    INIT_6B => X"A1401401055C0030A1401401055CA14000301430055C0030A1401401055C0030",
                    INIT_6C => X"607CD001B0235000005905460030055C11440030055C0030A1401401055C0030",
                    INIT_6D => X"056B00300410056B0030055C11BE0030055C11CC0030E5FF05270030A310112C",
                    INIT_6E => X"D008A010112A607CD008A0101129607CD003B023500000590053004000530010",
                    INIT_6F => X"0030A1401401055CA14000301434055C11550030E5FF05270030A310112C607C",
                    INIT_70 => X"0030A1401401055CA14000301430055C0030A1401401055C0030A1401401055C",
                    INIT_71 => X"00300410056B0030055C11BE0030055C0030A1401401055C0030A1401401055C",
                    INIT_72 => X"0527002007981700A210112C607CD001B023500000590053004000530010056B",
                    INIT_73 => X"D1002764D1034100057E00204100057E00201100055C11F0002014001301E5FF",
                    INIT_74 => X"D6004600076F1600A753C73027561601674AC73027564600410E410E16002746",
                    INIT_75 => X"D70007406736D34113010781016005A000200160275604301600675604306756",
                    INIT_76 => X"1B405000272E0546002003B002A007AA00590B300A20276E276E07AA00596765",
                    INIT_77 => X"4B0E277A4B0E9A01277FDA00ABB03A079A010A300BA04A0E4A0E4A0E9A010A30",
                    INIT_78 => X"2794DA001DFEACB03A079A010A300BA04A0E4A0E4A0E9A010A301B4031015000",
                    INIT_79 => X"1001E1001001E1001001E100104011005000ECB04C102CD0278E9A0141064D07",
                    INIT_7A => X"9C010053A0C01D071C071C405000E1001001E1001001E1001001E1001001E100",
                    INIT_7B => X"116D11B01100110011671172116111691174116C1175116D50000059E7AD9D01",
                    INIT_7C => X"11001100117411651173116511721192110011001170116D11751164116D1165",
                    INIT_7D => X"11011100117311791173113311011100116E116F1169117311721165117611BB",
                    INIT_7E => X"115F116711651172115F11631132116911EB110111001170116C116511681177",
                    INIT_7F => X"110011641172115F116711651172115F11631132116911081103110011721177",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"8A28A28A28A228A0D0080DD28AAA00C08AAA90D2AAD136AA228AAA0A8AB742AA",
                   INITP_02 => X"2D082AAAAAAAAAAAAAAAAAAAAAAA2288A228882AAAAAAAAAAAAAA0041040D2D6",
                   INITP_03 => X"36A5A5ADD82A6AA351548AAA20B429AA848B420A6AA1228B42AA2828AAA228A6",
                   INITP_04 => X"242022A8A828AD8A8A28D8AA28A8A2AA2A22A2AA2A8AA8AA2A2A28AA882D2E05",
                   INITP_05 => X"AAAAB4A888B54A28B82E388D6D2A222D2E38E2356D2A2222D2E38E388D56B62A",
                   INITP_06 => X"9355A222228A455B7740A034A82AAAAAAAA928A28A88E00D2EBB44D00034A82A",
                   INITP_07 => X"002554015495528A202DDDDA222228A4559355A222228A4559355A222228A455",
                   INITP_08 => X"55154A03751756740015075678278278278222288888A2915491542915429552",
                   INITP_09 => X"5A4AAA28AA8A28A0889038E3A0555A4920B5C028AC22D2A28C88C88C88C8AA09",
                   INITP_0A => X"CAD602D6032B60B60CB0830B0830C924CC0202CC020324AAA28A8A2893A22055",
                   INITP_0B => X"AAAAAAAAA800434A88E00D2AA834AAB0081554C2C020530C955C020309C02030",
                   INITP_0C => X"A2034A000D2A28A2A888888882082082082088380D2A28A28A2A8A28A2E0D2A0",
                   INITP_0D => X"06020E0343434A8882208380D2A2081818180818181808380D0D0D2A208380D2",
                   INITP_0E => X"65D045542A0282AB4D62083358D8D95375860803880D2A220882060606020606",
                   INITP_0F => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD604A66666660A095D0115502")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_2k_generate;
  --
  --	
  ram_4k_generate : if (C_RAM_SIZE_KWORDS = 4) generate
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      data_in_a <= "000000000000000000000000000000000000";
      --
      s6_a11_flop: FD
      port map (  D => address(11),
                  Q => pipe_a11,
                  C => clk);
      --
      s6_4k_mux0_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(0),
                I1 => data_out_a_hl(0),
                I2 => data_out_a_ll(1),
                I3 => data_out_a_hl(1),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(0),
                O6 => instruction(1));
      --
      s6_4k_mux2_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(2),
                I1 => data_out_a_hl(2),
                I2 => data_out_a_ll(3),
                I3 => data_out_a_hl(3),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(2),
                O6 => instruction(3));
      --
      s6_4k_mux4_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(4),
                I1 => data_out_a_hl(4),
                I2 => data_out_a_ll(5),
                I3 => data_out_a_hl(5),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(4),
                O6 => instruction(5));
      --
      s6_4k_mux6_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(6),
                I1 => data_out_a_hl(6),
                I2 => data_out_a_ll(7),
                I3 => data_out_a_hl(7),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(6),
                O6 => instruction(7));
      --
      s6_4k_mux8_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(32),
                I1 => data_out_a_hl(32),
                I2 => data_out_a_lh(0),
                I3 => data_out_a_hh(0),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(8),
                O6 => instruction(9));
      --
      s6_4k_mux10_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(1),
                I1 => data_out_a_hh(1),
                I2 => data_out_a_lh(2),
                I3 => data_out_a_hh(2),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(10),
                O6 => instruction(11));
      --
      s6_4k_mux12_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(3),
                I1 => data_out_a_hh(3),
                I2 => data_out_a_lh(4),
                I3 => data_out_a_hh(4),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(12),
                O6 => instruction(13));
      --
      s6_4k_mux14_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(5),
                I1 => data_out_a_hh(5),
                I2 => data_out_a_lh(6),
                I3 => data_out_a_hh(6),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(14),
                O6 => instruction(15));
      --
      s6_4k_mux16_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(7),
                I1 => data_out_a_hh(7),
                I2 => data_out_a_lh(32),
                I3 => data_out_a_hh(32),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(16),
                O6 => instruction(17));
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_ll <= "000" & data_out_b_ll(32) & "000000000000000000000000" & data_out_b_ll(7 downto 0);
        data_in_b_lh <= "000" & data_out_b_lh(32) & "000000000000000000000000" & data_out_b_lh(7 downto 0);
        data_in_b_hl <= "000" & data_out_b_hl(32) & "000000000000000000000000" & data_out_b_hl(7 downto 0);
        data_in_b_hh <= "000" & data_out_b_hh(32) & "000000000000000000000000" & data_out_b_hh(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b_l(3 downto 0) <= "0000";
        we_b_h(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
        jtag_dout <= data_out_b_lh(32) & data_out_b_lh(7 downto 0) & data_out_b_ll(32) & data_out_b_ll(7 downto 0);
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_lh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_ll <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        data_in_b_hh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_hl <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        --
        s6_4k_jtag_we_lut: LUT6_2
        generic map (INIT => X"8000000020000000")
        port map( I0 => jtag_we,
                  I1 => jtag_addr(11),
                  I2 => '1',
                  I3 => '1',
                  I4 => '1',
                  I5 => '1',
                  O5 => jtag_we_l,
                  O6 => jtag_we_h);
        --
        we_b_l(3 downto 0) <= jtag_we_l & jtag_we_l & jtag_we_l & jtag_we_l;
        we_b_h(3 downto 0) <= jtag_we_h & jtag_we_h & jtag_we_h & jtag_we_h;
        --
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
        --
        s6_4k_jtag_mux0_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(0),
                  I1 => data_out_b_hl(0),
                  I2 => data_out_b_ll(1),
                  I3 => data_out_b_hl(1),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(0),
                  O6 => jtag_dout(1));
        --
        s6_4k_jtag_mux2_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(2),
                  I1 => data_out_b_hl(2),
                  I2 => data_out_b_ll(3),
                  I3 => data_out_b_hl(3),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(2),
                  O6 => jtag_dout(3));
        --
        s6_4k_jtag_mux4_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(4),
                  I1 => data_out_b_hl(4),
                  I2 => data_out_b_ll(5),
                  I3 => data_out_b_hl(5),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(4),
                  O6 => jtag_dout(5));
        --
        s6_4k_jtag_mux6_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(6),
                  I1 => data_out_b_hl(6),
                  I2 => data_out_b_ll(7),
                  I3 => data_out_b_hl(7),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(6),
                  O6 => jtag_dout(7));
        --
        s6_4k_jtag_mux8_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(32),
                  I1 => data_out_b_hl(32),
                  I2 => data_out_b_lh(0),
                  I3 => data_out_b_hh(0),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(8),
                  O6 => jtag_dout(9));
        --
        s6_4k_jtag_mux10_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(1),
                  I1 => data_out_b_hh(1),
                  I2 => data_out_b_lh(2),
                  I3 => data_out_b_hh(2),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(10),
                  O6 => jtag_dout(11));
        --
        s6_4k_jtag_mux12_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(3),
                  I1 => data_out_b_hh(3),
                  I2 => data_out_b_lh(4),
                  I3 => data_out_b_hh(4),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(12),
                  O6 => jtag_dout(13));
        --
        s6_4k_jtag_mux14_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(5),
                  I1 => data_out_b_hh(5),
                  I2 => data_out_b_lh(6),
                  I3 => data_out_b_hh(6),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(14),
                  O6 => jtag_dout(15));
        --
        s6_4k_jtag_mux16_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(7),
                  I1 => data_out_b_hh(7),
                  I2 => data_out_b_lh(32),
                  I3 => data_out_b_hh(32),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(16),
                  O6 => jtag_dout(17));
      --
      end generate loader;
      --
      kcpsm6_rom_ll: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400BA81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"30012002013A02010201024130000023200000603387018701FFFF0131378E00",
                    INIT_05 => X"030804008100778E3F102B27BA042377005993599F03030130AEFF0102010241",
                    INIT_06 => X"E100D030C02CE1C02C017C01DA02230003F408070605040008070605C11003FF",
                    INIT_07 => X"53400253500253600253700201780201300201200253C0017802013002BDC001",
                    INIT_08 => X"6361725420322D6700C9002C1000011000011000011000307C022300E1010159",
                    INIT_09 => X"025360012E025370BD0164180100203A56206472616F426369676F4C2072656B",
                    INIT_0A => X"203A746E756F632064726F5700203A72656666754200595340012E025350012E",
                    INIT_0B => X"100283202000644B01002064726F5700202020203A73657A69732064726F5700",
                    INIT_0C => X"40002301020102412302645401590129020102010241200128020120027C0101",
                    INIT_0D => X"41103028012002C6400023646101599F010102010241103024014002012002B1",
                    INIT_0E => X"5003502C0606013001200201020102413064710159E940002359B70101020102",
                    INIT_0F => X"0301000059ED000359ED00010102FBFFF700A0B4070059DF01010201024160C9",
                    INIT_10 => X"FD07070201070702020002FE0707070702FD0202010008013E00000200F00707",
                    INIT_11 => X"FE0707020102020700020207070707020107070702FD0007070702FE07070702",
                    INIT_12 => X"02010702025D02FD5B06078000070702020702FE070702010702FD0007070702",
                    INIT_13 => X"010700020207800002000207070702FE07070201020702020755670E02FE0707",
                    INIT_14 => X"0006002AA75430A55420A35410A154000BFE013020100000077A0E02FE070702",
                    INIT_15 => X"002AC75420C55410C354000BFE01201000000006802A53A904A903A902A90100",
                    INIT_16 => X"77F054000117A35410EC54000BFE011000000006802A53C903C902C901000006",
                    INIT_17 => X"6F727265206B6341000006802A53F203F202F2010000302006002A3840774540",
                    INIT_18 => X"230003AF0003891C034001401603502A344030402C7C0323005964F802002E72",
                    INIT_19 => X"646142002E313400F300FD0200FD0100FD0000595330532003CF4030402C7C02",
                    INIT_1A => X"015B015B01FFFFFF00031F402C4B01230059643D030073746E656D7567726120",
                    INIT_1B => X"0040800000078900595340535053605370012002014902BDB23006060606005B",
                    INIT_1C => X"06060040800000078900595340535053605370012002014B02BD523006060606",
                    INIT_1D => X"060606060040800000078900595340535053605370012002014C02BDB0300606",
                    INIT_1E => X"5002000001000000000000000000595340535053605370012002015A02BD2030",
                    INIT_1F => X"0000000120C95006060606B0108F7006060606C0BD5006060606B05953C00201",
                    INIT_20 => X"060606B0C9503006060606B0FC58C9503006060606B00358BD5006060606B0C9",
                    INIT_21 => X"B052063FBCB5B062B080B09EB0595340535053605370012002015402BDB43006",
                    INIT_22 => X"00070000E0060606A01F1FDE060107520652BC57B052064BBC58B0520645BC56",
                    INIT_23 => X"06060606B0060606060759531010006C05720E01000E0E5FE0020F660E6510A0",
                    INIT_24 => X"DB03A008BD40DB029A04BD40DB019402BD40DB008E01BD40005959C9C07FBD50",
                    INIT_25 => X"0009100AB5C20001BC0209FFFF000920A50AAB0109000900A601FF00A5098000",
                    INIT_26 => X"E132E131000E005040E0B300DEB300DCB3AB100F060606060607A1000E010006",
                    INIT_27 => X"0606A1000E015953A90201023A01025359010201023A01025201025201024533",
                    INIT_28 => X"01023A01025359010252010252010245000E0010B3AB20AB30AB100F06060606",
                    INIT_29 => X"431039200B00000B2080004043012D020B00000B0208003501000E015953A902",
                    INIT_2A => X"008064010058200B0B20C052200B004E020B0B020C48020B5201000E01000E00",
                    INIT_2B => X"8E01007D770E087E0180007D6F0E087E0080007501006A650EA001806A5F0EA0",
                    INIT_2C => X"000E0E0E0E0E95200B0B0B0B4020008E200B000E87020B0B0B0B04020080020B",
                    INIT_2D => X"0B0B0B002006060606000E00B1200B00AB020B0B0B0B0002000E00A2020BB101",
                    INIT_2E => X"53305340E1C51030102C7C0223005953B300AB102C7C0123005953A900BE200B",
                    INIT_2F => X"5900646E756F6620656369766564206F4E00FD1030102C100110347C03230059",
                    INIT_30 => X"014F020146020059014502014E02014F02014E021A27102C7C0123005964EF05",
                    INIT_31 => X"30106B30106B30106B305C3330602730102C7C01230059014402014E02015502",
                    INIT_32 => X"005953405350536053705380539053A053B0106B30106B30106B30106B30106B",
                    INIT_33 => X"5953106B30102C7C0123005C40301030102C7C02230059015202015202014502",
                    INIT_34 => X"102C7C08102A7C0810297C0323005946305C44305CCC30FF2730102C7C012300",
                    INIT_35 => X"40015C3040015C4030305C3040015C3040015C3040015C4030345C5530FF2730",
                    INIT_36 => X"6B30106B305CBE305CCC30FF2730102C7C0123005946305C44305C3040015C30",
                    INIT_37 => X"3040015C4030345C5530FF2730102C7C08102A7C0810297C0323005953405310",
                    INIT_38 => X"30106B305CBE305C3040015C3040015C3040015C4030305C3040015C3040015C",
                    INIT_39 => X"006403007E20007E20005CF0200001FF27209800102C7C01230059534053106B",
                    INIT_3A => X"00403641018160A0206056300056305600006F00533056014A3056000E0E0046",
                    INIT_3B => X"0E7A0E017F00B0070130A00E0E0E013040002E4620B0A0AA5930206E6EAA5965",
                    INIT_3C => X"010001000100400000B010D08E0106079400FEB0070130A00E0E0E0130400100",
                    INIT_3D => X"6DB0000067726169746C756D0059AD010153C007074000000100010001000100",
                    INIT_3E => X"01007379733301006E6F6973726576BB00007465736572920000706D75646D65",
                    INIT_3F => X"0064725F6765725F63326908030072775F6765725F633269EB0100706C656877",
                   INITP_00 => X"000000017040E8A1C3A10130010408A41FFFE536008278000000008F80000002",
                   INITP_01 => X"066BE9411F10173831C5C181D008400634FFFFFFFFFFF800000FFFFFFF0CB204",
                   INITP_02 => X"FF00020800010000000100000000143982A33B1CDB11B3619E618C3199B0F81C",
                   INITP_03 => X"A003F0001110000006A000001A8000006A000001DD040FFFEF2480A0259F400F",
                   INITP_04 => X"0C2D56DB50821000616B186682082000000298018010208209500000000C0300",
                   INITP_05 => X"7FFF9BA00B40080400288052020108049B3B334D24110480A01A0130B56D1010",
                   INITP_06 => X"59B6220096DB05AAAB2AAB36C4416DB01619C000000032CB2CB2C00000000501",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFF31155550BA106EA20B55DFF652EEDFB73B4012D555955")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_ll(31 downto 0),
                  DOPA => data_out_a_ll(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_ll(31 downto 0),
                  DOPB => data_out_b_ll(35 downto 32), 
                   DIB => data_in_b_ll(31 downto 0),
                  DIPB => data_in_b_ll(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_lh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"506808006808006800690000000928787808280000F0C9F0C909095858010028",
                    INIT_05 => X"680868280028000404815859D0E8580028001000B0E818890090E96800690000",
                    INIT_06 => X"B0EE560E560E10560E0EB0E890E8582868086B6B6A6A68284B4B4A4A90684818",
                    INIT_07 => X"000000000000000000000000680800680800680800000068080068080000000E",
                    INIT_08 => X"080808080808080828005008035088035088025088025008B0E85828B0CE8E00",
                    INIT_09 => X"00000068080000000008000D0D08080808080808080808080808080808080808",
                    INIT_0A => X"0808080808080808080808080808080808080808082800000068080000006808",
                    INIT_0B => X"5000F0E05908000D0D0808080808080808080808080808080808080808080808",
                    INIT_0C => X"E1095A68006900005800000D0D00680800680069000000680800680800108868",
                    INIT_0D => X"00508008680800F0E1095A000D0D0010896800690000508008680800680800F0",
                    INIT_0E => X"E38B038AA2A28902680800680069000000000D0D00F0E1095A00108968006900",
                    INIT_0F => X"18B8B02800109D8D00109D8D680090E890E8250D0D280010CB680069000050D0",
                    INIT_10 => X"1E01016E2E01016E2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A0",
                    INIT_11 => X"1E01016E2E6E2E01286E2E010101016E2E0101016E1E280101016E1E0101016E",
                    INIT_12 => X"6E2E016E2E116E1ED1A2010A2801016E2E016E1E01016E2E016E1E280101016E",
                    INIT_13 => X"2E01A26B4B010A0A2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E0101",
                    INIT_14 => X"B8A00801D10102D10102D10102D101020118B8B1B1B0B02801F1A26E1E01016E",
                    INIT_15 => X"0801D10102D10102D101020118B8B1B0B028B8A0080100110811081108110828",
                    INIT_16 => X"01D101022801D10102D101020118B8B0B028B8A008010011081108110828B8A0",
                    INIT_17 => X"080808080808080828B8A008010011081108110828B8B1B1A008010101010101",
                    INIT_18 => X"5828D10128D101D1EA518A51D1EA520A0A500A500AB0E8582800000D0D080808",
                    INIT_19 => X"08080828010101B608280008280008280008280000000000D101500A500AB0E8",
                    INIT_1A => X"CEF1CFF1CF0F0F0E286818500AB1E8582800000D0D0808080808080808080808",
                    INIT_1B => X"28E20AC8DBDBDACA000000000000000000680800680800008818A0A0A0A028F1",
                    INIT_1C => X"A0A028E20AC8DBDBDACA000000000000000000680800680800008818A0A0A0A0",
                    INIT_1D => X"A0A0A0A028E20AC8DBDBDACA000000000000000000680800680800008818A0A0",
                    INIT_1E => X"08000D050E2888CB88CB88CA88CA000000000000000000680800680800008018",
                    INIT_1F => X"0B0B0A0A080088A0A0A0A000221A18A0A0A0A0000088A0A0A0A0000000000068",
                    INIT_20 => X"A0A0A000008818A0A0A0A0001A01008818A0A0A0A0002A010088A0A0A0A00000",
                    INIT_21 => X"0012A592010900010001000100000000000000000000680800680800008818A0",
                    INIT_22 => X"0008092518A0A0A0001DEDB1EE8EA512A59201090012A59201090012A5920109",
                    INIT_23 => X"A0A0A0A000A6A6A6A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E010",
                    INIT_24 => X"0108926A00080108926A00080108926A00080108926A000828000000121A0088",
                    INIT_25 => X"096808481292DFCFB218480F0F2868080268921848284828F2CF0F28026F0F28",
                    INIT_26 => X"1209120928A0080201D20202D20202D202022018A0A0A0A0A0A00228A10928A1",
                    INIT_27 => X"A0A00228A1090000020068000868000800690068000868000868000868000809",
                    INIT_28 => X"6800086800080068000868000868000828A008D20202000200022018A0A0A0A0",
                    INIT_29 => X"9268926848080868282808129268926848080868282808D26828A10900000200",
                    INIT_2A => X"0809D2682892684868280892684828926848682808926848D26828A00828A008",
                    INIT_2B => X"D2682812F2A1A00208090812F2A1A002080908D2682812F2A102080912F2A102",
                    INIT_2C => X"28A0A0A0A0A092684848486828280892684828A0926848484868282808926848",
                    INIT_2D => X"4848682828A0A0A0A0A0A008926848289268484848682828A0A008926848D268",
                    INIT_2E => X"00000000D20250085008B0E8582800000228025008B0E8582800000228926848",
                    INIT_2F => X"000808080808080808080808080808080828025008500851885108B0E8582800",
                    INIT_30 => X"6808006808002800680800680800680800680800D3025008B0E8582800000D0D",
                    INIT_31 => X"00030200020200020200020800F302005108B0E8582800680800680800680800",
                    INIT_32 => X"2800000000000000000000000000000000000502000502000402000402000302",
                    INIT_33 => X"00000002005108B0E8582802000052085108B0E8582800680800680800680800",
                    INIT_34 => X"5108B0E85008B0E85008B0E85828000200020800020800F202005108B0E85828",
                    INIT_35 => X"508A0200508A0250000A0200508A0200508A0200508A0250000A020800F20200",
                    INIT_36 => X"0200020200020800020800F202005108B0E858280002000208000200508A0200",
                    INIT_37 => X"00508A0250000A020800F202005108B0E85008B0E85008B0E858280000000000",
                    INIT_38 => X"000202000208000200508A0200508A0200508A0250000A0200508A0200508A02",
                    INIT_39 => X"E893E8A00200A00200080208000A09F20200030B5108B0E85828000000000002",
                    INIT_3A => X"EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3A0A00B93",
                    INIT_3B => X"A513A5CD93ED551DCD0585A5A5A5CD050D2813020001010300050513130300B3",
                    INIT_3C => X"88708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D1828",
                    INIT_3D => X"0808080808080808080808082800F3CECE00500E8E0E28708870887088708870",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                   INITP_00 => X"B6DB6D6C8229BF08BF89F85F5BF3BD1FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"5CCEA77D00BF4C6F8B137C5B1F66FD6D627FFFFFFFFFFF5AD6A7FFFFFFC00099",
                   INITP_02 => X"FFCEAC36E76A675676D467559DB6A1D7445EE6EBB6AF6EDF75DF7BEF776FA670",
                   INITP_03 => X"0400081B46AB556C090D55B0243556C090D55B0350C4E7FFFE6DBAC27F0804E7",
                   INITP_04 => X"33F6FB6CA86DC0324C86E59DAAAAAAF200314114001165965956AAD808060181",
                   INITP_05 => X"FFFFE013AC27E4FC2009841282112844B919174D2C9324A4A11A114FDBB69D40",
                   INITP_06 => X"1131113A94989D222222222622274989D130276DEAAA924924A6276DB7B6DC9C",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFE435554C880014800719F25252AA14921A27529111111")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_lh(31 downto 0),
                  DOPA => data_out_a_lh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_lh(31 downto 0),
                  DOPB => data_out_b_lh(35 downto 32), 
                   DIB => data_in_b_lh(31 downto 0),
                  DIPB => data_in_b_lh(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      --
      kcpsm6_rom_hl: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"32693403006361645F6C65735F6332692E03006364745F6C65735F6332691F03",
                    INIT_01 => X"5F6765725F71616405010072775F6765725F71616431030062645F6C65735F63",
                    INIT_02 => X"6474C8050065746972775F636474C405007375746174735F636474D100006472",
                    INIT_03 => X"64725F6765725F636474E2050072775F6765725F636474CF0500646165725F63",
                    INIT_04 => X"2707007364695F7465675F706D65740506006863726165735F706D6574D30500",
                    INIT_05 => X"5F706D65746B060065746972775F706D65742B060064695F7465675F706D6574",
                    INIT_06 => X"746E6972705F706D6574810600616E5F766E6F635F706D657476060064616572",
                    INIT_07 => X"63E60600746E6972705F706D6574930600766E6F635F706D6574CD0600616E5F",
                    INIT_08 => X"01012210202200203BFF2E00A000B407FFD10000726405010077648804007264",
                    INIT_09 => X"000060768E00608E10A0000110A000018E3F130000032200012A003BFFA01300",
                    INIT_0A => X"232057015E2F205E20105710200820100023236905012301412F012069102023",
                    INIT_0B => X"726F7272450059646A0900646E616D6D6F632064614200410001282300100124",
                    INIT_0C => X"104028004024B900230091200110002C04380059010201024120647B0900203A",
                    INIT_0D => X"2010002001060097702F60975001A601701C01600160B2509702012C06064050",
                    INIT_0E => X"EF0AEF0D000120BA608E5964C709590021776F6C667265764FBA10D9D1202001",
                    INIT_0F => X"02012002010802050220000C0120012059000C000102000C00200120EA20F608",
                    INIT_10 => X"000000000000000000000000000000000000000000000C002001202001200108",
                    INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_01 => X"6C00080FF905FFFFB113129E7239080FF87FFD8BE9FED08A1066EB7F24F9FFFF",
                   INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000003",
                   INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hl(31 downto 0),
                  DOPA => data_out_a_hl(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hl(31 downto 0),
                  DOPB => data_out_b_hl(35 downto 32), 
                   DIB => data_in_b_hl(31 downto 0),
                  DIPB => data_in_b_hl(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_hh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_01 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_02 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_03 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_04 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_05 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_06 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_07 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_08 => X"8D89B4E050F4E15894E894E825090D0D08080808080808080808080808080808",
                    INIT_09 => X"082800040028000021259D8D01259D8D040414099D8D149D8D94E894E825149D",
                    INIT_0A => X"58011489F4005094E101D4E15889017180087894E88858C9F4008950F4E15878",
                    INIT_0B => X"08080808082800000D0D080808080808080808080808281471C88858C150C888",
                    INIT_0C => X"538008528008F4E2580AD4E08870080889092800680069000000000D0D080808",
                    INIT_0D => X"5870885848002814700050B4E38B148B7000CB508B51D4E3D4CB8A8BA3A30383",
                    INIT_0E => X"94E894E850C85814000000000D0D000808080808080808080814C404F4E87888",
                    INIT_0F => X"00680800680800D5C85828A00878C8580028A008680028A00878C858F4E894E8",
                    INIT_10 => X"00000000000000000000000000000000000000000028A00878C85878C8586808",
                    INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_01 => X"DB24CE4AA1F3FFFA47D2C28002243FA7FF3FFF802D2130C97FC4E25624A8FFFF",
                   INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000492",
                   INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hh(31 downto 0),
                  DOPA => data_out_a_hh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hh(31 downto 0),
                  DOPB => data_out_b_hh(35 downto 32), 
                   DIB => data_in_b_hh(31 downto 0),
                  DIPB => data_in_b_hh(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400BA81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"30012002013A02010201024130000023200000603387018701FFFF0131378E00",
                    INIT_05 => X"030804008100778E3F102B27BA042377005993599F03030130AEFF0102010241",
                    INIT_06 => X"E100D030C02CE1C02C017C01DA02230003F408070605040008070605C11003FF",
                    INIT_07 => X"53400253500253600253700201780201300201200253C0017802013002BDC001",
                    INIT_08 => X"6361725420322D6700C9002C1000011000011000011000307C022300E1010159",
                    INIT_09 => X"025360012E025370BD0164180100203A56206472616F426369676F4C2072656B",
                    INIT_0A => X"203A746E756F632064726F5700203A72656666754200595340012E025350012E",
                    INIT_0B => X"100283202000644B01002064726F5700202020203A73657A69732064726F5700",
                    INIT_0C => X"40002301020102412302645401590129020102010241200128020120027C0101",
                    INIT_0D => X"41103028012002C6400023646101599F010102010241103024014002012002B1",
                    INIT_0E => X"5003502C0606013001200201020102413064710159E940002359B70101020102",
                    INIT_0F => X"0301000059ED000359ED00010102FBFFF700A0B4070059DF01010201024160C9",
                    INIT_10 => X"FD07070201070702020002FE0707070702FD0202010008013E00000200F00707",
                    INIT_11 => X"FE0707020102020700020207070707020107070702FD0007070702FE07070702",
                    INIT_12 => X"02010702025D02FD5B06078000070702020702FE070702010702FD0007070702",
                    INIT_13 => X"010700020207800002000207070702FE07070201020702020755670E02FE0707",
                    INIT_14 => X"0006002AA75430A55420A35410A154000BFE013020100000077A0E02FE070702",
                    INIT_15 => X"002AC75420C55410C354000BFE01201000000006802A53A904A903A902A90100",
                    INIT_16 => X"77F054000117A35410EC54000BFE011000000006802A53C903C902C901000006",
                    INIT_17 => X"6F727265206B6341000006802A53F203F202F2010000302006002A3840774540",
                    INIT_18 => X"230003AF0003891C034001401603502A344030402C7C0323005964F802002E72",
                    INIT_19 => X"646142002E313400F300FD0200FD0100FD0000595330532003CF4030402C7C02",
                    INIT_1A => X"015B015B01FFFFFF00031F402C4B01230059643D030073746E656D7567726120",
                    INIT_1B => X"0040800000078900595340535053605370012002014902BDB23006060606005B",
                    INIT_1C => X"06060040800000078900595340535053605370012002014B02BD523006060606",
                    INIT_1D => X"060606060040800000078900595340535053605370012002014C02BDB0300606",
                    INIT_1E => X"5002000001000000000000000000595340535053605370012002015A02BD2030",
                    INIT_1F => X"0000000120C95006060606B0108F7006060606C0BD5006060606B05953C00201",
                    INIT_20 => X"060606B0C9503006060606B0FC58C9503006060606B00358BD5006060606B0C9",
                    INIT_21 => X"B052063FBCB5B062B080B09EB0595340535053605370012002015402BDB43006",
                    INIT_22 => X"00070000E0060606A01F1FDE060107520652BC57B052064BBC58B0520645BC56",
                    INIT_23 => X"06060606B0060606060759531010006C05720E01000E0E5FE0020F660E6510A0",
                    INIT_24 => X"DB03A008BD40DB029A04BD40DB019402BD40DB008E01BD40005959C9C07FBD50",
                    INIT_25 => X"0009100AB5C20001BC0209FFFF000920A50AAB0109000900A601FF00A5098000",
                    INIT_26 => X"E132E131000E005040E0B300DEB300DCB3AB100F060606060607A1000E010006",
                    INIT_27 => X"0606A1000E015953A90201023A01025359010201023A01025201025201024533",
                    INIT_28 => X"01023A01025359010252010252010245000E0010B3AB20AB30AB100F06060606",
                    INIT_29 => X"431039200B00000B2080004043012D020B00000B0208003501000E015953A902",
                    INIT_2A => X"008064010058200B0B20C052200B004E020B0B020C48020B5201000E01000E00",
                    INIT_2B => X"8E01007D770E087E0180007D6F0E087E0080007501006A650EA001806A5F0EA0",
                    INIT_2C => X"000E0E0E0E0E95200B0B0B0B4020008E200B000E87020B0B0B0B04020080020B",
                    INIT_2D => X"0B0B0B002006060606000E00B1200B00AB020B0B0B0B0002000E00A2020BB101",
                    INIT_2E => X"53305340E1C51030102C7C0223005953B300AB102C7C0123005953A900BE200B",
                    INIT_2F => X"5900646E756F6620656369766564206F4E00FD1030102C100110347C03230059",
                    INIT_30 => X"014F020146020059014502014E02014F02014E021A27102C7C0123005964EF05",
                    INIT_31 => X"30106B30106B30106B305C3330602730102C7C01230059014402014E02015502",
                    INIT_32 => X"005953405350536053705380539053A053B0106B30106B30106B30106B30106B",
                    INIT_33 => X"5953106B30102C7C0123005C40301030102C7C02230059015202015202014502",
                    INIT_34 => X"102C7C08102A7C0810297C0323005946305C44305CCC30FF2730102C7C012300",
                    INIT_35 => X"40015C3040015C4030305C3040015C3040015C3040015C4030345C5530FF2730",
                    INIT_36 => X"6B30106B305CBE305CCC30FF2730102C7C0123005946305C44305C3040015C30",
                    INIT_37 => X"3040015C4030345C5530FF2730102C7C08102A7C0810297C0323005953405310",
                    INIT_38 => X"30106B305CBE305C3040015C3040015C3040015C4030305C3040015C3040015C",
                    INIT_39 => X"006403007E20007E20005CF0200001FF27209800102C7C01230059534053106B",
                    INIT_3A => X"00403641018160A0206056300056305600006F00533056014A3056000E0E0046",
                    INIT_3B => X"0E7A0E017F00B0070130A00E0E0E013040002E4620B0A0AA5930206E6EAA5965",
                    INIT_3C => X"010001000100400000B010D08E0106079400FEB0070130A00E0E0E0130400100",
                    INIT_3D => X"6DB0000067726169746C756D0059AD010153C007074000000100010001000100",
                    INIT_3E => X"01007379733301006E6F6973726576BB00007465736572920000706D75646D65",
                    INIT_3F => X"0064725F6765725F63326908030072775F6765725F633269EB0100706C656877",
                    INIT_40 => X"32693403006361645F6C65735F6332692E03006364745F6C65735F6332691F03",
                    INIT_41 => X"5F6765725F71616405010072775F6765725F71616431030062645F6C65735F63",
                    INIT_42 => X"6474C8050065746972775F636474C405007375746174735F636474D100006472",
                    INIT_43 => X"64725F6765725F636474E2050072775F6765725F636474CF0500646165725F63",
                    INIT_44 => X"2707007364695F7465675F706D65740506006863726165735F706D6574D30500",
                    INIT_45 => X"5F706D65746B060065746972775F706D65742B060064695F7465675F706D6574",
                    INIT_46 => X"746E6972705F706D6574810600616E5F766E6F635F706D657476060064616572",
                    INIT_47 => X"63E60600746E6972705F706D6574930600766E6F635F706D6574CD0600616E5F",
                    INIT_48 => X"01012210202200203BFF2E00A000B407FFD10000726405010077648804007264",
                    INIT_49 => X"000060768E00608E10A0000110A000018E3F130000032200012A003BFFA01300",
                    INIT_4A => X"232057015E2F205E20105710200820100023236905012301412F012069102023",
                    INIT_4B => X"726F7272450059646A0900646E616D6D6F632064614200410001282300100124",
                    INIT_4C => X"104028004024B900230091200110002C04380059010201024120647B0900203A",
                    INIT_4D => X"2010002001060097702F60975001A601701C01600160B2509702012C06064050",
                    INIT_4E => X"EF0AEF0D000120BA608E5964C709590021776F6C667265764FBA10D9D1202001",
                    INIT_4F => X"02012002010802050220000C0120012059000C000102000C00200120EA20F608",
                    INIT_50 => X"000000000000000000000000000000000000000000000C002001202001200108",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"000000017040E8A1C3A10130010408A41FFFE536008278000000008F80000002",
                   INITP_01 => X"066BE9411F10173831C5C181D008400634FFFFFFFFFFF800000FFFFFFF0CB204",
                   INITP_02 => X"FF00020800010000000100000000143982A33B1CDB11B3619E618C3199B0F81C",
                   INITP_03 => X"A003F0001110000006A000001A8000006A000001DD040FFFEF2480A0259F400F",
                   INITP_04 => X"0C2D56DB50821000616B186682082000000298018010208209500000000C0300",
                   INITP_05 => X"7FFF9BA00B40080400288052020108049B3B334D24110480A01A0130B56D1010",
                   INITP_06 => X"59B6220096DB05AAAB2AAB36C4416DB01619C000000032CB2CB2C00000000501",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFF31155550BA106EA20B55DFF652EEDFB73B4012D555955",
                   INITP_08 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"6C00080FF905FFFFB113129E7239080FF87FFD8BE9FED08A1066EB7F24F9FFFF",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000003",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"506808006808006800690000000928787808280000F0C9F0C909095858010028",
                    INIT_05 => X"680868280028000404815859D0E8580028001000B0E818890090E96800690000",
                    INIT_06 => X"B0EE560E560E10560E0EB0E890E8582868086B6B6A6A68284B4B4A4A90684818",
                    INIT_07 => X"000000000000000000000000680800680800680800000068080068080000000E",
                    INIT_08 => X"080808080808080828005008035088035088025088025008B0E85828B0CE8E00",
                    INIT_09 => X"00000068080000000008000D0D08080808080808080808080808080808080808",
                    INIT_0A => X"0808080808080808080808080808080808080808082800000068080000006808",
                    INIT_0B => X"5000F0E05908000D0D0808080808080808080808080808080808080808080808",
                    INIT_0C => X"E1095A68006900005800000D0D00680800680069000000680800680800108868",
                    INIT_0D => X"00508008680800F0E1095A000D0D0010896800690000508008680800680800F0",
                    INIT_0E => X"E38B038AA2A28902680800680069000000000D0D00F0E1095A00108968006900",
                    INIT_0F => X"18B8B02800109D8D00109D8D680090E890E8250D0D280010CB680069000050D0",
                    INIT_10 => X"1E01016E2E01016E2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A0",
                    INIT_11 => X"1E01016E2E6E2E01286E2E010101016E2E0101016E1E280101016E1E0101016E",
                    INIT_12 => X"6E2E016E2E116E1ED1A2010A2801016E2E016E1E01016E2E016E1E280101016E",
                    INIT_13 => X"2E01A26B4B010A0A2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E0101",
                    INIT_14 => X"B8A00801D10102D10102D10102D101020118B8B1B1B0B02801F1A26E1E01016E",
                    INIT_15 => X"0801D10102D10102D101020118B8B1B0B028B8A0080100110811081108110828",
                    INIT_16 => X"01D101022801D10102D101020118B8B0B028B8A008010011081108110828B8A0",
                    INIT_17 => X"080808080808080828B8A008010011081108110828B8B1B1A008010101010101",
                    INIT_18 => X"5828D10128D101D1EA518A51D1EA520A0A500A500AB0E8582800000D0D080808",
                    INIT_19 => X"08080828010101B608280008280008280008280000000000D101500A500AB0E8",
                    INIT_1A => X"CEF1CFF1CF0F0F0E286818500AB1E8582800000D0D0808080808080808080808",
                    INIT_1B => X"28E20AC8DBDBDACA000000000000000000680800680800008818A0A0A0A028F1",
                    INIT_1C => X"A0A028E20AC8DBDBDACA000000000000000000680800680800008818A0A0A0A0",
                    INIT_1D => X"A0A0A0A028E20AC8DBDBDACA000000000000000000680800680800008818A0A0",
                    INIT_1E => X"08000D050E2888CB88CB88CA88CA000000000000000000680800680800008018",
                    INIT_1F => X"0B0B0A0A080088A0A0A0A000221A18A0A0A0A0000088A0A0A0A0000000000068",
                    INIT_20 => X"A0A0A000008818A0A0A0A0001A01008818A0A0A0A0002A010088A0A0A0A00000",
                    INIT_21 => X"0012A592010900010001000100000000000000000000680800680800008818A0",
                    INIT_22 => X"0008092518A0A0A0001DEDB1EE8EA512A59201090012A59201090012A5920109",
                    INIT_23 => X"A0A0A0A000A6A6A6A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E010",
                    INIT_24 => X"0108926A00080108926A00080108926A00080108926A000828000000121A0088",
                    INIT_25 => X"096808481292DFCFB218480F0F2868080268921848284828F2CF0F28026F0F28",
                    INIT_26 => X"1209120928A0080201D20202D20202D202022018A0A0A0A0A0A00228A10928A1",
                    INIT_27 => X"A0A00228A1090000020068000868000800690068000868000868000868000809",
                    INIT_28 => X"6800086800080068000868000868000828A008D20202000200022018A0A0A0A0",
                    INIT_29 => X"9268926848080868282808129268926848080868282808D26828A10900000200",
                    INIT_2A => X"0809D2682892684868280892684828926848682808926848D26828A00828A008",
                    INIT_2B => X"D2682812F2A1A00208090812F2A1A002080908D2682812F2A102080912F2A102",
                    INIT_2C => X"28A0A0A0A0A092684848486828280892684828A0926848484868282808926848",
                    INIT_2D => X"4848682828A0A0A0A0A0A008926848289268484848682828A0A008926848D268",
                    INIT_2E => X"00000000D20250085008B0E8582800000228025008B0E8582800000228926848",
                    INIT_2F => X"000808080808080808080808080808080828025008500851885108B0E8582800",
                    INIT_30 => X"6808006808002800680800680800680800680800D3025008B0E8582800000D0D",
                    INIT_31 => X"00030200020200020200020800F302005108B0E8582800680800680800680800",
                    INIT_32 => X"2800000000000000000000000000000000000502000502000402000402000302",
                    INIT_33 => X"00000002005108B0E8582802000052085108B0E8582800680800680800680800",
                    INIT_34 => X"5108B0E85008B0E85008B0E85828000200020800020800F202005108B0E85828",
                    INIT_35 => X"508A0200508A0250000A0200508A0200508A0200508A0250000A020800F20200",
                    INIT_36 => X"0200020200020800020800F202005108B0E858280002000208000200508A0200",
                    INIT_37 => X"00508A0250000A020800F202005108B0E85008B0E85008B0E858280000000000",
                    INIT_38 => X"000202000208000200508A0200508A0200508A0250000A0200508A0200508A02",
                    INIT_39 => X"E893E8A00200A00200080208000A09F20200030B5108B0E85828000000000002",
                    INIT_3A => X"EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3A0A00B93",
                    INIT_3B => X"A513A5CD93ED551DCD0585A5A5A5CD050D2813020001010300050513130300B3",
                    INIT_3C => X"88708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D1828",
                    INIT_3D => X"0808080808080808080808082800F3CECE00500E8E0E28708870887088708870",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_40 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_41 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_42 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_43 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_44 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_45 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_46 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_47 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_48 => X"8D89B4E050F4E15894E894E825090D0D08080808080808080808080808080808",
                    INIT_49 => X"082800040028000021259D8D01259D8D040414099D8D149D8D94E894E825149D",
                    INIT_4A => X"58011489F4005094E101D4E15889017180087894E88858C9F4008950F4E15878",
                    INIT_4B => X"08080808082800000D0D080808080808080808080808281471C88858C150C888",
                    INIT_4C => X"538008528008F4E2580AD4E08870080889092800680069000000000D0D080808",
                    INIT_4D => X"5870885848002814700050B4E38B148B7000CB508B51D4E3D4CB8A8BA3A30383",
                    INIT_4E => X"94E894E850C85814000000000D0D000808080808080808080814C404F4E87888",
                    INIT_4F => X"00680800680800D5C85828A00878C8580028A008680028A00878C858F4E894E8",
                    INIT_50 => X"00000000000000000000000000000000000000000028A00878C85878C8586808",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"B6DB6D6C8229BF08BF89F85F5BF3BD1FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"5CCEA77D00BF4C6F8B137C5B1F66FD6D627FFFFFFFFFFF5AD6A7FFFFFFC00099",
                   INITP_02 => X"FFCEAC36E76A675676D467559DB6A1D7445EE6EBB6AF6EDF75DF7BEF776FA670",
                   INITP_03 => X"0400081B46AB556C090D55B0243556C090D55B0350C4E7FFFE6DBAC27F0804E7",
                   INITP_04 => X"33F6FB6CA86DC0324C86E59DAAAAAAF200314114001165965956AAD808060181",
                   INITP_05 => X"FFFFE013AC27E4FC2009841282112844B919174D2C9324A4A11A114FDBB69D40",
                   INITP_06 => X"1131113A94989D222222222622274989D130276DEAAA924924A6276DB7B6DC9C",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFE435554C880014800719F25252AA14921A27529111111",
                   INITP_08 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"DB24CE4AA1F3FFFA47D2C28002243FA7FF3FFF802D2130C97FC4E25624A8FFFF",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000492",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400BA81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"30012002013A02010201024130000023200000603387018701FFFF0131378E00",
                    INIT_05 => X"030804008100778E3F102B27BA042377005993599F03030130AEFF0102010241",
                    INIT_06 => X"E100D030C02CE1C02C017C01DA02230003F408070605040008070605C11003FF",
                    INIT_07 => X"53400253500253600253700201780201300201200253C0017802013002BDC001",
                    INIT_08 => X"6361725420322D6700C9002C1000011000011000011000307C022300E1010159",
                    INIT_09 => X"025360012E025370BD0164180100203A56206472616F426369676F4C2072656B",
                    INIT_0A => X"203A746E756F632064726F5700203A72656666754200595340012E025350012E",
                    INIT_0B => X"100283202000644B01002064726F5700202020203A73657A69732064726F5700",
                    INIT_0C => X"40002301020102412302645401590129020102010241200128020120027C0101",
                    INIT_0D => X"41103028012002C6400023646101599F010102010241103024014002012002B1",
                    INIT_0E => X"5003502C0606013001200201020102413064710159E940002359B70101020102",
                    INIT_0F => X"0301000059ED000359ED00010102FBFFF700A0B4070059DF01010201024160C9",
                    INIT_10 => X"FD07070201070702020002FE0707070702FD0202010008013E00000200F00707",
                    INIT_11 => X"FE0707020102020700020207070707020107070702FD0007070702FE07070702",
                    INIT_12 => X"02010702025D02FD5B06078000070702020702FE070702010702FD0007070702",
                    INIT_13 => X"010700020207800002000207070702FE07070201020702020755670E02FE0707",
                    INIT_14 => X"0006002AA75430A55420A35410A154000BFE013020100000077A0E02FE070702",
                    INIT_15 => X"002AC75420C55410C354000BFE01201000000006802A53A904A903A902A90100",
                    INIT_16 => X"77F054000117A35410EC54000BFE011000000006802A53C903C902C901000006",
                    INIT_17 => X"6F727265206B6341000006802A53F203F202F2010000302006002A3840774540",
                    INIT_18 => X"230003AF0003891C034001401603502A344030402C7C0323005964F802002E72",
                    INIT_19 => X"646142002E313400F300FD0200FD0100FD0000595330532003CF4030402C7C02",
                    INIT_1A => X"015B015B01FFFFFF00031F402C4B01230059643D030073746E656D7567726120",
                    INIT_1B => X"0040800000078900595340535053605370012002014902BDB23006060606005B",
                    INIT_1C => X"06060040800000078900595340535053605370012002014B02BD523006060606",
                    INIT_1D => X"060606060040800000078900595340535053605370012002014C02BDB0300606",
                    INIT_1E => X"5002000001000000000000000000595340535053605370012002015A02BD2030",
                    INIT_1F => X"0000000120C95006060606B0108F7006060606C0BD5006060606B05953C00201",
                    INIT_20 => X"060606B0C9503006060606B0FC58C9503006060606B00358BD5006060606B0C9",
                    INIT_21 => X"B052063FBCB5B062B080B09EB0595340535053605370012002015402BDB43006",
                    INIT_22 => X"00070000E0060606A01F1FDE060107520652BC57B052064BBC58B0520645BC56",
                    INIT_23 => X"06060606B0060606060759531010006C05720E01000E0E5FE0020F660E6510A0",
                    INIT_24 => X"DB03A008BD40DB029A04BD40DB019402BD40DB008E01BD40005959C9C07FBD50",
                    INIT_25 => X"0009100AB5C20001BC0209FFFF000920A50AAB0109000900A601FF00A5098000",
                    INIT_26 => X"E132E131000E005040E0B300DEB300DCB3AB100F060606060607A1000E010006",
                    INIT_27 => X"0606A1000E015953A90201023A01025359010201023A01025201025201024533",
                    INIT_28 => X"01023A01025359010252010252010245000E0010B3AB20AB30AB100F06060606",
                    INIT_29 => X"431039200B00000B2080004043012D020B00000B0208003501000E015953A902",
                    INIT_2A => X"008064010058200B0B20C052200B004E020B0B020C48020B5201000E01000E00",
                    INIT_2B => X"8E01007D770E087E0180007D6F0E087E0080007501006A650EA001806A5F0EA0",
                    INIT_2C => X"000E0E0E0E0E95200B0B0B0B4020008E200B000E87020B0B0B0B04020080020B",
                    INIT_2D => X"0B0B0B002006060606000E00B1200B00AB020B0B0B0B0002000E00A2020BB101",
                    INIT_2E => X"53305340E1C51030102C7C0223005953B300AB102C7C0123005953A900BE200B",
                    INIT_2F => X"5900646E756F6620656369766564206F4E00FD1030102C100110347C03230059",
                    INIT_30 => X"014F020146020059014502014E02014F02014E021A27102C7C0123005964EF05",
                    INIT_31 => X"30106B30106B30106B305C3330602730102C7C01230059014402014E02015502",
                    INIT_32 => X"005953405350536053705380539053A053B0106B30106B30106B30106B30106B",
                    INIT_33 => X"5953106B30102C7C0123005C40301030102C7C02230059015202015202014502",
                    INIT_34 => X"102C7C08102A7C0810297C0323005946305C44305CCC30FF2730102C7C012300",
                    INIT_35 => X"40015C3040015C4030305C3040015C3040015C3040015C4030345C5530FF2730",
                    INIT_36 => X"6B30106B305CBE305CCC30FF2730102C7C0123005946305C44305C3040015C30",
                    INIT_37 => X"3040015C4030345C5530FF2730102C7C08102A7C0810297C0323005953405310",
                    INIT_38 => X"30106B305CBE305C3040015C3040015C3040015C4030305C3040015C3040015C",
                    INIT_39 => X"006403007E20007E20005CF0200001FF27209800102C7C01230059534053106B",
                    INIT_3A => X"00403641018160A0206056300056305600006F00533056014A3056000E0E0046",
                    INIT_3B => X"0E7A0E017F00B0070130A00E0E0E013040002E4620B0A0AA5930206E6EAA5965",
                    INIT_3C => X"010001000100400000B010D08E0106079400FEB0070130A00E0E0E0130400100",
                    INIT_3D => X"6DB0000067726169746C756D0059AD010153C007074000000100010001000100",
                    INIT_3E => X"01007379733301006E6F6973726576BB00007465736572920000706D75646D65",
                    INIT_3F => X"0064725F6765725F63326908030072775F6765725F633269EB0100706C656877",
                    INIT_40 => X"32693403006361645F6C65735F6332692E03006364745F6C65735F6332691F03",
                    INIT_41 => X"5F6765725F71616405010072775F6765725F71616431030062645F6C65735F63",
                    INIT_42 => X"6474C8050065746972775F636474C405007375746174735F636474D100006472",
                    INIT_43 => X"64725F6765725F636474E2050072775F6765725F636474CF0500646165725F63",
                    INIT_44 => X"2707007364695F7465675F706D65740506006863726165735F706D6574D30500",
                    INIT_45 => X"5F706D65746B060065746972775F706D65742B060064695F7465675F706D6574",
                    INIT_46 => X"746E6972705F706D6574810600616E5F766E6F635F706D657476060064616572",
                    INIT_47 => X"63E60600746E6972705F706D6574930600766E6F635F706D6574CD0600616E5F",
                    INIT_48 => X"01012210202200203BFF2E00A000B407FFD10000726405010077648804007264",
                    INIT_49 => X"000060768E00608E10A0000110A000018E3F130000032200012A003BFFA01300",
                    INIT_4A => X"232057015E2F205E20105710200820100023236905012301412F012069102023",
                    INIT_4B => X"726F7272450059646A0900646E616D6D6F632064614200410001282300100124",
                    INIT_4C => X"104028004024B900230091200110002C04380059010201024120647B0900203A",
                    INIT_4D => X"2010002001060097702F60975001A601701C01600160B2509702012C06064050",
                    INIT_4E => X"EF0AEF0D000120BA608E5964C709590021776F6C667265764FBA10D9D1202001",
                    INIT_4F => X"02012002010802050220000C0120012059000C000102000C00200120EA20F608",
                    INIT_50 => X"000000000000000000000000000000000000000000000C002001202001200108",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"000000017040E8A1C3A10130010408A41FFFE536008278000000008F80000002",
                   INITP_01 => X"066BE9411F10173831C5C181D008400634FFFFFFFFFFF800000FFFFFFF0CB204",
                   INITP_02 => X"FF00020800010000000100000000143982A33B1CDB11B3619E618C3199B0F81C",
                   INITP_03 => X"A003F0001110000006A000001A8000006A000001DD040FFFEF2480A0259F400F",
                   INITP_04 => X"0C2D56DB50821000616B186682082000000298018010208209500000000C0300",
                   INITP_05 => X"7FFF9BA00B40080400288052020108049B3B334D24110480A01A0130B56D1010",
                   INITP_06 => X"59B6220096DB05AAAB2AAB36C4416DB01619C000000032CB2CB2C00000000501",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFF31155550BA106EA20B55DFF652EEDFB73B4012D555955",
                   INITP_08 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"6C00080FF905FFFFB113129E7239080FF87FFD8BE9FED08A1066EB7F24F9FFFF",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000003",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"506808006808006800690000000928787808280000F0C9F0C909095858010028",
                    INIT_05 => X"680868280028000404815859D0E8580028001000B0E818890090E96800690000",
                    INIT_06 => X"B0EE560E560E10560E0EB0E890E8582868086B6B6A6A68284B4B4A4A90684818",
                    INIT_07 => X"000000000000000000000000680800680800680800000068080068080000000E",
                    INIT_08 => X"080808080808080828005008035088035088025088025008B0E85828B0CE8E00",
                    INIT_09 => X"00000068080000000008000D0D08080808080808080808080808080808080808",
                    INIT_0A => X"0808080808080808080808080808080808080808082800000068080000006808",
                    INIT_0B => X"5000F0E05908000D0D0808080808080808080808080808080808080808080808",
                    INIT_0C => X"E1095A68006900005800000D0D00680800680069000000680800680800108868",
                    INIT_0D => X"00508008680800F0E1095A000D0D0010896800690000508008680800680800F0",
                    INIT_0E => X"E38B038AA2A28902680800680069000000000D0D00F0E1095A00108968006900",
                    INIT_0F => X"18B8B02800109D8D00109D8D680090E890E8250D0D280010CB680069000050D0",
                    INIT_10 => X"1E01016E2E01016E2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A0",
                    INIT_11 => X"1E01016E2E6E2E01286E2E010101016E2E0101016E1E280101016E1E0101016E",
                    INIT_12 => X"6E2E016E2E116E1ED1A2010A2801016E2E016E1E01016E2E016E1E280101016E",
                    INIT_13 => X"2E01A26B4B010A0A2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E0101",
                    INIT_14 => X"B8A00801D10102D10102D10102D101020118B8B1B1B0B02801F1A26E1E01016E",
                    INIT_15 => X"0801D10102D10102D101020118B8B1B0B028B8A0080100110811081108110828",
                    INIT_16 => X"01D101022801D10102D101020118B8B0B028B8A008010011081108110828B8A0",
                    INIT_17 => X"080808080808080828B8A008010011081108110828B8B1B1A008010101010101",
                    INIT_18 => X"5828D10128D101D1EA518A51D1EA520A0A500A500AB0E8582800000D0D080808",
                    INIT_19 => X"08080828010101B608280008280008280008280000000000D101500A500AB0E8",
                    INIT_1A => X"CEF1CFF1CF0F0F0E286818500AB1E8582800000D0D0808080808080808080808",
                    INIT_1B => X"28E20AC8DBDBDACA000000000000000000680800680800008818A0A0A0A028F1",
                    INIT_1C => X"A0A028E20AC8DBDBDACA000000000000000000680800680800008818A0A0A0A0",
                    INIT_1D => X"A0A0A0A028E20AC8DBDBDACA000000000000000000680800680800008818A0A0",
                    INIT_1E => X"08000D050E2888CB88CB88CA88CA000000000000000000680800680800008018",
                    INIT_1F => X"0B0B0A0A080088A0A0A0A000221A18A0A0A0A0000088A0A0A0A0000000000068",
                    INIT_20 => X"A0A0A000008818A0A0A0A0001A01008818A0A0A0A0002A010088A0A0A0A00000",
                    INIT_21 => X"0012A592010900010001000100000000000000000000680800680800008818A0",
                    INIT_22 => X"0008092518A0A0A0001DEDB1EE8EA512A59201090012A59201090012A5920109",
                    INIT_23 => X"A0A0A0A000A6A6A6A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E010",
                    INIT_24 => X"0108926A00080108926A00080108926A00080108926A000828000000121A0088",
                    INIT_25 => X"096808481292DFCFB218480F0F2868080268921848284828F2CF0F28026F0F28",
                    INIT_26 => X"1209120928A0080201D20202D20202D202022018A0A0A0A0A0A00228A10928A1",
                    INIT_27 => X"A0A00228A1090000020068000868000800690068000868000868000868000809",
                    INIT_28 => X"6800086800080068000868000868000828A008D20202000200022018A0A0A0A0",
                    INIT_29 => X"9268926848080868282808129268926848080868282808D26828A10900000200",
                    INIT_2A => X"0809D2682892684868280892684828926848682808926848D26828A00828A008",
                    INIT_2B => X"D2682812F2A1A00208090812F2A1A002080908D2682812F2A102080912F2A102",
                    INIT_2C => X"28A0A0A0A0A092684848486828280892684828A0926848484868282808926848",
                    INIT_2D => X"4848682828A0A0A0A0A0A008926848289268484848682828A0A008926848D268",
                    INIT_2E => X"00000000D20250085008B0E8582800000228025008B0E8582800000228926848",
                    INIT_2F => X"000808080808080808080808080808080828025008500851885108B0E8582800",
                    INIT_30 => X"6808006808002800680800680800680800680800D3025008B0E8582800000D0D",
                    INIT_31 => X"00030200020200020200020800F302005108B0E8582800680800680800680800",
                    INIT_32 => X"2800000000000000000000000000000000000502000502000402000402000302",
                    INIT_33 => X"00000002005108B0E8582802000052085108B0E8582800680800680800680800",
                    INIT_34 => X"5108B0E85008B0E85008B0E85828000200020800020800F202005108B0E85828",
                    INIT_35 => X"508A0200508A0250000A0200508A0200508A0200508A0250000A020800F20200",
                    INIT_36 => X"0200020200020800020800F202005108B0E858280002000208000200508A0200",
                    INIT_37 => X"00508A0250000A020800F202005108B0E85008B0E85008B0E858280000000000",
                    INIT_38 => X"000202000208000200508A0200508A0200508A0250000A0200508A0200508A02",
                    INIT_39 => X"E893E8A00200A00200080208000A09F20200030B5108B0E85828000000000002",
                    INIT_3A => X"EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3A0A00B93",
                    INIT_3B => X"A513A5CD93ED551DCD0585A5A5A5CD050D2813020001010300050513130300B3",
                    INIT_3C => X"88708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD050D1828",
                    INIT_3D => X"0808080808080808080808082800F3CECE00500E8E0E28708870887088708870",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_40 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_41 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_42 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_43 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_44 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_45 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_46 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_47 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_48 => X"8D89B4E050F4E15894E894E825090D0D08080808080808080808080808080808",
                    INIT_49 => X"082800040028000021259D8D01259D8D040414099D8D149D8D94E894E825149D",
                    INIT_4A => X"58011489F4005094E101D4E15889017180087894E88858C9F4008950F4E15878",
                    INIT_4B => X"08080808082800000D0D080808080808080808080808281471C88858C150C888",
                    INIT_4C => X"538008528008F4E2580AD4E08870080889092800680069000000000D0D080808",
                    INIT_4D => X"5870885848002814700050B4E38B148B7000CB508B51D4E3D4CB8A8BA3A30383",
                    INIT_4E => X"94E894E850C85814000000000D0D000808080808080808080814C404F4E87888",
                    INIT_4F => X"00680800680800D5C85828A00878C8580028A008680028A00878C858F4E894E8",
                    INIT_50 => X"00000000000000000000000000000000000000000028A00878C85878C8586808",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"B6DB6D6C8229BF08BF89F85F5BF3BD1FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"5CCEA77D00BF4C6F8B137C5B1F66FD6D627FFFFFFFFFFF5AD6A7FFFFFFC00099",
                   INITP_02 => X"FFCEAC36E76A675676D467559DB6A1D7445EE6EBB6AF6EDF75DF7BEF776FA670",
                   INITP_03 => X"0400081B46AB556C090D55B0243556C090D55B0350C4E7FFFE6DBAC27F0804E7",
                   INITP_04 => X"33F6FB6CA86DC0324C86E59DAAAAAAF200314114001165965956AAD808060181",
                   INITP_05 => X"FFFFE013AC27E4FC2009841282112844B919174D2C9324A4A11A114FDBB69D40",
                   INITP_06 => X"1131113A94989D222222222622274989D130276DEAAA924924A6276DB7B6DC9C",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFE435554C880014800719F25252AA14921A27529111111",
                   INITP_08 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"DB24CE4AA1F3FFFA47D2C28002243FA7FF3FFF802D2130C97FC4E25624A8FFFF",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000492",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_4k_generate;	              
  --
  --
  --
  --
  -- JTAG Loader
  --
  instantiate_loader : if (C_JTAG_LOADER_ENABLE = 1) generate
  --
    jtag_loader_6_inst : jtag_loader_6
    generic map(              C_FAMILY => C_FAMILY,
                       C_NUM_PICOBLAZE => 1,
                  C_JTAG_LOADER_ENABLE => C_JTAG_LOADER_ENABLE,
                 C_BRAM_MAX_ADDR_WIDTH => BRAM_ADDRESS_WIDTH,
	                  C_ADDR_WIDTH_0 => BRAM_ADDRESS_WIDTH)
    port map( picoblaze_reset => rdl_bus,
                      jtag_en => jtag_en,
                     jtag_din => jtag_din,
                    jtag_addr => jtag_addr(BRAM_ADDRESS_WIDTH-1 downto 0),
                     jtag_clk => jtag_clk,
                      jtag_we => jtag_we,
                  jtag_dout_0 => jtag_dout,
                  jtag_dout_1 => jtag_dout, -- ports 1-7 are not used
                  jtag_dout_2 => jtag_dout, -- in a 1 device debug 
                  jtag_dout_3 => jtag_dout, -- session.  However, Synplify
                  jtag_dout_4 => jtag_dout, -- etc require all ports to
                  jtag_dout_5 => jtag_dout, -- be connected
                  jtag_dout_6 => jtag_dout,
                  jtag_dout_7 => jtag_dout);
    --  
  end generate instantiate_loader;
  --
end low_level_definition;
--
--
-------------------------------------------------------------------------------------------
--
-- JTAG Loader 
--
-------------------------------------------------------------------------------------------
--
--
-- JTAG Loader 6 - Version 6.00
-- Kris Chaplin 4 February 2010
-- Ken Chapman 15 August 2011 - Revised coding style
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--
library unisim;
use unisim.vcomponents.all;
--
entity jtag_loader_6 is
generic(              C_JTAG_LOADER_ENABLE : integer := 1;
                                  C_FAMILY : string := "V6";
                           C_NUM_PICOBLAZE : integer := 1;
                     C_BRAM_MAX_ADDR_WIDTH : integer := 10;
        C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                              C_JTAG_CHAIN : integer := 2;
                            C_ADDR_WIDTH_0 : integer := 10;
                            C_ADDR_WIDTH_1 : integer := 10;
                            C_ADDR_WIDTH_2 : integer := 10;
                            C_ADDR_WIDTH_3 : integer := 10;
                            C_ADDR_WIDTH_4 : integer := 10;
                            C_ADDR_WIDTH_5 : integer := 10;
                            C_ADDR_WIDTH_6 : integer := 10;
                            C_ADDR_WIDTH_7 : integer := 10);
port(   picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
               jtag_din : out std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
              jtag_addr : out std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0) := (others => '0');
               jtag_clk : out std_logic := '0';
                jtag_we : out std_logic := '0';
            jtag_dout_0 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_1 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_2 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_3 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_4 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_5 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_6 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_7 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end jtag_loader_6;
--
architecture Behavioral of jtag_loader_6 is
  --
  signal num_picoblaze       : std_logic_vector(2 downto 0);
  signal picoblaze_instruction_data_width : std_logic_vector(4 downto 0);
  --
  signal drck                : std_logic;
  signal shift_clk           : std_logic;
  signal shift_din           : std_logic;
  signal shift_dout          : std_logic;
  signal shift               : std_logic;
  signal capture             : std_logic;
  --
  signal control_reg_ce      : std_logic;
  signal bram_ce             : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal bus_zero            : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  signal jtag_en_int         : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal jtag_en_expanded    : std_logic_vector(7 downto 0) := (others => '0');
  signal jtag_addr_int       : std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
  signal jtag_din_int        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal control_din         : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout_int    : std_logic_vector(7 downto 0):= (others => '0');
  signal bram_dout_int       : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
  signal jtag_we_int         : std_logic;
  signal jtag_clk_int        : std_logic;
  signal bram_ce_valid       : std_logic;
  signal din_load            : std_logic;
  --
  signal jtag_dout_0_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_1_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_2_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_3_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_4_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_5_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_6_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_7_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal picoblaze_reset_int : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  --        
begin
  bus_zero <= (others => '0');
  --
  jtag_loader_gen: if (C_JTAG_LOADER_ENABLE = 1) generate
    --
    -- Insert BSCAN primitive for target device architecture.
    --
    BSCAN_SPARTAN6_gen: if (C_FAMILY="S6") generate
    begin
      BSCAN_BLOCK_inst : BSCAN_SPARTAN6
      generic map ( JTAG_CHAIN => C_JTAG_CHAIN)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_SPARTAN6_gen;   
    --
    BSCAN_VIRTEX6_gen: if (C_FAMILY="V6") generate
    begin
      BSCAN_BLOCK_inst: BSCAN_VIRTEX6
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => FALSE)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_VIRTEX6_gen;   
    --
    BSCAN_7SERIES_gen: if (C_FAMILY="7S") generate
    begin
      BSCAN_BLOCK_inst: BSCANE2
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => "FALSE")
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_7SERIES_gen;   
    --
    --
    -- Insert clock buffer to ensure reliable shift operations.
    --
    upload_clock: BUFG
    port map( I => drck,
              O => shift_clk);
    --        
    --        
    --  Shift Register      
    --        
    --
    control_reg_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk = '1' then
        if (shift = '1') then
          control_reg_ce <= shift_din;
        end if;
      end if;
    end process control_reg_ce_shift;
    --        
    bram_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          if(C_NUM_PICOBLAZE > 1) then
            for i in 0 to C_NUM_PICOBLAZE-2 loop
              bram_ce(i+1) <= bram_ce(i);
            end loop;
          end if;
          bram_ce(0) <= control_reg_ce;
        end if;
      end if;
    end process bram_ce_shift;
    --        
    bram_we_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          jtag_we_int <= bram_ce(C_NUM_PICOBLAZE-1);
        end if;
      end if;
    end process bram_we_shift;
    --        
    bram_a_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          for i in 0 to C_BRAM_MAX_ADDR_WIDTH-2 loop
            jtag_addr_int(i+1) <= jtag_addr_int(i);
          end loop;
          jtag_addr_int(0) <= jtag_we_int;
        end if;
      end if;
    end process bram_a_shift;
    --        
    bram_d_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (din_load = '1') then
          jtag_din_int <= bram_dout_int;
         elsif (shift = '1') then
          for i in 0 to C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-2 loop
            jtag_din_int(i+1) <= jtag_din_int(i);
          end loop;
          jtag_din_int(0) <= jtag_addr_int(C_BRAM_MAX_ADDR_WIDTH-1);
        end if;
      end if;
    end process bram_d_shift;
    --
    shift_dout <= jtag_din_int(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1);
    --
    --
    din_load_select:process (bram_ce, din_load, capture, bus_zero, control_reg_ce) 
    begin
      if ( bram_ce = bus_zero ) then
        din_load <= capture and control_reg_ce;
       else
        din_load <= capture;
      end if;
    end process din_load_select;
    --
    --
    -- Control Registers 
    --
    num_picoblaze <= conv_std_logic_vector(C_NUM_PICOBLAZE-1,3);
    picoblaze_instruction_data_width <= conv_std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1,5);
    --	
    control_registers: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '0') and (control_reg_ce = '1') then
          case (jtag_addr_int(3 downto 0)) is 
            when "0000" => -- 0 = version - returns (7 downto 4) illustrating number of PB
                           --               and (3 downto 0) picoblaze instruction data width
                           control_dout_int <= num_picoblaze & picoblaze_instruction_data_width;
            when "0001" => -- 1 = PicoBlaze 0 reset / status
                           if (C_NUM_PICOBLAZE >= 1) then 
                            control_dout_int <= picoblaze_reset_int(0) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_0-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0010" => -- 2 = PicoBlaze 1 reset / status
                           if (C_NUM_PICOBLAZE >= 2) then 
                             control_dout_int <= picoblaze_reset_int(1) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_1-1,5) );
                            else 
                             control_dout_int <= (others => '0');
                           end if;
            when "0011" => -- 3 = PicoBlaze 2 reset / status
                           if (C_NUM_PICOBLAZE >= 3) then 
                            control_dout_int <= picoblaze_reset_int(2) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_2-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0100" => -- 4 = PicoBlaze 3 reset / status
                           if (C_NUM_PICOBLAZE >= 4) then 
                            control_dout_int <= picoblaze_reset_int(3) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_3-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0101" => -- 5 = PicoBlaze 4 reset / status
                           if (C_NUM_PICOBLAZE >= 5) then 
                            control_dout_int <= picoblaze_reset_int(4) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_4-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0110" => -- 6 = PicoBlaze 5 reset / status
                           if (C_NUM_PICOBLAZE >= 6) then 
                            control_dout_int <= picoblaze_reset_int(5) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_5-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0111" => -- 7 = PicoBlaze 6 reset / status
                           if (C_NUM_PICOBLAZE >= 7) then 
                            control_dout_int <= picoblaze_reset_int(6) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_6-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1000" => -- 8 = PicoBlaze 7 reset / status
                           if (C_NUM_PICOBLAZE >= 8) then 
                            control_dout_int <= picoblaze_reset_int(7) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_7-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1111" => control_dout_int <= conv_std_logic_vector(C_BRAM_MAX_ADDR_WIDTH -1,8);
            when others => control_dout_int <= (others => '1');
          end case;
        else 
          control_dout_int <= (others => '0');
        end if;
      end if;
    end process control_registers;
    -- 
    control_dout(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-8) <= control_dout_int;
    --
    pb_reset: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '1') and (control_reg_ce = '1') then
          picoblaze_reset_int(C_NUM_PICOBLAZE-1 downto 0) <= control_din(C_NUM_PICOBLAZE-1 downto 0);
        end if;
      end if;
    end process pb_reset;    
    --
    --
    -- Assignments 
    --
    control_dout (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-9 downto 0) <= (others => '0') when (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH > 8);
    --
    -- Qualify the blockram CS signal with bscan select output
    jtag_en_int <= bram_ce when bram_ce_valid = '1' else (others => '0');
    --      
    jtag_en_expanded(C_NUM_PICOBLAZE-1 downto 0) <= jtag_en_int;
    jtag_en_expanded(7 downto C_NUM_PICOBLAZE) <= (others => '0') when (C_NUM_PICOBLAZE < 8);
    --        
    bram_dout_int <= control_dout or jtag_dout_0_masked or jtag_dout_1_masked or jtag_dout_2_masked or jtag_dout_3_masked or jtag_dout_4_masked or jtag_dout_5_masked or jtag_dout_6_masked or jtag_dout_7_masked;
    --
    control_din <= jtag_din_int;
    --        
    jtag_dout_0_masked <= jtag_dout_0 when jtag_en_expanded(0) = '1' else (others => '0');
    jtag_dout_1_masked <= jtag_dout_1 when jtag_en_expanded(1) = '1' else (others => '0');
    jtag_dout_2_masked <= jtag_dout_2 when jtag_en_expanded(2) = '1' else (others => '0');
    jtag_dout_3_masked <= jtag_dout_3 when jtag_en_expanded(3) = '1' else (others => '0');
    jtag_dout_4_masked <= jtag_dout_4 when jtag_en_expanded(4) = '1' else (others => '0');
    jtag_dout_5_masked <= jtag_dout_5 when jtag_en_expanded(5) = '1' else (others => '0');
    jtag_dout_6_masked <= jtag_dout_6 when jtag_en_expanded(6) = '1' else (others => '0');
    jtag_dout_7_masked <= jtag_dout_7 when jtag_en_expanded(7) = '1' else (others => '0');
    --
    jtag_en <= jtag_en_int;
    jtag_din <= jtag_din_int;
    jtag_addr <= jtag_addr_int;
    jtag_clk <= jtag_clk_int;
    jtag_we <= jtag_we_int;
    picoblaze_reset <= picoblaze_reset_int;
    --        
  end generate jtag_loader_gen;
--
end Behavioral;
--
--
------------------------------------------------------------------------------------
--
-- END OF FILE cli.vhd
--
------------------------------------------------------------------------------------
