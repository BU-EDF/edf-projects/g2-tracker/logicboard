-------------------------------------------------------------------------------
-- g-2 tracker logic board TDC CDR
-- Dan Gastler
-- Partially addressed DEQUE
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

use work.Addressed_FIFO_package.all;

entity Addressed_FIFO is
  port (
    clk   : in std_logic;
    reset : in std_logic;

    fifo_full : out std_logic;
    fifo_in   : in  std_logic_vector(33 downto 0);
    fifo_wr   : in  std_logic;

    fifo_empty     : out std_logic;
    fifo_out       : out std_logic_vector(33 downto 0);
    fifo_out_valid : out std_logic;
    fifo_rd        : in  std_logic;

    data       : out data_array;
    data_valid : out std_logic;

    buffer_overflow : out std_logic
    );

end entity Addressed_FIFO;
architecture arch of Addressed_FIFO is
  component ADDR_FIFO is
    port (
      clk   : in  std_logic;
      rst   : in  std_logic;
      din   : in  std_logic_vector(33 downto 0);
      wr_en : in  std_logic;
      rd_en : in  std_logic;
      dout  : out std_logic_vector(33 downto 0);
      full  : out std_logic;
      empty : out std_logic;
      valid : out std_logic);
  end component ADDR_FIFO;

  -- values for the local address data array
  constant BAD_DATA            : std_logic_vector(33 downto 0) := "11"&x"DEADBEEF";
  signal local_data            : data_array                    := (others => BAD_DATA);
  constant EMPTY_LOCAL_DATA    : integer range -1 to 15        := -1;
  constant MAX_LOCAL_DATA      : integer range -1 to 15        := 15;
  signal last_valid_local_data : integer range -1 to 15        := EMPTY_LOCAL_DATA;

  --signals for interal use of the FIFO
  signal local_fifo_rd         : std_logic                     := '0';
  signal local_fifo_out        : std_logic_vector(33 downto 0) := BAD_DATA;
  signal local_fifo_empty      : std_logic                     := '1';
  signal local_fifo_data_valid : std_logic                     := '0';

begin  -- arch
  -- fifo data in
  ADDR_FIFO_1 : ADDR_FIFO
    port map (
      clk   => clk,
      rst   => reset,
      din   => fifo_in,
      wr_en => fifo_wr,
      rd_en => local_fifo_rd,
      dout  => local_fifo_out,
      full  => fifo_full,
      empty => local_fifo_empty,
      valid => local_fifo_data_valid);


  -- this process fills the array out of the fifo and presents the data to the
  -- user (via fifo like interface)
  local_data_filler : process (clk, reset) is
  begin  -- process local_data_filler
    if reset = '1' then                 -- asynchronous reset (active high)
      local_data            <= (others => BAD_DATA);
      buffer_overflow       <= '0';
      last_valid_local_data <= EMPTY_LOCAL_DATA;

      fifo_out       <= BAD_DATA;
      fifo_out_valid <= '0';
      local_fifo_rd  <= '0';

    elsif clk'event and clk = '1' then  -- rising clock edge
      --per clock resets
      fifo_out_valid  <= '0';
      buffer_overflow <= '0';
      local_fifo_rd   <= '0';

      ------------------------------------------------------------------------------------
      -- deal with read
      if fifo_rd = '1' then
        ------------------------------------------
        -- output data
        fifo_out <= local_data(0);
        if local_data(0)(33 downto 32) /= "11" then
          -- "11" means invalid data
          fifo_out_valid <= '1';
        end if;

        ------------------------------------------
        -- shift old data (add bad data)
        local_data(0 to MAX_LOCAL_DATA-1) <= local_data(1 to MAX_LOCAL_DATA);
        local_data(MAX_LOCAL_DATA)        <= BAD_DATA; -- add bad value to the
                                                       -- end
        if last_valid_local_data /= EMPTY_LOCAL_DATA then
          local_data(last_valid_local_data) <= BAD_DATA; -- add bad value to the
                                                         -- last valid
        end if;

        ------------------------------------------
        -- shift in new valid data 
        if local_fifo_data_valid = '1' then
          if last_valid_local_data = EMPTY_LOCAL_DATA then
            -- if there isn't any existing valid data, start at zero
            local_data(0) <= local_fifo_out;
          else
            -- we just clocked out a value, and added one, so this stays the same
            local_data(last_valid_local_data) <= local_fifo_out;
          end if;
        end if;

        ------------------------------------------
        -- update last_valid_local_data
        if last_valid_local_data /= EMPTY_LOCAL_DATA and local_fifo_data_valid = '0' then
          -- We clocked out valid data, but there is no new data
          last_valid_local_data <= last_valid_local_data - 1;
        elsif last_valid_local_data = EMPTY_LOCAL_DATA and local_fifo_data_valid = '1' then
          -- there was no valid data, but we got new data
          last_valid_local_data <= 0;
        end if;

        ------------------------------------------
        -- check if we could get more data
        if local_fifo_empty = '0' then
          if last_valid_local_data = MAX_LOCAL_DATA then
            -- if we are just moving from MAX_LOCAL_DATA down one, then we want
            -- to get more data, but only one more read, so if local_fifo_rd is
            -- already a '1', we are set.
            -- otherwise, get more data
            if local_fifo_data_valid = '0' and local_fifo_rd = '0' then
              local_fifo_rd <= '1';
            end if;
          else
            -- if we are less thatn MAX_LOCAL_DATA, then we already want more
            -- data and the fact that we just clocked out some data makes it
            -- even more pressing. Get more data
            local_fifo_rd <= '1';
          end if;
        end if;

      ------------------------------------------------------------------------------------
      -- deal with no read
      else
        ------------------------------------------
        -- shift in new data (if present) and update last_valid_local_data
        if local_fifo_data_valid = '1' then
          if last_valid_local_data /= MAX_LOCAL_DATA then
            -- add this data to the entry after the current last valid
            -- only if there is room
            local_data(last_valid_local_data+1) <= local_fifo_out;
            -- now last_valid_local_data has moved up 
            last_valid_local_data               <= last_valid_local_data + 1;
          else
            buffer_overflow <= '1';
          end if;
        end if;

        ------------------------------------------
        -- get more data if we need it. 
        if local_fifo_empty = '0' then
          case last_valid_local_data is
            when MAX_LOCAL_DATA => null;
            when MAX_LOCAL_DATA -1 =>
              -- we want one more word of data.  If we just got it
              -- (local_fifo_data_valid = '1'), great, we
              -- are done.
              -- if local_fifo_rd is already '1', then also great we are done.
              -- otherwise, we want more data
              if local_fifo_data_valid = '0' and local_fifo_rd = '0' then
                local_fifo_rd <= '1';
              end if;
            when MAX_LOCAL_DATA -2 =>
              --we want no more than 2 new words, so if we just got one and one
              --is requested, we are good.
              --If either is a zero, then we want to read
              if local_fifo_data_valid = '0' or local_fifo_rd = '0' then
                local_fifo_rd <= '1';
              end if;
            when others =>
              -- we want all the data
              local_fifo_rd <= '1';
          end case;
        end if;

      end if;
    end if;
  end process local_data_filler;

  --update the interface that we have data to give them
  fifo_emtpy_proc : process (clk, reset) is
  begin  -- process fifo_emtpy_proc
    if reset = '1' then                 -- asynchronous reset (active high)
      fifo_empty <= '1';
    elsif clk'event and clk = '1' then  -- rising clock edge
      fifo_empty <= '1';
      -- we aren't empty if we are not in the empty position in local_data, or
      -- if we just got some valid data out of the FIFO
      if last_valid_local_data > EMPTY_LOCAL_DATA or local_fifo_data_valid = '1' then
        fifo_empty <= '0';
      end if;
    end if;
  end process fifo_emtpy_proc;


  --When a the full header is available and the starting word is in the first
  --position, then expose the full header (data) and set data_valid to '1'
  -- this will stay valid until we start reading out the FIFO
  local_data_presenter : process (clk, reset) is
  begin  -- process local_data_filler
    if reset = '1' then                 -- asynchronous reset (active high)
      data       <= (others => BAD_DATA);
      data_valid <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      data       <= (others => BAD_DATA);
      data_valid <= '0';
      if local_data(0)(33 downto 32) = "10" and last_valid_local_data = MAX_LOCAL_DATA then
        data       <= local_data;
        data_valid <= '1';
      end if;
    end if;
  end process local_data_presenter;
end architecture arch;
