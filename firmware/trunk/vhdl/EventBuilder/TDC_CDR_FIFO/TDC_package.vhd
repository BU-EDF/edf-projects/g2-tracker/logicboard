----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package TDC_package is
  
  type TDC_header_info_t is record
    spill_number         : std_logic_vector(23 downto 0);
    spill_time           : std_logic_vector(43 downto 0);
    size                 : unsigned(14 downto 0);
    size_error           : std_logic;
    debug_expected_size  : unsigned(14 downto 0);
    fatal_error          : std_logic;
  end record TDC_header_info_t;

  type TDC_debug_monitor_t is record
    lost_alignment       : std_logic_vector(4 downto 0);
--    cdr_edges            : std_logic_vector(4 downto 0);
--    cdr_histogram        : std_logic_vector(4 downto 0);
--    current_10b_char     : std_logic_vector(10 downto 0); -- 10 bits + 1 dv
--    current_8b_char      : std_logic_vector(9 downto 0);  -- 9 bits + 1dv
    addr_fifo_header_valid : std_logic;
    tdc_size_valid       : std_logic;
    tdc_size_fifo_empty  : std_logic;
    cdr_phase_switch     : std_logic;
  end record TDC_debug_monitor_t;
--  constant DEFAULT_TDC_DEBUG_MONITOR : TDC_debug_monitor_t := ("00000","00000","00000","000"&x"00","00"&x"00",'0','0','0','0');
  constant DEFAULT_TDC_DEBUG_MONITOR : TDC_debug_monitor_t := ("00000",'0','0','0','0');
  
--  type TDC_debug_control_t is record
--    cdr_override_setting : std_logic_vector(2 downto 0);
--  end record TDC_debug_control_t;

  type TDC_counter_control_t is record
    reset_time                      : std_logic;
    reset_d_chars                   : std_logic;
    reset_k_chars                   : std_logic;
    reset_k_chars_3C                : std_logic;
    reset_k_chars_BC                : std_logic;
    reset_malformed_spills          : std_logic;
    reset_k_chars_in_data           : std_logic;
    reset_invalid_chars             : std_logic;
    reset_bad_disparity             : std_logic;
    reset_misaligned_syncs          : std_logic;
    reset_lock_timeouts             : std_logic;
--    reset_multi_valids              : std_logic;
    reset_DECODE_FIND_LOCK_time     : std_logic;
    reset_DECODE_LOCKED_time        : std_logic;
    reset_DECODE_PROCESS_SPILL_time : std_logic;
    reset_stream_starts             : std_logic;
    reset_stream_ends               : std_logic;
    reset_lost_alignment            : std_logic;

    enable_time                      : std_logic;
    enable_d_chars                   : std_logic;
    enable_k_chars                   : std_logic;
    enable_k_chars_3C                : std_logic;
    enable_k_chars_BC                : std_logic;
    enable_malformed_spills          : std_logic;
    enable_k_chars_in_data           : std_logic;
    enable_invalid_chars             : std_logic;
    enable_bad_disparity             : std_logic;
    enable_misaligned_syncs          : std_logic;
    enable_lock_timeouts             : std_logic;
--    enable_multi_valids              : std_logic;
    enable_DECODE_FIND_LOCK_time     : std_logic;
    enable_DECODE_LOCKED_time        : std_logic;
    enable_DECODE_PROCESS_SPILL_time : std_logic;
    enable_stream_starts             : std_logic;
    enable_stream_ends               : std_logic;
    enable_lost_alignment            : std_logic;
    
  end record TDC_counter_control_t;


  type TDC_counters_t is record
    time_count : unsigned(31 downto 0);
    
    d_chars    : unsigned(31 downto 0);
    k_chars    : unsigned(31 downto 0);
    k_chars_3C : unsigned(31 downto 0);
    k_chars_BC : unsigned(31 downto 0);

    malformed_spills : unsigned(31 downto 0);
    k_chars_in_data  : unsigned(31 downto 0);
    invalid_chars    : unsigned(31 downto 0);
    bad_disparity    : unsigned(31 downto 0);
    misaligned_syncs : unsigned(31 downto 0);
    lock_timeouts    : unsigned(31 downto 0);
--    multi_valids     : unsigned(31 downto 0);
    DECODE_FIND_LOCK_time     : unsigned(31 downto 0);
    DECODE_LOCKED_time        : unsigned(31 downto 0);
    DECODE_PROCESS_SPILL_time : unsigned(31 downto 0);
    stream_starts    : unsigned(15 downto 0);
    stream_ends      : unsigned(15 downto 0);

    lost_alignment   : unsigned(31 downto 0);
  end record TDC_counters_t;
  
end TDC_package;
