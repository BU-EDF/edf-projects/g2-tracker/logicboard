restart
source vhdl/sim/UART_helper.tcl
source vhdl/sim/8b10b_helper.tcl
source vhdl/sim/TDC_spill_helper.tcl


#set initial conditions for UART
isim force add {/top/sc_in} 1
isim force add {/top/control_register_1/eb_control.tdc_enable_mask} F -radix hex 
isim force add {/top/reset} 1
run 100 ns
isim force add {/top/reset} 0
#isim force add {/top/tdc_sda} 1

#set clock on /top/clk
isim force add {/top/clk125_in} 1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns 

#let everything start up
run 8 ms

#send daq_reg_rd command
sendUART_str cdr 115200 /top/sc_in;
#send CR (0x0D)
sendUART_hex 0D 115200 /top/sc_in;

set disp [send_8b10b_idle -1 /top/TDC_serial(0) 1000]

