----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package TestPulse_IO is
  
  type TestPulseControl_t is record
    trigger_all_types : std_logic;
    start_time_count  : std_logic_vector(19 downto 0);
    pulse_count       : std_Logic_vector(7 downto 0);
    pulse_pulse_count : std_logic_vector(19 downto 0);
    pulse_channel_mask: std_logic_vector(7 downto 0);
  end record;
end TestPulse_IO;
