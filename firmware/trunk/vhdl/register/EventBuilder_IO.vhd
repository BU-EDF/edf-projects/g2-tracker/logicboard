----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.C5_package.all;
use work.TDC_package.all;
use work.TDC_CDR_Debugging.all;

package EventBuilder_IO is
  type unsigned_array_t         is array (0 to 3) of unsigned(31 downto 0);
  type std_logic_vector_array_t is array (0 to 3) of std_logic_vector(33 downto 0);
  type TDC_size_array_t         is array (0 to 3) of std_logic_vector(14 downto 0);
  type std_logic_vector5_array_t        is array (0 to 3) of std_logic_vector(4 downto 0);
  type char10b_array_t          is array (0 to 3) of std_logic_vector(9 downto 0);
  type char8b_array_t           is array (0 to 3) of std_logic_vector(8 downto 0);
  type CDR_setting_array_t      is array (0 to 3) of std_logic_vector(2 downto 0);
  type Send_state_array_t       is array (1 to 7) of unsigned(31 downto 0);
  type TDC_Counters_array_t     is array (0 to 3) of TDC_counters_t;
  type TDC_Counter_control_array_t is array (0 to 3) of TDC_counter_control_t;
  type CDR_history_array_t       is array (0 to 3) of CDR_history_t;
  type CDR_history_control_array_t is array (0 to 3) of CDR_history_control_t;
  
  type EventBuilder_Monitor_t is
  record    
    new_spill_count  : std_logic_vector(31 downto 0);
    sent_spill_count : std_logic_vector(31 downto 0);

    -- EB stuff
    EB_reset : std_logic;
    EB_state : std_logic_vector(7 downto 0);
    EB_state_time_idle : unsigned(63 downto 0);
    EB_state_time : Send_state_array_t;

    -- C5 stuff
    C5_fifo_full  : std_logic;
    C5_fifo_empty : std_logic;
    C5_CMD_counters   : c5_counters_t;

    -- TDC stuff
    TDC_expected_size : TDC_size_array_t;
    TDC_actual_size   : TDC_size_array_t;
    TDC_has_header    : std_logic_vector(3 downto 0);

    TDC_addr_fifo_header_valid : std_logic_vector(3 downto 0);
    TDC_size_valid             : std_logic_vector(3 downto 0);
    TDC_size_fifo_empty        : std_logic_vector(3 downto 0);

    TDC_CDR_phase_switch       : std_logic_vector(3 downto 0);
    
    TDC_10b : char10b_array_t;
    TDC_8b  : char8b_array_t;

--    TDC_CDR_edges     : std_logic_vector5_array_t;
--    TDC_CDR_histogram : std_logic_vector5_array_t;
    TDC_CDR_lost_alignment : std_logic_vector5_array_t;
    TDC_CDR_history : CDR_history_array_t;


    -- counters
    TDC_counters      : TDC_Counters_array_t;
    TDC_missing_trailer_bits : unsigned_array_t;
    EB_output_8bchars : unsigned(31 downto 0);
    EB_output_10bchars : unsigned(31 downto 0);
    EB_missed_10bchars : unsigned(31 downto 0);
    EB_output_words : unsigned(31 downto 0);
    EB_output_word_overflows : unsigned(31 downto 0);
    EB_output_chars_total : unsigned(31 downto 0);

    
    -- debug FIFO IO
    TDC_fifo_full       : std_logic_vector(3 downto 0);
    TDC_fifo_empty      : std_logic_vector(3 downto 0);
    TDC_locked          : std_logic_vector(3 downto 0);
    TDC_fifo_data       : std_logic_vector_array_t;
    TDC_fifo_data_valid : std_logic_vector(3 downto 0);

    LB_fifo_full       : std_logic;
    LB_fifo_empty      : std_logic;
    LB_fifo_data       : std_logic_vector(8 downto 0);
    LB_fifo_data_valid : std_logic;
    

  end record;

  type EventBuilder_Control_t is record
    run_enable       : std_logic;
    TDC_enable_mask          : std_logic_vector(3 downto 0);
    TDC_FIFO_rd              : std_logic_vector(3 downto 0);
    LB_FIFO_rd               : std_logic;
    TDC_CDR_override_setting : CDR_setting_array_t;
    time_skew_max            : std_logic_vector(31 downto 0);

    TDC_CDR_history_control : CDR_history_control_array_t;
    
    TDC_Counter_control      : TDC_Counter_control_array_t;

    reset_TDC_missing_trailer_bits : std_logic_vector(3 downto 0);

    reset_EB_state_time            : std_logic_vector(7 downto 0);
    reset_EB_output_8bchars        : std_logic;
    reset_EB_output_10bchars       : std_logic;
    reset_EB_missed_10bchars       : std_logic;
    reset_EB_output_words          : std_logic;
    reset_EB_output_word_overflows : std_logic;
    reset_EB_output_chars_total    : std_logic;

    enable_TDC_missing_trailer_bits : std_logic_vector(3 downto 0);

    enable_EB_state_time            : std_logic_vector(7 downto 0);
    enable_EB_output_8bchars        : std_logic;
    enable_EB_output_10bchars       : std_logic;
    enable_EB_missed_10bchars       : std_logic;
    enable_EB_output_words          : std_logic;
    enable_EB_output_word_overflows : std_logic;
    enable_EB_output_chars_total    : std_logic;

    
  end record;
end EventBuilder_IO;
