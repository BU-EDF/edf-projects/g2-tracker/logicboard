----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

use work.TestPulse_IO.all;
use work.SpillTypes.all;

entity TestPulse_top is
  
  port (
    clk40      : in  std_logic;
    reset      : in  std_logic;
    spill_type : in  std_logic_vector(3 downto 0);
    new_spill  : in  std_logic;
    TestPulse  : out std_logic_vector(7 downto 0);
    clkdaq     : in  std_logic;
    control    : in  TestPulseControl_t);

end entity TestPulse_top;

architecture Behavioral of TestPulse_top is

  component pass_std_logic is
    generic (
      RESET_VAL : std_logic);
    port (
      clk_in   : in  std_logic;
      clk_out  : in  std_logic;
      reset    : in  std_logic;
      pass_in  : in  std_logic;
      pass_out : out std_logic);
  end component pass_std_logic;
  
  component pass_std_logic_vector is
    generic (
      DATA_WIDTH : integer;
      RESET_VAL  : std_logic_vector);
    port (
      clk_in   : in  std_logic;
      clk_out  : in  std_logic;
      reset    : in  std_logic;
      pass_in  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
      pass_out : out std_logic_vector(DATA_WIDTH-1 downto 0));
  end component pass_std_logic_vector;
  
  type pulser_state_t is (IDLE,START,PULSE_GEN);
  signal pulser_state : pulser_state_t := IDLE;

  constant counter_start : unsigned(19 downto 0) := x"00000";
  signal counter : unsigned(19 downto 0) := counter_start;
  signal pulse_count : unsigned(7 downto 0) := x"00";

  signal pulser_settings : TestPulseControl_t := ('0',others => (others =>'0'));
  signal control_local : TestPulseControl_t := ('0',others => (others =>'0'));
  
  signal local_new_spill : std_logic := '0';
  
begin  -- architecture Behavioral

  pipeline_delay: process (clk40) is
  begin  -- process pipeline_delay
    if clk40'event and clk40 = '1' then  -- rising clock edge
      local_new_spill <= new_spill;
    end if;
  end process pipeline_delay;

  --Pass the control parameters from the 125Mhz clock domain to the 40Mhz one
  pass_std_logic_1: pass_std_logic
    generic map (
      RESET_VAL => '0')
    port map (
      clk_in   => clkdaq,
      clk_out  => clk40,
      reset    => reset,
      pass_in  => control.trigger_all_types,
      pass_out => control_local.trigger_all_types);
  pass_std_logic_vector_1: pass_std_logic_vector
    generic map (
      DATA_WIDTH => 20,
      RESET_VAL  => x"00000")
    port map (
      clk_in   => clkdaq,
      clk_out  => clk40,
      reset    => reset,
      pass_in  => control.start_time_count,
      pass_out => control_local.start_time_count);
  pass_std_logic_vector_2: pass_std_logic_vector
    generic map (
      DATA_WIDTH => 8,
      RESET_VAL  => x"00")
    port map (
      clk_in   => clkdaq,
      clk_out  => clk40,
      reset    => reset,
      pass_in  => control.pulse_count,
      pass_out => control_local.pulse_count);
  pass_std_logic_vector_3: pass_std_logic_vector
    generic map (
      DATA_WIDTH => 20,
      RESET_VAL  => x"00000")
    port map (
      clk_in   => clkdaq,
      clk_out  => clk40,
      reset    => reset,
      pass_in  => control.pulse_pulse_count,
      pass_out => control_local.pulse_pulse_count);
  pass_std_logic_vector_4: pass_std_logic_vector
    generic map (
      DATA_WIDTH => 8,
      RESET_VAL  => x"00")
    port map (
      clk_in   => clkdaq,
      clk_out  => clk40,
      reset    => reset,
      pass_in  => control.pulse_channel_mask,
      pass_out => control_local.pulse_channel_mask);

  
   
  pulser: process (clk40, reset) is
  begin  -- process pulser
    if reset = '1' then                 -- asynchronous reset (active high)
      counter <= counter_start;
      TestPulse <= x"00";
    elsif clk40'event and clk40 = '1' then  -- rising clock edge
      TestPulse <= x"00";
      case pulser_state is
        when IDLE =>
          -- wait for a new spill from the C5 decoder
          if local_new_spill = '1' then
            pulser_settings <= control_local;
            -- do something different for each kind of new spill
            if spill_type = SPILL_CAL_1 then
              pulser_state <= START;
            elsif control.trigger_all_types = '1' then  -- take from control
                                                        -- instead of pulser
                                                        -- settings since we
                                                        -- need this on this
                                                        -- clock tic
              pulser_state <= START;
            else
              pulser_state <= IDLE;
            end if;
          end if;
        when START =>
          -- Wait in this state until we want to start putting out pulses
          TestPulse <= x"00";
          counter <= counter + 1;
          if counter >= unsigned(pulser_settings.start_time_count) then
            pulser_state <= PULSE_GEN;
            pulse_count <= x"00";
            counter <= counter_start;
          end if;
        when PULSE_GEN =>
          -- output pulses (counter now changes to pulse to pulse counter)
          counter <= counter + 1;
          if counter = unsigned(pulser_settings.pulse_pulse_count) then
            -- reset the counter
            counter <= counter_start;
            -- count the number of pulses we've sent out
            pulse_count <= pulse_count + 1;
            --check if we should send out a pulse or we are over
            if pulse_count = unsigned(pulser_settings.pulse_count) then
              TestPulse <= x"00";
              pulser_state <= IDLE;
            else
              -- send out a pulse
              TestPulse <= pulser_settings.pulse_channel_mask;
            end if;
          end if;
        when others => null;
      end case;      
    end if;
  end process pulser;
  

end architecture Behavioral;
