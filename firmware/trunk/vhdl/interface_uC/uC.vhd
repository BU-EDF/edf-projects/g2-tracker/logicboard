-------------------------------------------------------------------------------
-- top.vhd :
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed."+";
use ieee.std_logic_signed."=";

use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

use work.EventBuilder_IO.all;
entity uC is

  port (
    clk   : in std_logic;               -- 100MHz oscillator input
    reset : in std_logic;               -- reset to picoblaze

    -- SC_INT
    SC_Rx : in  std_logic;              -- serial in
    SC_Tx : out std_logic := '1';       -- serial out
    SC_NC : in  std_logic;              -- SC not connected
    SC_OE : out std_logic;              -- SC output enable

    TDC_UART_Rx : in  std_logic;
    TDC_UART_Tx : out std_logic;

    -- DAC i2c
    DAC_SDA_in  : in  std_logic;
    DAC_SDA_out : out std_logic;
    DAC_SDA_en  : out std_logic;
    DAC_SCL     : out std_logic := '1';

    -- DB i2c
    DB_SDA_in  : in  std_logic;
    DB_SDA_out : out std_logic;
    DB_SDA_en  : out std_logic;
    DB_SCL     : out std_logic := '1';

    
--    JTAG_TDO : in std_logic_vector(1 downto 0);
--    JTAG_TDI : out std_logic_vector(1 downto 0);
--    JTAG_TSM : out std_logic_vector(1 downto 0);
--    JTAG_TCK : out std_logic_vector(1 downto 0);

    -- C5 temperature convert signal
    C5_temp_CMD   : in std_logic;
    
    TDC_uC_UART_Tx : out std_logic_vector(1 downto 0);
    TDC_uC_UART_Rx : in  std_logic_vector(1 downto 0);
    TDC_uC_reset   : out std_logic_vector(1 downto 0);
    
    -- one wire temp sensors DS1820
    Wire_DS1820_in     : in  std_logic_vector(1 downto 0);
    Wire_DS1820_out    : out std_logic_vector(1 downto 0) := "11";
    Wire_DS1820_T      : out   std_logic_vector(1 downto 0) := "11";

    --FLASH interface
    FLASH_SPI_CLK   : out std_logic := '1';
    FLASH_SPI_MOSI  : out std_logic := '1';
    FLASH_SPI_MISO  : in std_logic;
    FLASH_SPI_CS    : out std_logic := '1';

    -- register interface
    reg_addr       : out std_logic_vector(7 downto 0);
    reg_data_in_rd : out std_logic;
    reg_data_in    : in  std_logic_vector(31 downto 0);
    reg_data_in_dv : in  std_logic;
    reg_data_out   : out std_logic_vector(31 downto 0);
    reg_wr         : out std_logic
    );


end entity uC;

architecture arch of uC is

--
-- KCPSM6 uc
-- 
  component kcpsm6
    generic(
      hwbuild                 : std_logic_vector(7 downto 0)  := X"00";
      interrupt_vector        : std_logic_vector(11 downto 0) := X"600";
      scratch_pad_memory_size : integer                       := 256);
    port (
      address        : out std_logic_vector(11 downto 0);
      instruction    : in  std_logic_vector(17 downto 0);
      bram_enable    : out std_logic;
      in_port        : in  std_logic_vector(7 downto 0);
      out_port       : out std_logic_vector(7 downto 0);
      port_id        : out std_logic_vector(7 downto 0);
      write_strobe   : out std_logic;
      k_write_strobe : out std_logic;
      read_strobe    : out std_logic;
      interrupt      : in  std_logic;
      interrupt_ack  : out std_logic;
      sleep          : in  std_logic;
      reset          : in  std_logic;
      clk            : in  std_logic);
  end component;

--
-- KCPSM6 ROM
-- 
  component cli
    generic(
      C_FAMILY             : string  := "S6";
      C_RAM_SIZE_KWORDS    : integer := 1;
      C_JTAG_LOADER_ENABLE : integer := 1);
    port (
      address     : in  std_logic_vector(11 downto 0);
      instruction : out std_logic_vector(17 downto 0);
      enable      : in  std_logic;
      rdl         : out std_logic;
      clk         : in  std_logic);
  end component;

--
-- UART Transmitter     
--
  component uart_tx6
    port (
      data_in             : in  std_logic_vector(7 downto 0);
      en_16_x_baud        : in  std_logic;
      serial_out          : out std_logic;
      buffer_write        : in  std_logic;
      buffer_data_present : out std_logic;
      buffer_half_full    : out std_logic;
      buffer_full         : out std_logic;
      buffer_reset        : in  std_logic;
      clk                 : in  std_logic);
  end component;

--
-- UART Receiver
--
  component uart_rx6
    port (
      serial_in           : in  std_logic;
      en_16_x_baud        : in  std_logic;
      data_out            : out std_logic_vector(7 downto 0);
      buffer_read         : in  std_logic;
      buffer_data_present : out std_logic;
      buffer_half_full    : out std_logic;
      buffer_full         : out std_logic;
      buffer_reset        : in  std_logic;
      clk                 : in  std_logic);
  end component;

  component TDC_UART_master is
    port (
      clk62_5         : in  std_logic;
      reset          : in  std_logic;
      rx             : in  std_logic;
      tx             : out std_logic;
      data_out       : in  std_logic_vector(7 downto 0);
      data_out_wr    : in  std_logic;
      data_out_empty : out std_logic;
      data_in        : out std_logic_vector(7 downto 0);
      data_in_valid  : out std_logic;
      rx_error       : out std_logic_vector(1 downto 0);
      data_in_rd     : in  std_logic);
  end component TDC_UART_master;

  component one_wire_master is
    port (
      clk62_5     : in  std_logic;
      reset       : in  std_logic;
      wire_in     : in  std_logic;
      wire_out    : out std_logic;
      wire_T      : out std_logic;
      bit_rd      : out std_logic;
      bit_wr      : in  std_logic;
      bit_mode    : in  std_logic_vector(1 downto 0);
      bit_op_go   : in  std_logic;
      bit_op_done : out std_logic);
  end component one_wire_master;

-----------------------------------------------------------------------------
-- Signals
-----------------------------------------------------------------------------
  signal rst : std_logic;

--
-- KCPSM6 & ROM signals
--
  signal address        : std_logic_vector(11 downto 0);
  signal instruction    : std_logic_vector(17 downto 0);
  signal bram_enable    : std_logic;
  signal in_port        : std_logic_vector(7 downto 0) := "00000000";
  signal out_port       : std_logic_vector(7 downto 0) := "00000000";
  signal port_id        : std_logic_vector(7 downto 0) := "00000000";
  signal write_strobe   : std_logic                    := '0';
  signal k_write_strobe : std_logic                    := '0';
  signal read_strobe    : std_logic                    := '0';
  signal interrupt      : std_logic;
  signal interrupt_ack  : std_logic;
  signal kcpsm6_sleep   : std_logic;
  signal kcpsm6_reset   : std_logic;
  
--
-- SC UART_TX signals
--
  signal SC_Tx_local : std_logic;
  signal SC_UART_Tx_data_in      : std_logic_vector(7 downto 0);
  signal write_to_SC_UART_Tx        : std_logic;
  signal SC_UART_Tx_data_present : std_logic;
  signal SC_UART_Tx_half_full    : std_logic;
  signal SC_UART_Tx_full         : std_logic;
  signal SC_UART_Tx_reset        : std_logic := '0';

--
-- SC UART_RX signals
--
  signal SC_UART_Rx_data_out     : std_logic_vector(7 downto 0);
  signal read_from_SC_UART_Rx    : std_logic;
  signal SC_UART_Rx_data_present : std_logic;
  signal SC_UART_Rx_half_full    : std_logic;
  signal SC_UART_Rx_full         : std_logic;
  signal SC_UART_Rx_reset        : std_logic := '0';

--
-- SC UART baud rate signals
--
  signal baud_count   : integer range 0 to 67 := 0;
  signal en_16_x_baud : std_logic             := '0';

--
-- UART passthrough
--
  signal TDC_Passthrough_enable : std_logic := '0';
  signal TDC_Passthrough : std_logic := '0';
    
--
-- I2c signals
--
  signal i2c_sel     : std_logic_vector(1 downto 0) := "00";
  signal i2c_SDA_in  : std_logic;
  signal i2c_SDA_out : std_logic                    := '1';
  signal i2c_SCL_out : std_logic                    := '1';

--
-- DAQ register signals
--
  signal DAQ_reg_local_addr     : std_logic_vector(7 downto 0) := x"00";
  signal daq_reg_local_data_in  : std_logic_vector(31 downto 0);
  signal daq_reg_local_data_out : std_logic_vector(31 downto 0);
  signal daq_reg_local_valid    : std_logic                    := '0';
  signal DAQ_reg_local_wr       : std_logic                    := '0';
  signal DAQ_reg_local_rd       : std_logic                    := '0';


--
-- TDC UART
--
  signal TDC_data_out       : std_logic_vector(7 downto 0) := x"FF";
  signal TDC_data_out_wr    : std_logic                    := '0';
  signal TDC_data_out_empty : std_logic                    := '1';
  signal TDC_data_in        : std_logic_vector(7 downto 0) := x"FF";
  signal TDC_data_in_valid  : std_logic                    := '0';
  signal TDC_data_in_rd     : std_logic                    := '0';
  signal TDC_rx_error       : std_logic_vector(1 downto 0) := "00";
  signal TDC_reset          : std_logic                    := '1';


--
-- one wire
--
  type DS1820 is record
                   rd    : std_logic;
                   wr    : std_logic;
                   done  : std_logic;
                   mode  : std_logic_vector(1 downto 0);
                   go    : std_logic;
  end record DS1820;

  signal onewire_A : DS1820 := ('0','0','0',"10",'0');
  signal onewire_B : DS1820 := ('0','0','0',"10",'0');

--
-- C5 temp request
--
  constant C5_TEMP_SR_SIZE : integer := 100;
  signal C5_temp_sr : std_logic_vector(C5_temp_sr_SIZE -1 downto 0) := (others => '0');
  signal C5_temp_request : std_logic := '0';
  signal C5_temp_request_reset : std_logic := '0';
  
  
begin  -- architecture arch


-----------------------------------------------------------------------------------------
-- Instantiate KCPSM6 and connect to Program Memory
-----------------------------------------------------------------------------------------
  processor : kcpsm6
    generic map (
      hwbuild                 => X"01",
      interrupt_vector        => X"7FF",
      scratch_pad_memory_size => 256)
    port map(
      address        => address,
      instruction    => instruction,
      bram_enable    => bram_enable,
      port_id        => port_id,
      write_strobe   => write_strobe,
      k_write_strobe => k_write_strobe,
      out_port       => out_port,
      read_strobe    => read_strobe,
      in_port        => in_port,
      interrupt      => interrupt,
      interrupt_ack  => interrupt_ack,
      sleep          => kcpsm6_sleep,
      reset          => rst,
      clk            => clk);

--
--Disable sleep and interrupts on kcpsm6
--
  kcpsm6_sleep <= '0';
  interrupt    <= interrupt_ack;
  rst          <= kcpsm6_reset or reset;

--
-- KCPSM6 ROM
--
  program_rom : cli
    generic map(
      C_FAMILY             => "S6",     --Family 'S6', 'V6' or '7S'
      C_RAM_SIZE_KWORDS    => 4,        --Program size '1', '2' or '4'
      C_JTAG_LOADER_ENABLE => 1)        --Include JTAG Loader when set to '1' 
    port map(
      address     => address,
      instruction => instruction,
      enable      => bram_enable,
      rdl         => kcpsm6_reset,
      clk         => clk);

--
-- Logic board UART Transmitter
--

  SC_OE <= '1';
  TDC_tx_mod : uart_tx6
    port map (
      data_in             => SC_UART_Tx_data_in,
      en_16_x_baud        => en_16_x_baud,
      serial_out          => SC_Tx_local,
      buffer_write        => write_to_SC_UART_Tx,
      buffer_data_present => SC_UART_Tx_data_present,
      buffer_half_full    => SC_UART_Tx_half_full,
      buffer_full         => SC_UART_Tx_full,
      buffer_reset        => SC_UART_Tx_reset,
      clk                 => clk);
--
-- Logic board UART Receiver
--
  TDC_rx_mod : uart_rx6
    port map (
      serial_in           => SC_Rx,
      en_16_x_baud        => en_16_x_baud,
      data_out            => SC_UART_Rx_data_out,
      buffer_read         => read_from_SC_UART_Rx,
      buffer_data_present => SC_UART_Rx_data_present,
      buffer_half_full    => SC_UART_Rx_half_full,
      buffer_full         => SC_UART_Rx_full,
      buffer_reset        => SC_UART_Rx_reset,
      clk                 => clk);


  
-----------------------------------------------------------------------------------------
-- RS232 (UART) baud rate 
-----------------------------------------------------------------------------------------
--
-- To set serial communication baud rate to 125,000 then en_16_x_baud must pulse 
-- High at 2MHz which is every 31 cycles at 62.65MHz. In this implementation 
-- a pulse is generated every 31 cycles resulting is a baud rate of 126,01 baud (0.8%)
--

  baud_rate : process(clk)
  begin
    if clk'event and clk = '1' then
      if baud_count = 30 then           -- counts 31 states including zero
        baud_count   <= 0;
        en_16_x_baud <= '1';            -- single cycle enable pulse
      else
        baud_count   <= baud_count + 1;
        en_16_x_baud <= '0';
      end if;        
    end if;
  end process baud_rate;

----
---- To set serial communication baud rate to 115,200 then en_16_x_baud must pulse 
---- High at 1,843,200Hz which is every 34 cycles at 6.25MHz. In this implementation 
---- a pulse is generated every 34 cycles resulting is a baud rate of 114,9 baud 
----
--
--  baud_rate : process(clk)
--  begin
--    if clk'event and clk = '1' then
----      if baud_count = 67 then           -- counts 68 states including zero
--      if baud_count = 33 then           -- counts 34 states including zero
--        baud_count   <= 0;
--        en_16_x_baud <= '1';            -- single cycle enable pulse
--      else
--        baud_count   <= baud_count + 1;
--        en_16_x_baud <= '0';
--      end if;        
--    end if;
--  end process baud_rate;
--

-------------------------------------------------------------------------------
-- UART passthrough
------------------------------------------------------------------------------- 
-- purpose: This directs the logicboard's bidirectional SC UART to the TDCs if requested
-- type   : combinational
-- inputs : SC_Rx
-- outputs: 
UART_passthrough: process (TDC_Passthrough_enable,TDC_Passthrough,SC_Tx_local,SC_Rx) is
begin  -- process UART_passthrough
  if TDC_Passthrough_enable = '0' then
    SC_Tx <= SC_Tx_local;
    TDC_uC_UART_Tx <= "11";
  else
    if TDC_Passthrough = '0' then
      SC_Tx <= TDC_uC_UART_Rx(0);
      TDC_uC_UART_Tx(0) <= SC_Rx;
      TDC_uC_UART_Tx(1) <= '1';
    else
      SC_Tx <= TDC_uC_UART_Rx(1);
      TDC_uC_UART_Tx(1) <= SC_Rx;
      TDC_uC_UART_Tx(0) <= '1';
    end if;
  end if;
end process UART_passthrough;



-------------------------------------------------------------------------------
-- c5 temp interface
-------------------------------------------------------------------------------
C5_temp: process (clk, reset) is
begin  -- process C5_temp
  if reset = '1' then                   -- asynchronous reset (active high)
    C5_temp_sr <= (others => '0');
  elsif clk'event and clk = '1' then    -- rising clock edge
    -- use a shift register to elongate a pulse from the C5 temp command

    -- Updates from the C5 core get added at bit zero and shifted up
    C5_temp_sr(0) <= C5_temp_CMD;
    C5_temp_sr(C5_temp_sr_SIZE -1 downto 1) <= C5_temp_sr(C5_temp_sr_SIZE -2 downto 0);

    --The request to the uC is the or_reduce of the shift register to keep it
    --active for C5_temp_sr_SIZE clock ticks
    C5_temp_request <= or_reduce(C5_temp_sr);

    --When the uC reads the C5_temp_request when it is active, reset the shift
    --register to make sure we don't read it out multiple times. 
    if C5_temp_request_reset = '1' then
      C5_temp_sr <= (others => '0');
    end if;
  end if;
end process C5_temp;



-------------------------------------------------------------------------------
-- DAQ interface
-------------------------------------------------------------------------------
  DAQ_interface : process (clk, reset) is
  begin  -- process DAQ_interface
    if reset = '1' then                 -- asynchronous reset (active high)
      DAQ_reg_local_valid <= '0';
      reg_data_in_rd      <= '0';
      reg_addr            <= x"00";
      reg_data_out        <= x"00000000";
      reg_wr              <= '0';
      DAQ_reg_local_valid <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- update address
      reg_addr <= DAQ_reg_local_addr;

      -- request a read from the register
      reg_data_in_rd <= '0';
      if DAQ_reg_local_rd = '1' then
        -- request the read
        reg_data_in_rd      <= '1';
        -- reset the local valid
        DAQ_reg_local_valid <= '0';
        DAQ_reg_local_data_in <= x"ABADCAFE";
      end if;

      -- latch reads when valid
      if reg_data_in_dv = '1' then
        DAQ_reg_local_data_in <= reg_data_in;
        DAQ_reg_local_valid   <= '1';
      end if;

      -- write data
      reg_wr <= '0';
      if DAQ_reg_local_wr = '1' then
        reg_data_out <= DAQ_reg_local_data_out;
        reg_wr       <= '1';
      end if;
    end if;
  end process DAQ_interface;

  
-----------------------------------------------------------------------------
-- Reading interface of the UC to the UART
-----------------------------------------------------------------------------
  input_ports : process(clk)
  begin
    if clk'event and clk = '1' then
      case port_id(3 downto 0) is
        -----------------------------------------------------------------------
        -- UART status port
        when x"0" =>
          in_port(0)          <= SC_UART_Tx_data_present;
          in_port(1)          <= SC_UART_Tx_half_full;
          in_port(2)          <= SC_UART_Tx_full;
          in_port(3)          <= SC_UART_Rx_data_present;
          in_port(4)          <= SC_UART_Rx_half_full;
          in_port(5)          <= SC_UART_Rx_full;
          in_port(6)          <= SC_NC;
          in_port(7)          <= '0';
        -----------------------------------------------------------------------
        -- UART input port
        when x"1" =>
          -- (see 'buffer_read' pulse generation below) 
          in_port <= SC_UART_Rx_data_out;
        -----------------------------------------------------------------------
        -- TDC i2c bus;
        when x"2" =>
          -- we do not support clock stretching right now
          in_port(0)          <= '0';   --TDC_SCL;
          --
          in_port(1)          <= i2c_SDA_in;
          in_port(7 downto 2) <= "000000";
        ----------------------------------------------------------------------
        -- DAQ register control
        when x"3" =>
          in_port <= "000" & DAQ_reg_local_valid & "0000";
        ----------------------------------------------------------------------
        -- DAQ register addr
        when x"4" =>
          in_port <= DAQ_reg_local_addr;
        -----------------------------------------------------------------------
        -- DAQ input byte 0
        when x"5" =>
          in_port <= DAQ_reg_local_data_in(7 downto 0);
        -----------------------------------------------------------------------
        -- DAQ input byte 1
        when x"6" =>
          in_port <= DAQ_reg_local_data_in(15 downto 8);
        -----------------------------------------------------------------------
        -- DAQ input byte 2
        when x"7" =>
          in_port <= DAQ_reg_local_data_in(23 downto 16);
        -----------------------------------------------------------------------
        -- DAQ input byte 3
        when x"8" =>
          in_port <= DAQ_reg_local_data_in(31 downto 24);
        -----------------------------------------------------------------------
        -- TDC UART status
        when x"9" =>
          in_port <= x"0" & TDC_rx_error & TDC_data_in_valid & TDC_data_out_empty;
        -----------------------------------------------------------------------
        -- TDC UART data in
        when x"A" =>
          in_port <= TDC_data_in;

        -----------------------------------------------------------------------
        -- one wire sensors
        when x"B" =>
          --A bus
          in_port(0)          <= onewire_A.rd;
          in_port(1)          <= onewire_A.done;
          in_port(3 downto 2) <= onewire_A.mode;
          --B bus
          in_port(4)          <= onewire_B.rd;
          in_port(5)          <= onewire_B.done;
          in_port(7 downto 6) <= onewire_B.mode;

        -----------------------------------------------------------------------
        -- Flash
        when x"C" =>
          in_port(2) <= FLASH_SPI_MISO;

        -----------------------------------------------------------------------
        -- TDC UART status port
        when x"D" =>
          in_port(0)          <= TDC_Passthrough_enable;
          in_port(1)          <= TDC_Passthrough;

        -----------------------------------------------------------------------
        -- C5 temp signal
        when x"E" =>
          in_port(0)          <= C5_temp_request;
          if C5_temp_request = '1' then
            C5_temp_request_reset <= '1';
          end if;
        -----------------------------------------------------------------------
        -- others do nothing
        when others => in_port <= x"00";

      end case;

      -------------------------------------------------------------------------
      -- For UART read
      -- Generate 'buffer_read' pulse following read from port address 01
      if (read_strobe = '1') and (port_id = x"01") then
        read_from_SC_UART_Rx <= '1';
      else
        read_from_SC_UART_Rx <= '0';
      end if;

    end if;
  end process input_ports;

-----------------------------------------------------------------------------
-- Writing interface of the UC to UART and other devices
-----------------------------------------------------------------------------
  output_ports : process(clk, reset)
  begin
    if reset = '1' then
      DAQ_reg_local_rd <= '0';
      DAQ_reg_local_wr <= '0';
      TDC_reset        <= '1';
      TDC_data_out_wr  <= '0';
      TDC_data_in_rd   <= '0';
      onewire_A.go <= '0';
      onewire_B.go <= '0';
      SC_UART_Tx_reset <= '1';
      SC_UART_Rx_reset <= '1';
      TDC_uc_reset <= "11";
    elsif clk'event and clk = '1' then
      -- reset register strobe
      SC_UART_Tx_reset <= '0';
      SC_UART_Rx_reset <= '0';
      DAQ_reg_local_rd <= '0';
      DAQ_reg_local_wr <= '0';
      TDC_data_out_wr  <= '0';
      TDC_data_in_rd   <= '0';
      TDC_reset        <= '0';
      onewire_A.go <= '0';
      onewire_B.go <= '0';
      -------------------------------------------------------------------------
      -- Constant write strobe
      if k_write_strobe = '1' then
        case port_id(3 downto 0) is
          --------------------
          -- Uart status port 
          when x"0" =>
            SC_UART_Tx_reset <= out_port(0);
            SC_UART_Rx_reset <= out_port(1);
          -----------------------------------------------------------------------
          -- Flash
          when x"C" =>
            FLASH_SPI_CLK  <= out_port(0);
            FLASH_SPI_CS   <= out_port(1);
            FLASH_SPI_MOSI <= out_port(2);
          when others => null;
        end case;
      -------------------------------------------------------------------------
      -- write strobe
      elsif write_strobe = '1' then
        case port_id(3 downto 0) is
          --------------------
          -- i2c busses;
          when x"2" =>
            i2c_sel     <= out_port(3 downto 2);
            i2c_SDA_out <= out_port(1);
            i2c_SCL_out <= out_port(0);

          --------------------
          -- DAQ register control
          when x"3" =>
            -- latch data at the address for reading in
            if out_port(3) = '1' then
              DAQ_reg_local_rd <= '1';
            end if;
            -- present data and write strobe
            if out_port(2) = '1' then
              DAQ_reg_local_wr <= '1';
            end if;
            --update 32 -> 8 bit read address
--            DAQ_word_addr <= out_port(1 downto 0);
          --------------------
          -- DAQ register address
          when x"4" =>
            DAQ_reg_local_addr <= out_port;
          --------------------
          -- DAQ output byte 0
          when x"5" =>
            DAQ_reg_local_data_out(7 downto 0)   <= out_port;
          --------------------
          -- DAQ output byte 1
          when x"6" =>
            DAQ_reg_local_data_out(15 downto 8)   <= out_port;
          --------------------
          -- DAQ output byte 2
          when x"7" =>
            DAQ_reg_local_data_out(23 downto 16)   <= out_port;
          --------------------
          -- DAQ output byte 3
          when x"8" =>
            DAQ_reg_local_data_out(31 downto 24)   <= out_port;
            
          --------------------
          -- TDC status
          when x"9" =>
            if out_port(5) = '1' then
              TDC_data_out_wr <= '1';
            end if;
            if out_port(4) = '1' then
              TDC_data_in_rd <= '1';
            end if;
            if out_port(7) = '1' then
              TDC_reset <= '1';
            end if;
          --------------------
          -- TDC_data
          when x"A" =>
            TDC_data_out <= out_port;
          -----------------------------------------------------------------------
          -- one wire sensors
          when x"B" =>
            --A bus
            onewire_A.wr                <= out_port(0);
            onewire_A.go                <= out_port(1);
            onewire_A.mode              <= out_port(3 downto 2);
            --B bus
            onewire_B.wr                <= out_port(4);
            onewire_B.go                <= out_port(5);
            onewire_B.mode              <= out_port(7 downto 6);
          -----------------------------------------------------------------------
          -- Flash
          when x"C" =>
            FLASH_SPI_CLK  <= out_port(0);
            FLASH_SPI_CS   <= out_port(1);
            FLASH_SPI_MOSI <= out_port(2);
          -----------------------------------------------------------------------
          -- TDC UART status port
          when x"D" =>            
            TDC_Passthrough_enable <= out_port(0);
            TDC_Passthrough        <= out_port(1);
            TDC_uC_reset(0)        <= not out_port(2);
            TDC_uC_reset(1)        <= not out_port(3);
          

          when others => null;
        end case;
      end if;
    end if;
  end process output_ports;

--
-- Update the uart out signals without the one clock delay
-- 
  SC_UART_Tx_data_in  <= out_port;
  write_to_SC_UART_Tx <= '1' when (write_strobe = '1') and (port_id = x"01")
                         else '0';

-------------------------------------------------------------------------------
-- TDC_UART (slow control)
-------------------------------------------------------------------------------
  TDC_reset_proc : process (clk, reset) is
  begin  -- process TDC_reset_proc
    if reset = '1' then                 -- asynchronous reset (active high)

    elsif clk'event and clk = '1' then  -- rising clock edge

    end if;
  end process TDC_reset_proc;

  TDC_UART_master_1 : TDC_UART_master
    port map (
      clk62_5         => clk,
      reset          => TDC_reset,
      rx             => TDC_UART_Rx,
      tx             => TDC_UART_Tx,
      data_out       => TDC_data_out,
      data_out_wr    => TDC_data_out_wr,
      data_out_empty => TDC_data_out_empty,
      data_in        => TDC_data_in,
      data_in_valid  => TDC_data_in_valid,
      rx_error       => TDC_rx_error,
      data_in_rd     => TDC_data_in_rd);


-------------------------------------------------------------------------------
-- I2c mux
-------------------------------------------------------------------------------
  i2c_mux : process (clk)
  begin  -- process i2c_mux
    if clk'event and clk = '1' then     -- rising clock edge
      case i2c_sel is
        when "01" =>
          i2c_SDA_in <= DB_SDA_in;
          DB_SDA_out <= i2c_SDA_out;
          DB_SDA_en  <= not(i2c_SDA_out);
          DB_SCL     <= i2c_SCL_out;
        when "10" =>
          i2c_SDA_in  <= DAC_SDA_in;
          DAC_SDA_out <= i2c_SDA_out;
          DAC_SDA_en  <= not(i2c_SDA_out);
          DAC_SCL     <= i2c_SCL_out;
        when others => null;
      end case;
    end if;
  end process i2c_mux;

-------------------------------------------------------------------------------
-- one wire
------------------------------------------------------------------------------- 

  one_wire_master_A: one_wire_master
    port map (
      clk62_5     => clk,
      reset       => reset,
      wire_in     => Wire_DS1820_in(0),
      wire_out    => Wire_DS1820_out(0),
      wire_T      => Wire_DS1820_T(0),
      bit_rd      => onewire_A.rd,
      bit_wr      => onewire_A.wr,
      bit_mode    => onewire_A.mode,
      bit_op_go   => onewire_A.go,
      bit_op_done => onewire_A.done);

  one_wire_master_B: one_wire_master
    port map (
      clk62_5     => clk,
      reset       => reset,
      wire_in     => Wire_DS1820_in(1),
      wire_out    => Wire_DS1820_out(1),
      wire_T      => Wire_DS1820_T(1),
      bit_rd      => onewire_B.rd,
      bit_wr      => onewire_B.wr,
      bit_mode    => onewire_B.mode,
      bit_op_go   => onewire_B.go,
      bit_op_done => onewire_B.done);

  
end architecture arch;
