;;; ============================================================================
;;; ONEWIRE
;;; ============================================================================
;;; | name                | s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7 | s8 | s9 | sA | sB | sC | sD | sE | sF |
;;; | onewire_presence    | x  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
;;; | onewire_write_byte  | x  | x  | x  |    |    |    |    |    |    |    |    |    |    |    |    |    |
;;; | onewire_read_byte   | x  | x  | x  |    |    |    |    |    |    |    |    |    |    |    |    |    |
;;; | onewire_pwr         | x  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
;;; | onewire_read_bit    | x  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
;;; | onewire_write_bit   | x  | x  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |




CONSTANT ONEWIRE_PORT, 0B

CONSTANT ONEWIRE_A_DATA, 01
CONSTANT ONEWIRE_A_OP, 02
CONSTANT ONEWIRE_A_WRITE,  00 	; write
CONSTANT ONEWIRE_A_READ,  04 	; read
CONSTANT ONEWIRE_A_INIT,  08 	; init
CONSTANT ONEWIRE_A_PWR,  0C 	; pwr

CONSTANT ONEWIRE_B_DATA, 10
CONSTANT ONEWIRE_B_OP, 20
CONSTANT ONEWIRE_B_WRITE,  00 	;write
CONSTANT ONEWIRE_B_READ,  40 	;read
CONSTANT ONEWIRE_B_INIT,  80 	;init
CONSTANT ONEWIRE_B_PWR,  C0 	;pwr

	
;;; ============================================================================
;;; Reset the bus and see if anyone is there
;;; input:
;;; 	s0, bus, A(0) B(1)
;;; output:
;;; 	C = 1 found, C = 0 none
;;; ============================================================================
onewire_presence:
	TEST s0, 01
	JUMP C, onewire_presence_B
	;; sensor A
	LOAD s0, 00
	OR s0, ONEWIRE_A_INIT
	OR s0, ONEWIRE_A_OP
	OUTPUT s0, ONEWIRE_PORT	;start the reset
onewire_presence_poll_A:
	LOAD s0, 00 		;delay
	LOAD s0, 00 		;delay
	INPUT s0, ONEWIRE_PORT
	TEST s0, ONEWIRE_A_OP	; test for op done	
	JUMP Z, onewire_presence_poll_A
	;; test for presence
	TEST s0, ONEWIRE_A_DATA
	JUMP Z, onewire_presence_found
	JUMP onewire_presence_none

onewire_presence_B:	
	;; sensor B
	LOAD s0, 00
	OR s0, ONEWIRE_B_INIT
	OR s0, ONEWIRE_B_OP
	OUTPUT s0, ONEWIRE_PORT	;start the reset
onewire_presence_poll_B:
	LOAD s0, 00 		;delay
	LOAD s0, 00 		;delay
	INPUT s0, ONEWIRE_PORT
	TEST s0, ONEWIRE_B_OP	; test for op done
	JUMP Z, onewire_presence_poll_B
	;; test for presence
	TEST s0, ONEWIRE_B_DATA
	JUMP Z, onewire_presence_found

onewire_presence_none:
	LOAD s0, 00
	SR0 s0
	RETURN

onewire_presence_found:
	LOAD s0, 01
	SR0 s0
	RETURN



;;; ============================================================================
;;; Put the bus in high power mode
;;; input:
;;; 	s0, bus, A(0) B(1)
;;; ============================================================================
onewire_pwr:
	TEST s0, 01
	JUMP C, onewire_pwr_B_init
onewire_pwr_A_init:
	;; make sure the last op finishes
	INPUT s0, ONEWIRE_PORT
	TEST s0, ONEWIRE_A_OP 	;wait for ready to be a '1'
	JUMP Z, onewire_pwr_A_init
	;; pwr op
	LOAD s0, ONEWIRE_A_PWR	; set for a power mode
	OR s0, ONEWIRE_A_OP	; set for an op
	OUTPUT s0, ONEWIRE_PORT
onewire_pwr_A_poll:
	;; wait for this op to finish
	INPUT s0, ONEWIRE_PORT
	TEST s0, ONEWIRE_A_OP
	JUMP Z, onewire_pwr_A_poll
	;; finished
	RETURN
onewire_pwr_B_init:
	;; make sure the last op finishes
	INPUT s0, ONEWIRE_PORT
	TEST s0, ONEWIRE_B_OP 	;wait for ready to be a '1'
	JUMP Z, onewire_pwr_B_init
	;; pwr op
	LOAD s0, ONEWIRE_B_PWR	; set for a power mode
	OR s0, ONEWIRE_B_OP	; set for an op
	OUTPUT s0, ONEWIRE_PORT
onewire_pwr_B_poll:
	;; wait for this op to finish
	INPUT s0, ONEWIRE_PORT
	TEST s0, ONEWIRE_B_OP
	JUMP Z, onewire_pwr_B_poll

	;; finished
	RETURN

;;; ============================================================================
;;; Reset the bus and see if anyone is there
;;; input:
;;; 	s0, bus, A(0) B(1)
;;; 	s1 data to write
;;; ============================================================================
onewire_write_byte:
	
	TEST s0, 01
	JUMP C, onewire_write_byte_B

onewire_write_byte_A:
	LOAD s2, 80		;begin bit counter
onewire_write_byte_A_loop:
	LOAD s0,00
	CALL onewire_write_bit	;write LSB of s1
	SR0 s2			;count down 
	JUMP NC, onewire_write_byte_A_loop
	;; finish
	JUMP onewire_write_byte_end
onewire_write_byte_B:
	LOAD s2, 80		;begin bit counter
onewire_write_byte_B_loop:
	LOAD s0,01
	CALL onewire_write_bit	;write LSB of s1
	SR0 s2			;count down 
	JUMP NC, onewire_write_byte_B_loop
	;; finish
	JUMP onewire_write_byte_end
onewire_write_byte_end:
	RETURN
	


;;; ============================================================================
;;; Reset the bus and see if anyone is there
;;; input:
;;; 	s0, bus, A(0) B(1)
;;; output:
;;; 	s1 returned byte
;;; ============================================================================
onewire_read_byte:	
	TEST s0, 01
	JUMP C, onewire_read_byte_B

	LOAD s1, 00		;zero input word
	LOAD s2, 80		;begin bit counter
onewire_read_byte_A_poll_1:
	LOAD s0, 00
	CALL onewire_read_bit
	SRA s1			;shift in bit to the top

	SR0 s2			;count down
	JUMP NC, onewire_read_byte_A_poll_1
	;; finish
	JUMP onewire_read_byte_end

onewire_read_byte_B:	
	LOAD s1, 00		;zero input word
	LOAD s2, 80		;begin bit counter
onewire_read_byte_B_poll_1:
	LOAD s0, 01
	CALL onewire_read_bit
	SRA s1			;shift in bit to the top
	
	SR0 s2			;count down
	JUMP NC, onewire_read_byte_B_poll_1
	;; finish
	JUMP onewire_read_byte_end

onewire_read_byte_end:	
	RETURN

	
;;; ============================================================================
;;; read one bit from the bus
;;; input:
;;; 	s0, bus, A(0) B(1)
;;; output:
;;; 	bit in C
;;; ============================================================================
onewire_read_bit:
	TEST s0, 01
	JUMP C, onewire_read_bit_B

onewire_read_bit_A:	
	INPUT s0, ONEWIRE_PORT
	TEST s0, ONEWIRE_A_OP	;wait for op finish
	JUMP Z, onewire_read_bit_A

	LOAD s0, 00		;clear s0
	OR s0, ONEWIRE_A_OP	;go
	OR s0, ONEWIRE_A_READ	; set read
	OUTPUT s0, ONEWIRE_PORT	;do the read

onewire_read_bit_A_poll:	;wait for op to register
	INPUT s0, ONEWIRE_PORT	;delay for finished bit to reset
	INPUT s0, ONEWIRE_PORT  ;delay for finished bit to reset
	INPUT s0, ONEWIRE_PORT	
	TEST s0, ONEWIRE_A_OP	;test for not done
	JUMP Z, onewire_read_bit_A_poll
	
	SR0 s0			;shift read bit into C
	RETURN

onewire_read_bit_B:	
	INPUT s0, ONEWIRE_PORT
	TEST s0, ONEWIRE_B_OP	;wait for op finish
	JUMP Z, onewire_read_bit_B

	LOAD s0, 00		;clear s0
	OR s0, ONEWIRE_B_OP	;go
	OR s0, ONEWIRE_B_READ	; set read
	OUTPUT s0, ONEWIRE_PORT	;do the read

onewire_read_bit_B_poll:	;wait for op to register
	INPUT s0, ONEWIRE_PORT	;delay for finished bit to reset
	INPUT s0, ONEWIRE_PORT  ;delay for finished bit to reset
	INPUT s0, ONEWIRE_PORT	
	TEST s0, ONEWIRE_B_OP	;test for not done
	JUMP Z, onewire_read_bit_B_poll
	
	SR0 s0			;shift bit into LSB (it is in bit 4)
	SR0 s0
	SR0 s0
	SR0 s0
	
	SR0 s0			;shift read bit into C
	RETURN


;;; ============================================================================
;;; write one bit to the bus
;;; input:
;;; 	s0, bus, A(0) B(1)
;;; 	s1, LSB to write (will shift register)
;;; ============================================================================
onewire_write_bit:
	TEST s0, 01
	JUMP C, onewire_write_bit_B

onewire_write_bit_A:	
	INPUT s0, ONEWIRE_PORT
	TEST s0, ONEWIRE_A_OP	;wait for op finish
	JUMP Z, onewire_write_bit_A

	LOAD s0, 00		;clear s0
	SR0 s1			; load bit from s1
	SLA s0			; move bit into s0
	OR s0, ONEWIRE_A_OP	;go
	OR s0, ONEWIRE_A_WRITE	; set write
	OUTPUT s0, ONEWIRE_PORT	;do the write

onewire_write_bit_A_poll:	;wait for op to register
	INPUT s0, ONEWIRE_PORT	;delay for finished bit to reset
	INPUT s0, ONEWIRE_PORT  ;delay for finished bit to reset
	INPUT s0, ONEWIRE_PORT	
	TEST s0, ONEWIRE_A_OP	;test for not done
	JUMP Z, onewire_write_bit_A_poll
	
	RETURN

onewire_write_bit_B:	
	INPUT s0, ONEWIRE_PORT
	TEST s0, ONEWIRE_B_OP	;wait for op finish
	JUMP Z, onewire_write_bit_B

	LOAD s0, 00		;clear s0
	SR0 s1			; load bit from s1
	SLA s0			; move bit into s0
	SL0 s0			; shift up to bit 4
	SL0 s0
	SL0 s0
	Sl0 s0
	OR s0, ONEWIRE_B_OP	;go
	OR s0, ONEWIRE_B_WRITE	; set write
	OUTPUT s0, ONEWIRE_PORT	;do the write

onewire_write_bit_B_poll:	;wait for op to register
	INPUT s0, ONEWIRE_PORT	;delay for finished bit to reset
	INPUT s0, ONEWIRE_PORT  ;delay for finished bit to reset
	INPUT s0, ONEWIRE_PORT	
	TEST s0, ONEWIRE_B_OP	;test for not done
	JUMP Z, onewire_write_bit_B_poll
	
	RETURN


;;; ============================================================================
;;; onewire_search
;;; input:
;;; 	s2, bus, A(0) B(1)
;;; ============================================================================
onewire_search:	
	LOAD s7, 00		;last discrepancy
	CALL CMD_TEMP_ROM_ZERO

onewire_search_start:	
	LOAD s0,s2
	;; call init and check that someone is there
	CALL onewire_presence
	JUMP NC, CMD_GET_TEMP_no_device
	
	LOAD s3, 01		;id_bit_number
	LOAD s4, 00		;last_zero
	
	;; start search
	LOAD s0, s2
	LOAD s1, F0
	CALL onewire_write_byte
onewire_search_bit:
	LOAD s1, 00
	;; read and of all sensor addresses for this bit
	LOAD s0,s2 		;bus number
	CALL onewire_read_bit
	SLA s1			;store in s1
	;; read and of all sensor compliment addresses for this bit
	LOAD s0,s2		;bus number
	CALL onewire_read_bit
	SLA s1
	
	COMPARE s1, 03
	JUMP Z, onewire_search_LAST_DEVICE
	COMPARE s1, 00
	JUMP Z, onewire_search_SET_DIR

	;; no collision, use bit 1 of s1 as addr
	LOAD s6, 00
	SR0 s1			;shift off compliment bit
	SR0 s1			;shift off bit
	SLA s6			;shift in bit s6
	JUMP onewire_search_SAVE_BIT

onewire_search_SET_DIR:
	
	COMPARE s7,s3		; lastdiscrepancy == id_bit_number?
	JUMP NZ, onewire_search_SET_DIR_NOT_EQUAL
	;; match
	LOAD s6,01
	JUMP onewire_search_SAVE_BIT
onewire_search_SET_DIR_NOT_EQUAL:
	COMPARE s7,s3			     ;check if last discrepancy is less than index
	JUMP C, onewire_search_SET_DIR_LESS_THAN ;jump if id_bit is greater than last discrepancy
	
	LOAD s6,00	
	CALL CMD_TEMP_ROM_RD
	SLA s6			;set bit to last search value
	;; check if the bit is zero
	COMPARE s6,00
	JUMP NZ, onewire_search_SAVE_BIT
	;; update last zero if it is
	LOAD s4,s3
	JUMP NZ, onewire_search_SAVE_BIT


onewire_search_SET_DIR_LESS_THAN:
	LOAD s6,00			     ;set bit to zero
	;; update last zero
	LOAD s4,s3
	JUMP onewire_search_SAVE_BIT
	
onewire_search_SAVE_BIT:
	;; save bit into ROM
	LOAD s1, s6		;buffer bit

	;; write bit
	LOAD s0,s2 		;bus number
	CALL onewire_write_bit 	;uses s1

	;; save bit to ROM addr
	LOAD s1,s6
	CALL CMD_TEMP_ROM_WR
		
	;; move to the next bit
	ADD s3,01

	
	COMPARE s3,41		;if less than or equal to bit 64 (0x41), keep going
	JUMP NZ, onewire_search_bit
	LOAD s7,s4		;set lst discrepancy to last zero
	
	COMPARE s7,00
	JUMP NZ, onewire_search_STORE
	;; we are at the end, print and return
;	CALL util_print_EOL
	LOAD sA,s2
	CALL C5_STORE_ADDRESS
	JUMP onewire_search_END
		

onewire_search_LAST_DEVICE:
	JUMP onewire_search_END


onewire_search_STORE:
	;; save s2,s3
	LOAD sA,s2
	LOAD sB,s3
	CALL C5_STORE_ADDRESS
	LOAD s2,sA
	LOAD s3,sB

	;; power up
	LOAD s0,s2
	CALL onewire_pwr
	
	JUMP onewire_search_start	

onewire_search_END:	
	RETURN
