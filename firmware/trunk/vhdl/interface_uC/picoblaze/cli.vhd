--
-------------------------------------------------------------------------------------------
-- Copyright � 2010-2013, Xilinx, Inc.
-- This file contains confidential and proprietary information of Xilinx, Inc. and is
-- protected under U.S. and international copyright and other intellectual property laws.
-------------------------------------------------------------------------------------------
--
-- Disclaimer:
-- This disclaimer is not a license and does not grant any rights to the materials
-- distributed herewith. Except as otherwise provided in a valid license issued to
-- you by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE
-- MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY
-- DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
-- INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT,
-- OR FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable
-- (whether in contract or tort, including negligence, or under any other theory
-- of liability) for any loss or damage of any kind or nature related to, arising
-- under or in connection with these materials, including for any direct, or any
-- indirect, special, incidental, or consequential loss or damage (including loss
-- of data, profits, goodwill, or any type of loss or damage suffered as a result
-- of any action brought by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-safe, or for use in any
-- application requiring fail-safe performance, such as life-support or safety
-- devices or systems, Class III medical devices, nuclear facilities, applications
-- related to the deployment of airbags, or any other applications that could lead
-- to death, personal injury, or severe property or environmental damage
-- (individually and collectively, "Critical Applications"). Customer assumes the
-- sole risk and liability of any use of Xilinx products in Critical Applications,
-- subject only to applicable laws and regulations governing limitations on product
-- liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
-------------------------------------------------------------------------------------------
--
--
-- Definition of a program memory for KCPSM6 including generic parameters for the 
-- convenient selection of device family, program memory size and the ability to include 
-- the JTAG Loader hardware for rapid software development.
--
-- This file is primarily for use during code development and it is recommended that the 
-- appropriate simplified program memory definition be used in a final production design. 
--
--    Generic                  Values             Comments
--    Parameter                Supported
--  
--    C_FAMILY                 "S6"               Spartan-6 device
--                             "V6"               Virtex-6 device
--                             "7S"               7-Series device 
--                                                  (Artix-7, Kintex-7, Virtex-7 or Zynq)
--
--    C_RAM_SIZE_KWORDS        1, 2 or 4          Size of program memory in K-instructions
--
--    C_JTAG_LOADER_ENABLE     0 or 1             Set to '1' to include JTAG Loader
--
-- Notes
--
-- If your design contains MULTIPLE KCPSM6 instances then only one should have the 
-- JTAG Loader enabled at a time (i.e. make sure that C_JTAG_LOADER_ENABLE is only set to 
-- '1' on one instance of the program memory). Advanced users may be interested to know 
-- that it is possible to connect JTAG Loader to multiple memories and then to use the 
-- JTAG Loader utility to specify which memory contents are to be modified. However, 
-- this scheme does require some effort to set up and the additional connectivity of the 
-- multiple BRAMs can impact the placement, routing and performance of the complete 
-- design. Please contact the author at Xilinx for more detailed information. 
--
-- Regardless of the size of program memory specified by C_RAM_SIZE_KWORDS, the complete 
-- 12-bit address bus is connected to KCPSM6. This enables the generic to be modified 
-- without requiring changes to the fundamental hardware definition. However, when the 
-- program memory is 1K then only the lower 10-bits of the address are actually used and 
-- the valid address range is 000 to 3FF hex. Likewise, for a 2K program only the lower 
-- 11-bits of the address are actually used and the valid address range is 000 to 7FF hex.
--
-- Programs are stored in Block Memory (BRAM) and the number of BRAM used depends on the 
-- size of the program and the device family. 
--
-- In a Spartan-6 device a BRAM is capable of holding 1K instructions. Hence a 2K program 
-- will require 2 BRAMs to be used and a 4K program will require 4 BRAMs to be used. It 
-- should be noted that a 4K program is not such a natural fit in a Spartan-6 device and 
-- the implementation also requires a small amount of logic resulting in slightly lower 
-- performance. A Spartan-6 BRAM can also be split into two 9k-bit memories suggesting 
-- that a program containing up to 512 instructions could be implemented. However, there 
-- is a silicon errata which makes this unsuitable and therefore it is not supported by 
-- this file.
--
-- In a Virtex-6 or any 7-Series device a BRAM is capable of holding 2K instructions so 
-- obviously a 2K program requires only a single BRAM. Each BRAM can also be divided into 
-- 2 smaller memories supporting programs of 1K in half of a 36k-bit BRAM (generally 
-- reported as being an 18k-bit BRAM). For a program of 4K instructions, 2 BRAMs are used.
--
--
-- Program defined by 'Z:\home\dan\work\g-2\NewElectronics\LogicBoard\firmware\trunk\vhdl\interface_uC\picoblaze\cli.psm'.
--
-- Generated by KCPSM6 Assembler: 08 Sep 2017 - 13:51:57. 
--
-- Assembler used ROM_form template: ROM_form_JTAGLoader_14March13.vhd
--
-- Standard IEEE libraries
--
--
package jtag_loader_pkg is
 function addr_width_calc (size_in_k: integer) return integer;
end jtag_loader_pkg;
--
package body jtag_loader_pkg is
  function addr_width_calc (size_in_k: integer) return integer is
   begin
    if (size_in_k = 1) then return 10;
      elsif (size_in_k = 2) then return 11;
      elsif (size_in_k = 4) then return 12;
      else report "Invalid BlockRAM size. Please set to 1, 2 or 4 K words." severity FAILURE;
    end if;
    return 0;
  end function addr_width_calc;
end package body;
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.jtag_loader_pkg.ALL;
--
-- The Unisim Library is used to define Xilinx primitives. It is also used during
-- simulation. The source can be viewed at %XILINX%\vhdl\src\unisims\unisim_VCOMP.vhd
--  
library unisim;
use unisim.vcomponents.all;
--
--
entity cli is
  generic(             C_FAMILY : string := "S6"; 
              C_RAM_SIZE_KWORDS : integer := 1;
           C_JTAG_LOADER_ENABLE : integer := 0);
  Port (      address : in std_logic_vector(11 downto 0);
          instruction : out std_logic_vector(17 downto 0);
               enable : in std_logic;
                  rdl : out std_logic;                    
                  clk : in std_logic);
  end cli;
--
architecture low_level_definition of cli is
--
signal       address_a : std_logic_vector(15 downto 0);
signal        pipe_a11 : std_logic;
signal       data_in_a : std_logic_vector(35 downto 0);
signal      data_out_a : std_logic_vector(35 downto 0);
signal    data_out_a_l : std_logic_vector(35 downto 0);
signal    data_out_a_h : std_logic_vector(35 downto 0);
signal   data_out_a_ll : std_logic_vector(35 downto 0);
signal   data_out_a_lh : std_logic_vector(35 downto 0);
signal   data_out_a_hl : std_logic_vector(35 downto 0);
signal   data_out_a_hh : std_logic_vector(35 downto 0);
signal       address_b : std_logic_vector(15 downto 0);
signal       data_in_b : std_logic_vector(35 downto 0);
signal     data_in_b_l : std_logic_vector(35 downto 0);
signal    data_in_b_ll : std_logic_vector(35 downto 0);
signal    data_in_b_hl : std_logic_vector(35 downto 0);
signal      data_out_b : std_logic_vector(35 downto 0);
signal    data_out_b_l : std_logic_vector(35 downto 0);
signal   data_out_b_ll : std_logic_vector(35 downto 0);
signal   data_out_b_hl : std_logic_vector(35 downto 0);
signal     data_in_b_h : std_logic_vector(35 downto 0);
signal    data_in_b_lh : std_logic_vector(35 downto 0);
signal    data_in_b_hh : std_logic_vector(35 downto 0);
signal    data_out_b_h : std_logic_vector(35 downto 0);
signal   data_out_b_lh : std_logic_vector(35 downto 0);
signal   data_out_b_hh : std_logic_vector(35 downto 0);
signal        enable_b : std_logic;
signal           clk_b : std_logic;
signal            we_b : std_logic_vector(7 downto 0);
signal          we_b_l : std_logic_vector(3 downto 0);
signal          we_b_h : std_logic_vector(3 downto 0);
-- 
signal       jtag_addr : std_logic_vector(11 downto 0);
signal         jtag_we : std_logic;
signal       jtag_we_l : std_logic;
signal       jtag_we_h : std_logic;
signal        jtag_clk : std_logic;
signal        jtag_din : std_logic_vector(17 downto 0);
signal       jtag_dout : std_logic_vector(17 downto 0);
signal     jtag_dout_1 : std_logic_vector(17 downto 0);
signal         jtag_en : std_logic_vector(0 downto 0);
-- 
signal picoblaze_reset : std_logic_vector(0 downto 0);
signal         rdl_bus : std_logic_vector(0 downto 0);
--
constant BRAM_ADDRESS_WIDTH  : integer := addr_width_calc(C_RAM_SIZE_KWORDS);
--
--
component jtag_loader_6
generic(                C_JTAG_LOADER_ENABLE : integer := 1;
                                    C_FAMILY : string  := "V6";
                             C_NUM_PICOBLAZE : integer := 1;
                       C_BRAM_MAX_ADDR_WIDTH : integer := 10;
          C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                                C_JTAG_CHAIN : integer := 2;
                              C_ADDR_WIDTH_0 : integer := 10;
                              C_ADDR_WIDTH_1 : integer := 10;
                              C_ADDR_WIDTH_2 : integer := 10;
                              C_ADDR_WIDTH_3 : integer := 10;
                              C_ADDR_WIDTH_4 : integer := 10;
                              C_ADDR_WIDTH_5 : integer := 10;
                              C_ADDR_WIDTH_6 : integer := 10;
                              C_ADDR_WIDTH_7 : integer := 10);
port(              picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                           jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                          jtag_din : out STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                         jtag_addr : out STD_LOGIC_VECTOR(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
                          jtag_clk : out std_logic;
                           jtag_we : out std_logic;
                       jtag_dout_0 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_1 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_2 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_3 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_4 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_5 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_6 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_7 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end component;
--
begin
  --
  --  
  ram_1k_generate : if (C_RAM_SIZE_KWORDS = 1) generate
 
    s6: if (C_FAMILY = "S6") generate 
      --
      address_a(13 downto 0) <= address(9 downto 0) & "0000";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "0000000000000000000000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "0000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB16BWER
      generic map ( DATA_WIDTH_A => 18,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 18,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D00490002CF00142",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"A050159045064506450645060530D00110200002D001103A0002005300305000",
                    INIT_09 => X"0053A05095010053A0501508D001102000020053A0501501D001102000020053",
                    INIT_0A => X"95010053A05095010053A05095010053A05095010053A05095010053A0509501",
                    INIT_0B => X"3D0FBD801C40500020B713010081E0BCC3401300340FB480500000590053A050",
                    INIT_0C => X"9E011D011C01E0D0A0C01E071D01EAD01D01E0D010C01D904D064D064D064D06",
                    INIT_0D => X"4206420642064206B281500014011201E340A3205000FD801D013D0FBD80E0CB",
                    INIT_0E => X"143000D600D600D600D614341201F32AF3291308F32CA3201201F32313031290",
                    INIT_0F => X"5000E12031EF5120A120920BE1201201E420025007B0052000D600D600D600D6",
                    INIT_10 => X"1301082F14011300F0805080F081300FB080068C1201068C1200F081F0801000",
                    INIT_11 => X"211DD002900B062411441000062411CC1000130205EF100013005000082F1401",
                    INIT_12 => X"B18150000100A12FD080B0805000F08110002123C0302030900BD00B5002100C",
                    INIT_13 => X"1000D001900E5000F0811001B08100DB50000113F08110006138C100310F300F",
                    INIT_14 => X"10005000006001FBE1489201E148930112FF13FFB001B03103FF014F5000012A",
                    INIT_15 => X"0002005300410030130050000059E100B130B02C607CD002B0235000F023F020",
                    INIT_16 => X"D00030030030D1010002D20100020041A03093011304D00110200002D001103A",
                    INIT_17 => X"0C750210B12BB227A182D004B023023F50000059215C2176D300130400596166",
                    INIT_18 => X"97089607950694052189D010900331FFD1031108D004500001425000023F0CC4",
                    INIT_19 => X"1C2C1D01607CD00121A2D002B0235000D10311F4D708D607D506D405D0045000",
                    INIT_1A => X"10780002D00110300002018500C01D0161A9DD00ADD01D30ACC01C2C21A9ACC0",
                    INIT_1B => X"0002005300700002D00110780002D00110300002D00110200002005300C0D001",
                    INIT_1C => X"607CD002B023500061A99D011C01005900530040000200530050000200530060",
                    INIT_1D => X"50000191A000102C0710A10010010610A10010010510A10010010410A1001030",
                    INIT_1E => X"11691167116F114C112011721165116B116311611172115411201132112D1167",
                    INIT_1F => X"0185100100641AE01B0111001120113A11561120116411721161116F11421163",
                    INIT_20 => X"0040D001102E000200530050D001102E000200530060D001102E000200530070",
                    INIT_21 => X"11641172116F115711001120113A117211651166116611751142500000590053",
                    INIT_22 => X"11691173112011641172116F115711001120113A1174116E1175116F11631120",
                    INIT_23 => X"1B021100112011641172116F115711001120112011201120113A11731165117A",
                    INIT_24 => X"10280002D0011020000222441101D001A0100002E24BC120B220110000641A13",
                    INIT_25 => X"B023000200641A1C1B020059D00110290002D1010002D201000200410020D001",
                    INIT_26 => X"1124D00110400002D00110200002E279C3401300B423D1010002D20100020041",
                    INIT_27 => X"C3401300B42300641A291B02005922671301D1010002D20100020041A0100130",
                    INIT_28 => X"B4230059227F1301D1010002D20100020041A01001301128D00110200002E28E",
                    INIT_29 => X"D00110200002D1010002D20100020041003000641A391B020059E2B1C3401300",
                    INIT_2A => X"9601D1010002D20100020041A060A291C65016030650152C4506450613010530",
                    INIT_2B => X"005922B53B001A01D101000222C3D1FF22BFD1004BA01AB11B0A5000005922A7",
                    INIT_2C => X"1E3E50007000DD024D003DF0400740073003700160005000005922B53B001A03",
                    INIT_2D => X"5D025000DD023DFE02CF02CF02CF02CFDD023DFDDD025D025D015000E2D09E01",
                    INIT_2E => X"02CF02CFDD023DFE02CF02CF02CFDD023DFD02CF02CFDD025D0102CF02CFDD02",
                    INIT_2F => X"5000DD025D0202CF02CF02CF02CFDD025D0102CF02CF02CFDD023DFD500002CF",
                    INIT_30 => X"02CFDD023DFD500002CF02CF02CFDD023DFE02CF02CFDD025D01DD025D0202CF",
                    INIT_31 => X"A323440602CF1580500002CF02CFDD025D0202CFDD023DFE02CF02CFDD025D01",
                    INIT_32 => X"02CF231DA32F450EDD023DFE02CF02CFDD025D0102CFDD025D022325DD023DFD",
                    INIT_33 => X"5D025000D50202CF02CF02CFDD023DFE02CF02CFDD025D01950202CFDD025D02",
                    INIT_34 => X"02CFE342450EDD023DFE02CF02CFDD025D0102CF4400D602960202CF15801400",
                    INIT_35 => X"031C0420A36B031C0410A369031C040002D330FE700163306220611060005000",
                    INIT_36 => X"1004237110032371100223711001500070004006100002F2A36F031C0430A36D",
                    INIT_37 => X"A38B031C040002D330FE7001622061106000500070004006108002F200532371",
                    INIT_38 => X"10032391100223911001500070004006100002F2A38F031C0420A38D031C0410",
                    INIT_39 => X"0410A3B4031C040002D330FE700161106000500070004006108002F200532391",
                    INIT_3A => X"4006100002F203000340033F030D0240033FA3B8031C0400500102DFA36B031C",
                    INIT_3B => X"500070004006108002F2005323BA100323BA100223BA10015000700063306220",
                    INIT_3C => X"5000005900641AC01B031100112E1172116F1172117211651120116B11631141",
                    INIT_3D => X"D503A2401401A340A3DED503A550152A1434A1401430A040142C607CD003B023",
                    INIT_3E => X"A3CB0397A1401430A040142C607CD002B0235000A3CB03775000A3CB0351A3E4",
                    INIT_3F => X"10F3500002C51002500002C51001500002C51000500000590053003000530020",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"A01988AA2A86886155258A4356066155029B40A8618618618618A18A155228A2",
                   INITP_02 => X"90D2AB5B42AA1628A82A0D2A2ADD0AAAC292A8D02B0A3020C208082820882228",
                   INITP_03 => X"882AAAAAAAAAAAAAA0041040D2D68A28A28A28A228A0D0080DD28AAA00C08AAA",
                   INITP_04 => X"420A6AA1228B42AA2828AAA228A62D082AAAAAAAAAAAAAAAAAAAAAAA2288A228",
                   INITP_05 => X"A2AA2A8AA8AA2A2A28AA882D2E0536A5A5ADD82A6AA351548AAA20B429AA848B",
                   INITP_06 => X"E2356D2A2222D2E38E388D56B62A242022A8A828AD8A8A28D8AA28A8A2AA2A22",
                   INITP_07 => X"28A28A88E00D2EBB44D00034A82AAAAAB4A888B54A28B82E388D6D2A222D2E38")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a(31 downto 0),
                  DOPA => data_out_a(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b(31 downto 0),
                  DOPB => data_out_b(35 downto 32), 
                   DIB => data_in_b(31 downto 0),
                  DIPB => data_in_b(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --               
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D00490002CF00142",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"A050159045064506450645060530D00110200002D001103A0002005300305000",
                    INIT_09 => X"0053A05095010053A0501508D001102000020053A0501501D001102000020053",
                    INIT_0A => X"95010053A05095010053A05095010053A05095010053A05095010053A0509501",
                    INIT_0B => X"3D0FBD801C40500020B713010081E0BCC3401300340FB480500000590053A050",
                    INIT_0C => X"9E011D011C01E0D0A0C01E071D01EAD01D01E0D010C01D904D064D064D064D06",
                    INIT_0D => X"4206420642064206B281500014011201E340A3205000FD801D013D0FBD80E0CB",
                    INIT_0E => X"143000D600D600D600D614341201F32AF3291308F32CA3201201F32313031290",
                    INIT_0F => X"5000E12031EF5120A120920BE1201201E420025007B0052000D600D600D600D6",
                    INIT_10 => X"1301082F14011300F0805080F081300FB080068C1201068C1200F081F0801000",
                    INIT_11 => X"211DD002900B062411441000062411CC1000130205EF100013005000082F1401",
                    INIT_12 => X"B18150000100A12FD080B0805000F08110002123C0302030900BD00B5002100C",
                    INIT_13 => X"1000D001900E5000F0811001B08100DB50000113F08110006138C100310F300F",
                    INIT_14 => X"10005000006001FBE1489201E148930112FF13FFB001B03103FF014F5000012A",
                    INIT_15 => X"0002005300410030130050000059E100B130B02C607CD002B0235000F023F020",
                    INIT_16 => X"D00030030030D1010002D20100020041A03093011304D00110200002D001103A",
                    INIT_17 => X"0C750210B12BB227A182D004B023023F50000059215C2176D300130400596166",
                    INIT_18 => X"97089607950694052189D010900331FFD1031108D004500001425000023F0CC4",
                    INIT_19 => X"1C2C1D01607CD00121A2D002B0235000D10311F4D708D607D506D405D0045000",
                    INIT_1A => X"10780002D00110300002018500C01D0161A9DD00ADD01D30ACC01C2C21A9ACC0",
                    INIT_1B => X"0002005300700002D00110780002D00110300002D00110200002005300C0D001",
                    INIT_1C => X"607CD002B023500061A99D011C01005900530040000200530050000200530060",
                    INIT_1D => X"50000191A000102C0710A10010010610A10010010510A10010010410A1001030",
                    INIT_1E => X"11691167116F114C112011721165116B116311611172115411201132112D1167",
                    INIT_1F => X"0185100100641AE01B0111001120113A11561120116411721161116F11421163",
                    INIT_20 => X"0040D001102E000200530050D001102E000200530060D001102E000200530070",
                    INIT_21 => X"11641172116F115711001120113A117211651166116611751142500000590053",
                    INIT_22 => X"11691173112011641172116F115711001120113A1174116E1175116F11631120",
                    INIT_23 => X"1B021100112011641172116F115711001120112011201120113A11731165117A",
                    INIT_24 => X"10280002D0011020000222441101D001A0100002E24BC120B220110000641A13",
                    INIT_25 => X"B023000200641A1C1B020059D00110290002D1010002D201000200410020D001",
                    INIT_26 => X"1124D00110400002D00110200002E279C3401300B423D1010002D20100020041",
                    INIT_27 => X"C3401300B42300641A291B02005922671301D1010002D20100020041A0100130",
                    INIT_28 => X"B4230059227F1301D1010002D20100020041A01001301128D00110200002E28E",
                    INIT_29 => X"D00110200002D1010002D20100020041003000641A391B020059E2B1C3401300",
                    INIT_2A => X"9601D1010002D20100020041A060A291C65016030650152C4506450613010530",
                    INIT_2B => X"005922B53B001A01D101000222C3D1FF22BFD1004BA01AB11B0A5000005922A7",
                    INIT_2C => X"1E3E50007000DD024D003DF0400740073003700160005000005922B53B001A03",
                    INIT_2D => X"5D025000DD023DFE02CF02CF02CF02CFDD023DFDDD025D025D015000E2D09E01",
                    INIT_2E => X"02CF02CFDD023DFE02CF02CF02CFDD023DFD02CF02CFDD025D0102CF02CFDD02",
                    INIT_2F => X"5000DD025D0202CF02CF02CF02CFDD025D0102CF02CF02CFDD023DFD500002CF",
                    INIT_30 => X"02CFDD023DFD500002CF02CF02CFDD023DFE02CF02CFDD025D01DD025D0202CF",
                    INIT_31 => X"A323440602CF1580500002CF02CFDD025D0202CFDD023DFE02CF02CFDD025D01",
                    INIT_32 => X"02CF231DA32F450EDD023DFE02CF02CFDD025D0102CFDD025D022325DD023DFD",
                    INIT_33 => X"5D025000D50202CF02CF02CFDD023DFE02CF02CFDD025D01950202CFDD025D02",
                    INIT_34 => X"02CFE342450EDD023DFE02CF02CFDD025D0102CF4400D602960202CF15801400",
                    INIT_35 => X"031C0420A36B031C0410A369031C040002D330FE700163306220611060005000",
                    INIT_36 => X"1004237110032371100223711001500070004006100002F2A36F031C0430A36D",
                    INIT_37 => X"A38B031C040002D330FE7001622061106000500070004006108002F200532371",
                    INIT_38 => X"10032391100223911001500070004006100002F2A38F031C0420A38D031C0410",
                    INIT_39 => X"0410A3B4031C040002D330FE700161106000500070004006108002F200532391",
                    INIT_3A => X"4006100002F203000340033F030D0240033FA3B8031C0400500102DFA36B031C",
                    INIT_3B => X"500070004006108002F2005323BA100323BA100223BA10015000700063306220",
                    INIT_3C => X"5000005900641AC01B031100112E1172116F1172117211651120116B11631141",
                    INIT_3D => X"D503A2401401A340A3DED503A550152A1434A1401430A040142C607CD003B023",
                    INIT_3E => X"A3CB0397A1401430A040142C607CD002B0235000A3CB03775000A3CB0351A3E4",
                    INIT_3F => X"10F3500002C51002500002C51001500002C51000500000590053003000530020",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"A01988AA2A86886155258A4356066155029B40A8618618618618A18A155228A2",
                   INITP_02 => X"90D2AB5B42AA1628A82A0D2A2ADD0AAAC292A8D02B0A3020C208082820882228",
                   INITP_03 => X"882AAAAAAAAAAAAAA0041040D2D68A28A28A28A228A0D0080DD28AAA00C08AAA",
                   INITP_04 => X"420A6AA1228B42AA2828AAA228A62D082AAAAAAAAAAAAAAAAAAAAAAA2288A228",
                   INITP_05 => X"A2AA2A8AA8AA2A2A28AA882D2E0536A5A5ADD82A6AA351548AAA20B429AA848B",
                   INITP_06 => X"E2356D2A2222D2E38E388D56B62A242022A8A828AD8A8A28D8AA28A8A2AA2A22",
                   INITP_07 => X"28A28A88E00D2EBB44D00034A82AAAAAB4A888B54A28B82E388D6D2A222D2E38")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D00490002CF00142",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"A050159045064506450645060530D00110200002D001103A0002005300305000",
                    INIT_09 => X"0053A05095010053A0501508D001102000020053A0501501D001102000020053",
                    INIT_0A => X"95010053A05095010053A05095010053A05095010053A05095010053A0509501",
                    INIT_0B => X"3D0FBD801C40500020B713010081E0BCC3401300340FB480500000590053A050",
                    INIT_0C => X"9E011D011C01E0D0A0C01E071D01EAD01D01E0D010C01D904D064D064D064D06",
                    INIT_0D => X"4206420642064206B281500014011201E340A3205000FD801D013D0FBD80E0CB",
                    INIT_0E => X"143000D600D600D600D614341201F32AF3291308F32CA3201201F32313031290",
                    INIT_0F => X"5000E12031EF5120A120920BE1201201E420025007B0052000D600D600D600D6",
                    INIT_10 => X"1301082F14011300F0805080F081300FB080068C1201068C1200F081F0801000",
                    INIT_11 => X"211DD002900B062411441000062411CC1000130205EF100013005000082F1401",
                    INIT_12 => X"B18150000100A12FD080B0805000F08110002123C0302030900BD00B5002100C",
                    INIT_13 => X"1000D001900E5000F0811001B08100DB50000113F08110006138C100310F300F",
                    INIT_14 => X"10005000006001FBE1489201E148930112FF13FFB001B03103FF014F5000012A",
                    INIT_15 => X"0002005300410030130050000059E100B130B02C607CD002B0235000F023F020",
                    INIT_16 => X"D00030030030D1010002D20100020041A03093011304D00110200002D001103A",
                    INIT_17 => X"0C750210B12BB227A182D004B023023F50000059215C2176D300130400596166",
                    INIT_18 => X"97089607950694052189D010900331FFD1031108D004500001425000023F0CC4",
                    INIT_19 => X"1C2C1D01607CD00121A2D002B0235000D10311F4D708D607D506D405D0045000",
                    INIT_1A => X"10780002D00110300002018500C01D0161A9DD00ADD01D30ACC01C2C21A9ACC0",
                    INIT_1B => X"0002005300700002D00110780002D00110300002D00110200002005300C0D001",
                    INIT_1C => X"607CD002B023500061A99D011C01005900530040000200530050000200530060",
                    INIT_1D => X"50000191A000102C0710A10010010610A10010010510A10010010410A1001030",
                    INIT_1E => X"11691167116F114C112011721165116B116311611172115411201132112D1167",
                    INIT_1F => X"0185100100641AE01B0111001120113A11561120116411721161116F11421163",
                    INIT_20 => X"0040D001102E000200530050D001102E000200530060D001102E000200530070",
                    INIT_21 => X"11641172116F115711001120113A117211651166116611751142500000590053",
                    INIT_22 => X"11691173112011641172116F115711001120113A1174116E1175116F11631120",
                    INIT_23 => X"1B021100112011641172116F115711001120112011201120113A11731165117A",
                    INIT_24 => X"10280002D0011020000222441101D001A0100002E24BC120B220110000641A13",
                    INIT_25 => X"B023000200641A1C1B020059D00110290002D1010002D201000200410020D001",
                    INIT_26 => X"1124D00110400002D00110200002E279C3401300B423D1010002D20100020041",
                    INIT_27 => X"C3401300B42300641A291B02005922671301D1010002D20100020041A0100130",
                    INIT_28 => X"B4230059227F1301D1010002D20100020041A01001301128D00110200002E28E",
                    INIT_29 => X"D00110200002D1010002D20100020041003000641A391B020059E2B1C3401300",
                    INIT_2A => X"9601D1010002D20100020041A060A291C65016030650152C4506450613010530",
                    INIT_2B => X"005922B53B001A01D101000222C3D1FF22BFD1004BA01AB11B0A5000005922A7",
                    INIT_2C => X"1E3E50007000DD024D003DF0400740073003700160005000005922B53B001A03",
                    INIT_2D => X"5D025000DD023DFE02CF02CF02CF02CFDD023DFDDD025D025D015000E2D09E01",
                    INIT_2E => X"02CF02CFDD023DFE02CF02CF02CFDD023DFD02CF02CFDD025D0102CF02CFDD02",
                    INIT_2F => X"5000DD025D0202CF02CF02CF02CFDD025D0102CF02CF02CFDD023DFD500002CF",
                    INIT_30 => X"02CFDD023DFD500002CF02CF02CFDD023DFE02CF02CFDD025D01DD025D0202CF",
                    INIT_31 => X"A323440602CF1580500002CF02CFDD025D0202CFDD023DFE02CF02CFDD025D01",
                    INIT_32 => X"02CF231DA32F450EDD023DFE02CF02CFDD025D0102CFDD025D022325DD023DFD",
                    INIT_33 => X"5D025000D50202CF02CF02CFDD023DFE02CF02CFDD025D01950202CFDD025D02",
                    INIT_34 => X"02CFE342450EDD023DFE02CF02CFDD025D0102CF4400D602960202CF15801400",
                    INIT_35 => X"031C0420A36B031C0410A369031C040002D330FE700163306220611060005000",
                    INIT_36 => X"1004237110032371100223711001500070004006100002F2A36F031C0430A36D",
                    INIT_37 => X"A38B031C040002D330FE7001622061106000500070004006108002F200532371",
                    INIT_38 => X"10032391100223911001500070004006100002F2A38F031C0420A38D031C0410",
                    INIT_39 => X"0410A3B4031C040002D330FE700161106000500070004006108002F200532391",
                    INIT_3A => X"4006100002F203000340033F030D0240033FA3B8031C0400500102DFA36B031C",
                    INIT_3B => X"500070004006108002F2005323BA100323BA100223BA10015000700063306220",
                    INIT_3C => X"5000005900641AC01B031100112E1172116F1172117211651120116B11631141",
                    INIT_3D => X"D503A2401401A340A3DED503A550152A1434A1401430A040142C607CD003B023",
                    INIT_3E => X"A3CB0397A1401430A040142C607CD002B0235000A3CB03775000A3CB0351A3E4",
                    INIT_3F => X"10F3500002C51002500002C51001500002C51000500000590053003000530020",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"A01988AA2A86886155258A4356066155029B40A8618618618618A18A155228A2",
                   INITP_02 => X"90D2AB5B42AA1628A82A0D2A2ADD0AAAC292A8D02B0A3020C208082820882228",
                   INITP_03 => X"882AAAAAAAAAAAAAA0041040D2D68A28A28A28A228A0D0080DD28AAA00C08AAA",
                   INITP_04 => X"420A6AA1228B42AA2828AAA228A62D082AAAAAAAAAAAAAAAAAAAAAAA2288A228",
                   INITP_05 => X"A2AA2A8AA8AA2A2A28AA882D2E0536A5A5ADD82A6AA351548AAA20B429AA848B",
                   INITP_06 => X"E2356D2A2222D2E38E388D56B62A242022A8A828AD8A8A28D8AA28A8A2AA2A22",
                   INITP_07 => X"28A28A88E00D2EBB44D00034A82AAAAAB4A888B54A28B82E388D6D2A222D2E38")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate akv7;
    --
  end generate ram_1k_generate;
  --
  --
  --
  ram_2k_generate : if (C_RAM_SIZE_KWORDS = 2) generate
    --
    --
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400F042",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"5350015350080120025350010120025350900606060630012002013A02533000",
                    INIT_05 => X"0F804000B70181BC40000F800059535001535001535001535001535001535001",
                    INIT_06 => X"060606068100010140200080010F80CB010101D0C00701D001D0C09006060606",
                    INIT_07 => X"0020EF20200B20012050B020D6D6D6D630D6D6D6D634012A29082C2001230390",
                    INIT_08 => X"1D020B24440024CC0002EF0000002F01012F01008080810F808C018C00818000",
                    INIT_09 => X"00010E00810181DB0013810038000F0F8100002F80800081002330300B0B020C",
                    INIT_0A => X"0253413000005900302C7C0223002320000060FB48014801FFFF0131FF4F002A",
                    INIT_0B => X"75102B278204233F00595C76000459660003300102010241300104012002013A",
                    INIT_0C => X"2C017C01A202230003F408070605040008070605891003FF0308040042003FC4",
                    INIT_0D => X"0253700201780201300201200253C001780201300285C001A900D030C02CA9C0",
                    INIT_0E => X"0091002C1000011000011000011000307C022300A90101595340025350025360",
                    INIT_0F => X"850164E00100203A56206472616F426369676F4C2072656B6361725420322D67",
                    INIT_10 => X"64726F5700203A72656666754200595340012E025350012E025360012E025370",
                    INIT_11 => X"02002064726F5700202020203A73657A69732064726F5700203A746E756F6320",
                    INIT_12 => X"2302641C025901290201020102412001280201200244010110024B2020006413",
                    INIT_13 => X"4000236429025967010102010241103024014002012002794000230102010241",
                    INIT_14 => X"01200201020102413064390259B1400023597F0101020102411030280120028E",
                    INIT_15 => X"59B500010102C3FFBF00A0B10A0059A701010201024160915003502C06060130",
                    INIT_16 => X"020002FECFCFCFCF02FD02020100D0013E00000200F007070301000059B50003",
                    INIT_17 => X"000202CFCFCFCF0201CFCFCF02FD00CFCFCF02FECFCFCF02FDCFCF0201CFCF02",
                    INIT_18 => X"2306CF8000CFCF0202CF02FECFCF0201CF02FD00CFCFCF02FECFCF02010202CF",
                    INIT_19 => X"020002CFCFCF02FECFCF020102CF0202CF1D2F0E02FECFCF0201CF02022502FD",
                    INIT_1A => X"1C206B1C10691C00D3FE013020100000CF420E02FECFCF0201CF000202CF8000",
                    INIT_1B => X"8B1C00D3FE0120100000000680F253710471037102710100000600F26F1C306D",
                    INIT_1C => X"10B41C00D3FE01100000000680F25391039102910100000600F28F1C208D1C10",
                    INIT_1D => X"00000680F253BA03BA02BA01000030200600F200403F0D403FB81C0001DF6B1C",
                    INIT_1E => X"03400140DE03502A344030402C7C0323005964C003002E726F727265206B6341",
                    INIT_1F => X"F300C50200C50100C500005953305320CB974030402C7C022300CB7700CB51E4",
                    INIT_20 => X"00031F402C13012300596405040073746E656D756772612064614200F6F9FC00",
                    INIT_21 => X"59534053505360537001200201490285B2300606060600230123012301FFFFFF",
                    INIT_22 => X"8900595340535053605370012002014B02855230060606060040800000078900",
                    INIT_23 => X"00078900595340535053605370012002014C0285B03006060606004080000007",
                    INIT_24 => X"000000000000595340535053605370012002015A028520300606060600408000",
                    INIT_25 => X"060606B0108F7006060606C0855006060606B05953C002015002000001000000",
                    INIT_26 => X"060606B0FC2091503006060606B00320855006060606B0910000000120915006",
                    INIT_27 => X"B048B066B059534053505360537001200201540285B43006060606B091503006",
                    INIT_28 => X"A01F1FA60601071A061A8457B01A06138458B01A060D8456B01A060784B5B02A",
                    INIT_29 => X"0607595310100034053A0E01000E0E27E0020F2E0E2D10A000070000E0060606",
                    INIT_2A => X"62048540A3015C028540A3005601854000595991C07F855006060606B0060606",
                    INIT_2B => X"840209FFFF0009206D0A7301090009006E01FF006D098000A30368088540A302",
                    INIT_2C => X"40A87B00A67B00A47B73100F06060606060769000E0100060009100A7D8A0001",
                    INIT_2D => X"710201023A01025359010201023A01025201025201024533A932A931000E0050",
                    INIT_2E => X"0252010252010245000E00D87B7320733073100F06060606060669000E015953",
                    INIT_2F => X"208000080B01F5020B00000B020800FD01000E015953710201023A0102535901",
                    INIT_30 => X"0B20C01A200B0016020B0B020C10020B1A01000E01000E000B1001200B00000B",
                    INIT_31 => X"01800045370E08460080003D0100322D0E68018032270E6800802C010020200B",
                    INIT_32 => X"0B0B0B0B40200056200B000E4F020B0B0B0B04020048020B560100453F0E0846",
                    INIT_33 => X"06000E0079200B0073020B0B0B0B0002000E006A020B7901000E0E0E0E0E5D20",
                    INIT_34 => X"00C4030046200046200024F020000109EF20B6000086200B0B0B0B0020060606",
                    INIT_35 => X"00409641019F60682060B63000B630B600008D00B330B601AA30B6000E0E00A6",
                    INIT_36 => X"7C02230059537B0073102C7C012300595371008E0E20B0A0BD3020CDCDBD20C5",
                    INIT_37 => X"69766564206F4E00C51030102C100110347C0323005953305340EB8D1030102C",
                    INIT_38 => X"02014E02014F02014E0224EF102C7C0123005964F9065900646E756F66206563",
                    INIT_39 => X"002440301030102C7C02230059014402014E02015502014F0201460200590145",
                    INIT_3A => X"7C032300590E3024443024CC3009EF30102C7C0123005953103330102C7C0123",
                    INIT_3B => X"2430400124304001243040012440303424553009EF30102C7C08102A7C081029",
                    INIT_3C => X"3009EF30102C7C012300590E3024443024304001243040012430400124403030",
                    INIT_3D => X"09EF30102C7C08102A7C0810297C0323005953405310333010333024BE3024CC",
                    INIT_3E => X"0124304001243040012440303024304001243040012430400124403034245530",
                    INIT_3F => X"012002013A0253300F8000F8002301302C19808000333010333024BE30243040",
                   INITP_00 => X"7A3001F600DE429FC4C0924924107E001FFFE536008278000000008F80000001",
                   INITP_01 => X"8FFFFFFF4CB20C00000005F248E8A9C8283D106009801B4D004EB04089689000",
                   INITP_02 => X"618C3199B0F81C022968401F10131830C4C180D008400214FFFFFFFFFFF80000",
                   INITP_03 => X"0000E0379F400FFF02A21EE361015036C101540DB6147982A33B7CDF91B3619E",
                   INITP_04 => X"000008080A0281A403F8001110000406A000101A8000406A0001008D000FFFEF",
                   INITP_05 => X"020132B56D1D502CAD56DBF0EFD0206DEBB8EE28A28A120003D9158001451450",
                   INITP_06 => X"FEEE801D02A04240DA40044CB253900020004000000000022220000000000000",
                   INITP_07 => X"0030801111211122D88800096C02222422245B110096C01827000000003405FF")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_l(31 downto 0),
                  DOPA => data_out_a_l(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_l(31 downto 0),
                  DOPB => data_out_b_l(35 downto 32), 
                   DIB => data_in_b_l(31 downto 0),
                  DIPB => data_in_b_l(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_h: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481600",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"0050CA00508A68080000508A68080000508AA2A2A2A202680800680800000028",
                    INIT_05 => X"1E5E0E28108900F0E1091A5A28000050CA0050CA0050CA0050CA0050CA0050CA",
                    INIT_06 => X"A1A1A1A159288A897151287E8E1E5EF0CF8E8E70500F8E758E70088EA6A6A6A6",
                    INIT_07 => X"2870182850C9708972010302000000000A000000000A89797909795189790989",
                    INIT_08 => X"9068480308080308080902080928040A09040A09782878185803090309787808",
                    INIT_09 => X"886848287888580028007808B0E01818582800D0685828780890601048682808",
                    INIT_0A => X"00000000092800705858B0E85828787808280000F0C9F0C90909585801002800",
                    INIT_0B => X"06815859D0E8580128001090E98900B0E81800680069000050C9896808006808",
                    INIT_0C => X"0E0EB0E890E8582868086B6B6A6A68284B4B4A4A906848186808682800280106",
                    INIT_0D => X"00000000680800680800680800000068080068080000000EB0EE560E560E1056",
                    INIT_0E => X"28005008035088035088025088025008B0E85828B0CE8E000000000000000000",
                    INIT_0F => X"0008000D0D080808080808080808080808080808080808080808080808080808",
                    INIT_10 => X"0808080808080808080808080828000000680800000068080000006808000000",
                    INIT_11 => X"0D08080808080808080808080808080808080808080808080808080808080808",
                    INIT_12 => X"5800000D0D006808006800690000006808006808001188685000F1E05908000D",
                    INIT_13 => X"E1095A000D0D0011896800690000508008680800680800F1E1095A6800690000",
                    INIT_14 => X"680800680069000000000D0D00F1E1095A0011896800690000508008680800F1",
                    INIT_15 => X"00119D8D680091E891E8250D0D280011CB680069000050D1E38B038AA2A28902",
                    INIT_16 => X"2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A018B8B02800119D8D",
                    INIT_17 => X"286E2E010101016E2E0101016E1E280101016E1E0101016E1E01016E2E01016E",
                    INIT_18 => X"D1A2010A2801016E2E016E1E01016E2E016E1E280101016E1E01016E2E6E2E01",
                    INIT_19 => X"2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E01016E2E016E2E116E1E",
                    INIT_1A => X"0102D10102D101020118B8B1B1B0B02801F1A26E1E01016E2E01A26B4B010A0A",
                    INIT_1B => X"D101020118B8B1B0B028B8A0080100110811081108110828B8A00801D10102D1",
                    INIT_1C => X"02D101020118B8B0B028B8A008010011081108110828B8A00801D10102D10102",
                    INIT_1D => X"28B8A008010011081108110828B8B1B1A00801010101010101D101022801D101",
                    INIT_1E => X"EA518A51D1EA520A0A500A500AB0E8582800000D0D0808080808080808080808",
                    INIT_1F => X"08280108280108280108280000000000D101500A500AB0E85828D10128D101D1",
                    INIT_20 => X"286818500AB2E8582800000D0D080808080808080808080808080828010101B6",
                    INIT_21 => X"000000000000000000680800680800008818A0A0A0A028F2CEF2CFF2CF0F0F0E",
                    INIT_22 => X"DACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDBDACA",
                    INIT_23 => X"DBDBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDB",
                    INIT_24 => X"88CB88CA88CA000000000000000000680800680800008018A0A0A0A028E20AC8",
                    INIT_25 => X"A0A0A000221A18A0A0A0A0000088A0A0A0A000000000006808000D050E2888CB",
                    INIT_26 => X"A0A0A0001A02008818A0A0A0A0002A020088A0A0A0A000000B0B0A0A080088A0",
                    INIT_27 => X"0002000200000000000000000000680800680800008818A0A0A0A000008818A0",
                    INIT_28 => X"001DEDB2EE8EA512A59202090012A59202090012A59202090012A59202090002",
                    INIT_29 => X"A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E0100008092518A0A0A0",
                    INIT_2A => X"926A00080208926A00080208926A000828000000121A0088A0A0A0A000A6A6A6",
                    INIT_2B => X"B218480F0F2868080268921848284828F2CF0F28026F0F280208926A00080208",
                    INIT_2C => X"01D20202D20202D202022018A0A0A0A0A0A00228A10928A1096808481292DFCF",
                    INIT_2D => X"0200680008680008006900680008680008680008680008091209120928A00802",
                    INIT_2E => X"000868000868000828A008D20202000200022018A0A0A0A0A0A00228A1090000",
                    INIT_2F => X"282808139368926848080868282808D26828A109000002006800086800080068",
                    INIT_30 => X"68280893684828936848682808936848D36828A00828A0089368936848080868",
                    INIT_31 => X"08090813F3A1A003080908D3682813F3A103080913F3A1030809D36828936848",
                    INIT_32 => X"4848486828280893684828A0936848484868282808936848D3682813F3A1A003",
                    INIT_33 => X"A0A0A008936848289368484848682828A0A008936848D36828A0A0A0A0A09368",
                    INIT_34 => X"E893E8A00300A00300080308000A09F30200040B289368484848682828A0A0A0",
                    INIT_35 => X"EB03B3E989040003000013020BB302B3EBA3040BD3E3130BB3E313A3A0A00B93",
                    INIT_36 => X"B0E8582800000228025008B0E8582800000228130300010100050513130005B3",
                    INIT_37 => X"0808080808080828025008500851885108B0E858280000000000D30250085008",
                    INIT_38 => X"00680800680800680800D3025008B0E8582800000D0D00080808080808080808",
                    INIT_39 => X"2803000052085108B0E858280068080068080068080068080068080028006808",
                    INIT_3A => X"B0E85828000300030800030800F302005108B0E8582800000003005108B0E858",
                    INIT_3B => X"0300508A0300508A0300508A0350000A030800F302005108B0E85008B0E85008",
                    INIT_3C => X"00F302005108B0E858280003000308000300508A0300508A0300508A0350000A",
                    INIT_3D => X"F302005108B0E85008B0E85008B0E85828000000000003000203000308000308",
                    INIT_3E => X"8A0300508A0300508A0350000A0300508A0300508A0300508A0350000A030800",
                    INIT_3F => X"68080068080000001A5A09B3E8588A0259F46808280300020300030800030050",
                   INITP_00 => X"C2AF79A404B111401B0E492492CB016DCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"A7FFFFFFC00099B6DB6D6C8229BF08BF89F31F16E7277A3F99E8734492264A56",
                   INITP_02 => X"DF7BEF776FA6705CCEA77D00BF4C6F8B137C5B1F66FD6D627FFFFFFFFFFF5AD6",
                   INITP_03 => X"6DBAC27F0804E7FFCEAC36E76A675676D467559DB6A1D7445EE6EBB6AF6EDF75",
                   INITP_04 => X"56AAD8080601810400081B46AB556C090D55B0243556C090D55B0350C4E7FFFE",
                   INITP_05 => X"1A114FDBB69D4033F6FB6CA86DC0324C86E59DAAAAAAF2003141140011659659",
                   INITP_06 => X"FF804EB09F93F89D25252AA14921AC2009841282112844B919174D2C9324A4A1",
                   INITP_07 => X"B6104CA444444444C444EA5262748888888898889D262744C09DB6DEDB7273FF")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_h(31 downto 0),
                  DOPA => data_out_a_h(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_h(31 downto 0),
                  DOPB => data_out_b_h(35 downto 32), 
                   DIB => data_in_b_h(31 downto 0),
                  DIPB => data_in_b_h(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D00490002CF00142",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"A050159045064506450645060530D00110200002D001103A0002005300305000",
                    INIT_09 => X"0053A05095010053A0501508D001102000020053A0501501D001102000020053",
                    INIT_0A => X"95010053A05095010053A05095010053A05095010053A05095010053A0509501",
                    INIT_0B => X"3D0FBD801C40500020B713010081E0BCC3401300340FB480500000590053A050",
                    INIT_0C => X"9E011D011C01E0D0A0C01E071D01EAD01D01E0D010C01D904D064D064D064D06",
                    INIT_0D => X"4206420642064206B281500014011201E340A3205000FD801D013D0FBD80E0CB",
                    INIT_0E => X"143000D600D600D600D614341201F32AF3291308F32CA3201201F32313031290",
                    INIT_0F => X"5000E12031EF5120A120920BE1201201E420025007B0052000D600D600D600D6",
                    INIT_10 => X"1301082F14011300F0805080F081300FB080068C1201068C1200F081F0801000",
                    INIT_11 => X"211DD002900B062411441000062411CC1000130205EF100013005000082F1401",
                    INIT_12 => X"B18150000100A12FD080B0805000F08110002123C0302030900BD00B5002100C",
                    INIT_13 => X"1000D001900E5000F0811001B08100DB50000113F08110006138C100310F300F",
                    INIT_14 => X"10005000006001FBE1489201E148930112FF13FFB001B03103FF014F5000012A",
                    INIT_15 => X"0002005300410030130050000059E100B130B02C607CD002B0235000F023F020",
                    INIT_16 => X"D00030030030D1010002D20100020041A03093011304D00110200002D001103A",
                    INIT_17 => X"0C750210B12BB227A182D004B023023F50000059215C2176D300130400596166",
                    INIT_18 => X"97089607950694052189D010900331FFD1031108D004500001425000023F0CC4",
                    INIT_19 => X"1C2C1D01607CD00121A2D002B0235000D10311F4D708D607D506D405D0045000",
                    INIT_1A => X"10780002D00110300002018500C01D0161A9DD00ADD01D30ACC01C2C21A9ACC0",
                    INIT_1B => X"0002005300700002D00110780002D00110300002D00110200002005300C0D001",
                    INIT_1C => X"607CD002B023500061A99D011C01005900530040000200530050000200530060",
                    INIT_1D => X"50000191A000102C0710A10010010610A10010010510A10010010410A1001030",
                    INIT_1E => X"11691167116F114C112011721165116B116311611172115411201132112D1167",
                    INIT_1F => X"0185100100641AE01B0111001120113A11561120116411721161116F11421163",
                    INIT_20 => X"0040D001102E000200530050D001102E000200530060D001102E000200530070",
                    INIT_21 => X"11641172116F115711001120113A117211651166116611751142500000590053",
                    INIT_22 => X"11691173112011641172116F115711001120113A1174116E1175116F11631120",
                    INIT_23 => X"1B021100112011641172116F115711001120112011201120113A11731165117A",
                    INIT_24 => X"10280002D0011020000222441101D001A0100002E24BC120B220110000641A13",
                    INIT_25 => X"B023000200641A1C1B020059D00110290002D1010002D201000200410020D001",
                    INIT_26 => X"1124D00110400002D00110200002E279C3401300B423D1010002D20100020041",
                    INIT_27 => X"C3401300B42300641A291B02005922671301D1010002D20100020041A0100130",
                    INIT_28 => X"B4230059227F1301D1010002D20100020041A01001301128D00110200002E28E",
                    INIT_29 => X"D00110200002D1010002D20100020041003000641A391B020059E2B1C3401300",
                    INIT_2A => X"9601D1010002D20100020041A060A291C65016030650152C4506450613010530",
                    INIT_2B => X"005922B53B001A01D101000222C3D1FF22BFD1004BA01AB11B0A5000005922A7",
                    INIT_2C => X"1E3E50007000DD024D003DF0400740073003700160005000005922B53B001A03",
                    INIT_2D => X"5D025000DD023DFE02CF02CF02CF02CFDD023DFDDD025D025D015000E2D09E01",
                    INIT_2E => X"02CF02CFDD023DFE02CF02CF02CFDD023DFD02CF02CFDD025D0102CF02CFDD02",
                    INIT_2F => X"5000DD025D0202CF02CF02CF02CFDD025D0102CF02CF02CFDD023DFD500002CF",
                    INIT_30 => X"02CFDD023DFD500002CF02CF02CFDD023DFE02CF02CFDD025D01DD025D0202CF",
                    INIT_31 => X"A323440602CF1580500002CF02CFDD025D0202CFDD023DFE02CF02CFDD025D01",
                    INIT_32 => X"02CF231DA32F450EDD023DFE02CF02CFDD025D0102CFDD025D022325DD023DFD",
                    INIT_33 => X"5D025000D50202CF02CF02CFDD023DFE02CF02CFDD025D01950202CFDD025D02",
                    INIT_34 => X"02CFE342450EDD023DFE02CF02CFDD025D0102CF4400D602960202CF15801400",
                    INIT_35 => X"031C0420A36B031C0410A369031C040002D330FE700163306220611060005000",
                    INIT_36 => X"1004237110032371100223711001500070004006100002F2A36F031C0430A36D",
                    INIT_37 => X"A38B031C040002D330FE7001622061106000500070004006108002F200532371",
                    INIT_38 => X"10032391100223911001500070004006100002F2A38F031C0420A38D031C0410",
                    INIT_39 => X"0410A3B4031C040002D330FE700161106000500070004006108002F200532391",
                    INIT_3A => X"4006100002F203000340033F030D0240033FA3B8031C0400500102DFA36B031C",
                    INIT_3B => X"500070004006108002F2005323BA100323BA100223BA10015000700063306220",
                    INIT_3C => X"5000005900641AC01B031100112E1172116F1172117211651120116B11631141",
                    INIT_3D => X"D503A2401401A340A3DED503A550152A1434A1401430A040142C607CD003B023",
                    INIT_3E => X"A3CB0397A1401430A040142C607CD002B0235000A3CB03775000A3CB0351A3E4",
                    INIT_3F => X"10F3500002C51002500002C51001500002C51000500000590053003000530020",
                    INIT_40 => X"116E1165116D11751167117211611120116411611142500003F603F903FC6D00",
                    INIT_41 => X"5000D003301FA040142C6413D001B0235000005900641A051B04110011731174",
                    INIT_42 => X"10B2303040064006400640065000E4239D01E4239E01E4239F011FFF1EFF1DFF",
                    INIT_43 => X"005900530040005300500053006000530070D00110200002D001104900020185",
                    INIT_44 => X"000201851052303040064006400640065000C54015809000B700B607B5899400",
                    INIT_45 => X"B5899400005900530040005300500053006000530070D00110200002D001104B",
                    INIT_46 => X"D001104C0002018510B0303040064006400640065000C54015809000B700B607",
                    INIT_47 => X"B700B607B5899400005900530040005300500053006000530070D00110200002",
                    INIT_48 => X"10200002D001105A000201850020303040064006400640065000C54015809000",
                    INIT_49 => X"100096001000950010009400005900530040005300500053006000530070D001",
                    INIT_4A => X"4006400600B00059005300C00002D001105000021A000B001C01500010009700",
                    INIT_4B => X"40064006400600B04410348F3170410641064106410601C00185105040064006",
                    INIT_4C => X"01851050400640064006400600B0019117001600150014011020019110504006",
                    INIT_4D => X"40064006400600B035FC0420019110503030400640064006400600B055030420",
                    INIT_4E => X"0002D00110540002018510B43030400640064006400600B00191105030304006",
                    INIT_4F => X"00B0044800B0046600B0005900530040005300500053006000530070D0011020",
                    INIT_50 => X"0484125800B0251A4A06250D0484125600B0251A4A062507048412B500B0042A",
                    INIT_51 => X"00A03A1FDA1F64A6DC061C014A07251A4A06251A0484125700B0251A4A062513",
                    INIT_52 => X"D0E04002420F252E420E252DC01021A00100100712004A0030E0400640064006",
                    INIT_53 => X"4C063C070059005300100C101100A534D105A53A420E11011100420E420E6527",
                    INIT_54 => X"500000590059019124C0347F01851050400640064006400600B04C064C064C06",
                    INIT_55 => X"2562D4040185104004A31001255CD4020185104004A310002556D40101851040",
                    INIT_56 => X"E56E9F011FFF5000056DDF091F80500004A310032568D4080185104004A31002",
                    INIT_57 => X"6584310291091EFF1FFF5000D1091120056DD00A257331019109500090095000",
                    INIT_58 => X"4006400705695000420E1201500042061200D1091110900A257D258ABE009F01",
                    INIT_59 => X"0340A5A8057B0400A5A6057B0500A5A4057B05734010310F4006400640064006",
                    INIT_5A => X"1152D10100021152D10100021145133325A9133225A913315000400E10000450",
                    INIT_5B => X"05710002D1010002113AD101000211530059D3010002D1010002113AD1010002",
                    INIT_5C => X"003005734010310F40064006400640064006400605695000430E130100590053",
                    INIT_5D => X"00021152D10100021152D101000211455000400E1000A5D8057B057300200573",
                    INIT_5E => X"D0015000430E13010059005305710002D1010002113AD101000211530059D101",
                    INIT_5F => X"5020508010002608260BD00125F5D002900B10001000D00B500250081000A5FD",
                    INIT_60 => X"A61AD0015000400E10015000400E1000260BD0102601D020900B10001000D00B",
                    INIT_61 => X"D00B502010C0261AD020900B50002616D002900BD00B5002100C2610D002900B",
                    INIT_62 => X"420E0668100112802632E627420E066810001280A62CD00150002620D020900B",
                    INIT_63 => X"1001128011002645E637420E41080646100012801100A63DD00150002632E62D",
                    INIT_64 => X"900BD00B5004500210002648D002900BA656D00150002645E63F420E41080646",
                    INIT_65 => X"900B900B900BD00B5040502010002656D020900B5000400E264FD002900B900B",
                    INIT_66 => X"4000410E1000266AD002900BA679D0015000400E400E400E400E400E265DD020",
                    INIT_67 => X"40064000410E10002679D020900B50002673D002900B900B900BD00B50005002",
                    INIT_68 => X"05EF002008B6170050002686D020900B900B900BD00B50005020400640064006",
                    INIT_69 => X"D10026C4D1034100064600204100064600201100062411F0002014001301E709",
                    INIT_6A => X"D6004600088D1600A6B3C73026B6160166AAC73026B64600410E410E160026A6",
                    INIT_6B => X"D70007406696D3411301089F016006680020016026B60430160066B6043066B6",
                    INIT_6C => X"005305715000268E060E002003B002A000BD0B300A2026CD26CD00BD0A2066C5",
                    INIT_6D => X"607CD002B023500000590053057B50000573A010112C607CD001B02350000059",
                    INIT_6E => X"1134607CD003B023500000590053003000530040A6EB058DA1101130A010112C",
                    INIT_6F => X"11691176116511641120116F114E500005C5A1101130A010112CA3101101A210",
                    INIT_70 => X"B0235000005900641AF91B06005911001164116E1175116F1166112011651163",
                    INIT_71 => X"0002D001104E0002D001104F0002D001104E0002A72405EFA010112C607CD001",
                    INIT_72 => X"D001104E0002D00110550002D001104F0002D0011046000250000059D0011045",
                    INIT_73 => X"5000062401400030A4101130A310112C607CD002B02350000059D00110440002",
                    INIT_74 => X"A310112C607CD001B023500000590053001006330030A310112C607CD001B023",
                    INIT_75 => X"607CD003B02350000059060E0030062411440030062411CC0030E70905EF0030",
                    INIT_76 => X"062411550030E70905EF0030A310112C607CD008A010112A607CD008A0101129",
                    INIT_77 => X"06240030A140140106240030A140140106240030A14014010624A14000301434",
                    INIT_78 => X"06240030A140140106240030A140140106240030A14014010624A14000301430",
                    INIT_79 => X"0030E70905EF0030A310112C607CD001B02350000059060E0030062411440030",
                    INIT_7A => X"50000059005300400053001006330030041006330030062411BE0030062411CC",
                    INIT_7B => X"E70905EF0030A310112C607CD008A010112A607CD008A0101129607CD003B023",
                    INIT_7C => X"140106240030A140140106240030A14014010624A14000301434062411550030",
                    INIT_7D => X"140106240030A140140106240030A14014010624A1400030143006240030A140",
                    INIT_7E => X"B32CE819D0801080500006330030041006330030062411BE003006240030A140",
                    INIT_7F => X"D00110200002D001103A000200530030340FB480130067F8D000B02314010430",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"A01988AA2A86886155258A4356066155029B40A8618618618618A18A155228A2",
                   INITP_02 => X"90D2AB5B42AA1628A82A0D2A2ADD0AAAC292A8D02B0A3020C208082820882228",
                   INITP_03 => X"882AAAAAAAAAAAAAA0041040D2D68A28A28A28A228A0D0080DD28AAA00C08AAA",
                   INITP_04 => X"420A6AA1228B42AA2828AAA228A62D082AAAAAAAAAAAAAAAAAAAAAAA2288A228",
                   INITP_05 => X"A2AA2A8AA8AA2A2A28AA882D2E0536A5A5ADD82A6AA351548AAA20B429AA848B",
                   INITP_06 => X"E2356D2A2222D2E38E388D56B62A242022A8A828AD8A8A28D8AA28A8A2AA2A22",
                   INITP_07 => X"28A28A88E00D2EBB44D00034A82AAAAAB4A888B54A28B82E388D6D2A222D2E38",
                   INITP_08 => X"55A222228A4559355A222228A4559355A222228A455B7740A034A82AAAAAAAA9",
                   INITP_09 => X"22288888A2915491542915429552002554015495528A202DDDDA222228A45593",
                   INITP_0A => X"C028AC22D2A28C88C88C88C8AA0955154A037517567400150756782782782782",
                   INITP_0B => X"02CC020324AAA28A8A2893A220555A4AAA28AA8A28A0889038E3A0555A4920B5",
                   INITP_0C => X"54C2C020530C955C020309C02030CAD602D6032B60B60CB0830B0830C924CC02",
                   INITP_0D => X"AAAA800434A88E00D2AA834AAA8082A34D62083358D8D9537586080388B00815",
                   INITP_0E => X"818181808380D0D0D2A208380D2A2034A000D2A28A28A2A8A28A2E0D2A0AAAAA",
                   INITP_0F => X"8A28034430A088206060602060606020E0343434A8882208380D2A2081818180")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D00490002CF00142",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"A050159045064506450645060530D00110200002D001103A0002005300305000",
                    INIT_09 => X"0053A05095010053A0501508D001102000020053A0501501D001102000020053",
                    INIT_0A => X"95010053A05095010053A05095010053A05095010053A05095010053A0509501",
                    INIT_0B => X"3D0FBD801C40500020B713010081E0BCC3401300340FB480500000590053A050",
                    INIT_0C => X"9E011D011C01E0D0A0C01E071D01EAD01D01E0D010C01D904D064D064D064D06",
                    INIT_0D => X"4206420642064206B281500014011201E340A3205000FD801D013D0FBD80E0CB",
                    INIT_0E => X"143000D600D600D600D614341201F32AF3291308F32CA3201201F32313031290",
                    INIT_0F => X"5000E12031EF5120A120920BE1201201E420025007B0052000D600D600D600D6",
                    INIT_10 => X"1301082F14011300F0805080F081300FB080068C1201068C1200F081F0801000",
                    INIT_11 => X"211DD002900B062411441000062411CC1000130205EF100013005000082F1401",
                    INIT_12 => X"B18150000100A12FD080B0805000F08110002123C0302030900BD00B5002100C",
                    INIT_13 => X"1000D001900E5000F0811001B08100DB50000113F08110006138C100310F300F",
                    INIT_14 => X"10005000006001FBE1489201E148930112FF13FFB001B03103FF014F5000012A",
                    INIT_15 => X"0002005300410030130050000059E100B130B02C607CD002B0235000F023F020",
                    INIT_16 => X"D00030030030D1010002D20100020041A03093011304D00110200002D001103A",
                    INIT_17 => X"0C750210B12BB227A182D004B023023F50000059215C2176D300130400596166",
                    INIT_18 => X"97089607950694052189D010900331FFD1031108D004500001425000023F0CC4",
                    INIT_19 => X"1C2C1D01607CD00121A2D002B0235000D10311F4D708D607D506D405D0045000",
                    INIT_1A => X"10780002D00110300002018500C01D0161A9DD00ADD01D30ACC01C2C21A9ACC0",
                    INIT_1B => X"0002005300700002D00110780002D00110300002D00110200002005300C0D001",
                    INIT_1C => X"607CD002B023500061A99D011C01005900530040000200530050000200530060",
                    INIT_1D => X"50000191A000102C0710A10010010610A10010010510A10010010410A1001030",
                    INIT_1E => X"11691167116F114C112011721165116B116311611172115411201132112D1167",
                    INIT_1F => X"0185100100641AE01B0111001120113A11561120116411721161116F11421163",
                    INIT_20 => X"0040D001102E000200530050D001102E000200530060D001102E000200530070",
                    INIT_21 => X"11641172116F115711001120113A117211651166116611751142500000590053",
                    INIT_22 => X"11691173112011641172116F115711001120113A1174116E1175116F11631120",
                    INIT_23 => X"1B021100112011641172116F115711001120112011201120113A11731165117A",
                    INIT_24 => X"10280002D0011020000222441101D001A0100002E24BC120B220110000641A13",
                    INIT_25 => X"B023000200641A1C1B020059D00110290002D1010002D201000200410020D001",
                    INIT_26 => X"1124D00110400002D00110200002E279C3401300B423D1010002D20100020041",
                    INIT_27 => X"C3401300B42300641A291B02005922671301D1010002D20100020041A0100130",
                    INIT_28 => X"B4230059227F1301D1010002D20100020041A01001301128D00110200002E28E",
                    INIT_29 => X"D00110200002D1010002D20100020041003000641A391B020059E2B1C3401300",
                    INIT_2A => X"9601D1010002D20100020041A060A291C65016030650152C4506450613010530",
                    INIT_2B => X"005922B53B001A01D101000222C3D1FF22BFD1004BA01AB11B0A5000005922A7",
                    INIT_2C => X"1E3E50007000DD024D003DF0400740073003700160005000005922B53B001A03",
                    INIT_2D => X"5D025000DD023DFE02CF02CF02CF02CFDD023DFDDD025D025D015000E2D09E01",
                    INIT_2E => X"02CF02CFDD023DFE02CF02CF02CFDD023DFD02CF02CFDD025D0102CF02CFDD02",
                    INIT_2F => X"5000DD025D0202CF02CF02CF02CFDD025D0102CF02CF02CFDD023DFD500002CF",
                    INIT_30 => X"02CFDD023DFD500002CF02CF02CFDD023DFE02CF02CFDD025D01DD025D0202CF",
                    INIT_31 => X"A323440602CF1580500002CF02CFDD025D0202CFDD023DFE02CF02CFDD025D01",
                    INIT_32 => X"02CF231DA32F450EDD023DFE02CF02CFDD025D0102CFDD025D022325DD023DFD",
                    INIT_33 => X"5D025000D50202CF02CF02CFDD023DFE02CF02CFDD025D01950202CFDD025D02",
                    INIT_34 => X"02CFE342450EDD023DFE02CF02CFDD025D0102CF4400D602960202CF15801400",
                    INIT_35 => X"031C0420A36B031C0410A369031C040002D330FE700163306220611060005000",
                    INIT_36 => X"1004237110032371100223711001500070004006100002F2A36F031C0430A36D",
                    INIT_37 => X"A38B031C040002D330FE7001622061106000500070004006108002F200532371",
                    INIT_38 => X"10032391100223911001500070004006100002F2A38F031C0420A38D031C0410",
                    INIT_39 => X"0410A3B4031C040002D330FE700161106000500070004006108002F200532391",
                    INIT_3A => X"4006100002F203000340033F030D0240033FA3B8031C0400500102DFA36B031C",
                    INIT_3B => X"500070004006108002F2005323BA100323BA100223BA10015000700063306220",
                    INIT_3C => X"5000005900641AC01B031100112E1172116F1172117211651120116B11631141",
                    INIT_3D => X"D503A2401401A340A3DED503A550152A1434A1401430A040142C607CD003B023",
                    INIT_3E => X"A3CB0397A1401430A040142C607CD002B0235000A3CB03775000A3CB0351A3E4",
                    INIT_3F => X"10F3500002C51002500002C51001500002C51000500000590053003000530020",
                    INIT_40 => X"116E1165116D11751167117211611120116411611142500003F603F903FC6D00",
                    INIT_41 => X"5000D003301FA040142C6413D001B0235000005900641A051B04110011731174",
                    INIT_42 => X"10B2303040064006400640065000E4239D01E4239E01E4239F011FFF1EFF1DFF",
                    INIT_43 => X"005900530040005300500053006000530070D00110200002D001104900020185",
                    INIT_44 => X"000201851052303040064006400640065000C54015809000B700B607B5899400",
                    INIT_45 => X"B5899400005900530040005300500053006000530070D00110200002D001104B",
                    INIT_46 => X"D001104C0002018510B0303040064006400640065000C54015809000B700B607",
                    INIT_47 => X"B700B607B5899400005900530040005300500053006000530070D00110200002",
                    INIT_48 => X"10200002D001105A000201850020303040064006400640065000C54015809000",
                    INIT_49 => X"100096001000950010009400005900530040005300500053006000530070D001",
                    INIT_4A => X"4006400600B00059005300C00002D001105000021A000B001C01500010009700",
                    INIT_4B => X"40064006400600B04410348F3170410641064106410601C00185105040064006",
                    INIT_4C => X"01851050400640064006400600B0019117001600150014011020019110504006",
                    INIT_4D => X"40064006400600B035FC0420019110503030400640064006400600B055030420",
                    INIT_4E => X"0002D00110540002018510B43030400640064006400600B00191105030304006",
                    INIT_4F => X"00B0044800B0046600B0005900530040005300500053006000530070D0011020",
                    INIT_50 => X"0484125800B0251A4A06250D0484125600B0251A4A062507048412B500B0042A",
                    INIT_51 => X"00A03A1FDA1F64A6DC061C014A07251A4A06251A0484125700B0251A4A062513",
                    INIT_52 => X"D0E04002420F252E420E252DC01021A00100100712004A0030E0400640064006",
                    INIT_53 => X"4C063C070059005300100C101100A534D105A53A420E11011100420E420E6527",
                    INIT_54 => X"500000590059019124C0347F01851050400640064006400600B04C064C064C06",
                    INIT_55 => X"2562D4040185104004A31001255CD4020185104004A310002556D40101851040",
                    INIT_56 => X"E56E9F011FFF5000056DDF091F80500004A310032568D4080185104004A31002",
                    INIT_57 => X"6584310291091EFF1FFF5000D1091120056DD00A257331019109500090095000",
                    INIT_58 => X"4006400705695000420E1201500042061200D1091110900A257D258ABE009F01",
                    INIT_59 => X"0340A5A8057B0400A5A6057B0500A5A4057B05734010310F4006400640064006",
                    INIT_5A => X"1152D10100021152D10100021145133325A9133225A913315000400E10000450",
                    INIT_5B => X"05710002D1010002113AD101000211530059D3010002D1010002113AD1010002",
                    INIT_5C => X"003005734010310F40064006400640064006400605695000430E130100590053",
                    INIT_5D => X"00021152D10100021152D101000211455000400E1000A5D8057B057300200573",
                    INIT_5E => X"D0015000430E13010059005305710002D1010002113AD101000211530059D101",
                    INIT_5F => X"5020508010002608260BD00125F5D002900B10001000D00B500250081000A5FD",
                    INIT_60 => X"A61AD0015000400E10015000400E1000260BD0102601D020900B10001000D00B",
                    INIT_61 => X"D00B502010C0261AD020900B50002616D002900BD00B5002100C2610D002900B",
                    INIT_62 => X"420E0668100112802632E627420E066810001280A62CD00150002620D020900B",
                    INIT_63 => X"1001128011002645E637420E41080646100012801100A63DD00150002632E62D",
                    INIT_64 => X"900BD00B5004500210002648D002900BA656D00150002645E63F420E41080646",
                    INIT_65 => X"900B900B900BD00B5040502010002656D020900B5000400E264FD002900B900B",
                    INIT_66 => X"4000410E1000266AD002900BA679D0015000400E400E400E400E400E265DD020",
                    INIT_67 => X"40064000410E10002679D020900B50002673D002900B900B900BD00B50005002",
                    INIT_68 => X"05EF002008B6170050002686D020900B900B900BD00B50005020400640064006",
                    INIT_69 => X"D10026C4D1034100064600204100064600201100062411F0002014001301E709",
                    INIT_6A => X"D6004600088D1600A6B3C73026B6160166AAC73026B64600410E410E160026A6",
                    INIT_6B => X"D70007406696D3411301089F016006680020016026B60430160066B6043066B6",
                    INIT_6C => X"005305715000268E060E002003B002A000BD0B300A2026CD26CD00BD0A2066C5",
                    INIT_6D => X"607CD002B023500000590053057B50000573A010112C607CD001B02350000059",
                    INIT_6E => X"1134607CD003B023500000590053003000530040A6EB058DA1101130A010112C",
                    INIT_6F => X"11691176116511641120116F114E500005C5A1101130A010112CA3101101A210",
                    INIT_70 => X"B0235000005900641AF91B06005911001164116E1175116F1166112011651163",
                    INIT_71 => X"0002D001104E0002D001104F0002D001104E0002A72405EFA010112C607CD001",
                    INIT_72 => X"D001104E0002D00110550002D001104F0002D0011046000250000059D0011045",
                    INIT_73 => X"5000062401400030A4101130A310112C607CD002B02350000059D00110440002",
                    INIT_74 => X"A310112C607CD001B023500000590053001006330030A310112C607CD001B023",
                    INIT_75 => X"607CD003B02350000059060E0030062411440030062411CC0030E70905EF0030",
                    INIT_76 => X"062411550030E70905EF0030A310112C607CD008A010112A607CD008A0101129",
                    INIT_77 => X"06240030A140140106240030A140140106240030A14014010624A14000301434",
                    INIT_78 => X"06240030A140140106240030A140140106240030A14014010624A14000301430",
                    INIT_79 => X"0030E70905EF0030A310112C607CD001B02350000059060E0030062411440030",
                    INIT_7A => X"50000059005300400053001006330030041006330030062411BE0030062411CC",
                    INIT_7B => X"E70905EF0030A310112C607CD008A010112A607CD008A0101129607CD003B023",
                    INIT_7C => X"140106240030A140140106240030A14014010624A14000301434062411550030",
                    INIT_7D => X"140106240030A140140106240030A14014010624A1400030143006240030A140",
                    INIT_7E => X"B32CE819D0801080500006330030041006330030062411BE003006240030A140",
                    INIT_7F => X"D00110200002D001103A000200530030340FB480130067F8D000B02314010430",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"A01988AA2A86886155258A4356066155029B40A8618618618618A18A155228A2",
                   INITP_02 => X"90D2AB5B42AA1628A82A0D2A2ADD0AAAC292A8D02B0A3020C208082820882228",
                   INITP_03 => X"882AAAAAAAAAAAAAA0041040D2D68A28A28A28A228A0D0080DD28AAA00C08AAA",
                   INITP_04 => X"420A6AA1228B42AA2828AAA228A62D082AAAAAAAAAAAAAAAAAAAAAAA2288A228",
                   INITP_05 => X"A2AA2A8AA8AA2A2A28AA882D2E0536A5A5ADD82A6AA351548AAA20B429AA848B",
                   INITP_06 => X"E2356D2A2222D2E38E388D56B62A242022A8A828AD8A8A28D8AA28A8A2AA2A22",
                   INITP_07 => X"28A28A88E00D2EBB44D00034A82AAAAAB4A888B54A28B82E388D6D2A222D2E38",
                   INITP_08 => X"55A222228A4559355A222228A4559355A222228A455B7740A034A82AAAAAAAA9",
                   INITP_09 => X"22288888A2915491542915429552002554015495528A202DDDDA222228A45593",
                   INITP_0A => X"C028AC22D2A28C88C88C88C8AA0955154A037517567400150756782782782782",
                   INITP_0B => X"02CC020324AAA28A8A2893A220555A4AAA28AA8A28A0889038E3A0555A4920B5",
                   INITP_0C => X"54C2C020530C955C020309C02030CAD602D6032B60B60CB0830B0830C924CC02",
                   INITP_0D => X"AAAA800434A88E00D2AA834AAA8082A34D62083358D8D9537586080388B00815",
                   INITP_0E => X"818181808380D0D0D2A208380D2A2034A000D2A28A28A2A8A28A2E0D2A0AAAAA",
                   INITP_0F => X"8A28034430A088206060602060606020E0343434A8882208380D2A2081818180")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_2k_generate;
  --
  --	
  ram_4k_generate : if (C_RAM_SIZE_KWORDS = 4) generate
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      data_in_a <= "000000000000000000000000000000000000";
      --
      s6_a11_flop: FD
      port map (  D => address(11),
                  Q => pipe_a11,
                  C => clk);
      --
      s6_4k_mux0_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(0),
                I1 => data_out_a_hl(0),
                I2 => data_out_a_ll(1),
                I3 => data_out_a_hl(1),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(0),
                O6 => instruction(1));
      --
      s6_4k_mux2_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(2),
                I1 => data_out_a_hl(2),
                I2 => data_out_a_ll(3),
                I3 => data_out_a_hl(3),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(2),
                O6 => instruction(3));
      --
      s6_4k_mux4_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(4),
                I1 => data_out_a_hl(4),
                I2 => data_out_a_ll(5),
                I3 => data_out_a_hl(5),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(4),
                O6 => instruction(5));
      --
      s6_4k_mux6_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(6),
                I1 => data_out_a_hl(6),
                I2 => data_out_a_ll(7),
                I3 => data_out_a_hl(7),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(6),
                O6 => instruction(7));
      --
      s6_4k_mux8_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(32),
                I1 => data_out_a_hl(32),
                I2 => data_out_a_lh(0),
                I3 => data_out_a_hh(0),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(8),
                O6 => instruction(9));
      --
      s6_4k_mux10_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(1),
                I1 => data_out_a_hh(1),
                I2 => data_out_a_lh(2),
                I3 => data_out_a_hh(2),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(10),
                O6 => instruction(11));
      --
      s6_4k_mux12_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(3),
                I1 => data_out_a_hh(3),
                I2 => data_out_a_lh(4),
                I3 => data_out_a_hh(4),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(12),
                O6 => instruction(13));
      --
      s6_4k_mux14_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(5),
                I1 => data_out_a_hh(5),
                I2 => data_out_a_lh(6),
                I3 => data_out_a_hh(6),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(14),
                O6 => instruction(15));
      --
      s6_4k_mux16_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(7),
                I1 => data_out_a_hh(7),
                I2 => data_out_a_lh(32),
                I3 => data_out_a_hh(32),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(16),
                O6 => instruction(17));
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_ll <= "000" & data_out_b_ll(32) & "000000000000000000000000" & data_out_b_ll(7 downto 0);
        data_in_b_lh <= "000" & data_out_b_lh(32) & "000000000000000000000000" & data_out_b_lh(7 downto 0);
        data_in_b_hl <= "000" & data_out_b_hl(32) & "000000000000000000000000" & data_out_b_hl(7 downto 0);
        data_in_b_hh <= "000" & data_out_b_hh(32) & "000000000000000000000000" & data_out_b_hh(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b_l(3 downto 0) <= "0000";
        we_b_h(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
        jtag_dout <= data_out_b_lh(32) & data_out_b_lh(7 downto 0) & data_out_b_ll(32) & data_out_b_ll(7 downto 0);
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_lh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_ll <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        data_in_b_hh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_hl <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        --
        s6_4k_jtag_we_lut: LUT6_2
        generic map (INIT => X"8000000020000000")
        port map( I0 => jtag_we,
                  I1 => jtag_addr(11),
                  I2 => '1',
                  I3 => '1',
                  I4 => '1',
                  I5 => '1',
                  O5 => jtag_we_l,
                  O6 => jtag_we_h);
        --
        we_b_l(3 downto 0) <= jtag_we_l & jtag_we_l & jtag_we_l & jtag_we_l;
        we_b_h(3 downto 0) <= jtag_we_h & jtag_we_h & jtag_we_h & jtag_we_h;
        --
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
        --
        s6_4k_jtag_mux0_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(0),
                  I1 => data_out_b_hl(0),
                  I2 => data_out_b_ll(1),
                  I3 => data_out_b_hl(1),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(0),
                  O6 => jtag_dout(1));
        --
        s6_4k_jtag_mux2_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(2),
                  I1 => data_out_b_hl(2),
                  I2 => data_out_b_ll(3),
                  I3 => data_out_b_hl(3),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(2),
                  O6 => jtag_dout(3));
        --
        s6_4k_jtag_mux4_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(4),
                  I1 => data_out_b_hl(4),
                  I2 => data_out_b_ll(5),
                  I3 => data_out_b_hl(5),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(4),
                  O6 => jtag_dout(5));
        --
        s6_4k_jtag_mux6_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(6),
                  I1 => data_out_b_hl(6),
                  I2 => data_out_b_ll(7),
                  I3 => data_out_b_hl(7),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(6),
                  O6 => jtag_dout(7));
        --
        s6_4k_jtag_mux8_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(32),
                  I1 => data_out_b_hl(32),
                  I2 => data_out_b_lh(0),
                  I3 => data_out_b_hh(0),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(8),
                  O6 => jtag_dout(9));
        --
        s6_4k_jtag_mux10_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(1),
                  I1 => data_out_b_hh(1),
                  I2 => data_out_b_lh(2),
                  I3 => data_out_b_hh(2),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(10),
                  O6 => jtag_dout(11));
        --
        s6_4k_jtag_mux12_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(3),
                  I1 => data_out_b_hh(3),
                  I2 => data_out_b_lh(4),
                  I3 => data_out_b_hh(4),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(12),
                  O6 => jtag_dout(13));
        --
        s6_4k_jtag_mux14_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(5),
                  I1 => data_out_b_hh(5),
                  I2 => data_out_b_lh(6),
                  I3 => data_out_b_hh(6),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(14),
                  O6 => jtag_dout(15));
        --
        s6_4k_jtag_mux16_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(7),
                  I1 => data_out_b_hh(7),
                  I2 => data_out_b_lh(32),
                  I3 => data_out_b_hh(32),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(16),
                  O6 => jtag_dout(17));
      --
      end generate loader;
      --
      kcpsm6_rom_ll: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400F042",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"5350015350080120025350010120025350900606060630012002013A02533000",
                    INIT_05 => X"0F804000B70181BC40000F800059535001535001535001535001535001535001",
                    INIT_06 => X"060606068100010140200080010F80CB010101D0C00701D001D0C09006060606",
                    INIT_07 => X"0020EF20200B20012050B020D6D6D6D630D6D6D6D634012A29082C2001230390",
                    INIT_08 => X"1D020B24440024CC0002EF0000002F01012F01008080810F808C018C00818000",
                    INIT_09 => X"00010E00810181DB0013810038000F0F8100002F80800081002330300B0B020C",
                    INIT_0A => X"0253413000005900302C7C0223002320000060FB48014801FFFF0131FF4F002A",
                    INIT_0B => X"75102B278204233F00595C76000459660003300102010241300104012002013A",
                    INIT_0C => X"2C017C01A202230003F408070605040008070605891003FF0308040042003FC4",
                    INIT_0D => X"0253700201780201300201200253C001780201300285C001A900D030C02CA9C0",
                    INIT_0E => X"0091002C1000011000011000011000307C022300A90101595340025350025360",
                    INIT_0F => X"850164E00100203A56206472616F426369676F4C2072656B6361725420322D67",
                    INIT_10 => X"64726F5700203A72656666754200595340012E025350012E025360012E025370",
                    INIT_11 => X"02002064726F5700202020203A73657A69732064726F5700203A746E756F6320",
                    INIT_12 => X"2302641C025901290201020102412001280201200244010110024B2020006413",
                    INIT_13 => X"4000236429025967010102010241103024014002012002794000230102010241",
                    INIT_14 => X"01200201020102413064390259B1400023597F0101020102411030280120028E",
                    INIT_15 => X"59B500010102C3FFBF00A0B10A0059A701010201024160915003502C06060130",
                    INIT_16 => X"020002FECFCFCFCF02FD02020100D0013E00000200F007070301000059B50003",
                    INIT_17 => X"000202CFCFCFCF0201CFCFCF02FD00CFCFCF02FECFCFCF02FDCFCF0201CFCF02",
                    INIT_18 => X"2306CF8000CFCF0202CF02FECFCF0201CF02FD00CFCFCF02FECFCF02010202CF",
                    INIT_19 => X"020002CFCFCF02FECFCF020102CF0202CF1D2F0E02FECFCF0201CF02022502FD",
                    INIT_1A => X"1C206B1C10691C00D3FE013020100000CF420E02FECFCF0201CF000202CF8000",
                    INIT_1B => X"8B1C00D3FE0120100000000680F253710471037102710100000600F26F1C306D",
                    INIT_1C => X"10B41C00D3FE01100000000680F25391039102910100000600F28F1C208D1C10",
                    INIT_1D => X"00000680F253BA03BA02BA01000030200600F200403F0D403FB81C0001DF6B1C",
                    INIT_1E => X"03400140DE03502A344030402C7C0323005964C003002E726F727265206B6341",
                    INIT_1F => X"F300C50200C50100C500005953305320CB974030402C7C022300CB7700CB51E4",
                    INIT_20 => X"00031F402C13012300596405040073746E656D756772612064614200F6F9FC00",
                    INIT_21 => X"59534053505360537001200201490285B2300606060600230123012301FFFFFF",
                    INIT_22 => X"8900595340535053605370012002014B02855230060606060040800000078900",
                    INIT_23 => X"00078900595340535053605370012002014C0285B03006060606004080000007",
                    INIT_24 => X"000000000000595340535053605370012002015A028520300606060600408000",
                    INIT_25 => X"060606B0108F7006060606C0855006060606B05953C002015002000001000000",
                    INIT_26 => X"060606B0FC2091503006060606B00320855006060606B0910000000120915006",
                    INIT_27 => X"B048B066B059534053505360537001200201540285B43006060606B091503006",
                    INIT_28 => X"A01F1FA60601071A061A8457B01A06138458B01A060D8456B01A060784B5B02A",
                    INIT_29 => X"0607595310100034053A0E01000E0E27E0020F2E0E2D10A000070000E0060606",
                    INIT_2A => X"62048540A3015C028540A3005601854000595991C07F855006060606B0060606",
                    INIT_2B => X"840209FFFF0009206D0A7301090009006E01FF006D098000A30368088540A302",
                    INIT_2C => X"40A87B00A67B00A47B73100F06060606060769000E0100060009100A7D8A0001",
                    INIT_2D => X"710201023A01025359010201023A01025201025201024533A932A931000E0050",
                    INIT_2E => X"0252010252010245000E00D87B7320733073100F06060606060669000E015953",
                    INIT_2F => X"208000080B01F5020B00000B020800FD01000E015953710201023A0102535901",
                    INIT_30 => X"0B20C01A200B0016020B0B020C10020B1A01000E01000E000B1001200B00000B",
                    INIT_31 => X"01800045370E08460080003D0100322D0E68018032270E6800802C010020200B",
                    INIT_32 => X"0B0B0B0B40200056200B000E4F020B0B0B0B04020048020B560100453F0E0846",
                    INIT_33 => X"06000E0079200B0073020B0B0B0B0002000E006A020B7901000E0E0E0E0E5D20",
                    INIT_34 => X"00C4030046200046200024F020000109EF20B6000086200B0B0B0B0020060606",
                    INIT_35 => X"00409641019F60682060B63000B630B600008D00B330B601AA30B6000E0E00A6",
                    INIT_36 => X"7C02230059537B0073102C7C012300595371008E0E20B0A0BD3020CDCDBD20C5",
                    INIT_37 => X"69766564206F4E00C51030102C100110347C0323005953305340EB8D1030102C",
                    INIT_38 => X"02014E02014F02014E0224EF102C7C0123005964F9065900646E756F66206563",
                    INIT_39 => X"002440301030102C7C02230059014402014E02015502014F0201460200590145",
                    INIT_3A => X"7C032300590E3024443024CC3009EF30102C7C0123005953103330102C7C0123",
                    INIT_3B => X"2430400124304001243040012440303424553009EF30102C7C08102A7C081029",
                    INIT_3C => X"3009EF30102C7C012300590E3024443024304001243040012430400124403030",
                    INIT_3D => X"09EF30102C7C08102A7C0810297C0323005953405310333010333024BE3024CC",
                    INIT_3E => X"0124304001243040012440303024304001243040012430400124403034245530",
                    INIT_3F => X"012002013A0253300F8000F8002301302C19808000333010333024BE30243040",
                   INITP_00 => X"7A3001F600DE429FC4C0924924107E001FFFE536008278000000008F80000001",
                   INITP_01 => X"8FFFFFFF4CB20C00000005F248E8A9C8283D106009801B4D004EB04089689000",
                   INITP_02 => X"618C3199B0F81C022968401F10131830C4C180D008400214FFFFFFFFFFF80000",
                   INITP_03 => X"0000E0379F400FFF02A21EE361015036C101540DB6147982A33B7CDF91B3619E",
                   INITP_04 => X"000008080A0281A403F8001110000406A000101A8000406A0001008D000FFFEF",
                   INITP_05 => X"020132B56D1D502CAD56DBF0EFD0206DEBB8EE28A28A120003D9158001451450",
                   INITP_06 => X"FEEE801D02A04240DA40044CB253900020004000000000022220000000000000",
                   INITP_07 => X"0030801111211122D88800096C02222422245B110096C01827000000003405FF")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_ll(31 downto 0),
                  DOPA => data_out_a_ll(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_ll(31 downto 0),
                  DOPB => data_out_b_ll(35 downto 32), 
                   DIB => data_in_b_ll(31 downto 0),
                  DIPB => data_in_b_ll(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_lh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481600",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"0050CA00508A68080000508A68080000508AA2A2A2A202680800680800000028",
                    INIT_05 => X"1E5E0E28108900F0E1091A5A28000050CA0050CA0050CA0050CA0050CA0050CA",
                    INIT_06 => X"A1A1A1A159288A897151287E8E1E5EF0CF8E8E70500F8E758E70088EA6A6A6A6",
                    INIT_07 => X"2870182850C9708972010302000000000A000000000A89797909795189790989",
                    INIT_08 => X"9068480308080308080902080928040A09040A09782878185803090309787808",
                    INIT_09 => X"886848287888580028007808B0E01818582800D0685828780890601048682808",
                    INIT_0A => X"00000000092800705858B0E85828787808280000F0C9F0C90909585801002800",
                    INIT_0B => X"06815859D0E8580128001090E98900B0E81800680069000050C9896808006808",
                    INIT_0C => X"0E0EB0E890E8582868086B6B6A6A68284B4B4A4A906848186808682800280106",
                    INIT_0D => X"00000000680800680800680800000068080068080000000EB0EE560E560E1056",
                    INIT_0E => X"28005008035088035088025088025008B0E85828B0CE8E000000000000000000",
                    INIT_0F => X"0008000D0D080808080808080808080808080808080808080808080808080808",
                    INIT_10 => X"0808080808080808080808080828000000680800000068080000006808000000",
                    INIT_11 => X"0D08080808080808080808080808080808080808080808080808080808080808",
                    INIT_12 => X"5800000D0D006808006800690000006808006808001188685000F1E05908000D",
                    INIT_13 => X"E1095A000D0D0011896800690000508008680800680800F1E1095A6800690000",
                    INIT_14 => X"680800680069000000000D0D00F1E1095A0011896800690000508008680800F1",
                    INIT_15 => X"00119D8D680091E891E8250D0D280011CB680069000050D1E38B038AA2A28902",
                    INIT_16 => X"2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A018B8B02800119D8D",
                    INIT_17 => X"286E2E010101016E2E0101016E1E280101016E1E0101016E1E01016E2E01016E",
                    INIT_18 => X"D1A2010A2801016E2E016E1E01016E2E016E1E280101016E1E01016E2E6E2E01",
                    INIT_19 => X"2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E01016E2E016E2E116E1E",
                    INIT_1A => X"0102D10102D101020118B8B1B1B0B02801F1A26E1E01016E2E01A26B4B010A0A",
                    INIT_1B => X"D101020118B8B1B0B028B8A0080100110811081108110828B8A00801D10102D1",
                    INIT_1C => X"02D101020118B8B0B028B8A008010011081108110828B8A00801D10102D10102",
                    INIT_1D => X"28B8A008010011081108110828B8B1B1A00801010101010101D101022801D101",
                    INIT_1E => X"EA518A51D1EA520A0A500A500AB0E8582800000D0D0808080808080808080808",
                    INIT_1F => X"08280108280108280108280000000000D101500A500AB0E85828D10128D101D1",
                    INIT_20 => X"286818500AB2E8582800000D0D080808080808080808080808080828010101B6",
                    INIT_21 => X"000000000000000000680800680800008818A0A0A0A028F2CEF2CFF2CF0F0F0E",
                    INIT_22 => X"DACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDBDACA",
                    INIT_23 => X"DBDBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDB",
                    INIT_24 => X"88CB88CA88CA000000000000000000680800680800008018A0A0A0A028E20AC8",
                    INIT_25 => X"A0A0A000221A18A0A0A0A0000088A0A0A0A000000000006808000D050E2888CB",
                    INIT_26 => X"A0A0A0001A02008818A0A0A0A0002A020088A0A0A0A000000B0B0A0A080088A0",
                    INIT_27 => X"0002000200000000000000000000680800680800008818A0A0A0A000008818A0",
                    INIT_28 => X"001DEDB2EE8EA512A59202090012A59202090012A59202090012A59202090002",
                    INIT_29 => X"A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E0100008092518A0A0A0",
                    INIT_2A => X"926A00080208926A00080208926A000828000000121A0088A0A0A0A000A6A6A6",
                    INIT_2B => X"B218480F0F2868080268921848284828F2CF0F28026F0F280208926A00080208",
                    INIT_2C => X"01D20202D20202D202022018A0A0A0A0A0A00228A10928A1096808481292DFCF",
                    INIT_2D => X"0200680008680008006900680008680008680008680008091209120928A00802",
                    INIT_2E => X"000868000868000828A008D20202000200022018A0A0A0A0A0A00228A1090000",
                    INIT_2F => X"282808139368926848080868282808D26828A109000002006800086800080068",
                    INIT_30 => X"68280893684828936848682808936848D36828A00828A0089368936848080868",
                    INIT_31 => X"08090813F3A1A003080908D3682813F3A103080913F3A1030809D36828936848",
                    INIT_32 => X"4848486828280893684828A0936848484868282808936848D3682813F3A1A003",
                    INIT_33 => X"A0A0A008936848289368484848682828A0A008936848D36828A0A0A0A0A09368",
                    INIT_34 => X"E893E8A00300A00300080308000A09F30200040B289368484848682828A0A0A0",
                    INIT_35 => X"EB03B3E989040003000013020BB302B3EBA3040BD3E3130BB3E313A3A0A00B93",
                    INIT_36 => X"B0E8582800000228025008B0E8582800000228130300010100050513130005B3",
                    INIT_37 => X"0808080808080828025008500851885108B0E858280000000000D30250085008",
                    INIT_38 => X"00680800680800680800D3025008B0E8582800000D0D00080808080808080808",
                    INIT_39 => X"2803000052085108B0E858280068080068080068080068080068080028006808",
                    INIT_3A => X"B0E85828000300030800030800F302005108B0E8582800000003005108B0E858",
                    INIT_3B => X"0300508A0300508A0300508A0350000A030800F302005108B0E85008B0E85008",
                    INIT_3C => X"00F302005108B0E858280003000308000300508A0300508A0300508A0350000A",
                    INIT_3D => X"F302005108B0E85008B0E85008B0E85828000000000003000203000308000308",
                    INIT_3E => X"8A0300508A0300508A0350000A0300508A0300508A0300508A0350000A030800",
                    INIT_3F => X"68080068080000001A5A09B3E8588A0259F46808280300020300030800030050",
                   INITP_00 => X"C2AF79A404B111401B0E492492CB016DCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"A7FFFFFFC00099B6DB6D6C8229BF08BF89F31F16E7277A3F99E8734492264A56",
                   INITP_02 => X"DF7BEF776FA6705CCEA77D00BF4C6F8B137C5B1F66FD6D627FFFFFFFFFFF5AD6",
                   INITP_03 => X"6DBAC27F0804E7FFCEAC36E76A675676D467559DB6A1D7445EE6EBB6AF6EDF75",
                   INITP_04 => X"56AAD8080601810400081B46AB556C090D55B0243556C090D55B0350C4E7FFFE",
                   INITP_05 => X"1A114FDBB69D4033F6FB6CA86DC0324C86E59DAAAAAAF2003141140011659659",
                   INITP_06 => X"FF804EB09F93F89D25252AA14921AC2009841282112844B919174D2C9324A4A1",
                   INITP_07 => X"B6104CA444444444C444EA5262748888888898889D262744C09DB6DEDB7273FF")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_lh(31 downto 0),
                  DOPA => data_out_a_lh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_lh(31 downto 0),
                  DOPB => data_out_b_lh(35 downto 32), 
                   DIB => data_in_b_lh(31 downto 0),
                  DIPB => data_in_b_lh(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      --
      kcpsm6_rom_hl: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"5901520152014500F840015953500153500B0120025010505350900606060630",
                    INIT_01 => X"243040248030248030244E3024CC3009EF30102C7F070707070700307C022300",
                    INIT_02 => X"24304001243040012440303424553009EF30102C7C08102A7C0810297C032300",
                    INIT_03 => X"53405310333010333024BE302430400124304001243040012440303024304001",
                    INIT_04 => X"01000E980E019D00B0070130A00E0E0E01304000B48C018C0080008B80800059",
                    INIT_05 => X"0100010001000100400000B010D0AC010607B200FEB0070130A00E0E0E013040",
                    INIT_06 => X"00E401EC0D0100DD00102C7C01230059CB010153C00707400000010001000100",
                    INIT_07 => X"00E4010C0D0100FD00102C617C0123E4EC1D010600590142014C0D00EC0D0201",
                    INIT_08 => X"0D020100E401310D01001A00102C0F617C01230C00590142014C0D000C0D0201",
                    INIT_09 => X"51526444554F754F504B414F3001067C590142014C0D0000590142014C0D0031",
                    INIT_0A => X"01065931010106020106290001063101064F810106310106010601063129015E",
                    INIT_0B => X"001C3C007C0D007701000D08007C017C0D006C01000D04007100102C7C012321",
                    INIT_0C => X"80100C07009001020C010C020202801007008301000C0C0C0C1C07070C07003C",
                    INIT_0D => X"8F7D050080828F7D050080B0010140828F7D9F038000029E010C0C010C020202",
                    INIT_0E => X"4000808F7D0400808F7D0600BD808F7DC7D500808F408F7D01D5000080C00C82",
                    INIT_0F => X"60404060034080F401014082808F40018F40018F408F7D0302407CE680016003",
                    INIT_10 => X"801A40018F4002408F027DD57C80017C0050034000400140010B400140010B40",
                    INIT_11 => X"0340000C00800340004001400137400140013740504040500340802001018F40",
                    INIT_12 => X"0120103056380101201034800606062C7C082A7C08297C01287C032300DF0080",
                    INIT_13 => X"7C012300C52C7C0123005953B7005953300153300153300280AB00CE005D3401",
                    INIT_14 => X"990707400153400780005953400153400153400240005983010101201003402C",
                    INIT_15 => X"646D656D78010067726169746C756D0059AA0101535080400340DF0059980959",
                    INIT_16 => X"FB01006E6F6973726576830100746573657253010072776D656D5B0100706D75",
                    INIT_17 => X"65725F633269D0030072775F6765725F633269B30200706C65683F0200737973",
                    INIT_18 => X"6361645F6C65735F633269F603006364745F6C65735F633269E7030064725F67",
                    INIT_19 => X"716164CD010072775F6765725F716164F9030062645F6C65735F633269FC0300",
                    INIT_1A => X"65746972775F636474CE06007375746174735F63647499010064725F6765725F",
                    INIT_1B => X"725F636474EC060072775F6765725F636474D90600646165725F636474D20600",
                    INIT_1C => X"646165725F706D65748208007364695F7465675F706D6574DD060064725F6765",
                    INIT_1D => X"007365725F7465735F706D6574EC0700746E6972705F706D657441080074756F",
                    INIT_1E => X"7766770A00777366730A00727366640A006566660A0069665005007264632108",
                    INIT_1F => X"3E0A007266380A0077668B0A00726166970A007277667D0A00776166440A0077",
                    INIT_20 => X"676F72705F63647461090074657365725F636474D208006C6C6568735F636474",
                    INIT_21 => X"5F656B61669901007264CD010077640D0900676F72705F636474F10800726E5F",
                    INIT_22 => X"580001600071FFA04900010158102058002071FF6400A000B10AFF2A01003563",
                    INIT_23 => X"2301772F01209F102023000060AC4F00604F10A0000110A00001C47549000003",
                    INIT_24 => X"0077000128230010012423208D01942F209420108D10200820100023239F0501",
                    INIT_25 => X"0102412064B10C00203A726F727245005964A00C00646E616D6D6F6320646142",
                    INIT_26 => X"E850CD02012C06064050104028004024EF002300C7200110002C043800590102",
                    INIT_27 => X"F046120A2020012010002001F008003D00CD702F60CD5001DC01701C01600160",
                    INIT_28 => X"20012023202F08280A280D000120F0604F5964000D590021776F6C667265764F",
                    INIT_29 => X"01202001200108020120020108023E0220000C0120012059000C000102000C00",
                    INIT_2A => X"00000000000000000000000000000000000000000000000000000000000C0020",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"9E3063F78E2044555540C841A8882000002222422245B1102925B00000E2403F",
                   INITP_01 => X"C336DF103DEFDFCFEFBBF3C03700B8340F39E791B9BB76AFFFFC7E7FF3E713FF",
                   INITP_02 => X"FFFFFFFFFFFFFFFFFFFE1220000000A5080849D2952000070C739DEB0CF6F739",
                   INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_04 => X"3F610044079C064203FE1FFF22F037B0008259824B40167FFFFFFFFFFFFFFFFF",
                   INITP_05 => X"00000000000000000000000000000000000000000000000006DA00101FF08BFF",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hl(31 downto 0),
                  DOPA => data_out_a_hl(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hl(31 downto 0),
                  DOPB => data_out_b_hl(35 downto 32), 
                   DIB => data_in_b_hl(31 downto 0),
                  DIPB => data_in_b_hl(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_hh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"0068086808680828B3E189000050CA00508A68080070285000508AA2A2A2A202",
                    INIT_01 => X"030000030800030800030800030800F3020051081AA2A2A2A2A25208B0E85828",
                    INIT_02 => X"0300508A0300508A0350000A030800F302005108B0E85008B0E85008B0E85828",
                    INIT_03 => X"0000000003000203000308000300508A0300508A0300508A0350000A0300508A",
                    INIT_04 => X"1828A514A5CD94ED551DCD0585A5A5A5CD050D2800030903097808D468582800",
                    INIT_05 => X"887088708870887008082876261614CDA0A694ED0E561DCD0585A5A5A5CD050D",
                    INIT_06 => X"08B4E914682808B4E95108B0E8582800F4CECE00500E8E0E2870887088708870",
                    INIT_07 => X"08B4E914682808B4E9510804B0E85814B4E84800280068086808680814682828",
                    INIT_08 => X"68282808B4E914682808B4E951080904B0E85814280068086808680814682828",
                    INIT_09 => X"E894E894E894E894E894E894E848001000680868086808280068086808680814",
                    INIT_0A => X"48001494C84900884800B4E8480014480094E84800144800480048001494C994",
                    INIT_0B => X"28585828146808F48808682808B0E9146808F48808682808B4E95108B0E85814",
                    INIT_0C => X"1901A00928F4C9A0693969A1A1A119010928F4C9A0A1A1A14958E9E958092858",
                    INIT_0D => X"0404082804040404082804B4CA8A70040404080A0A28A0F4C9A0693969A1A1A1",
                    INIT_0E => X"0B28040404082804040408280404040408042804040004040804022804D4A004",
                    INIT_0F => X"82520A538B0B04F4CB8A70040A0450CA0450CA04500404088A0A10D4EBCB538B",
                    INIT_10 => X"0AF5EACA04508A0A04080404F0EACA90EA528A0A28728A528AF5728A528AF572",
                    INIT_11 => X"8808280570088808287189518AF57189518AF57181510A528A0A04F5CA8A0450",
                    INIT_12 => X"88705008D5E8898870500889A1A1A159B0E858B0E858B0E858B0E85828047008",
                    INIT_13 => X"B0E858280458B0E8582800000428000050C90050C90050890904280428D5E889",
                    INIT_14 => X"B5E81800CA00508A0A28000050CA0050CA00508A0A2800B5C989887050090908",
                    INIT_15 => X"0808080808080808080808080808082800B5CA8A00500A528A0A042800F58A00",
                    INIT_16 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_17 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_18 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_19 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_20 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_21 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_22 => X"169D8D96E896E825169D8D89B6E050F6E15896E896E825090D0D080808080808",
                    INIT_23 => X"58C9F6008950F6E15878082800060028000021259D8D01259D8D060616099D8D",
                    INIT_24 => X"281671C88858C150C88858011689F6005096E101D6E15889017180087896E888",
                    INIT_25 => X"69000000000D0D08080808080808082800000D0D080808080808080808080808",
                    INIT_26 => X"D6E3D6CB8A8BA3A30383538008528008F6E2580AD6E088700808890928006800",
                    INIT_27 => X"16C606F6E878885870885848F66848002816700050B6E38B168B7000CB508B51",
                    INIT_28 => X"78C858F6E896E896E896E850C85816000000000D0D0008080808080808080808",
                    INIT_29 => X"C85878C858680800680800680800D6C85828A00878C8580028A008680028A008",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000028A00878",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"59199EAC591390D55532200052001D53A94888888889888992498009D5992C80",
                   INITP_01 => X"033496307BDF7B5DDF73C5280CA0604BFD25A4897266CD5D5553D5D58B219EAC",
                   INITP_02 => X"FFFFFFFFFFFFFFFFFFFFC83D847247109A7F247C4880924E38C6303248B90C63",
                   INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_04 => X"F489F4B0A000890FE9FFCFFFE00B484C325FF13895892A3FFFFFFFFFFFFFFFFF",
                   INITP_05 => X"00000000000000000000000000000000000000000000000925B6499C9543E7FF",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hh(31 downto 0),
                  DOPA => data_out_a_hh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hh(31 downto 0),
                  DOPB => data_out_b_hh(35 downto 32), 
                   DIB => data_in_b_hh(31 downto 0),
                  DIPB => data_in_b_hh(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400F042",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"5350015350080120025350010120025350900606060630012002013A02533000",
                    INIT_05 => X"0F804000B70181BC40000F800059535001535001535001535001535001535001",
                    INIT_06 => X"060606068100010140200080010F80CB010101D0C00701D001D0C09006060606",
                    INIT_07 => X"0020EF20200B20012050B020D6D6D6D630D6D6D6D634012A29082C2001230390",
                    INIT_08 => X"1D020B24440024CC0002EF0000002F01012F01008080810F808C018C00818000",
                    INIT_09 => X"00010E00810181DB0013810038000F0F8100002F80800081002330300B0B020C",
                    INIT_0A => X"0253413000005900302C7C0223002320000060FB48014801FFFF0131FF4F002A",
                    INIT_0B => X"75102B278204233F00595C76000459660003300102010241300104012002013A",
                    INIT_0C => X"2C017C01A202230003F408070605040008070605891003FF0308040042003FC4",
                    INIT_0D => X"0253700201780201300201200253C001780201300285C001A900D030C02CA9C0",
                    INIT_0E => X"0091002C1000011000011000011000307C022300A90101595340025350025360",
                    INIT_0F => X"850164E00100203A56206472616F426369676F4C2072656B6361725420322D67",
                    INIT_10 => X"64726F5700203A72656666754200595340012E025350012E025360012E025370",
                    INIT_11 => X"02002064726F5700202020203A73657A69732064726F5700203A746E756F6320",
                    INIT_12 => X"2302641C025901290201020102412001280201200244010110024B2020006413",
                    INIT_13 => X"4000236429025967010102010241103024014002012002794000230102010241",
                    INIT_14 => X"01200201020102413064390259B1400023597F0101020102411030280120028E",
                    INIT_15 => X"59B500010102C3FFBF00A0B10A0059A701010201024160915003502C06060130",
                    INIT_16 => X"020002FECFCFCFCF02FD02020100D0013E00000200F007070301000059B50003",
                    INIT_17 => X"000202CFCFCFCF0201CFCFCF02FD00CFCFCF02FECFCFCF02FDCFCF0201CFCF02",
                    INIT_18 => X"2306CF8000CFCF0202CF02FECFCF0201CF02FD00CFCFCF02FECFCF02010202CF",
                    INIT_19 => X"020002CFCFCF02FECFCF020102CF0202CF1D2F0E02FECFCF0201CF02022502FD",
                    INIT_1A => X"1C206B1C10691C00D3FE013020100000CF420E02FECFCF0201CF000202CF8000",
                    INIT_1B => X"8B1C00D3FE0120100000000680F253710471037102710100000600F26F1C306D",
                    INIT_1C => X"10B41C00D3FE01100000000680F25391039102910100000600F28F1C208D1C10",
                    INIT_1D => X"00000680F253BA03BA02BA01000030200600F200403F0D403FB81C0001DF6B1C",
                    INIT_1E => X"03400140DE03502A344030402C7C0323005964C003002E726F727265206B6341",
                    INIT_1F => X"F300C50200C50100C500005953305320CB974030402C7C022300CB7700CB51E4",
                    INIT_20 => X"00031F402C13012300596405040073746E656D756772612064614200F6F9FC00",
                    INIT_21 => X"59534053505360537001200201490285B2300606060600230123012301FFFFFF",
                    INIT_22 => X"8900595340535053605370012002014B02855230060606060040800000078900",
                    INIT_23 => X"00078900595340535053605370012002014C0285B03006060606004080000007",
                    INIT_24 => X"000000000000595340535053605370012002015A028520300606060600408000",
                    INIT_25 => X"060606B0108F7006060606C0855006060606B05953C002015002000001000000",
                    INIT_26 => X"060606B0FC2091503006060606B00320855006060606B0910000000120915006",
                    INIT_27 => X"B048B066B059534053505360537001200201540285B43006060606B091503006",
                    INIT_28 => X"A01F1FA60601071A061A8457B01A06138458B01A060D8456B01A060784B5B02A",
                    INIT_29 => X"0607595310100034053A0E01000E0E27E0020F2E0E2D10A000070000E0060606",
                    INIT_2A => X"62048540A3015C028540A3005601854000595991C07F855006060606B0060606",
                    INIT_2B => X"840209FFFF0009206D0A7301090009006E01FF006D098000A30368088540A302",
                    INIT_2C => X"40A87B00A67B00A47B73100F06060606060769000E0100060009100A7D8A0001",
                    INIT_2D => X"710201023A01025359010201023A01025201025201024533A932A931000E0050",
                    INIT_2E => X"0252010252010245000E00D87B7320733073100F06060606060669000E015953",
                    INIT_2F => X"208000080B01F5020B00000B020800FD01000E015953710201023A0102535901",
                    INIT_30 => X"0B20C01A200B0016020B0B020C10020B1A01000E01000E000B1001200B00000B",
                    INIT_31 => X"01800045370E08460080003D0100322D0E68018032270E6800802C010020200B",
                    INIT_32 => X"0B0B0B0B40200056200B000E4F020B0B0B0B04020048020B560100453F0E0846",
                    INIT_33 => X"06000E0079200B0073020B0B0B0B0002000E006A020B7901000E0E0E0E0E5D20",
                    INIT_34 => X"00C4030046200046200024F020000109EF20B6000086200B0B0B0B0020060606",
                    INIT_35 => X"00409641019F60682060B63000B630B600008D00B330B601AA30B6000E0E00A6",
                    INIT_36 => X"7C02230059537B0073102C7C012300595371008E0E20B0A0BD3020CDCDBD20C5",
                    INIT_37 => X"69766564206F4E00C51030102C100110347C0323005953305340EB8D1030102C",
                    INIT_38 => X"02014E02014F02014E0224EF102C7C0123005964F9065900646E756F66206563",
                    INIT_39 => X"002440301030102C7C02230059014402014E02015502014F0201460200590145",
                    INIT_3A => X"7C032300590E3024443024CC3009EF30102C7C0123005953103330102C7C0123",
                    INIT_3B => X"2430400124304001243040012440303424553009EF30102C7C08102A7C081029",
                    INIT_3C => X"3009EF30102C7C012300590E3024443024304001243040012430400124403030",
                    INIT_3D => X"09EF30102C7C08102A7C0810297C0323005953405310333010333024BE3024CC",
                    INIT_3E => X"0124304001243040012440303024304001243040012430400124403034245530",
                    INIT_3F => X"012002013A0253300F8000F8002301302C19808000333010333024BE30243040",
                    INIT_40 => X"5901520152014500F840015953500153500B0120025010505350900606060630",
                    INIT_41 => X"243040248030248030244E3024CC3009EF30102C7F070707070700307C022300",
                    INIT_42 => X"24304001243040012440303424553009EF30102C7C08102A7C0810297C032300",
                    INIT_43 => X"53405310333010333024BE302430400124304001243040012440303024304001",
                    INIT_44 => X"01000E980E019D00B0070130A00E0E0E01304000B48C018C0080008B80800059",
                    INIT_45 => X"0100010001000100400000B010D0AC010607B200FEB0070130A00E0E0E013040",
                    INIT_46 => X"00E401EC0D0100DD00102C7C01230059CB010153C00707400000010001000100",
                    INIT_47 => X"00E4010C0D0100FD00102C617C0123E4EC1D010600590142014C0D00EC0D0201",
                    INIT_48 => X"0D020100E401310D01001A00102C0F617C01230C00590142014C0D000C0D0201",
                    INIT_49 => X"51526444554F754F504B414F3001067C590142014C0D0000590142014C0D0031",
                    INIT_4A => X"01065931010106020106290001063101064F810106310106010601063129015E",
                    INIT_4B => X"001C3C007C0D007701000D08007C017C0D006C01000D04007100102C7C012321",
                    INIT_4C => X"80100C07009001020C010C020202801007008301000C0C0C0C1C07070C07003C",
                    INIT_4D => X"8F7D050080828F7D050080B0010140828F7D9F038000029E010C0C010C020202",
                    INIT_4E => X"4000808F7D0400808F7D0600BD808F7DC7D500808F408F7D01D5000080C00C82",
                    INIT_4F => X"60404060034080F401014082808F40018F40018F408F7D0302407CE680016003",
                    INIT_50 => X"801A40018F4002408F027DD57C80017C0050034000400140010B400140010B40",
                    INIT_51 => X"0340000C00800340004001400137400140013740504040500340802001018F40",
                    INIT_52 => X"0120103056380101201034800606062C7C082A7C08297C01287C032300DF0080",
                    INIT_53 => X"7C012300C52C7C0123005953B7005953300153300153300280AB00CE005D3401",
                    INIT_54 => X"990707400153400780005953400153400153400240005983010101201003402C",
                    INIT_55 => X"646D656D78010067726169746C756D0059AA0101535080400340DF0059980959",
                    INIT_56 => X"FB01006E6F6973726576830100746573657253010072776D656D5B0100706D75",
                    INIT_57 => X"65725F633269D0030072775F6765725F633269B30200706C65683F0200737973",
                    INIT_58 => X"6361645F6C65735F633269F603006364745F6C65735F633269E7030064725F67",
                    INIT_59 => X"716164CD010072775F6765725F716164F9030062645F6C65735F633269FC0300",
                    INIT_5A => X"65746972775F636474CE06007375746174735F63647499010064725F6765725F",
                    INIT_5B => X"725F636474EC060072775F6765725F636474D90600646165725F636474D20600",
                    INIT_5C => X"646165725F706D65748208007364695F7465675F706D6574DD060064725F6765",
                    INIT_5D => X"007365725F7465735F706D6574EC0700746E6972705F706D657441080074756F",
                    INIT_5E => X"7766770A00777366730A00727366640A006566660A0069665005007264632108",
                    INIT_5F => X"3E0A007266380A0077668B0A00726166970A007277667D0A00776166440A0077",
                    INIT_60 => X"676F72705F63647461090074657365725F636474D208006C6C6568735F636474",
                    INIT_61 => X"5F656B61669901007264CD010077640D0900676F72705F636474F10800726E5F",
                    INIT_62 => X"580001600071FFA04900010158102058002071FF6400A000B10AFF2A01003563",
                    INIT_63 => X"2301772F01209F102023000060AC4F00604F10A0000110A00001C47549000003",
                    INIT_64 => X"0077000128230010012423208D01942F209420108D10200820100023239F0501",
                    INIT_65 => X"0102412064B10C00203A726F727245005964A00C00646E616D6D6F6320646142",
                    INIT_66 => X"E850CD02012C06064050104028004024EF002300C7200110002C043800590102",
                    INIT_67 => X"F046120A2020012010002001F008003D00CD702F60CD5001DC01701C01600160",
                    INIT_68 => X"20012023202F08280A280D000120F0604F5964000D590021776F6C667265764F",
                    INIT_69 => X"01202001200108020120020108023E0220000C0120012059000C000102000C00",
                    INIT_6A => X"00000000000000000000000000000000000000000000000000000000000C0020",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"7A3001F600DE429FC4C0924924107E001FFFE536008278000000008F80000001",
                   INITP_01 => X"8FFFFFFF4CB20C00000005F248E8A9C8283D106009801B4D004EB04089689000",
                   INITP_02 => X"618C3199B0F81C022968401F10131830C4C180D008400214FFFFFFFFFFF80000",
                   INITP_03 => X"0000E0379F400FFF02A21EE361015036C101540DB6147982A33B7CDF91B3619E",
                   INITP_04 => X"000008080A0281A403F8001110000406A000101A8000406A0001008D000FFFEF",
                   INITP_05 => X"020132B56D1D502CAD56DBF0EFD0206DEBB8EE28A28A120003D9158001451450",
                   INITP_06 => X"FEEE801D02A04240DA40044CB253900020004000000000022220000000000000",
                   INITP_07 => X"0030801111211122D88800096C02222422245B110096C01827000000003405FF",
                   INITP_08 => X"9E3063F78E2044555540C841A8882000002222422245B1102925B00000E2403F",
                   INITP_09 => X"C336DF103DEFDFCFEFBBF3C03700B8340F39E791B9BB76AFFFFC7E7FF3E713FF",
                   INITP_0A => X"FFFFFFFFFFFFFFFFFFFE1220000000A5080849D2952000070C739DEB0CF6F739",
                   INITP_0B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0C => X"3F610044079C064203FE1FFF22F037B0008259824B40167FFFFFFFFFFFFFFFFF",
                   INITP_0D => X"00000000000000000000000000000000000000000000000006DA00101FF08BFF",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481600",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"0050CA00508A68080000508A68080000508AA2A2A2A202680800680800000028",
                    INIT_05 => X"1E5E0E28108900F0E1091A5A28000050CA0050CA0050CA0050CA0050CA0050CA",
                    INIT_06 => X"A1A1A1A159288A897151287E8E1E5EF0CF8E8E70500F8E758E70088EA6A6A6A6",
                    INIT_07 => X"2870182850C9708972010302000000000A000000000A89797909795189790989",
                    INIT_08 => X"9068480308080308080902080928040A09040A09782878185803090309787808",
                    INIT_09 => X"886848287888580028007808B0E01818582800D0685828780890601048682808",
                    INIT_0A => X"00000000092800705858B0E85828787808280000F0C9F0C90909585801002800",
                    INIT_0B => X"06815859D0E8580128001090E98900B0E81800680069000050C9896808006808",
                    INIT_0C => X"0E0EB0E890E8582868086B6B6A6A68284B4B4A4A906848186808682800280106",
                    INIT_0D => X"00000000680800680800680800000068080068080000000EB0EE560E560E1056",
                    INIT_0E => X"28005008035088035088025088025008B0E85828B0CE8E000000000000000000",
                    INIT_0F => X"0008000D0D080808080808080808080808080808080808080808080808080808",
                    INIT_10 => X"0808080808080808080808080828000000680800000068080000006808000000",
                    INIT_11 => X"0D08080808080808080808080808080808080808080808080808080808080808",
                    INIT_12 => X"5800000D0D006808006800690000006808006808001188685000F1E05908000D",
                    INIT_13 => X"E1095A000D0D0011896800690000508008680800680800F1E1095A6800690000",
                    INIT_14 => X"680800680069000000000D0D00F1E1095A0011896800690000508008680800F1",
                    INIT_15 => X"00119D8D680091E891E8250D0D280011CB680069000050D1E38B038AA2A28902",
                    INIT_16 => X"2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A018B8B02800119D8D",
                    INIT_17 => X"286E2E010101016E2E0101016E1E280101016E1E0101016E1E01016E2E01016E",
                    INIT_18 => X"D1A2010A2801016E2E016E1E01016E2E016E1E280101016E1E01016E2E6E2E01",
                    INIT_19 => X"2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E01016E2E016E2E116E1E",
                    INIT_1A => X"0102D10102D101020118B8B1B1B0B02801F1A26E1E01016E2E01A26B4B010A0A",
                    INIT_1B => X"D101020118B8B1B0B028B8A0080100110811081108110828B8A00801D10102D1",
                    INIT_1C => X"02D101020118B8B0B028B8A008010011081108110828B8A00801D10102D10102",
                    INIT_1D => X"28B8A008010011081108110828B8B1B1A00801010101010101D101022801D101",
                    INIT_1E => X"EA518A51D1EA520A0A500A500AB0E8582800000D0D0808080808080808080808",
                    INIT_1F => X"08280108280108280108280000000000D101500A500AB0E85828D10128D101D1",
                    INIT_20 => X"286818500AB2E8582800000D0D080808080808080808080808080828010101B6",
                    INIT_21 => X"000000000000000000680800680800008818A0A0A0A028F2CEF2CFF2CF0F0F0E",
                    INIT_22 => X"DACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDBDACA",
                    INIT_23 => X"DBDBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDB",
                    INIT_24 => X"88CB88CA88CA000000000000000000680800680800008018A0A0A0A028E20AC8",
                    INIT_25 => X"A0A0A000221A18A0A0A0A0000088A0A0A0A000000000006808000D050E2888CB",
                    INIT_26 => X"A0A0A0001A02008818A0A0A0A0002A020088A0A0A0A000000B0B0A0A080088A0",
                    INIT_27 => X"0002000200000000000000000000680800680800008818A0A0A0A000008818A0",
                    INIT_28 => X"001DEDB2EE8EA512A59202090012A59202090012A59202090012A59202090002",
                    INIT_29 => X"A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E0100008092518A0A0A0",
                    INIT_2A => X"926A00080208926A00080208926A000828000000121A0088A0A0A0A000A6A6A6",
                    INIT_2B => X"B218480F0F2868080268921848284828F2CF0F28026F0F280208926A00080208",
                    INIT_2C => X"01D20202D20202D202022018A0A0A0A0A0A00228A10928A1096808481292DFCF",
                    INIT_2D => X"0200680008680008006900680008680008680008680008091209120928A00802",
                    INIT_2E => X"000868000868000828A008D20202000200022018A0A0A0A0A0A00228A1090000",
                    INIT_2F => X"282808139368926848080868282808D26828A109000002006800086800080068",
                    INIT_30 => X"68280893684828936848682808936848D36828A00828A0089368936848080868",
                    INIT_31 => X"08090813F3A1A003080908D3682813F3A103080913F3A1030809D36828936848",
                    INIT_32 => X"4848486828280893684828A0936848484868282808936848D3682813F3A1A003",
                    INIT_33 => X"A0A0A008936848289368484848682828A0A008936848D36828A0A0A0A0A09368",
                    INIT_34 => X"E893E8A00300A00300080308000A09F30200040B289368484848682828A0A0A0",
                    INIT_35 => X"EB03B3E989040003000013020BB302B3EBA3040BD3E3130BB3E313A3A0A00B93",
                    INIT_36 => X"B0E8582800000228025008B0E8582800000228130300010100050513130005B3",
                    INIT_37 => X"0808080808080828025008500851885108B0E858280000000000D30250085008",
                    INIT_38 => X"00680800680800680800D3025008B0E8582800000D0D00080808080808080808",
                    INIT_39 => X"2803000052085108B0E858280068080068080068080068080068080028006808",
                    INIT_3A => X"B0E85828000300030800030800F302005108B0E8582800000003005108B0E858",
                    INIT_3B => X"0300508A0300508A0300508A0350000A030800F302005108B0E85008B0E85008",
                    INIT_3C => X"00F302005108B0E858280003000308000300508A0300508A0300508A0350000A",
                    INIT_3D => X"F302005108B0E85008B0E85008B0E85828000000000003000203000308000308",
                    INIT_3E => X"8A0300508A0300508A0350000A0300508A0300508A0300508A0350000A030800",
                    INIT_3F => X"68080068080000001A5A09B3E8588A0259F46808280300020300030800030050",
                    INIT_40 => X"0068086808680828B3E189000050CA00508A68080070285000508AA2A2A2A202",
                    INIT_41 => X"030000030800030800030800030800F3020051081AA2A2A2A2A25208B0E85828",
                    INIT_42 => X"0300508A0300508A0350000A030800F302005108B0E85008B0E85008B0E85828",
                    INIT_43 => X"0000000003000203000308000300508A0300508A0300508A0350000A0300508A",
                    INIT_44 => X"1828A514A5CD94ED551DCD0585A5A5A5CD050D2800030903097808D468582800",
                    INIT_45 => X"887088708870887008082876261614CDA0A694ED0E561DCD0585A5A5A5CD050D",
                    INIT_46 => X"08B4E914682808B4E95108B0E8582800F4CECE00500E8E0E2870887088708870",
                    INIT_47 => X"08B4E914682808B4E9510804B0E85814B4E84800280068086808680814682828",
                    INIT_48 => X"68282808B4E914682808B4E951080904B0E85814280068086808680814682828",
                    INIT_49 => X"E894E894E894E894E894E894E848001000680868086808280068086808680814",
                    INIT_4A => X"48001494C84900884800B4E8480014480094E84800144800480048001494C994",
                    INIT_4B => X"28585828146808F48808682808B0E9146808F48808682808B4E95108B0E85814",
                    INIT_4C => X"1901A00928F4C9A0693969A1A1A119010928F4C9A0A1A1A14958E9E958092858",
                    INIT_4D => X"0404082804040404082804B4CA8A70040404080A0A28A0F4C9A0693969A1A1A1",
                    INIT_4E => X"0B28040404082804040408280404040408042804040004040804022804D4A004",
                    INIT_4F => X"82520A538B0B04F4CB8A70040A0450CA0450CA04500404088A0A10D4EBCB538B",
                    INIT_50 => X"0AF5EACA04508A0A04080404F0EACA90EA528A0A28728A528AF5728A528AF572",
                    INIT_51 => X"8808280570088808287189518AF57189518AF57181510A528A0A04F5CA8A0450",
                    INIT_52 => X"88705008D5E8898870500889A1A1A159B0E858B0E858B0E858B0E85828047008",
                    INIT_53 => X"B0E858280458B0E8582800000428000050C90050C90050890904280428D5E889",
                    INIT_54 => X"B5E81800CA00508A0A28000050CA0050CA00508A0A2800B5C989887050090908",
                    INIT_55 => X"0808080808080808080808080808082800B5CA8A00500A528A0A042800F58A00",
                    INIT_56 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_57 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_58 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_59 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_60 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_61 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_62 => X"169D8D96E896E825169D8D89B6E050F6E15896E896E825090D0D080808080808",
                    INIT_63 => X"58C9F6008950F6E15878082800060028000021259D8D01259D8D060616099D8D",
                    INIT_64 => X"281671C88858C150C88858011689F6005096E101D6E15889017180087896E888",
                    INIT_65 => X"69000000000D0D08080808080808082800000D0D080808080808080808080808",
                    INIT_66 => X"D6E3D6CB8A8BA3A30383538008528008F6E2580AD6E088700808890928006800",
                    INIT_67 => X"16C606F6E878885870885848F66848002816700050B6E38B168B7000CB508B51",
                    INIT_68 => X"78C858F6E896E896E896E850C85816000000000D0D0008080808080808080808",
                    INIT_69 => X"C85878C858680800680800680800D6C85828A00878C8580028A008680028A008",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000028A00878",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"C2AF79A404B111401B0E492492CB016DCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"A7FFFFFFC00099B6DB6D6C8229BF08BF89F31F16E7277A3F99E8734492264A56",
                   INITP_02 => X"DF7BEF776FA6705CCEA77D00BF4C6F8B137C5B1F66FD6D627FFFFFFFFFFF5AD6",
                   INITP_03 => X"6DBAC27F0804E7FFCEAC36E76A675676D467559DB6A1D7445EE6EBB6AF6EDF75",
                   INITP_04 => X"56AAD8080601810400081B46AB556C090D55B0243556C090D55B0350C4E7FFFE",
                   INITP_05 => X"1A114FDBB69D4033F6FB6CA86DC0324C86E59DAAAAAAF2003141140011659659",
                   INITP_06 => X"FF804EB09F93F89D25252AA14921AC2009841282112844B919174D2C9324A4A1",
                   INITP_07 => X"B6104CA444444444C444EA5262748888888898889D262744C09DB6DEDB7273FF",
                   INITP_08 => X"59199EAC591390D55532200052001D53A94888888889888992498009D5992C80",
                   INITP_09 => X"033496307BDF7B5DDF73C5280CA0604BFD25A4897266CD5D5553D5D58B219EAC",
                   INITP_0A => X"FFFFFFFFFFFFFFFFFFFFC83D847247109A7F247C4880924E38C6303248B90C63",
                   INITP_0B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0C => X"F489F4B0A000890FE9FFCFFFE00B484C325FF13895892A3FFFFFFFFFFFFFFFFF",
                   INITP_0D => X"00000000000000000000000000000000000000000000000925B6499C9543E7FF",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400F042",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"5350015350080120025350010120025350900606060630012002013A02533000",
                    INIT_05 => X"0F804000B70181BC40000F800059535001535001535001535001535001535001",
                    INIT_06 => X"060606068100010140200080010F80CB010101D0C00701D001D0C09006060606",
                    INIT_07 => X"0020EF20200B20012050B020D6D6D6D630D6D6D6D634012A29082C2001230390",
                    INIT_08 => X"1D020B24440024CC0002EF0000002F01012F01008080810F808C018C00818000",
                    INIT_09 => X"00010E00810181DB0013810038000F0F8100002F80800081002330300B0B020C",
                    INIT_0A => X"0253413000005900302C7C0223002320000060FB48014801FFFF0131FF4F002A",
                    INIT_0B => X"75102B278204233F00595C76000459660003300102010241300104012002013A",
                    INIT_0C => X"2C017C01A202230003F408070605040008070605891003FF0308040042003FC4",
                    INIT_0D => X"0253700201780201300201200253C001780201300285C001A900D030C02CA9C0",
                    INIT_0E => X"0091002C1000011000011000011000307C022300A90101595340025350025360",
                    INIT_0F => X"850164E00100203A56206472616F426369676F4C2072656B6361725420322D67",
                    INIT_10 => X"64726F5700203A72656666754200595340012E025350012E025360012E025370",
                    INIT_11 => X"02002064726F5700202020203A73657A69732064726F5700203A746E756F6320",
                    INIT_12 => X"2302641C025901290201020102412001280201200244010110024B2020006413",
                    INIT_13 => X"4000236429025967010102010241103024014002012002794000230102010241",
                    INIT_14 => X"01200201020102413064390259B1400023597F0101020102411030280120028E",
                    INIT_15 => X"59B500010102C3FFBF00A0B10A0059A701010201024160915003502C06060130",
                    INIT_16 => X"020002FECFCFCFCF02FD02020100D0013E00000200F007070301000059B50003",
                    INIT_17 => X"000202CFCFCFCF0201CFCFCF02FD00CFCFCF02FECFCFCF02FDCFCF0201CFCF02",
                    INIT_18 => X"2306CF8000CFCF0202CF02FECFCF0201CF02FD00CFCFCF02FECFCF02010202CF",
                    INIT_19 => X"020002CFCFCF02FECFCF020102CF0202CF1D2F0E02FECFCF0201CF02022502FD",
                    INIT_1A => X"1C206B1C10691C00D3FE013020100000CF420E02FECFCF0201CF000202CF8000",
                    INIT_1B => X"8B1C00D3FE0120100000000680F253710471037102710100000600F26F1C306D",
                    INIT_1C => X"10B41C00D3FE01100000000680F25391039102910100000600F28F1C208D1C10",
                    INIT_1D => X"00000680F253BA03BA02BA01000030200600F200403F0D403FB81C0001DF6B1C",
                    INIT_1E => X"03400140DE03502A344030402C7C0323005964C003002E726F727265206B6341",
                    INIT_1F => X"F300C50200C50100C500005953305320CB974030402C7C022300CB7700CB51E4",
                    INIT_20 => X"00031F402C13012300596405040073746E656D756772612064614200F6F9FC00",
                    INIT_21 => X"59534053505360537001200201490285B2300606060600230123012301FFFFFF",
                    INIT_22 => X"8900595340535053605370012002014B02855230060606060040800000078900",
                    INIT_23 => X"00078900595340535053605370012002014C0285B03006060606004080000007",
                    INIT_24 => X"000000000000595340535053605370012002015A028520300606060600408000",
                    INIT_25 => X"060606B0108F7006060606C0855006060606B05953C002015002000001000000",
                    INIT_26 => X"060606B0FC2091503006060606B00320855006060606B0910000000120915006",
                    INIT_27 => X"B048B066B059534053505360537001200201540285B43006060606B091503006",
                    INIT_28 => X"A01F1FA60601071A061A8457B01A06138458B01A060D8456B01A060784B5B02A",
                    INIT_29 => X"0607595310100034053A0E01000E0E27E0020F2E0E2D10A000070000E0060606",
                    INIT_2A => X"62048540A3015C028540A3005601854000595991C07F855006060606B0060606",
                    INIT_2B => X"840209FFFF0009206D0A7301090009006E01FF006D098000A30368088540A302",
                    INIT_2C => X"40A87B00A67B00A47B73100F06060606060769000E0100060009100A7D8A0001",
                    INIT_2D => X"710201023A01025359010201023A01025201025201024533A932A931000E0050",
                    INIT_2E => X"0252010252010245000E00D87B7320733073100F06060606060669000E015953",
                    INIT_2F => X"208000080B01F5020B00000B020800FD01000E015953710201023A0102535901",
                    INIT_30 => X"0B20C01A200B0016020B0B020C10020B1A01000E01000E000B1001200B00000B",
                    INIT_31 => X"01800045370E08460080003D0100322D0E68018032270E6800802C010020200B",
                    INIT_32 => X"0B0B0B0B40200056200B000E4F020B0B0B0B04020048020B560100453F0E0846",
                    INIT_33 => X"06000E0079200B0073020B0B0B0B0002000E006A020B7901000E0E0E0E0E5D20",
                    INIT_34 => X"00C4030046200046200024F020000109EF20B6000086200B0B0B0B0020060606",
                    INIT_35 => X"00409641019F60682060B63000B630B600008D00B330B601AA30B6000E0E00A6",
                    INIT_36 => X"7C02230059537B0073102C7C012300595371008E0E20B0A0BD3020CDCDBD20C5",
                    INIT_37 => X"69766564206F4E00C51030102C100110347C0323005953305340EB8D1030102C",
                    INIT_38 => X"02014E02014F02014E0224EF102C7C0123005964F9065900646E756F66206563",
                    INIT_39 => X"002440301030102C7C02230059014402014E02015502014F0201460200590145",
                    INIT_3A => X"7C032300590E3024443024CC3009EF30102C7C0123005953103330102C7C0123",
                    INIT_3B => X"2430400124304001243040012440303424553009EF30102C7C08102A7C081029",
                    INIT_3C => X"3009EF30102C7C012300590E3024443024304001243040012430400124403030",
                    INIT_3D => X"09EF30102C7C08102A7C0810297C0323005953405310333010333024BE3024CC",
                    INIT_3E => X"0124304001243040012440303024304001243040012430400124403034245530",
                    INIT_3F => X"012002013A0253300F8000F8002301302C19808000333010333024BE30243040",
                    INIT_40 => X"5901520152014500F840015953500153500B0120025010505350900606060630",
                    INIT_41 => X"243040248030248030244E3024CC3009EF30102C7F070707070700307C022300",
                    INIT_42 => X"24304001243040012440303424553009EF30102C7C08102A7C0810297C032300",
                    INIT_43 => X"53405310333010333024BE302430400124304001243040012440303024304001",
                    INIT_44 => X"01000E980E019D00B0070130A00E0E0E01304000B48C018C0080008B80800059",
                    INIT_45 => X"0100010001000100400000B010D0AC010607B200FEB0070130A00E0E0E013040",
                    INIT_46 => X"00E401EC0D0100DD00102C7C01230059CB010153C00707400000010001000100",
                    INIT_47 => X"00E4010C0D0100FD00102C617C0123E4EC1D010600590142014C0D00EC0D0201",
                    INIT_48 => X"0D020100E401310D01001A00102C0F617C01230C00590142014C0D000C0D0201",
                    INIT_49 => X"51526444554F754F504B414F3001067C590142014C0D0000590142014C0D0031",
                    INIT_4A => X"01065931010106020106290001063101064F810106310106010601063129015E",
                    INIT_4B => X"001C3C007C0D007701000D08007C017C0D006C01000D04007100102C7C012321",
                    INIT_4C => X"80100C07009001020C010C020202801007008301000C0C0C0C1C07070C07003C",
                    INIT_4D => X"8F7D050080828F7D050080B0010140828F7D9F038000029E010C0C010C020202",
                    INIT_4E => X"4000808F7D0400808F7D0600BD808F7DC7D500808F408F7D01D5000080C00C82",
                    INIT_4F => X"60404060034080F401014082808F40018F40018F408F7D0302407CE680016003",
                    INIT_50 => X"801A40018F4002408F027DD57C80017C0050034000400140010B400140010B40",
                    INIT_51 => X"0340000C00800340004001400137400140013740504040500340802001018F40",
                    INIT_52 => X"0120103056380101201034800606062C7C082A7C08297C01287C032300DF0080",
                    INIT_53 => X"7C012300C52C7C0123005953B7005953300153300153300280AB00CE005D3401",
                    INIT_54 => X"990707400153400780005953400153400153400240005983010101201003402C",
                    INIT_55 => X"646D656D78010067726169746C756D0059AA0101535080400340DF0059980959",
                    INIT_56 => X"FB01006E6F6973726576830100746573657253010072776D656D5B0100706D75",
                    INIT_57 => X"65725F633269D0030072775F6765725F633269B30200706C65683F0200737973",
                    INIT_58 => X"6361645F6C65735F633269F603006364745F6C65735F633269E7030064725F67",
                    INIT_59 => X"716164CD010072775F6765725F716164F9030062645F6C65735F633269FC0300",
                    INIT_5A => X"65746972775F636474CE06007375746174735F63647499010064725F6765725F",
                    INIT_5B => X"725F636474EC060072775F6765725F636474D90600646165725F636474D20600",
                    INIT_5C => X"646165725F706D65748208007364695F7465675F706D6574DD060064725F6765",
                    INIT_5D => X"007365725F7465735F706D6574EC0700746E6972705F706D657441080074756F",
                    INIT_5E => X"7766770A00777366730A00727366640A006566660A0069665005007264632108",
                    INIT_5F => X"3E0A007266380A0077668B0A00726166970A007277667D0A00776166440A0077",
                    INIT_60 => X"676F72705F63647461090074657365725F636474D208006C6C6568735F636474",
                    INIT_61 => X"5F656B61669901007264CD010077640D0900676F72705F636474F10800726E5F",
                    INIT_62 => X"580001600071FFA04900010158102058002071FF6400A000B10AFF2A01003563",
                    INIT_63 => X"2301772F01209F102023000060AC4F00604F10A0000110A00001C47549000003",
                    INIT_64 => X"0077000128230010012423208D01942F209420108D10200820100023239F0501",
                    INIT_65 => X"0102412064B10C00203A726F727245005964A00C00646E616D6D6F6320646142",
                    INIT_66 => X"E850CD02012C06064050104028004024EF002300C7200110002C043800590102",
                    INIT_67 => X"F046120A2020012010002001F008003D00CD702F60CD5001DC01701C01600160",
                    INIT_68 => X"20012023202F08280A280D000120F0604F5964000D590021776F6C667265764F",
                    INIT_69 => X"01202001200108020120020108023E0220000C0120012059000C000102000C00",
                    INIT_6A => X"00000000000000000000000000000000000000000000000000000000000C0020",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"7A3001F600DE429FC4C0924924107E001FFFE536008278000000008F80000001",
                   INITP_01 => X"8FFFFFFF4CB20C00000005F248E8A9C8283D106009801B4D004EB04089689000",
                   INITP_02 => X"618C3199B0F81C022968401F10131830C4C180D008400214FFFFFFFFFFF80000",
                   INITP_03 => X"0000E0379F400FFF02A21EE361015036C101540DB6147982A33B7CDF91B3619E",
                   INITP_04 => X"000008080A0281A403F8001110000406A000101A8000406A0001008D000FFFEF",
                   INITP_05 => X"020132B56D1D502CAD56DBF0EFD0206DEBB8EE28A28A120003D9158001451450",
                   INITP_06 => X"FEEE801D02A04240DA40044CB253900020004000000000022220000000000000",
                   INITP_07 => X"0030801111211122D88800096C02222422245B110096C01827000000003405FF",
                   INITP_08 => X"9E3063F78E2044555540C841A8882000002222422245B1102925B00000E2403F",
                   INITP_09 => X"C336DF103DEFDFCFEFBBF3C03700B8340F39E791B9BB76AFFFFC7E7FF3E713FF",
                   INITP_0A => X"FFFFFFFFFFFFFFFFFFFE1220000000A5080849D2952000070C739DEB0CF6F739",
                   INITP_0B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0C => X"3F610044079C064203FE1FFF22F037B0008259824B40167FFFFFFFFFFFFFFFFF",
                   INITP_0D => X"00000000000000000000000000000000000000000000000006DA00101FF08BFF",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481600",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"0050CA00508A68080000508A68080000508AA2A2A2A202680800680800000028",
                    INIT_05 => X"1E5E0E28108900F0E1091A5A28000050CA0050CA0050CA0050CA0050CA0050CA",
                    INIT_06 => X"A1A1A1A159288A897151287E8E1E5EF0CF8E8E70500F8E758E70088EA6A6A6A6",
                    INIT_07 => X"2870182850C9708972010302000000000A000000000A89797909795189790989",
                    INIT_08 => X"9068480308080308080902080928040A09040A09782878185803090309787808",
                    INIT_09 => X"886848287888580028007808B0E01818582800D0685828780890601048682808",
                    INIT_0A => X"00000000092800705858B0E85828787808280000F0C9F0C90909585801002800",
                    INIT_0B => X"06815859D0E8580128001090E98900B0E81800680069000050C9896808006808",
                    INIT_0C => X"0E0EB0E890E8582868086B6B6A6A68284B4B4A4A906848186808682800280106",
                    INIT_0D => X"00000000680800680800680800000068080068080000000EB0EE560E560E1056",
                    INIT_0E => X"28005008035088035088025088025008B0E85828B0CE8E000000000000000000",
                    INIT_0F => X"0008000D0D080808080808080808080808080808080808080808080808080808",
                    INIT_10 => X"0808080808080808080808080828000000680800000068080000006808000000",
                    INIT_11 => X"0D08080808080808080808080808080808080808080808080808080808080808",
                    INIT_12 => X"5800000D0D006808006800690000006808006808001188685000F1E05908000D",
                    INIT_13 => X"E1095A000D0D0011896800690000508008680800680800F1E1095A6800690000",
                    INIT_14 => X"680800680069000000000D0D00F1E1095A0011896800690000508008680800F1",
                    INIT_15 => X"00119D8D680091E891E8250D0D280011CB680069000050D1E38B038AA2A28902",
                    INIT_16 => X"2E286E1E010101016E1E6E2E2E28F1CF0F28B86E261EA0A018B8B02800119D8D",
                    INIT_17 => X"286E2E010101016E2E0101016E1E280101016E1E0101016E1E01016E2E01016E",
                    INIT_18 => X"D1A2010A2801016E2E016E1E01016E2E016E1E280101016E1E01016E2E6E2E01",
                    INIT_19 => X"2E286A0101016E1E01016E2E4A016E2E0111D1A26E1E01016E2E016E2E116E1E",
                    INIT_1A => X"0102D10102D101020118B8B1B1B0B02801F1A26E1E01016E2E01A26B4B010A0A",
                    INIT_1B => X"D101020118B8B1B0B028B8A0080100110811081108110828B8A00801D10102D1",
                    INIT_1C => X"02D101020118B8B0B028B8A008010011081108110828B8A00801D10102D10102",
                    INIT_1D => X"28B8A008010011081108110828B8B1B1A00801010101010101D101022801D101",
                    INIT_1E => X"EA518A51D1EA520A0A500A500AB0E8582800000D0D0808080808080808080808",
                    INIT_1F => X"08280108280108280108280000000000D101500A500AB0E85828D10128D101D1",
                    INIT_20 => X"286818500AB2E8582800000D0D080808080808080808080808080828010101B6",
                    INIT_21 => X"000000000000000000680800680800008818A0A0A0A028F2CEF2CFF2CF0F0F0E",
                    INIT_22 => X"DACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDBDACA",
                    INIT_23 => X"DBDBDACA000000000000000000680800680800008818A0A0A0A028E20AC8DBDB",
                    INIT_24 => X"88CB88CA88CA000000000000000000680800680800008018A0A0A0A028E20AC8",
                    INIT_25 => X"A0A0A000221A18A0A0A0A0000088A0A0A0A000000000006808000D050E2888CB",
                    INIT_26 => X"A0A0A0001A02008818A0A0A0A0002A020088A0A0A0A000000B0B0A0A080088A0",
                    INIT_27 => X"0002000200000000000000000000680800680800008818A0A0A0A000008818A0",
                    INIT_28 => X"001DEDB2EE8EA512A59202090012A59202090012A59202090012A59202090002",
                    INIT_29 => X"A61E0000000608D2E8D2A18808A1A1B2E8A0A112A192E0100008092518A0A0A0",
                    INIT_2A => X"926A00080208926A00080208926A000828000000121A0088A0A0A0A000A6A6A6",
                    INIT_2B => X"B218480F0F2868080268921848284828F2CF0F28026F0F280208926A00080208",
                    INIT_2C => X"01D20202D20202D202022018A0A0A0A0A0A00228A10928A1096808481292DFCF",
                    INIT_2D => X"0200680008680008006900680008680008680008680008091209120928A00802",
                    INIT_2E => X"000868000868000828A008D20202000200022018A0A0A0A0A0A00228A1090000",
                    INIT_2F => X"282808139368926848080868282808D26828A109000002006800086800080068",
                    INIT_30 => X"68280893684828936848682808936848D36828A00828A0089368936848080868",
                    INIT_31 => X"08090813F3A1A003080908D3682813F3A103080913F3A1030809D36828936848",
                    INIT_32 => X"4848486828280893684828A0936848484868282808936848D3682813F3A1A003",
                    INIT_33 => X"A0A0A008936848289368484848682828A0A008936848D36828A0A0A0A0A09368",
                    INIT_34 => X"E893E8A00300A00300080308000A09F30200040B289368484848682828A0A0A0",
                    INIT_35 => X"EB03B3E989040003000013020BB302B3EBA3040BD3E3130BB3E313A3A0A00B93",
                    INIT_36 => X"B0E8582800000228025008B0E8582800000228130300010100050513130005B3",
                    INIT_37 => X"0808080808080828025008500851885108B0E858280000000000D30250085008",
                    INIT_38 => X"00680800680800680800D3025008B0E8582800000D0D00080808080808080808",
                    INIT_39 => X"2803000052085108B0E858280068080068080068080068080068080028006808",
                    INIT_3A => X"B0E85828000300030800030800F302005108B0E8582800000003005108B0E858",
                    INIT_3B => X"0300508A0300508A0300508A0350000A030800F302005108B0E85008B0E85008",
                    INIT_3C => X"00F302005108B0E858280003000308000300508A0300508A0300508A0350000A",
                    INIT_3D => X"F302005108B0E85008B0E85008B0E85828000000000003000203000308000308",
                    INIT_3E => X"8A0300508A0300508A0350000A0300508A0300508A0300508A0350000A030800",
                    INIT_3F => X"68080068080000001A5A09B3E8588A0259F46808280300020300030800030050",
                    INIT_40 => X"0068086808680828B3E189000050CA00508A68080070285000508AA2A2A2A202",
                    INIT_41 => X"030000030800030800030800030800F3020051081AA2A2A2A2A25208B0E85828",
                    INIT_42 => X"0300508A0300508A0350000A030800F302005108B0E85008B0E85008B0E85828",
                    INIT_43 => X"0000000003000203000308000300508A0300508A0300508A0350000A0300508A",
                    INIT_44 => X"1828A514A5CD94ED551DCD0585A5A5A5CD050D2800030903097808D468582800",
                    INIT_45 => X"887088708870887008082876261614CDA0A694ED0E561DCD0585A5A5A5CD050D",
                    INIT_46 => X"08B4E914682808B4E95108B0E8582800F4CECE00500E8E0E2870887088708870",
                    INIT_47 => X"08B4E914682808B4E9510804B0E85814B4E84800280068086808680814682828",
                    INIT_48 => X"68282808B4E914682808B4E951080904B0E85814280068086808680814682828",
                    INIT_49 => X"E894E894E894E894E894E894E848001000680868086808280068086808680814",
                    INIT_4A => X"48001494C84900884800B4E8480014480094E84800144800480048001494C994",
                    INIT_4B => X"28585828146808F48808682808B0E9146808F48808682808B4E95108B0E85814",
                    INIT_4C => X"1901A00928F4C9A0693969A1A1A119010928F4C9A0A1A1A14958E9E958092858",
                    INIT_4D => X"0404082804040404082804B4CA8A70040404080A0A28A0F4C9A0693969A1A1A1",
                    INIT_4E => X"0B28040404082804040408280404040408042804040004040804022804D4A004",
                    INIT_4F => X"82520A538B0B04F4CB8A70040A0450CA0450CA04500404088A0A10D4EBCB538B",
                    INIT_50 => X"0AF5EACA04508A0A04080404F0EACA90EA528A0A28728A528AF5728A528AF572",
                    INIT_51 => X"8808280570088808287189518AF57189518AF57181510A528A0A04F5CA8A0450",
                    INIT_52 => X"88705008D5E8898870500889A1A1A159B0E858B0E858B0E858B0E85828047008",
                    INIT_53 => X"B0E858280458B0E8582800000428000050C90050C90050890904280428D5E889",
                    INIT_54 => X"B5E81800CA00508A0A28000050CA0050CA00508A0A2800B5C989887050090908",
                    INIT_55 => X"0808080808080808080808080808082800B5CA8A00500A528A0A042800F58A00",
                    INIT_56 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_57 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_58 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_59 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_5F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_60 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_61 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_62 => X"169D8D96E896E825169D8D89B6E050F6E15896E896E825090D0D080808080808",
                    INIT_63 => X"58C9F6008950F6E15878082800060028000021259D8D01259D8D060616099D8D",
                    INIT_64 => X"281671C88858C150C88858011689F6005096E101D6E15889017180087896E888",
                    INIT_65 => X"69000000000D0D08080808080808082800000D0D080808080808080808080808",
                    INIT_66 => X"D6E3D6CB8A8BA3A30383538008528008F6E2580AD6E088700808890928006800",
                    INIT_67 => X"16C606F6E878885870885848F66848002816700050B6E38B168B7000CB508B51",
                    INIT_68 => X"78C858F6E896E896E896E850C85816000000000D0D0008080808080808080808",
                    INIT_69 => X"C85878C858680800680800680800D6C85828A00878C8580028A008680028A008",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000028A00878",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"C2AF79A404B111401B0E492492CB016DCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"A7FFFFFFC00099B6DB6D6C8229BF08BF89F31F16E7277A3F99E8734492264A56",
                   INITP_02 => X"DF7BEF776FA6705CCEA77D00BF4C6F8B137C5B1F66FD6D627FFFFFFFFFFF5AD6",
                   INITP_03 => X"6DBAC27F0804E7FFCEAC36E76A675676D467559DB6A1D7445EE6EBB6AF6EDF75",
                   INITP_04 => X"56AAD8080601810400081B46AB556C090D55B0243556C090D55B0350C4E7FFFE",
                   INITP_05 => X"1A114FDBB69D4033F6FB6CA86DC0324C86E59DAAAAAAF2003141140011659659",
                   INITP_06 => X"FF804EB09F93F89D25252AA14921AC2009841282112844B919174D2C9324A4A1",
                   INITP_07 => X"B6104CA444444444C444EA5262748888888898889D262744C09DB6DEDB7273FF",
                   INITP_08 => X"59199EAC591390D55532200052001D53A94888888889888992498009D5992C80",
                   INITP_09 => X"033496307BDF7B5DDF73C5280CA0604BFD25A4897266CD5D5553D5D58B219EAC",
                   INITP_0A => X"FFFFFFFFFFFFFFFFFFFFC83D847247109A7F247C4880924E38C6303248B90C63",
                   INITP_0B => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_0C => X"F489F4B0A000890FE9FFCFFFE00B484C325FF13895892A3FFFFFFFFFFFFFFFFF",
                   INITP_0D => X"00000000000000000000000000000000000000000000000925B6499C9543E7FF",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_4k_generate;	              
  --
  --
  --
  --
  -- JTAG Loader
  --
  instantiate_loader : if (C_JTAG_LOADER_ENABLE = 1) generate
  --
    jtag_loader_6_inst : jtag_loader_6
    generic map(              C_FAMILY => C_FAMILY,
                       C_NUM_PICOBLAZE => 1,
                  C_JTAG_LOADER_ENABLE => C_JTAG_LOADER_ENABLE,
                 C_BRAM_MAX_ADDR_WIDTH => BRAM_ADDRESS_WIDTH,
	                  C_ADDR_WIDTH_0 => BRAM_ADDRESS_WIDTH)
    port map( picoblaze_reset => rdl_bus,
                      jtag_en => jtag_en,
                     jtag_din => jtag_din,
                    jtag_addr => jtag_addr(BRAM_ADDRESS_WIDTH-1 downto 0),
                     jtag_clk => jtag_clk,
                      jtag_we => jtag_we,
                  jtag_dout_0 => jtag_dout,
                  jtag_dout_1 => jtag_dout, -- ports 1-7 are not used
                  jtag_dout_2 => jtag_dout, -- in a 1 device debug 
                  jtag_dout_3 => jtag_dout, -- session.  However, Synplify
                  jtag_dout_4 => jtag_dout, -- etc require all ports to
                  jtag_dout_5 => jtag_dout, -- be connected
                  jtag_dout_6 => jtag_dout,
                  jtag_dout_7 => jtag_dout);
    --  
  end generate instantiate_loader;
  --
end low_level_definition;
--
--
-------------------------------------------------------------------------------------------
--
-- JTAG Loader 
--
-------------------------------------------------------------------------------------------
--
--
-- JTAG Loader 6 - Version 6.00
-- Kris Chaplin 4 February 2010
-- Ken Chapman 15 August 2011 - Revised coding style
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--
library unisim;
use unisim.vcomponents.all;
--
entity jtag_loader_6 is
generic(              C_JTAG_LOADER_ENABLE : integer := 1;
                                  C_FAMILY : string := "V6";
                           C_NUM_PICOBLAZE : integer := 1;
                     C_BRAM_MAX_ADDR_WIDTH : integer := 10;
        C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                              C_JTAG_CHAIN : integer := 2;
                            C_ADDR_WIDTH_0 : integer := 10;
                            C_ADDR_WIDTH_1 : integer := 10;
                            C_ADDR_WIDTH_2 : integer := 10;
                            C_ADDR_WIDTH_3 : integer := 10;
                            C_ADDR_WIDTH_4 : integer := 10;
                            C_ADDR_WIDTH_5 : integer := 10;
                            C_ADDR_WIDTH_6 : integer := 10;
                            C_ADDR_WIDTH_7 : integer := 10);
port(   picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
               jtag_din : out std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
              jtag_addr : out std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0) := (others => '0');
               jtag_clk : out std_logic := '0';
                jtag_we : out std_logic := '0';
            jtag_dout_0 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_1 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_2 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_3 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_4 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_5 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_6 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_7 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end jtag_loader_6;
--
architecture Behavioral of jtag_loader_6 is
  --
  signal num_picoblaze       : std_logic_vector(2 downto 0);
  signal picoblaze_instruction_data_width : std_logic_vector(4 downto 0);
  --
  signal drck                : std_logic;
  signal shift_clk           : std_logic;
  signal shift_din           : std_logic;
  signal shift_dout          : std_logic;
  signal shift               : std_logic;
  signal capture             : std_logic;
  --
  signal control_reg_ce      : std_logic;
  signal bram_ce             : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal bus_zero            : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  signal jtag_en_int         : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal jtag_en_expanded    : std_logic_vector(7 downto 0) := (others => '0');
  signal jtag_addr_int       : std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
  signal jtag_din_int        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal control_din         : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout_int    : std_logic_vector(7 downto 0):= (others => '0');
  signal bram_dout_int       : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
  signal jtag_we_int         : std_logic;
  signal jtag_clk_int        : std_logic;
  signal bram_ce_valid       : std_logic;
  signal din_load            : std_logic;
  --
  signal jtag_dout_0_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_1_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_2_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_3_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_4_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_5_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_6_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_7_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal picoblaze_reset_int : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  --        
begin
  bus_zero <= (others => '0');
  --
  jtag_loader_gen: if (C_JTAG_LOADER_ENABLE = 1) generate
    --
    -- Insert BSCAN primitive for target device architecture.
    --
    BSCAN_SPARTAN6_gen: if (C_FAMILY="S6") generate
    begin
      BSCAN_BLOCK_inst : BSCAN_SPARTAN6
      generic map ( JTAG_CHAIN => C_JTAG_CHAIN)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_SPARTAN6_gen;   
    --
    BSCAN_VIRTEX6_gen: if (C_FAMILY="V6") generate
    begin
      BSCAN_BLOCK_inst: BSCAN_VIRTEX6
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => FALSE)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_VIRTEX6_gen;   
    --
    BSCAN_7SERIES_gen: if (C_FAMILY="7S") generate
    begin
      BSCAN_BLOCK_inst: BSCANE2
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => "FALSE")
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_7SERIES_gen;   
    --
    --
    -- Insert clock buffer to ensure reliable shift operations.
    --
    upload_clock: BUFG
    port map( I => drck,
              O => shift_clk);
    --        
    --        
    --  Shift Register      
    --        
    --
    control_reg_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk = '1' then
        if (shift = '1') then
          control_reg_ce <= shift_din;
        end if;
      end if;
    end process control_reg_ce_shift;
    --        
    bram_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          if(C_NUM_PICOBLAZE > 1) then
            for i in 0 to C_NUM_PICOBLAZE-2 loop
              bram_ce(i+1) <= bram_ce(i);
            end loop;
          end if;
          bram_ce(0) <= control_reg_ce;
        end if;
      end if;
    end process bram_ce_shift;
    --        
    bram_we_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          jtag_we_int <= bram_ce(C_NUM_PICOBLAZE-1);
        end if;
      end if;
    end process bram_we_shift;
    --        
    bram_a_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          for i in 0 to C_BRAM_MAX_ADDR_WIDTH-2 loop
            jtag_addr_int(i+1) <= jtag_addr_int(i);
          end loop;
          jtag_addr_int(0) <= jtag_we_int;
        end if;
      end if;
    end process bram_a_shift;
    --        
    bram_d_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (din_load = '1') then
          jtag_din_int <= bram_dout_int;
         elsif (shift = '1') then
          for i in 0 to C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-2 loop
            jtag_din_int(i+1) <= jtag_din_int(i);
          end loop;
          jtag_din_int(0) <= jtag_addr_int(C_BRAM_MAX_ADDR_WIDTH-1);
        end if;
      end if;
    end process bram_d_shift;
    --
    shift_dout <= jtag_din_int(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1);
    --
    --
    din_load_select:process (bram_ce, din_load, capture, bus_zero, control_reg_ce) 
    begin
      if ( bram_ce = bus_zero ) then
        din_load <= capture and control_reg_ce;
       else
        din_load <= capture;
      end if;
    end process din_load_select;
    --
    --
    -- Control Registers 
    --
    num_picoblaze <= conv_std_logic_vector(C_NUM_PICOBLAZE-1,3);
    picoblaze_instruction_data_width <= conv_std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1,5);
    --	
    control_registers: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '0') and (control_reg_ce = '1') then
          case (jtag_addr_int(3 downto 0)) is 
            when "0000" => -- 0 = version - returns (7 downto 4) illustrating number of PB
                           --               and (3 downto 0) picoblaze instruction data width
                           control_dout_int <= num_picoblaze & picoblaze_instruction_data_width;
            when "0001" => -- 1 = PicoBlaze 0 reset / status
                           if (C_NUM_PICOBLAZE >= 1) then 
                            control_dout_int <= picoblaze_reset_int(0) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_0-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0010" => -- 2 = PicoBlaze 1 reset / status
                           if (C_NUM_PICOBLAZE >= 2) then 
                             control_dout_int <= picoblaze_reset_int(1) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_1-1,5) );
                            else 
                             control_dout_int <= (others => '0');
                           end if;
            when "0011" => -- 3 = PicoBlaze 2 reset / status
                           if (C_NUM_PICOBLAZE >= 3) then 
                            control_dout_int <= picoblaze_reset_int(2) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_2-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0100" => -- 4 = PicoBlaze 3 reset / status
                           if (C_NUM_PICOBLAZE >= 4) then 
                            control_dout_int <= picoblaze_reset_int(3) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_3-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0101" => -- 5 = PicoBlaze 4 reset / status
                           if (C_NUM_PICOBLAZE >= 5) then 
                            control_dout_int <= picoblaze_reset_int(4) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_4-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0110" => -- 6 = PicoBlaze 5 reset / status
                           if (C_NUM_PICOBLAZE >= 6) then 
                            control_dout_int <= picoblaze_reset_int(5) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_5-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0111" => -- 7 = PicoBlaze 6 reset / status
                           if (C_NUM_PICOBLAZE >= 7) then 
                            control_dout_int <= picoblaze_reset_int(6) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_6-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1000" => -- 8 = PicoBlaze 7 reset / status
                           if (C_NUM_PICOBLAZE >= 8) then 
                            control_dout_int <= picoblaze_reset_int(7) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_7-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1111" => control_dout_int <= conv_std_logic_vector(C_BRAM_MAX_ADDR_WIDTH -1,8);
            when others => control_dout_int <= (others => '1');
          end case;
        else 
          control_dout_int <= (others => '0');
        end if;
      end if;
    end process control_registers;
    -- 
    control_dout(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-8) <= control_dout_int;
    --
    pb_reset: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '1') and (control_reg_ce = '1') then
          picoblaze_reset_int(C_NUM_PICOBLAZE-1 downto 0) <= control_din(C_NUM_PICOBLAZE-1 downto 0);
        end if;
      end if;
    end process pb_reset;    
    --
    --
    -- Assignments 
    --
    control_dout (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-9 downto 0) <= (others => '0') when (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH > 8);
    --
    -- Qualify the blockram CS signal with bscan select output
    jtag_en_int <= bram_ce when bram_ce_valid = '1' else (others => '0');
    --      
    jtag_en_expanded(C_NUM_PICOBLAZE-1 downto 0) <= jtag_en_int;
    jtag_en_expanded(7 downto C_NUM_PICOBLAZE) <= (others => '0') when (C_NUM_PICOBLAZE < 8);
    --        
    bram_dout_int <= control_dout or jtag_dout_0_masked or jtag_dout_1_masked or jtag_dout_2_masked or jtag_dout_3_masked or jtag_dout_4_masked or jtag_dout_5_masked or jtag_dout_6_masked or jtag_dout_7_masked;
    --
    control_din <= jtag_din_int;
    --        
    jtag_dout_0_masked <= jtag_dout_0 when jtag_en_expanded(0) = '1' else (others => '0');
    jtag_dout_1_masked <= jtag_dout_1 when jtag_en_expanded(1) = '1' else (others => '0');
    jtag_dout_2_masked <= jtag_dout_2 when jtag_en_expanded(2) = '1' else (others => '0');
    jtag_dout_3_masked <= jtag_dout_3 when jtag_en_expanded(3) = '1' else (others => '0');
    jtag_dout_4_masked <= jtag_dout_4 when jtag_en_expanded(4) = '1' else (others => '0');
    jtag_dout_5_masked <= jtag_dout_5 when jtag_en_expanded(5) = '1' else (others => '0');
    jtag_dout_6_masked <= jtag_dout_6 when jtag_en_expanded(6) = '1' else (others => '0');
    jtag_dout_7_masked <= jtag_dout_7 when jtag_en_expanded(7) = '1' else (others => '0');
    --
    jtag_en <= jtag_en_int;
    jtag_din <= jtag_din_int;
    jtag_addr <= jtag_addr_int;
    jtag_clk <= jtag_clk_int;
    jtag_we <= jtag_we_int;
    picoblaze_reset <= picoblaze_reset_int;
    --        
  end generate jtag_loader_gen;
--
end Behavioral;
--
--
------------------------------------------------------------------------------------
--
-- END OF FILE cli.vhd
--
------------------------------------------------------------------------------------
