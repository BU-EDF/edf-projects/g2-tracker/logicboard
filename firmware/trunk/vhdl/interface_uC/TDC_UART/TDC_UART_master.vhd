library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity TDC_UART_master is
  port (
    clk62_5         : in  std_logic;
    reset          : in  std_logic;

    rx             : in  std_logic;
    tx             : out std_logic;

    data_out       : in  std_logic_vector(7 downto 0);
    data_out_wr    : in  std_logic;
    data_out_empty : out std_logic;
    
    data_in        : out std_logic_vector(7 downto 0);
    data_in_valid  : out std_logic;
    rx_error       : out std_logic_vector(1 downto 0);
    data_in_rd     : in  std_logic);

end entity TDC_UART_master;

architecture Behaviour of TDC_UART_master is

  component ClkUnit is
    port (
      SysClk   : in  Std_Logic;
      EnableRx : out Std_Logic;
      EnableTx : out Std_Logic;
      Reset    : in  Std_Logic);
  end component ClkUnit;

  component RxUnit is
    port (
      Clk    : in  Std_Logic;
      Reset  : in  Std_Logic;
      Enable : in  Std_Logic;
      RxD    : in  Std_Logic;
      RD     : in  Std_Logic;
      FErr   : out Std_Logic;
      OErr   : out Std_Logic;
      DRdy   : out Std_Logic;
      DataIn : out Std_Logic_Vector(7 downto 0));
  end component RxUnit;

  component TxUnit is
    port (
      Clk    : in  Std_Logic;
      Reset  : in  Std_Logic;
      Enable : in  Std_Logic;
      Load   : in  Std_Logic;
      TxD    : out Std_Logic;
      TRegE  : out Std_Logic;
      TBufE  : out Std_Logic;
      DataO  : in  Std_Logic_Vector(7 downto 0));
  end component TxUnit;

  signal local_reset : std_logic := '0';

  signal clk_Enable_Rx : std_logic;
  signal clk_Enable_Tx : std_logic;
  
  signal data_out_register_empty : std_logic;
  signal data_out_buffer_empty : std_logic;

  
begin  -- architecture Behaviour

  local_reset <= reset;

  ClkUnit_1: ClkUnit
    port map (
      SysClk   => clk62_5,
      EnableRx => clk_Enable_Rx,
      EnableTx => clk_Enable_Tx,
      Reset    => local_reset);

  RxUnit_1: RxUnit
    port map (
      Clk    => clk62_5,
      Reset  => local_reset,
      Enable => clk_enable_rx,
      RxD    => rx,
      RD     => data_in_rd,
      FErr   => rx_error(0),
      OErr   => rx_error(1),
      DRdy   => data_in_valid,
      DataIn => data_in);

  data_out_empty <= data_out_register_empty and data_out_buffer_empty;
  
  TxUnit_1: TxUnit
    port map (
      Clk    => clk62_5,
      Reset  => local_reset,
      Enable => clk_enable_tx,
      Load   => data_out_wr,
      TxD    => tx,
      TRegE  => data_out_register_empty,
      TBufE  => data_out_buffer_empty,
      DataO  => data_out);  
  
end architecture Behaviour;
