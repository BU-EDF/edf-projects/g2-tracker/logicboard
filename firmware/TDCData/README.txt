Data taken for the pulse separation study (see DocDB 2478).

There is a problem with the external trigger than means only about 1/2 of the events actually have pulses incoming and the others are expected to empty.

The discriminator threshold setting is 300 mV and the test pulse goes in on even channels at a value of 1200 mV (-600 mV at ASDQ).  The channel mask is set to only even channels above 8 (8,10,12,14).

Pulse separation 100 ns has two pulses (recording both edges) separated by 100 ns.  It has some spurious hits in channels which were masked off etc.
Pulse separation 90 ns has two pulses (recording both edges) separated by 90 ns. As far as I can see it doesn't have any spurious hits.
