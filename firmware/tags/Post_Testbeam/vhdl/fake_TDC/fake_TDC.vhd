-------------------------------------------------------------------------------
-- g-2 tracker 
-- Dan Gastler
-- Generate fake TDC datastreams for testing
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;


library UNISIM;
use UNISIM.vcomponents.all;

entity fake_TDC is
  port (
    clk125       : in std_logic;
    reset        : in std_logic;
    send_spill   : in std_logic;
    spill_number : in std_logic_vector(23 downto 0);
    spill_time   : in std_logic_vector(43 downto 0);
    spill_hit_count : in unsigned(10 downto 0);
    
    data_out   : out std_logic
    );
  
end fake_TDC;

architecture arch of fake_TDC is



  -----------------------------------------------------------------------------
  -- components
  -----------------------------------------------------------------------------
  -- 8b10b encoder
  component encode_8b10b_top
    generic (
      C_HAS_CE : INTEGER;
      C_HAS_ND : INTEGER);
    port (
      CLK          : IN  STD_LOGIC                    := '0';
      DIN          : IN  STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
      KIN          : IN  STD_LOGIC                    := '0';
      DOUT         : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
      CE           : IN  STD_LOGIC                    := '0';
      FORCE_CODE   : IN  STD_LOGIC                    := '0';
      FORCE_DISP   : IN  STD_LOGIC                    := '0';
      DISP_IN      : IN  STD_LOGIC                    := '0';
      DISP_OUT     : OUT STD_LOGIC;
      ND           : OUT STD_LOGIC                    := '0';
      KERR         : OUT STD_LOGIC                    := '0';
      CLK_B        : IN  STD_LOGIC                    := '0';
      DIN_B        : IN  STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
      KIN_B        : IN  STD_LOGIC                    := '0';
      DOUT_B       : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
      CE_B         : IN  STD_LOGIC                    := '0';
      FORCE_CODE_B : IN  STD_LOGIC                    := '0';
      FORCE_DISP_B : IN  STD_LOGIC                    := '0';
      DISP_IN_B    : IN  STD_LOGIC                    := '0';
      DISP_OUT_B   : OUT STD_LOGIC;
      ND_B         : OUT STD_LOGIC                    := '0';
      KERR_B       : OUT STD_LOGIC                    := '0');
  end component;


  
  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------
  constant OUTPUT_IDLE : std_logic_vector(8 downto 0) := '1'&x"BC";
  constant OUTPUT_START : std_logic_vector(8 downto 0) := '1'&x"3C";
  
  -----------------------------------------------------------------------------
  -- signals
  -----------------------------------------------------------------------------

  --different clocks
  signal div_by_5_counter : integer range 0 to 5 := 0;
  signal div_by_10_counter : integer range 0 to 10 := 0;
  signal clk_en_1b : std_logic := '0';  -- clock enable for a 25 Mhz process
  signal clk_en_10b : std_logic := '0';  -- clock enable for a 2.5Mhz process
  
  -- fake event generation
  type TDC_State_t is (SEND_IDLE,SEND_KCHAR,SEND_HEADER,SEND_DATA);
  signal event_state : TDC_State_t := SEND_IDLE;
  signal data_pointer : integer range 0 to 2048 := 0;
  signal byte_pointer : integer range 0 to 3 := 0;
  
  -- fake spill parameters
  signal spill_number_local : std_logic_vector(23 downto 0);
  signal spill_time_local : std_logic_vector(43 downto 0);  
  signal spill_hit_count_local : unsigned(10 downto 0);
  
  -- fake data
  type word_array_t is array (0 to 30) of std_logic_vector(31 downto 0);
  signal fake_spill : word_array_t := (x"00000067",x"0000002d",x"00000032",x"00000054",x"00000044",x"00000043",x"20150407",x"0000FFFF",x"00000000",x"00000000",x"00000000",x"0000FFFD",x"FFFEFFFF",x"00000000",x"00000000",others=> (others=>'0'));
  
  
  --8b10b encoding and streaming
  signal in_8b : std_logic_vector(8 downto 0) := OUTPUT_IDLE;  -- byte to be 8b10b encoded plus the K char bit
  signal out_10b : std_logic_vector(9 downto 0);  -- output 10b value
  signal out_10b_buffer : std_logic_vector(9 downto 0);  -- buffer of out_10b for bitshifting out
  signal out_10b_valid : std_logic;
  
begin  -- arch

  -----------------------------------------------------------------------------
  -- Generate an enable to gate the 125Mhz clock @ 25Mhz and 2.5Mhz
  -----------------------------------------------------------------------------
  -- Generate a 125Mhz pulse at 25Mhz for 8b10b streaming
  clock_en_1b: process (clk125, reset)
  begin  -- process clock_en_1b
    if reset = '1' then                 -- asynchronous reset (active high)
      clk_en_1b <= '0';
      div_by_5_counter <= 0;                   
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      clk_en_1b <= '0';
      div_by_5_counter <= div_by_5_counter + 1;
      if div_by_5_counter = 4 then
        clk_en_1b <= '1';
        div_by_5_counter <= 0;
      end if;
    end if;
  end process clock_en_1b;

  -- Generate a 125Mhz pulse at 2.5Mhz for 8b10b encoding
  clock_en_10b: process (clk125, reset)
  begin  -- process clock_en_10b
    if reset = '1' then                 -- asynchronous reset (active high)
      clk_en_10b <= '0';
      div_by_10_counter <= 0;
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      clk_en_10b <= '0';
      if clk_en_1b = '1' then
        div_by_10_counter <= div_by_10_counter +1;
        if div_by_10_counter = 9 then
          div_by_10_counter <= 0;
          clk_en_10b <= '1';
        end if;
      end if;
    end if;
  end process clock_en_10b;

  -----------------------------------------------------------------------------
  -- Generate data
  -----------------------------------------------------------------------------
  fake_data: process (clk125, reset)
  begin  -- process fake_data
    if reset = '1' then                 -- asynchronous reset (active high)
      fake_spill(0 to 14) <= (x"00000067",x"0000002d",x"00000032",x"00000054",x"00000044",x"00000043",x"20150407",x"0000FFFF",x"00000000",x"00000000",x"00000000",x"0000FFFD",x"FFFEFFFF",x"00000000",x"00000000");
      for iWord in 15 to 30 loop
        fake_spill(iWord) <= x"00000000";
      end loop;  -- iWord
--      for iWord in 31 to 2047 loop
--        fake_spill(iWord) <= x"20000000";
----        fake_spill(iWord)(31 downto 28) <= x"2";  -- his with 0 edge
----        fake_spill(iWord)(27 downto 24) <= std_logic_vector(to_unsigned(iWord,11))(3 downto 0); --ch
----        fake_spill(iWord)(23 downto 0)  <= "000000000000"&to_unsigned(iWord,11)(11 downto 0);
--      end loop;  -- iWord
    elsif clk125'event and clk125 = '1' then  -- rising clock edge

      if send_spill = '1' then
        -- latch the input values
        spill_number_local <= spill_number;
        spill_time_local <= spill_time;
        spill_hit_count_local <= spill_hit_count;
        -- change state and start clocking out the event
        event_state <= SEND_KCHAR;
      elsif clk_en_10b = '1' then
        case event_state is
          when SEND_IDLE =>
            in_8b <= OUTPUT_IDLE;
          when SEND_KCHAR =>
            -- send start of spill char and move to sending the event
            in_8b <= OUTPUT_START;
            event_state <= SEND_HEADER;
            -- initialize the spill and the sending state
            data_pointer <= 0;
            byte_pointer <= 0;
            -- set spill size
            if spill_hit_count_local > 2016 then
              fake_spill(7)(26 downto 16) <= std_logic_vector(to_unsigned(2047,11));
            else
              fake_spill(7)(26 downto 16) <= std_logic_vector(spill_hit_count_local + 32);
            end if;

            -- set spill number
            fake_spill(8)(23 downto 0) <= spill_number_local;
            -- set spill time
            fake_spill(9) <= spill_time_local(43 downto 12);
            fake_spill(10)(11 downto 0) <= spill_time_local(11 downto 0);
            
          when SEND_HEADER =>
            -- move through the spill's bytes and words
            if byte_pointer /= 3 then
              byte_pointer <= byte_pointer + 1;
            end if;            
            case byte_pointer is
              when 0 =>
                in_8b <= '0' & fake_spill(data_pointer)(31 downto 24);
              when 1 =>
                in_8b <= '0' & fake_spill(data_pointer)(23 downto 16);
              when 2 =>
                in_8b <= '0' & fake_spill(data_pointer)(15 downto 8);
              when 3 =>
                in_8b <= '0' & fake_spill(data_pointer)(7 downto 0);
                byte_pointer <= 0;
                data_pointer <= data_pointer + 1;            
              when others => null;
            end case;
            
            -- look for end of the header
            event_state <= SEND_HEADER;
            if data_pointer = 31 and byte_pointer = 3 then
              event_state <= SEND_DATA;
            end if;
          when SEND_DATA =>
            -- move through the spill's bytes and words
            if byte_pointer /= 3 then
              byte_pointer <= byte_pointer + 1;
            end if;            
            case byte_pointer is
              when 0  =>
                in_8b <= '0' & x"20";
              when 1 to 2 =>
                in_8b <= '0' & x"00";
              when 3 =>
                in_8b <= '0' & x"00";
                byte_pointer <= 0;
                data_pointer <= data_pointer + 1;            
              when others => null;
            end case;

            if std_logic_vector(to_unsigned(data_pointer,11)) = fake_spill(7)(26 downto 16) and byte_pointer = 3 then
              event_state <= SEND_IDLE;
            end if;
          when others => null;
        end case;
      end if;
    end if;
  end process fake_data;

  -----------------------------------------------------------------------------
  -- encode and bitshift out the 8b10b stream
  -----------------------------------------------------------------------------
  encode_8b10b_top_1: encode_8b10b_top
    generic map (
      C_HAS_CE => 1,
      C_HAS_ND => 1)
    port map (
      CLK          => clk125,
      DIN          => in_8b(7 downto 0),
      KIN          => in_8b(8),
      DOUT         => out_10b,
      CE           => clk_en_10b,
      ND           => out_10b_valid);  


  serialize_8b10b: process (clk125, reset)
  begin  -- process serialize_8b10b
    if reset = '1' then                 -- asynchronous reset (active high)
      out_10b_buffer <= "00"&x"00";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      if out_10b_valid = '1' then
        -- load a new 10bit word for output
        out_10b_buffer <= out_10b;        
      elsif clk_en_1b = '1' then
        -- serialize out 10b data
        data_out <= out_10b_buffer(0);       
        out_10b_buffer <= '0' & out_10b_buffer(9 downto 1);        
      end if;

    end if;
  end process serialize_8b10b;

  
--  serialize_8b10b: process (clk125, reset)
--  begin  -- process serialize_8b10b
--    if reset = '1' then                 -- asynchronous reset (active high)
--      out_10b_buffer <= "00"&x"00";
--    elsif clk125'event and clk125 = '1' then  -- rising clock edge
--      if out_10b_valid = '1' then
--        -- load a new 10bit word for output
--        out_10b_buffer <= out_10b;        
--      elsif clk_en_1b = '1' then
--        -- serialize out 10b data
--        data_out <= out_10b_buffer(9);       
--        out_10b_buffer <= out_10b_buffer(8 downto 0) & '0';        
--      end if;
--
--    end if;
--  end process serialize_8b10b;

  
end arch;

