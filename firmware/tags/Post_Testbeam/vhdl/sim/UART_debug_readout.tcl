restart
source vhdl/sim/UART_helper.tcl
source vhdl/sim/8b10b_helper.tcl

#set initial conditions for UART
isim force add {/top/sc_in} 1

#set clock on /top/clk
isim force add {/top/clk125} 1 -radix bin -value 0 -radix bin -time 4 ns -repeat 8 ns 
isim force add {/top/clk100} 1 -radix bin -value 0 -radix bin -time 5 ns -repeat 10 ns 
isim force add {/top/clk40} 1 -radix bin -value 0 -radix bin -time 12.5 ns -repeat 25 ns 

run 1ms

isim force add {/top/reset} 0
isim force add {/top/reset_eb} 0
#isim force add {/top/reset_c5_clk} 0

set disp "-1"

for { set i 0 } {$i < 255 } {incr i} {
    set disp [gen_8b10b_data_code BC $disp /top/tdc_serial(0)]
}

set disp [gen_8b10b_control_code 3C $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 67  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 2d  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 32  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 54  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 44  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 43  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 20 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 13 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 04 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 01  $disp /top/tdc_serial(0)]

#size and channel mask
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 20 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 20  $disp /top/tdc_serial(0)]

#spill number
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code AA  $disp /top/tdc_serial(0)]

#time
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

#time2
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 05  $disp /top/tdc_serial(0)]

#star/end TDC time
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 80 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

#selb/readout
set disp [gen_8b10b_data_code 82 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 81 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

#zero
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

#12-scalars, 8-both-edges
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 00  $disp /top/tdc_serial(0)]

set disp [gen_8b10b_data_code AA $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 55 $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code AA $disp /top/tdc_serial(0)]
set disp [gen_8b10b_data_code 55  $disp /top/tdc_serial(0)]


for { set i 0 } {$i < 10 } {incr i} {
    set disp [gen_8b10b_data_code BC $disp /top/tdc_serial(0)]
}



for { set i 0 } {$i < 1 } {incr i} {
#send daq_reg_wr command
sendUART_str daq_reg_wr 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str 55 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str 8000 115200 /top/sc_in;

#send CR (0x0D)
sendUART_hex 0D 115200 /top/sc_in;

run 1 ms

#send daq_reg_rd command
sendUART_str daq_reg_rd 115200 /top/sc_in;
#send space (0x20)
sendUART_hex 20 115200 /top/sc_in;
sendUART_str 56 115200 /top/sc_in;

#send CR (0x0D)
sendUART_hex 0D 115200 /top/sc_in;


run 1 ms
}