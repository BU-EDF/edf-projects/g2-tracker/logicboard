----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;


package EventBuilder_IO is
  type unsigned_array is array (0 to 3) of unsigned(31 downto 0);
  type std_logic_vector_array is array (0 to 3) of std_logic_vector(33 downto 0);
  type TDC_size_array is array (0 to 3) of std_logic_vector(14 downto 0);
  type cdr_edges_array_t is array (0 to 3) of std_logic_vector(4 downto 0);
  type char10b_array_t is array (0 to 3) of std_logic_vector(9 downto 0);
  type char8b_array_t is array (0 to 3) of std_logic_vector(8 downto 0);
  type CDR_setting_array_t is array (0 to 3) of std_logic_vector(2 downto 0);

  type EventBuilder_Monitor is
  record
    new_spill_count  : std_logic_vector(31 downto 0);
    sent_spill_count : std_logic_vector(31 downto 0);

    EB_reset : std_logic;

    C5_fifo_full  : std_logic;
    C5_fifo_empty : std_logic;

    TDC_enable_mask : std_logic_vector(3 downto 0);

    TDC_expected_size : TDC_size_array;
    TDC_actual_size   : TDC_size_array;

    TDC_d_chars              : unsigned_array;
    TDC_k_chars              : unsigned_array;
    TDC_k_chars_3C           : unsigned_array;
    TDC_k_chars_in_data      : unsigned_array;
    TDC_malformed_spills     : unsigned_array;
    TDC_invalid_chars        : unsigned_array;
    TDC_misaligned_syncs     : unsigned_array;
    TDC_missing_trailer_bits : unsigned_array;
    TDC_lock_timeouts        : unsigned_array;

    TDC_10b : char10b_array_t;
    TDC_8b  : char8b_array_t;

    TDC_CDR_edges     : cdr_edges_array_t;
    TDC_CDR_histogram : cdr_edges_array_t;

    TDC_fifo_full       : std_logic_vector(3 downto 0);
    TDC_fifo_empty      : std_logic_vector(3 downto 0);
    TDC_locked          : std_logic_vector(3 downto 0);
    TDC_fifo_data       : std_logic_vector_array;
    TDC_fifo_data_valid : std_logic_vector(3 downto 0);

    LB_fifo_full       : std_logic;
    LB_fifo_empty      : std_logic;
    LB_fifo_data       : std_logic_vector(8 downto 0);
    LB_fifo_data_valid : std_logic;

    TDC_CDR_override_setting : CDR_setting_array_t;

  end record;

  type EventBuilder_Control is record
    TDC_enable_mask          : std_logic_vector(3 downto 0);
    TDC_FIFO_rd              : std_logic_vector(3 downto 0);
    LB_FIFO_rd               : std_logic;
    TDC_CDR_override_setting : CDR_setting_array_t;
  end record;
end EventBuilder_IO;
