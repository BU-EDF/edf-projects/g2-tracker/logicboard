----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

use work.TestPulse_IO.all;
use work.SpillTypes.all;

entity TestPulse_top is
  
  port (
    clk40      : in  std_logic;
    reset      : in  std_logic;
    spill_type : in  std_logic_vector(3 downto 0);
    new_spill  : in  std_logic;
    TestPulse  : out std_logic_vector(7 downto 0);
    control    : in  TestPulseControl);

end entity TestPulse_top;

architecture Behavioral of TestPulse_top is

  type pulser_state_t is (IDLE,START,PULSE_GEN);
  signal pulser_state : pulser_state_t := IDLE;

  constant counter_start : unsigned(19 downto 0) := x"00000";
  signal counter : unsigned(19 downto 0) := counter_start;
  signal pulse_count : unsigned(7 downto 0) := x"00";

  signal pulser_settings : TestPulseControl := ('0',others => (others =>'0'));

  
begin  -- architecture Behavioral

  pulser: process (clk40, reset) is
  begin  -- process pulser
    if reset = '1' then                 -- asynchronous reset (active high)
      counter <= counter_start;
      TestPulse <= x"00";
    elsif clk40'event and clk40 = '1' then  -- rising clock edge
      TestPulse <= x"00";
      case pulser_state is
        when IDLE =>
          -- wait for a new spill from the C5 decoder
          if new_spill = '1' then
            pulser_settings <= control;
            -- do something different for each kind of new spill
            if spill_type = SPILL_CAL_1 then
              pulser_state <= START;
            elsif control.trigger_all_types = '1' then  -- take from control
                                                        -- instead of pulser
                                                        -- settings since we
                                                        -- need this on this
                                                        -- clock tic
              pulser_state <= START;
            else
              pulser_state <= IDLE;
            end if;
          end if;
        when START =>
          -- Wait in this state until we want to start putting out pulses
          TestPulse <= x"00";
          counter <= counter + 1;
          if counter >= unsigned(pulser_settings.start_time_count) then
            pulser_state <= PULSE_GEN;
            pulse_count <= x"00";
            counter <= counter_start;
          end if;
        when PULSE_GEN =>
          -- output pulses (counter now changes to pulse to pulse counter)
          counter <= counter + 1;
          if counter = unsigned(pulser_settings.pulse_pulse_count) then
            -- reset the counter
            counter <= counter_start;
            -- count the number of pulses we've sent out
            pulse_count <= pulse_count + 1;
            --check if we should send out a pulse or we are over
            if pulse_count = unsigned(pulser_settings.pulse_count) then
              TestPulse <= x"00";
              pulser_state <= IDLE;
            else
              -- send out a pulse
              TestPulse <= pulser_settings.pulse_channel_mask;
            end if;
          end if;
        when others => null;
      end case;      
    end if;
  end process pulser;
  

end architecture Behavioral;
