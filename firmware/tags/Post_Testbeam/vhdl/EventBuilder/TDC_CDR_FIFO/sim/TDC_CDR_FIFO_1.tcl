source vhdl/sim/TDC_spill_helper.tcl
restart



#------------------------------------------------------------
# main simulation
#------------------------------------------------------------
# set up clock

isim force add /TDC_CDR_FIFO/clk125 1 -value 0 -time 4 ns -repeat 8 ns 
isim force add /TDC_CDR_FIFO/reset 1
isim force add /TDC_CDR_FIFO/enable 1
isim force add /TDC_CDR_FIFO/read_strobe 0
run 100 ns
set disp "-1"
isim force add /TDC_CDR_FIFO/reset 0
run 200ns

#send some idle chars to lock on to
set disp [send_8b10b_idle $disp /TDC_CDR_FIFO/serial 25 ]
for {set iEvent 1} { $iEvent < 3 } {incr iEvent} {
    set disp [send_TDC_spill $disp /TDC_CDR_FIFO/serial $iEvent $iEvent ]
    set disp [send_8b10b_idle $disp /TDC_CDR_FIFO/serial 10 ]
}
    
isim force add /TDC_CDR_FIFO/read_strobe 1

run 100 us



#source vhdl/EventBuilder/TDC_CDR_FIFO/sim/TDC_CDR_FIFO_1.tcl







