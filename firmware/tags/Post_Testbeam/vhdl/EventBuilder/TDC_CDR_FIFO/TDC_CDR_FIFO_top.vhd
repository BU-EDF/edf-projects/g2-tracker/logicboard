-------------------------------------------------------------------------------
-- g-2 tracker logic board TDC CDR
-- Dan Gastler
-- Process TDC stream and provide a spill FIFO
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity TDC_CDR_FIFO is

  port (
    clk125 : in std_logic;
    reset  : in std_logic;
    enable : in std_logic;

    serial : in  std_logic;             -- TDC data in
    locked : out std_logic;             -- CDR locked
    spy    : out std_logic;

    -- Data out fifo and parsed values
    data                     : out std_logic_vector(33 downto 0);  -- TDC data stream
                                        -- (start,end,data[31:0])
    data_contains_full_spill : out std_logic;  -- If our fifo contains
                                               -- atleast one trailer
                                               -- bit (125/100Mhz boundary)
    spill_size               : out unsigned(14 downto 0);
    spill_size_error         : out std_logic;
    data_valid               : out std_logic;
    fatal_error              : out std_logic;
    debug_current_expected_size    : out std_logic_vector(14 downto 0);
    debug_current_actual_size      : out std_logic_vector(14 downto 0);
    
    --FIFO parameters
    full                     : out std_logic;
    almost_full              : out std_logic;
    empty                    : out std_logic;
    read_strobe              : in  std_logic;

    d_chars    : out unsigned(31 downto 0);
    k_chars    : out unsigned(31 downto 0);
    k_chars_3C : out unsigned(31 downto 0);

    malformed_spills : out unsigned(31 downto 0);
    k_chars_in_data  : out unsigned(31 downto 0);

    invalid_chars    : out unsigned(31 downto 0);
    misaligned_syncs : out unsigned(31 downto 0);
    lock_timeouts    : out unsigned(31 downto 0);

    cdr_edges        : out std_logic_vector(4 downto 0);
    cdr_histogram    : out std_logic_vector(4 downto 0);
    current_10b_char : out std_logic_vector(9 downto 0);
    current_8b_char  : out std_logic_vector(8 downto 0);

    cdr_override_setting : in std_logic_vector(2 downto 0)
    );
end TDC_CDR_FIFO;
architecture arch of TDC_CDR_FIFO is

  -----------------------------------------------------------------------------
  -- components
  -----------------------------------------------------------------------------

  -- clock and data recovery for 8b10b
  component datarec
    generic (
      W : integer := 12;
      M : integer := 5);
    port (
      clk   : in  std_logic;
      rst_n : in  std_logic;
      ser   : in  std_logic;
      dv    : out std_logic;
      edges : out std_logic_vector(4 downto 0);
      histo : out std_logic_vector(4 downto 0);
      override_setting : in std_logic_vector(2 downto 0);
      d     : out std_logic);
  end component;

  -- deserialize the 8b10b stream
  component bytesynch is
    generic (
      M : integer);
    port (
      clk      : in  std_logic;
      reset    : in  std_logic;
      d        : in  std_logic;
      dv       : in  std_logic;
      K        : out std_logic;
      in_sync  : out std_logic;
      dav_out  : out std_logic;
      bad_sync : out std_logic;
      q        : out std_logic_vector (M-1 downto 0));
  end component bytesynch;

  -- decode the 8b10b
  component decode_8b10b_top
    generic (
      C_HAS_CODE_ERR : integer;
      C_HAS_DISP_ERR : integer;
      C_HAS_CE       : integer;
      C_HAS_ND       : integer;
      C_HAS_SINIT    : integer);
    port (
      CLK        : in  std_logic                    := '0';
      DIN        : in  std_logic_vector(9 downto 0) := (others => '0');
      DOUT       : out std_logic_vector(7 downto 0);
      KOUT       : out std_logic;
      CE         : in  std_logic                    := '0';
      CE_B       : in  std_logic                    := '0';
      CLK_B      : in  std_logic                    := '0';
      DIN_B      : in  std_logic_vector(9 downto 0) := (others => '0');
      DISP_IN    : in  std_logic                    := '0';
      DISP_IN_B  : in  std_logic                    := '0';
      SINIT      : in  std_logic                    := '0';
      SINIT_B    : in  std_logic                    := '0';
      CODE_ERR   : out std_logic                    := '0';
      CODE_ERR_B : out std_logic                    := '0';
      DISP_ERR   : out std_logic                    := '0';
      DISP_ERR_B : out std_logic                    := '0';
      DOUT_B     : out std_logic_vector(7 downto 0);
      KOUT_B     : out std_logic;
      ND         : out std_logic                    := '0';
      ND_B       : out std_logic                    := '0';
      RUN_DISP   : out std_logic;
      RUN_DISP_B : out std_logic;
      SYM_DISP   : out std_logic_vector(1 downto 0);
      SYM_DISP_B : out std_logic_vector(1 downto 0));
  end component;




  component deser_8b10b_sync is
    port (clk     : in  std_logic;      -- oversampling clock
          rst_n   : in  std_logic;      -- asynchronous reset
          dv      : in  std_logic;  -- input data valid obatained from Data recovery module
          d_out   : out std_logic_vector (7 downto 0);  -- decoded data out 
          KO      : out std_logic;      --control character seen
          d       : in  std_logic;      --serial bits in 
          K28     : out std_logic;      -- K.28.1 sequence detected
                                        -- !!! 2 tics before dav_out !!!
          in_sync : out std_logic;      --synchronized
          dav_out : out std_logic);
  end component;

  -- FIFO of 32bit + header bit + trailer bit for TDC data
  component spill_fifo
    port (
      rst         : in  std_logic;
      clk         : in  std_logic;
      din         : in  std_logic_vector(33 downto 0);
      wr_en       : in  std_logic;
      rd_en       : in  std_logic;
      dout        : out std_logic_vector(33 downto 0);
      full        : out std_logic;
      almost_full : out std_logic;
      empty       : out std_logic;
      valid       : out std_logic);
  end component;

  component TDC_Event_Size_FIFO is
    port (
      clk   : in  std_logic;
      rst   : in  std_logic;
      din   : in  std_logic_vector(15 downto 0);
      wr_en : in  std_logic;
      rd_en : in  std_logic;
      dout  : out std_logic_vector(15 downto 0);
      full  : out std_logic;
      empty : out std_logic;
      valid : OUT STD_LOGIC);
  end component TDC_Event_Size_FIFO;

  -----------------------------------------------------------------------------
  -- constants
  -----------------------------------------------------------------------------
  constant data_index_start : unsigned(11 downto 0) := x"800";
  constant data_index_size  : unsigned(11 downto 0) := x"7f8";--x"7f7";  --2040;
  constant data_index_end   : unsigned(11 downto 0) := x"001";
  constant data_index_reset : unsigned(11 downto 0) := x"000";

  constant IDLE_CHAR           : std_logic_vector(8 downto 0) := '1'&x"BC";
  constant START_OF_SPILL_CHAR : std_logic_vector(8 downto 0) := '1'&x"3C";

  constant LOCK_TIMEOUT     : unsigned(15 downto 0)        := x"4000";
  constant LOCK_TIME_START  : unsigned(15 downto 0)        := x"0000";
  -----------------------------------------------------------------------------
  -- signals
  -----------------------------------------------------------------------------
  signal local_reset_buffer : std_logic_vector(5 downto 0) := (others => '1');
  signal local_reset        : std_logic                    := '1';
  signal reset_n            : std_logic                    := '0';
  signal lost_lock_counter  : unsigned(15 downto 0)        := LOCK_TIME_START;
  signal lost_lock_reset    : std_logic                    := '0';


  -- serial stream
  signal recovered_serial       : std_logic                    := '0';
  signal recovered_serial_valid : std_logic                    := '0';
  signal recovered_serial_sync  : std_logic                    := '0';
  signal recovered_in_sync      : std_logic                    := '0';
  -- 10b words
  signal recovered_10b_valid    : std_logic                    := '0';
  signal recovered_10b_data     : std_logic_vector(9 downto 0) := "00"&x"00";

  -- 8b words
  signal locked_local             : std_logic                    := '0';
  type data_queue_t is array (2 downto 0) of std_logic_vector(8 downto 0);
  signal recovered_8b_data        : data_queue_t                 := (others => (others => '0'));
  signal recovered_8b_valid       : std_logic_vector(2 downto 0) := (others => '0');
  signal invalid_8b10b_conversion : std_logic                    := '0';
  signal data_disparity_invalid   : std_logic                    := '0';
  signal misaligned_idle_char     : std_logic                    := '0';

  -- 32 + 2 bit data words
  signal valid_building_word          : std_logic                     := '0';
  signal building_word                : std_logic_vector(31 downto 0) := x"00000000";
  signal processing_data              : std_logic                     := '0';
  signal start_of_spill               : std_logic                     := '0';
  signal error_spill_size_for_counter : std_logic                     := '0';
  signal error_spill_size             : std_logic                     := '0';
  signal word_byte                    : integer range 0 to 3          := 0;
  signal current_word                 : std_logic_vector(33 downto 0) := "00"&x"00000000";
  signal data_index                   : unsigned(11 downto 0)         := data_index_reset;
  signal expected_tdc_size            : std_logic_vector(14 downto 0) := "000" & x"000";
  signal word_wr_delay                : std_logic_vector(47 downto 0) := x"000000000000";

  -- fifo interface
  signal word_wr           : std_logic                     := '0';
  signal spill_rd          : std_logic                     := '0';
  signal spill_out         : std_logic_vector(33 downto 0) := "00"&x"00000000";
  signal spill_almost_full : std_logic                     := '0';
  signal spill_empty       : std_logic                     := '1';
  signal data_valid_buf    : std_logic                     := '0';
  signal spill_full        : std_logic                     := '0';

  signal unread_trailer_count : std_logic_vector(3 downto 0) := x"0";

  signal tdc_fifo_rd              : std_logic                     := '0';
  signal tdc_size_wr              : std_logic                     := '0';
  signal tdc_size_out             : std_logic_vector(15 downto 0) := x"0000";
  signal tdc_event_size_fifo_full : std_logic                     := '0';
  signal tdc_size_fifo_empty      : std_logic                     := '1';
  signal tdc_size                 : unsigned(15 downto 0)         := x"0000";
  signal tdc_size_valid           : std_logic                     := '0';
  
  -- counters
  signal counter_k_chars          : unsigned(31 downto 0) := x"00000000";
  signal counter_d_chars          : unsigned(31 downto 0) := x"00000000";
  signal counter_k_chars_3C       : unsigned(31 downto 0) := x"00000000";
  signal counter_malformed_spills : unsigned(31 downto 0) := x"00000000";
  signal counter_k_chars_in_data  : unsigned(31 downto 0) := x"00000000";
  signal counter_invalid_char     : unsigned(31 downto 0) := x"00000000";
  signal counter_misaligned_syncs : unsigned(31 downto 0) := x"00000000";
  signal counter_lock_timeout     : unsigned(31 downto 0) := x"00000000";
  
begin  -- arch

  -----------------------------------------------------------------------------
  -- Reset manager
  -----------------------------------------------------------------------------
  stream_sync_monitor : process (clk125, reset) is
  begin  -- process stream_sync_monitor
    if reset = '1' then                 -- asynchronous reset (active high)
      local_reset_buffer <= (others => '1');
      lost_lock_counter  <= LOCK_TIME_START;
      lost_lock_reset <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge

      -- monitor data valid for serial locked timeout
      if locked_local = '1' then
        lost_lock_counter <= lost_lock_counter + 1;
        if recovered_10b_valid = '1' then
          lost_lock_counter <= LOCK_TIME_START;
        end if;
      else
        lost_lock_counter <= LOCK_TIME_START;    
      end if;

      --shift through our reset cycle
      local_reset_buffer(5 downto 0) <= '0' & local_reset_buffer(5 downto 1);
      
      -- monitor the system for error conditions and call reset if we find them.      
      lost_lock_reset <= '0';
      if misaligned_idle_char = '1' then
        local_reset_buffer <= (others => '1');
      elsif lost_lock_counter = LOCK_TIMEOUT then
        lost_lock_reset <= '1';
        local_reset_buffer <= (others => '1');
      end if;


    end if;
  end process stream_sync_monitor;
  local_reset <= local_reset_buffer(0);
  reset_n     <= not reset;

  -----------------------------------------------------------------------------
  -- data and clock recovery
  -----------------------------------------------------------------------------  
  datarec_1 : datarec
    port map (
      clk   => clk125,
      rst_n => reset_n,
      ser   => serial,
      dv    => recovered_serial_valid,
      edges  => cdr_edges,
      histo  => cdr_histogram,        
      override_setting => cdr_override_setting,
      d     => recovered_serial);
  spy_proc : process (recovered_serial_valid) is
  begin  -- process spy_proc
    if recovered_serial_valid = '1' then
      spy <= recovered_serial;
    end if;
  end process spy_proc;

  -----------------------------------------------------------------------------
  -- deserializer
  -----------------------------------------------------------------------------
  bytesynch_1 : bytesynch
    generic map (
      M => 10)
    port map (
      clk      => clk125,
      reset    => reset,
      d        => recovered_serial,
      dv       => recovered_serial_valid,
      K        => open,
      in_sync  => recovered_in_sync,       -- we se a K.28.5
      dav_out  => recovered_10b_valid,
      bad_sync => misaligned_idle_char,
      q        => recovered_10b_data);
  -- make sure we aren't asking the decoder to decode bad data
  --  recovered_10b_valid <= recovered_10b_sync and recovered_in_sync;
  process (clk125) is
  begin  -- process
    if clk125'event and clk125 = '1' then  -- rising clock edge
      if recovered_10b_valid = '1' then
        current_10b_char <= recovered_10b_data;
      end if;
    end if;
  end process;
  
  -----------------------------------------------------------------------------
  -- decoder
  -----------------------------------------------------------------------------

  decode_8b10b_top_1 : decode_8b10b_top
    generic map (
      C_HAS_CE       => 1,
      C_HAS_CODE_ERR => 1,
      C_HAS_DISP_ERR => 1,
      C_HAS_ND       => 1,
      C_HAS_SINIT    => 1)
    port map (
      CLK      => clk125,
      CE       => recovered_10b_valid,
      SINIT    => local_reset_buffer(5),
      DIN      => recovered_10b_data,
      DOUT     => recovered_8b_data(0)(7 downto 0),
      KOUT     => recovered_8b_data(0)(8),
      ND       => recovered_8b_valid(0),
      CODE_ERR => invalid_8b10b_conversion,
      DISP_ERR => data_disparity_invalid);
  process (clk125) is
  begin  -- process
    if clk125'event and clk125 = '1' then  -- rising clock edge
      if recovered_8b_valid(0) = '1' then
        current_8b_char <= recovered_8b_data(0);
      end if;
    end if;
  end process;


  -----------------------------------------------------------------------------
  -- 8b char level stuff
  -- look for idle lock-on and start of spill
  -- pipe-line the data and give the other processes a heads up about new data
  -----------------------------------------------------------------------------
  decoder_processor : process (clk125, local_reset)
  begin  -- process decoder_processor
    if local_reset = '1' then           -- asynchronous reset (active high)
      locked_local                   <= '0';
      recovered_8b_data(2 downto 1)  <= (others => (others => '0'));
      recovered_8b_valid(2 downto 1) <= "00";
      start_of_spill                 <= '0';
      processing_data                <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      start_of_spill <= '0';

      -- pipeline the data
      recovered_8b_data(2 downto 1)  <= recovered_8b_data(1 downto 0);
      recovered_8b_valid(2 downto 1) <= recovered_8b_valid(1 downto 0);

      -- highest priority signals happen on index 0 of our pipeline
      if recovered_8b_valid(0) = '1' then
        -- look at the most recent word for IDLE char lockon
        -- and a heads-up on a new spill coming in
        if recovered_8b_data(0) = IDLE_CHAR then
          -- lock on to idle chars
          locked_local <= '1';
          -- stop building words for the fifo if we are back to idle chars
          if processing_data = '1' then
            processing_data <= '0';
          end if;
        elsif recovered_8b_data(0) = START_OF_SPILL_CHAR then
          -- give the processes a heads up on a start of spill so they can get
          -- ready
          start_of_spill <= '1';
        end if;
      end if;

      -- lower prioirity signals set after a pipeline delay 
      if recovered_8b_valid(1) = '1' then
        -- now that the processors are ready, tell them to go
        if recovered_8b_data(1) = START_OF_SPILL_CHAR then
          processing_data <= '1';
        end if;
      end if;
    end if;
  end process decoder_processor;

  locked <= locked_local;



  -----------------------------------------------------------------------------
  -- build the 32bit words we will be writing into the FIFO
  -----------------------------------------------------------------------------  
  word_builder : process (clk125, local_reset) is
  begin  -- process word_builder
    if local_reset = '1' then           -- asynchronous reset (active high)
      word_byte           <= 0;
      building_word       <= x"00000000";
      valid_building_word <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      valid_building_word <= '0';
      if start_of_spill = '1' then
        -- get ready for data
        word_byte <= 0;
      elsif processing_data = '1' and recovered_8b_valid(2) = '1' then
        -- merge in the new word (reset all bits on byte zero)
        case word_byte is
          when 0      => building_word(31 downto 0)  <= x"000000" & recovered_8b_data(2)(7 downto 0);
          when 1      => building_word(15 downto 8)  <= recovered_8b_data(2)(7 downto 0);
          when 2      => building_word(23 downto 16) <= recovered_8b_data(2)(7 downto 0);
          when 3      => building_word(31 downto 24) <= recovered_8b_data(2)(7 downto 0);
          when others => null;
        end case;

        -- update the word byte pos
        if word_byte /= 3 then
          word_byte <= word_byte + 1;
        else
          valid_building_word <= '1';   -- valid word for next processor
          word_byte           <= 0;
        end if;
      end if;
    end if;
  end process word_builder;

  -----------------------------------------------------------------------------
  -- process the new 32bit words and add header/trailer bits
  -- write into FIFO
  -----------------------------------------------------------------------------  
  write_word_to_fifo : process (clk125, local_reset) is
  begin  -- process write_word_to_fifo
    if local_reset = '1' then           -- asynchronous reset (active high)
      word_wr       <= '0';
      current_word  <= "00"&x"00000000";
      word_wr_delay <= x"000000000000";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      word_wr <= '0';

      if valid_building_word = '1' then
        -- latch the data from the building word
        current_word(31 downto 0) <= building_word;
        -- set header bit if this is the first word of data
        current_word(33)          <= '0';
        if data_index = data_index_start then
          current_word(33) <= '1';
        end if;
        -- set the trailer to the default value
        current_word(32) <= '0';
        -- delay writing this word into the FIFO so we can catch if this is the
        -- trailer word
        word_wr_delay    <= x"800000000000";
      elsif word_wr_delay(0) = '1' then
        -- set trailer bit if needed
        current_word(32) <= '0';
        if processing_data = '0' then
          current_word(32) <= '1';
          if data_index /= data_index_end then
          --other error increment
          end if;
        end if;
        -- write this word
        word_wr       <= '1';
        word_wr_delay <= x"000000000000";
      else
        -- countdown to writing this word
        word_wr_delay <= '0' & word_wr_delay(47 downto 1);
      end if;
    end if;
  end process write_word_to_fifo;

  -----------------------------------------------------------------------------
  -- monitor the words going into the FIFOs to check their data sizes
  -----------------------------------------------------------------------------
  data_integrity_check : process (clk125, local_reset) is
  begin  -- process data_integrity_check
    if local_reset = '1' then           -- asynchronous reset (active high)
      error_spill_size_for_counter <= '0';
      expected_tdc_size            <= "000"&x"000";
      data_index                   <= data_index_reset;
      error_spill_size             <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      error_spill_size_for_counter <= '0';
      if start_of_spill = '1' then
        -- get ready for data
        data_index       <= data_index_start;
        error_spill_size <= '0';
      elsif word_wr = '1' then
        case data_index is
          when data_index_size =>
            --record the size for later
            expected_tdc_size <=  current_word(30 downto 16);

            if unsigned(current_word(27 downto 16)) >= 32 then
              -- The header is 32 words, so if this is less than 32, we have
              --   a problem.
              -- Update the spill parse counter to this word, minus the 8
              --   words already parsed, and 1 to set it correctly for the next
              --   word.
              data_index <= unsigned(current_word(27 downto 16)) - 9;
            else
              -- for debugging, save the full event as if it was a max event
              data_index                   <= data_index - 1;
              error_spill_size_for_counter <= '1';
              error_spill_size             <= '1';
            end if;
          when data_index_end =>
            -- we should be done processing data if we are on the last word
--            if processing_data = '1' then
--              error_spill_size_for_counter <= '1';
--              error_spill_size <= '1';
--            end if;
            data_index <= data_index_reset;
          when data_index_reset => null;  -- stay in this state so we can catch
                                          -- data that is longer than we were
                                          -- told from the TDC header
          when others =>
            data_index <= data_index - 1;
        end case;
      end if;
    end if;
  end process data_integrity_check;



  -----------------------------------------------------------------------------
  -- FIFO of spill data
  -----------------------------------------------------------------------------
  data <= spill_out;

  spill_fifo_1 : spill_fifo
    port map (
      rst         => local_reset_buffer(5),
      clk         => clk125,
      din         => current_word,
      wr_en       => word_wr,
      rd_en       => read_strobe,
      dout        => spill_out,
      full        => spill_full,
      almost_full => almost_full,
      empty       => empty,
      valid       => data_valid_buf);

  full       <= spill_full;
  data_valid <= data_valid_buf;


  -----------------------------------------------------------------------------
  -- Compute the true (IDLE -> IDLE) event size of the last event
  -- Store the size in a 16 bit number using the MSB as an error bit
  -- (overflow is an error)
  -----------------------------------------------------------------------------  
  TDC_True_Size : process (clk125, local_reset) is
  begin  -- process TDC_True_Size
    if local_reset = '1' then           -- asynchronous reset (active high)
      tdc_size    <= x"0000";
      tdc_size_wr <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      tdc_size_wr <= '0';

      if word_wr = '1' then
        if current_word(33) = '1' then
          tdc_size <= x"0002";  -- need to count this word and the trailer which
        -- we will have to write into the fifo at the
        -- same time we latch the data size
        elsif current_word(32) = '1' then
          tdc_size_wr <= '1';

          debug_current_expected_size <= expected_tdc_size;
          debug_current_actual_size <= std_logic_vector(tdc_size(14 downto 0));
          
          if std_logic_vector(tdc_size(14 downto 0)) /= expected_tdc_size then
            tdc_size(15) <= '1';
          elsif error_spill_size = '1' then
            tdc_size(15) <= '1';
          end if;
        else
          if tdc_size < x"8000" then    -- tdc_size(15) = '0'
            tdc_size <= tdc_size + 1;
          end if;
        end if;

      end if;
    end if;
  end process TDC_True_Size;

  size_output: process (clk125, local_reset) is
  begin  -- process size_output
    if local_reset = '1' then           -- asynchronous reset (active high)
      spill_size <= "000"&x"000";
      spill_size_error <= '0';
      data_contains_full_spill <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      -- default output is zero and no full spills
      spill_size <= "000"&x"000";
      spill_size_error <= '0';
      data_contains_full_spill <= '0';
      -- update if we have valid data
      if tdc_size_fifo_empty = '0' and tdc_size_valid = '1' then
        spill_size               <= unsigned(tdc_size_out(14 downto 0));
        spill_size_error         <= tdc_size_out(15);
        data_contains_full_spill <= '1';        
      end if;
    end if;
  end process size_output;
  

  TDC_Event_Size_FIFO_1 : entity work.TDC_Event_Size_FIFO
    port map (
      clk   => clk125,
      rst   => local_reset,
      din   => std_logic_vector(tdc_size),
      wr_en => tdc_size_wr,
      rd_en => tdc_fifo_rd,
      dout  => tdc_size_out,
      full  => tdc_event_size_fifo_full,
      empty => tdc_size_fifo_empty,
      valid => tdc_size_valid);

  display_size : process (clk125, local_reset) is
  begin  -- process display_size
    if local_reset = '1' then
      tdc_fifo_rd <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      tdc_fifo_rd <= '0';
      if data_valid_buf = '1' and spill_out(32) = '1' then
        tdc_fifo_rd <= '1';
      end if;
    end if;
  end process display_size;


  monitor_FIFOs : process (clk125, local_reset) is
  begin  -- process monitor_FIFOs
    if local_reset = '1' then           -- asynchronous reset (active high)
      fatal_error <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      if ((tdc_size_wr = '1' and tdc_event_size_fifo_full = '1') or
          (word_wr = '1' and spill_full = '1')) then
        fatal_error <= '1';
      end if;
    end if;
  end process monitor_FIFOs;






  -----------------------------------------------------------------------------
  -- Counters
  -----------------------------------------------------------------------------

  ------------------------------------------------------------------------
  -- lock timeout counter
  lock_timeout_counter : process (clk125, reset)
  begin  -- process lock_timeout_counter
    if reset = '1' then                 -- asynchronous reset (active high)
      counter_lock_timeout <= x"00000000";
      lock_timeouts        <= x"00000000";      
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      lock_timeouts <= counter_lock_timeout;      
      if lost_lock_reset = '1' then
        if counter_lock_timeout /= x"FFFFFFFF" then
          counter_lock_timeout <= counter_lock_timeout + 1;
        end if;
      end if;
    end if;
  end process lock_timeout_counter;

  ------------------------------------------------------------------------
  -- misaligned sync counter
  misaligned_sync_counter : process (clk125, reset)
  begin  -- process misaligned_sync_counter
    if reset = '1' then                 -- asynchronous reset (active high)
      counter_misaligned_syncs <= x"00000000";
      misaligned_syncs         <= x"00000000";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      misaligned_syncs <= counter_misaligned_syncs;

      if misaligned_idle_char = '1' then
        if counter_misaligned_syncs /= x"FFFFFFFF" then
          counter_misaligned_syncs <= counter_misaligned_syncs + 1;
        end if;
      end if;
    end if;
  end process misaligned_sync_counter;
  
  ------------------------------------------------------------------------
  -- bad char counter  
  bad_char_counter : process (clk125, reset)
  begin  -- process bad_char_counter
    if reset = '1' then                 -- asynchronous reset (active high)
      counter_invalid_char <= x"00000000";
      invalid_chars        <= x"00000000";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      invalid_chars    <= counter_invalid_char;

      if locked_local = '1' and recovered_8b_valid(0) = '1' and  invalid_8b10b_conversion = '1' then
        if counter_invalid_char /= x"FFFFFFFF" then
          counter_invalid_char <= counter_invalid_char + 1;
        end if;
      end if;
    end if;
  end process bad_char_counter;

  ------------------------------------------------------------------------
  -- size counters  
  malformed_counter : process (clk125, reset)
  begin  -- process malformed_counter
    if reset = '1' then                 -- asynchronous reset (active high)
      counter_malformed_spills <= x"00000000";
      malformed_spills         <= x"00000000";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      malformed_spills <= counter_malformed_spills;
      
      if error_spill_size_for_counter = '1' and counter_malformed_spills /= X"FFFFFFFF" then
        -- bad spill data (keep track)
        counter_malformed_spills <= counter_malformed_spills + 1;
      end if;
    end if;
  end process malformed_counter;

  ------------------------------------------------------------------------
  --char counters                                                         
  char_counter : process (clk125, reset)
  begin  -- process k_char_counter
    if reset = '1' then                 -- asynchronous reset (active high)
      counter_k_chars         <= x"00000000";
      counter_k_chars_3C      <= x"00000000";
      counter_k_chars_in_data <= x"00000000";
      counter_d_chars         <= x"00000000";

      k_chars                 <= x"00000000";
      k_chars_3C              <= x"00000000";
      k_chars_in_data         <= x"00000000";
      d_chars                 <= x"00000000";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      if locked_local = '1' and recovered_8b_valid(2) = '1' then
        -- update outputs
        k_chars          <= counter_k_chars;
        k_chars_3C       <= counter_k_chars_3C;
        k_chars_in_data  <= counter_k_chars_in_data;
        d_chars          <= counter_d_chars;
        
        if recovered_8b_data(2)(8) = '1' then
          -- count control chars
          if counter_k_chars = x"FFFFFFFF" then
            counter_k_chars <= x"00000000";
          else
            counter_k_chars <= counter_k_chars + 1;
          end if;

          -- count 3C control chars
          if recovered_8b_data(2) = START_OF_SPILL_CHAR and counter_k_chars_3C /= x"FFFFFFFF" then
            counter_k_chars_3C <= counter_k_chars_3C + 1;
          end if;

          -- count control chars during spills
          if processing_data = '1' then
            if counter_k_chars_in_data /= x"FFFFFFFF" then
              counter_k_chars_in_data <= counter_k_chars_in_data + 1;
            end if;
          end if;
        else
          -- count data chars
          if counter_d_chars = x"FFFFFFFF" then
            counter_d_chars <= x"00000000";
          else
            counter_d_chars <= counter_d_chars + 1;
          end if;

        end if;
      end if;
    end if;
  end process char_counter;



end arch;


