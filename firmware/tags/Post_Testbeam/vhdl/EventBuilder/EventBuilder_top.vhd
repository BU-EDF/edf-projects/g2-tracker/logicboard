-------------------------------------------------------------------------------
-- g-2 tracker logic board event builder
-- Dan Gastler
-- Merge and write TDC events
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;

-- for monitoring and controlling of the event builder
use work.EventBuilder_IO.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity event_builder is
  generic (
    TDC_COUNT : integer := 4);
  port (
    reset : in std_logic;

    C5_stream : in std_logic;
    clk40     : in std_logic;

    clk125     : in std_logic;
    TDC_serial : in std_logic_vector(3 downto 0);
    TDC_spy    : out std_logic_vector(3 downto 0);
    Firmware_v : in std_logic_vector(31 downto 0);
    control    : in EventBuilder_Control;

    data_out : out std_logic;

    monitoring : out EventBuilder_Monitor;
    new_spill  : out std_logic;         -- for test pulse sync
    spill_type : out std_logic_vector(3 downto 0)

    );

end event_builder;

architecture arch of event_builder is

  -----------------------------------------------------------------------------
  -- components
  -----------------------------------------------------------------------------

  -- TDC CDR and FIFO
  component TDC_CDR_FIFO is
    port (
      clk125                   : in  std_logic;
      reset                    : in  std_logic;
      enable                   : in  std_logic;
      serial                   : in  std_logic;
      locked                   : out std_logic;
      spy                      : out std_logic;
      data                     : out std_logic_vector(33 downto 0);
      data_contains_full_spill : out std_logic;
      spill_size               : out unsigned(14 downto 0);
      spill_size_error         : out std_logic;
      data_valid               : out std_logic;
      fatal_error              : out std_logic;
      debug_current_expected_size    : out std_logic_vector(14 downto 0);
      debug_current_actual_size      : out std_logic_vector(14 downto 0);
      full                     : out std_logic;
      almost_full              : out std_logic;
      empty                    : out std_logic;
      read_strobe              : in  std_logic;
      d_chars                  : out unsigned(31 downto 0);
      k_chars                  : out unsigned(31 downto 0);
      k_chars_3C               : out unsigned(31 downto 0);
      malformed_spills         : out unsigned(31 downto 0);
      k_chars_in_data          : out unsigned(31 downto 0);
      invalid_chars            : out unsigned(31 downto 0);
      misaligned_syncs         : out unsigned(31 downto 0);
      lock_timeouts            : out unsigned(31 downto 0);
      cdr_edges                : out std_logic_vector(4 downto 0);
      cdr_histogram            : out std_logic_vector(4 downto 0);
      current_10b_char         : out std_logic_vector(9 downto 0);
      current_8b_char          : out std_logic_vector(8 downto 0);
      cdr_override_setting     : in std_logic_vector(2 downto 0));
  end component TDC_CDR_FIFO;
  -- C5 CDR and FIFO
  component C5_CDR_FIFO
    port (
      clk40           : in  std_logic;
      C5_raw          : in  std_logic;
      clk125          : in  std_logic;
      reset           : in  std_logic;
      new_spill       : out std_logic;
      spill_type      : out std_logic_vector(3 downto 0);
      spill_rd        : in  std_logic;
      spill_data      : out std_logic_vector(71 downto 0);
      fifo_full       : out std_logic;
      fifo_empty      : out std_logic;
      fifo_data_valid : out std_logic);
  end component;

  component spill_8b10b is
    port (
      rst         : in  std_logic;
      rd_clk      : in  std_logic;
      wr_clk      : in  std_logic;
      din         : in  std_logic_vector(35 downto 0);
      wr_en       : in  std_logic;
      rd_en       : in  std_logic;
      dout        : out std_logic_vector(8 downto 0);
      full        : out std_logic;
      almost_full : out std_logic;
      empty       : out std_logic;
      valid       : out std_logic);
  end component spill_8b10b;


  -- 8b10b encoder
  component encode_8b10b_top
    generic (
      C_HAS_CE : integer);
    port (
      CLK          : in  std_logic                    := '0';
      DIN          : in  std_logic_vector(7 downto 0) := (others => '0');
      KIN          : in  std_logic                    := '0';
      DOUT         : out std_logic_vector(9 downto 0);
      CE           : in  std_logic                    := '0';
      FORCE_CODE   : in  std_logic                    := '0';
      FORCE_DISP   : in  std_logic                    := '0';
      DISP_IN      : in  std_logic                    := '0';
      DISP_OUT     : out std_logic;
      ND           : out std_logic                    := '0';
      KERR         : out std_logic                    := '0';
      CLK_B        : in  std_logic                    := '0';
      DIN_B        : in  std_logic_vector(7 downto 0) := (others => '0');
      KIN_B        : in  std_logic                    := '0';
      DOUT_B       : out std_logic_vector(9 downto 0);
      CE_B         : in  std_logic                    := '0';
      FORCE_CODE_B : in  std_logic                    := '0';
      FORCE_DISP_B : in  std_logic                    := '0';
      DISP_IN_B    : in  std_logic                    := '0';
      DISP_OUT_B   : out std_logic;
      ND_B         : out std_logic                    := '0';
      KERR_B       : out std_logic                    := '0');
  end component;

  component Debug_Output_FIFO is
    port (
      clk   : IN  STD_LOGIC;
      rst   : IN  STD_LOGIC;
      din   : IN  STD_LOGIC_VECTOR(8 DOWNTO 0);
      wr_en : IN  STD_LOGIC;
      rd_en : IN  STD_LOGIC;
      dout  : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
      full  : OUT STD_LOGIC;
      empty : OUT STD_LOGIC;
      valid : OUT STD_LOGIC);
  end component Debug_Output_FIFO;

  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------
  constant OUTPUT_IDLE  : std_logic_vector(8 downto 0) := '1'&x"BC";
  constant OUTPUT_START : std_logic_vector(8 downto 0) := '1'&x"3C";
  constant OUTPUT_END   : std_logic_vector(8 downto 0) := '1'&x"5C";

  -----------------------------------------------------------------------------
  -- signals
  -----------------------------------------------------------------------------
  signal reset_local : std_logic_vector(3 downto 0) := x"F";
  signal control_local : EventBuilder_Control;
  signal monitor_local : EventBuilder_Monitor;
  
  signal C5_fifo_read       : std_logic := '0';
  signal C5_fifo_data       : std_logic_vector(71 downto 0);
  signal C5_fifo_data_valid : std_logic;
  signal C5_fifo_full       : std_logic;
  signal C5_fifo_empty      : std_logic;
  signal C5_full_spill      : std_logic;
  signal C5_spill_number    : std_logic_vector(23 downto 0);
  signal C5_spill_time      : std_logic_vector(43 downto 0);
  signal C5_spill_type      : std_logic_vector(2 downto 0);


  type TDC_data_array is array (0 to 3) of std_logic_vector(33 downto 0);
  type TDC_Size_t is array (0 to 3) of unsigned(14 downto 0);
  signal TDC_locked      : std_logic_vector(3 downto 0);
  signal TDC_data        : TDC_data_array;
  signal TDC_spill_size  : TDC_size_t := (others => (others => '0'));
  signal TDC_spill_size_error : std_logic_vector(3 downto 0) := x"0";
  signal TDC_fatal_error : std_logic_vector(3 downto 0) := x"0";
  signal TDC_empty       : std_logic_vector(3 downto 0);
  signal TDC_full        : std_logic_vector(3 downto 0);
  signal TDC_has_trailer : std_logic_vector(3 downto 0) := x"0";
  signal TDC_read        : std_logic_vector(3 downto 0);

  signal C5_valid : std_logic := '0';

  type TDC_spill_header_t is array (0 to 15) of std_logic_vector(33 downto 0);
  type TDC_header_t is array (0 to 3) of TDC_spill_header_t;
  type TDC_bad_header_t is array (0 to 3) of std_logic_vector(31 downto 0);
  type TDC_header_pointer_t is array (0 to 3) of integer range 0 to 16;
  signal TDC_enable         : std_logic_vector(3 downto 0) := x"0";
  signal TDC_header         : TDC_header_t                 := (others => (others => (others => '0')));
  signal TDC_has_header     : std_logic_vector(3 downto 0) := x"0";
  signal TDC_bad_header     : TDC_bad_header_t             := (others => x"00000000");
  signal TDC_data_valid     : std_logic_vector(3 downto 0);
  signal header_end_pointer : TDC_header_pointer_t;
  signal Active_TDC_stream  : integer range 0 to 4         := 4;


  signal total_spill_size    : unsigned(15 downto 0);
  signal tdc_size_error      : std_logic_vector(3 downto 0) := x"0";
  signal fatal_error         : std_logic := '0';
  signal sending_to_TRM      : std_logic                     := '0';
  type Send_State_t is (SEND_IDLE,CHECK_NEW_DATA,BUILD_HEADER, SEND_HEADER, SEND_TDC_DATA, SEND_END,SEND_CRC, SEND_TRAILER);
  signal send_state          : Send_State_t                  := SEND_IDLE;
  signal TDC_Size            : TDC_Size_t                    := (others => (others => '0'));
  signal TDC_Size_left       : TDC_Size_t                    := (others => (others => '0'));
  type TDC_Spillnumber_t is array (0 to 3) of std_logic_vector(23 downto 0);
  signal TDC_Spillnumber     : TDC_spillnumber_t;
  signal spill_numbers_match : std_logic_vector(3 downto 0)  := x"0";
  signal LB_data_word_count  : unsigned(31 downto 0)         := x"00000000";
  signal LB_data_word        : std_logic_vector(31 downto 0) := x"00000000";
  signal LB_data_word_k_mask : std_logic_vector(3 downto 0)  := x"0";
  signal send_header_counter : integer range 0 to 10          := 0;

  --8b10b fifo 32 +4 bit down to 9 bit (1+8)
  signal spill_8b10b_wr      : std_logic                     := '0';
  signal spill_8b10b_in      : std_logic_vector(35 downto 0) := x"000000000";
  signal fifo_8b_rd          : std_logic                     := '0';
  signal fifo_8b_out         : std_logic_vector(8 downto 0);
  signal fifo_8b_full        : std_logic;
  signal fifo_8b_almost_full : std_logic;
  signal fifo_8b_empty       : std_logic;
  signal fifo_8b_valid       : std_logic;


  signal in_8b              : std_logic_vector(8 downto 0) := OUTPUT_IDLE;
  signal encode_8b_to_10b   : std_logic                    := '0';
  signal out_10b            : std_logic_vector(9 downto 0);
  signal out_10b_shift      : std_logic_vector(9 downto 0);
  signal clk_en_10b_counter : integer range 0 to 10        := 0;


  -- debugging output data signals
  type LB_8b10b_t is array (0 to 6) of std_logic_vector(8 downto 0);
  signal debug_8b : LB_8b10b_t := (others => (others => '0'));
  signal debug_encode_buffer : std_logic_vector(6 downto 0) := (others => '0');
  signal debug_wr : std_logic := '0';
  signal debug_reset : std_logic := '1';
  signal debug_fifo_full : std_logic;
  signal debug_fifo_active : std_logic := '0';
  
  signal events_out : std_logic_vector(31 downto 0) := x"00000000";
  signal events_in  : std_logic_vector(31 downto 0) := x"00000000";

  -- additional counters for TDC streams
  signal tdc_missing_trailer_bit : std_logic_vector(3 downto 0) := x"0";
  type unsigned_array_t is array (0 to 3) of unsigned(31 downto 0);
  signal TDC_missing_trailer_bit_counter : unsigned_array_t := (others =>( others => '0'));
  
begin  -- arch

  long_reset: process (clk125,reset) is
  begin  -- process long_reset
    if reset = '1' then
      reset_local <= x"F";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      reset_local(3 downto 0) <= '0' & reset_local(3 downto 1);
    end if;
  end process long_reset;

  -- control/monitoring buffer
  monitor_control_buffer: process (clk125) is
  begin  -- process monitor_control_buffer
    if clk125'event and clk125 = '1' then  -- rising clock edge
      control_local <= control;
      monitoring <= monitor_local;
    end if;
  end process monitor_control_buffer;

  -- monitoring
  monitoring_proc: process (clk125) is
  begin  -- process monitoring_proc
    if clk125'event and clk125 = '1' then  -- rising clock edge
      monitor_local.sent_spill_count <= events_out;
      monitor_local.new_spill_count  <= events_in;

      monitor_local.C5_fifo_full  <= C5_fifo_full;
      monitor_local.C5_fifo_empty <= C5_fifo_empty;
      
      monitor_local.TDC_enable_mask <= control_local.TDC_enable_mask;
      monitor_local.TDC_fifo_full   <= TDC_full;
      monitor_local.TDC_fifo_empty  <= TDC_empty;
      monitor_local.TDC_locked      <= TDC_locked;

      monitor_local.EB_reset <= reset_local(0);

      monitor_local.TDC_CDR_override_setting <= control_local.TDC_CDR_override_setting;
    end if;
  end process monitoring_proc;


  -- control
  TDC_enable_sync : process (clk125) is
  begin  -- process TDC_enable_sync
    if clk125'event and clk125 = '1' then  -- rising clock edge
      TDC_enable <= control_local.TDC_enable_mask;
    end if;
  end process TDC_enable_sync;



  --loop over TDC components
  TDC_CDR : for iTDC in 0 to 3 generate
    TDC_CDR_FIFO_2 : entity work.TDC_CDR_FIFO
      port map (
        clk125                   => clk125,
        reset                    => reset_local(0),
        enable                   => TDC_enable(iTDC),
        serial                   => TDC_serial(iTDC),
        locked                   => TDC_locked(iTDC),
        spy                      => TDC_spy(iTDC),
        data                     => TDC_data(iTDC),
        data_contains_full_spill => TDC_has_trailer(iTDC),
        spill_size               => TDC_spill_size(iTDC),
        spill_size_error         => TDC_spill_size_error(iTDC),
        data_valid               => TDC_data_valid(iTDC),
        fatal_error              => TDC_fatal_error(iTDC),
        debug_current_expected_size => monitor_local.TDC_expected_size(iTDC),
        debug_current_actual_size   => monitor_local.TDC_actual_size(iTDC),
        full                     => TDC_full(iTDC),
        almost_full              => open,
        empty                    => TDC_empty(iTDC),
        read_strobe              => TDC_read(iTDC),
        d_chars                  => monitor_local.TDC_d_chars(iTDC),
        k_chars                  => monitor_local.TDC_k_chars(iTDC),
        k_chars_3c               => monitor_local.TDC_k_chars_3C(iTDC),
        malformed_spills         => monitor_local.TDC_malformed_spills(iTDC),
        lock_timeouts            => monitor_local.TDC_lock_timeouts(iTDC),
        k_chars_in_data          => monitor_local.TDC_k_chars_in_data(iTDC),
        invalid_chars            => monitor_local.TDC_invalid_chars(iTDC),
        misaligned_syncs         => monitor_local.TDC_misaligned_syncs(iTDC),
        cdr_edges                => monitor_local.TDC_CDR_edges(iTDC),
        cdr_histogram            => monitor_local.TDC_CDR_histogram(iTDC),
        current_10b_char         => monitor_local.TDC_10b(iTDC),
        current_8b_char          => monitor_local.TDC_8b(iTDC),
        cdr_override_setting     => control_local.TDC_CDR_override_setting(iTDC)
        );
  end generate TDC_CDR;

  -- C5 CDR & FIFO

  C5_CDR_FIFO_1 : C5_CDR_FIFO
    port map (
      clk40           => clk40,
      C5_raw          => C5_stream,
      clk125          => clk125,
      reset           => reset_local(0),
      new_spill       => new_spill,
      spill_type      => spill_type,
      spill_rd        => C5_fifo_read,
      spill_data      => C5_fifo_data,
      fifo_full       => C5_fifo_full,
      fifo_empty      => C5_fifo_empty,
      fifo_data_valid => C5_fifo_data_valid);



  -----------------------------------------------------------------------------
  -- Read out each of the TDCs to fill up their headers
  -----------------------------------------------------------------------------
  TDC_Readouts : for iTDC in 0 to 3 generate
    TDC_Readout : process (clk125, reset_local(0))
    begin  -- process TDC_Readout
      if reset_local(0) = '1' then               -- asynchronous reset (active high)
        monitor_local.TDC_fifo_data(iTDC)       <= "00"&x"00000000";
        monitor_local.TDC_fifo_data_valid(iTDC) <= '0';
        TDC_has_header(iTDC) <= '0';
        TDC_read(iTDC) <= '0';
        header_end_pointer(iTDC) <= 0;
      elsif clk125'event and clk125 = '1' then  -- rising clock edge
        TDC_read(iTDC) <= '0';          -- default is not reading from the FIFO
        --Readout the header of a TDC so that the Event_Builder can build
        if TDC_enable(iTDC) = '1' then

          if TDC_has_header(iTDC) = '0' then
            -- pull data until we have a valid header word
            if TDC_header(iTDC)(0)(33) /= '1' then  -- true if we don't have a
                                                    -- header bit in the 0th word
              -- push in new data when available
              if TDC_data_valid(iTDC) = '1' then
                -- shift through data in TDC_header and fill the end with new
                -- fifo data
                TDC_header(iTDC)(0 to 14) <= TDC_header(iTDC)(1 to 15);
                TDC_header(iTDC)(15)          <= TDC_data(iTDC);
              elsif TDC_read(iTDC) = '0' and TDC_empty(iTDC) = '0' then
                --get new data from FIFO               
                TDC_read(iTDC) <= '1';
              end if;              
            else
              -- we have a header at word 0, we have a spill!
              TDC_has_header(iTDC)     <= '1';
              header_end_pointer(iTDC) <= 15;       -- last valid header word
              --store the size
              TDC_SpillNumber(iTDC)    <= TDC_header(iTDC)(9)(23 downto 0);
            end if;
          elsif iTDC = Active_TDC_stream then

            -- keep our tdc_header buffer full if we can
            if  TDC_empty(iTDC) = '0' and TDC_read(iTDC) = '0' then
              if header_end_pointer(iTDC) < 14 then
                TDC_read(iTDC) <= '1';
              elsif header_end_pointer(iTDC) = 14 and TDC_data_valid(iTDC) = '0' then
                -- if header_end_pointer is 14 and the data is valid, we are
                -- already filling the last position, so we only want to read
                -- if we dont' have valid data
                TDC_read(iTDC) <= '1';
              end if;
            end if;
            
            -- process
            if fifo_8b_almost_full = '0' then
--            if spill_8b10b_wr = '1' then
              -- update the header every time the send state machine does a write
              
              -- shift through data in TDC_header
              TDC_header(iTDC)(0 to 14) <= TDC_header(iTDC)(1 to 15);
              -- by default, write a garbage word into the stream if we have no
              -- data to fill it.
              -- This should be overwritten by the next few lines
              TDC_header(iTDC)(header_end_pointer(iTDC)) <= "00"&x"DEADBEEF"; 

              -- look for valid data
              if TDC_data_valid(iTDC) = '1' then
                -- read in the data for this word
                TDC_header(iTDC)(header_end_pointer(iTDC)) <= TDC_data(iTDC);
              elsif header_end_pointer(iTDC) > 0 then
                header_end_pointer(iTDC) <= header_end_pointer(iTDC) -1;
              end if;

              -- since we know we just opened up a spot on the TDC_header
              -- buffer, we sould start a read if we can
              if TDC_empty(iTDC) = '0' and TDC_read(iTDC) = '0' then
                TDC_read(iTDC) <= '1';
              end if;

            elsif TDC_data_valid(iTDC) = '1' then
              -- If we aren't writing a word, but we are catching up with out
              -- TDC_header buffer, append good data.
              TDC_header(iTDC)(header_end_pointer(iTDC)+1) <= TDC_data(iTDC);
              header_end_pointer(iTDC)                   <= header_end_pointer(iTDC)+1;
            end if;
          elsif send_state = SEND_TRAILER then
            -- we are done sending TDC data, so we can switch back to
            -- collecting data for the next event. 
            TDC_has_header(iTDC) <= '0';              
          end if;
        -- TDC is disabled, now we can read out the TDC via the slow control
        else
          -- slow control readout
          if TDC_empty(iTDC) = '0' and control_local.TDC_FIFO_rd(iTDC) = '1' then
            TDC_read(iTDC) <= '1';
          end if;
          if TDC_data_valid(iTDC) = '1' then
            monitor_local.TDC_fifo_data(iTDC)       <= TDC_data(iTDC);
            monitor_local.TDC_fifo_data_valid(iTDC) <= TDC_data_valid(iTDC);
          end if;
        end if;
      end if;
    end process TDC_Readout;
  end generate TDC_Readouts;

-------------------------------------------------------------------------------
-- Read out the next C5 header
-------------------------------------------------------------------------------
  C5_readout : process (clk125, reset_local(0))
  begin  -- process C5_readout
    if reset_local(0) = '1' then                 -- asynchronous reset (active high)
      events_in <= x"00000000";
      C5_spill_time <= x"00000000000";
      C5_spill_number <= x"000000";
      C5_spill_type <= "000";
      C5_valid <= '0';
      C5_fifo_read <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      --default the C5fifo reader to zero
      C5_fifo_read <= '0';
      if C5_valid = '0' then
        -- read from fifo if not empty
        if C5_fifo_empty = '0' then
          C5_fifo_read <= '1';
        end if;

        if C5_fifo_data_valid = '1' then
          C5_spill_time   <= C5_fifo_data(43 downto 0);
          C5_spill_number <= C5_fifo_data(67 downto 44);
          C5_spill_type   <= C5_fifo_data(70 downto 68);
          C5_valid        <= '1';
          -- update processed events_in
          events_in       <= std_logic_vector(unsigned(events_in) + 1);
        end if;
      else
        if sending_to_TRM = '1' then
          C5_valid <= '0';
        end if;
      end if;
    end if;
  end process C5_readout;



  -----------------------------------------------------------------------------
  -- State machine that waits for a new event and then adds it to the 8b10b sender
  -----------------------------------------------------------------------------
  spill_checking : process (clk125, reset_local(0))
  begin  -- process spill_checking
    if reset_local(0) = '1' then                 -- asynchronous reset (active high)
      events_out        <= x"00000000";

      Active_TDC_stream <= 4; -- 4 = no TDC since TDC number goes 0 to 3
      send_state <= SEND_IDLE;
      sending_to_TRM    <= '0';
      send_header_counter <= 0;
      
      
      spill_8b10b_wr <= '0';
      LB_data_word_k_mask <= x"0";
      LB_data_word <= x"00000000";
      LB_data_word_count <= x"00000000";

      total_spill_size <= x"0000";
      fatal_error <= '0';
      spill_numbers_match <= x"0";
      TDC_Size <= (others => (others => '0'));
      TDC_size_error <= x"0";      
      TDC_Size_left <= (others => (others => '0'));
      
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      spill_8b10b_wr      <= '0';       -- default is not to be writing to the
                                    -- FIFO
      LB_data_word_k_mask <= x"0";

      TDC_missing_trailer_bit <= x"0"; -- missing trailer bit flag
      
      -- only add new words when the fifo has space
      if fifo_8b_almost_full = '0' then
        case send_state is
          -----------------------------------------------------------------------
          -- IDLE state: wait for new event
          when SEND_IDLE =>
            if C5_valid = '1' and  -- The C5 header has been loaded            
              (TDC_has_header and TDC_enable) = TDC_enable and  -- The TDC headers have been loaded
              (TDC_has_trailer and TDC_enable) = TDC_enable then  -- All TDC data has arrived

              -- change our state so we start sending data
              send_state <= CHECK_NEW_DATA;
            else
              send_state <= SEND_IDLE;
            end if;
          -----------------------------------------------------------------------
          -- CHECK_NEW_DATA state: latch the tdc attributes
          when CHECK_NEW_DATA =>

            -- fatal error (we only care about this on active TDCs)
            fatal_error <= or_reduce(TDC_fatal_error and TDC_enable); 

            for iTDC in 0 to 3 loop
              if TDC_enable(iTDC) = '1' then
                -- check spill numbers
                spill_numbers_match(iTDC) <= '0';
                if C5_spill_number = TDC_SpillNumber(iTDC) then
                  spill_numbers_match(iTDC) <= '1';
                end if;
                -- save the spill size
                TDC_Size(iTDC)       <= TDC_spill_size(iTDC);
                TDC_size_error(iTDC) <= TDC_spill_size_error(iTDC);

              else
                spill_numbers_match(iTDC) <= '1';       -- since we use the not
                                                        -- of this as an error,
                                                        -- we put this to 1
                                                        -- since this tdc is disabled
                --store the event building size            
                TDC_Size(iTDC)            <= "000"&x"000";
                TDC_size_error(iTDC)      <= '0';
              end if;
            end loop;  -- iTDC              

            send_state <= BUILD_HEADER;
          -----------------------------------------------------------------------
          -- BUILD_HEADER state: put together the final values we need to send
          -- the data
          when BUILD_HEADER =>
            ------------------------------------------
            -- compute numbers used in the header
            -- compute the total size of the data (header + trailer + TDC(i))
            total_spill_size <= x"000A" + x"0003" + TDC_Size(0) + TDC_Size(1) + TDC_Size(2) + TDC_Size(3);

            -- cached copies of the TDC_Sizes to use for counting down during
            -- sending
            TDC_Size_left <= TDC_Size;

            -- start the data flow 
            send_header_counter <= 0;
            sending_to_TRM      <= '1';
            send_state          <= SEND_HEADER;

          -----------------------------------------------------------------------
          -- SEND_HEADER state: send the LogicBoard header
          when SEND_HEADER =>
            spill_8b10b_wr      <= '1';
            send_header_counter <= send_header_counter + 1;
            LB_data_word_count  <= LB_data_word_count + 1;
            case send_header_counter is
              when 0 =>
                -- send start of data and three zero bytes
                LB_data_word_k_mask <= "1000";
                LB_data_word        <= OUTPUT_START(7 downto 0) & x"000000";
                LB_data_word_count  <= x"00000004"; -- stores the initial 3C
                                                    -- word (this word), the
                                                    -- trailing count word,
                                                    -- trailing CRC word and
                                                    -- trailing end word
              when 1 =>                 -- send total size
                LB_data_word(31 downto 16) <= x"0000";
                LB_data_word(15 downto 0)  <= std_logic_vector(total_spill_size);
              when 2 =>
                LB_data_word <= Firmware_v;
              when 3 =>  -- send the spill number of the C5 system
                LB_data_word <= x"00" & C5_spill_number;
              when 4 =>                 -- sent hit time bits 43 downto 12
                LB_data_word <= C5_spill_time(43 downto 12);
              when 5 =>                 -- send spill summary and LSbs timing
                -- lsbs of the time
                LB_data_word(11 downto 0)  <= C5_spill_time(11 downto 0);
                -- TDC enable mask
                LB_data_word(15 downto 12) <= TDC_enable;
                -- In sync
                LB_data_word(19 downto 16) <= not spill_numbers_match;
                -- Error
                if (TDC_enable and spill_numbers_match) = TDC_enable then
                  LB_data_word(20) <= '0';
                else
                  LB_data_word(20) <= '1';
                end if;
                -- Fatal error
                LB_data_word(21)           <= fatal_error;
                -- spill type
                LB_data_word(24 downto 22) <= C5_spill_type;
                LB_data_word(31 downto 25) <= "0000000";

              when 6 to 9 =>            --TDC summaries
                -- enabled
                LB_data_word(31)          <= TDC_enable(send_header_counter - 6);
                -- out of sync
                LB_data_word(30)          <= TDC_enable(send_header_counter - 6) and (not spill_numbers_match(send_header_counter - 6));                
                LB_data_word(29)          <= TDC_size_error(send_header_counter - 6);
                LB_data_word(28 downto 15) <= "00"&x"000";
                LB_data_word(14 downto 0) <= std_logic_vector(TDC_Size(send_header_counter - 6)(14 downto 0));

                if send_header_counter = 9 then
                  --Move on to next state
                  send_state <= SEND_TDC_DATA;
                  if TDC_Size_left(0) /= x"0000" then
                    Active_TDC_stream <= 0;
                  elsif TDC_Size_left(1) /= x"0000" then
                    Active_TDC_stream <= 1;
                  elsif TDC_Size_left(2) /= x"0000" then
                    Active_TDC_stream <= 2;
                  elsif TDC_Size_left(3) /= x"0000" then
                    Active_TDC_stream <= 3;
                  else
                    Active_TDC_stream <= 4;  -- turn off tdc streaming
                    send_state        <= SEND_TRAILER;
                  end if;
                  
                end if;
              when others => null;
            end case;



          -----------------------------------------------------------------------
          -- SEND_TDC_DATA state: send the data from the TDCs
          when SEND_TDC_DATA =>
            spill_8b10b_wr     <= '1';
            LB_data_word       <= TDC_header(Active_TDC_stream)(0)(31 downto 0);
            LB_data_word_count <= LB_data_word_count + 1;
            TDC_Size_left(Active_TDC_stream) <= TDC_Size_left(Active_TDC_stream) - 1;
            if TDC_Size_left(Active_TDC_stream) = x"0001" then              
              --choose next thing to stream
              if Active_TDC_stream < 1 and TDC_Size_left(1) /= x"0000" then
                Active_TDC_stream <= 1;
              elsif Active_TDC_stream < 2 and TDC_Size_left(2) /= x"0000" then
                Active_TDC_stream <= 2;
              elsif Active_TDC_stream < 3 and TDC_Size_left(3) /= x"0000" then
                Active_TDC_stream <= 3;
              else
                Active_TDC_stream <= 4;  -- turn off tdc streaming
                send_state        <= SEND_TRAILER;
              end if;

              --check that this was a trailer word
              if TDC_header(Active_TDC_stream)(0)(32) = '0' then
                TDC_missing_trailer_bit(Active_TDC_stream) <= '1';
              end if;
              
            end if;
          when SEND_TRAILER =>
            spill_8b10b_wr      <= '1';
            LB_data_word_k_mask <= x"0";
            LB_data_word  <= std_logic_vector(LB_data_word_count);
            LB_data_word_count  <= x"00000000";
            send_state          <= SEND_CRC;
          when SEND_CRC =>
            spill_8b10b_wr      <= '1';
            LB_data_word_k_mask <= x"0";
            LB_data_word        <= x"00000000";
            send_state          <= SEND_END;
          when SEND_END =>
            spill_8b10b_wr      <= '1';
            LB_data_word_k_mask <= x"1";
            LB_data_word        <= x"000000" & OUTPUT_END(7 downto 0);
            sending_to_TRM      <= '0';
            send_state          <= SEND_IDLE;
            -- update processed events_out
            events_out          <= std_logic_vector(unsigned(events_out) + 1);
          when others => null;
        end case;
      end if;
    end if;
  end process spill_checking;


  -- 8b10b streamer @125Mhz
  spill_8b10b_in(35 downto 27) <= LB_data_word_k_mask(3) & LB_data_word(31 downto 24);
  spill_8b10b_in(26 downto 18) <= LB_data_word_k_mask(2) & LB_data_word(23 downto 16);
  spill_8b10b_in(17 downto 9)  <= LB_data_word_k_mask(1) & LB_data_word(15 downto 8);
  spill_8b10b_in(8 downto 0)   <= LB_data_word_k_mask(0) & LB_data_word(7 downto 0);

  spill_8b10b_1 : spill_8b10b
    port map (
      rst         => reset_local(0),
      rd_clk      => clk125,
      wr_clk      => clk125,
      din         => spill_8b10b_in,
      wr_en       => spill_8b10b_wr,
      rd_en       => fifo_8b_rd,
      dout        => fifo_8b_out,
      full        => fifo_8b_full,
      almost_full => fifo_8b_almost_full,
      empty       => fifo_8b_empty,
      valid       => fifo_8b_valid);



  clk_en_10b : process (clk125, reset_local(0))
  begin  -- process clk_en_10b
    if reset_local(0) = '1' then                 -- asynchronous reset (active high)
      clk_en_10b_counter <= 0;
    elsif clk125'event and clk125 = '1' then  -- rising clock edge      
      if clk_en_10b_counter = 9 then
        clk_en_10b_counter <= 0;
      else
        clk_en_10b_counter <= clk_en_10b_counter + 1;
      end if;
    end if;
  end process clk_en_10b;

  Generate_bytes : process (clk125, reset_local(0))
  begin  -- process Generate_bytes
    if reset_local(0) = '1' then                 -- asynchronous reset (active high)
      fifo_8b_rd       <= '0';
      in_8b            <= OUTPUT_IDLE;
      encode_8b_to_10b <= '0';
    elsif clk125'event and clk125 = '1' then  -- rising clock edge     
      if clk_en_10b_counter = 7 then
        --load next byte if it is available
        if fifo_8b_empty = '0' then
          fifo_8b_rd <= '1';
        end if;
      elsif clk_en_10b_counter = 9 then
        encode_8b_to_10b <= '1';
        -- assign real data if available, or IDLE pattern if not
        if fifo_8b_valid = '1' then
          in_8b <= fifo_8b_out;
        else
          in_8b <= OUTPUT_IDLE;
        end if;
      else
        -- reset stream read to zero and 8b10b byte to IDLE pattern
        fifo_8b_rd       <= '0';
        encode_8b_to_10b <= '0';
      end if;
    end if;
  end process Generate_bytes;


  encode_8b10b_top_1 : encode_8b10b_top
    generic map (
      C_HAS_CE => 1)
    port map (
      CLK  => clk125,
      DIN  => in_8b(7 downto 0),
      KIN  => in_8b(8),
      DOUT => out_10b,
      CE   => encode_8b_to_10b);

  Debug_Output_FIFO_control: process (clk125, reset_local(0)) is
  begin  -- process Debug_Output_FIFO_control
    if reset_local(0) = '1' then        -- asynchronous reset (active high)
      debug_8b <= (others => (others => '0'));
      debug_wr <= '0';
      debug_encode_buffer <= (others => '0');
      debug_reset <= '1';
      debug_fifo_active <= '0';      
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      debug_reset <= '0';      
      debug_wr <= '0';
      
      --buffer the data land latch signals
      debug_8b(0 to 6) <= in_8b & debug_8b(0 to 5);
      debug_encode_buffer(6 downto 0) <= debug_encode_buffer(5 downto 0) & encode_8b_to_10b;
      
      if debug_fifo_active = '1' and debug_encode_buffer(5) = '1' then
        if debug_fifo_full = '0' then
          -- stream data into the fifo not full
          debug_wr <= '1';
        else
          -- turn off recording data now that the fifo is full
          debug_fifo_active <= '0';
        end if;         
      elsif encode_8b_to_10b = '1' and in_8b = OUTPUT_START then
        -- reset the fifo for the new event
        debug_reset <= '1';
      elsif debug_encode_buffer(0) = '1' and debug_8b(0) = OUTPUT_START then
        -- Begin taking data for the incoming event
        debug_fifo_active <= '1';
      end if;
    end if;
  end process Debug_Output_FIFO_control;
  
  Debug_Output_FIFO_1: entity work.Debug_Output_FIFO
    port map (
      clk   => clk125,
      rst   => debug_reset,
      din   => debug_8b(6),
      wr_en => debug_wr,
      rd_en => control_local.LB_FIFO_rd,
      dout  => monitor_local.LB_FIFO_data,
      full  => debug_fifo_full,
      empty => monitor_local.LB_fifo_empty,
      valid => monitor_local.LB_fifo_data_valid);
  monitor_local.LB_fifo_full <= debug_fifo_full;
  
  serialize_8b10b : process (clk125, reset_local(0))
  begin  -- process serialize_8b10b
    if reset_local(0) = '1' then                 -- asynchronous reset (active high)
      out_10b_shift <= "00"&x"00";
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      -- serialize out 10b data
      data_out      <= out_10b_shift(0);
      out_10b_shift <= '0' & out_10b_shift(9 downto 1);

      -- load a new 10bit word for output
      if clk_en_10b_counter = 9 then
        out_10b_shift <= out_10b;
      end if;
    end if;
  end process serialize_8b10b;



  missing_trailer_counter: process (clk125, reset_local(0)) is
  begin  -- process missing_trailer_counter
    if reset_local(0) = '1' then        -- asynchronous reset (active high)
      
    elsif clk125'event and clk125 = '1' then  -- rising clock edge
      for iTDC in 0 to 3 loop
        if TDC_missing_trailer_bit(iTDC) = '1' then
          TDC_missing_trailer_bit_counter(iTDC) <= TDC_missing_trailer_bit_counter(iTDC) + 1;
        end if;
        monitor_local.TDC_missing_trailer_bits(iTDC) <= TDC_missing_trailer_bit_counter(iTDC);
      end loop;  -- iTDC
    end if;
  end process missing_trailer_counter;

end arch;

