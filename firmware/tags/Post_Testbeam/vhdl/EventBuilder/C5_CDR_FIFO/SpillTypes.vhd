----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

--Spill types
package SpillTypes is
  constant SPILL_NORMAL : std_logic_vector(3 downto 0) := x"0";
  constant SPILL_CAL_1 : std_logic_vector(3 downto 0) := x"1";
  constant SPILL_CAL_2 : std_logic_vector(3 downto 0) := x"2";
end SpillTypes;
