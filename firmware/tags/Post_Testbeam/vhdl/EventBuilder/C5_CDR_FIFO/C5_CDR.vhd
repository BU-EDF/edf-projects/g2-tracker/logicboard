-------------------------------------------------------------------------------
-- g-2 tracker logic C5 CDR
-- Dan Gastler
-- CDR for C5
-- for C5 doc, google : Clock-Command Combined Carrier Coding (C5) – A DC Balanced Coding Scheme for Transmitting Messages with Clock
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.std_logic_misc.or_reduce;

library UNISIM;
use UNISIM.vcomponents.all;

entity C5_CDR is
  
  port (
    clk_40       : in  std_logic;          -- 40 Mhz clock for 10Mhz C5
    C5_stream    : in  std_logic;          -- C5 raw stream

    c5_data      : out std_logic_vector(3 downto 0);
    c5_is_k_char : out std_logic;
    c5_data_valid: out std_logic
    );

end C5_CDR;

architecture arch of C5_CDR is

  signal C5_1_8 : std_logic := '0';
  signal C5_3_8 : std_logic := '0';
  signal C5_5_8 : std_logic := '1';

  signal delayed_3_8 : std_logic_vector(5 downto 0) := "111111";
  signal delayed_5_8 : std_logic_vector(5 downto 0) := "111111";

  signal Y : std_logic_vector(4 downto 0) := "00000";
  signal local_C5 : std_logic_vector(3 downto 0) := "0000";

  signal k_char : std_logic := '0';
  signal local_c5_valid : std_logic := '0';
  signal c5_count : natural range 0 to 6 := 0;
  
begin  -- arch

  -----------------------------------------------------------------------------
  -- Generate the delayed phases of the c5 stream
  --   sample the c5 stream at 3/8 and 5/8ths through the C5 period
  --   this is done by sampling on the inverse 40Mhz clock and pipe-lining the samples
  delayed_phase_generator: process (clk_40)
  begin
    if clk_40'event and clk_40 = '0' then  -- falling clock edge
      C5_1_8 <= C5_stream;
      C5_3_8 <= C5_1_8;
      C5_5_8 <= C5_3_8;
    end if;
  end process delayed_phase_generator;

  -----------------------------------------------------------------------------
  -- Build a pipeline to use to determine the 4 bit code
  --   create a pipe-line of the 3/8 and 5/8 delayed sampling of the C5 datastream
  --   to be used to look for transitions on the falling edge of normal clock
  --   this is also used to determine the C5 data bits
  delayed_sample_pipeline: process (c5_stream)
  begin
    if c5_stream'event and c5_stream = '1' then  -- rising clock edge
      -- we not the 3_8 delay for convenience 
      delayed_3_8(0) <= not C5_3_8;
      delayed_5_8(0) <= C5_5_8;
      -- pipeline
      for iDelay in 0 to 4 loop
        delayed_3_8(iDelay+1) <= delayed_3_8(iDelay);
        delayed_5_8(iDelay+1) <= delayed_5_8(iDelay);
      end loop;  -- iDelay
    end if;
  end process delayed_sample_pipeline;


  -----------------------------------------------------------------------------
  -- Generate bits of the C5 data
  --   this process converts the pipeline of sample (delayed_*_8(n)) into the four
  --   bits of C5 commands
  decode_bits: process (C5_stream)
  begin  -- process decode_bits
    if C5_stream'event and C5_stream = '1' then  -- rising clock edge
      -- We delay saving the Ys by one tick because we need to compute if a k char
      local_C5(3 downto 0) <= Y(4 downto 1);

      
      -- The MSB is set by the first pulse (delayed_*_8(5) in the pipeline) was
      -- long or short.  Long is '1', short is '0'
      Y(4) <= (not delayed_3_8(5)) and delayed_5_8(5);  -- comparing before and
                                                        -- after the clock edge
                                                        -- to determine if it
                                                        -- is long or short
      -- The rest of the bits are set by if they are a 50% duty cycle pulse or
      -- not. 
      for iBit in 0 to 3 loop
        Y(iBit) <= not (delayed_3_8(1+iBit) and delayed_5_8(1+iBit));  -- if this
                                                                       -- was a 50%
                                                                       -- or not cycle
      end loop;
    end if;
  end process decode_bits;

  -----------------------------------------------------------------------------
  -- Compute control character and push data into the FIFO
  --   Control words are set by if the first pulse and the third pulse were
  --     both short/long or if they differed.
  --   If no third pulse, then this is not a control word
  --   Also, we use the Ys from the last clock, so we need to remember that
  --    for the pipelined delayed_*_8(n) signals.
  --   Also (2), Y(4) was the long/shortness of the first pulse.  
  data_control_lookup: process (C5_stream)
  begin  -- process data_control_lookup      
    if C5_stream'event and C5_stream = '1' then  -- rising clock edge
      -- cases with a third wide/narrow pulse
      case Y(3 downto 1) is
        when "111" | "110" =>
          -- We care about Y(1) in this case, which was delayed_*_8(3) last
          -- clock, so at this clock it is delayed_*_8(4)
          k_char <= Y(4) xor ((not delayed_3_8(4)) and delayed_5_8(4));  
        when "101" | "011" =>
          -- In this case we are about Y(0), which was delayed_*_8(2) last
          -- clock, so at this clock it is delayed_*_8(3)
          k_char <= Y(4) xor ((not delayed_3_8(3)) and delayed_5_8(3));
        when others => k_char <= '0';
      end case;
    end if;
  end process data_control_lookup;
  
  -----------------------------------------------------------------------------
  -- validate lock on to the incoming frame
  C5_lockon: process (C5_stream)
  begin  -- process C5_lockon    
    if C5_stream'event and C5_stream = '1' then  -- rising clock edge
      if Y(0) = '1' and c5_count = 0 then  -- this is the start of a new char
        c5_count <= 4;
      elsif c5_count > 1 then           -- count down to data valid
        c5_count <= c5_count -1;
      elsif c5_count = 1 then           -- the data is valid, so present
        c5_count <= c5_count -1;
        local_c5_valid <= '1';
      else                              -- data no longer valid
         local_c5_valid <= '0';
      end if;
      
    end if; 
  end process C5_lockon;

  -----------------------------------------------------------------------------
  -- final outputs
  -----------------------------------------------------------------------------
  c5_data <= local_C5;
  c5_is_k_char <= k_char;
  c5_data_valid <= local_C5_valid;
  
  
end arch;
