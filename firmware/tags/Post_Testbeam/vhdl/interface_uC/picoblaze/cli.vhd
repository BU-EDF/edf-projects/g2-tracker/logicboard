--
-------------------------------------------------------------------------------------------
-- Copyright � 2010-2013, Xilinx, Inc.
-- This file contains confidential and proprietary information of Xilinx, Inc. and is
-- protected under U.S. and international copyright and other intellectual property laws.
-------------------------------------------------------------------------------------------
--
-- Disclaimer:
-- This disclaimer is not a license and does not grant any rights to the materials
-- distributed herewith. Except as otherwise provided in a valid license issued to
-- you by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE
-- MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY
-- DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
-- INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT,
-- OR FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable
-- (whether in contract or tort, including negligence, or under any other theory
-- of liability) for any loss or damage of any kind or nature related to, arising
-- under or in connection with these materials, including for any direct, or any
-- indirect, special, incidental, or consequential loss or damage (including loss
-- of data, profits, goodwill, or any type of loss or damage suffered as a result
-- of any action brought by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-safe, or for use in any
-- application requiring fail-safe performance, such as life-support or safety
-- devices or systems, Class III medical devices, nuclear facilities, applications
-- related to the deployment of airbags, or any other applications that could lead
-- to death, personal injury, or severe property or environmental damage
-- (individually and collectively, "Critical Applications"). Customer assumes the
-- sole risk and liability of any use of Xilinx products in Critical Applications,
-- subject only to applicable laws and regulations governing limitations on product
-- liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
-------------------------------------------------------------------------------------------
--
--
-- Definition of a program memory for KCPSM6 including generic parameters for the 
-- convenient selection of device family, program memory size and the ability to include 
-- the JTAG Loader hardware for rapid software development.
--
-- This file is primarily for use during code development and it is recommended that the 
-- appropriate simplified program memory definition be used in a final production design. 
--
--    Generic                  Values             Comments
--    Parameter                Supported
--  
--    C_FAMILY                 "S6"               Spartan-6 device
--                             "V6"               Virtex-6 device
--                             "7S"               7-Series device 
--                                                  (Artix-7, Kintex-7, Virtex-7 or Zynq)
--
--    C_RAM_SIZE_KWORDS        1, 2 or 4          Size of program memory in K-instructions
--
--    C_JTAG_LOADER_ENABLE     0 or 1             Set to '1' to include JTAG Loader
--
-- Notes
--
-- If your design contains MULTIPLE KCPSM6 instances then only one should have the 
-- JTAG Loader enabled at a time (i.e. make sure that C_JTAG_LOADER_ENABLE is only set to 
-- '1' on one instance of the program memory). Advanced users may be interested to know 
-- that it is possible to connect JTAG Loader to multiple memories and then to use the 
-- JTAG Loader utility to specify which memory contents are to be modified. However, 
-- this scheme does require some effort to set up and the additional connectivity of the 
-- multiple BRAMs can impact the placement, routing and performance of the complete 
-- design. Please contact the author at Xilinx for more detailed information. 
--
-- Regardless of the size of program memory specified by C_RAM_SIZE_KWORDS, the complete 
-- 12-bit address bus is connected to KCPSM6. This enables the generic to be modified 
-- without requiring changes to the fundamental hardware definition. However, when the 
-- program memory is 1K then only the lower 10-bits of the address are actually used and 
-- the valid address range is 000 to 3FF hex. Likewise, for a 2K program only the lower 
-- 11-bits of the address are actually used and the valid address range is 000 to 7FF hex.
--
-- Programs are stored in Block Memory (BRAM) and the number of BRAM used depends on the 
-- size of the program and the device family. 
--
-- In a Spartan-6 device a BRAM is capable of holding 1K instructions. Hence a 2K program 
-- will require 2 BRAMs to be used and a 4K program will require 4 BRAMs to be used. It 
-- should be noted that a 4K program is not such a natural fit in a Spartan-6 device and 
-- the implementation also requires a small amount of logic resulting in slightly lower 
-- performance. A Spartan-6 BRAM can also be split into two 9k-bit memories suggesting 
-- that a program containing up to 512 instructions could be implemented. However, there 
-- is a silicon errata which makes this unsuitable and therefore it is not supported by 
-- this file.
--
-- In a Virtex-6 or any 7-Series device a BRAM is capable of holding 2K instructions so 
-- obviously a 2K program requires only a single BRAM. Each BRAM can also be divided into 
-- 2 smaller memories supporting programs of 1K in half of a 36k-bit BRAM (generally 
-- reported as being an 18k-bit BRAM). For a program of 4K instructions, 2 BRAMs are used.
--
--
-- Program defined by 'Z:\home\dan\LogicBoard\vhdl\interface_uC\picoblaze\cli.psm'.
--
-- Generated by KCPSM6 Assembler: 10 Jul 2015 - 17:08:01. 
--
-- Assembler used ROM_form template: ROM_form_JTAGLoader_14March13.vhd
--
-- Standard IEEE libraries
--
--
package jtag_loader_pkg is
 function addr_width_calc (size_in_k: integer) return integer;
end jtag_loader_pkg;
--
package body jtag_loader_pkg is
  function addr_width_calc (size_in_k: integer) return integer is
   begin
    if (size_in_k = 1) then return 10;
      elsif (size_in_k = 2) then return 11;
      elsif (size_in_k = 4) then return 12;
      else report "Invalid BlockRAM size. Please set to 1, 2 or 4 K words." severity FAILURE;
    end if;
    return 0;
  end function addr_width_calc;
end package body;
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.jtag_loader_pkg.ALL;
--
-- The Unisim Library is used to define Xilinx primitives. It is also used during
-- simulation. The source can be viewed at %XILINX%\vhdl\src\unisims\unisim_VCOMP.vhd
--  
library unisim;
use unisim.vcomponents.all;
--
--
entity cli is
  generic(             C_FAMILY : string := "S6"; 
              C_RAM_SIZE_KWORDS : integer := 1;
           C_JTAG_LOADER_ENABLE : integer := 0);
  Port (      address : in std_logic_vector(11 downto 0);
          instruction : out std_logic_vector(17 downto 0);
               enable : in std_logic;
                  rdl : out std_logic;                    
                  clk : in std_logic);
  end cli;
--
architecture low_level_definition of cli is
--
signal       address_a : std_logic_vector(15 downto 0);
signal        pipe_a11 : std_logic;
signal       data_in_a : std_logic_vector(35 downto 0);
signal      data_out_a : std_logic_vector(35 downto 0);
signal    data_out_a_l : std_logic_vector(35 downto 0);
signal    data_out_a_h : std_logic_vector(35 downto 0);
signal   data_out_a_ll : std_logic_vector(35 downto 0);
signal   data_out_a_lh : std_logic_vector(35 downto 0);
signal   data_out_a_hl : std_logic_vector(35 downto 0);
signal   data_out_a_hh : std_logic_vector(35 downto 0);
signal       address_b : std_logic_vector(15 downto 0);
signal       data_in_b : std_logic_vector(35 downto 0);
signal     data_in_b_l : std_logic_vector(35 downto 0);
signal    data_in_b_ll : std_logic_vector(35 downto 0);
signal    data_in_b_hl : std_logic_vector(35 downto 0);
signal      data_out_b : std_logic_vector(35 downto 0);
signal    data_out_b_l : std_logic_vector(35 downto 0);
signal   data_out_b_ll : std_logic_vector(35 downto 0);
signal   data_out_b_hl : std_logic_vector(35 downto 0);
signal     data_in_b_h : std_logic_vector(35 downto 0);
signal    data_in_b_lh : std_logic_vector(35 downto 0);
signal    data_in_b_hh : std_logic_vector(35 downto 0);
signal    data_out_b_h : std_logic_vector(35 downto 0);
signal   data_out_b_lh : std_logic_vector(35 downto 0);
signal   data_out_b_hh : std_logic_vector(35 downto 0);
signal        enable_b : std_logic;
signal           clk_b : std_logic;
signal            we_b : std_logic_vector(7 downto 0);
signal          we_b_l : std_logic_vector(3 downto 0);
signal          we_b_h : std_logic_vector(3 downto 0);
-- 
signal       jtag_addr : std_logic_vector(11 downto 0);
signal         jtag_we : std_logic;
signal       jtag_we_l : std_logic;
signal       jtag_we_h : std_logic;
signal        jtag_clk : std_logic;
signal        jtag_din : std_logic_vector(17 downto 0);
signal       jtag_dout : std_logic_vector(17 downto 0);
signal     jtag_dout_1 : std_logic_vector(17 downto 0);
signal         jtag_en : std_logic_vector(0 downto 0);
-- 
signal picoblaze_reset : std_logic_vector(0 downto 0);
signal         rdl_bus : std_logic_vector(0 downto 0);
--
constant BRAM_ADDRESS_WIDTH  : integer := addr_width_calc(C_RAM_SIZE_KWORDS);
--
--
component jtag_loader_6
generic(                C_JTAG_LOADER_ENABLE : integer := 1;
                                    C_FAMILY : string  := "V6";
                             C_NUM_PICOBLAZE : integer := 1;
                       C_BRAM_MAX_ADDR_WIDTH : integer := 10;
          C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                                C_JTAG_CHAIN : integer := 2;
                              C_ADDR_WIDTH_0 : integer := 10;
                              C_ADDR_WIDTH_1 : integer := 10;
                              C_ADDR_WIDTH_2 : integer := 10;
                              C_ADDR_WIDTH_3 : integer := 10;
                              C_ADDR_WIDTH_4 : integer := 10;
                              C_ADDR_WIDTH_5 : integer := 10;
                              C_ADDR_WIDTH_6 : integer := 10;
                              C_ADDR_WIDTH_7 : integer := 10);
port(              picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                           jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                          jtag_din : out STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                         jtag_addr : out STD_LOGIC_VECTOR(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
                          jtag_clk : out std_logic;
                           jtag_we : out std_logic;
                       jtag_dout_0 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_1 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_2 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_3 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_4 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_5 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_6 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_7 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end component;
--
begin
  --
  --  
  ram_1k_generate : if (C_RAM_SIZE_KWORDS = 1) generate
 
    s6: if (C_FAMILY = "S6") generate 
      --
      address_a(13 downto 0) <= address(9 downto 0) & "0000";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "0000000000000000000000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "0000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB16BWER
      generic map ( DATA_WIDTH_A => 18,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 18,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D0049000290C0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"00020041003013005000F023F020100050000060010EB001B031031200885000",
                    INIT_09 => X"D3FFD1010002D20100020041A030D00110200002D001103A0002D1010002D201",
                    INIT_0A => X"B12BB227A0B4D004B023015250000059208D00596099D00330031301003020A8",
                    INIT_0B => X"96079506940520BAD0109003D1031108D004500000815000015208E008910210",
                    INIT_0C => X"00B7A000102C607CD001B0235000D10311F4D708D607D506D405D00450009708",
                    INIT_0D => X"5000005900530040005300500053006000530070D00110780002D00110300002",
                    INIT_0E => X"102C0710A10010010610A10010010510A10010010410A1001030607CD002B023",
                    INIT_0F => X"114C112011721165116B116311611172115411201132112D1167500000C2A000",
                    INIT_10 => X"1AF31B0011001120113A11561120116411721161116F1142116311691167116F",
                    INIT_11 => X"000200530050D001102E000200530060D001102E00020053007000B710010064",
                    INIT_12 => X"115711001120113A1172116511661166117511425000005900530040D001102E",
                    INIT_13 => X"11641172116F115711001120113A1174116E1175116F1163112011641172116F",
                    INIT_14 => X"11641172116F115711001120112011201120113A11731165117A116911731120",
                    INIT_15 => X"1020000221571101D001A0100002E15EC120B220110000641A261B0111001120",
                    INIT_16 => X"1A2F1B010059D00110290002D1010002D201000200410020D00110280002D001",
                    INIT_17 => X"0002D00110200002E18CC3401300B423D1010002D20100020041B02300020064",
                    INIT_18 => X"00641A3C1B010059217A1301D1010002D20100020041A01001301124D0011040",
                    INIT_19 => X"1301D1010002D20100020041A01001301128D00110200002E1A1C3401300B423",
                    INIT_1A => X"D1010002D20100020041003000641A4C1B010059E1C4C3401300B42300592192",
                    INIT_1B => X"D20100020041A060A1A4C65016030650152C4506450613010530D00110200002",
                    INIT_1C => X"1A01D101000221D6D1FF21D2D1004BA01A101B075000005921BA9601D1010002",
                    INIT_1D => X"DD024D003DF0400740073003700160005000005921C83B001A03005921C83B00",
                    INIT_1E => X"3DFE01E201E201E201E2DD023DFDDD025D025D015000E1E39E011E3E50007000",
                    INIT_1F => X"3DFE01E201E201E2DD023DFD01E201E2DD025D0101E201E2DD025D025000DD02",
                    INIT_20 => X"01E201E201E201E2DD025D0101E201E201E2DD023DFD500001E201E201E2DD02",
                    INIT_21 => X"500001E201E201E2DD023DFE01E201E2DD025D01DD025D0201E25000DD025D02",
                    INIT_22 => X"1580500001E201E2DD025D0201E2DD023DFE01E201E2DD025D0101E2DD023DFD",
                    INIT_23 => X"450EDD023DFE01E201E2DD025D0101E2DD025D022238DD023DFDA236440601E2",
                    INIT_24 => X"01E201E201E2DD023DFE01E201E2DD025D01950201E2DD025D0201E22230A242",
                    INIT_25 => X"DD023DFE01E201E2DD025D0101E24400D602960201E2158014005D025000D502",
                    INIT_26 => X"022F0410A27C022F040001E630FE70016330622061106000500001E2E255450E",
                    INIT_27 => X"228410022284100150007000400610000205A282022F0430A280022F0420A27E",
                    INIT_28 => X"01E630FE70016220611060005000700040061080020500532284100422841003",
                    INIT_29 => X"22A4100150007000400610000205A2A2022F0420A2A0022F0410A29E022F0400",
                    INIT_2A => X"040001E630FE70016110600050007000400610800205005322A4100322A41002",
                    INIT_2B => X"021303400252022002400252A2CB022F0400500101F2A27E022F0410A2C7022F",
                    INIT_2C => X"10800205005322CD100322CD100222CD10015000700063306220400610000205",
                    INIT_2D => X"1AD31B021100112E1172116F1172117211651120116B11631141500070004006",
                    INIT_2E => X"A340A2F1D503A550152A1434A1401430A040142C607CD003B023500000590064",
                    INIT_2F => X"1430A040142C607CD002B0235000A2DE028A5000A2DE0264A2F7D503A2401401",
                    INIT_30 => X"1002500001D81001500001D81000500000590053003000530020A2DE02AAA140",
                    INIT_31 => X"1175116711721161112011641161114250000309030C030F6D0010F3500001D8",
                    INIT_32 => X"A040142C6326D001B0235000005900641A181B03110011731174116E1165116D",
                    INIT_33 => X"30304006400640064006130F5000E3359E01E3359F011FFF1EFF5000D003301F",
                    INIT_34 => X"A3516350D513A7B0A6A0A590848000B703330B700A600950084000B712FF1052",
                    INIT_35 => X"0A600950084000B712FF002030304006400640064006130F5000E34293011201",
                    INIT_36 => X"1C005000E35C93011201636CA7B0636CA6A0636CA590636C848000B703330B70",
                    INIT_37 => X"D20F035400D01257D0011020D001103A0059005300C0D0011050D00110200D00",
                    INIT_38 => X"D20F035400D01258D0011032D001102C6399D20F035400D01256D00110316399",
                    INIT_39 => X"4006400600D023B1DC061C01005923CAD20F033A00D0D0011033D001102C6399",
                    INIT_3A => X"00C21050400640064006400600D0440644064406440604C000B7105040064006",
                    INIT_3B => X"0002D00110200002005300D0D00110430002D00110440002D001105400022371",
                    INIT_3C => X"00020059D0011034D001102C50000059D00110520002D00110520002D0011045",
                    INIT_3D => X"D00110400002D00110200002005300D0D00110430002D00110440002D0011054",
                    INIT_3E => X"036F100123F0D40200B71040036F100023EAD40100B7104050000059005300C0",
                    INIT_3F => X"0401DF091F805000036F100323FCD40800B71040036F100223F6D40400B71040",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"AAAAAAA801041034A88888A2834A2AA80308AAA90D2AAD136AA228AAA0A8AAAA",
                   INITP_02 => X"A2D0AA8A0A2AA88A298B420AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAA",
                   INITP_03 => X"2A8A8A8A2AA20B4B814DA9696B760A9AA8D45522AA882D0A6AA122D0829AA848",
                   INITP_04 => X"88B4B8E38E2355AD8A890808AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA",
                   INITP_05 => X"034BAED134000D2A0AAAAAAD2A222D528A2E0B8E235B4A888B4B8E388D5B4A88",
                   INITP_06 => X"6088A2202D777768021154B5F5568021154B74280D2A0AAAAAAAAA4A28A2A238",
                   INITP_07 => X"A28C88C88C88C8A88A288A28A88A8A28A288A28A95515495535B62236088D823")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a(31 downto 0),
                  DOPA => data_out_a(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b(31 downto 0),
                  DOPB => data_out_b(35 downto 32), 
                   DIB => data_in_b(31 downto 0),
                  DIPB => data_in_b(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --               
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D0049000290C0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"00020041003013005000F023F020100050000060010EB001B031031200885000",
                    INIT_09 => X"D3FFD1010002D20100020041A030D00110200002D001103A0002D1010002D201",
                    INIT_0A => X"B12BB227A0B4D004B023015250000059208D00596099D00330031301003020A8",
                    INIT_0B => X"96079506940520BAD0109003D1031108D004500000815000015208E008910210",
                    INIT_0C => X"00B7A000102C607CD001B0235000D10311F4D708D607D506D405D00450009708",
                    INIT_0D => X"5000005900530040005300500053006000530070D00110780002D00110300002",
                    INIT_0E => X"102C0710A10010010610A10010010510A10010010410A1001030607CD002B023",
                    INIT_0F => X"114C112011721165116B116311611172115411201132112D1167500000C2A000",
                    INIT_10 => X"1AF31B0011001120113A11561120116411721161116F1142116311691167116F",
                    INIT_11 => X"000200530050D001102E000200530060D001102E00020053007000B710010064",
                    INIT_12 => X"115711001120113A1172116511661166117511425000005900530040D001102E",
                    INIT_13 => X"11641172116F115711001120113A1174116E1175116F1163112011641172116F",
                    INIT_14 => X"11641172116F115711001120112011201120113A11731165117A116911731120",
                    INIT_15 => X"1020000221571101D001A0100002E15EC120B220110000641A261B0111001120",
                    INIT_16 => X"1A2F1B010059D00110290002D1010002D201000200410020D00110280002D001",
                    INIT_17 => X"0002D00110200002E18CC3401300B423D1010002D20100020041B02300020064",
                    INIT_18 => X"00641A3C1B010059217A1301D1010002D20100020041A01001301124D0011040",
                    INIT_19 => X"1301D1010002D20100020041A01001301128D00110200002E1A1C3401300B423",
                    INIT_1A => X"D1010002D20100020041003000641A4C1B010059E1C4C3401300B42300592192",
                    INIT_1B => X"D20100020041A060A1A4C65016030650152C4506450613010530D00110200002",
                    INIT_1C => X"1A01D101000221D6D1FF21D2D1004BA01A101B075000005921BA9601D1010002",
                    INIT_1D => X"DD024D003DF0400740073003700160005000005921C83B001A03005921C83B00",
                    INIT_1E => X"3DFE01E201E201E201E2DD023DFDDD025D025D015000E1E39E011E3E50007000",
                    INIT_1F => X"3DFE01E201E201E2DD023DFD01E201E2DD025D0101E201E2DD025D025000DD02",
                    INIT_20 => X"01E201E201E201E2DD025D0101E201E201E2DD023DFD500001E201E201E2DD02",
                    INIT_21 => X"500001E201E201E2DD023DFE01E201E2DD025D01DD025D0201E25000DD025D02",
                    INIT_22 => X"1580500001E201E2DD025D0201E2DD023DFE01E201E2DD025D0101E2DD023DFD",
                    INIT_23 => X"450EDD023DFE01E201E2DD025D0101E2DD025D022238DD023DFDA236440601E2",
                    INIT_24 => X"01E201E201E2DD023DFE01E201E2DD025D01950201E2DD025D0201E22230A242",
                    INIT_25 => X"DD023DFE01E201E2DD025D0101E24400D602960201E2158014005D025000D502",
                    INIT_26 => X"022F0410A27C022F040001E630FE70016330622061106000500001E2E255450E",
                    INIT_27 => X"228410022284100150007000400610000205A282022F0430A280022F0420A27E",
                    INIT_28 => X"01E630FE70016220611060005000700040061080020500532284100422841003",
                    INIT_29 => X"22A4100150007000400610000205A2A2022F0420A2A0022F0410A29E022F0400",
                    INIT_2A => X"040001E630FE70016110600050007000400610800205005322A4100322A41002",
                    INIT_2B => X"021303400252022002400252A2CB022F0400500101F2A27E022F0410A2C7022F",
                    INIT_2C => X"10800205005322CD100322CD100222CD10015000700063306220400610000205",
                    INIT_2D => X"1AD31B021100112E1172116F1172117211651120116B11631141500070004006",
                    INIT_2E => X"A340A2F1D503A550152A1434A1401430A040142C607CD003B023500000590064",
                    INIT_2F => X"1430A040142C607CD002B0235000A2DE028A5000A2DE0264A2F7D503A2401401",
                    INIT_30 => X"1002500001D81001500001D81000500000590053003000530020A2DE02AAA140",
                    INIT_31 => X"1175116711721161112011641161114250000309030C030F6D0010F3500001D8",
                    INIT_32 => X"A040142C6326D001B0235000005900641A181B03110011731174116E1165116D",
                    INIT_33 => X"30304006400640064006130F5000E3359E01E3359F011FFF1EFF5000D003301F",
                    INIT_34 => X"A3516350D513A7B0A6A0A590848000B703330B700A600950084000B712FF1052",
                    INIT_35 => X"0A600950084000B712FF002030304006400640064006130F5000E34293011201",
                    INIT_36 => X"1C005000E35C93011201636CA7B0636CA6A0636CA590636C848000B703330B70",
                    INIT_37 => X"D20F035400D01257D0011020D001103A0059005300C0D0011050D00110200D00",
                    INIT_38 => X"D20F035400D01258D0011032D001102C6399D20F035400D01256D00110316399",
                    INIT_39 => X"4006400600D023B1DC061C01005923CAD20F033A00D0D0011033D001102C6399",
                    INIT_3A => X"00C21050400640064006400600D0440644064406440604C000B7105040064006",
                    INIT_3B => X"0002D00110200002005300D0D00110430002D00110440002D001105400022371",
                    INIT_3C => X"00020059D0011034D001102C50000059D00110520002D00110520002D0011045",
                    INIT_3D => X"D00110400002D00110200002005300D0D00110430002D00110440002D0011054",
                    INIT_3E => X"036F100123F0D40200B71040036F100023EAD40100B7104050000059005300C0",
                    INIT_3F => X"0401DF091F805000036F100323FCD40800B71040036F100223F6D40400B71040",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"AAAAAAA801041034A88888A2834A2AA80308AAA90D2AAD136AA228AAA0A8AAAA",
                   INITP_02 => X"A2D0AA8A0A2AA88A298B420AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAA",
                   INITP_03 => X"2A8A8A8A2AA20B4B814DA9696B760A9AA8D45522AA882D0A6AA122D0829AA848",
                   INITP_04 => X"88B4B8E38E2355AD8A890808AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA",
                   INITP_05 => X"034BAED134000D2A0AAAAAAD2A222D528A2E0B8E235B4A888B4B8E388D5B4A88",
                   INITP_06 => X"6088A2202D777768021154B5F5568021154B74280D2A0AAAAAAAAA4A28A2A238",
                   INITP_07 => X"A28C88C88C88C8A88A288A28A88A8A28A288A28A95515495535B62236088D823")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D0049000290C0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"00020041003013005000F023F020100050000060010EB001B031031200885000",
                    INIT_09 => X"D3FFD1010002D20100020041A030D00110200002D001103A0002D1010002D201",
                    INIT_0A => X"B12BB227A0B4D004B023015250000059208D00596099D00330031301003020A8",
                    INIT_0B => X"96079506940520BAD0109003D1031108D004500000815000015208E008910210",
                    INIT_0C => X"00B7A000102C607CD001B0235000D10311F4D708D607D506D405D00450009708",
                    INIT_0D => X"5000005900530040005300500053006000530070D00110780002D00110300002",
                    INIT_0E => X"102C0710A10010010610A10010010510A10010010410A1001030607CD002B023",
                    INIT_0F => X"114C112011721165116B116311611172115411201132112D1167500000C2A000",
                    INIT_10 => X"1AF31B0011001120113A11561120116411721161116F1142116311691167116F",
                    INIT_11 => X"000200530050D001102E000200530060D001102E00020053007000B710010064",
                    INIT_12 => X"115711001120113A1172116511661166117511425000005900530040D001102E",
                    INIT_13 => X"11641172116F115711001120113A1174116E1175116F1163112011641172116F",
                    INIT_14 => X"11641172116F115711001120112011201120113A11731165117A116911731120",
                    INIT_15 => X"1020000221571101D001A0100002E15EC120B220110000641A261B0111001120",
                    INIT_16 => X"1A2F1B010059D00110290002D1010002D201000200410020D00110280002D001",
                    INIT_17 => X"0002D00110200002E18CC3401300B423D1010002D20100020041B02300020064",
                    INIT_18 => X"00641A3C1B010059217A1301D1010002D20100020041A01001301124D0011040",
                    INIT_19 => X"1301D1010002D20100020041A01001301128D00110200002E1A1C3401300B423",
                    INIT_1A => X"D1010002D20100020041003000641A4C1B010059E1C4C3401300B42300592192",
                    INIT_1B => X"D20100020041A060A1A4C65016030650152C4506450613010530D00110200002",
                    INIT_1C => X"1A01D101000221D6D1FF21D2D1004BA01A101B075000005921BA9601D1010002",
                    INIT_1D => X"DD024D003DF0400740073003700160005000005921C83B001A03005921C83B00",
                    INIT_1E => X"3DFE01E201E201E201E2DD023DFDDD025D025D015000E1E39E011E3E50007000",
                    INIT_1F => X"3DFE01E201E201E2DD023DFD01E201E2DD025D0101E201E2DD025D025000DD02",
                    INIT_20 => X"01E201E201E201E2DD025D0101E201E201E2DD023DFD500001E201E201E2DD02",
                    INIT_21 => X"500001E201E201E2DD023DFE01E201E2DD025D01DD025D0201E25000DD025D02",
                    INIT_22 => X"1580500001E201E2DD025D0201E2DD023DFE01E201E2DD025D0101E2DD023DFD",
                    INIT_23 => X"450EDD023DFE01E201E2DD025D0101E2DD025D022238DD023DFDA236440601E2",
                    INIT_24 => X"01E201E201E2DD023DFE01E201E2DD025D01950201E2DD025D0201E22230A242",
                    INIT_25 => X"DD023DFE01E201E2DD025D0101E24400D602960201E2158014005D025000D502",
                    INIT_26 => X"022F0410A27C022F040001E630FE70016330622061106000500001E2E255450E",
                    INIT_27 => X"228410022284100150007000400610000205A282022F0430A280022F0420A27E",
                    INIT_28 => X"01E630FE70016220611060005000700040061080020500532284100422841003",
                    INIT_29 => X"22A4100150007000400610000205A2A2022F0420A2A0022F0410A29E022F0400",
                    INIT_2A => X"040001E630FE70016110600050007000400610800205005322A4100322A41002",
                    INIT_2B => X"021303400252022002400252A2CB022F0400500101F2A27E022F0410A2C7022F",
                    INIT_2C => X"10800205005322CD100322CD100222CD10015000700063306220400610000205",
                    INIT_2D => X"1AD31B021100112E1172116F1172117211651120116B11631141500070004006",
                    INIT_2E => X"A340A2F1D503A550152A1434A1401430A040142C607CD003B023500000590064",
                    INIT_2F => X"1430A040142C607CD002B0235000A2DE028A5000A2DE0264A2F7D503A2401401",
                    INIT_30 => X"1002500001D81001500001D81000500000590053003000530020A2DE02AAA140",
                    INIT_31 => X"1175116711721161112011641161114250000309030C030F6D0010F3500001D8",
                    INIT_32 => X"A040142C6326D001B0235000005900641A181B03110011731174116E1165116D",
                    INIT_33 => X"30304006400640064006130F5000E3359E01E3359F011FFF1EFF5000D003301F",
                    INIT_34 => X"A3516350D513A7B0A6A0A590848000B703330B700A600950084000B712FF1052",
                    INIT_35 => X"0A600950084000B712FF002030304006400640064006130F5000E34293011201",
                    INIT_36 => X"1C005000E35C93011201636CA7B0636CA6A0636CA590636C848000B703330B70",
                    INIT_37 => X"D20F035400D01257D0011020D001103A0059005300C0D0011050D00110200D00",
                    INIT_38 => X"D20F035400D01258D0011032D001102C6399D20F035400D01256D00110316399",
                    INIT_39 => X"4006400600D023B1DC061C01005923CAD20F033A00D0D0011033D001102C6399",
                    INIT_3A => X"00C21050400640064006400600D0440644064406440604C000B7105040064006",
                    INIT_3B => X"0002D00110200002005300D0D00110430002D00110440002D001105400022371",
                    INIT_3C => X"00020059D0011034D001102C50000059D00110520002D00110520002D0011045",
                    INIT_3D => X"D00110400002D00110200002005300D0D00110430002D00110440002D0011054",
                    INIT_3E => X"036F100123F0D40200B71040036F100023EAD40100B7104050000059005300C0",
                    INIT_3F => X"0401DF091F805000036F100323FCD40800B71040036F100223F6D40400B71040",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"AAAAAAA801041034A88888A2834A2AA80308AAA90D2AAD136AA228AAA0A8AAAA",
                   INITP_02 => X"A2D0AA8A0A2AA88A298B420AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAA",
                   INITP_03 => X"2A8A8A8A2AA20B4B814DA9696B760A9AA8D45522AA882D0A6AA122D0829AA848",
                   INITP_04 => X"88B4B8E38E2355AD8A890808AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA",
                   INITP_05 => X"034BAED134000D2A0AAAAAAD2A222D528A2E0B8E235B4A888B4B8E388D5B4A88",
                   INITP_06 => X"6088A2202D777768021154B5F5568021154B74280D2A0AAAAAAAAA4A28A2A238",
                   INITP_07 => X"A28C88C88C88C8A88A288A28A88A8A28A288A28A95515495535B62236088D823")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate akv7;
    --
  end generate ram_1k_generate;
  --
  --
  --
  ram_2k_generate : if (C_RAM_SIZE_KWORDS = 2) generate
    --
    --
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004000C81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"FF010201024130012002013A02010201024130000023200000600E0131128800",
                    INIT_05 => X"070605BA100303080400810052E091102B27B404235200598D599903030130A8",
                    INIT_06 => X"00595340535053605370017802013002B7002C7C01230003F408070605040008",
                    INIT_07 => X"4C2072656B6361725420322D6700C2002C1000011000011000011000307C0223",
                    INIT_08 => X"025350012E025360012E025370B70164F30000203A56206472616F426369676F",
                    INIT_09 => X"64726F5700203A746E756F632064726F5700203A72656666754200595340012E",
                    INIT_0A => X"200257010110025E202000642601002064726F5700202020203A73657A697320",
                    INIT_0B => X"020120028C40002301020102412302642F015901290201020102412001280201",
                    INIT_0C => X"010102010241103028012002A1400023643C01597A0101020102411030240140",
                    INIT_0D => X"01024160A45003502C06060130012002010201024130644C0159C44000235992",
                    INIT_0E => X"0200F007070301000059C8000359C800010102D6FFD200A010070059BA010102",
                    INIT_0F => X"FEE2E2E202FDE2E20201E2E202020002FEE2E2E2E202FD02020100E3013E0000",
                    INIT_10 => X"00E2E2E202FEE2E202010202E2000202E2E2E2E20201E2E2E202FD00E2E2E202",
                    INIT_11 => X"0E02FEE2E20201E202023802FD3606E28000E2E20202E202FEE2E20201E202FD",
                    INIT_12 => X"02FEE2E20201E2000202E28000020002E2E2E202FEE2E2020102E20202E23042",
                    INIT_13 => X"840284010000060005822F30802F207E2F107C2F00E6FE013020100000E2550E",
                    INIT_14 => X"A4010000060005A22F20A02F109E2F00E6FE0120100000000680055384048403",
                    INIT_15 => X"134052204052CB2F0001F27E2F10C72F00E6FE011000000006800553A403A402",
                    INIT_16 => X"D302002E726F727265206B6341000006800553CD03CD02CD0100003020060005",
                    INIT_17 => X"30402C7C022300DE8A00DE64F703400140F103502A344030402C7C0323005964",
                    INIT_18 => X"756772612064614200090C0F00F300D80200D80100D800005953305320DEAA40",
                    INIT_19 => X"30060606060F0035013501FFFF00031F402C26012300596418030073746E656D",
                    INIT_1A => X"605040B7FF2030060606060F00420101515013B0A09080B73370605040B7FF52",
                    INIT_1B => X"0F54D0570120013A5953C0015001200000005C01016CB06CA06C906C80B73370",
                    INIT_1C => X"0606D0B1060159CA0F3AD00133012C990F54D0580132012C990F54D056013199",
                    INIT_1D => X"0201200253D001430201440201540271C25006060606D006060606C0B7500606",
                    INIT_1E => X"01400201200253D0014302014402015402590134012C00590152020152020145",
                    INIT_1F => X"010980006F03FC08B7406F02F604B7406F01F002B7406F00EA01B740005953C0",
                    INIT_20 => X"0E0100060009100A111E0001180209FFFF000920010A0701090009000201FF00",
                    INIT_21 => X"010245333D323D31000E0050403C0F003A0F00380F07100F060606060607FD00",
                    INIT_22 => X"060606060606FD000E015953050201023A01025359010201023A010252010252",
                    INIT_23 => X"5953050201023A01025359010252010252010245000E006C0F0720073007100F",
                    INIT_24 => X"01000E009F1095200B00000B2080009C9F0189020B00000B0208009101000E01",
                    INIT_25 => X"C6BB0EFC0080C00100B4200B0B20C0AE200B00AA020B0B020CA4020BAE01000E",
                    INIT_26 => X"00DC020BEA0100D9D30E08DA018000D9CB0E08DA008000D10100C6C10EFC0180",
                    INIT_27 => X"020B0D01000E0E0E0E0EF1200B0B0B0B402000EA200B000EE3020B0B0B0B0402",
                    INIT_28 => X"001A200B0B0B0B002006060606000E000D200B0007020B0B0B0B0002000E00FE",
                    INIT_29 => X"03230059533053403D211030102C7C02230059530F0007102C7C012300595305",
                    INIT_2A => X"59644B055900646E756F6620656369766564206F4E00591030102C100110347C",
                    INIT_2B => X"02015502014F020146020059014502014E02014F02014E027683102C7C012300",
                    INIT_2C => X"C73010C73010C73010C73010C730B83330BC8330102C7C01230059014402014E",
                    INIT_2D => X"02014502005953405350536053705380539053A053B010C73010C73010C73010",
                    INIT_2E => X"7C012300595310C730102C7C012300B840301030102C7C022300590152020152",
                    INIT_2F => X"305B8330102C7C08102A7C0810297C03230059A230B84430B8CC305B8330102C",
                    INIT_30 => X"4001B8304001B8304001B8403030B8304001B8304001B8304001B8403034B855",
                    INIT_31 => X"53405310C73010C730B8BE30B8CC305B8330102C7C01230059A230B84430B830",
                    INIT_32 => X"304001B8304001B8403034B855305B8330102C7C08102A7C0810297C03230059",
                    INIT_33 => X"405310C73010C730B8BE30B8304001B8304001B8304001B8403030B8304001B8",
                    INIT_34 => X"0E0E00A200C00300DA2000DA2000B8F02000015B8320F400102C7C0123005953",
                    INIT_35 => X"CA0659C10040924101DD60FC2060B23000B230B20000CB00AF30B201A630B200",
                    INIT_36 => X"304001000ED60E01DB00B0070130A00E0E0E013040008AA220B0A006593020CA",
                    INIT_37 => X"01000100010001000100400000B010D0EA010607F000FEB0070130A00E0E0E01",
                    INIT_38 => X"75646D656DAA000067726169746C756D005909010153C0070740000001000100",
                    INIT_39 => X"6C65685201007379730E01006E6F6973726576B5000074657365728C0000706D",
                    INIT_3A => X"3269FA020064725F6765725F633269E3020072775F6765725F633269C6010070",
                    INIT_3B => X"65735F6332690F03006361645F6C65735F6332690903006364745F6C65735F63",
                    INIT_3C => X"000064725F6765725F716164E0000072775F6765725F7161640C030062645F6C",
                    INIT_3D => X"65725F63647424050065746972775F6364742005007375746174735F636474CA",
                    INIT_3E => X"742F050064725F6765725F6364743E050072775F6765725F6364742B05006461",
                    INIT_3F => X"706D65748306007364695F7465675F706D65746105006863726165735F706D65",
                   INITP_00 => X"FFF86590000001D143088404C00410241FFFE536008278000000008F80000002",
                   INITP_01 => X"FFFDFFD0E0335F4A08F880B9C18E2E0C0E80420031A7FFFFFFFFFFC000007FFF",
                   INITP_02 => X"0004BA007FF800104020480000008800000004A5FE35FFFCFFD9BFFF7FFBFFEF",
                   INITP_03 => X"6A28A2800000000000010000114140A1400137734016F4D00570207FFF792401",
                   INITP_04 => X"200000000022220000000000000000030B56D00102C2D56DB50821020616B186",
                   INITP_05 => X"4C44025300609C000000020820814C000000009017FFF9BA00B4008040028804",
                   INITP_06 => X"555032106A2208544DA40044CB25314000444484448A62200025300888908891",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3115")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_l(31 downto 0),
                  DOPA => data_out_a_l(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_l(31 downto 0),
                  DOPB => data_out_b_l(35 downto 32), 
                   DIB => data_in_b_l(31 downto 0),
                  DIPB => data_in_b_l(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_h: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"E968006900005068080068080068006900000009287878082800005858010028",
                    INIT_05 => X"4B4A4A906848680868280028000404815859D0E8580028001000B0E818890090",
                    INIT_06 => X"28000000000000000000680800680800005008B0E8582868086B6B6A6A68284B",
                    INIT_07 => X"0808080808080808080808080828005008035088035088025088025008B0E858",
                    INIT_08 => X"000000680800000068080000000008000D0D0808080808080808080808080808",
                    INIT_09 => X"0808080808080808080808080808080808080808080808080808280000006808",
                    INIT_0A => X"08001088685000F0E05908000D0D080808080808080808080808080808080808",
                    INIT_0B => X"00680800F0E1095A68006900005800000D0D0068080068006900000068080068",
                    INIT_0C => X"896800690000508008680800F0E1095A000D0D00108968006900005080086808",
                    INIT_0D => X"69000050D0E38B038AA2A28902680800680069000000000D0D00F0E1095A0010",
                    INIT_0E => X"6E261EA0A018B8B02800109D8D00109D8D680090E890E8250D0D280010CB6800",
                    INIT_0F => X"1E0000006E1E00006E2E00006E2E286E1E000000006E1E6E2E2E28F0CF0F28B8",
                    INIT_10 => X"280000006E1E00006E2E6E2E00286E2E000000006E2E0000006E1E280000006E",
                    INIT_11 => X"A26E1E00006E2E006E2E116E1ED1A2000A2800006E2E006E1E00006E2E006E1E",
                    INIT_12 => X"6E1E00006E2E00A26B4B000A0A2E286A0000006E1E00006E2E4A006E2E0011D1",
                    INIT_13 => X"1108110828B8A00801D10102D10102D10102D101020018B8B1B1B0B02800F1A2",
                    INIT_14 => X"110828B8A00801D10102D10102D101020018B8B1B0B028B8A008010011081108",
                    INIT_15 => X"010101010101D101022800D10102D101020018B8B0B028B8A008010011081108",
                    INIT_16 => X"0D0D080808080808080808080828B8A008010011081108110828B8B1B1A00801",
                    INIT_17 => X"0A500AB0E85828D10128D101D1EA518A51D1EA520A0A500A500AB0E858280000",
                    INIT_18 => X"080808080808080828010101B608280008280008280008280000000000D10150",
                    INIT_19 => X"18A0A0A0A00928F1CFF1CF0F0F286818500AB1E8582800000D0D080808080808",
                    INIT_1A => X"05040400098018A0A0A0A00928F1C989D1B1EAD3D3D2C2000105050404000988",
                    INIT_1B => X"E90100096808680800000068086808060E28F1C989B1D3B1D3B1D2B1C2000105",
                    INIT_1C => X"A0A00091EE8E0091E9010068086808B1E901000968086808B1E90100096808B1",
                    INIT_1D => X"006808000000680800680800680800110088A0A0A0A000A2A2A2A2020088A0A0",
                    INIT_1E => X"6808006808000000680800680800680800006808680828006808006808006808",
                    INIT_1F => X"026F0F280108916A00080108916A00080108916A00080108916A000828000000",
                    INIT_20 => X"A10928A1096808481292DFCFB218480F0F2868080268921848284828F2CF0F28",
                    INIT_21 => X"680008091209120928A0080201D20202D20202D202022018A0A0A0A0A0A00128",
                    INIT_22 => X"A0A0A0A0A0A00128A10900000200680008680008006900680008680008680008",
                    INIT_23 => X"000002006800086800080068000868000868000828A008D20202000200022018",
                    INIT_24 => X"0828A0089268926848080868282808129268926848080868282808D26828A109",
                    INIT_25 => X"12F2A1020809D2682892684868280892684828926848682808926848D26828A0",
                    INIT_26 => X"08926848D2682812F2A1A00208090812F2A1A002080908D2682812F2A1020809",
                    INIT_27 => X"6848D26828A0A0A0A0A092684848486828280892684828A09268484848682828",
                    INIT_28 => X"289268484848682828A0A0A0A0A0A008926848289268484848682828A0A00892",
                    INIT_29 => X"E858280000000000D20250085008B0E8582800000228025008B0E85828000002",
                    INIT_2A => X"00000D0D000808080808080808080808080808080828025008500851885108B0",
                    INIT_2B => X"006808006808006808002800680800680800680800680800D2025008B0E85828",
                    INIT_2C => X"0200030200030200020200020200020800F202005108B0E85828006808006808",
                    INIT_2D => X"0068080028000000000000000000000000000000000005020005020004020004",
                    INIT_2E => X"B0E8582800000002005108B0E8582802000052085108B0E85828006808006808",
                    INIT_2F => X"00F202005108B0E85008B0E85008B0E85828000200020800020800F202005108",
                    INIT_30 => X"508A0200508A0200508A0250000A0200508A0200508A0200508A0250000A0208",
                    INIT_31 => X"000000000200020200020800020800F202005108B0E858280002000208000200",
                    INIT_32 => X"00508A0200508A0250000A020800F202005108B0E85008B0E85008B0E8582800",
                    INIT_33 => X"00000002000202000208000200508A0200508A0200508A0250000A0200508A02",
                    INIT_34 => X"A0A00B93E893E8A00200A00200080208000A09F20200030B5108B0E858280000",
                    INIT_35 => X"130300B3EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3",
                    INIT_36 => X"050D1828A513A5CD93ED551DCD0585A5A5A5CD050D2813020001010300050513",
                    INIT_37 => X"8870887088708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD",
                    INIT_38 => X"080808080808080808080808080808082800F3CECE00500E8E0E287088708870",
                    INIT_39 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                   INITP_00 => X"FFFE0004EAAD937E12FE27E17D6FCEFFCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"7BBB7D3382E6753BE805FA637C589BE2D8FB37EB6B13FFFFFFFFFFFAD6B53FFF",
                   INITP_02 => X"13F840273FFE7561B73B533AB3B6A33AACEDB50EBA22F7375DB57B76FBAEFBDF",
                   INITP_03 => X"DAAAAAAEB6B6EBB6DADB800813554AA54AD46556100CC1840346273FFFF36DD6",
                   INITP_04 => X"282112844B919174D2C9324A4A11A114FDBB69D4033F6FB6CA86DC0324C86E59",
                   INITP_05 => X"622274989D130276DEAAA924924A6276DB7B6DC9CFFFFE013AC27E4FC2009841",
                   INITP_06 => X"554C880014800719F25252AA14921A275291111111131113A94989D222222222",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE435")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_h(31 downto 0),
                  DOPA => data_out_a_h(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_h(31 downto 0),
                  DOPB => data_out_b_h(35 downto 32), 
                   DIB => data_in_b_h(31 downto 0),
                  DIPB => data_in_b_h(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D0049000290C0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"00020041003013005000F023F020100050000060010EB001B031031200885000",
                    INIT_09 => X"D3FFD1010002D20100020041A030D00110200002D001103A0002D1010002D201",
                    INIT_0A => X"B12BB227A0B4D004B023015250000059208D00596099D00330031301003020A8",
                    INIT_0B => X"96079506940520BAD0109003D1031108D004500000815000015208E008910210",
                    INIT_0C => X"00B7A000102C607CD001B0235000D10311F4D708D607D506D405D00450009708",
                    INIT_0D => X"5000005900530040005300500053006000530070D00110780002D00110300002",
                    INIT_0E => X"102C0710A10010010610A10010010510A10010010410A1001030607CD002B023",
                    INIT_0F => X"114C112011721165116B116311611172115411201132112D1167500000C2A000",
                    INIT_10 => X"1AF31B0011001120113A11561120116411721161116F1142116311691167116F",
                    INIT_11 => X"000200530050D001102E000200530060D001102E00020053007000B710010064",
                    INIT_12 => X"115711001120113A1172116511661166117511425000005900530040D001102E",
                    INIT_13 => X"11641172116F115711001120113A1174116E1175116F1163112011641172116F",
                    INIT_14 => X"11641172116F115711001120112011201120113A11731165117A116911731120",
                    INIT_15 => X"1020000221571101D001A0100002E15EC120B220110000641A261B0111001120",
                    INIT_16 => X"1A2F1B010059D00110290002D1010002D201000200410020D00110280002D001",
                    INIT_17 => X"0002D00110200002E18CC3401300B423D1010002D20100020041B02300020064",
                    INIT_18 => X"00641A3C1B010059217A1301D1010002D20100020041A01001301124D0011040",
                    INIT_19 => X"1301D1010002D20100020041A01001301128D00110200002E1A1C3401300B423",
                    INIT_1A => X"D1010002D20100020041003000641A4C1B010059E1C4C3401300B42300592192",
                    INIT_1B => X"D20100020041A060A1A4C65016030650152C4506450613010530D00110200002",
                    INIT_1C => X"1A01D101000221D6D1FF21D2D1004BA01A101B075000005921BA9601D1010002",
                    INIT_1D => X"DD024D003DF0400740073003700160005000005921C83B001A03005921C83B00",
                    INIT_1E => X"3DFE01E201E201E201E2DD023DFDDD025D025D015000E1E39E011E3E50007000",
                    INIT_1F => X"3DFE01E201E201E2DD023DFD01E201E2DD025D0101E201E2DD025D025000DD02",
                    INIT_20 => X"01E201E201E201E2DD025D0101E201E201E2DD023DFD500001E201E201E2DD02",
                    INIT_21 => X"500001E201E201E2DD023DFE01E201E2DD025D01DD025D0201E25000DD025D02",
                    INIT_22 => X"1580500001E201E2DD025D0201E2DD023DFE01E201E2DD025D0101E2DD023DFD",
                    INIT_23 => X"450EDD023DFE01E201E2DD025D0101E2DD025D022238DD023DFDA236440601E2",
                    INIT_24 => X"01E201E201E2DD023DFE01E201E2DD025D01950201E2DD025D0201E22230A242",
                    INIT_25 => X"DD023DFE01E201E2DD025D0101E24400D602960201E2158014005D025000D502",
                    INIT_26 => X"022F0410A27C022F040001E630FE70016330622061106000500001E2E255450E",
                    INIT_27 => X"228410022284100150007000400610000205A282022F0430A280022F0420A27E",
                    INIT_28 => X"01E630FE70016220611060005000700040061080020500532284100422841003",
                    INIT_29 => X"22A4100150007000400610000205A2A2022F0420A2A0022F0410A29E022F0400",
                    INIT_2A => X"040001E630FE70016110600050007000400610800205005322A4100322A41002",
                    INIT_2B => X"021303400252022002400252A2CB022F0400500101F2A27E022F0410A2C7022F",
                    INIT_2C => X"10800205005322CD100322CD100222CD10015000700063306220400610000205",
                    INIT_2D => X"1AD31B021100112E1172116F1172117211651120116B11631141500070004006",
                    INIT_2E => X"A340A2F1D503A550152A1434A1401430A040142C607CD003B023500000590064",
                    INIT_2F => X"1430A040142C607CD002B0235000A2DE028A5000A2DE0264A2F7D503A2401401",
                    INIT_30 => X"1002500001D81001500001D81000500000590053003000530020A2DE02AAA140",
                    INIT_31 => X"1175116711721161112011641161114250000309030C030F6D0010F3500001D8",
                    INIT_32 => X"A040142C6326D001B0235000005900641A181B03110011731174116E1165116D",
                    INIT_33 => X"30304006400640064006130F5000E3359E01E3359F011FFF1EFF5000D003301F",
                    INIT_34 => X"A3516350D513A7B0A6A0A590848000B703330B700A600950084000B712FF1052",
                    INIT_35 => X"0A600950084000B712FF002030304006400640064006130F5000E34293011201",
                    INIT_36 => X"1C005000E35C93011201636CA7B0636CA6A0636CA590636C848000B703330B70",
                    INIT_37 => X"D20F035400D01257D0011020D001103A0059005300C0D0011050D00110200D00",
                    INIT_38 => X"D20F035400D01258D0011032D001102C6399D20F035400D01256D00110316399",
                    INIT_39 => X"4006400600D023B1DC061C01005923CAD20F033A00D0D0011033D001102C6399",
                    INIT_3A => X"00C21050400640064006400600D0440644064406440604C000B7105040064006",
                    INIT_3B => X"0002D00110200002005300D0D00110430002D00110440002D001105400022371",
                    INIT_3C => X"00020059D0011034D001102C50000059D00110520002D00110520002D0011045",
                    INIT_3D => X"D00110400002D00110200002005300D0D00110430002D00110440002D0011054",
                    INIT_3E => X"036F100123F0D40200B71040036F100023EAD40100B7104050000059005300C0",
                    INIT_3F => X"0401DF091F805000036F100323FCD40800B71040036F100223F6D40400B71040",
                    INIT_40 => X"1FFF5000D10911200401D00A240731019109500090095000E4029F011FFF5000",
                    INIT_41 => X"420E1201500042061200D1091110900A2411241EBE009F016418310291091EFF",
                    INIT_42 => X"A43A040F0500A438040F04074010310F40064006400640064006400703FD5000",
                    INIT_43 => X"D101000211451333243D1332243D13315000400E100004500340A43C040F0400",
                    INIT_44 => X"113AD101000211530059D3010002D1010002113AD10100021152D10100021152",
                    INIT_45 => X"40064006400640064006400603FD5000430E13010059005304050002D1010002",
                    INIT_46 => X"1152D101000211455000400E1000A46C040F040700200407003004074010310F",
                    INIT_47 => X"0059005304050002D1010002113AD101000211530059D10100021152D1010002",
                    INIT_48 => X"249FD0012489D002900B10001000D00B500250081000A491D0015000430E1301",
                    INIT_49 => X"10015000400E1000249FD0102495D020900B10001000D00B502050801000249C",
                    INIT_4A => X"D020900B500024AAD002900BD00B5002100C24A4D002900BA4AED0015000400E",
                    INIT_4B => X"24C6E4BB420E04FC10001280A4C0D001500024B4D020900BD00B502010C024AE",
                    INIT_4C => X"E4CB420E410804DA100012801100A4D1D001500024C6E4C1420E04FC10011280",
                    INIT_4D => X"100024DCD002900BA4EAD001500024D9E4D3420E410804DA10011280110024D9",
                    INIT_4E => X"50405020100024EAD020900B5000400E24E3D002900B900B900BD00B50045002",
                    INIT_4F => X"D002900BA50DD0015000400E400E400E400E400E24F1D020900B900B900BD00B",
                    INIT_50 => X"250DD020900B50002507D002900B900B900BD00B500050024000410E100024FE",
                    INIT_51 => X"5000251AD020900B900B900BD00B5000502040064006400640064000410E1000",
                    INIT_52 => X"B023500000590053040F50000407A010112C607CD001B0235000005900530405",
                    INIT_53 => X"D003B023500000590053003000530040A53D0421A1101130A010112C607CD002",
                    INIT_54 => X"116511641120116F114E50000459A1101130A010112CA3101101A2101134607C",
                    INIT_55 => X"005900641A4B1B05005911001164116E1175116F116611201165116311691176",
                    INIT_56 => X"104E0002D001104F0002D001104E0002A5760483A010112C607CD001B0235000",
                    INIT_57 => X"0002D00110550002D001104F0002D0011046000250000059D00110450002D001",
                    INIT_58 => X"0030E5BC04830030A310112C607CD001B02350000059D00110440002D001104E",
                    INIT_59 => X"04C70030071004C70030061004C70030051004C70030041004C7003004B81133",
                    INIT_5A => X"00530090005300A0005300B00B1004C700300A1004C70030091004C700300810",
                    INIT_5B => X"0002D00110450002500000590053004000530050005300600053007000530080",
                    INIT_5C => X"01400030A4101130A310112C607CD002B02350000059D00110520002D0011052",
                    INIT_5D => X"607CD001B023500000590053001004C70030A310112C607CD001B023500004B8",
                    INIT_5E => X"B0235000005904A2003004B81144003004B811CC0030E55B04830030A310112C",
                    INIT_5F => X"0030E55B04830030A310112C607CD008A010112A607CD008A0101129607CD003",
                    INIT_60 => X"A140140104B80030A140140104B80030A140140104B8A1400030143404B81155",
                    INIT_61 => X"A140140104B80030A140140104B80030A140140104B8A1400030143004B80030",
                    INIT_62 => X"04830030A310112C607CD001B0235000005904A2003004B81144003004B80030",
                    INIT_63 => X"005300400053001004C70030041004C7003004B811BE003004B811CC0030E55B",
                    INIT_64 => X"0030A310112C607CD008A010112A607CD008A0101129607CD003B02350000059",
                    INIT_65 => X"0030A140140104B80030A140140104B8A1400030143404B811550030E55B0483",
                    INIT_66 => X"0030A140140104B80030A140140104B8A1400030143004B80030A140140104B8",
                    INIT_67 => X"00400053001004C70030041004C7003004B811BE003004B80030A140140104B8",
                    INIT_68 => X"002014001301E55B0483002006F41700A210112C607CD001B023500000590053",
                    INIT_69 => X"410E410E160026A2D10026C0D103410004DA0020410004DA0020110004B811F0",
                    INIT_6A => X"160066B2043066B2D600460006CB1600A6AFC73026B2160166A6C73026B24600",
                    INIT_6B => X"26CA0706005966C1D70007406692D341130106DD016004FC0020016026B20430",
                    INIT_6C => X"4A0E4A0E9A010A301B405000268A04A2002003B002A0070600590B300A2026CA",
                    INIT_6D => X"0A301B40310150004B0E26D64B0E9A0126DBDA00ABB03A079A010A300BA04A0E",
                    INIT_6E => X"26EA9A0141064D0726F0DA001DFEACB03A079A010A300BA04A0E4A0E4A0E9A01",
                    INIT_6F => X"1001E1001001E1001001E1001001E1001001E100104011005000ECB04C102CD0",
                    INIT_70 => X"50000059E7099D019C010053A0C01D071C071C405000E1001001E1001001E100",
                    INIT_71 => X"11751164116D1165116D11AA1100110011671172116111691174116C1175116D",
                    INIT_72 => X"11721165117611B51100110011741165117311651172118C110011001170116D",
                    INIT_73 => X"116C11651168115211011100117311791173110E11011100116E116F11691173",
                    INIT_74 => X"1102110011721177115F116711651172115F11631132116911C6110111001170",
                    INIT_75 => X"1132116911FA1102110011641172115F116711651172115F11631132116911E3",
                    INIT_76 => X"115F116311321169110911031100116311641174115F116C11651173115F1163",
                    INIT_77 => X"11651173115F116311321169110F11031100116311611164115F116C11651173",
                    INIT_78 => X"1177115F116711651172115F117111611164110C1103110011621164115F116C",
                    INIT_79 => X"1100110011641172115F116711651172115F11711161116411E0110011001172",
                    INIT_7A => X"11641174112011051100117311751174116111741173115F11631164117411CA",
                    INIT_7B => X"11651172115F11631164117411241105110011651174116911721177115F1163",
                    INIT_7C => X"110011721177115F116711651172115F116311641174112B1105110011641161",
                    INIT_7D => X"1174112F1105110011641172115F116711651172115F116311641174113E1105",
                    INIT_7E => X"116D11651174116111051100116811631172116111651173115F1170116D1165",
                    INIT_7F => X"1170116D11651174118311061100117311641169115F117411651167115F1170",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"AAAAAAA801041034A88888A2834A2AA80308AAA90D2AAD136AA228AAA0A8AAAA",
                   INITP_02 => X"A2D0AA8A0A2AA88A298B420AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAA",
                   INITP_03 => X"2A8A8A8A2AA20B4B814DA9696B760A9AA8D45522AA882D0A6AA122D0829AA848",
                   INITP_04 => X"88B4B8E38E2355AD8A890808AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA",
                   INITP_05 => X"034BAED134000D2A0AAAAAAD2A222D528A2E0B8E235B4A888B4B8E388D5B4A88",
                   INITP_06 => X"6088A2202D777768021154B5F5568021154B74280D2A0AAAAAAAAA4A28A2A238",
                   INITP_07 => X"A28C88C88C88C8A88A288A28A88A8A28A288A28A95515495535B62236088D823",
                   INITP_08 => X"AAA28A8A2893A220555A4AAA28AA8A28A0889038E3A0555A4920B5C028AC22D2",
                   INITP_09 => X"0C955C020309C02030CAD602D6032B60B60CB0830B0830C924CC0202CC020324",
                   INITP_0A => X"A28A2A8A28A2E0D2A0AAAAAAAAA800434A88E00D2AA834AAB0081554C2C02053",
                   INITP_0B => X"380D0D0D2A208380D2A2034A000D2A28A2A888888882082082082088380D2A28",
                   INITP_0C => X"22088206060602060606020E0343434A8882208380D2A2081818180818181808",
                   INITP_0D => X"666660A095D011550265D045542A0282AB4D62083358D8D95375860803880D2A",
                   INITP_0E => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD604A66",
                   INITP_0F => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D0049000290C0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"00020041003013005000F023F020100050000060010EB001B031031200885000",
                    INIT_09 => X"D3FFD1010002D20100020041A030D00110200002D001103A0002D1010002D201",
                    INIT_0A => X"B12BB227A0B4D004B023015250000059208D00596099D00330031301003020A8",
                    INIT_0B => X"96079506940520BAD0109003D1031108D004500000815000015208E008910210",
                    INIT_0C => X"00B7A000102C607CD001B0235000D10311F4D708D607D506D405D00450009708",
                    INIT_0D => X"5000005900530040005300500053006000530070D00110780002D00110300002",
                    INIT_0E => X"102C0710A10010010610A10010010510A10010010410A1001030607CD002B023",
                    INIT_0F => X"114C112011721165116B116311611172115411201132112D1167500000C2A000",
                    INIT_10 => X"1AF31B0011001120113A11561120116411721161116F1142116311691167116F",
                    INIT_11 => X"000200530050D001102E000200530060D001102E00020053007000B710010064",
                    INIT_12 => X"115711001120113A1172116511661166117511425000005900530040D001102E",
                    INIT_13 => X"11641172116F115711001120113A1174116E1175116F1163112011641172116F",
                    INIT_14 => X"11641172116F115711001120112011201120113A11731165117A116911731120",
                    INIT_15 => X"1020000221571101D001A0100002E15EC120B220110000641A261B0111001120",
                    INIT_16 => X"1A2F1B010059D00110290002D1010002D201000200410020D00110280002D001",
                    INIT_17 => X"0002D00110200002E18CC3401300B423D1010002D20100020041B02300020064",
                    INIT_18 => X"00641A3C1B010059217A1301D1010002D20100020041A01001301124D0011040",
                    INIT_19 => X"1301D1010002D20100020041A01001301128D00110200002E1A1C3401300B423",
                    INIT_1A => X"D1010002D20100020041003000641A4C1B010059E1C4C3401300B42300592192",
                    INIT_1B => X"D20100020041A060A1A4C65016030650152C4506450613010530D00110200002",
                    INIT_1C => X"1A01D101000221D6D1FF21D2D1004BA01A101B075000005921BA9601D1010002",
                    INIT_1D => X"DD024D003DF0400740073003700160005000005921C83B001A03005921C83B00",
                    INIT_1E => X"3DFE01E201E201E201E2DD023DFDDD025D025D015000E1E39E011E3E50007000",
                    INIT_1F => X"3DFE01E201E201E2DD023DFD01E201E2DD025D0101E201E2DD025D025000DD02",
                    INIT_20 => X"01E201E201E201E2DD025D0101E201E201E2DD023DFD500001E201E201E2DD02",
                    INIT_21 => X"500001E201E201E2DD023DFE01E201E2DD025D01DD025D0201E25000DD025D02",
                    INIT_22 => X"1580500001E201E2DD025D0201E2DD023DFE01E201E2DD025D0101E2DD023DFD",
                    INIT_23 => X"450EDD023DFE01E201E2DD025D0101E2DD025D022238DD023DFDA236440601E2",
                    INIT_24 => X"01E201E201E2DD023DFE01E201E2DD025D01950201E2DD025D0201E22230A242",
                    INIT_25 => X"DD023DFE01E201E2DD025D0101E24400D602960201E2158014005D025000D502",
                    INIT_26 => X"022F0410A27C022F040001E630FE70016330622061106000500001E2E255450E",
                    INIT_27 => X"228410022284100150007000400610000205A282022F0430A280022F0420A27E",
                    INIT_28 => X"01E630FE70016220611060005000700040061080020500532284100422841003",
                    INIT_29 => X"22A4100150007000400610000205A2A2022F0420A2A0022F0410A29E022F0400",
                    INIT_2A => X"040001E630FE70016110600050007000400610800205005322A4100322A41002",
                    INIT_2B => X"021303400252022002400252A2CB022F0400500101F2A27E022F0410A2C7022F",
                    INIT_2C => X"10800205005322CD100322CD100222CD10015000700063306220400610000205",
                    INIT_2D => X"1AD31B021100112E1172116F1172117211651120116B11631141500070004006",
                    INIT_2E => X"A340A2F1D503A550152A1434A1401430A040142C607CD003B023500000590064",
                    INIT_2F => X"1430A040142C607CD002B0235000A2DE028A5000A2DE0264A2F7D503A2401401",
                    INIT_30 => X"1002500001D81001500001D81000500000590053003000530020A2DE02AAA140",
                    INIT_31 => X"1175116711721161112011641161114250000309030C030F6D0010F3500001D8",
                    INIT_32 => X"A040142C6326D001B0235000005900641A181B03110011731174116E1165116D",
                    INIT_33 => X"30304006400640064006130F5000E3359E01E3359F011FFF1EFF5000D003301F",
                    INIT_34 => X"A3516350D513A7B0A6A0A590848000B703330B700A600950084000B712FF1052",
                    INIT_35 => X"0A600950084000B712FF002030304006400640064006130F5000E34293011201",
                    INIT_36 => X"1C005000E35C93011201636CA7B0636CA6A0636CA590636C848000B703330B70",
                    INIT_37 => X"D20F035400D01257D0011020D001103A0059005300C0D0011050D00110200D00",
                    INIT_38 => X"D20F035400D01258D0011032D001102C6399D20F035400D01256D00110316399",
                    INIT_39 => X"4006400600D023B1DC061C01005923CAD20F033A00D0D0011033D001102C6399",
                    INIT_3A => X"00C21050400640064006400600D0440644064406440604C000B7105040064006",
                    INIT_3B => X"0002D00110200002005300D0D00110430002D00110440002D001105400022371",
                    INIT_3C => X"00020059D0011034D001102C50000059D00110520002D00110520002D0011045",
                    INIT_3D => X"D00110400002D00110200002005300D0D00110430002D00110440002D0011054",
                    INIT_3E => X"036F100123F0D40200B71040036F100023EAD40100B7104050000059005300C0",
                    INIT_3F => X"0401DF091F805000036F100323FCD40800B71040036F100223F6D40400B71040",
                    INIT_40 => X"1FFF5000D10911200401D00A240731019109500090095000E4029F011FFF5000",
                    INIT_41 => X"420E1201500042061200D1091110900A2411241EBE009F016418310291091EFF",
                    INIT_42 => X"A43A040F0500A438040F04074010310F40064006400640064006400703FD5000",
                    INIT_43 => X"D101000211451333243D1332243D13315000400E100004500340A43C040F0400",
                    INIT_44 => X"113AD101000211530059D3010002D1010002113AD10100021152D10100021152",
                    INIT_45 => X"40064006400640064006400603FD5000430E13010059005304050002D1010002",
                    INIT_46 => X"1152D101000211455000400E1000A46C040F040700200407003004074010310F",
                    INIT_47 => X"0059005304050002D1010002113AD101000211530059D10100021152D1010002",
                    INIT_48 => X"249FD0012489D002900B10001000D00B500250081000A491D0015000430E1301",
                    INIT_49 => X"10015000400E1000249FD0102495D020900B10001000D00B502050801000249C",
                    INIT_4A => X"D020900B500024AAD002900BD00B5002100C24A4D002900BA4AED0015000400E",
                    INIT_4B => X"24C6E4BB420E04FC10001280A4C0D001500024B4D020900BD00B502010C024AE",
                    INIT_4C => X"E4CB420E410804DA100012801100A4D1D001500024C6E4C1420E04FC10011280",
                    INIT_4D => X"100024DCD002900BA4EAD001500024D9E4D3420E410804DA10011280110024D9",
                    INIT_4E => X"50405020100024EAD020900B5000400E24E3D002900B900B900BD00B50045002",
                    INIT_4F => X"D002900BA50DD0015000400E400E400E400E400E24F1D020900B900B900BD00B",
                    INIT_50 => X"250DD020900B50002507D002900B900B900BD00B500050024000410E100024FE",
                    INIT_51 => X"5000251AD020900B900B900BD00B5000502040064006400640064000410E1000",
                    INIT_52 => X"B023500000590053040F50000407A010112C607CD001B0235000005900530405",
                    INIT_53 => X"D003B023500000590053003000530040A53D0421A1101130A010112C607CD002",
                    INIT_54 => X"116511641120116F114E50000459A1101130A010112CA3101101A2101134607C",
                    INIT_55 => X"005900641A4B1B05005911001164116E1175116F116611201165116311691176",
                    INIT_56 => X"104E0002D001104F0002D001104E0002A5760483A010112C607CD001B0235000",
                    INIT_57 => X"0002D00110550002D001104F0002D0011046000250000059D00110450002D001",
                    INIT_58 => X"0030E5BC04830030A310112C607CD001B02350000059D00110440002D001104E",
                    INIT_59 => X"04C70030071004C70030061004C70030051004C70030041004C7003004B81133",
                    INIT_5A => X"00530090005300A0005300B00B1004C700300A1004C70030091004C700300810",
                    INIT_5B => X"0002D00110450002500000590053004000530050005300600053007000530080",
                    INIT_5C => X"01400030A4101130A310112C607CD002B02350000059D00110520002D0011052",
                    INIT_5D => X"607CD001B023500000590053001004C70030A310112C607CD001B023500004B8",
                    INIT_5E => X"B0235000005904A2003004B81144003004B811CC0030E55B04830030A310112C",
                    INIT_5F => X"0030E55B04830030A310112C607CD008A010112A607CD008A0101129607CD003",
                    INIT_60 => X"A140140104B80030A140140104B80030A140140104B8A1400030143404B81155",
                    INIT_61 => X"A140140104B80030A140140104B80030A140140104B8A1400030143004B80030",
                    INIT_62 => X"04830030A310112C607CD001B0235000005904A2003004B81144003004B80030",
                    INIT_63 => X"005300400053001004C70030041004C7003004B811BE003004B811CC0030E55B",
                    INIT_64 => X"0030A310112C607CD008A010112A607CD008A0101129607CD003B02350000059",
                    INIT_65 => X"0030A140140104B80030A140140104B8A1400030143404B811550030E55B0483",
                    INIT_66 => X"0030A140140104B80030A140140104B8A1400030143004B80030A140140104B8",
                    INIT_67 => X"00400053001004C70030041004C7003004B811BE003004B80030A140140104B8",
                    INIT_68 => X"002014001301E55B0483002006F41700A210112C607CD001B023500000590053",
                    INIT_69 => X"410E410E160026A2D10026C0D103410004DA0020410004DA0020110004B811F0",
                    INIT_6A => X"160066B2043066B2D600460006CB1600A6AFC73026B2160166A6C73026B24600",
                    INIT_6B => X"26CA0706005966C1D70007406692D341130106DD016004FC0020016026B20430",
                    INIT_6C => X"4A0E4A0E9A010A301B405000268A04A2002003B002A0070600590B300A2026CA",
                    INIT_6D => X"0A301B40310150004B0E26D64B0E9A0126DBDA00ABB03A079A010A300BA04A0E",
                    INIT_6E => X"26EA9A0141064D0726F0DA001DFEACB03A079A010A300BA04A0E4A0E4A0E9A01",
                    INIT_6F => X"1001E1001001E1001001E1001001E1001001E100104011005000ECB04C102CD0",
                    INIT_70 => X"50000059E7099D019C010053A0C01D071C071C405000E1001001E1001001E100",
                    INIT_71 => X"11751164116D1165116D11AA1100110011671172116111691174116C1175116D",
                    INIT_72 => X"11721165117611B51100110011741165117311651172118C110011001170116D",
                    INIT_73 => X"116C11651168115211011100117311791173110E11011100116E116F11691173",
                    INIT_74 => X"1102110011721177115F116711651172115F11631132116911C6110111001170",
                    INIT_75 => X"1132116911FA1102110011641172115F116711651172115F11631132116911E3",
                    INIT_76 => X"115F116311321169110911031100116311641174115F116C11651173115F1163",
                    INIT_77 => X"11651173115F116311321169110F11031100116311611164115F116C11651173",
                    INIT_78 => X"1177115F116711651172115F117111611164110C1103110011621164115F116C",
                    INIT_79 => X"1100110011641172115F116711651172115F11711161116411E0110011001172",
                    INIT_7A => X"11641174112011051100117311751174116111741173115F11631164117411CA",
                    INIT_7B => X"11651172115F11631164117411241105110011651174116911721177115F1163",
                    INIT_7C => X"110011721177115F116711651172115F116311641174112B1105110011641161",
                    INIT_7D => X"1174112F1105110011641172115F116711651172115F116311641174113E1105",
                    INIT_7E => X"116D11651174116111051100116811631172116111651173115F1170116D1165",
                    INIT_7F => X"1170116D11651174118311061100117311641169115F117411651167115F1170",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"AAAAAAA801041034A88888A2834A2AA80308AAA90D2AAD136AA228AAA0A8AAAA",
                   INITP_02 => X"A2D0AA8A0A2AA88A298B420AAAAAAAAAAAAAAAAAAAAAAA88A2288A220AAAAAAA",
                   INITP_03 => X"2A8A8A8A2AA20B4B814DA9696B760A9AA8D45522AA882D0A6AA122D0829AA848",
                   INITP_04 => X"88B4B8E38E2355AD8A890808AA2A0A2B62A28A362A8A2A28AA8A88A8AA8AA2AA",
                   INITP_05 => X"034BAED134000D2A0AAAAAAD2A222D528A2E0B8E235B4A888B4B8E388D5B4A88",
                   INITP_06 => X"6088A2202D777768021154B5F5568021154B74280D2A0AAAAAAAAA4A28A2A238",
                   INITP_07 => X"A28C88C88C88C8A88A288A28A88A8A28A288A28A95515495535B62236088D823",
                   INITP_08 => X"AAA28A8A2893A220555A4AAA28AA8A28A0889038E3A0555A4920B5C028AC22D2",
                   INITP_09 => X"0C955C020309C02030CAD602D6032B60B60CB0830B0830C924CC0202CC020324",
                   INITP_0A => X"A28A2A8A28A2E0D2A0AAAAAAAAA800434A88E00D2AA834AAB0081554C2C02053",
                   INITP_0B => X"380D0D0D2A208380D2A2034A000D2A28A2A888888882082082082088380D2A28",
                   INITP_0C => X"22088206060602060606020E0343434A8882208380D2A2081818180818181808",
                   INITP_0D => X"666660A095D011550265D045542A0282AB4D62083358D8D95375860803880D2A",
                   INITP_0E => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD604A66",
                   INITP_0F => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_2k_generate;
  --
  --	
  ram_4k_generate : if (C_RAM_SIZE_KWORDS = 4) generate
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      data_in_a <= "000000000000000000000000000000000000";
      --
      s6_a11_flop: FD
      port map (  D => address(11),
                  Q => pipe_a11,
                  C => clk);
      --
      s6_4k_mux0_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(0),
                I1 => data_out_a_hl(0),
                I2 => data_out_a_ll(1),
                I3 => data_out_a_hl(1),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(0),
                O6 => instruction(1));
      --
      s6_4k_mux2_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(2),
                I1 => data_out_a_hl(2),
                I2 => data_out_a_ll(3),
                I3 => data_out_a_hl(3),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(2),
                O6 => instruction(3));
      --
      s6_4k_mux4_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(4),
                I1 => data_out_a_hl(4),
                I2 => data_out_a_ll(5),
                I3 => data_out_a_hl(5),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(4),
                O6 => instruction(5));
      --
      s6_4k_mux6_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(6),
                I1 => data_out_a_hl(6),
                I2 => data_out_a_ll(7),
                I3 => data_out_a_hl(7),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(6),
                O6 => instruction(7));
      --
      s6_4k_mux8_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(32),
                I1 => data_out_a_hl(32),
                I2 => data_out_a_lh(0),
                I3 => data_out_a_hh(0),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(8),
                O6 => instruction(9));
      --
      s6_4k_mux10_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(1),
                I1 => data_out_a_hh(1),
                I2 => data_out_a_lh(2),
                I3 => data_out_a_hh(2),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(10),
                O6 => instruction(11));
      --
      s6_4k_mux12_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(3),
                I1 => data_out_a_hh(3),
                I2 => data_out_a_lh(4),
                I3 => data_out_a_hh(4),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(12),
                O6 => instruction(13));
      --
      s6_4k_mux14_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(5),
                I1 => data_out_a_hh(5),
                I2 => data_out_a_lh(6),
                I3 => data_out_a_hh(6),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(14),
                O6 => instruction(15));
      --
      s6_4k_mux16_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(7),
                I1 => data_out_a_hh(7),
                I2 => data_out_a_lh(32),
                I3 => data_out_a_hh(32),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(16),
                O6 => instruction(17));
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_ll <= "000" & data_out_b_ll(32) & "000000000000000000000000" & data_out_b_ll(7 downto 0);
        data_in_b_lh <= "000" & data_out_b_lh(32) & "000000000000000000000000" & data_out_b_lh(7 downto 0);
        data_in_b_hl <= "000" & data_out_b_hl(32) & "000000000000000000000000" & data_out_b_hl(7 downto 0);
        data_in_b_hh <= "000" & data_out_b_hh(32) & "000000000000000000000000" & data_out_b_hh(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b_l(3 downto 0) <= "0000";
        we_b_h(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
        jtag_dout <= data_out_b_lh(32) & data_out_b_lh(7 downto 0) & data_out_b_ll(32) & data_out_b_ll(7 downto 0);
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_lh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_ll <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        data_in_b_hh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_hl <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        --
        s6_4k_jtag_we_lut: LUT6_2
        generic map (INIT => X"8000000020000000")
        port map( I0 => jtag_we,
                  I1 => jtag_addr(11),
                  I2 => '1',
                  I3 => '1',
                  I4 => '1',
                  I5 => '1',
                  O5 => jtag_we_l,
                  O6 => jtag_we_h);
        --
        we_b_l(3 downto 0) <= jtag_we_l & jtag_we_l & jtag_we_l & jtag_we_l;
        we_b_h(3 downto 0) <= jtag_we_h & jtag_we_h & jtag_we_h & jtag_we_h;
        --
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
        --
        s6_4k_jtag_mux0_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(0),
                  I1 => data_out_b_hl(0),
                  I2 => data_out_b_ll(1),
                  I3 => data_out_b_hl(1),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(0),
                  O6 => jtag_dout(1));
        --
        s6_4k_jtag_mux2_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(2),
                  I1 => data_out_b_hl(2),
                  I2 => data_out_b_ll(3),
                  I3 => data_out_b_hl(3),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(2),
                  O6 => jtag_dout(3));
        --
        s6_4k_jtag_mux4_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(4),
                  I1 => data_out_b_hl(4),
                  I2 => data_out_b_ll(5),
                  I3 => data_out_b_hl(5),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(4),
                  O6 => jtag_dout(5));
        --
        s6_4k_jtag_mux6_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(6),
                  I1 => data_out_b_hl(6),
                  I2 => data_out_b_ll(7),
                  I3 => data_out_b_hl(7),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(6),
                  O6 => jtag_dout(7));
        --
        s6_4k_jtag_mux8_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(32),
                  I1 => data_out_b_hl(32),
                  I2 => data_out_b_lh(0),
                  I3 => data_out_b_hh(0),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(8),
                  O6 => jtag_dout(9));
        --
        s6_4k_jtag_mux10_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(1),
                  I1 => data_out_b_hh(1),
                  I2 => data_out_b_lh(2),
                  I3 => data_out_b_hh(2),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(10),
                  O6 => jtag_dout(11));
        --
        s6_4k_jtag_mux12_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(3),
                  I1 => data_out_b_hh(3),
                  I2 => data_out_b_lh(4),
                  I3 => data_out_b_hh(4),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(12),
                  O6 => jtag_dout(13));
        --
        s6_4k_jtag_mux14_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(5),
                  I1 => data_out_b_hh(5),
                  I2 => data_out_b_lh(6),
                  I3 => data_out_b_hh(6),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(14),
                  O6 => jtag_dout(15));
        --
        s6_4k_jtag_mux16_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(7),
                  I1 => data_out_b_hh(7),
                  I2 => data_out_b_lh(32),
                  I3 => data_out_b_hh(32),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(16),
                  O6 => jtag_dout(17));
      --
      end generate loader;
      --
      kcpsm6_rom_ll: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004000C81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"FF010201024130012002013A02010201024130000023200000600E0131128800",
                    INIT_05 => X"070605BA100303080400810052E091102B27B404235200598D599903030130A8",
                    INIT_06 => X"00595340535053605370017802013002B7002C7C01230003F408070605040008",
                    INIT_07 => X"4C2072656B6361725420322D6700C2002C1000011000011000011000307C0223",
                    INIT_08 => X"025350012E025360012E025370B70164F30000203A56206472616F426369676F",
                    INIT_09 => X"64726F5700203A746E756F632064726F5700203A72656666754200595340012E",
                    INIT_0A => X"200257010110025E202000642601002064726F5700202020203A73657A697320",
                    INIT_0B => X"020120028C40002301020102412302642F015901290201020102412001280201",
                    INIT_0C => X"010102010241103028012002A1400023643C01597A0101020102411030240140",
                    INIT_0D => X"01024160A45003502C06060130012002010201024130644C0159C44000235992",
                    INIT_0E => X"0200F007070301000059C8000359C800010102D6FFD200A010070059BA010102",
                    INIT_0F => X"FEE2E2E202FDE2E20201E2E202020002FEE2E2E2E202FD02020100E3013E0000",
                    INIT_10 => X"00E2E2E202FEE2E202010202E2000202E2E2E2E20201E2E2E202FD00E2E2E202",
                    INIT_11 => X"0E02FEE2E20201E202023802FD3606E28000E2E20202E202FEE2E20201E202FD",
                    INIT_12 => X"02FEE2E20201E2000202E28000020002E2E2E202FEE2E2020102E20202E23042",
                    INIT_13 => X"840284010000060005822F30802F207E2F107C2F00E6FE013020100000E2550E",
                    INIT_14 => X"A4010000060005A22F20A02F109E2F00E6FE0120100000000680055384048403",
                    INIT_15 => X"134052204052CB2F0001F27E2F10C72F00E6FE011000000006800553A403A402",
                    INIT_16 => X"D302002E726F727265206B6341000006800553CD03CD02CD0100003020060005",
                    INIT_17 => X"30402C7C022300DE8A00DE64F703400140F103502A344030402C7C0323005964",
                    INIT_18 => X"756772612064614200090C0F00F300D80200D80100D800005953305320DEAA40",
                    INIT_19 => X"30060606060F0035013501FFFF00031F402C26012300596418030073746E656D",
                    INIT_1A => X"605040B7FF2030060606060F00420101515013B0A09080B73370605040B7FF52",
                    INIT_1B => X"0F54D0570120013A5953C0015001200000005C01016CB06CA06C906C80B73370",
                    INIT_1C => X"0606D0B1060159CA0F3AD00133012C990F54D0580132012C990F54D056013199",
                    INIT_1D => X"0201200253D001430201440201540271C25006060606D006060606C0B7500606",
                    INIT_1E => X"01400201200253D0014302014402015402590134012C00590152020152020145",
                    INIT_1F => X"010980006F03FC08B7406F02F604B7406F01F002B7406F00EA01B740005953C0",
                    INIT_20 => X"0E0100060009100A111E0001180209FFFF000920010A0701090009000201FF00",
                    INIT_21 => X"010245333D323D31000E0050403C0F003A0F00380F07100F060606060607FD00",
                    INIT_22 => X"060606060606FD000E015953050201023A01025359010201023A010252010252",
                    INIT_23 => X"5953050201023A01025359010252010252010245000E006C0F0720073007100F",
                    INIT_24 => X"01000E009F1095200B00000B2080009C9F0189020B00000B0208009101000E01",
                    INIT_25 => X"C6BB0EFC0080C00100B4200B0B20C0AE200B00AA020B0B020CA4020BAE01000E",
                    INIT_26 => X"00DC020BEA0100D9D30E08DA018000D9CB0E08DA008000D10100C6C10EFC0180",
                    INIT_27 => X"020B0D01000E0E0E0E0EF1200B0B0B0B402000EA200B000EE3020B0B0B0B0402",
                    INIT_28 => X"001A200B0B0B0B002006060606000E000D200B0007020B0B0B0B0002000E00FE",
                    INIT_29 => X"03230059533053403D211030102C7C02230059530F0007102C7C012300595305",
                    INIT_2A => X"59644B055900646E756F6620656369766564206F4E00591030102C100110347C",
                    INIT_2B => X"02015502014F020146020059014502014E02014F02014E027683102C7C012300",
                    INIT_2C => X"C73010C73010C73010C73010C730B83330BC8330102C7C01230059014402014E",
                    INIT_2D => X"02014502005953405350536053705380539053A053B010C73010C73010C73010",
                    INIT_2E => X"7C012300595310C730102C7C012300B840301030102C7C022300590152020152",
                    INIT_2F => X"305B8330102C7C08102A7C0810297C03230059A230B84430B8CC305B8330102C",
                    INIT_30 => X"4001B8304001B8304001B8403030B8304001B8304001B8304001B8403034B855",
                    INIT_31 => X"53405310C73010C730B8BE30B8CC305B8330102C7C01230059A230B84430B830",
                    INIT_32 => X"304001B8304001B8403034B855305B8330102C7C08102A7C0810297C03230059",
                    INIT_33 => X"405310C73010C730B8BE30B8304001B8304001B8304001B8403030B8304001B8",
                    INIT_34 => X"0E0E00A200C00300DA2000DA2000B8F02000015B8320F400102C7C0123005953",
                    INIT_35 => X"CA0659C10040924101DD60FC2060B23000B230B20000CB00AF30B201A630B200",
                    INIT_36 => X"304001000ED60E01DB00B0070130A00E0E0E013040008AA220B0A006593020CA",
                    INIT_37 => X"01000100010001000100400000B010D0EA010607F000FEB0070130A00E0E0E01",
                    INIT_38 => X"75646D656DAA000067726169746C756D005909010153C0070740000001000100",
                    INIT_39 => X"6C65685201007379730E01006E6F6973726576B5000074657365728C0000706D",
                    INIT_3A => X"3269FA020064725F6765725F633269E3020072775F6765725F633269C6010070",
                    INIT_3B => X"65735F6332690F03006361645F6C65735F6332690903006364745F6C65735F63",
                    INIT_3C => X"000064725F6765725F716164E0000072775F6765725F7161640C030062645F6C",
                    INIT_3D => X"65725F63647424050065746972775F6364742005007375746174735F636474CA",
                    INIT_3E => X"742F050064725F6765725F6364743E050072775F6765725F6364742B05006461",
                    INIT_3F => X"706D65748306007364695F7465675F706D65746105006863726165735F706D65",
                   INITP_00 => X"FFF86590000001D143088404C00410241FFFE536008278000000008F80000002",
                   INITP_01 => X"FFFDFFD0E0335F4A08F880B9C18E2E0C0E80420031A7FFFFFFFFFFC000007FFF",
                   INITP_02 => X"0004BA007FF800104020480000008800000004A5FE35FFFCFFD9BFFF7FFBFFEF",
                   INITP_03 => X"6A28A2800000000000010000114140A1400137734016F4D00570207FFF792401",
                   INITP_04 => X"200000000022220000000000000000030B56D00102C2D56DB50821020616B186",
                   INITP_05 => X"4C44025300609C000000020820814C000000009017FFF9BA00B4008040028804",
                   INITP_06 => X"555032106A2208544DA40044CB25314000444484448A62200025300888908891",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3115")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_ll(31 downto 0),
                  DOPA => data_out_a_ll(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_ll(31 downto 0),
                  DOPB => data_out_b_ll(35 downto 32), 
                   DIB => data_in_b_ll(31 downto 0),
                  DIPB => data_in_b_ll(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_lh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"E968006900005068080068080068006900000009287878082800005858010028",
                    INIT_05 => X"4B4A4A906848680868280028000404815859D0E8580028001000B0E818890090",
                    INIT_06 => X"28000000000000000000680800680800005008B0E8582868086B6B6A6A68284B",
                    INIT_07 => X"0808080808080808080808080828005008035088035088025088025008B0E858",
                    INIT_08 => X"000000680800000068080000000008000D0D0808080808080808080808080808",
                    INIT_09 => X"0808080808080808080808080808080808080808080808080808280000006808",
                    INIT_0A => X"08001088685000F0E05908000D0D080808080808080808080808080808080808",
                    INIT_0B => X"00680800F0E1095A68006900005800000D0D0068080068006900000068080068",
                    INIT_0C => X"896800690000508008680800F0E1095A000D0D00108968006900005080086808",
                    INIT_0D => X"69000050D0E38B038AA2A28902680800680069000000000D0D00F0E1095A0010",
                    INIT_0E => X"6E261EA0A018B8B02800109D8D00109D8D680090E890E8250D0D280010CB6800",
                    INIT_0F => X"1E0000006E1E00006E2E00006E2E286E1E000000006E1E6E2E2E28F0CF0F28B8",
                    INIT_10 => X"280000006E1E00006E2E6E2E00286E2E000000006E2E0000006E1E280000006E",
                    INIT_11 => X"A26E1E00006E2E006E2E116E1ED1A2000A2800006E2E006E1E00006E2E006E1E",
                    INIT_12 => X"6E1E00006E2E00A26B4B000A0A2E286A0000006E1E00006E2E4A006E2E0011D1",
                    INIT_13 => X"1108110828B8A00801D10102D10102D10102D101020018B8B1B1B0B02800F1A2",
                    INIT_14 => X"110828B8A00801D10102D10102D101020018B8B1B0B028B8A008010011081108",
                    INIT_15 => X"010101010101D101022800D10102D101020018B8B0B028B8A008010011081108",
                    INIT_16 => X"0D0D080808080808080808080828B8A008010011081108110828B8B1B1A00801",
                    INIT_17 => X"0A500AB0E85828D10128D101D1EA518A51D1EA520A0A500A500AB0E858280000",
                    INIT_18 => X"080808080808080828010101B608280008280008280008280000000000D10150",
                    INIT_19 => X"18A0A0A0A00928F1CFF1CF0F0F286818500AB1E8582800000D0D080808080808",
                    INIT_1A => X"05040400098018A0A0A0A00928F1C989D1B1EAD3D3D2C2000105050404000988",
                    INIT_1B => X"E90100096808680800000068086808060E28F1C989B1D3B1D3B1D2B1C2000105",
                    INIT_1C => X"A0A00091EE8E0091E9010068086808B1E901000968086808B1E90100096808B1",
                    INIT_1D => X"006808000000680800680800680800110088A0A0A0A000A2A2A2A2020088A0A0",
                    INIT_1E => X"6808006808000000680800680800680800006808680828006808006808006808",
                    INIT_1F => X"026F0F280108916A00080108916A00080108916A00080108916A000828000000",
                    INIT_20 => X"A10928A1096808481292DFCFB218480F0F2868080268921848284828F2CF0F28",
                    INIT_21 => X"680008091209120928A0080201D20202D20202D202022018A0A0A0A0A0A00128",
                    INIT_22 => X"A0A0A0A0A0A00128A10900000200680008680008006900680008680008680008",
                    INIT_23 => X"000002006800086800080068000868000868000828A008D20202000200022018",
                    INIT_24 => X"0828A0089268926848080868282808129268926848080868282808D26828A109",
                    INIT_25 => X"12F2A1020809D2682892684868280892684828926848682808926848D26828A0",
                    INIT_26 => X"08926848D2682812F2A1A00208090812F2A1A002080908D2682812F2A1020809",
                    INIT_27 => X"6848D26828A0A0A0A0A092684848486828280892684828A09268484848682828",
                    INIT_28 => X"289268484848682828A0A0A0A0A0A008926848289268484848682828A0A00892",
                    INIT_29 => X"E858280000000000D20250085008B0E8582800000228025008B0E85828000002",
                    INIT_2A => X"00000D0D000808080808080808080808080808080828025008500851885108B0",
                    INIT_2B => X"006808006808006808002800680800680800680800680800D2025008B0E85828",
                    INIT_2C => X"0200030200030200020200020200020800F202005108B0E85828006808006808",
                    INIT_2D => X"0068080028000000000000000000000000000000000005020005020004020004",
                    INIT_2E => X"B0E8582800000002005108B0E8582802000052085108B0E85828006808006808",
                    INIT_2F => X"00F202005108B0E85008B0E85008B0E85828000200020800020800F202005108",
                    INIT_30 => X"508A0200508A0200508A0250000A0200508A0200508A0200508A0250000A0208",
                    INIT_31 => X"000000000200020200020800020800F202005108B0E858280002000208000200",
                    INIT_32 => X"00508A0200508A0250000A020800F202005108B0E85008B0E85008B0E8582800",
                    INIT_33 => X"00000002000202000208000200508A0200508A0200508A0250000A0200508A02",
                    INIT_34 => X"A0A00B93E893E8A00200A00200080208000A09F20200030B5108B0E858280000",
                    INIT_35 => X"130300B3EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3",
                    INIT_36 => X"050D1828A513A5CD93ED551DCD0585A5A5A5CD050D2813020001010300050513",
                    INIT_37 => X"8870887088708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD",
                    INIT_38 => X"080808080808080808080808080808082800F3CECE00500E8E0E287088708870",
                    INIT_39 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                   INITP_00 => X"FFFE0004EAAD937E12FE27E17D6FCEFFCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"7BBB7D3382E6753BE805FA637C589BE2D8FB37EB6B13FFFFFFFFFFFAD6B53FFF",
                   INITP_02 => X"13F840273FFE7561B73B533AB3B6A33AACEDB50EBA22F7375DB57B76FBAEFBDF",
                   INITP_03 => X"DAAAAAAEB6B6EBB6DADB800813554AA54AD46556100CC1840346273FFFF36DD6",
                   INITP_04 => X"282112844B919174D2C9324A4A11A114FDBB69D4033F6FB6CA86DC0324C86E59",
                   INITP_05 => X"622274989D130276DEAAA924924A6276DB7B6DC9CFFFFE013AC27E4FC2009841",
                   INITP_06 => X"554C880014800719F25252AA14921A275291111111131113A94989D222222222",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE435")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_lh(31 downto 0),
                  DOPA => data_out_a_lh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_lh(31 downto 0),
                  DOPB => data_out_b_lh(35 downto 32), 
                   DIB => data_in_b_lh(31 downto 0),
                  DIPB => data_in_b_lh(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      --
      kcpsm6_rom_hl: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"646165725F706D6574C7050065746972775F706D657487050064695F7465675F",
                    INIT_01 => X"00616E5F746E6972705F706D6574DD0500616E5F766E6F635F706D6574D20500",
                    INIT_02 => X"0300726463420600746E6972705F706D6574EF0500766E6F635F706D65742906",
                    INIT_03 => X"650000037400017C008DFFA0650001017410207400208DFF8000A0001007FFE4",
                    INIT_04 => X"23BB05012301932F0120BB102023000060C88800608810A0000110A00001E091",
                    INIT_05 => X"20646142009300012823001001242320A901B02F20B02010A910200820100023",
                    INIT_06 => X"005901020102412064CD0800203A726F727245005964BC0800646E616D6D6F63",
                    INIT_07 => X"016001600450E902012C060640501040280040240B002300E3200110002C0438",
                    INIT_08 => X"6F6C667265764F0C622B2320200120100020010600E9702F60E95001F801701C",
                    INIT_09 => X"0C000102000C002001203C204808410A410D0001200C60885964190959002177",
                    INIT_0A => X"000000000C00200120200120010802012002010802570220000C012001205900",
                    INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"4879C864203FE1FFF22F037B0008019824B40167FFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_01 => X"0000000000000000000000000000000000000000000DB400203FE417FF7EC004",
                   INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hl(31 downto 0),
                  DOPA => data_out_a_hl(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hl(31 downto 0),
                  DOPB => data_out_b_hl(35 downto 32), 
                   DIB => data_in_b_hl(31 downto 0),
                  DIPB => data_in_b_hl(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_hh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_01 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_02 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_03 => X"14099D8D149D8D94E894E825149D8D89B4E050F4E15894E894E825090D0D0808",
                    INIT_04 => X"7894E88858C9F4008950F4E15878082800040028000021259D8D01259D8D0404",
                    INIT_05 => X"08080808281471C88858C150C88858011489F4005094E101D4E1588901718008",
                    INIT_06 => X"2800680069000000000D0D08080808080808082800000D0D0808080808080808",
                    INIT_07 => X"CB508B51D4E3D4CB8A8BA3A30383538008528008F4E2580AD4E0887008088909",
                    INIT_08 => X"0808080808080814C404F4E878885870885848002814700050B4E38B148B7000",
                    INIT_09 => X"A008680028A00878C858F4E894E894E894E850C85814000000000D0D00080808",
                    INIT_0A => X"00000028A00878C85878C858680800680800680800D4C85828A00878C8580028",
                    INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"0A000890FE9FFCFFFE00B484C325FF13895892A3FFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_01 => X"0000000000000000000000000000000000000000124B6C93392A87CFFFE91F4B",
                   INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hh(31 downto 0),
                  DOPA => data_out_a_hh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hh(31 downto 0),
                  DOPB => data_out_b_hh(35 downto 32), 
                   DIB => data_in_b_hh(31 downto 0),
                  DIPB => data_in_b_hh(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004000C81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"FF010201024130012002013A02010201024130000023200000600E0131128800",
                    INIT_05 => X"070605BA100303080400810052E091102B27B404235200598D599903030130A8",
                    INIT_06 => X"00595340535053605370017802013002B7002C7C01230003F408070605040008",
                    INIT_07 => X"4C2072656B6361725420322D6700C2002C1000011000011000011000307C0223",
                    INIT_08 => X"025350012E025360012E025370B70164F30000203A56206472616F426369676F",
                    INIT_09 => X"64726F5700203A746E756F632064726F5700203A72656666754200595340012E",
                    INIT_0A => X"200257010110025E202000642601002064726F5700202020203A73657A697320",
                    INIT_0B => X"020120028C40002301020102412302642F015901290201020102412001280201",
                    INIT_0C => X"010102010241103028012002A1400023643C01597A0101020102411030240140",
                    INIT_0D => X"01024160A45003502C06060130012002010201024130644C0159C44000235992",
                    INIT_0E => X"0200F007070301000059C8000359C800010102D6FFD200A010070059BA010102",
                    INIT_0F => X"FEE2E2E202FDE2E20201E2E202020002FEE2E2E2E202FD02020100E3013E0000",
                    INIT_10 => X"00E2E2E202FEE2E202010202E2000202E2E2E2E20201E2E2E202FD00E2E2E202",
                    INIT_11 => X"0E02FEE2E20201E202023802FD3606E28000E2E20202E202FEE2E20201E202FD",
                    INIT_12 => X"02FEE2E20201E2000202E28000020002E2E2E202FEE2E2020102E20202E23042",
                    INIT_13 => X"840284010000060005822F30802F207E2F107C2F00E6FE013020100000E2550E",
                    INIT_14 => X"A4010000060005A22F20A02F109E2F00E6FE0120100000000680055384048403",
                    INIT_15 => X"134052204052CB2F0001F27E2F10C72F00E6FE011000000006800553A403A402",
                    INIT_16 => X"D302002E726F727265206B6341000006800553CD03CD02CD0100003020060005",
                    INIT_17 => X"30402C7C022300DE8A00DE64F703400140F103502A344030402C7C0323005964",
                    INIT_18 => X"756772612064614200090C0F00F300D80200D80100D800005953305320DEAA40",
                    INIT_19 => X"30060606060F0035013501FFFF00031F402C26012300596418030073746E656D",
                    INIT_1A => X"605040B7FF2030060606060F00420101515013B0A09080B73370605040B7FF52",
                    INIT_1B => X"0F54D0570120013A5953C0015001200000005C01016CB06CA06C906C80B73370",
                    INIT_1C => X"0606D0B1060159CA0F3AD00133012C990F54D0580132012C990F54D056013199",
                    INIT_1D => X"0201200253D001430201440201540271C25006060606D006060606C0B7500606",
                    INIT_1E => X"01400201200253D0014302014402015402590134012C00590152020152020145",
                    INIT_1F => X"010980006F03FC08B7406F02F604B7406F01F002B7406F00EA01B740005953C0",
                    INIT_20 => X"0E0100060009100A111E0001180209FFFF000920010A0701090009000201FF00",
                    INIT_21 => X"010245333D323D31000E0050403C0F003A0F00380F07100F060606060607FD00",
                    INIT_22 => X"060606060606FD000E015953050201023A01025359010201023A010252010252",
                    INIT_23 => X"5953050201023A01025359010252010252010245000E006C0F0720073007100F",
                    INIT_24 => X"01000E009F1095200B00000B2080009C9F0189020B00000B0208009101000E01",
                    INIT_25 => X"C6BB0EFC0080C00100B4200B0B20C0AE200B00AA020B0B020CA4020BAE01000E",
                    INIT_26 => X"00DC020BEA0100D9D30E08DA018000D9CB0E08DA008000D10100C6C10EFC0180",
                    INIT_27 => X"020B0D01000E0E0E0E0EF1200B0B0B0B402000EA200B000EE3020B0B0B0B0402",
                    INIT_28 => X"001A200B0B0B0B002006060606000E000D200B0007020B0B0B0B0002000E00FE",
                    INIT_29 => X"03230059533053403D211030102C7C02230059530F0007102C7C012300595305",
                    INIT_2A => X"59644B055900646E756F6620656369766564206F4E00591030102C100110347C",
                    INIT_2B => X"02015502014F020146020059014502014E02014F02014E027683102C7C012300",
                    INIT_2C => X"C73010C73010C73010C73010C730B83330BC8330102C7C01230059014402014E",
                    INIT_2D => X"02014502005953405350536053705380539053A053B010C73010C73010C73010",
                    INIT_2E => X"7C012300595310C730102C7C012300B840301030102C7C022300590152020152",
                    INIT_2F => X"305B8330102C7C08102A7C0810297C03230059A230B84430B8CC305B8330102C",
                    INIT_30 => X"4001B8304001B8304001B8403030B8304001B8304001B8304001B8403034B855",
                    INIT_31 => X"53405310C73010C730B8BE30B8CC305B8330102C7C01230059A230B84430B830",
                    INIT_32 => X"304001B8304001B8403034B855305B8330102C7C08102A7C0810297C03230059",
                    INIT_33 => X"405310C73010C730B8BE30B8304001B8304001B8304001B8403030B8304001B8",
                    INIT_34 => X"0E0E00A200C00300DA2000DA2000B8F02000015B8320F400102C7C0123005953",
                    INIT_35 => X"CA0659C10040924101DD60FC2060B23000B230B20000CB00AF30B201A630B200",
                    INIT_36 => X"304001000ED60E01DB00B0070130A00E0E0E013040008AA220B0A006593020CA",
                    INIT_37 => X"01000100010001000100400000B010D0EA010607F000FEB0070130A00E0E0E01",
                    INIT_38 => X"75646D656DAA000067726169746C756D005909010153C0070740000001000100",
                    INIT_39 => X"6C65685201007379730E01006E6F6973726576B5000074657365728C0000706D",
                    INIT_3A => X"3269FA020064725F6765725F633269E3020072775F6765725F633269C6010070",
                    INIT_3B => X"65735F6332690F03006361645F6C65735F6332690903006364745F6C65735F63",
                    INIT_3C => X"000064725F6765725F716164E0000072775F6765725F7161640C030062645F6C",
                    INIT_3D => X"65725F63647424050065746972775F6364742005007375746174735F636474CA",
                    INIT_3E => X"742F050064725F6765725F6364743E050072775F6765725F6364742B05006461",
                    INIT_3F => X"706D65748306007364695F7465675F706D65746105006863726165735F706D65",
                    INIT_40 => X"646165725F706D6574C7050065746972775F706D657487050064695F7465675F",
                    INIT_41 => X"00616E5F746E6972705F706D6574DD0500616E5F766E6F635F706D6574D20500",
                    INIT_42 => X"0300726463420600746E6972705F706D6574EF0500766E6F635F706D65742906",
                    INIT_43 => X"650000037400017C008DFFA0650001017410207400208DFF8000A0001007FFE4",
                    INIT_44 => X"23BB05012301932F0120BB102023000060C88800608810A0000110A00001E091",
                    INIT_45 => X"20646142009300012823001001242320A901B02F20B02010A910200820100023",
                    INIT_46 => X"005901020102412064CD0800203A726F727245005964BC0800646E616D6D6F63",
                    INIT_47 => X"016001600450E902012C060640501040280040240B002300E3200110002C0438",
                    INIT_48 => X"6F6C667265764F0C622B2320200120100020010600E9702F60E95001F801701C",
                    INIT_49 => X"0C000102000C002001203C204808410A410D0001200C60885964190959002177",
                    INIT_4A => X"000000000C00200120200120010802012002010802570220000C012001205900",
                    INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"FFF86590000001D143088404C00410241FFFE536008278000000008F80000002",
                   INITP_01 => X"FFFDFFD0E0335F4A08F880B9C18E2E0C0E80420031A7FFFFFFFFFFC000007FFF",
                   INITP_02 => X"0004BA007FF800104020480000008800000004A5FE35FFFCFFD9BFFF7FFBFFEF",
                   INITP_03 => X"6A28A2800000000000010000114140A1400137734016F4D00570207FFF792401",
                   INITP_04 => X"200000000022220000000000000000030B56D00102C2D56DB50821020616B186",
                   INITP_05 => X"4C44025300609C000000020820814C000000009017FFF9BA00B4008040028804",
                   INITP_06 => X"555032106A2208544DA40044CB25314000444484448A62200025300888908891",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3115",
                   INITP_08 => X"4879C864203FE1FFF22F037B0008019824B40167FFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"0000000000000000000000000000000000000000000DB400203FE417FF7EC004",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"E968006900005068080068080068006900000009287878082800005858010028",
                    INIT_05 => X"4B4A4A906848680868280028000404815859D0E8580028001000B0E818890090",
                    INIT_06 => X"28000000000000000000680800680800005008B0E8582868086B6B6A6A68284B",
                    INIT_07 => X"0808080808080808080808080828005008035088035088025088025008B0E858",
                    INIT_08 => X"000000680800000068080000000008000D0D0808080808080808080808080808",
                    INIT_09 => X"0808080808080808080808080808080808080808080808080808280000006808",
                    INIT_0A => X"08001088685000F0E05908000D0D080808080808080808080808080808080808",
                    INIT_0B => X"00680800F0E1095A68006900005800000D0D0068080068006900000068080068",
                    INIT_0C => X"896800690000508008680800F0E1095A000D0D00108968006900005080086808",
                    INIT_0D => X"69000050D0E38B038AA2A28902680800680069000000000D0D00F0E1095A0010",
                    INIT_0E => X"6E261EA0A018B8B02800109D8D00109D8D680090E890E8250D0D280010CB6800",
                    INIT_0F => X"1E0000006E1E00006E2E00006E2E286E1E000000006E1E6E2E2E28F0CF0F28B8",
                    INIT_10 => X"280000006E1E00006E2E6E2E00286E2E000000006E2E0000006E1E280000006E",
                    INIT_11 => X"A26E1E00006E2E006E2E116E1ED1A2000A2800006E2E006E1E00006E2E006E1E",
                    INIT_12 => X"6E1E00006E2E00A26B4B000A0A2E286A0000006E1E00006E2E4A006E2E0011D1",
                    INIT_13 => X"1108110828B8A00801D10102D10102D10102D101020018B8B1B1B0B02800F1A2",
                    INIT_14 => X"110828B8A00801D10102D10102D101020018B8B1B0B028B8A008010011081108",
                    INIT_15 => X"010101010101D101022800D10102D101020018B8B0B028B8A008010011081108",
                    INIT_16 => X"0D0D080808080808080808080828B8A008010011081108110828B8B1B1A00801",
                    INIT_17 => X"0A500AB0E85828D10128D101D1EA518A51D1EA520A0A500A500AB0E858280000",
                    INIT_18 => X"080808080808080828010101B608280008280008280008280000000000D10150",
                    INIT_19 => X"18A0A0A0A00928F1CFF1CF0F0F286818500AB1E8582800000D0D080808080808",
                    INIT_1A => X"05040400098018A0A0A0A00928F1C989D1B1EAD3D3D2C2000105050404000988",
                    INIT_1B => X"E90100096808680800000068086808060E28F1C989B1D3B1D3B1D2B1C2000105",
                    INIT_1C => X"A0A00091EE8E0091E9010068086808B1E901000968086808B1E90100096808B1",
                    INIT_1D => X"006808000000680800680800680800110088A0A0A0A000A2A2A2A2020088A0A0",
                    INIT_1E => X"6808006808000000680800680800680800006808680828006808006808006808",
                    INIT_1F => X"026F0F280108916A00080108916A00080108916A00080108916A000828000000",
                    INIT_20 => X"A10928A1096808481292DFCFB218480F0F2868080268921848284828F2CF0F28",
                    INIT_21 => X"680008091209120928A0080201D20202D20202D202022018A0A0A0A0A0A00128",
                    INIT_22 => X"A0A0A0A0A0A00128A10900000200680008680008006900680008680008680008",
                    INIT_23 => X"000002006800086800080068000868000868000828A008D20202000200022018",
                    INIT_24 => X"0828A0089268926848080868282808129268926848080868282808D26828A109",
                    INIT_25 => X"12F2A1020809D2682892684868280892684828926848682808926848D26828A0",
                    INIT_26 => X"08926848D2682812F2A1A00208090812F2A1A002080908D2682812F2A1020809",
                    INIT_27 => X"6848D26828A0A0A0A0A092684848486828280892684828A09268484848682828",
                    INIT_28 => X"289268484848682828A0A0A0A0A0A008926848289268484848682828A0A00892",
                    INIT_29 => X"E858280000000000D20250085008B0E8582800000228025008B0E85828000002",
                    INIT_2A => X"00000D0D000808080808080808080808080808080828025008500851885108B0",
                    INIT_2B => X"006808006808006808002800680800680800680800680800D2025008B0E85828",
                    INIT_2C => X"0200030200030200020200020200020800F202005108B0E85828006808006808",
                    INIT_2D => X"0068080028000000000000000000000000000000000005020005020004020004",
                    INIT_2E => X"B0E8582800000002005108B0E8582802000052085108B0E85828006808006808",
                    INIT_2F => X"00F202005108B0E85008B0E85008B0E85828000200020800020800F202005108",
                    INIT_30 => X"508A0200508A0200508A0250000A0200508A0200508A0200508A0250000A0208",
                    INIT_31 => X"000000000200020200020800020800F202005108B0E858280002000208000200",
                    INIT_32 => X"00508A0200508A0250000A020800F202005108B0E85008B0E85008B0E8582800",
                    INIT_33 => X"00000002000202000208000200508A0200508A0200508A0250000A0200508A02",
                    INIT_34 => X"A0A00B93E893E8A00200A00200080208000A09F20200030B5108B0E858280000",
                    INIT_35 => X"130300B3EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3",
                    INIT_36 => X"050D1828A513A5CD93ED551DCD0585A5A5A5CD050D2813020001010300050513",
                    INIT_37 => X"8870887088708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD",
                    INIT_38 => X"080808080808080808080808080808082800F3CECE00500E8E0E287088708870",
                    INIT_39 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_40 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_41 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_42 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_43 => X"14099D8D149D8D94E894E825149D8D89B4E050F4E15894E894E825090D0D0808",
                    INIT_44 => X"7894E88858C9F4008950F4E15878082800040028000021259D8D01259D8D0404",
                    INIT_45 => X"08080808281471C88858C150C88858011489F4005094E101D4E1588901718008",
                    INIT_46 => X"2800680069000000000D0D08080808080808082800000D0D0808080808080808",
                    INIT_47 => X"CB508B51D4E3D4CB8A8BA3A30383538008528008F4E2580AD4E0887008088909",
                    INIT_48 => X"0808080808080814C404F4E878885870885848002814700050B4E38B148B7000",
                    INIT_49 => X"A008680028A00878C858F4E894E894E894E850C85814000000000D0D00080808",
                    INIT_4A => X"00000028A00878C85878C858680800680800680800D4C85828A00878C8580028",
                    INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"FFFE0004EAAD937E12FE27E17D6FCEFFCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"7BBB7D3382E6753BE805FA637C589BE2D8FB37EB6B13FFFFFFFFFFFAD6B53FFF",
                   INITP_02 => X"13F840273FFE7561B73B533AB3B6A33AACEDB50EBA22F7375DB57B76FBAEFBDF",
                   INITP_03 => X"DAAAAAAEB6B6EBB6DADB800813554AA54AD46556100CC1840346273FFFF36DD6",
                   INITP_04 => X"282112844B919174D2C9324A4A11A114FDBB69D4033F6FB6CA86DC0324C86E59",
                   INITP_05 => X"622274989D130276DEAAA924924A6276DB7B6DC9CFFFFE013AC27E4FC2009841",
                   INITP_06 => X"554C880014800719F25252AA14921A275291111111131113A94989D222222222",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE435",
                   INITP_08 => X"0A000890FE9FFCFFFE00B484C325FF13895892A3FFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"0000000000000000000000000000000000000000124B6C93392A87CFFFE91F4B",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"002C2F2000020000100000040000200000010000080006000800020004000C81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"FF010201024130012002013A02010201024130000023200000600E0131128800",
                    INIT_05 => X"070605BA100303080400810052E091102B27B404235200598D599903030130A8",
                    INIT_06 => X"00595340535053605370017802013002B7002C7C01230003F408070605040008",
                    INIT_07 => X"4C2072656B6361725420322D6700C2002C1000011000011000011000307C0223",
                    INIT_08 => X"025350012E025360012E025370B70164F30000203A56206472616F426369676F",
                    INIT_09 => X"64726F5700203A746E756F632064726F5700203A72656666754200595340012E",
                    INIT_0A => X"200257010110025E202000642601002064726F5700202020203A73657A697320",
                    INIT_0B => X"020120028C40002301020102412302642F015901290201020102412001280201",
                    INIT_0C => X"010102010241103028012002A1400023643C01597A0101020102411030240140",
                    INIT_0D => X"01024160A45003502C06060130012002010201024130644C0159C44000235992",
                    INIT_0E => X"0200F007070301000059C8000359C800010102D6FFD200A010070059BA010102",
                    INIT_0F => X"FEE2E2E202FDE2E20201E2E202020002FEE2E2E2E202FD02020100E3013E0000",
                    INIT_10 => X"00E2E2E202FEE2E202010202E2000202E2E2E2E20201E2E2E202FD00E2E2E202",
                    INIT_11 => X"0E02FEE2E20201E202023802FD3606E28000E2E20202E202FEE2E20201E202FD",
                    INIT_12 => X"02FEE2E20201E2000202E28000020002E2E2E202FEE2E2020102E20202E23042",
                    INIT_13 => X"840284010000060005822F30802F207E2F107C2F00E6FE013020100000E2550E",
                    INIT_14 => X"A4010000060005A22F20A02F109E2F00E6FE0120100000000680055384048403",
                    INIT_15 => X"134052204052CB2F0001F27E2F10C72F00E6FE011000000006800553A403A402",
                    INIT_16 => X"D302002E726F727265206B6341000006800553CD03CD02CD0100003020060005",
                    INIT_17 => X"30402C7C022300DE8A00DE64F703400140F103502A344030402C7C0323005964",
                    INIT_18 => X"756772612064614200090C0F00F300D80200D80100D800005953305320DEAA40",
                    INIT_19 => X"30060606060F0035013501FFFF00031F402C26012300596418030073746E656D",
                    INIT_1A => X"605040B7FF2030060606060F00420101515013B0A09080B73370605040B7FF52",
                    INIT_1B => X"0F54D0570120013A5953C0015001200000005C01016CB06CA06C906C80B73370",
                    INIT_1C => X"0606D0B1060159CA0F3AD00133012C990F54D0580132012C990F54D056013199",
                    INIT_1D => X"0201200253D001430201440201540271C25006060606D006060606C0B7500606",
                    INIT_1E => X"01400201200253D0014302014402015402590134012C00590152020152020145",
                    INIT_1F => X"010980006F03FC08B7406F02F604B7406F01F002B7406F00EA01B740005953C0",
                    INIT_20 => X"0E0100060009100A111E0001180209FFFF000920010A0701090009000201FF00",
                    INIT_21 => X"010245333D323D31000E0050403C0F003A0F00380F07100F060606060607FD00",
                    INIT_22 => X"060606060606FD000E015953050201023A01025359010201023A010252010252",
                    INIT_23 => X"5953050201023A01025359010252010252010245000E006C0F0720073007100F",
                    INIT_24 => X"01000E009F1095200B00000B2080009C9F0189020B00000B0208009101000E01",
                    INIT_25 => X"C6BB0EFC0080C00100B4200B0B20C0AE200B00AA020B0B020CA4020BAE01000E",
                    INIT_26 => X"00DC020BEA0100D9D30E08DA018000D9CB0E08DA008000D10100C6C10EFC0180",
                    INIT_27 => X"020B0D01000E0E0E0E0EF1200B0B0B0B402000EA200B000EE3020B0B0B0B0402",
                    INIT_28 => X"001A200B0B0B0B002006060606000E000D200B0007020B0B0B0B0002000E00FE",
                    INIT_29 => X"03230059533053403D211030102C7C02230059530F0007102C7C012300595305",
                    INIT_2A => X"59644B055900646E756F6620656369766564206F4E00591030102C100110347C",
                    INIT_2B => X"02015502014F020146020059014502014E02014F02014E027683102C7C012300",
                    INIT_2C => X"C73010C73010C73010C73010C730B83330BC8330102C7C01230059014402014E",
                    INIT_2D => X"02014502005953405350536053705380539053A053B010C73010C73010C73010",
                    INIT_2E => X"7C012300595310C730102C7C012300B840301030102C7C022300590152020152",
                    INIT_2F => X"305B8330102C7C08102A7C0810297C03230059A230B84430B8CC305B8330102C",
                    INIT_30 => X"4001B8304001B8304001B8403030B8304001B8304001B8304001B8403034B855",
                    INIT_31 => X"53405310C73010C730B8BE30B8CC305B8330102C7C01230059A230B84430B830",
                    INIT_32 => X"304001B8304001B8403034B855305B8330102C7C08102A7C0810297C03230059",
                    INIT_33 => X"405310C73010C730B8BE30B8304001B8304001B8304001B8403030B8304001B8",
                    INIT_34 => X"0E0E00A200C00300DA2000DA2000B8F02000015B8320F400102C7C0123005953",
                    INIT_35 => X"CA0659C10040924101DD60FC2060B23000B230B20000CB00AF30B201A630B200",
                    INIT_36 => X"304001000ED60E01DB00B0070130A00E0E0E013040008AA220B0A006593020CA",
                    INIT_37 => X"01000100010001000100400000B010D0EA010607F000FEB0070130A00E0E0E01",
                    INIT_38 => X"75646D656DAA000067726169746C756D005909010153C0070740000001000100",
                    INIT_39 => X"6C65685201007379730E01006E6F6973726576B5000074657365728C0000706D",
                    INIT_3A => X"3269FA020064725F6765725F633269E3020072775F6765725F633269C6010070",
                    INIT_3B => X"65735F6332690F03006361645F6C65735F6332690903006364745F6C65735F63",
                    INIT_3C => X"000064725F6765725F716164E0000072775F6765725F7161640C030062645F6C",
                    INIT_3D => X"65725F63647424050065746972775F6364742005007375746174735F636474CA",
                    INIT_3E => X"742F050064725F6765725F6364743E050072775F6765725F6364742B05006461",
                    INIT_3F => X"706D65748306007364695F7465675F706D65746105006863726165735F706D65",
                    INIT_40 => X"646165725F706D6574C7050065746972775F706D657487050064695F7465675F",
                    INIT_41 => X"00616E5F746E6972705F706D6574DD0500616E5F766E6F635F706D6574D20500",
                    INIT_42 => X"0300726463420600746E6972705F706D6574EF0500766E6F635F706D65742906",
                    INIT_43 => X"650000037400017C008DFFA0650001017410207400208DFF8000A0001007FFE4",
                    INIT_44 => X"23BB05012301932F0120BB102023000060C88800608810A0000110A00001E091",
                    INIT_45 => X"20646142009300012823001001242320A901B02F20B02010A910200820100023",
                    INIT_46 => X"005901020102412064CD0800203A726F727245005964BC0800646E616D6D6F63",
                    INIT_47 => X"016001600450E902012C060640501040280040240B002300E3200110002C0438",
                    INIT_48 => X"6F6C667265764F0C622B2320200120100020010600E9702F60E95001F801701C",
                    INIT_49 => X"0C000102000C002001203C204808410A410D0001200C60885964190959002177",
                    INIT_4A => X"000000000C00200120200120010802012002010802570220000C012001205900",
                    INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"FFF86590000001D143088404C00410241FFFE536008278000000008F80000002",
                   INITP_01 => X"FFFDFFD0E0335F4A08F880B9C18E2E0C0E80420031A7FFFFFFFFFFC000007FFF",
                   INITP_02 => X"0004BA007FF800104020480000008800000004A5FE35FFFCFFD9BFFF7FFBFFEF",
                   INITP_03 => X"6A28A2800000000000010000114140A1400137734016F4D00570207FFF792401",
                   INITP_04 => X"200000000022220000000000000000030B56D00102C2D56DB50821020616B186",
                   INITP_05 => X"4C44025300609C000000020820814C000000009017FFF9BA00B4008040028804",
                   INITP_06 => X"555032106A2208544DA40044CB25314000444484448A62200025300888908891",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3115",
                   INITP_08 => X"4879C864203FE1FFF22F037B0008019824B40167FFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"0000000000000000000000000000000000000000000DB400203FE417FF7EC004",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481400",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"E968006900005068080068080068006900000009287878082800005858010028",
                    INIT_05 => X"4B4A4A906848680868280028000404815859D0E8580028001000B0E818890090",
                    INIT_06 => X"28000000000000000000680800680800005008B0E8582868086B6B6A6A68284B",
                    INIT_07 => X"0808080808080808080808080828005008035088035088025088025008B0E858",
                    INIT_08 => X"000000680800000068080000000008000D0D0808080808080808080808080808",
                    INIT_09 => X"0808080808080808080808080808080808080808080808080808280000006808",
                    INIT_0A => X"08001088685000F0E05908000D0D080808080808080808080808080808080808",
                    INIT_0B => X"00680800F0E1095A68006900005800000D0D0068080068006900000068080068",
                    INIT_0C => X"896800690000508008680800F0E1095A000D0D00108968006900005080086808",
                    INIT_0D => X"69000050D0E38B038AA2A28902680800680069000000000D0D00F0E1095A0010",
                    INIT_0E => X"6E261EA0A018B8B02800109D8D00109D8D680090E890E8250D0D280010CB6800",
                    INIT_0F => X"1E0000006E1E00006E2E00006E2E286E1E000000006E1E6E2E2E28F0CF0F28B8",
                    INIT_10 => X"280000006E1E00006E2E6E2E00286E2E000000006E2E0000006E1E280000006E",
                    INIT_11 => X"A26E1E00006E2E006E2E116E1ED1A2000A2800006E2E006E1E00006E2E006E1E",
                    INIT_12 => X"6E1E00006E2E00A26B4B000A0A2E286A0000006E1E00006E2E4A006E2E0011D1",
                    INIT_13 => X"1108110828B8A00801D10102D10102D10102D101020018B8B1B1B0B02800F1A2",
                    INIT_14 => X"110828B8A00801D10102D10102D101020018B8B1B0B028B8A008010011081108",
                    INIT_15 => X"010101010101D101022800D10102D101020018B8B0B028B8A008010011081108",
                    INIT_16 => X"0D0D080808080808080808080828B8A008010011081108110828B8B1B1A00801",
                    INIT_17 => X"0A500AB0E85828D10128D101D1EA518A51D1EA520A0A500A500AB0E858280000",
                    INIT_18 => X"080808080808080828010101B608280008280008280008280000000000D10150",
                    INIT_19 => X"18A0A0A0A00928F1CFF1CF0F0F286818500AB1E8582800000D0D080808080808",
                    INIT_1A => X"05040400098018A0A0A0A00928F1C989D1B1EAD3D3D2C2000105050404000988",
                    INIT_1B => X"E90100096808680800000068086808060E28F1C989B1D3B1D3B1D2B1C2000105",
                    INIT_1C => X"A0A00091EE8E0091E9010068086808B1E901000968086808B1E90100096808B1",
                    INIT_1D => X"006808000000680800680800680800110088A0A0A0A000A2A2A2A2020088A0A0",
                    INIT_1E => X"6808006808000000680800680800680800006808680828006808006808006808",
                    INIT_1F => X"026F0F280108916A00080108916A00080108916A00080108916A000828000000",
                    INIT_20 => X"A10928A1096808481292DFCFB218480F0F2868080268921848284828F2CF0F28",
                    INIT_21 => X"680008091209120928A0080201D20202D20202D202022018A0A0A0A0A0A00128",
                    INIT_22 => X"A0A0A0A0A0A00128A10900000200680008680008006900680008680008680008",
                    INIT_23 => X"000002006800086800080068000868000868000828A008D20202000200022018",
                    INIT_24 => X"0828A0089268926848080868282808129268926848080868282808D26828A109",
                    INIT_25 => X"12F2A1020809D2682892684868280892684828926848682808926848D26828A0",
                    INIT_26 => X"08926848D2682812F2A1A00208090812F2A1A002080908D2682812F2A1020809",
                    INIT_27 => X"6848D26828A0A0A0A0A092684848486828280892684828A09268484848682828",
                    INIT_28 => X"289268484848682828A0A0A0A0A0A008926848289268484848682828A0A00892",
                    INIT_29 => X"E858280000000000D20250085008B0E8582800000228025008B0E85828000002",
                    INIT_2A => X"00000D0D000808080808080808080808080808080828025008500851885108B0",
                    INIT_2B => X"006808006808006808002800680800680800680800680800D2025008B0E85828",
                    INIT_2C => X"0200030200030200020200020200020800F202005108B0E85828006808006808",
                    INIT_2D => X"0068080028000000000000000000000000000000000005020005020004020004",
                    INIT_2E => X"B0E8582800000002005108B0E8582802000052085108B0E85828006808006808",
                    INIT_2F => X"00F202005108B0E85008B0E85008B0E85828000200020800020800F202005108",
                    INIT_30 => X"508A0200508A0200508A0250000A0200508A0200508A0200508A0250000A0208",
                    INIT_31 => X"000000000200020200020800020800F202005108B0E858280002000208000200",
                    INIT_32 => X"00508A0200508A0250000A020800F202005108B0E85008B0E85008B0E8582800",
                    INIT_33 => X"00000002000202000208000200508A0200508A0200508A0250000A0200508A02",
                    INIT_34 => X"A0A00B93E893E8A00200A00200080208000A09F20200030B5108B0E858280000",
                    INIT_35 => X"130300B3EB03B3E989030002000013020BB302B3EBA3030BD3E3130BB3E313A3",
                    INIT_36 => X"050D1828A513A5CD93ED551DCD0585A5A5A5CD050D2813020001010300050513",
                    INIT_37 => X"8870887088708870887008082876261613CDA0A693ED0E561DCD0585A5A5A5CD",
                    INIT_38 => X"080808080808080808080808080808082800F3CECE00500E8E0E287088708870",
                    INIT_39 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_3F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_40 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_41 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_42 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_43 => X"14099D8D149D8D94E894E825149D8D89B4E050F4E15894E894E825090D0D0808",
                    INIT_44 => X"7894E88858C9F4008950F4E15878082800040028000021259D8D01259D8D0404",
                    INIT_45 => X"08080808281471C88858C150C88858011489F4005094E101D4E1588901718008",
                    INIT_46 => X"2800680069000000000D0D08080808080808082800000D0D0808080808080808",
                    INIT_47 => X"CB508B51D4E3D4CB8A8BA3A30383538008528008F4E2580AD4E0887008088909",
                    INIT_48 => X"0808080808080814C404F4E878885870885848002814700050B4E38B148B7000",
                    INIT_49 => X"A008680028A00878C858F4E894E894E894E850C85814000000000D0D00080808",
                    INIT_4A => X"00000028A00878C85878C858680800680800680800D4C85828A00878C8580028",
                    INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"FFFE0004EAAD937E12FE27E17D6FCEFFCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"7BBB7D3382E6753BE805FA637C589BE2D8FB37EB6B13FFFFFFFFFFFAD6B53FFF",
                   INITP_02 => X"13F840273FFE7561B73B533AB3B6A33AACEDB50EBA22F7375DB57B76FBAEFBDF",
                   INITP_03 => X"DAAAAAAEB6B6EBB6DADB800813554AA54AD46556100CC1840346273FFFF36DD6",
                   INITP_04 => X"282112844B919174D2C9324A4A11A114FDBB69D4033F6FB6CA86DC0324C86E59",
                   INITP_05 => X"622274989D130276DEAAA924924A6276DB7B6DC9CFFFFE013AC27E4FC2009841",
                   INITP_06 => X"554C880014800719F25252AA14921A275291111111131113A94989D222222222",
                   INITP_07 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE435",
                   INITP_08 => X"0A000890FE9FFCFFFE00B484C325FF13895892A3FFFFFFFFFFFFFFFFFFFFFFFF",
                   INITP_09 => X"0000000000000000000000000000000000000000124B6C93392A87CFFFE91F4B",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_4k_generate;	              
  --
  --
  --
  --
  -- JTAG Loader
  --
  instantiate_loader : if (C_JTAG_LOADER_ENABLE = 1) generate
  --
    jtag_loader_6_inst : jtag_loader_6
    generic map(              C_FAMILY => C_FAMILY,
                       C_NUM_PICOBLAZE => 1,
                  C_JTAG_LOADER_ENABLE => C_JTAG_LOADER_ENABLE,
                 C_BRAM_MAX_ADDR_WIDTH => BRAM_ADDRESS_WIDTH,
	                  C_ADDR_WIDTH_0 => BRAM_ADDRESS_WIDTH)
    port map( picoblaze_reset => rdl_bus,
                      jtag_en => jtag_en,
                     jtag_din => jtag_din,
                    jtag_addr => jtag_addr(BRAM_ADDRESS_WIDTH-1 downto 0),
                     jtag_clk => jtag_clk,
                      jtag_we => jtag_we,
                  jtag_dout_0 => jtag_dout,
                  jtag_dout_1 => jtag_dout, -- ports 1-7 are not used
                  jtag_dout_2 => jtag_dout, -- in a 1 device debug 
                  jtag_dout_3 => jtag_dout, -- session.  However, Synplify
                  jtag_dout_4 => jtag_dout, -- etc require all ports to
                  jtag_dout_5 => jtag_dout, -- be connected
                  jtag_dout_6 => jtag_dout,
                  jtag_dout_7 => jtag_dout);
    --  
  end generate instantiate_loader;
  --
end low_level_definition;
--
--
-------------------------------------------------------------------------------------------
--
-- JTAG Loader 
--
-------------------------------------------------------------------------------------------
--
--
-- JTAG Loader 6 - Version 6.00
-- Kris Chaplin 4 February 2010
-- Ken Chapman 15 August 2011 - Revised coding style
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--
library unisim;
use unisim.vcomponents.all;
--
entity jtag_loader_6 is
generic(              C_JTAG_LOADER_ENABLE : integer := 1;
                                  C_FAMILY : string := "V6";
                           C_NUM_PICOBLAZE : integer := 1;
                     C_BRAM_MAX_ADDR_WIDTH : integer := 10;
        C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                              C_JTAG_CHAIN : integer := 2;
                            C_ADDR_WIDTH_0 : integer := 10;
                            C_ADDR_WIDTH_1 : integer := 10;
                            C_ADDR_WIDTH_2 : integer := 10;
                            C_ADDR_WIDTH_3 : integer := 10;
                            C_ADDR_WIDTH_4 : integer := 10;
                            C_ADDR_WIDTH_5 : integer := 10;
                            C_ADDR_WIDTH_6 : integer := 10;
                            C_ADDR_WIDTH_7 : integer := 10);
port(   picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
               jtag_din : out std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
              jtag_addr : out std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0) := (others => '0');
               jtag_clk : out std_logic := '0';
                jtag_we : out std_logic := '0';
            jtag_dout_0 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_1 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_2 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_3 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_4 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_5 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_6 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_7 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end jtag_loader_6;
--
architecture Behavioral of jtag_loader_6 is
  --
  signal num_picoblaze       : std_logic_vector(2 downto 0);
  signal picoblaze_instruction_data_width : std_logic_vector(4 downto 0);
  --
  signal drck                : std_logic;
  signal shift_clk           : std_logic;
  signal shift_din           : std_logic;
  signal shift_dout          : std_logic;
  signal shift               : std_logic;
  signal capture             : std_logic;
  --
  signal control_reg_ce      : std_logic;
  signal bram_ce             : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal bus_zero            : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  signal jtag_en_int         : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal jtag_en_expanded    : std_logic_vector(7 downto 0) := (others => '0');
  signal jtag_addr_int       : std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
  signal jtag_din_int        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal control_din         : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout_int    : std_logic_vector(7 downto 0):= (others => '0');
  signal bram_dout_int       : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
  signal jtag_we_int         : std_logic;
  signal jtag_clk_int        : std_logic;
  signal bram_ce_valid       : std_logic;
  signal din_load            : std_logic;
  --
  signal jtag_dout_0_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_1_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_2_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_3_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_4_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_5_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_6_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_7_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal picoblaze_reset_int : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  --        
begin
  bus_zero <= (others => '0');
  --
  jtag_loader_gen: if (C_JTAG_LOADER_ENABLE = 1) generate
    --
    -- Insert BSCAN primitive for target device architecture.
    --
    BSCAN_SPARTAN6_gen: if (C_FAMILY="S6") generate
    begin
      BSCAN_BLOCK_inst : BSCAN_SPARTAN6
      generic map ( JTAG_CHAIN => C_JTAG_CHAIN)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_SPARTAN6_gen;   
    --
    BSCAN_VIRTEX6_gen: if (C_FAMILY="V6") generate
    begin
      BSCAN_BLOCK_inst: BSCAN_VIRTEX6
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => FALSE)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_VIRTEX6_gen;   
    --
    BSCAN_7SERIES_gen: if (C_FAMILY="7S") generate
    begin
      BSCAN_BLOCK_inst: BSCANE2
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => "FALSE")
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_7SERIES_gen;   
    --
    --
    -- Insert clock buffer to ensure reliable shift operations.
    --
    upload_clock: BUFG
    port map( I => drck,
              O => shift_clk);
    --        
    --        
    --  Shift Register      
    --        
    --
    control_reg_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk = '1' then
        if (shift = '1') then
          control_reg_ce <= shift_din;
        end if;
      end if;
    end process control_reg_ce_shift;
    --        
    bram_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          if(C_NUM_PICOBLAZE > 1) then
            for i in 0 to C_NUM_PICOBLAZE-2 loop
              bram_ce(i+1) <= bram_ce(i);
            end loop;
          end if;
          bram_ce(0) <= control_reg_ce;
        end if;
      end if;
    end process bram_ce_shift;
    --        
    bram_we_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          jtag_we_int <= bram_ce(C_NUM_PICOBLAZE-1);
        end if;
      end if;
    end process bram_we_shift;
    --        
    bram_a_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          for i in 0 to C_BRAM_MAX_ADDR_WIDTH-2 loop
            jtag_addr_int(i+1) <= jtag_addr_int(i);
          end loop;
          jtag_addr_int(0) <= jtag_we_int;
        end if;
      end if;
    end process bram_a_shift;
    --        
    bram_d_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (din_load = '1') then
          jtag_din_int <= bram_dout_int;
         elsif (shift = '1') then
          for i in 0 to C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-2 loop
            jtag_din_int(i+1) <= jtag_din_int(i);
          end loop;
          jtag_din_int(0) <= jtag_addr_int(C_BRAM_MAX_ADDR_WIDTH-1);
        end if;
      end if;
    end process bram_d_shift;
    --
    shift_dout <= jtag_din_int(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1);
    --
    --
    din_load_select:process (bram_ce, din_load, capture, bus_zero, control_reg_ce) 
    begin
      if ( bram_ce = bus_zero ) then
        din_load <= capture and control_reg_ce;
       else
        din_load <= capture;
      end if;
    end process din_load_select;
    --
    --
    -- Control Registers 
    --
    num_picoblaze <= conv_std_logic_vector(C_NUM_PICOBLAZE-1,3);
    picoblaze_instruction_data_width <= conv_std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1,5);
    --	
    control_registers: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '0') and (control_reg_ce = '1') then
          case (jtag_addr_int(3 downto 0)) is 
            when "0000" => -- 0 = version - returns (7 downto 4) illustrating number of PB
                           --               and (3 downto 0) picoblaze instruction data width
                           control_dout_int <= num_picoblaze & picoblaze_instruction_data_width;
            when "0001" => -- 1 = PicoBlaze 0 reset / status
                           if (C_NUM_PICOBLAZE >= 1) then 
                            control_dout_int <= picoblaze_reset_int(0) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_0-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0010" => -- 2 = PicoBlaze 1 reset / status
                           if (C_NUM_PICOBLAZE >= 2) then 
                             control_dout_int <= picoblaze_reset_int(1) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_1-1,5) );
                            else 
                             control_dout_int <= (others => '0');
                           end if;
            when "0011" => -- 3 = PicoBlaze 2 reset / status
                           if (C_NUM_PICOBLAZE >= 3) then 
                            control_dout_int <= picoblaze_reset_int(2) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_2-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0100" => -- 4 = PicoBlaze 3 reset / status
                           if (C_NUM_PICOBLAZE >= 4) then 
                            control_dout_int <= picoblaze_reset_int(3) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_3-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0101" => -- 5 = PicoBlaze 4 reset / status
                           if (C_NUM_PICOBLAZE >= 5) then 
                            control_dout_int <= picoblaze_reset_int(4) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_4-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0110" => -- 6 = PicoBlaze 5 reset / status
                           if (C_NUM_PICOBLAZE >= 6) then 
                            control_dout_int <= picoblaze_reset_int(5) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_5-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0111" => -- 7 = PicoBlaze 6 reset / status
                           if (C_NUM_PICOBLAZE >= 7) then 
                            control_dout_int <= picoblaze_reset_int(6) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_6-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1000" => -- 8 = PicoBlaze 7 reset / status
                           if (C_NUM_PICOBLAZE >= 8) then 
                            control_dout_int <= picoblaze_reset_int(7) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_7-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1111" => control_dout_int <= conv_std_logic_vector(C_BRAM_MAX_ADDR_WIDTH -1,8);
            when others => control_dout_int <= (others => '1');
          end case;
        else 
          control_dout_int <= (others => '0');
        end if;
      end if;
    end process control_registers;
    -- 
    control_dout(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-8) <= control_dout_int;
    --
    pb_reset: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '1') and (control_reg_ce = '1') then
          picoblaze_reset_int(C_NUM_PICOBLAZE-1 downto 0) <= control_din(C_NUM_PICOBLAZE-1 downto 0);
        end if;
      end if;
    end process pb_reset;    
    --
    --
    -- Assignments 
    --
    control_dout (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-9 downto 0) <= (others => '0') when (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH > 8);
    --
    -- Qualify the blockram CS signal with bscan select output
    jtag_en_int <= bram_ce when bram_ce_valid = '1' else (others => '0');
    --      
    jtag_en_expanded(C_NUM_PICOBLAZE-1 downto 0) <= jtag_en_int;
    jtag_en_expanded(7 downto C_NUM_PICOBLAZE) <= (others => '0') when (C_NUM_PICOBLAZE < 8);
    --        
    bram_dout_int <= control_dout or jtag_dout_0_masked or jtag_dout_1_masked or jtag_dout_2_masked or jtag_dout_3_masked or jtag_dout_4_masked or jtag_dout_5_masked or jtag_dout_6_masked or jtag_dout_7_masked;
    --
    control_din <= jtag_din_int;
    --        
    jtag_dout_0_masked <= jtag_dout_0 when jtag_en_expanded(0) = '1' else (others => '0');
    jtag_dout_1_masked <= jtag_dout_1 when jtag_en_expanded(1) = '1' else (others => '0');
    jtag_dout_2_masked <= jtag_dout_2 when jtag_en_expanded(2) = '1' else (others => '0');
    jtag_dout_3_masked <= jtag_dout_3 when jtag_en_expanded(3) = '1' else (others => '0');
    jtag_dout_4_masked <= jtag_dout_4 when jtag_en_expanded(4) = '1' else (others => '0');
    jtag_dout_5_masked <= jtag_dout_5 when jtag_en_expanded(5) = '1' else (others => '0');
    jtag_dout_6_masked <= jtag_dout_6 when jtag_en_expanded(6) = '1' else (others => '0');
    jtag_dout_7_masked <= jtag_dout_7 when jtag_en_expanded(7) = '1' else (others => '0');
    --
    jtag_en <= jtag_en_int;
    jtag_din <= jtag_din_int;
    jtag_addr <= jtag_addr_int;
    jtag_clk <= jtag_clk_int;
    jtag_we <= jtag_we_int;
    picoblaze_reset <= picoblaze_reset_int;
    --        
  end generate jtag_loader_gen;
--
end Behavioral;
--
--
------------------------------------------------------------------------------------
--
-- END OF FILE cli.vhd
--
------------------------------------------------------------------------------------
