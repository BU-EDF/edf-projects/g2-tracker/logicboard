;;; ============================================================================
;;; Register map (x means use explicity, y implicitly)
;;; ============================================================================
;;; | name (A)             | s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7 | s8 | s9 | sA | sB | sC | sD | sE | sF |
;;; |----------------------|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
;;; | name (B)             | s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7 | s8 | s9 | sA | sB | sC | sD | sE | sF |
;;; | i2c_half_clock_sleep |    |    |    |    |    |    |    |    |    |    |    |    |    |    | x  |    |
;;; |----------------------|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
;;; | i2c_send_start       |    |    |    |    |    |    |    |    |    |    |    |    |    | x  | y  |    |
;;; | i2c_send_stop        |    |    |    |    |    |    |    |    |    |    |    |    |    | x  | y  |    |
;;; |----------------------|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
;;; | i2c_send_byte        |    |    |    |    | x  | x  |    |    |    |    |    |    |    | x  | y  |    |
;;; |----------------------|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
;;; | i2c_reg_write_word   | x  | x  | x  | x  | y  | y  |    |    |    |    |    |    |    | y  | y  |    |
	
;;; ============================================================================
;;; i2c_write
;;; ============================================================================
CONSTANT I2C_PORT_OUTPUT, 	02	; bit 0 SCL, bit 1 SDA
CONSTANT I2C_PORT_INPUT, 	02
CONSTANT I2C_SDA,        	02
CONSTANT I2C_SDA_0,		FD
CONSTANT I2C_SDA_1,      	02
CONSTANT I2C_SCL,        	01
CONSTANT I2C_SCL_0,		FE
CONSTANT I2C_SCL_1,      	01
CONSTANT I2C_SEL_TDC,		00
CONSTANT I2C_SEL_DB,		01
CONSTANT I2C_SEL_DAC,		02

;; ============================================================================
;; select i2c bus
;; input:
;;  	s0 bus to use
;; ============================================================================
i2c_select_bus:
	STAR s0, s0		;move s0A to s0B
	REGBANK B		; switch to the B bank
	AND s0, 03		; make sure only addr bits are set
	SL1 s0			; shift the address over two bits
	SL1 s0			; to be in the correct place
				; make sure bits 1:0 are 11
	
	AND sD, F0		; clear the address and i2c bits in sD
	OR sD, s0
	OUTPUT sD, I2C_PORT_OUTPUT
	REGBANK A
	RETURN	
	
	
;;; ============================================================================
;;; i2c_half_clock_sleep (2.5us)
;;; ============================================================================
	;; 125 instructions - call
i2c_half_clock_sleep:	
	LOAD sE, 3e		; load count down
i2c_half_clock_sleep_loop:	
	SUB sE, 01		; subtract 1
	JUMP NC, i2c_half_clock_sleep_loop
	RETURN

;;; ============================================================================
;;; i2c_send_start
;;; this always starts with the SDA and SCL high
;;; 
;;; ============================================================================
i2c_send_start:
	OR sd, I2C_SCL_1		; change clock to a 1
	OR sd, I2C_SDA_1		; change SDA to a 1
	OUTPUT sd, I2C_PORT_OUTPUT
	AND sd, I2C_SDA_0 		; change SDA to a 0
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	AND sd, I2C_SCL_0 		;change SCL to a 0
	OUTPUT sd, I2C_PORT_OUTPUT	
	RETURN
;;; ============================================================================
;;; i2c_send_restart
;;; ============================================================================
i2c_send_restart:
	OR sd, I2C_SDA_1		; change SDA to a 1
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep

	OR sd, I2C_SCL_1		; change SCL to a 1
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	
	AND sd, I2C_SDA_0		; change SDA to a 0
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	
	AND sd, I2C_SCL_0		; change SCL to a 0
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep	

	RETURN
;;; ============================================================================
;;; i2c_send_stop
;;; ============================================================================
i2c_send_stop:
	AND sd, I2C_SDA_0
	OUTPUT sd, I2C_PORT_OUTPUT	
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	OR sd, I2C_SCL_1		; change clock to a 1
	OUTPUT sd, I2C_PORT_OUTPUT	
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	OR sd, I2C_SDA_1		; change SDA to a 1
	OUTPUT sd, I2C_PORT_OUTPUT	
	
	RETURN


;;; ============================================================================
;;; i2c_read_no_ack
;;; ============================================================================
i2c_read_no_ack:
	CALL i2c_half_clock_sleep
	OR sd, I2C_SDA_1		; set to 'Z' for no ack
	OUTPUT sd, I2C_PORT_OUTPUT
	
	OR sd, I2C_SCL_1		; SCL set to a 1
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	
	AND sd, I2C_SCL_0		;SCL set to a 0
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep

	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	RETURN

;;; ============================================================================
;;; i2c_read_ack
;;; ============================================================================
i2c_read_ack:
;	CALL i2c_half_clock_sleep 
	AND sd, I2C_SDA_0		; set to '0' for ack
	OUTPUT sd, I2C_PORT_OUTPUT

	CALL i2c_half_clock_sleep
		
	OR sd, I2C_SCL_1		; SCL set to a 1
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep

	AND sd, I2C_SCL_0		;SCL set to a 0
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep

	OR sd, I2C_SDA_1
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	RETURN


;;; ============================================================================
;;; i2c_send_byte
;;; input byte in s4
;;; ============================================================================
i2c_send_byte:
	;; start a countdown for the 8 bits
	LOAD s5, 80
i2c_send_byte_loop_start:
	;; sleep for next bit
	CALL i2c_half_clock_sleep
	SL0 s4			;shift the MSB into C
	JUMP C, i2c_send_byte_loop_send_1 ; test if bit is 1/0

	;; write a 0
	AND sd, I2C_SDA_0		; set I2C_SDA bit to a 0
	OUTPUT sd, I2C_PORT_OUTPUT
	JUMP i2c_send_byte_loop_clk
	
i2c_send_byte_loop_send_1:
	;; write a 1 (Z)
	OR sd, I2C_SDA_1		; set I2C_SDA bit to a 1
	OUTPUT sd, I2C_PORT_OUTPUT

i2c_send_byte_loop_clk:
	;; update clock
	CALL i2c_half_clock_sleep
	OR sd, I2C_SCL_1		; change clock to a 1
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	AND sd, I2C_SCL_0		; change clock to a 0
	OUTPUT sd, I2C_PORT_OUTPUT	

	;; shift counter and check if the carry bit is set
	SR0 s5
	JUMP C, i2c_send_byte_loop_end
	JUMP i2c_send_byte_loop_start
i2c_send_byte_loop_end:
	
	CALL i2c_half_clock_sleep
	OR sd, I2C_SDA_1		; set I2C_SDA bit to a 1(Z) slave control
	OUTPUT sd, I2C_PORT_OUTPUT

	CALL i2c_half_clock_sleep
	;; ack check
	INPUT s5, I2C_PORT_INPUT
	;; finish clocks
	OR sd, I2C_SCL_1		; SCL set to a 1
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	AND sd, I2C_SCL_0		;SCL set to a 0
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep

	;; set C to ack bit C=1 => not acknowledged	
	TEST s5, I2C_SDA	
	RETURN

;;; ============================================================================
;;; i2c_read_byte
;;; output byte in s4
;;; ============================================================================
i2c_read_byte:	
	;; Set sda to 'Z'
	OR sd, I2C_SDA_1		
	;; zero our read byte
	LOAD s4, 00	
	;; start a countdown for the 8 bits
	LOAD s5, 80
i2c_read_byte_loop_start:
	;; sleep for next bit
	CALL i2c_half_clock_sleep
	INPUT s6, I2C_PORT_INPUT ; Read in the SDA state
	TEST s6, I2C_SDA	 ; test for 1/0 => C
	SLA s4			 ; shift in C

	;; update clock
	CALL i2c_half_clock_sleep
	OR sd, I2C_SCL_1		; change clock to a 1
	OUTPUT sd, I2C_PORT_OUTPUT
	CALL i2c_half_clock_sleep
	CALL i2c_half_clock_sleep
	AND sd, I2C_SCL_0		; change clock to a 0
	OUTPUT sd, I2C_PORT_OUTPUT	

	;; shift counter and check if the carry bit is set
	SR0 s5
	JUMP NC, i2c_read_byte_loop_start
	CALL i2c_half_clock_sleep
	;; We are done, return
	RETURN
	
	

;;; ============================================================================
;;; i2c_reg_write_word
;;; input:
;;;     s0 address byte (7:1) this funtion will force the r/w bit
;;; 	s1 register byte
;;; 	s2 data byte MSB
;;; 	s3 data byte LSB
;;; output:
;;; 	C = 1 on error
;;; ============================================================================	
i2c_reg_write_word:
	;; move data and address to 
	STAR s0, s0
	STAR s1, s1
	STAR s2, s2
	STAR s3, s3	
	REGBANK B
	;; update address for writing
	AND s0, FE
	;; start i2c sequence
	CALL i2c_send_start

	;; broadcast address (w)
	LOAD s4, s0
	CALL i2c_send_byte
	;; check for ack
	JUMP C, i2c_reg_write_word_error1

	;; send first data byte (register address)
	LOAD s4, s1
	CALL i2c_send_byte
	;; check for ack
	JUMP C, i2c_reg_write_word_error2

	;; send second data byte (data MSB)
	LOAD s4, s2
	CALL i2c_send_byte
	;; check for ack
	JUMP C, i2c_reg_write_word_error3
	
	;; send third data byte (data LSB)
	LOAD s4, s3
	CALL i2c_send_byte
	;; check for ack
	JUMP C, i2c_reg_write_word_error4

	CALL i2c_send_stop
	;; make sure C is 0 for no error
	LOAD s0,00
	SL0 s0
	REGBANK A
	RETURN
		
i2c_reg_write_word_error1:
	LOAD s0,01
	JUMP i2c_reg_write_word_error
i2c_reg_write_word_error2:
	LOAD s0,02
	JUMP i2c_reg_write_word_error
i2c_reg_write_word_error3:
	LOAD s0,03
	JUMP i2c_reg_write_word_error
i2c_reg_write_word_error4:
	LOAD s0,04
	JUMP i2c_reg_write_word_error
i2c_reg_write_word_error:
	;; print error num
	CALL util_print_hex_byte
	;; stop the operation because of an error
	CALL i2c_send_stop	
	;; make sure C is 1 for error
	LOAD s0,80	
	SL0 s0
	REGBANK A
	RETURN

;;; ============================================================================
;;; i2c_reg_write_byte
;;; input:
;;;     s0 address byte (7:1) this funtion will force the r/w bit
;;; 	s1 register byte
;;; 	s2 data byte
;;; output:
;;; 	C = 1 on error
;;; ============================================================================	
i2c_reg_write_byte:
	;; move data and address to 
	STAR s0, s0
	STAR s1, s1
	STAR s2, s2
	REGBANK B
	;; update address for writing
	AND s0, FE
	;; start i2c sequence
	CALL i2c_send_start

	;; broadcast address (w)
	LOAD s4, s0
	CALL i2c_send_byte
	;; check for ack
	JUMP C, i2c_reg_write_byte_error1

	;; send first data byte (register address)
	LOAD s4, s1
	CALL i2c_send_byte
	;; check for ack
	JUMP C, i2c_reg_write_byte_error2

	;; send second data byte (data MSB)
	LOAD s4, s2
	CALL i2c_send_byte
	;; check for ack
	JUMP C, i2c_reg_write_byte_error3
	
	CALL i2c_send_stop
	;; make sure C is 0 for no error
	LOAD s0,00
	SL0 s0
	REGBANK A
	RETURN
i2c_reg_write_byte_error1:
	LOAD s0,01
	JUMP i2c_reg_write_byte_error		
i2c_reg_write_byte_error2:
	LOAD s0,02
	JUMP i2c_reg_write_byte_error		
i2c_reg_write_byte_error3:
	LOAD s0,03
	JUMP i2c_reg_write_byte_error		
i2c_reg_write_byte_error:
	;; print error number
	CALL util_print_hex_byte
	;; stop the operation because of an error
	CALL i2c_send_stop
	;; make sure C is 1 for error
	LOAD s0,80	
	SL0 s0
	REGBANK A
	RETURN

	
	
;;; ============================================================================
;;; i2c_reg_read_word
;;; input:
;;;     s0 address byte (7:1) this funtion will force the r/w bit
;;; 	s1 register byte
;;; output:
;;; 	s2 data byte MSB
;;; 	s3 data byte LSB
;;; 	C = 1 on error
;;; ============================================================================
i2c_reg_read_word:
	;; move data and address to 
	STAR s0, s0
	STAR s1, s1
	REGBANK B
	
	;; update address for writing
	AND s0, FE
	;; start i2c sequence
	CALL i2c_send_start

	;; broadcast address (w)
	LOAD s4, s0
	CALL i2c_send_byte
	;; check for ack
	JUMP C, i2c_reg_read_word_error1

	;; send first data byte (register address)
	LOAD s4, s1
	CALL i2c_send_byte
	;; check for ack
	JUMP C, i2c_reg_write_word_error2
	
	;; restart for reads
	CALL i2c_send_restart
	
	;; update address for reading
	OR s0, 01
	;; broadcast address for reading
	LOAD s4, s0
	CALL i2c_send_byte
	JUMP C, i2c_reg_read_word_error3
	
	;; read MSB
	CALL i2c_read_byte
	LOAD s2, s4		;save MSB
	CALL i2c_read_ack

	;; read LSB
	CALL i2c_read_byte
	LOAD s3, s4		;save LSB
	CALL i2c_read_no_ack

	CALL i2c_send_stop
	;; make sure C is 0 for no error
	LOAD s0,00
	SL0 s0
	STAR s2,s2		;transfer return regs
	STAR s3,s3
	REGBANK A
	RETURN

i2c_reg_read_word_error1:
	LOAD s0,01
	JUMP i2c_reg_read_word_error
i2c_reg_read_word_error2:
	LOAD s0,02
	JUMP i2c_reg_read_word_error			
i2c_reg_read_word_error3:
	LOAD s0,03
	JUMP i2c_reg_read_word_error			
i2c_reg_read_word_error:	
	;; print error number
	CALL util_print_hex_byte
	;; stop the operation because of an error
	CALL i2c_send_stop
	;; make sure C is 1 for error
	LOAD s0,80	
	SL0 s0
	REGBANK A
	RETURN
	
