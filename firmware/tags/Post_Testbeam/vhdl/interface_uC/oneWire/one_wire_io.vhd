library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

entity one_wire_io is
  
  port (
    clk        : in  std_logic;
    reset      : in  std_logic;
    clk_en     : in  std_logic;
    wire_in    : in  std_logic;
    wire_out   : out std_logic;   
    wire_T     : out std_logic;
    
    bit_in     : out std_logic;
    bit_out    : in  std_logic;

    go         : in  std_logic;
    bit_mode   : in  std_logic_vector(1 downto 0); 
    done       : out std_logic);

end entity one_wire_io;

architecture Behaviour of one_wire_io is

  -- constants for timing
  constant rw_start_end_count : unsigned(19 downto 0) := x"00002";
  constant rw_read_count      : unsigned(19 downto 0) := x"0000F";
  constant rw_update_count    : unsigned(19 downto 0) := x"0003F";
  constant rw_end_count       : unsigned(19 downto 0) := x"00040";
  constant rw_idle_count      : unsigned(19 downto 0) := x"00041";

  constant init_start_end_count : unsigned(19 downto 0) := x"00200";
  constant init_read_count      : unsigned(19 downto 0) := x"00240";
  constant init_update_count    : unsigned(19 downto 0) := x"00241";
  constant init_end_count       : unsigned(19 downto 0) := x"00400";
  constant init_idle_count      : unsigned(19 downto 0) := x"00401";

  constant pwr_update_count    : unsigned(19 downto 0) := x"AFFFD";
  constant pwr_end_count       : unsigned(19 downto 0) := x"AFFFE";
  constant pwr_idle_count      : unsigned(19 downto 0) := x"AFFFF";

  constant start_count     : unsigned(19 downto 0) := x"00000";


  -- signals
  signal timing_counter : unsigned(19 downto 0) := rw_idle_count;

  signal current_mode : std_logic_vector(1 downto 0) := "00";
  -- read/write signals
  signal rw_wire_out : std_logic;
  signal rw_bit_in : std_logic;
  signal rw_wire_T : std_logic;
  signal rw_done : std_logic;
  -- init signals
  signal init_wire_out : std_logic;
  signal init_bit_in : std_logic;
  signal init_wire_T : std_logic;
  signal init_done : std_logic;
  -- pwr signals
  signal pwr_wire_out : std_logic;
  signal pwr_wire_T : std_logic;
  signal pwr_done : std_logic;

  signal write_bit : std_logic;
  
begin  -- architecture Behaviour

  -- handle starting a new onewire sequence and the overall state counters
  io_start: process (clk, reset) is
  begin  -- process io_start
    if reset = '1' then                 -- asynchronous reset (active high)
      current_mode <= "00";
      timing_counter <= rw_idle_count;
      done <= '1';
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- look for new command
      if go = '1' then
        write_bit <= bit_out; -- cache  the write bit
        current_mode <= bit_mode;
        timing_counter <= start_count;
        done <= '0';
      elsif clk_en = '1' then        
        -- increment timer unless we are in the idle state
        timing_counter <= timing_counter + 1;
        if (current_mode(1) = '0' and timing_counter = rw_idle_count)       or
          (current_mode = "10" and timing_counter = init_idle_count)        or
          (current_mode = "11" and timing_counter = pwr_idle_count)        then
          timing_counter <= timing_counter; -- stay at idle time
          done <= '1';
        end if;
      end if;
    end if;
  end process io_start;


  io_multiplex: process (clk, reset) is
  begin  -- process io_multiplex
    if reset = '1' then                 -- asynchronous reset (active high)
      wire_out <= '1';
      wire_T <= '1';
--    done <= '1';
    elsif clk'event and clk = '1' then  -- rising clock edge
      case current_mode is
        when "00" =>
          wire_out <= rw_wire_out;
          wire_T <= rw_wire_T;
--          done <= rw_done;
          bit_in <= rw_bit_in;
        when "01" =>
          wire_out <= rw_wire_out;
          wire_T <= rw_wire_T;
--        done <= rw_done;
          bit_in <= rw_bit_in;
        when "10" =>
          wire_out <= init_wire_out;
          wire_T <= init_wire_T;
--        done <= init_done;
          bit_in <= init_bit_in;
        when "11" =>
          wire_out <= pwr_wire_out;
          wire_T <= pwr_wire_T;
--        done <= pwr_done;         
        when others => null;
      end case;
    end if;
  end process io_multiplex;


  rw_sequence: process (clk, reset) is
  begin  -- process rw_sequence
    if reset = '1' then                 -- asynchronous reset (active high)
      rw_wire_out <= '1';
      rw_wire_T <= '1';
      rw_done <= '1'; -- done/ready
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bit_mode(1) = '0' then
        if go = '1' then
          rw_done <= '0';
        elsif clk_en = '1' then
          case timing_counter is
            when start_count =>
              -- start all sequences by driving the bus low
              rw_wire_out <= '0';
              rw_wire_T <= '0'; -- enable output
              rw_done <= '0';
              rw_bit_in <= '1';
            when rw_start_end_count =>
              -- the initial zero time is over, return to 'Z' unless we are writing
              -- a zero
              if current_mode(0) = '0' and write_bit = '0' then
                rw_wire_out <= '0';
              else
                rw_wire_T <= '1'; -- disable output
                rw_wire_out <= '1';
              end if;
            when rw_read_count =>
              -- if we are reading, now is the time to latch the bit            
              if current_mode(0) = '1' then
                rw_bit_in <= wire_in;
              end if;
            when rw_update_count =>
              -- we want the output to be 'Z' now (it shoudl already be unless we
              -- are writing a zero
              rw_wire_out <= '1';
              rw_wire_t <= '1';
              
            when rw_end_count =>
              -- This bit sequence is done, if this was a read, the bit is also
              -- ready now
              rw_done <= '1';
            when rw_idle_count =>
              -- idle state
              rw_wire_out <= '1';
              rw_wire_T <= '1'; -- disable output
            when others => null;
          end case;        
        end if;
      else
        rw_done <= '1';
        rw_wire_t <= '1';
        rw_wire_out <= '1';
      end if;
    end if;
  end process rw_sequence;

  init_sequence: process (clk, reset) is
  begin  -- process init_sequence
    if reset = '1' then                 -- asynchronous reset (active high)
      init_wire_out <= '1';
      init_wire_T <= '1';
      init_done <= '1'; -- done/re     
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bit_mode = "10" then
        if go = '1' then
          init_done <= '0';
        elsif clk_en = '1' then
          case timing_counter is
            when start_count =>
              -- start all sequences by driving the bus low
              init_wire_out <= '0';
              init_wire_T <= '0'; -- enable output
              init_done <= '0';
              init_bit_in <= '1';
            when init_start_end_count =>
              -- the initial zero time is over, return to 'Z'
              init_wire_T <= '1'; -- disable output
              init_wire_out <= '1';
            when init_read_count =>
              -- if we are reading, now is the time to latch the bit            
              init_bit_in <= wire_in;
            when init_update_count =>
              -- we want the output to be 'Z' now (it shoudl already be unless we
              -- are writing a zero
              init_wire_out <= '1';
              init_wire_T <= '1'; -- disable output
              
            when init_end_count =>
              -- This bit sequence is done, if this was a read, the bit is also
              -- ready now
              init_done <= '1';
            when init_idle_count =>
              -- idle state
              init_wire_out <= '1';
              init_wire_T <= '1'; -- disable output
            when others => null;
          end case;                  
        end if;
      else
        init_done <= '1';
        init_wire_t <= '1';
        init_wire_out <= '1';
      end if;      
    end if;
  end process init_sequence;

  pwr_sequence: process (clk, reset) is
  begin  -- process pwr_sequence
    if reset = '1' then                 -- asynchronous reset (active high)
      pwr_wire_out <= '1';
      pwr_wire_T <= '1';
      pwr_done <= '1'; -- done/re           
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bit_mode = "11" then
        if go = '1' then
          init_done <= '0';
        elsif clk_en = '1' then
          case timing_counter is
            when start_count =>
              -- write a powered '1' out to the bus
              pwr_wire_T <= '0'; -- tristate off
              pwr_wire_out <= '1'; -- power out
            when pwr_update_count =>
              -- power time is over
              -- we want the output to be 'Z' now
              pwr_wire_out <= '1';
              pwr_wire_t <= '1';            
            when pwr_end_count =>
              -- This bit sequence is done, if this was a read, the bit is also
              -- ready now
              pwr_done <= '1';
            when pwr_idle_count =>
              -- idle state
              pwr_wire_out <= '1';
              pwr_wire_T <= '1';
            when others => null;
          end case;        
        end if;      
      else
        pwr_done <= '1';
        pwr_wire_t <= '1';
        pwr_wire_out <= '1';
      end if;
    end if;
  end process pwr_sequence;

end architecture Behaviour;
