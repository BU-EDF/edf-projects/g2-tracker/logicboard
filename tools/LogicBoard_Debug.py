from utils import *

#read out a TDC event from the logicboard
#this skips the event builder and only works when the TDC is not enabled in the TDC control register
def LogicBoard_Readout_TDC(ser,TDC_no):
    #check if this TDC is disabled since this path only works in that case
    TDC_control_reg = int(WriteCommand(ser,"daq_reg_rd 40"),16)
    if TDC_control_reg & (1 << TDC_no):
        raise LB_EXP("Can not do a debug readout from an enabled TDC.")
    
    #build commands to use during readout
    fifo_read_command    = "daq_reg_wr " + hex(TDC_no*0x10 + 0x50)[2:] + " 8000"
    ht_bits_read_command = "daq_reg_rd " + hex(TDC_no*0x10 + 0x50)[2:] 
    data_read_command    = "daq_reg_rd " + hex(TDC_no*0x10 + 0x51)[2:]
    
    TDC_FIFO_empty_flag = (1 << int(TDC_no)) << 12  # build a mask for this TDC's fifo empty flag

    iword = 0
    while (TDC_control_reg & TDC_FIFO_empty_flag) == 0:
        # do a read on the fifo
        WriteCommand(ser,fifo_read_command)

        #get the control reg for the next read/check
        TDC_ht_bits = int(WriteCommand(ser,ht_bits_read_command),16)
        #read the head/trailer bits(in the control reg)
        header_bit = (TDC_ht_bits & 0x2) >> 1
        trailer_bit = TDC_ht_bits & 0x1
        
        #read the data word
        TDC_data = int(WriteCommand(ser,data_read_command),16)
        
        print TDC_no, "0x"+hex(iword)[2:].zfill(8), header_bit, trailer_bit, "0x"+hex(TDC_data)[2:].zfill(8)

        #read the TDC control word to see if there is more data
        TDC_control_reg = int(WriteCommand(ser,"daq_reg_rd 40"),16)
        iword = iword + 1

    return iword
