#!/usr/bin/python

from utils import *
from LogicBoard import *
from tdc_helpers import *
import serial
import os

#open the serial port
ser = serial.Serial("/dev/ttyUSB0",115200);

#init the logicboard 
LogicBoard_init(ser)

#init the logic daughter
LogicBoard_DB_init(ser)
time.sleep(1)
print "ALL on"
LogicBoard_DB_power(ser,True,True,True)
time.sleep(1)
LogicBoard_DB_rd_V(ser);
LogicBoard_DB_rd_I(ser);

#Loop over DAC values taking data at each one
thresholdVals = [0,100,200,300,350,400,425,450,475,500,525,550,575,600,625,650,675,700,750,800,900,1000,1200,1400,1600,1800,2000,2200,2400,2600]
#thresholdVals = [0,500,1000,1500,2000,2500,3000,3500]
#thresholdVals = [3500]
for writeVal in thresholdVals:

    #check that state is FF and send reset if not
    while int(WriteCommand(ser,"daq_reg_rd 40"),16) != int("0x0004F000",16):
        print "Reg 40 is not 0x000FF000. Reg 40 is ", WriteCommand(ser,"daq_reg_rd 40")
        LogicBoard_reset_event_builder(ser)
        time.sleep(1)

    #Call function to set BLR and TREF values
    LogicBoard_dac_set_noise_thresh(ser)

    #Write threshold value
    print "DAC value = ", writeVal, " threshold = ", 3.3*writeVal/4095.
    LogicBoard_dac_channel_set(ser,0x20,1,writeVal)
    LogicBoard_dac_channel_set(ser,0x20,5,writeVal)
    LogicBoard_dac_channel_set(ser,0x26,1,writeVal)
    LogicBoard_dac_channel_set(ser,0x26,5,writeVal)
    time.sleep(3)
 
    #send a c5 start of spill command
    os.system("./sendSpill.sh > spillsSent.txt")
    time.sleep(3)
    
    #read out each of the TDCs
    nWords = LogicBoard_Readout_TDC(ser,0)
    print "TDC 0: nWords = " , nWords
    nWords = LogicBoard_Readout_TDC(ser,1)
    print "TDC 1: nWords = " , nWords
    nWords = LogicBoard_Readout_TDC(ser,2)
    print "TDC 2: nWords = " , nWords
    nWords = LogicBoard_Readout_TDC(ser,3)
    print "TDC 3: nWords = " , nWords

    time.sleep(10)
