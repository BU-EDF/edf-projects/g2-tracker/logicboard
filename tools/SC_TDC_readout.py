#!/usr/bin/python

from utils import *
from LogicBoard import *
from tdc_helpers import *
import serial

#open the serial port
ser = serial.Serial("/dev/ttyUSB0",115200);

#init the logicboard 
#LogicBoard_init(ser,send_C5 = True)

#time.sleep(1)
#send a c5 start of spill command
#WriteCommand(ser,"daq_reg_wr 31 c")
#time.sleep(2)

#read out each of the TDCs
LogicBoard_Readout_TDC(ser,0)
LogicBoard_Readout_TDC(ser,1)
#LogicBoard_Readout_TDC(ser,2)
#LogicBoard_Readout_TDC(ser,3)
