from utils import *


def LogicBoard_dac_rd(ser, address, register):
    address = int(address)
    # check if address is in a valid range
    if address <0 or address > 255:
        raise I2C("invalid address")
    #check if register is in a valid range
    register = int(register)
    if register < 0 or register > 255:
        raise I2C("invalid register")
    #make sure the tdc i2c bus is selected
    WriteCommand(ser,"i2c_sel_dac")
    #do the write
    cmd = "i2c_reg_rd " + hex(address)[2:] + " " + hex(register)[2:]
    ret = WriteCommand(ser,cmd)
    try_count = 5
    while ret.find("Ack error") > -1:
        ret = WriteCommand(ser,cmd)
        if try_count > 0:
            try_count = try_count -1
        else:
            raise I2C("Ack error")
    return int(ret,16)

def LogicBoard_dac_wr(ser,address,register,data):
    address = int(address)
    # check if address is in a valid range
    if address <0 or address > 255:
        raise I2C("invalid address")
    #check if register is in a valid range
    register = int(register)
    if register < 0 or register > 255:
        raise I2C("invalid register")
    #check if data is in a valid range

    data = int(data)
    if data < 0 or data > 65535:
        raise I2C("invalid register")


    address = hex(address)[2:]
    data = hex(data)[2:].zfill(4)
    cmd = hex(register)[2:]

    #make sure the tdc i2c bus is selected
    WriteCommand(ser,"i2c_sel_dac")
    #do the write    
    cmd = "i2c_reg_wr " + address  + " " + cmd  + " " + data
    ret = WriteCommand(ser,cmd)
    try_count = 5
    while ret.find("Ack error") > -1:
        ret = WriteCommand(ser,cmd)
        if try_count > 0:
            try_count = try_count -1
        else:
            raise I2C("Ack error")
    return

    
def LogicBoard_dac_channel_set(ser,address,channel,value):
    #check if the channel is in a valid range
    channel = int(channel)
    if channel < 0 or channel > 7:
        raise LB_DAC("Bad channel "+channel)
    #check if data is in a valid range

    value = int(value)
    if value < 0 or value > 4095:
        raise LB_DAC("DAC value out of range")

    #build command to execute DAC codeN & loadN command
    dac_command = 0xb0 + channel
    value = value << 4
    #do write
    LogicBoard_dac_wr(ser,address,dac_command,value)

    return
    
def LogicBoard_dac_set_default(ser):
    print "Setting DAC values (high threshold)..."
    LogicBoard_dac_channel_set(ser,0x20,0,2730)
#    LogicBoard_dac_channel_set(ser,0x20,1,650)
    LogicBoard_dac_channel_set(ser,0x20,1,0xfff)
    LogicBoard_dac_channel_set(ser,0x20,2,0)
    LogicBoard_dac_channel_set(ser,0x20,3,0)
    LogicBoard_dac_channel_set(ser,0x20,4,2730)
#    LogicBoard_dac_channel_set(ser,0x20,5,650)
    LogicBoard_dac_channel_set(ser,0x20,5,0xfff)
    LogicBoard_dac_channel_set(ser,0x20,6,0)
    LogicBoard_dac_channel_set(ser,0x20,7,0)
    LogicBoard_dac_channel_set(ser,0x26,0,2730)
#    LogicBoard_dac_channel_set(ser,0x26,1,650)
    LogicBoard_dac_channel_set(ser,0x26,1,0xfff)
    LogicBoard_dac_channel_set(ser,0x26,2,0)
    LogicBoard_dac_channel_set(ser,0x26,3,0)
    LogicBoard_dac_channel_set(ser,0x26,4,2730)
#    LogicBoard_dac_channel_set(ser,0x26,5,650)
    LogicBoard_dac_channel_set(ser,0x26,5,0xfff)
    LogicBoard_dac_channel_set(ser,0x26,6,0)
    LogicBoard_dac_channel_set(ser,0x26,7,0)

def LogicBoard_dac_set_max_thresh(ser):
    print "Setting DAC values (high threshold)..."
    LogicBoard_dac_channel_set(ser,0x20,0,2730)
#    LogicBoard_dac_channel_set(ser,0x20,1,650)
    LogicBoard_dac_channel_set(ser,0x20,1,0xfff)
    LogicBoard_dac_channel_set(ser,0x20,2,0)
    LogicBoard_dac_channel_set(ser,0x20,3,0)
    LogicBoard_dac_channel_set(ser,0x20,4,2730)
#    LogicBoard_dac_channel_set(ser,0x20,5,650)
    LogicBoard_dac_channel_set(ser,0x20,5,0xfff)
    LogicBoard_dac_channel_set(ser,0x20,6,0)
    LogicBoard_dac_channel_set(ser,0x20,7,0)
    LogicBoard_dac_channel_set(ser,0x26,0,2730)
#    LogicBoard_dac_channel_set(ser,0x26,1,650)
    LogicBoard_dac_channel_set(ser,0x26,1,0xfff)
    LogicBoard_dac_channel_set(ser,0x26,2,0)
    LogicBoard_dac_channel_set(ser,0x26,3,0)
    LogicBoard_dac_channel_set(ser,0x26,4,2730)
#    LogicBoard_dac_channel_set(ser,0x26,5,650)
    LogicBoard_dac_channel_set(ser,0x26,5,0xfff)
    LogicBoard_dac_channel_set(ser,0x26,6,0)
    LogicBoard_dac_channel_set(ser,0x26,7,0)

def LogicBoard_dac_set_noise_thresh(ser):
    print "Setting DAC values (high threshold)..."
    LogicBoard_dac_channel_set(ser,0x20,0,2730)
    LogicBoard_dac_channel_set(ser,0x20,1,120)
    LogicBoard_dac_channel_set(ser,0x20,2,0)
    LogicBoard_dac_channel_set(ser,0x20,3,0)
    LogicBoard_dac_channel_set(ser,0x20,4,2730)
    LogicBoard_dac_channel_set(ser,0x20,5,120)
    LogicBoard_dac_channel_set(ser,0x20,6,0)
    LogicBoard_dac_channel_set(ser,0x20,7,0)
    LogicBoard_dac_channel_set(ser,0x26,0,2730)
    LogicBoard_dac_channel_set(ser,0x26,1,120)
    LogicBoard_dac_channel_set(ser,0x26,2,0)
    LogicBoard_dac_channel_set(ser,0x26,3,0)
    LogicBoard_dac_channel_set(ser,0x26,4,2730)
    LogicBoard_dac_channel_set(ser,0x26,5,120)
    LogicBoard_dac_channel_set(ser,0x26,6,0)
    LogicBoard_dac_channel_set(ser,0x26,7,0)

