from utils import *

def LogicBoard_TP_init(ser, enable_mask):
    #power up the chips, but disable the outputs
    value = 0x30
    WriteCommand(ser,"daq_reg_wr 05 ",value)
    value = value& 0xFFFFFFF0 + 0xF&enable_mask
    WriteCommand(ser,"daq_reg_wr 05 ",value)

#times in 25ns increments
def LogicBoard_TP_setup(ser, wait_time, pulse_to_pulse_time, pulse_count, channel_mask, enable_for_all_spills=False)
    #check if the system is powered up and enabled
    status = int(WriteCommand(ser,"daq_reg_rd 05"),16)
    if status & 0x30 != 0x30:
        print "Error: Pulse injectors are not powered up!"
        return

    for iBit in range(0,4):
        bit_mask = 0x3 << (iBit*2)
        if bit_mask & channel_mask != channel_mask:
            print "Error: tyring to send a pulse to disabled pulse channel"
            return
    
    #setup the first daq register
    value = wait_time & 0x000FFFFF #wait time
    value = value + (channel_mask << 20)&0x0FF00000 #channel mask
    if enable_for_all_spills:
        value = value | 0x80000000
    WriteCommand(ser,"daq_reg_wr ",0x06,value);

    #setup the second daq register
    value = pulse_to_pulse_time & 0x000FFFFF
    value = value + (pulse_count << 20)& 0x0FF00000
    WriteCommand(ser,"daq_reg_wr ",0x07,value);
    
    
