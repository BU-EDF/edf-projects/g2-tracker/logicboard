from utils import *

#resets the event builder
def LogicBoard_reset_event_builder(ser):
    #reset the event builder
    WriteCommand(ser,"daq_reg_wr 20 1")
    time.sleep(0.1)

#use_external_clock means use the incoming C5 clock to generate all DAQ clocks
#output_8b10b_normal means the data stream out of the logicboard on the SFP is 8b10b
#hijack_TDC_C5 means don't pass on the external C5 to the TDCs, but instead, send our own stream
def LogicBoard_init(ser,use_external_clock = True,output_8b10b_normal = True,hijack_TDC_c5 = False):
    print "Resetting logicboard"
    WriteCommand(ser,"reset") #make sure we aren't in another command
    WriteCommand(ser,"reset") #reset the board if it wasn't alread


    #start with Reseting the daq clock DCM
    clock_bits = 0x4
    WriteCommand(ser,["daq_reg_wr",0,clock_bits])

    if use_external_clock:
        C5_clock_locked = False
        while (not C5_clock_locked ):
            #also reset the external clocking
            clock_bits = (clock_bits & 0xFFFFFFFE) + 0x1
            #do the reset
            WriteCommand(ser,["daq_reg_wr",0,clock_bits])
            # re-enable the clock and check for 
            clock_bits = (clock_bits & 0xFFFFFFFE)
            WriteCommand(ser,["daq_reg_wr",0,clock_bits])            
            #check that we are locked            
            print "Checking for lock on external clock"
            clocking_status = int(WriteCommand(ser,["daq_reg_rd",0]),16)
            #check bit 17 for external clock locked
            if (clocking_status & (0x1 << 17) ):
                C5_clock_locked = True            
    else:
        # force us to use the internal derived C5 clock
        clock_bits = clock_bits & 0xFFFFFFFD + 0x2
        WriteCommand(ser,["daq_reg_wr",0,clock_bits])

    #now we should either be using a locked on external clock, or we are using the local clock
    DAQ_clock_locked = False
    # turn off the reset on the daq clocking
    while (not DAQ_clock_locked ):
        # ensure the daq clock is in reset
        clock_bits = (clock_bits & 0xFFFFFFFB) + 0x4
        WriteCommand(ser,["daq_reg_wr",0,clock_bits])
        # re-enable the daq clock and check for 
        clock_bits = (clock_bits & 0xFFFFFFFB)
        WriteCommand(ser,["daq_reg_wr",0,clock_bits])            
        #check that we are locked            
        print "Checking for lock on daq clock"
        clocking_status = int(WriteCommand(ser,["daq_reg_rd",0]),16)
        #check bit 17 for external clock locked
        if (clocking_status & (0x1 << 18) ):
            DAQ_clock_locked = True            

    #We now zero our clock watching bits so we know if something changes
    clock_bits = (clock_bits & 0xFFFFFFF7) +0x8
    WriteCommand(ser,["daq_reg_wr",0,clock_bits])            


    #choose which C5 clock to send the TDCs
    rd_value = int(WriteCommand(ser,"daq_reg_rd 30"),16)
    if hijack_TDC_c5:
        rd_value = rd_value | 0x00000004
    else:
        rd_value = rd_value & 0xFFFFFFFB

    line = "daq_reg_wr 30 " + hex(rd_value)[2:]
    WriteCommand(ser,line)    

    #enable the SFP
    if output_8b10b_normal:
        rd_value = 0x00000000
    else:
        rd_value = 0x00000010

    line = "daq_reg_wr 09 " + hex(rd_value)[2:]
    WriteCommand(ser,["daq_reg_wr 09 ",rd_value])


    LogicBoard_reset_event_builder(ser)

    print "Logic board ready"

