from utils import *


def LogicBoard_DB_rd(ser, address, register,one_byte_reads = False):
    address = int(address)
    # check if address is in a valid range
    if address <0 or address > 255:
        raise I2C("invalid address")
    #check if register is in a valid range
    register = int(register)
    if register < 0 or register > 255:
        raise I2C("invalid register")
    #make sure the tdc i2c bus is selected
    WriteCommand(ser,"i2c_sel_db")
    #do the write
    cmd = "i2c_reg_rd " + hex(address)[2:] + " " + hex(register)[2:]
    ret = WriteCommand(ser,cmd)
    try_count = 5
    while ret.find("Ack error") > -1:
        ret = WriteCommand(ser,cmd)
        if try_count > 0:
            try_count = try_count -1
        else:
            raise I2C("Ack error")
    if one_byte_reads:
        return (int(ret,16)>>8)&0xFF
    return int(ret,16)

def LogicBoard_DB_wr(ser,address,register,data,one_byte_writes = False):
    address = int(address)
    # check if address is in a valid range
    if address < 0 or address > 255:
        raise I2C("invalid address")
    #check if register is in a valid range
    register = int(register)
    if register < 0 or register > 255:
        raise I2C("invalid register")
    #check if data is in a valid range

    data = int(data)
    if data < 0 or data > 255:
        raise I2C("invalid register")


    address = hex(address)[2:]
    #switch between one and two byte writes for the io expander
    if one_byte_writes:
        data = hex(data)[2:].zfill(2)
    else:
        data = hex(data)[2:].zfill(4)
    cmd = hex(register)[2:]

    #make sure the tdc i2c bus is selected
    WriteCommand(ser,"i2c_sel_db")
    #do the write    
    cmd = "i2c_reg_wr " + address  + " " + cmd  + " " + data
    ret = WriteCommand(ser,cmd)
    try_count = 5
    while ret.find("Ack error") > -1:
        ret = WriteCommand(ser,cmd)
        if try_count > 0:
            try_count = try_count -1
        else:
            raise I2C("Ack error")
    return


    
def LogicBoard_DB_init(ser):
    #setup the io expander (0x4E)
    LogicBoard_DB_wr(ser,0x4E,0xA,0x42,True) #enable one interrupt that is drivent and active high
    
    #set the polarity of the pins
    LogicBoard_DB_wr(ser,0x4E,2,0x00,True)
    LogicBoard_DB_wr(ser,0x4E,3,0x00,True)

    #Turn on change of state interrupts off
    LogicBoard_DB_wr(ser,0x4E,0x4,0x00,True)
    LogicBoard_DB_wr(ser,0x4E,0x5,0x00,True)

    #Set the default values for interrupts
    LogicBoard_DB_wr(ser,0x4E,0x6,0x00,True)
    LogicBoard_DB_wr(ser,0x4E,0x7,0x00,True)

    #enable interrupts based on default values
    LogicBoard_DB_wr(ser,0x4E,0x8,0x7E,True)
    LogicBoard_DB_wr(ser,0x4E,0x9,0x5C,True)

    #turn off pull up resistors
    LogicBoard_DB_wr(ser,0x4E,0xC,0x00,True)
    LogicBoard_DB_wr(ser,0x4E,0xD,0x00,True)

    #set the default output values of the channels
    #first disable the regulators and reset the current trips
    LogicBoard_DB_wr(ser,0x4E,0x14,0x7F,True) 
    LogicBoard_DB_wr(ser,0x4E,0x15,0xFC,True) 

    #set the R/W direction of the pins (this applies the output values already set)
    LogicBoard_DB_wr(ser,0x4E,0,0x7E,True)
    LogicBoard_DB_wr(ser,0x4E,1,0x5c,True)

    #leave the regulators off, but now remove the reset state on the current monitors
    LogicBoard_DB_wr(ser,0x4E,0x14,0x01,True) #just disable
    LogicBoard_DB_wr(ser,0x4E,0x15,0xA0,True) #just disable
#    LogicBoard_DB_wr(ser,0x4E,0x14,0x81,True) #just disable
#    LogicBoard_DB_wr(ser,0x4E,0x15,0xA3,True) #just disable

    return

def LogicBoard_DB_set_p3V0(ser,turn_on = False):
    #do a read to get the reset states
    outputs = LogicBoard_DB_rd(ser,0x4E,0x14,True)
    #bit zero of 0x14 is the disable bit for 3V
    if turn_on: #turn on +3V
        outputs = (outputs&0xFFFE)  
    else :    #turn off +3V
        outputs = (outputs&0xFFFE) + 1
    LogicBoard_DB_wr(ser,0x4E,0x14,outputs & 0xFF,True)

def LogicBoard_DB_set_n3V0(ser,turn_on = False):
    #do a read to get the reset states
    outputs = LogicBoard_DB_rd(ser,0x4E,0x15,True)
    #bit 7 of 0x15 is the disable bit for -3V
    if turn_on: #turn on +3V
        outputs = (outputs&0x7F)  
    else :    #turn off +3V
        outputs = (outputs&0x7F) + 0x80
    LogicBoard_DB_wr(ser,0x4E,0x15,outputs & 0xFF,True)

def LogicBoard_DB_set_p1V4(ser,turn_on = False):
    #do a read to get the reset states
    outputs = LogicBoard_DB_rd(ser,0x4E,0x15,True)
    #bit 5 of 0x15 is the disable bit for 1.4V
    if turn_on: #turn on +1.4V
        outputs = (outputs&0xDF)  
    else :    #turn off +1.4V
        outputs = (outputs&0xDF) + 0x20
    LogicBoard_DB_wr(ser,0x4E,0x15,outputs & 0xFF,True)

    
def LogicBoard_DB_power(ser,P_3V0 = False,N_3V0 = False,P_1V4 = False):
    LogicBoard_DB_set_p3V0(ser,P_3V0)
    LogicBoard_DB_set_p1V4(ser,P_1V4)
    LogicBoard_DB_set_n3V0(ser,N_3V0)


def LogicBoard_DB_ADC_rd(ser,addr,channel):
    if channel < 0 or channel > 7:
        raise LB_DB("Bad ADC channel");
    #i2c command for the ADC to convert and readout is 0x80 + channel number on bits 6-4
    command = 0x80 + (channel<<4)
    return LogicBoard_DB_rd(ser,addr,command)


def LogicBoard_DB_rd_V(ser):
    print "Logic Daughter Supply Voltages:"
    print "+3V:   %.2f V" % float((3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x44,3)))
    print "+1.4V: %.2f V" % float((3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x44,0)))
    print "-3V:  -%.2f V" % float((3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x42,0)))

def LogicBoard_DB_rd_I(ser):
    print "Logic Daughter Currents:"
    p3_0 = 100*(3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x42,2))
    p3_1 = 100*(3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x44,7))
    p3_2 = 100*(3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x42,6))
    p3_3 = 100*(3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x42,4))
    print "+3V:   %.0f mA; %.0f mA; %.0f mA; %.0f mA" % (p3_0, p3_1, p3_2, p3_3)

    print "+1.4V: %.0f mA" % float(200*(3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x44,2)))

    n3_0 = 100*(3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x42,5))
    n3_1 = 100*(3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x42,7))
    n3_2 = 100*(3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x42,1))
    n3_3 = 100*(3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x42,3))
    print "-3V:   %.0f mA; %.0f mA; %.0f mA; %.0f mA" % (n3_0, n3_1, n3_2, n3_3)

    print "TDCs:  %.0f mA" % float(500*(3.3/float(0xFFF))*float(0xFFF&LogicBoard_DB_ADC_rd(ser,0x44,4)))

