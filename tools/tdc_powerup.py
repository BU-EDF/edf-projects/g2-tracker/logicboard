#!/usr/bin/python

from utils import *
from LogicBoard import *
from tdc_helpers import *
import serial

#open the serial port
ser = serial.Serial("/dev/ttyUSB0",115200);

#init the logicboard 

WriteCommand(ser,"daq_reg_wr 00 7")
time.sleep(0.1)
WriteCommand(ser,"daq_reg_wr 00 6")
time.sleep(0.1)
WriteCommand(ser,"daq_reg_wr 00 2")
time.sleep(0.1)
WriteCommand(ser,"daq_reg_wr 00 0")
time.sleep(0.1)
WriteCommand(ser,"daq_reg_wr 00 8")
WriteCommand(ser,"daq_reg_wr 20 1")
time.sleep(0.1)
print WriteCommand(ser,"daq_reg_rd 40")

print ""

print "52" WriteCommand(ser,"daq_reg_rd 52")
print "53" WriteCommand(ser,"daq_reg_rd 53")
print "54" WriteCommand(ser,"daq_reg_rd 54")
print "55" WriteCommand(ser,"daq_reg_rd 55")
print "56" WriteCommand(ser,"daq_reg_rd 56")
print "57" WriteCommand(ser,"daq_reg_rd 57")

print ""

print "62" WriteCommand(ser,"daq_reg_rd 62")
print "63" WriteCommand(ser,"daq_reg_rd 63")
print "64" WriteCommand(ser,"daq_reg_rd 64")
print "65" WriteCommand(ser,"daq_reg_rd 65")
print "66" WriteCommand(ser,"daq_reg_rd 66")
print "67" WriteCommand(ser,"daq_reg_rd 67")

print ""

print "72" WriteCommand(ser,"daq_reg_rd 72")
print "73" WriteCommand(ser,"daq_reg_rd 73")
print "74" WriteCommand(ser,"daq_reg_rd 74")
print "75" WriteCommand(ser,"daq_reg_rd 75")
print "76" WriteCommand(ser,"daq_reg_rd 76")
print "77" WriteCommand(ser,"daq_reg_rd 77")

print ""

print "82" WriteCommand(ser,"daq_reg_rd 82")
print "83" WriteCommand(ser,"daq_reg_rd 83")
print "84" WriteCommand(ser,"daq_reg_rd 84")
print "85" WriteCommand(ser,"daq_reg_rd 85")
print "86" WriteCommand(ser,"daq_reg_rd 86")
print "87" WriteCommand(ser,"daq_reg_rd 87")






##read out each of the TDCs
#LogicBoard_Readout_TDC(ser,0)
#LogicBoard_Readout_TDC(ser,1)
#LogicBoard_Readout_TDC(ser,2)
#LogicBoard_Readout_TDC(ser,3)
