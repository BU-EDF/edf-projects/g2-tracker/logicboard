#!/usr/bin/python

from utils import *
from LogicBoard import *
from tdc_helpers import *
import serial

#open the serial port
ser = serial.Serial("/dev/ttyUSB0",115200);

#init the logicboard 
LogicBoard_init(ser,send_C5 = True)

time.sleep(1)

#LogicBoard_dac_wr(ser,0x20,0xc2,0xFFF0) #load all max
LogicBoard_dac_channel_set(ser,0x20,0,0x7FF)
LogicBoard_dac_channel_set(ser,0x20,1,0x7FF)
LogicBoard_dac_channel_set(ser,0x20,2,0x7FF)
LogicBoard_dac_channel_set(ser,0x20,3,0x7FF)
LogicBoard_dac_channel_set(ser,0x20,4,0x7FF)
LogicBoard_dac_channel_set(ser,0x20,5,0x7FF)
LogicBoard_dac_channel_set(ser,0x20,6,0x7FF)
LogicBoard_dac_channel_set(ser,0x20,7,0x7FF)

WriteCommand(ser,"daq_reg_wr 40 000f")

while True:
#send a c5 start of spill
    WriteCommand(ser,"daq_reg_wr 31 c")
    time.sleep(2)

    #read the started and sent event numbers
    events_started = int(WriteCommand(ser,"daq_reg_rd 21"),16)
    events_finished = int( WriteCommand(ser,"daq_reg_rd 22"),16)
    print "Events in/out: ",events_started,"/",events_finished

    #read the TDC 2c kchar counts
    
    locked_on = (int(WriteCommand(ser,"daq_reg_rd 40"),16)&0xF0000) >> 16
    print "Locked: ", hex(locked_on)
    
    count_TDC_0 = int(WriteCommand(ser,"daq_reg_rd 51"),16)
    count_TDC_1 = int(WriteCommand(ser,"daq_reg_rd 61"),16)
    count_TDC_2 = int(WriteCommand(ser,"daq_reg_rd 71"),16)
    count_TDC_3 = int(WriteCommand(ser,"daq_reg_rd 81"),16)

    count_i2c_TDC_0 = -1
    count_i2c_TDC_1 = -1
    count_i2c_TDC_2 = -1
    count_i2c_TDC_3 = -1

    try:    
        count_i2c_TDC_0 = int(TDCBoard_read(ser,0xa8,0x1c))
        count_i2c_TDC_1 = int(TDCBoard_read(ser,0xaa,0x1c))
        count_i2c_TDC_2 = int(TDCBoard_read(ser,0xac,0x1c))
        count_i2c_TDC_3 = int(TDCBoard_read(ser,0xae,0x1c))
    except I2C:
        print "Error on TDC read"
    


    print "TDC_0(",(locked_on & 0x1),    "): ",count_TDC_0,"(",count_i2c_TDC_0,")"
    print "TDC_1(",(locked_on & 0x2)>>1, "): ",count_TDC_1,"(",count_i2c_TDC_1,")"
    print "TDC_2(",(locked_on & 0x4)>>2, "): ",count_TDC_2,"(",count_i2c_TDC_2,")"
    print "TDC_3(",(locked_on & 0x8)>>3, "): ",count_TDC_3,"(",count_i2c_TDC_3,")"
