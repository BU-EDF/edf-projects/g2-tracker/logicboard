from utils import *

#Sets register 40 to add TDC to event builder
def LogicBoard_enable_TDC(ser,TDC):
    rd_value = int(WriteCommand(ser,"daq_reg_rd 40"),16)
    mask = 1 << TDC
    writeValue = rd_value | mask
    cmd = "daq_reg_wr 40 " + hex(writeValue)[2:]
    WriteCommand(ser,cmd)
    print "TDC",TDC,"enabled in event builder."
    time.sleep(0.1)


#Sets register 40 to remove TDC from event builder
def LogicBoard_disable_TDC(ser,TDC):
    rd_value = int(WriteCommand(ser,"daq_reg_rd 40"),16)
    mask = ~(1 << TDC)
    writeValue = rd_value & mask
    cmd = "daq_reg_wr 40 " + hex(writeValue)[2:]
    WriteCommand(ser,cmd)
    print "TDC",TDC,"disabled in event builder."
    time.sleep(0.1)
