#!/usr/bin/python

from utils import *
from LogicBoard import *
from tdc_helpers import *
import serial

#open the serial port
ser = serial.Serial("/dev/ttyUSB0",115200);

#init the logicboard 
#LogicBoard_init(ser)
#LogicBoard_init(ser,send_C5 = True)

#LogicBoard_dac_set_max_thresh(ser)
LogicBoard_dac_set_noise_thresh(ser)

LogicBoard_DB_init(ser)

time.sleep(1)
print "ALL on"
LogicBoard_DB_power(ser,True,True,True)
time.sleep(1)
LogicBoard_DB_rd_V(ser);
LogicBoard_DB_rd_I(ser);
