from utils import *
from LogicBoard import *

def TDCBoard_present(ser,TDC):
    cmd = "tdc_reg_rd " + hex(TDC)[2:] + " 1"
    if "ERR" in WriteCommand(ser,cmd).strip():
        present=False
    else:
        present=True

    return present


def TDCBoard_read(ser,TDC,register):
    if TDCBoard_present(ser,TDC):
        cmd = "tdc_reg_rd " + hex(TDC)[2:] + " " + hex(register)[2:]
        value = WriteCommand(ser,cmd).strip()
        return value
    else:
        print "TDC",TDC,"not present. Failed to read"


def TDCBoard_write(ser,TDC,register,value):

    if TDCBoard_present(ser,TDC):
        cmd = "tdc_reg_wr " + hex(TDC)[2:] + " " + hex(register)[2:] + " " + hex(value)[2:]
        ret = WriteCommand(ser,cmd)
        try_count = 50
        while "ERR" in ret:
            ret = WriteCommand(ser,cmd)
            if try_count > 0:
                try_count = try_count-1
            else: 
                print "Failed to write cmd",cmd 

    else:
        print "TDC",TDC,"not present. Failed to write"


def TDCBoard_TDC_nums(ser):
    TDCs = []
    for TDC in range(0,4):
        if TDCBoard_present(ser,TDC):
            TDCs.append(TDC)
    print "TDCs present:",TDCs


def TDCBoard_enable_spill(ser,TDC):
    if TDCBoard_present(ser,TDC):
        TDCBoard_write(ser,TDC,0,2)
        print "TDC", TDC, "set to accept spills."
    else:
        print "TDC", TDC, "not present. Failed to enable."

def TDCBoard_disable_spill(ser,TDC):
    if TDCBoard_present(ser,TDC):
        TDCBoard_write(ser,TDC,0,0)
        print "TDC", TDC, "set to ignore spills."
    else:
        print "TDC", TDC, "not present. Failed to disable."


def TDCBoard_set_scaler_on(ser,TDC):
    if TDCBoard_present(ser,TDC):
        currentReg = int(TDCBoard_read(ser,TDC,1),16)
        mask = 1 << 5
        writeValue = currentReg | mask
        TDCBoard_write(ser,TDC,1,writeValue)
        ret = TDCBoard_read(ser,TDC,1)
        print "TDC", TDC, "has register 1 set to","0x"+ret,"=", bin(int(ret,16))
    else:
        print "TDC", TDC, "not present. Scaler not set."


def TDCBoard_set_scaler_off(ser,TDC):
    if TDCBoard_present(ser,TDC):
        currentReg = int(TDCBoard_read(ser,TDC,1),16)
        mask = ~(1 << 5) 
        writeValue = currentReg & mask
        TDCBoard_write(ser,TDC,1,writeValue)
        ret = TDCBoard_read(ser,TDC,1)
        print "TDC", TDC, "has register 1 set to","0x"+ret,"=", bin(int(ret,16))
    else:
        print "TDC", TDC, "not present. Scaler not set."


def TDCBoard_set_edges_on(ser,TDC):
    if TDCBoard_present(ser,TDC):
        currentReg = int(TDCBoard_read(ser,TDC,1),16)
        mask = 1 << 4
        writeValue = currentReg | mask
        TDCBoard_write(ser,TDC,1,writeValue)
        ret = TDCBoard_read(ser,TDC,1)
        print "TDC", TDC, "has register 1 set to","0x"+ret,"=", bin(int(ret,16))
    else:
        print "TDC", TDC, "not present. Both edges value not set."


def TDCBoard_set_edges_off(ser,TDC):
    if TDCBoard_present(ser,TDC):
        currentReg = int(TDCBoard_read(ser,TDC,1),16)
        mask = ~(1 << 4) 
        writeValue = currentReg & mask
        TDCBoard_write(ser,TDC,1,writeValue)
        ret = TDCBoard_read(ser,TDC,1)
        print "TDC", TDC, "has register 1 set to","0x"+ret,"=", bin(int(ret,16))
    else:
        print "TDC", TDC, "not present. Both edges value not set."


def TDCBoard_set_chan_mask(ser,TDC,mask):
    if TDCBoard_present(ser,TDC):
        TDCBoard_write(ser,TDC,2,mask)
        ret = TDCBoard_read(ser,TDC,2)
        print "TDC", TDC, "has channel mask","0x"+ret,"=", bin(int(ret,16))
    else:
        print "TDC", TDC, "not present. Channel mask not set."


def TDCBoard_set_start_time(ser,TDC,time):
    if TDCBoard_present(ser,TDC):
        TDCBoard_write(ser,TDC,3,time)
        ret = TDCBoard_read(ser,TDC,3)
        print "TDC", TDC, "has start time","0x"+ret,"=", int(ret,16)
    else:
        print "TDC", TDC, "not present. Start time not set."


def TDCBoard_set_selb_time(ser,TDC,time):
    if TDCBoard_present(ser,TDC):
        TDCBoard_write(ser,TDC,4,time)
        ret = TDCBoard_read(ser,TDC,4)
        print "TDC", TDC, "has selb time","0x"+ret,"=", int(ret,16)
    else:
        print "TDC", TDC, "not present. Selb time not set."


def TDCBoard_set_end_time(ser,TDC,time):
    if TDCBoard_present(ser,TDC):
        TDCBoard_write(ser,TDC,5,time)
        ret = TDCBoard_read(ser,TDC,5)
        print "TDC", TDC, "has end time","0x"+ret,"=", int(ret,16)
    else:
        print "TDC", TDC, "not present. End time not set."


def TDCBoard_set_readout_time(ser,TDC,time):
    if TDCBoard_present(ser,TDC):
        TDCBoard_write(ser,TDC,6,time)
        ret = TDCBoard_read(ser,TDC,6)
        print "TDC", TDC, "has readout time","0x"+ret,"=", int(ret,16)
    else:
        print "TDC", TDC, "not present. Readout time not set."


def TDCBoard_set_defaults(ser,TDC):
    TDCBoard_set_scaler_on(ser,TDC)
    TDCBoard_set_edges_off(ser,TDC)
    TDCBoard_set_chan_mask(ser,TDC,0xFFFF)
    TDCBoard_set_start_time(ser,TDC,10)
    TDCBoard_set_selb_time(ser,TDC,0xFFFD)
    TDCBoard_set_end_time(ser,TDC,0xFFFE)
    TDCBoard_set_readout_time(ser,TDC,0xFFFF)
    

def TDCBoard_enable_TDC(ser,TDC):
    TDCBoard_set_defaults(ser,TDC)
    TDCBoard_enable_spill(ser,TDC)
    LogicBoard_enable_TDC(ser,TDC)


def TDCBoard_disable_TDC(ser,TDC):
    TDCBoard_disable_spill(ser,TDC)
    LogicBoard_disable_TDC(ser,TDC)


def TDCBoard_enable_all_TDCs(ser):
    TDCBoard_TDC_nums(ser)
    for TDC in range(0,4):
        if TDCBoard_present(ser,TDC):
            TDCBoard_enable_TDC(ser,TDC)
        else:
            print "TDC",TDC,"not present."
