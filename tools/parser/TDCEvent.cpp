#include <TDCEvent.h>
#include <string.h>
#include <algorithm>    // std::find


TDCEvent::TDCEvent()
{
  data = NULL;
  data_size = 0;
}

size_t TDCEvent::Parse(TDCInfo * info,uint32_t * _data, size_t buffer_size)
{
  //Check if this TDC is enabled
  if( info->TDC_enabled == 0) {
    return 0;
  }
  
  // Size checks on incoming data
  if (_data == NULL){
    printf("Bad TDC data pointer\n");
    return 0;
  } else if (buffer_size < sizeof(TDCHeader)/sizeof(uint32_t) ){
    printf("TDC data too small to be an event. (%zu < %lu)",
	   buffer_size,sizeof(TDCHeader)/sizeof(uint32_t));
    return 0;
  }
  //Valid data
  data = _data;
  data_size = buffer_size;

  //================================
  //Check header
  std::vector<size_t> bad_lines;
  TDCHeader * header = (TDCHeader *) data;

  //Start of spill word
  if ((header->start_of_spill_zeros != 0) || (header->start_of_spill != 0x3C)){
    printf("TDC: Bad Header magic\n");
    bad_lines.push_back(0);
  }

  //g-2TDC
  char c_gm2TDC[24];
  memset(c_gm2TDC,0,sizeof(c_gm2TDC));
  c_gm2TDC[0]  = 'g';
  c_gm2TDC[4]  = 0x2D;
  c_gm2TDC[8]  = '2';
  c_gm2TDC[12] = 'T';
  c_gm2TDC[16] = 'D';
  c_gm2TDC[20] = 'C';
  for( size_t iLine = 0; iLine < sizeof(c_gm2TDC);iLine+=4){
    //check if there is any difference between the four chars of this line
    if( strncmp(&(c_gm2TDC[iLine]),&(header->gm2TDC[iLine]),4) ){
      printf("TDC: Bad header line: %zu\n",1+(iLine>>2));
      bad_lines.push_back(1+(iLine>>2));
    }
  }

  //size word
  if ( (info->size) != (header->size) ){
    printf("TDC: LB size and TDC size don't agree. (%d/%d)\n",info->size,header->size);
    bad_lines.push_back(8);
  }
  if ( header->size > data_size) {
    printf("TDC: size is larger than buffer size, parsing up to buffer size");
    bad_lines.push_back(8);
  }
  if (header->size_bit_15 != 0){
    printf("TDC: TDC size bit 15 isn't zero\n");
    bad_lines.push_back(8);
  }

  //Check spill number zeros
  if (header->spill_number_zeros != 0){
    printf("TDC: Spill number zeros are not zero\n");
    bad_lines.push_back(9);
  }

  //Check Time LSB zeros
  if (header->time_zeros != 0){
    printf("TDC: Time lsb zeros are not zero\n");
    bad_lines.push_back(11);
  }

  //Check blank line
  if (header->blank_line != 0){
    printf("TDC: blank line is not zero\n");
    bad_lines.push_back(14);
  }

  //Check misc line zeros
  if (header->zeros != 0){
    printf("TDC: misc line zeros are non-zero\n");
    bad_lines.push_back(15);
  }


  //================================
  //Check data words
  for(size_t pos = sizeof(TDCHeader)/sizeof(uint32_t);
      ((pos < header->size) && (pos < data_size));
      pos++){
    // get current hit
    TDCHit * current_hit = (TDCHit * ) &(data[pos]);
    //check hit
    if (current_hit->magic != 1) {
      printf("TDC: bad magic on hit %zu\n",pos - sizeof(TDCHeader)/sizeof(uint32_t));
      bad_lines.push_back(pos);
    }
  }

  if ( bad_lines.size() ){
    //Print out the TDC raw data highlighting bad words

    //make sure we don't run off the end of valid data
    size_t end = header->size;
    if (data_size < end){
      end = data_size;
    }
    
    PrintRange(0,end,bad_lines);
  }

  if( header->size < data_size){
    return header->size;    
  }
  return data_size;
}

void TDCEvent::PrintRange(size_t start, size_t end, std::vector<size_t> attn){
  if (end >= data_size) {
    printf("PrintRange end is greater than data_size,using data_size\n");
    end = data_size;
  }
  //sort any lines of interest
  std::sort(attn.begin(),attn.end());

  std::vector<size_t>::iterator it;
  for(size_t iWord = start; iWord < end;iWord++){
    //See if this is a special line
    if ((it = std::find(attn.begin(),attn.end(),iWord)) != attn.end()){
      printf(">>> 0x%08zX: 0x%08X\n",iWord,data[iWord]);
      attn.erase(it);	//remove this special line from the list
    } else {
      // Print the normal line
      printf("0x%08zX: 0x%08X\n",iWord,data[iWord]);
    }
  }
}


