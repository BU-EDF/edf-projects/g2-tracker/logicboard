#include <LBEvent.h>
#include <TDCEvent.h>
#include <algorithm>    // std::find


LBEvent::LBEvent()
{
  data = NULL; //Null out the data parser pointer
}

bool LBEvent::Parse(uint32_t * _data, size_t buffer_size)
{
  // Size checks on incoming data
  if (_data == NULL){
    printf("Bad LBEvent data pointer\n");
    return false;
  } else if (buffer_size < (sizeof(LBHeader)/sizeof(uint32_t) + sizeof(LBTrailer)/sizeof(uint32_t))){
    printf("LBEvent data too small to be an event. (%zu < %zu)",
	   buffer_size,
	   sizeof(LBHeader)/sizeof(uint32_t) + sizeof(LBTrailer)/sizeof(uint32_t));
    return false;
  }
  //Valid data
  data = _data;
  data_size = buffer_size;

  //================================
  //Check header
  std::vector<size_t> bad_lines;
  LBHeader * header = (LBHeader * ) data;
  LBTrailer * trailer = NULL;
  
  //Start of spill word
  if ((header->start_of_spill_zeros != 0) || (header->start_of_spill != 0x3C)){
    printf("LB: Bad Header magic\n");
    bad_lines.push_back(0);
  }
  //Size word
  if (header->size_zeros != 0) {
    printf("LB: Unexpected non-zeros in size word\n");
    bad_lines.push_back(1);
  }
  if (header->size > data_size) {
    printf("LB: size greater than data. (parsing what we can)\n");
    bad_lines.push_back(1);
  } else {
    trailer = (LBTrailer*) (data + header->size - sizeof(trailer)/sizeof(uint32_t));
  }  
  
  //Spill number
  if (header->spill_zeros != 0){
    printf("LB: Unexpected non-zeros in spill number word");
    bad_lines.push_back(2);
  }

  //Misc line
  if (header->zeros != 0){
    printf("LB: Unexpected non-zeros in spill number word");
    bad_lines.push_back(3);
  }
  if (header->error){
    printf("LB: Event has an error\n");
    bad_lines.push_back(3);
  }
  if (header->fatal_error){
    printf("LB: Event has a fatal(FIFO) error\n");
    bad_lines.push_back(3);
  }
  uint8_t TDCs_OOS = header->TDC_en & header->TDC_OOS;
  for(size_t iTDC = 0; iTDC < 4;iTDC++){
    if ((TDCs_OOS >> iTDC)&0x1){
      printf("LB: TDC %zu is out of sync\n",iTDC);
      bad_lines.push_back(3);
    }
  }

  //Print the data if there was an error
  if (bad_lines.size()){
    PrintRange(0,sizeof(LBHeader)/sizeof(uint32_t),bad_lines);
  }
  
  //==============
  // Parse each TDC info line and the TDC that goes with it
  uint32_t * parse_pos = data + sizeof(LBHeader)/sizeof(uint32_t);
  size_t size_left = data_size - sizeof(LBHeader)/sizeof(uint32_t);
  uint32_t words_read = 0;
  for( size_t iTDC = 0; iTDC < 4; iTDC++){
    words_read = TDC[iTDC].Parse(&(header->TDC_info[iTDC]),parse_pos,size_left);    
    parse_pos += words_read;
    size_left -= words_read;
  }

  //==============
  //check trailer (if valid)
  std::vector<size_t> bad_lines_trailer;
  if (trailer != NULL){
    if (trailer != (LBTrailer*)parse_pos) {
      printf("LB: Inconsistant trailer position. Got/Expected (0x%zX/0x%zX)\n",
	     ((uint32_t*) trailer)-data,(parse_pos-data));
      trailer = (LBTrailer *) parse_pos;
    } 
    //Check CRC
    
    //Check size
    if (header->size != trailer->size){
      printf("LB: header/trailer size aren't equal (0x%X/0x%X)\n",header->size,trailer->size);
      bad_lines_trailer.push_back( ((uint32_t*)trailer) - data + 1);
      printf("%zu\n",((uint32_t*)trailer) - data + 1); 
    }
    //Check last word
    if ((trailer->End_Of_Spill != 0x5C) || (trailer->End_Of_Spill_zeros != 0)){
      printf("LB: Bad trailer\n");
      bad_lines_trailer.push_back(((uint32_t*)trailer)-data + 2);
    }
  }

  //Print data if there was an error
  if( bad_lines_trailer.size() > 0){
    PrintRange(trailer->size - sizeof(LBTrailer)/sizeof(uint32_t),trailer->size,bad_lines_trailer);
  }


  return (bad_lines.size() + bad_lines_trailer.size());
}

void LBEvent::PrintRange(size_t start, size_t end, std::vector<size_t> attn){
  if (end >= data_size) {
    printf("PrintRange end is greater than data_size,using data_size\n");
    end = data_size;
  }
  //sort any lines of interest
  std::sort(attn.begin(),attn.end());

  std::vector<size_t>::iterator it;
  for(size_t iWord = start; iWord < end;iWord++){
    //See if this is a special line
    if ((it = std::find(attn.begin(),attn.end(),iWord)) != attn.end()){
      printf(">>> 0x%08zX: 0x%08X\n",iWord,data[iWord]);
      attn.erase(it);	//remove this special line from the list
    } else {
      // Print the normal line
      printf("0x%08zX: 0x%08X\n",iWord,data[iWord]);
    }
  }
}

