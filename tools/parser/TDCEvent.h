#ifndef __TDCEVENT_H__
#define __TDCEVENT_H__
#include <stdint.h>
#include <vector>
#include <cstddef>
#include <cstdio>

struct TDCInfo {
  // size
  uint32_t size : 15;
  uint32_t blank : 14;
  uint32_t size_error : 1;
  uint32_t OOS : 1;
  uint32_t TDC_enabled : 1;
};

struct TDCHit {
  uint32_t time : 24;
  uint32_t channel : 4;
  uint32_t edge : 1;
  uint32_t magic :3;
};

struct TDCHeader {
  //Start of spill: 0
  uint32_t start_of_spill:8;
  uint32_t start_of_spill_zeros:24;
  //g-2TDC: 1- 6
  char gm2TDC[24];
  //firmware date: 7
  char ver_day[1];
  char ver_month[1];
  char ver_year[2];
  //channel mask and size :8 
  uint32_t channel_mask:16;
  uint32_t size:15;
  uint32_t size_bit_15:1;
  //spill number :9 
  uint32_t spill_number:24;
  uint8_t  spill_number_zeros:8;
  //Time MSB : 10
  uint32_t time_MSB;
  //Time LSB: 11
  uint32_t time_LSB:12;  
  uint32_t time_zeros:20;
  //start and end time: 12
  uint16_t end_time;
  uint16_t start_time;
  //readout and selb time: 13
  uint16_t readout_time;
  uint16_t selB_time;
  //address line: 14
  uint32_t hw_addr:2;
  uint32_t sw_addr:2;
  uint32_t blank_line:28;
  //misc line: 15
  uint32_t edges:1;
  uint32_t scalars_en:1;
  uint32_t zeros:30;
  uint32_t scalars[16];
};

class TDCEvent{
public:
  TDCEvent();
  size_t Parse(TDCInfo * info,uint32_t * _data,size_t buffer_size);
private:
  void PrintRange(size_t start, size_t end, std::vector<size_t> attn);
private:
  uint32_t * data;
  uint32_t data_size;
};

#endif
