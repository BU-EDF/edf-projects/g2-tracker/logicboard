#ifndef __LBEVENT_H__
#define __LBEVENT_H__
#include <stdint.h>
#include <TDCEvent.h>


struct LBHeader {
  //Start of spill
  uint32_t start_of_spill_zeros:24;
  uint32_t start_of_spill:8;
  //size word
  uint32_t size:16;
  uint32_t size_zeros:16;
  // date
  uint32_t version:8;
  uint32_t day:8;
  uint32_t month:8;
  uint32_t year:8;
  // spill number
  uint32_t spill_number:24;
  uint32_t spill_zeros:8;
  //Time MSB
  uint32_t time_MSB;
  //Misc line
  uint32_t time_LSB:12;
  uint32_t TDC_en:4;
  uint32_t TDC_OOS:4;
  uint32_t error:1;
  uint32_t fatal_error:1;
  uint32_t spill_type:3;  
  uint32_t zeros:7;

  TDCInfo TDC_info[4];
};

struct LBTrailer {
  uint32_t size;
  uint32_t CRC;
  uint32_t End_Of_Spill:8;
  uint32_t End_Of_Spill_zeros:24;
};

class LBEvent {
public:
  LBEvent();
  bool Parse(uint32_t * _data, size_t buffer_size);
  
private:
  void PrintRange(size_t start,size_t end,std::vector<size_t> attn);
private: 
  uint32_t * data;
  size_t data_size;
  TDCEvent TDC[4];
};
#endif
