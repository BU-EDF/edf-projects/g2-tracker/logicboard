#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include <iostream>

#include "LBEvent.h"

std::vector<uint32_t> BinaryRead(std::string filename){
  //Find file size
  int iFD = open(filename.c_str(),O_RDONLY);
  size_t read_ret = 0;
  std::vector<uint32_t> data;
  uint32_t word;
  while( (read_ret = read(iFD,&word,sizeof(uint32_t))) == sizeof(uint32_t)){
    data.push_back(word);
  }
  return data;
}

std::vector<uint32_t> ASCIIRead(std::string filename){
  //Find file size
  std::ifstream inFile(filename.c_str());
  std::vector<uint32_t> data;
  std::copy(std::istream_iterator<uint32_t>(inFile >> std::hex),
	    std::istream_iterator<uint32_t>(),
	    std::back_inserter(data));
  std::cout << data.size() << std::endl;
  return data;
}


int main()
{
  //  std::vector<uint32_t> data = BinaryRead("out.dat");
    std::vector<uint32_t> data = ASCIIRead("GoodEvent.txt");
  //std::vector<uint32_t> data = ASCIIRead("BadEvent.txt");

  
  LBEvent event;
  event.Parse(&(data[0]),data.size());
  
  return 0;
}
