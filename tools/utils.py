import serial
import time

#def WriteCommand(ser,line):
#    if line.find("\n") != -1:
#        raise cmd("new line in command")
#    ser.write(line)
#    ser.write("\n")
#    rd = ser.read()
#    while rd != "\n":
#        rd = ser.read()
#    rd = ser.read()
#    line = ""
#    while rd != ">":
#        line = line + rd
#        rd = ser.read()
##    line = line.split()
#    return line


def WriteCommand(ser,parts):
    # generate the full command if given a list
    cmd = ""
    if type(parts) == type([]):        
        for part in parts:
            if type(part) == type(int):
                cmd = cmd + hex(part)[2:] + " "
            else :
                cmd = cmd + str(part) + " "
    else :
        cmd = parts;
    #make sure there are not new-lines
    if cmd.find("\n") != -1:
        raise cmd("new line in command")
    #send the command and our one new-line
#    print cmd
    ser.write(cmd)
    ser.write("\n")
    #read back the UART echo of our command (and \n)
    rd = ser.read()
    while rd != "\n":
        rd = ser.read()
    rd = ser.read()
    #read back and store everything else until the ">" char and return
    cmd = ""
    while rd != ">":
        cmd = cmd + rd
        rd = ser.read()
    return cmd
