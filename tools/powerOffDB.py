#!/usr/bin/python

from utils import *
from LogicBoard import *
from tdc_helpers import *
import serial

#open the serial port
ser = serial.Serial("/dev/ttyUSB0",115200);

time.sleep(1)
print "ALL off"
LogicBoard_DB_power(ser,False,False,False)
time.sleep(1)
LogicBoard_DB_rd_V(ser);
LogicBoard_DB_rd_I(ser);
