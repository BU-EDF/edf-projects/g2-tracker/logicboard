#!/usr/bin/python

from utils import *
from LogicBoard import *
from tdc_helpers import *
import serial

#open the serial port
ser = serial.Serial("/dev/ttyUSB0",115200);

#init the logicboard 
#LogicBoard_init(ser)
#LogicBoard_init(ser,send_C5 = True)
#time.sleep(1)

#init the logicboard_db and switch on all power
#LogicBoard_DB_init(ser)
#LogicBoard_DB_power(ser,True,True,True)
#time.sleep(1)

writeVal = 0
while True:
    print writeVal
    LogicBoard_dac_channel_set(ser,0x20,0,writeVal)
    LogicBoard_dac_channel_set(ser,0x20,1,writeVal)
    LogicBoard_dac_channel_set(ser,0x20,2,writeVal)
    LogicBoard_dac_channel_set(ser,0x20,3,writeVal)
    LogicBoard_dac_channel_set(ser,0x20,4,writeVal)
    LogicBoard_dac_channel_set(ser,0x20,5,writeVal)
    LogicBoard_dac_channel_set(ser,0x20,6,writeVal)
    LogicBoard_dac_channel_set(ser,0x20,7,writeVal)
    LogicBoard_dac_channel_set(ser,0x26,0,writeVal)
    LogicBoard_dac_channel_set(ser,0x26,1,writeVal)
    LogicBoard_dac_channel_set(ser,0x26,2,writeVal)
    LogicBoard_dac_channel_set(ser,0x26,3,writeVal)
    LogicBoard_dac_channel_set(ser,0x26,4,writeVal)
    LogicBoard_dac_channel_set(ser,0x26,5,writeVal)
    LogicBoard_dac_channel_set(ser,0x26,6,writeVal)
    LogicBoard_dac_channel_set(ser,0x26,7,writeVal)
    writeVal = writeVal+20
    time.sleep(0.1)
    if writeVal > 1000:
        writeVal = 0
