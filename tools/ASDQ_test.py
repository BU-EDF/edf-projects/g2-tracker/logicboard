#!/usr/bin/python

from utils import *
from LogicBoard import *
from tdc_helpers import *
import serial

#open the serial port
ser = serial.Serial("/dev/ttyUSB0",115200);

#init the logicboard 
LogicBoard_init(ser,send_C5 = True)

time.sleep(1)

#LogicBoard_dac_wr(ser,0x20,0xc2,0xFFF0) #load all max
LogicBoard_dac_channel_set(ser,0x20,0,0x9b1)
LogicBoard_dac_channel_set(ser,0x20,1,0x88 )
LogicBoard_dac_channel_set(ser,0x20,2,0x0  )
LogicBoard_dac_channel_set(ser,0x20,3,0x0  )
LogicBoard_dac_channel_set(ser,0x20,4,0x9b1)
LogicBoard_dac_channel_set(ser,0x20,5,0x88 )
LogicBoard_dac_channel_set(ser,0x20,6,0x0  )
LogicBoard_dac_channel_set(ser,0x20,7,0x0  )

LogicBoard_dac_channel_set(ser,0x26,0,0x9b1)
LogicBoard_dac_channel_set(ser,0x26,1,0x88 )
LogicBoard_dac_channel_set(ser,0x26,2,0x0  )
LogicBoard_dac_channel_set(ser,0x26,3,0x0  )
LogicBoard_dac_channel_set(ser,0x26,4,0x9b1)
LogicBoard_dac_channel_set(ser,0x26,5,0x88 )
LogicBoard_dac_channel_set(ser,0x26,6,0x0  )
LogicBoard_dac_channel_set(ser,0x26,7,0x0  )

time.sleep(1)

#power up ASDQs
LogicBoard_DB_power(ser,True,True,True)

#send a c5 start of spill command
WriteCommand(ser,"daq_reg_wr 31 c")
time.sleep(2)

#read out each of the TDCs
print "TDC0"
LogicBoard_Readout_TDC(ser,0)
print "TDC1"
LogicBoard_Readout_TDC(ser,1)
print "TDC2"
LogicBoard_Readout_TDC(ser,2)
print "TDC3"
LogicBoard_Readout_TDC(ser,3)

#set a new DAC level
LogicBoard_dac_channel_set(ser,0x20,0,0x9b1)
LogicBoard_dac_channel_set(ser,0x20,1,0x19a)
LogicBoard_dac_channel_set(ser,0x20,2,0x0  )
LogicBoard_dac_channel_set(ser,0x20,3,0x0  )
LogicBoard_dac_channel_set(ser,0x20,4,0x9b1)
LogicBoard_dac_channel_set(ser,0x20,5,0x19a )
LogicBoard_dac_channel_set(ser,0x20,6,0x0  )
LogicBoard_dac_channel_set(ser,0x20,7,0x0  )

LogicBoard_dac_channel_set(ser,0x26,0,0x9b1)
LogicBoard_dac_channel_set(ser,0x26,1,0x19a)
LogicBoard_dac_channel_set(ser,0x26,2,0x0  )
LogicBoard_dac_channel_set(ser,0x26,3,0x0  )
LogicBoard_dac_channel_set(ser,0x26,4,0x9b1)
LogicBoard_dac_channel_set(ser,0x26,5,0x19a)
LogicBoard_dac_channel_set(ser,0x26,6,0x0  )
LogicBoard_dac_channel_set(ser,0x26,7,0x0  )
time.sleep(1)

#power up ASDQs
LogicBoard_DB_power(ser,True,True,True)

#send a c5 start of spill command
WriteCommand(ser,"daq_reg_wr 31 c")
time.sleep(2)

#read out each of the TDCs
print "TDC0"
LogicBoard_Readout_TDC(ser,0)
print "TDC1"
LogicBoard_Readout_TDC(ser,1)
print "TDC2"
LogicBoard_Readout_TDC(ser,2)
print "TDC3"
LogicBoard_Readout_TDC(ser,3)

#power up ASDQs
LogicBoard_DB_power(ser)
